import numpy as np
from scipy.optimize import ridder, brentq, brenth


from sms_ray_modelling.raytrace import (
    isotropic_ray_trace,
    one_way_isotropic_raytrace_layered,
    get_vertical_slowness_isotropic,
    get_layer,
    get_layer_stack_along_ray,
)

from nmxseis.model.phase import Phase
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.velocity_model.one_dimensional import (
    VelocityModel1D,
    VelocityModel1DLayer,
)

velocity_model = [
    {"rho": 2180.0, "vp": 2446.0, "vs": 1069.0},
    {"rho": 2324.0, "vp": 3163.0, "vs": 1513.0, "top": 460.0},
    {"rho": 2421.0, "vp": 3726.0, "vs": 1862.0, "top": -20.0},
    {"rho": 2451.0, "vp": 3911.0, "vs": 2046.0, "top": -220.0},
    {"rho": 2495.0, "vp": 4201.0, "vs": 2301.0, "top": -1140.0},
    {"rho": 2734.0, "vp": 6052.0, "vs": 3303.0, "top": -1480.0},
    {"rho": 2686.0, "vp": 5637.0, "vs": 3027.0, "top": -1720.0},
    {"rho": 2749.0, "vp": 6186.0, "vs": 3275.0, "top": -1920.0},
    {"rho": 2494.0, "vp": 4190.0, "vs": 2288.0, "top": -2320.0},
    {"rho": 2614.0, "vp": 5063.0, "vs": 2687.0, "top": -2560.0},
]

VELMOD = VelocityModel1D(
    [
        VelocityModel1DLayer(**{"rho": 2180.0, "vp": 2446.0, "vs": 1069.0}),
        VelocityModel1DLayer(
            **{"rho": 2324.0, "vp": 3163.0, "vs": 1513.0, "top": 460.0}
        ),
        VelocityModel1DLayer(
            **{"rho": 2421.0, "vp": 3726.0, "vs": 1862.0, "top": -20.0}
        ),
        VelocityModel1DLayer(
            **{"rho": 2451.0, "vp": 3911.0, "vs": 2046.0, "top": -220.0}
        ),
        VelocityModel1DLayer(
            **{"rho": 2495.0, "vp": 4201.0, "vs": 2301.0, "top": -1140.0}
        ),
        VelocityModel1DLayer(
            **{"rho": 2734.0, "vp": 6052.0, "vs": 3303.0, "top": -1480.0}
        ),
        VelocityModel1DLayer(
            **{"rho": 2686.0, "vp": 5637.0, "vs": 3027.0, "top": -1720.0}
        ),
        VelocityModel1DLayer(
            **{"rho": 2749.0, "vp": 6186.0, "vs": 3275.0, "top": -1920.0}
        ),
        VelocityModel1DLayer(
            **{"rho": 2494.0, "vp": 4190.0, "vs": 2288.0, "top": -2320.0}
        ),
        VelocityModel1DLayer(
            **{"rho": 2614.0, "vp": 5063.0, "vs": 2687.0, "top": -2560.0}
        ),
    ]
)

SOURCE = np.array([0, 0, -2700])
RECEIVER = np.array([10, 2000, 900])

raypath = Raypath.isotropic_ray_trace(SOURCE, RECEIVER, VELMOD, Phase.P)
VELMOD.get_layer_index_by_elevation(-2700)

VELMOD[0]


SOURCE

p_e, p_n = raypath.hrz_slowness_e, raypath.hrz_slowness_n
p = [p_e, p_n]

# _get_isotropic_vertical_slowness(*p, raypath.velocity_model_slice.source.vel)

1 / np.sqrt(sum([v**2 for v in takeoff_vector]))


def get_raypath(raypath):
    from nmxseis.numerics.ray_modeling import (
        _get_isotropic_vertical_slowness,
        one_way_isotropic_raytrace_layered,
    )

    vert_slowness_source = _get_isotropic_vertical_slowness(
        raypath.hrz_slowness_e,
        raypath.hrz_slowness_n,
        raypath.velocity_model_slice.source.vel,
    )
    takeoff_vector = np.array(
        [raypath.hrz_slowness_e, raypath.hrz_slowness_n, vert_slowness_source]
    )
    ray = one_way_isotropic_raytrace_layered(
        takeoff_vector, SOURCE, VELMOD, Phase.P, total_traveltime=raypath.traveltime
    )
    ray = one_way_isotropic_raytrace_layered(
        takeoff_vector, SOURCE, VELMOD, Phase.P, final_elevation=900
    )

    np.array(ray)[:, :3]


from nmxseis.numerics.ray_modeling import _calc_homogeneous_ray_path, _calc_transit_time

layer = VelocityModel1DLayer(rho=2700, vp=5000, vs=3000)


slowness = np.array([0.5, 0, np.sqrt(0.75)]) / layer.vp
ray = _calc_homogeneous_ray_path(slowness, 1)
np.sqrt(sum([r**2 for r in ray]))
np.linalg.norm(ray)

slowness = np.array([0.5 * np.sqrt(0.5), 0.5 * np.sqrt(0.5), np.sqrt(0.75)]) / layer.vp
ray = _calc_homogeneous_ray_path(slowness, 1)
np.sqrt(sum([r**2 for r in ray]))

_calc_transit_time(100, slowness)


slowness

100 * 2 / np.sqrt(3) / 5000

1000 * slowness[2]
1000 / np.sqrt(3) / 5000
ray
ray[-1][0]


raypath.up_down_sign

layer_stack = get_layer_stack_along_ray(source["z"], velocity_model)
phase = "P"
vel_key = "v" + phase.lower()
max_vel = max([layer[vel_key] for layer in layer_stack])


max_amgle = np.arcsin(layer_stack[0][vel_key] / max_vel)


start_elevation = source["z"]
i_start_layer = get_layer(start_elevation, velocity_model, get_index=True)
i_end_layer = get_layer(900, velocity_model, get_index=True)
velocity_model[9::-1]
velocity_model[0]


raypath_two_point = isotropic_ray_trace(source, receiver, velocity_model, "P")


slowness = {
    **raypath_two_point["hrz_slowness"],
    "z": get_vertical_slowness_isotropic(
        raypath_two_point["hrz_slowness"], get_layer(source["z"], velocity_model)["vp"]
    ),
}
phase = "P"
end_elevation = 900
end_horizontal = 5000
takeoff_angle = angle2


def one_way_ray_tracer_wrapper(
    takeoff_angle, end_horizontal, velocity_model, source, end_elevation, phase
):
    takeoff_vector = {"e": 0, "n": np.sin(takeoff_angle), "z": np.cos(takeoff_angle)}
    raypath = one_way_isotropic_raytrace_layered(
        takeoff_vector, source, velocity_model, phase, final_elevation=end_elevation
    )
    return raypath[-1]["n"] - end_horizontal


angle1 = 0
angle2 = max_amgle / 2
initial_residual = one_way_ray_tracer_wrapper(
    angle1, 5000, velocity_model, source, 900, "P"
)
next_residual = one_way_ray_tracer_wrapper(
    angle2, 5000, velocity_model, source, 900, "P"
)
while initial_residual * next_residual > 0:
    delta_angle = angle2 - angle1
    angle1 = angle2
    angle2 += delta_angle / 2
    initial_residual = next_residual
    next_residual = one_way_ray_tracer_wrapper(
        angle2, 5000, velocity_model, source, 900, "P"
    )

final_angle = brenth(
    one_way_ray_tracer_wrapper,
    angle1,
    angle2,
    args=(5000, velocity_model, source, 900, "P"),
)
