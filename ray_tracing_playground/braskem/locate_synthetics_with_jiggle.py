from collections import defaultdict
import json
from pathlib import Path
import sys
import time

import matplotlib.pyplot as plt

import numpy as np
from obspy import read, read_inventory, UTCDateTime


from nmxseis.interact.basic import PickSet
from nmxseis.model.nslc import NSL
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.location.locator import Locator
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

for tt_rms in [0.002, 0.01, 0.02, 0.05]:

    np.random.seed(288)

    base_dir = Path("braskem")
    with open(base_dir.joinpath("velocity_model.json")) as f:
        velocity_model_json = json.load(f)
    layers = []
    for layer in velocity_model_json:
        layers.append(
            VMLayer1D(
                vp=layer["vp"],
                vs=layer["vs"],
                rho=layer["rho"],
                top=layer["top"] if "top" in layer else np.inf,
            )
        )

    velocity_model = VelocityModel1D(layers)

    inv = read_inventory(base_dir.joinpath("FBK_Full.xml"))
    reproject = EPSGReprojector(32725)

    def raytracer(source_enu, receiver_enu, phase):
        return Raypath.isotropic_ray_trace(
            source_enu, receiver_enu, velocity_model, phase
        )

    def tt_function(source_enu, receiver_enu, phase):
        return raytracer(source_enu, receiver_enu, phase).traveltime_sec

    br_nsls = [nsl for nsl in NSL.iter_inv(inv) if nsl.net == "BR"]
    station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
    average_station_location = np.average(list(station_enus.values()), axis=0)
    locator = Locator(
        tt_function, np.array([*average_station_location]), raytracer=raytracer
    )

    event_ids = [
        path.name.split(".json")[0]
        for path in base_dir.joinpath("event_locations").glob("*.json")
    ]

    image_dir = base_dir.joinpath(f"location_image_{int(tt_rms*1000)}_ms")

    if not image_dir.is_dir():
        image_dir.mkdir()

    catalog = defaultdict()
    for event_id in event_ids:
        with open(base_dir.joinpath("event_locations", f"{event_id}.json")) as f:
            event_json = json.load(f)
        now = time.time()
        source = EventOrigin(
            lat=event_json["latitude"],
            lon=event_json["longitude"],
            depth_m=event_json["depth_m"],
            time=event_json["time"],
        )
        source_enu = source.enu(reproject)
        pick_set = PickSet.load(
            base_dir.joinpath("synthetic_picks", f"{event_id}.picks")
        )
        br_pick_set = PickSet(picks=[p for p in pick_set.picks if p.nsl.net == "BR"])
        for pick in br_pick_set.picks:
            pick.time += tt_rms * np.random.randn()

        location = locator.locate_tt(br_pick_set, station_enus)
        min_pick, rescaled_picks, rescaled_stations = locator.rescale_inputs(
            br_pick_set, station_enus
        )
        rescaled_init_location = locator.get_starting_enut(
            rescaled_picks, rescaled_stations
        )
        init_enu = rescaled_init_location[:3] + locator.enu_0

        fig, (ax_p, ax_d) = plt.subplots(1, 2, sharey=True)

        ax_p.plot(*source_enu[:2], "*", color="orangered", mec="0.1", zorder=4, ms=10)
        ax_d.plot(
            *source_enu[1:][::-1], "*", color="orangered", mec="0.1", zorder=4, ms=10
        )
        for nsl in br_nsls:
            ax_p.plot(*station_enus[nsl][:2], "v", color="w", mec="0.2", ms=10)
            ax_d.plot(*station_enus[nsl][1:][::-1], ">", color="w", mec="0.2", ms=10)
        ax_p.plot(*location[0][:2], "o", color="gold", mec="0.2", zorder=5)
        ax_d.plot(*location[0][1:][::-1], "o", color="gold", mec="0.2", zorder=5)
        ax_p.plot(*init_enu[:2], ".", color="0.1", mec="0.1", zorder=6)
        ax_d.plot(*init_enu[1:][::-1], ".", color="0.1", mec="0.1", zorder=6)
        for ax in (ax_p, ax_d):
            ax.set_aspect("equal")
            gray_background_with_grid(ax, grid_spacing=100)
        ax_d.invert_xaxis()
        fig.suptitle(f"location with picks perturbed by {int(tt_rms*1000)} ms")
        fig.savefig(image_dir.joinpath(f"{event_id}.png"))
        catalog[event_id] = {
            "athena_source_enu": source_enu.tolist(),
            "athena_origin_time": source.time,
            "inverted_source_enu": location[0].tolist(),
            "inverted_origin_time": location[1].timestamp,
        }
        print(tt_rms, event_id, time.time() - now)
        plt.close(fig)
    with open(
        base_dir.joinpath(
            f"tt_inverted_locations_perturbed_by_{int(tt_rms*1000)}ms.json"
        ),
        "w",
    ) as f:
        json.dump(catalog, f)
