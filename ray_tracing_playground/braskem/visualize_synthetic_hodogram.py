from collections import defaultdict
from dataclasses import dataclass
import json
from pathlib import Path
import random

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from obspy import read, read_inventory, UTCDateTime
from pyproj import Proj

from nmxseis.model.basic import Window, NSLP
from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import get_station_enus, EPSGReprojector
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D


from plotting import plot_3C_trace, add_window_to_trace, plot_hodogram

cscale = cm.get_cmap("magma")

event_id = "93507"
base_dir = Path("braskem")

inv = read_inventory(base_dir.joinpath("FBK_Full.xml"))

with open(base_dir.joinpath("event_locations", f"{event_id}.json")) as f:
    event_json = json.load(f)
with open(base_dir.joinpath("velocity_model.json")) as f:
    velocity_model_json = json.load(f)

layers = []
for layer in velocity_model_json:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )
velocity_model = VelocityModel1D(layers)

stream = read(base_dir.joinpath("synthetic_waveforms", f"{event_id}.mseed"))
source = EventOrigin(
    lat=event_json["latitude"],
    lon=event_json["longitude"],
    depth_m=event_json["depth_m"],
    time=event_json["time"],
)
epsg = event_json["epsg"]

source_enu = source.enu(epsg)

stream_3c = NSL.partition_stream(stream)


station_enus = get_station_enus(inv, Proj(epsg), stream_3c.keys())


def get_hodo_windows(pick_set, window, pre_seconds, post_seconds) -> dict[NSLP, Window]:
    windows = defaultdict()
    for (
        (nsl, ph),
        pick,
    ) in pick_set:
        windows[nsl, ph] = Window(
            pick_set.picks[nsl, ph] - pre_seconds,
            pick_set.picks[nsl, ph] + post_seconds,
        )
    return windows


def measure_hodograms_for_event(stream, pick_set, windows):
    hodograms = defaultdict()
    for (nsl, ph), window in windows:
        tr_3c = stream.select(network=nsl.net, station=nsl.sta, location=nsl.loc)
        hodograms[nsl, ph] = Hodogram.from_windowed_phase(tr_3c.copy(), ph, window)
    return hodograms


fig, ax = plt.subplots()
ax.set_aspect("equal")
ax.plot(*source_enu[:2], "*", color="orangered", mec="0.1", zorder=4, ms=10)
hodograms: dict[(NSL, Phase), Hodogram] = defaultdict()
rays: dict[(NSL, Phase), Raypath] = defaultdict()
windows: dict[(NSL, Phase), Window] = defaultdict()
for nsl in [k for k in stream_3c]:

    tr_3c = stream.select(network=nsl.net, station=nsl.sta, location=nsl.loc)
    rays[nsl, Phase.P] = Raypath.isotropic_ray_trace(
        source_enu, station_enus[nsl], velocity_model, Phase.P
    )
    rays[nsl, Phase.S] = Raypath.isotropic_ray_trace(
        source_enu, station_enus[nsl], velocity_model, Phase.S
    )
    p_time = UTCDateTime(source.time + rays[nsl, Phase.P].traveltime_sec)
    s_time = UTCDateTime(source.time + rays[nsl, Phase.S].traveltime_sec)
    s_win_end = UTCDateTime(
        source.time
        + 2 * rays[nsl, Phase.S].traveltime_sec
        - rays[nsl, Phase.P].traveltime_sec
    )
    ps_sep = s_time - p_time
    windows[nsl, Phase.P] = Window([p_time - 0.1 * ps_sep, s_time - 0.1 * ps_sep])
    windows[nsl, Phase.S] = Window([s_time - 0.2 * ps_sep, s_win_end - 0.2 * ps_sep])
    hodograms[nsl, Phase.P] = Hodogram.from_windowed_phase(
        tr_3c.copy(), Phase.P, windows[nsl, Phase.P]
    )
    hodograms[nsl, Phase.S] = Hodogram.from_windowed_phase(
        tr_3c.copy(), Phase.S, windows[nsl, Phase.S]
    )
    ax.quiver(
        *station_enus[nsl][:2],
        np.sin(np.deg2rad(hodograms[nsl, Phase.P].trend_deg)),
        np.cos(np.deg2rad(hodograms[nsl, Phase.P].trend_deg)),
        pivot="middle",
        color="0.1",
        width=0.005,
        zorder=5,
        headlength=0,
        headaxislength=0,
        headwidth=0,
        scale=1 / 200,
        scale_units="x",
    )

    hodo_dot = abs(hodograms[nsl, Phase.P].dot_product_with_ray(rays[nsl, Phase.P]))
    ax.plot(*station_enus[nsl][:2], "v", color=cscale(hodo_dot), mec="0.2", ms=10)


example_nsl = NSL.parse("BR.ESM02.")
example_st = stream.select(station=example_nsl.sta)

random_keys = random.sample(list(hodograms.keys()), 3)
random_hodos = {k: hodograms[k] for k in random_keys}


# fig_tr,ax_tr = plt.subplots(figsize=[12,4])
# plot_3C_trace(example_st, ax=ax_tr)
# add_window_to_trace(windows[example_nsl, Phase.P], ax_tr)
# ax_tr.set_title(nsl)
#
# fig_h, ax_h = plt.subplots(figsize=[5,5])
# plot_hodogram(example_st, windows[example_nsl, Phase.P], ax=ax_h)
xlim = ax.get_xlim()
ylim = ax.get_ylim()
xgrid = np.linspace(*xlim, 200)
ygrid = np.linspace(*ylim, 200)
objective_field = np.zeros([len(xgrid), len(ygrid)])
for i_x, x in enumerate(xgrid):
    for i_y, y in enumerate(ygrid):
        for (nsl, phase), hodogram in random_hodos.items():
            if phase.is_P:
                unit_vector = station_enus[nsl] - np.array([x, y, source_enu[-1]])
                unit_vector /= np.linalg.norm(unit_vector)
                objective_field[i_x, i_y] += abs(
                    np.dot(unit_vector, hodogram.unit_vector_enu)
                )

print("done")
ax.pcolor(xgrid, ygrid, objective_field.T, zorder=-1, shading="auto")
plt.show()
