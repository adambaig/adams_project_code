from collections import defaultdict
import json
from pathlib import Path
import sys

import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D
from NocMeta.Meta import NOC_META

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")


from locate_synthetics_tt_hodo import get_hodo_windows, measure_hodograms_for_event

HODO_SCALE = 100
EPSG = 32725
BASE_DIR = Path("braskem")
ATHENA_CODE = "FBK"
STATION_XML = BASE_DIR.joinpath("FBK_Full.xml")
inventory = read_inventory(STATION_XML)
surface_nsls = [nsl for nsl in NSL.iter_inv(inventory) if "T" in nsl.sta]

athena_config = NOC_META[ATHENA_CODE]
athena_client = AthenaClient(
    rf'http://{athena_config["athIP"]}', athena_config["athApi"]
)

pick_error = {Phase.P: 0.010, Phase.S: 0.020}
pick_errors = {
    (nsl, ph): pick_error[ph]
    for nsl in NSL.iter_inv(inventory)
    for ph in [Phase.P, Phase.S]
}
hodogram_error_radians = {
    (nsl, ph): np.deg2rad(30)
    for nsl in NSL.iter_inv(inventory)
    for ph in [Phase.P, Phase.S]
}

with open(BASE_DIR.joinpath("velocity_model.json")) as f:
    velocity_model_json = json.load(f)

layers = []
for layer in velocity_model_json:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )

velocity_model = VelocityModel1D(layers)
inv = read_inventory(BASE_DIR.joinpath("FBK_Full.xml"))
reproject = EPSGReprojector(NOC_META["FBK"]["epsg"])


def raytracer(source_enu, receiver_enu, phase):
    return Raypath.isotropic_ray_trace(source_enu, receiver_enu, velocity_model, phase)


def tt_function(source_enu, receiver_enu, phase):
    return raytracer(source_enu, receiver_enu, phase).traveltime_sec


br_nsls = [nsl for nsl in NSL.iter_inv(inv) if nsl.net == "BR"]
station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
average_station_location = np.average(list(station_enus.values()), axis=0)
locator = OptimizingRelocator(tt_function, station_enus, epsg=EPSG, raytracer=raytracer)

event_ids = [
    path.name.split(".json")[0]
    for path in BASE_DIR.joinpath("event_locations").glob("*.json")
]

catalog = defaultdict()

event_id = "91340"
source, pick_set = athena_client.get_preferred_origin_and_picks_for_event(event_id)

source_enu = source.enu(reproject)
with open(BASE_DIR.joinpath("event_locations", f"{event_id}.json")) as f:
    event_json = json.load(f)
athena_source = EventOrigin(
    lat=event_json["latitude"],
    lon=event_json["longitude"],
    depth_m=event_json["depth_m"],
    time=event_json["time"],
)
stream = athena_client.read_event_waveforms(event_id).rotate("->ZNE", inventory=inv)
hodograms = measure_hodograms_for_event(
    stream, get_hodo_windows(pick_set, 0.005, 0.020)
)
reloc_result = locator.relocate(
    pick_set,
    pick_errors=pick_errors,
    hodograms=hodograms,
    hodogram_errors_radians=hodogram_error_radians,
    locator_options={"maxfev": 2000, "maxiter": 2000},
)
