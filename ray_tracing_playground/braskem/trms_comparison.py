import json
from pathlib import Path

from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory, UTCDateTime


from nmxseis.model.nslc import NSL
from nmxseis.model.pick_set import PickSet
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

start_time = UTCDateTime("Oct 6, 2022")

EPSG = 32613
base_dir = Path("NTR_Rocanville")
inv = read_inventory(base_dir.joinpath("NTR_RCN_Full.xml"))
reproject = EPSGReprojector(EPSG)
nsls = [nsl for nsl in NSL.iter_inv(inv) if nsl.loc != "D0"]
station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))

tt_only_json = base_dir.joinpath("athena_and_tt_inverted_locations.json")
tt_hodo_json = base_dir.joinpath("athena_and_tt_hodo_inverted_locations.json")

catalog = {}
for json_file, key in zip([tt_only_json, tt_hodo_json], ["tt_only", "tt_hodo"]):
    with open(json_file) as f:
        catalog[key] = json.load(f)

athena_enus = np.array(
    [event["athena_source_enu"] for event in catalog["tt_only"].values()]
)
inverted_tt_enus = np.array(
    [event["inverted_source_enu"] for event in catalog["tt_only"].values()]
)
inverted_tt_hodo_enus = np.array(
    [event["inverted_source_enu"] for event in catalog["tt_hodo"].values()]
)

with open(base_dir.joinpath("velocity_model.json")) as f:
    velocity_model_json = json.load(f)

layers = []
for layer in velocity_model_json:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )
velocity_model = VelocityModel1D(layers)


def raytracer(src_enu, rec_enu, phase):
    return Raypath.isotropic_ray_trace(src_enu, rec_enu, velocity_model, phase)


def tt_function(src_enu, rec_enu, phase):
    return ray_tracer(src_enu, rec_enu, phase).traveltime_sec


locator = OptimizingRelocator(tt_function, station_enus, epsg=EPSG, raytracer=raytracer)


def tt_rms(enu, origin_time, picks):
    min_picks, rescaled_picks, rescaled_stations = locator.rescale_inputs(
        picks, station_enus
    )
    enut = [*(enu - locator.enu_0), origin_time - min_picks.timestamp]
    return np.sqrt(locator.tt_residual(enut, rescaled_picks, rescaled_stations))


rms = {}
for event_id, locations in catalog["tt_only"].items():
    picks = PickSet.load(base_dir.joinpath("athena_picks", f"{event_id}.picks"))
    rms[event_id] = {
        "NLL": tt_rms(
            locations["athena_source_enu"], locations["athena_origin_time"], picks
        ),
        "simplex_tt_only": tt_rms(
            locations["inverted_source_enu"], locations["inverted_origin_time"], picks
        ),
        "simplex_tt_hodo": tt_rms(
            catalog["tt_hodo"][event_id]["inverted_source_enu"],
            catalog["tt_hodo"][event_id]["inverted_origin_time"],
            picks,
        ),
    }

fig, (ax_nll_athena, ax_nll_raytrace, ax_tt_only, ax_tt_hodo) = plt.subplots(
    4, figsize=[8, 13], sharex=True
)

tt_bins = [*np.linspace(0, 0.5, 51), 2]

with open(base_dir.joinpath("t_rms_athena.json")) as f:
    athena_trms = json.load(f)

ax_nll_athena.hist(
    (trms_nll_ath := athena_trms.values()), tt_bins, facecolor="firebrick"
)
ax_nll_raytrace.hist(
    (trms_nll_rt := [v["NLL"] for v in rms.values()]), tt_bins, facecolor="lightcoral"
)
ax_tt_only.hist(
    (trms_tt_only := [v["simplex_tt_only"] for v in rms.values()]),
    tt_bins,
    facecolor="steelblue",
)
ax_tt_hodo.hist(
    (trms_tt_hodo := [v["simplex_tt_hodo"] for v in rms.values()]),
    tt_bins,
    facecolor="royalblue",
)

for ax, trms_dist in zip(
    [ax_nll_athena, ax_nll_raytrace, ax_tt_only, ax_tt_hodo],
    [trms_nll_ath, trms_nll_rt, trms_tt_only, trms_tt_hodo],
):
    ax.text(
        0.95,
        0.8,
        f"median = {np.median(list(trms_dist)):.3f}s",
        ha="right",
        transform=ax.transAxes,
    )
    ax.text(
        0.95,
        0.73,
        f"mean = {np.average(list(trms_dist)):.3f}s",
        ha="right",
        transform=ax.transAxes,
    )


ax_tt_hodo.set_xlabel("$t_{rms}$ (s)")
ax_nll_athena.set_ylabel("NonLinLoc in Athena")
ax_nll_raytrace.set_ylabel("NonLinLoc residual\nfrom raytracing")
ax_tt_only.set_ylabel("simplex tt only")
ax_tt_hodo.set_ylabel("simplex tt + hodo")
ax_tt_only.set_xlim([0, 0.51])
fig.savefig(base_dir.joinpath("tt_rms_from_athena_histograms.png"))


cmap = cm.get_cmap("turbo")
fig_nll_rt, (ax_mp_rt, ax_xs_rt) = plt.subplots(2, figsize=[4, 5], sharex=True)
fig_nll_ath, (ax_mp_ath, ax_xs_ath) = plt.subplots(2, figsize=[4, 5], sharex=True)
for event_id, locations in catalog["tt_only"].items():
    ax_mp_rt.plot(
        (loc := locations["athena_source_enu"])[1],
        -loc[0],
        "o",
        color=(clr := cmap(rms[event_id]["NLL"] / 0.5)),
        mec="0.2",
    )
    ax_xs_rt.plot(loc[1], loc[2], "o", color=clr, mec="0.2")
    ax_mp_ath.plot(
        loc[1],
        -loc[0],
        "o",
        color=(clr := cmap(athena_trms[event_id] / 0.5)),
        mec="0.2",
    )
    ax_xs_ath.plot(loc[1], loc[2], "o", color=clr, mec="0.2")

xmin_p = np.percentile(athena_enus[:, 1], 1)
xmax_p = np.percentile(athena_enus[:, 1], 99)
ymin_p = np.percentile(-athena_enus[:, 0], 1)
ymax_p = np.percentile(-athena_enus[:, 0], 99)

x_range = xmax_p - xmin_p
y_range = ymax_p - ymin_p

xmin = xmin_p - 0.2 * x_range
xmax = xmax_p + 0.2 * x_range
ymin = ymin_p - 0.2 * y_range
ymax = ymax_p + 0.2 * y_range

for ax in ax_mp_rt, ax_mp_ath:
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
for ax in ax_xs_rt, ax_xs_ath:
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(-2500, 1000)

for ax in ax_xs_rt, ax_mp_rt, ax_xs_ath, ax_mp_ath:
    gray_background_with_grid(ax, grid_spacing=500)

for ax in ax_xs_rt, ax_xs_ath:
    ax.set_ylabel(r"$\longleftarrow$ Depth")
    ax.set_xlabel(r"Northing $\longrightarrow$")

for ax in ax_mp_rt, ax_mp_ath:
    ax.set_ylabel(r"$\longleftarrow$ Easting ")
fig_nll_ath.suptitle(r"Athena Locations, Athena t$_{\rm rms}$")
fig_nll_ath.savefig(base_dir.joinpath("athena_locations_athena_trms.png"))
fig_nll_rt.suptitle(r"Athena Locations, raytracing t$_{\rm rms}$")
fig_nll_rt.savefig(base_dir.joinpath("athena_locations_raytracing_trms.png"))

fg_cb = plt.figure(figsize=[4, 1])
ax_cb = fg_cb.add_axes([0.1, 0.6, 0.8, 0.2])
colorbar = np.array([np.linspace(0, 1, 1000), np.linspace(0, 1, 1000)])
ax_cb.pcolor(colorbar, cmap=cmap)
ax_cb.set_xticks([0, 200, 400, 600, 800, 1000])
ax_cb.set_xticklabels([0, 0.1, 0.2, 0.3, 0.4, 0.5])
ax_cb.set_yticks([])
ax_cb.set_xlabel(r"$t_{\rm rms}$ (s)")
fg_cb.savefig(base_dir.joinpath("t_rms_colorbar.png"))
plt.show()

plt.show()
