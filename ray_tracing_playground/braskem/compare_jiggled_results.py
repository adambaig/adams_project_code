import json
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np

basedir = Path("braskem").joinpath("L-BFGS-B_perturbation_runs")

distance_bins = np.arange(0, 100, 2)

catalog_jsons = basedir.glob("*ms.json")
for catalog_json in catalog_jsons:
    perturbation = catalog_json.stem.split("_")[-1]
    with open(catalog_json) as f:
        catalog = json.load(f)
    athena_enus = np.array([event["athena_source_enu"] for event in catalog.values()])
    inverted_enus = np.array(
        [event["inverted_source_enu"] for event in catalog.values()]
    )
    difference_enus = inverted_enus - athena_enus
    hrz_drift = np.hypot(difference_enus[:, 0], difference_enus[:, 1])
    vrt_drift = abs(difference_enus[:, 2])
    fig, (ax_h, ax_v) = plt.subplots(2, figsize=[8, 6], sharex=True)
    ax_h.hist(hrz_drift, bins=distance_bins)
    ax_v.hist(vrt_drift, bins=distance_bins)
    ax_v.set_xlabel("drift (m)")
    ax_h.set_ylabel("count")
    ax_v.set_ylabel("count")
    ax_h.set_title("horizontal")
    ax_v.set_title("vertical")
    fig.suptitle(f"perturbation of RMS {perturbation}")
    fig.savefig(basedir.joinpath(f"perturb_{perturbation}.png"))
