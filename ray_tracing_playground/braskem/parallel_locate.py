from collections import defaultdict
from joblib import Parallel, delayed
import json
from pathlib import Path
import sys
import time

import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory, UTCDateTime

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.nslc import NSL
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.location.locator import Locator, MinimizerFailedToConvergeError
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D
from NocMeta.Meta import NOC_META

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid


from braskem.locate_synthetics_tt_hodo import (
    get_hodo_windows,
    measure_hodograms_for_event,
)

hodo_scale = 100

base_dir = Path("braskem")
athena_code = "FBK"
STATION_XML = base_dir.joinpath("FBK_Full.xml")
inventory = read_inventory(STATION_XML)
surface_nsls = [nsl for nsl in NSL.iter_inv(inventory) if "T" in nsl.sta]

athena_config = NOC_META[athena_code]
athena_client = AthenaClient(
    rf'http://{athena_config["athIP"]}', athena_config["athApi"]
)

with open(base_dir.joinpath("velocity_model.json")) as f:
    velocity_model_json = json.load(f)

layers = []
for layer in velocity_model_json:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )

velocity_model = VelocityModel1D(layers)
inv = read_inventory(base_dir.joinpath("FBK_Full.xml"))
reproject = EPSGReprojector(NOC_META["FBK"]["epsg"])


def raytracer(source_enu, receiver_enu, phase):
    return Raypath.isotropic_ray_trace(source_enu, receiver_enu, velocity_model, phase)


def tt_function(source_enu, receiver_enu, phase):
    return raytracer(source_enu, receiver_enu, phase).traveltime_sec


br_nsls = [nsl for nsl in NSL.iter_inv(inv) if nsl.net == "BR"]
station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
average_station_location = np.average(list(station_enus.values()), axis=0)
locator = Locator(tt_function, average_station_location, raytracer=raytracer)

event_ids = [
    path.name.split(".json")[0]
    for path in base_dir.joinpath("event_locations").glob("*.json")
]
event_id = event_ids[0]
catalog = defaultdict()

source, pick_set = athena_client.get_preferred_origin_and_picks_for_event(event_id)

source_enu = source.enu(reproject)
with open(base_dir.joinpath("event_locations", f"{event_id}.json")) as f:
    event_json = json.load(f)
athena_source = EventOrigin(
    lat=event_json["latitude"],
    lon=event_json["longitude"],
    depth_m=event_json["depth_m"],
    time=event_json["time"],
)
stream = athena_client.read_event_waveforms(event_id).rotate("->ZNE", inventory=inv)
hodograms = measure_hodograms_for_event(
    stream, get_hodo_windows(pick_set, 0.005, 0.020)
)

min_pick, rescaled_picks, rescaled_stations = locator.rescale_inputs(
    pick_set, station_enus
)
rescaled_init_location = locator.get_starting_enut_ms(rescaled_picks, rescaled_stations)
init_enu = rescaled_init_location[:3] + locator.enu_0

n_jobs = 20
scale = 120
random_start_locations = [
    [
        *[xyz + scale * np.random.randn() for xyz in init_enu],
        min_pick + rescaled_init_location[3] / 10000 * scale * np.random.randn(),
    ]
    for ii in range(n_jobs)
]

locations = Parallel(n_jobs=n_jobs)(
    delayed(locator.locate_tt)(
        pick_set,
        station_enus,
        initial_enut_ms=random_start,
        locator_options={"maxfev": 8000, "maxiter": 8000},
    )
    for random_start in random_start_locations
)

fig, (ax_p, ax_d) = plt.subplots(1, 2, sharey=True)

for nsl in br_nsls:
    ax_p.plot(*station_enus[nsl][:2], "v", color="w", mec="0.2", ms=10)
    ax_d.plot(*station_enus[nsl][1:][::-1], ">", color="w", mec="0.2", ms=10)
for start, location in zip(random_start_locations, locations):
    ax_p.plot(*location[0][:2], ".", color="k", mec="0.2", zorder=5, alpha=0.3)
    ax_d.plot(*location[0][1:][::-1], ".", color="k", mec="0.2", zorder=5, alpha=0.3)
    ax_p.plot([location[0][0], start[0]], [location[0][1], start[1]], zorder=4)
    ax_d.plot([location[0][2], start[2]], [location[0][1], start[1]], zorder=4)
# for (nsl, ph),hg in {nslp: hg.unit_vector_enu for nslp,hg in hodograms.items() if hg.phase.is_P}.items():
#     ax_p.plot(
#         station_enus[nsl][0]+hodo_scale*hg[0]*np.array([1,-1]) ,
#         station_enus[nsl][1] + hodo_scale * hg[1] * np.array([1, -1]),
#         lw=2,
#         color='k'
#     )
#     ax_d.plot(
#         station_enus[nsl][2] + hodo_scale * hg[2] * np.array([1, -1]),
#         station_enus[nsl][1] + hodo_scale * hg[1] * np.array([1, -1]),
#         lw=2,
#         color='k'
#     )
for ax in (ax_p, ax_d):
    ax.set_aspect("equal")
    gray_background_with_grid(ax, grid_spacing=100)

ax_d.invert_xaxis()
plt.show()
