from collections import defaultdict
import json
from pathlib import Path
import sys
import time

import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory, Stream

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.basic import Window
from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D
from NocMeta.Meta import NOC_META

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

ATHENA_CODE = "FBK"
BASE_DIR = Path("braskem")
NSLP = tuple[NSL, Phase]


def raytracer(source_enu, receiver_enu, phase):
    return Raypath.isotropic_ray_trace(source_enu, receiver_enu, velocity_model, phase)


def tt_function(source_enu, receiver_enu, phase):
    return raytracer(source_enu, receiver_enu, phase).traveltime_sec


def get_hodo_windows(
    pick_set: PickSet, pre_seconds: float, post_seconds: float
) -> dict[NSLP, Window]:
    windows = defaultdict()
    for pick in pick_set.picks:
        windows[pick.nsl, pick.phase] = Window(
            [pick.time - pre_seconds, pick.time + post_seconds]
        )
    return windows


def measure_hodograms_for_event(stream: Stream, windows: dict[NSLP, Window]):
    hodograms = defaultdict()
    for (nsl, ph), window in windows.items():
        tr_3c = stream.select(network=nsl.net, station=nsl.sta, location=nsl.loc)
        hodograms[nsl, ph] = Hodogram.from_windowed_phase(tr_3c.copy(), ph, window)
    return hodograms


def main():
    with open(BASE_DIR.joinpath("velocity_model.json")) as f:
        velocity_model_json = json.load(f)

    layers = []
    for layer in velocity_model_json:
        layers.append(
            VMLayer1D(
                vp=layer["vp"],
                vs=layer["vs"],
                rho=layer["rho"],
                top=layer["top"] if "top" in layer else np.inf,
            )
        )

    velocity_model = VelocityModel1D(layers)
    athena_config = NOC_META[ATHENA_CODE]
    athena_client = AthenaClient(
        rf'http://{athena_config["athIP"]}', athena_config["athApi"]
    )
    inv = read_inventory(BASE_DIR.joinpath("FBK_Full.xml"))
    reproject = EPSGReprojector(32725)

    br_nsls = [nsl for nsl in NSL.iter_inv(inv) if nsl.net == "BR"]
    station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
    average_station_location = np.average(list(station_enus.values()), axis=0)
    locator = OptimizingRelocator(
        tt_function, np.array([*average_station_location]), raytracer=raytracer
    )

    event_ids = [
        path.name.split(".json")[0]
        for path in BASE_DIR.joinpath("event_locations").glob("*.json")
    ]

    for event_id in event_ids[:4]:
        with open(BASE_DIR.joinpath("event_locations", f"{event_id}.json")) as f:
            event_json = json.load(f)
        now = time.time()
        source = EventOrigin(
            lat=event_json["latitude"],
            lon=event_json["longitude"],
            depth_m=event_json["depth_m"],
            time=event_json["time"],
        )
        source_enu = source.enu(reproject)
        pick_set = PickSet.load(
            BASE_DIR.joinpath("synthetic_picks", f"{event_id}.picks")
        )
        br_pick_set = PickSet(picks=[p for p in pick_set.picks if p.nsl.net == "BR"])
        stream = read(BASE_DIR.joinpath("synthetic_waveforms", f"{event_id}.mseed"))
        windows = get_hodo_windows(pick_set, 0.005, 0.025)
        hodograms = measure_hodograms_for_event(stream, windows)

        # location_tt = locator.locate_tt(br_pick_set, station_enus)
        location_tt_hodo = locator.locate_combined(br_pick_set, hodograms, station_enus)
        min_pick, rescaled_picks, rescaled_stations = locator.rescale_inputs(
            br_pick_set, station_enus
        )
        rescaled_init_location = locator.get_starting_enut(
            rescaled_picks, rescaled_stations
        )
        init_enu = rescaled_init_location[:3] + locator.enu_0
        print(event_id, time.time() - now)
        fig, (ax_p, ax_d) = plt.subplots(1, 2, sharey=True)

        ax_p.plot(*source_enu[:2], "*", color="orangered", mec="0.1", zorder=4, ms=10)
        ax_d.plot(
            *source_enu[1:][::-1], "*", color="orangered", mec="0.1", zorder=4, ms=10
        )
        for nsl in br_nsls:
            ax_p.plot(*station_enus[nsl][:2], "v", color="w", mec="0.2", ms=10)
            ax_d.plot(*station_enus[nsl][1:][::-1], ">", color="w", mec="0.2", ms=10)
        ax_p.plot(*location_tt_hodo[0][:2], "o", color="gold", mec="0.2", zorder=5)
        ax_d.plot(
            *location_tt_hodo[0][1:][::-1], "o", color="gold", mec="0.2", zorder=5
        )
        ax_p.plot(*init_enu[:2], ".", color="0.1", mec="0.1", zorder=6)
        ax_d.plot(*init_enu[1:][::-1], ".", color="0.1", mec="0.1", zorder=6)
        for ax in (ax_p, ax_d):
            ax.set_aspect("equal")
            gray_background_with_grid(ax, grid_spacing=100)
        ax_d.invert_xaxis()
        fig.suptitle("location with tt and hodogram")
        fig.savefig(BASE_DIR.joinpath("location_image_tthodo", f"{event_id}.png"))

    plt.show()


if __name__ == "__main__":
    main()
