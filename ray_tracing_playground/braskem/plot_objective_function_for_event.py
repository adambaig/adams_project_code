import json
import sys
from collections import defaultdict
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from NocMeta.Meta import NOC_META
from nmxseis.interact.athena import AthenaClient
from nmxseis.model.basic import Window, NSLP
from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D
from obspy import read, read_inventory, Stream

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")
from generalPlots import gray_background_with_grid

ATHENA_CODE = "FBK"
base_dir = Path("braskem")
EPSG = 32725
STATION_XML = base_dir.joinpath("FBK_Full.xml")
hodo_scale = 100
inv = read_inventory(STATION_XML)
surface_nsls = [nsl for nsl in NSL.iter_inv(inv) if "T" in nsl.sta]

pick_error = {Phase.P: 0.010, Phase.S: 0.020}
pick_errors = {
    (nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
    (nsl, ph): 1000 * pick_error[ph]
    for nsl in NSL.iter_inv(inv)
    for ph in [Phase.P, Phase.S]
}
athena_config = NOC_META[ATHENA_CODE]
athena_client = AthenaClient(
    rf'http://{athena_config["athIP"]}', athena_config["athApi"]
)
hodogram_error_radians = {
    (nsl, ph): np.deg2rad(10) for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}

with open(base_dir.joinpath("velocity_model.json")) as f:
    velocity_model_json = json.load(f)

layers = []
for layer in velocity_model_json:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )

velocity_model = VelocityModel1D(layers)
reproject = EPSGReprojector(32725)


def raytracer(source_enu, receiver_enu, phase):
    return Raypath.isotropic_ray_trace(source_enu, receiver_enu, velocity_model, phase)


def tt_function(source_enu, receiver_enu, phase):
    return raytracer(source_enu, receiver_enu, phase).traveltime_sec


def get_hodo_windows(
    pick_set: PickSet, pre_seconds: float, post_seconds: float
) -> dict[NSLP, Window]:
    windows = defaultdict()
    for pick in pick_set.picks:
        windows[pick.nsl, pick.phase] = Window(
            [pick.time - pre_seconds, pick.time + post_seconds]
        )
    return windows


def measure_hodograms_for_event(stream: Stream, windows: dict[NSLP, Window]):
    hodograms = defaultdict()
    for (nsl, ph), window in windows.items():
        tr_3c = stream.select(network=nsl.net, station=nsl.sta, location=nsl.loc)
        hodograms[nsl, ph] = Hodogram.from_windowed_phase(tr_3c.copy(), ph, window)
    return hodograms


br_nsls = [nsl for nsl in NSL.iter_inv(inv) if nsl.net == "BR"]
station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
average_station_location = np.average(list(station_enus.values()), axis=0)
locator = OptimizingRelocator(tt_function, station_enus, epsg=EPSG, raytracer=raytracer)

event_id = "91340"
with open(base_dir.joinpath("event_locations", f"{event_id}.json")) as f:
    event_json = json.load(f)
athena_source = EventOrigin(
    lat=event_json["latitude"],
    lon=event_json["longitude"],
    depth_m=event_json["depth_m"],
    time=event_json["time"],
)
pick_set = PickSet.load(base_dir.joinpath("athena_picks", f"{event_id}.picks"))
if (stream_path := base_dir / "event_waveforms" / f"{event_id}.mseed").is_file():
    stream = read(stream_path)
else:
    stream = athena_client.read_event_waveforms(event_id).rotate("->ZNE", inventory=inv)
hodograms = measure_hodograms_for_event(
    stream, get_hodo_windows(pick_set, 0.005, 0.020)
)

reloc_result = locator.relocate(
    pick_set, pick_errors=pick_errors, locator_options={"maxfev": 2000, "maxiter": 2000}
)

reloc_with_hodogram_result = locator.relocate(
    pick_set,
    pick_errors=pick_errors,
    hodograms=hodograms,
    hodogram_errors_radians=hodogram_error_radians,
    locator_options={"maxfev": 2000, "maxiter": 2000},
)

min_pick, rescaled_picks, rescaled_stations = locator._rescale_inputs(
    pick_set,
    station_enus,
)

source_enu = reloc_result.origin.enu(EPSGReprojector(EPSG))
local_enut = locator._translate_to_local_enut(
    source_enu, reloc_result.origin.time, min_pick
)
minimum_chi = locator.tt_chi_sq(
    local_enut, rescaled_picks, rescaled_stations, pick_errors_ms
)
minimum_hodo_chi = locator.hodo_chi_sq(
    local_enut[:3], hodograms, rescaled_stations, hodogram_error_radians
)

fig, (ax_p, ax_d) = plt.subplots(1, 2, sharey=True, figsize=[12, 12])

ax_p.plot(*source_enu[:2], ".", color="0.1", mec="0.1", zorder=4, ms=10)
ax_d.plot(*source_enu[1:][::-1], ".", color="0.1", mec="0.1", zorder=4, ms=10)
for nsl in br_nsls:
    ax_p.plot(*station_enus[nsl][:2], "v", color="w", mec="0.2", ms=10)
    ax_d.plot(*station_enus[nsl][1:][::-1], ">", color="w", mec="0.2", ms=10)

for (nsl, ph), hg in {
    nslp: hg.unit_vector_enu for nslp, hg in hodograms.items() if hg.phase.is_P
}.items():
    ax_p.plot(
        station_enus[nsl][0] + hodo_scale * hg[0] * np.array([1, -1]),
        station_enus[nsl][1] + hodo_scale * hg[1] * np.array([1, -1]),
        lw=2,
        color="k",
    )
    ax_d.plot(
        station_enus[nsl][2] + hodo_scale * hg[2] * np.array([1, -1]),
        station_enus[nsl][1] + hodo_scale * hg[1] * np.array([1, -1]),
        lw=2,
        color="k",
    )
for ax in (ax_p, ax_d):
    ax.set_aspect("equal")
    gray_background_with_grid(ax, grid_spacing=100)

spacing = 10
xmin, xmax = ax_p.get_xlim()
ymin, ymax = ax_p.get_ylim()
zmin, zmax = ax_d.get_xlim()
xrange = np.arange(xmin, xmax, spacing)
yrange = np.arange(ymin, ymax, spacing)
zrange = np.arange(zmin, zmax, spacing)


def get_slices_of_tt_chi_sq():
    chi_tt_plan = np.zeros([len(xrange), len(yrange)])
    chi_tt_depth = np.zeros([len(zrange), len(yrange)])
    for ix, x in enumerate(xrange):
        for iy, y in enumerate(yrange):
            global_enu = np.array([x, y, source_enu[2]])
            local_enut = locator._translate_to_local_enut(
                global_enu, reloc_result.origin.time, min_pick
            )
            chi_tt_plan[ix, iy] = locator.hodo_tt_sq(
                local_enut,
                rescaled_picks,
                rescaled_stations,
                pick_errors_ms=pick_errors_ms,
            )
    for iy, y in enumerate(yrange):
        for iz, z in enumerate(zrange):
            global_enu = np.array([source_enu[0], y, z])
            local_enut = locator._translate_to_local_enut(
                global_enu, reloc_result.origin.time, min_pick
            )
            chi_tt_depth[iz, iy] = locator.tt_chi_sq(
                local_enut,
                rescaled_picks,
                rescaled_stations,
                pick_errors_ms=pick_errors_ms,
            )
    return chi_tt_plan, chi_tt_depth


def get_slices_of_combo_chi_sq():
    chi_tt_plan = np.zeros([len(xrange), len(yrange)])
    chi_tt_depth = np.zeros([len(zrange), len(yrange)])
    chi_hodo_plan = np.zeros([len(xrange), len(yrange)])
    chi_hodo_depth = np.zeros([len(zrange), len(yrange)])
    for ix, x in enumerate(xrange):
        for iy, y in enumerate(yrange):
            global_enu = np.array([x, y, source_enu[2]])
            local_enut = locator._translate_to_local_enut(
                global_enu, reloc_result.origin.time, min_pick
            )
            chi_hodo_plan[ix, iy] = locator.hodo_chi_sq(
                local_enut[:3],
                hodograms,
                rescaled_stations,
                hodogram_errors_radians=hodogram_error_radians,
            )
            chi_tt_plan[ix, iy] = locator.tt_chi_sq(
                local_enut,
                rescaled_picks,
                rescaled_stations,
                pick_errors_ms=pick_errors_ms,
            )
    for iy, y in enumerate(yrange):
        for iz, z in enumerate(zrange):
            global_enu = np.array([source_enu[0], y, z])
            local_enut = locator._translate_to_local_enut(
                global_enu, reloc_result.origin.time, min_pick
            )
            chi_hodo_depth[iz, iy] = locator.hodo_chi_sq(
                local_enut[:3],
                hodograms,
                rescaled_stations,
                hodogram_errors_radians=hodogram_error_radians,
            )
            chi_tt_depth[iz, iy] = locator.tt_chi_sq(
                local_enut,
                rescaled_picks,
                rescaled_stations,
                pick_errors_ms=pick_errors_ms,
            )
    return chi_tt_plan + 0.1 * chi_hodo_plan, chi_tt_depth + 0.1 * chi_hodo_depth


def get_slices_of_hodo_chi_sq():
    chi_hodo_plan = np.zeros([len(xrange), len(yrange)])
    chi_hodo_depth = np.zeros([len(zrange), len(yrange)])
    for ix, x in enumerate(xrange):
        for iy, y in enumerate(yrange):
            global_enu = np.array([x, y, source_enu[2]])
            local_enut = locator._translate_to_local_enut(
                global_enu, reloc_result.origin.time, min_pick
            )
            chi_hodo_plan[ix, iy] = locator.hodo_chi_sq(
                local_enut[:3],
                hodograms,
                rescaled_stations,
                hodogram_errors_radians=hodogram_error_radians,
            )
    for iy, y in enumerate(yrange):
        for iz, z in enumerate(zrange):
            global_enu = np.array([source_enu[0], y, z])
            local_enut = locator._translate_to_local_enut(
                global_enu, reloc_result.origin.time, min_pick
            )
            chi_hodo_depth[iz, iy] = locator.hodo_chi_sq(
                local_enut[:3],
                hodograms,
                rescaled_stations,
                hodogram_errors_radians=hodogram_error_radians,
            )
    return chi_hodo_plan, chi_hodo_depth


def plot_slices(chi_plan, chi_depth):
    min_c = min(np.amin(chi_depth), np.amin(chi_plan))
    log_max_c = max(
        np.amax(np.log10(chi_depth - min_c)), np.amax(np.log10(chi_plan - min_c))
    )
    colors = ax_d.pcolor(
        zrange, yrange, np.log10(chi_depth - min_c).T, vmin=0, vmax=log_max_c, alpha=0.6
    )
    ax_p.pcolor(
        xrange, yrange, np.log10(chi_plan - min_c).T, vmin=0, vmax=log_max_c, alpha=0.6
    )
    return colors


chi_combo_plan, chi_combo_depth = get_slices_of_combo_chi_sq()
colors = plot_slices(chi_combo_plan, chi_combo_depth)
fig.suptitle(r"combo $\chi^2$")
ax_d.invert_xaxis()
ax_p.set_ylabel(r"Northing $\longrightarrow$ ")
ax_d.set_xlabel(r"Depth $\longrightarrow$")
ax_p.set_xlabel(r"Easting $\longrightarrow$")
cbar = fig.colorbar(colors, ax=[ax_p, ax_d], orientation="horizontal")
cbar.set_label(r"log$_{10}$ $\chi^2$ above minimum")

fig.savefig(base_dir.joinpath(f"chi_sq_combo_field_{event_id}.png"))
plt.show()
