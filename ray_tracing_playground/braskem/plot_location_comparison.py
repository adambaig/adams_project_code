import json
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from nmxseis.model.nslc import NSL
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.core import RelocationResult
from obspy import read_inventory, UTCDateTime

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

basepath = Path("braskem")

catalog_json = basepath.joinpath("athena_and_tt_hodo_inverted_locations.json")

relocated_events = {}
athena_events = {}
with open(catalog_json) as f:
    catalog = json.load(f)

for event_id, event in catalog.items():
    relocated_events[event_id] = RelocationResult.from_json_dict(event)
    with open(basepath / "event_locations" / f"{event_id}.json") as f:
        event_json = json.load(f)
        athena_events[event_id] = EventOrigin(
            lat=event_json["latitude"],
            lon=event_json["longitude"],
            depth_m=event_json["depth_m"],
            time=UTCDateTime(event_json["time"]),
        )

reproject = EPSGReprojector(32725)

athena_enus = np.array([ev.enu(reproject) for ev in athena_events.values()])
inverted_enus = np.array(
    [reloc_ev.origin.enu(reproject) for reloc_ev in relocated_events.values()]
)

inv = read_inventory(Path("braskem").joinpath("FBK_Full.xml"))

br_nsls = [nsl for nsl in NSL.iter_inv(inv) if nsl.net == "BR"]
station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))

fig, ((ax_ap, ax_ad), (ax_ip, ax_id)) = plt.subplots(2, 2, sharey=True, figsize=[8, 8])

ax_ap.plot(
    athena_enus[:, 0],
    athena_enus[:, 1],
    ".",
    color="firebrick",
    mec="0.1",
    zorder=4,
    alpha=0.6,
)
ax_ad.plot(
    athena_enus[:, 2],
    athena_enus[:, 1],
    ".",
    color="firebrick",
    mec="0.1",
    zorder=4,
    alpha=0.6,
)
for nsl in br_nsls:
    ax_ap.plot(*station_enus[nsl][:2], "v", color="w", mec="0.2", ms=6)
    ax_ad.plot(*station_enus[nsl][1:][::-1], ">", color="w", mec="0.2", ms=6)
    ax_ip.plot(*station_enus[nsl][:2], "v", color="w", mec="0.2", ms=6)
    ax_id.plot(*station_enus[nsl][1:][::-1], ">", color="w", mec="0.2", ms=6)
ax_ip.plot(
    inverted_enus[:, 0],
    inverted_enus[:, 1],
    ".",
    color="royalblue",
    mec="0.2",
    zorder=3,
    alpha=0.6,
)
ax_id.plot(
    inverted_enus[:, 2],
    inverted_enus[:, 1],
    ".",
    color="royalblue",
    mec="0.2",
    zorder=3,
    alpha=0.6,
)

for ax in (ax_ap, ax_ad, ax_ip, ax_id):
    ax.set_aspect("equal")
    gray_background_with_grid(ax, grid_spacing=200)
ax_ad.invert_xaxis()
ax_id.invert_xaxis()

ax_ap.set_xlabel(r"Easting $\longrightarrow$")
ax_ad.set_xlabel(r"Depth $\longrightarrow$")
ax_ap.set_ylabel(r"Northing $\longrightarrow$")
ax_ip.set_xlabel(r"Easting $\longrightarrow$")
ax_id.set_xlabel(r"Depth $\longrightarrow$")
ax_ip.set_ylabel(r"Northing $\longrightarrow$")
fig.suptitle("NonLinLoc vs tt + hodo located")
#
# difference_enus= inverted_enus - athena_enus
# hrz_drift = np.hypot(difference_enus[:,0], difference_enus[:,1])
# vrt_drift= abs(difference_enus[2])
# distance_bins = np.arange(0,500, 10)
#
# fig,ax_hrz,ax_vrt = plt.subplots(1,2)
# ax_hrz.hist(hrz_drift, distance_bins )
# ax_vrt.hist(vrt_drift, distance_bins )
plt.show()

# fig.savefig(basepath / 'athena_vs_simplex_with_hodo.png')
