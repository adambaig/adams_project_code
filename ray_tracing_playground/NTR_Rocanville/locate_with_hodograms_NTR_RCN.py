import json
import sys
from pathlib import Path
import time

import matplotlib.pyplot as plt
import numpy as np
from NocMeta.Meta import NOC_META
from nmxseis.interact.athena import AthenaClient
from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.velocity_model.vm_1d import VM1DInterpolator
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator, \
	OptimizingRelocatorHint as Hint
from obspy import UTCDateTime, read

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

from read_inputs import get_station_enus, INV, get_velocity_model

hodo_scale = 100

base_dir = Path()
athena_code = "NTR_RCN"
velocity_model = get_velocity_model()
athena_config = NOC_META[athena_code]
athena_client = AthenaClient(
	rf'http://{athena_config["athIP"]}', athena_config["athApi"]
)

pick_error = {Phase.P: 0.010, Phase.S: 0.020}
pick_errors = {
	(nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(INV) for ph in [Phase.P, Phase.S]
}

ray_grids = base_dir.joinpath('tt_ray_grid.json')

interp = VM1DInterpolator.from_grids(ray_grids)


EPSG = NOC_META[athena_code]["epsg"]
reproject = EPSGReprojector(EPSG)

station_enus = get_station_enus()
nsls = list(station_enus.keys())
average_station_location = np.average(list(station_enus.values()), axis=0)
enu_0 = np.array([*average_station_location[:2], 0])
recentered_station_enus = {k: v - enu_0 for k, v in station_enus.items()}


locator = OptimizingRelocator(
	station_enus,
	EPSG,
	tt_provider=velocity_model,
	incoming_ray_enu_provider=interp
)

event_ids = [
	path.name.split(".json")[0]
	for path in base_dir.joinpath("event_locations").glob("*.json")
]

start = time.time()
catalog = {}
for event_id in event_ids:
	print(event_id)
	# source, pick_set = athena_client.get_preferred_origin_and_picks_for_event(event_id)
	# source_enu = source.enu(reproject)
	# with open(base_dir.joinpath("event_locations", f"{event_id}.json")) as f:
	# 	event_json = json.load(f)
	# athena_source = EventOrigin(
	# 	lat=event_json["latitude"],
	# 	lon=event_json["longitude"],
	# 	depth_m=event_json["depth_m"],
	# 	time=UTCDateTime(event_json["time"]),
	# )
	pick_file = list(base_dir.joinpath('athena_picks').glob(f'*{event_id}_*.picks'))[0]
	pick_set = PickSet.load(pick_file)
	if (
			test_path := base_dir.joinpath("event_waveforms", f"{event_id}.mseed")
	).is_file():
		stream = read(test_path)
	else:
		stream = athena_client.read_event_waveforms(event_id).rotate(
			"->ZNE", inventory=INV
		)
		stream.write(test_path, format="MSEED")
	hodograms = Hodogram.measure_around_windows(
		stream, Hodogram.get_windows_around_picks(pick_set, 0.05, 0.20)
	)
	reloc_result = locator.relocate(
		pick_set,
		Hint(
			hodograms=hodograms,
			pick_errors=pick_errors,
			locator_options={"maxfev": 2000, "maxiter": 2000},
		),
	)
	min_pick, rescaled_picks, rescaled_stations = locator._rescale_inputs(
		pick_set, station_enus
	)
	location = reloc_result.origin.enu(reproject)

	# fig, (ax_p, ax_d) = plt.subplots(1, 2, sharey=True)
	#
	# ax_p.plot(*source_enu[:2], "*", color="orangered", mec="0.1", zorder=4, ms=10)
	# ax_d.plot(*source_enu[1:][::-1], "*", color="orangered", mec="0.1", zorder=4, ms=10)
	# for nsl in nsls:
	# 	ax_p.plot(*station_enus[nsl][:2], "v", color="w", mec="0.2", ms=10)
	# ax_d.plot(*station_enus[nsl][1:][::-1], ">", color="w", mec="0.2", ms=10)
	# ax_p.plot(*location[:2], "o", color="gold", mec="0.2", zorder=5)
	# ax_d.plot(*location[1:][::-1], "o", color="gold", mec="0.2", zorder=5)
	# for (nsl, ph), hg in {
	# 	nslp: hg.unit_vector_enu
	# 	for nslp, hg in hodograms.items() if hg.phase.is_P
	# }.items():
	# 	ax_p.plot(
	# 		station_enus[nsl][0] + hodo_scale * hg[0] * np.array([1, -1]),
	# 		station_enus[nsl][1] + hodo_scale * hg[1] * np.array([1, -1]),
	# 		lw=2,
	# 		color="k",
	# 	)
	# 	ax_d.plot(
	# 		station_enus[nsl][2] + hodo_scale * hg[2] * np.array([1, -1]),
	# 		station_enus[nsl][1] + hodo_scale * hg[1] * np.array([1, -1]),
	# 		lw=2,
	# 		color="k",
	# 	)
	# for ax in (ax_p, ax_d):
	# 	ax.set_aspect("equal")
	# gray_background_with_grid(ax, grid_spacing=1000)
	#
	# ax_d.invert_xaxis()
	# fig.suptitle("picks from Athena: tt only")
	# fig.savefig(
	# 	base_dir.joinpath("location_image_tthodo", "athena_picks", f"{event_id}.png")
	# )
	# plt.close(fig)
	# pick_set.dump(
	# 	base_dir.joinpath(
	# 		"athena_picks",
	# 		f'{event_id.zfill(10)}_{UTCDateTime(source.time).strftime("%Y%m%d.%H%M%S.%f")}.picks',
	# 	)
	# )
	catalog[event_id] = {
		**reloc_result.to_json_serializable(),
	}
print(time.time() - start)

with open(
		base_dir.joinpath("tt_hodo_inverted_locations_vm_in.json"),
		"w") as f:
	json.dump(catalog, f)
