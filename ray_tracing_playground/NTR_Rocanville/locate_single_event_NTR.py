from collections import defaultdict
import json
from pathlib import Path
import sys

import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory, UTCDateTime

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.phase import Phase
from nmxseis.model.nslc import NSL
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D
from NocMeta.Meta import NOC_META

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

from braskem.locate_synthetics_tt_hodo import (
    get_hodo_windows,
    measure_hodograms_for_event,
)

hodo_scale = 100

base_dir = Path("NTR_Rocanville")
athena_code = "NTR_RCN"
STATION_XML = base_dir.joinpath("NTR_RCN_Full.xml")
inv = read_inventory(STATION_XML)
surface_nsls = [nsl for nsl in NSL.iter_inv(inv) if "T" in nsl.sta]

athena_config = NOC_META[athena_code]
athena_client = AthenaClient(
    rf'http://{athena_config["athIP"]}', athena_config["athApi"]
)

pick_error = {Phase.P: 0.010, Phase.S: 0.020}
pick_errors = {
    (nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}
hodogram_error_radians = {
    (nsl, ph): np.deg2rad(10) for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}


with open(base_dir.joinpath("velocity_model.json")) as f:
    velocity_model_json = json.load(f)

layers = []
for layer in velocity_model_json:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )

velocity_model = VelocityModel1D(layers)
EPSG = NOC_META[athena_code]["epsg"]
reproject = EPSGReprojector(EPSG)


def raytracer(source_enu, receiver_enu, phase):
    return Raypath.isotropic_ray_trace(source_enu, receiver_enu, velocity_model, phase)


def tt_function(source_enu, receiver_enu, phase):
    return raytracer(source_enu, receiver_enu, phase).traveltime_sec


station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
nsls = list(station_enus.keys())
average_station_location = np.average(list(station_enus.values()), axis=0)
locator = OptimizingRelocator(tt_function, station_enus, epsg=EPSG, raytracer=raytracer)
event_ids = [
    path.name.split(".json")[0]
    for path in base_dir.joinpath("event_locations").glob("*.json")
]

catalog = defaultdict()
for ii in range(100):
    event_id = "13870"
    source, pick_set = athena_client.get_preferred_origin_and_picks_for_event(event_id)

    source_enu = source.enu(reproject)
    with open(base_dir.joinpath("event_locations", f"{event_id}.json")) as f:
        event_json = json.load(f)
    athena_source = EventOrigin(
        lat=event_json["latitude"],
        lon=event_json["longitude"],
        depth_m=event_json["depth_m"],
        time=event_json["time"],
    )
    stream = athena_client.read_event_waveforms(event_id).rotate("->ZNE", inventory=inv)
    hodograms = measure_hodograms_for_event(
        stream, get_hodo_windows(pick_set, 0.005, 0.020)
    )
    reloc_result = locator.relocate(
        pick_set,
        pick_errors=pick_errors,
        hodograms=hodograms,
        hodogram_errors_radians=hodogram_error_radians,
        locator_options={"maxfev": 2000, "maxiter": 2000},
    )
    min_pick, rescaled_picks, rescaled_stations = locator._rescale_inputs(
        pick_set, station_enus
    )
    location = reloc_result.origin.enu(reproject)
