from pathlib import Path

import numpy as np
from nmxseis.numerics.geometry import GridXYZ
from nmxseis.numerics.velocity_model.vm_1d import VM1DTTInterpolator, VM1DIncomingEnuInterpolator, \
	VM1DRayPropertyInterpolator

from read_inputs import get_velocity_model, get_station_enus

if __name__ == '__main__':
	velocity_model = get_velocity_model()
	station_enus = get_station_enus()
	station_enus_array = np.array([np.array(v) for v in station_enus.values()])
	elevations = np.hstack([-3000,np.arange(-1000, 400, 50),550, 1000])
	min_east = min(station_enus_array[:, 0])
	max_east = max(station_enus_array[:, 0])
	min_north = min(station_enus_array[:, 1])
	max_north = max(station_enus_array[:, 1])

	eastings = np.hstack(np.arange(-10000 + min_east, 10000 + max_east, 200))
	northings = np.hstack(np.arange(-10000 + min_north, 10000 + max_north, 200))

	grid = GridXYZ(xs=eastings, ys=northings, zs=elevations)

	tt_interp, hodo_interp = VM1DRayPropertyInterpolator.from_vm(
		(VM1DTTInterpolator, VM1DIncomingEnuInterpolator),
		velocity_model=velocity_model,
		station_enus=station_enus,
		grid=grid,
	)
	# tt_interp.dump_to_npz(Path().joinpath('interpolators', 'interp_tt_ntr_rcn.npz'))
	# hodo_interp.dump_to_npz(Path().joinpath('interpolators', 'interp_hodo_ntr_rcn.npz'))
	tt_interp.write_to_npz_dir(Path().joinpath('interpolators'))
	hodo_interp.write_to_npz_dir(Path().joinpath('interpolators'))
