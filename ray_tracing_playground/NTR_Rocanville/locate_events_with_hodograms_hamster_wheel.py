import json
from pathlib import Path
import time

from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.velocity_model.vm_1d import VM1DInterpolator
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator, \
	OptimizingRelocatorHint as Hint
from obspy import read

from read_inputs import get_station_enus, INV, get_velocity_model, SASK_EPSG

base_dir = Path()
velocity_model = get_velocity_model()

pick_error = {Phase.P: 0.010, Phase.S: 0.020}
pick_errors = {
	(nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(INV) for ph in [Phase.P, Phase.S]
}

ray_grids = base_dir.joinpath('tt_ray_grid.json')

interp = VM1DInterpolator.from_grids(ray_grids)

reproject = EPSGReprojector(SASK_EPSG)

station_enus = get_station_enus()
nsls = list(station_enus.keys())

locator = OptimizingRelocator(
	station_enus,
	SASK_EPSG,
	tt_provider=velocity_model,
	incoming_ray_enu_provider=interp
)

event_ids = [
	path.name.split(".json")[0]
	for path in base_dir.joinpath("event_locations").glob("*.json")
]

start = time.time()
catalog = {}
for event_id in event_ids:
	print(event_id)
	pick_file = list(base_dir.joinpath('athena_picks').glob(f'*{event_id}_*.picks'))[0]
	pick_set = PickSet.load(pick_file)
	stream_path = base_dir.joinpath("event_waveforms", f"{event_id}.mseed")
	stream = read(stream_path)
	hodograms = Hodogram.measure_around_windows(
		stream, Hodogram.get_windows_around_picks(pick_set, 0.05, 0.20)
	)
	reloc_result = locator.relocate(
		pick_set,
		Hint(
			hodograms=hodograms,
			pick_errors=pick_errors,
			locator_options={"maxfev": 2000, "maxiter": 2000},
		),
	)
	catalog[event_id] = reloc_result.to_json_serializable(),

print(time.time() - start)

with open(
		base_dir.joinpath("tt_hodo_inverted_locations_vm_in.json"),
		"w") as f:
	json.dump(catalog, f)
