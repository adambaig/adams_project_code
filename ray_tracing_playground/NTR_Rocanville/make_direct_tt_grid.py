from pathlib import Path

import numpy as np
from nmxseis.numerics.velocity_model.vm_1d import VM1DInterpolator

from read_inputs import get_velocity_model, get_station_enus

velocity_model = get_velocity_model()
station_enus = get_station_enus()


nr = int(np.hypot(ne, nn))

elevation_min,elevation_max = -2050, 550

radial_grid = np.arange(nr) * 50
elevations = np.hstack([-5000, np.linspace(elevation_min, elevation_max, nz), 2000])
out_json = Path().joinpath('tt_ray_grid.json')

VM1DInterpolator.make_json_grids(
	velocity_model,
	station_enus,
	radial_grid,
	elevations,
	out_path=out_json,
	sin_incoming_too=True
)

