import json
import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.core import RelocationResult

from read_inputs import get_station_enus, SASK_EPSG

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

BASEDIR = Path()
reproject = EPSGReprojector(SASK_EPSG)
station_enus = get_station_enus()
old_json = BASEDIR.joinpath(
	"athena_and_tt_hodo_inverted_locations_with_tt_lookup.json")
new_json = BASEDIR.joinpath(
	"athena_and_tt_hodo_inverted_locations_with_tt_and_hodo_lookup.json")

with open(old_json) as f:
	old_catalog = {
		k: RelocationResult.from_json_dict(v) for k, v in json.load(f).items()
	}
with open(new_json) as f:
	new_catalog = {
		k: RelocationResult.from_json_dict(v) for k, v in json.load(f).items()
	}

old_enus = np.array([reloc.origin.enu(reproject) for reloc in old_catalog.values()])
new_enus = np.array([reloc.origin.enu(reproject) for reloc in new_catalog.values()])

xmin_p = np.percentile(old_enus[:, 0], 1)
xmax_p = np.percentile(old_enus[:, 0], 99)
ymin_p = np.percentile(old_enus[:, 1], 1)
ymax_p = np.percentile(old_enus[:, 1], 99)
x_range = xmax_p - xmin_p
y_range = ymax_p - ymin_p

xmin = xmin_p - 0.2 * x_range
xmax = xmax_p + 0.2 * x_range
ymin = ymin_p - 0.2 * y_range
ymax = ymax_p + 0.2 * y_range

old_kwargs = {
	"color": "firebrick",
	"mec": "0.1",
	"zorder": 4,
	"alpha": 0.6}

new_kwargs = {
	"color": "steelblue",
	"mec": "0.2",
	"zorder": 3,
	"alpha": 0.6,
}

station_kwargs = {
	"color": "w",
	"mec": "0.1",
	"zorder": 2
}

fig, ((ax_ap, ax_ip), (ax_ad, ax_id)) = plt.subplots(
	2, 2, sharex=True, figsize=[12, 10]
)

ax_ap.plot(old_enus[:, 0], old_enus[:, 1], '.', **old_kwargs)
ax_ad.plot(old_enus[:, 0], old_enus[:, 2], '.', **old_kwargs)
ax_ip.plot(new_enus[:, 0], new_enus[:, 1], '.', **new_kwargs)
ax_id.plot(new_enus[:, 0], new_enus[:, 2], '.', **new_kwargs)
for station in station_enus.values():
	for ax in ax_ap, ax_ip:
		ax.plot(station[0], station[1], 'v', **station_kwargs)
	for ax in ax_ad, ax_id:
		ax.plot(station[0], station[2], 'v', **station_kwargs)

for ax in (ax_ap, ax_ad, ax_ip, ax_id):
	gray_background_with_grid(ax, grid_spacing=2000)

ax_ad.invert_xaxis()
ax_id.invert_xaxis()
ax_ad.set_ylabel(r"$\longleftarrow$ Depth")
ax_ad.set_xlabel(r"Easting $\longrightarrow$")
ax_ap.set_ylabel(r"Northing $\longrightarrow$")
ax_id.set_xlabel(r"Easting $\longrightarrow$")
fig.suptitle("on-the-fly vs lookup and cubic interp")
fig.savefig(BASEDIR.joinpath("onthefly_vs_tt_cubic_interp_for_Jun_2022.png"))

fig_e, (ax_old, ax_new) = plt.subplots(1, 2, figsize=[8, 4])

depth_error_old = [reloc.get_depth_error_m() for reloc in old_catalog.values()]
depth_error_new = [reloc.get_depth_error_m() for reloc in new_catalog.values()]

for ax, enus, errors in zip([ax_old, ax_new], [old_enus, new_enus], [depth_error_old, depth_error_new]):
	ax.plot(errors, enus[:, 2], '.')
	ax.set_xlim(0, 50)
	ax.set_ylim(-1250, 550)
	ax.set_xlabel('depth error (m)')

ax_old.set_ylabel('depth (m)')
ax_new.set_title('interpolate hodo and tt')
ax_old.set_title('on-the-fly ray tracing for hodo')
fig_e.savefig(BASEDIR.joinpath("onthefly_vs_tt_and_hodo_interp_errors.png"))

fig_h, (ax_h_old, ax_h_new) = plt.subplots(1, 2, figsize=[8, 4])
bins = np.arange(0, 50, 2)

for ax, errors in zip([ax_h_old, ax_h_new], [depth_error_old, depth_error_new]):
	ax.hist(errors, bins)
	ax.set_xlabel('depth errors (m)')

ax_h_new.set_title('interpolate hodo and tt')
ax_h_old.set_title('on-the-fly ray tracing for hodo')
fig_h.savefig('depth_error_hist.png')

# plt.show()
