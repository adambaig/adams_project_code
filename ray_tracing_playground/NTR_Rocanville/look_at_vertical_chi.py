import json
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np


from nmxseis.interact.athena import AthenaClient
from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase

from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from nmxseis.numerics.relocation.core import RelocationResult
from NocMeta.Meta import NOC_META

from read_inputs import get_velocity_model, get_station_enus, INV, SASK_EPSG, REPROJECTOR, get_radial_interpolator

ATHENA_CODE = 'NTR_RCN'
BASEDIR = Path()


new_json = BASEDIR.joinpath(
	"athena_and_tt_hodo_inverted_locations_with_tt_lookup_and_interp_quintic.json")

with open(new_json) as f:
	relocated_events = {
		k: RelocationResult.from_json_dict(v) for k, v in json.load(f).items()
	}


events_to_investigate = [k for k,v in relocated_events.items() if v.get_depth_error_m()<1]
athena_config = NOC_META[ATHENA_CODE]
athena_client = AthenaClient(
    rf'http://{athena_config["athIP"]}', athena_config["athApi"]
)

pick_error = {Phase.P: 0.010, Phase.S: 0.020}
pick_errors = {
    (nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(INV) for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
    (nsl, ph): 1000*pick_error[ph] for nsl in NSL.iter_inv(INV) for ph in [Phase.P, Phase.S]
}
velocity_model = get_velocity_model()
station_enus = get_station_enus()

interpolators = {}
for phase in [Phase.P, Phase.S]:
	for nsl in station_enus.keys():
		elev_int = int(1000 * station_enus[nsl][2])
		interpolators[(elev_int, phase)] = get_radial_interpolator(f'{nsl}', phase)


def tt_function_2d(src_enu, rec_enu, interpolator):
	r_dist = np.linalg.norm([src_enu[:2] - rec_enu[:2]])
	elev = src_enu[2]
	return float(interpolator([r_dist, elev]))


relocator = OptimizingRelocator(
	station_enus,
	SASK_EPSG,
	velocity_model=velocity_model,
	tt_function=lambda src, rec, phase: tt_function_2d(
		src, rec, interpolators[(int(rec[2] * 1000), phase)]
	)
)

for event_id in events_to_investigate:
    enu = relocated_events[event_id].origin.enu(REPROJECTOR)
    athena_source, pick_set = athena_client.get_preferred_origin_and_picks_for_event(event_id)
    stream = (
        athena_client.read_event_waveforms(event_id)
        .merge()
        .rotate("->ZNE", inventory=INV)
    )
    hodograms = Hodogram.measure_around_windows(
        stream, Hodogram.get_windows_around_picks(pick_set, 0.05, 0.20)
    )
    min_pick, rescaled_picks_ms, recentered_sta_enus = relocator._rescale_inputs(pick_set, station_enus)
    location_enut_ms = [*enu-relocator.enu_0, 1000*(relocated_events[event_id].origin.time - min_pick)]

    combo_chi_sq = lambda x : relocator.combined_chi_sq(x, rescaled_picks_ms, hodograms, recentered_sta_enus, pick_errors_ms)
    tt_chi_sq = lambda x : relocator.tt_chi_sq(x, rescaled_picks_ms, recentered_sta_enus, pick_errors_ms)
    hodo_chi_sq = lambda x : relocator.hodo_chi_sq(x, hodograms, recentered_sta_enus)

    dx = np.linspace(-100,100,201)
    fig,ax = plt.subplots(1,3, sharey=True, figsize=[12,6])
    ax[0].plot([combo_chi_sq(location_enut_ms+np.array([0,0,x,0])) for x in dx],dx, 'purple' )
    ax[1].plot([tt_chi_sq(location_enut_ms+np.array([0,0,x,0])) for x in dx],dx, 'royalblue')
    ax[2].plot([hodo_chi_sq(location_enut_ms[:3]+np.array([0,0,x])) for x in dx],dx, 'firebrick')
    ax[0].set_xlabel('combined $\chi^2$')
    ax[1].set_xlabel('tt only $\chi^2$')
    ax[2].set_xlabel('hodo only $\chi^2$')
    fig.savefig(f'{event_id}.png')
plt.show()











