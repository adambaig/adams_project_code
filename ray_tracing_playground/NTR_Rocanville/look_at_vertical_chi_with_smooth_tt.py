import json
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase

from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from nmxseis.numerics.relocation.core import RelocationResult
from NocMeta.Meta import NOC_META
from scipy.signal import gaussian
from read_inputs import get_velocity_model, get_station_enus, INV, SASK_EPSG, REPROJECTOR

ATHENA_CODE = 'NTR_RCN'

relocated_events = {}
# for event_json in Path('Oct_2022_events').glob('*.json'):
#     with open(event_json) as f:
#         relocated_events[event_json.stem] = RelocationResult.from_json_dict(json.load(f))

with open(new_json) as f:
	relocated_events = {
		k: RelocationResult.from_json_dict(v) for k, v in json.load(f).items()
	}


event_to_investigate = [k for k,v in relocated_events.items() if v.get_depth_error_m()<0.01][1]
athena_config = NOC_META[ATHENA_CODE]
athena_client = AthenaClient(rf'http://{athena_config["athIP"]}', athena_config["athApi"])

pick_error = {Phase.P: 0.010, Phase.S: 0.020}
pick_errors = {
    (nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(INV) for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
    (nsl, ph): 1000*pick_error[ph] for nsl in NSL.iter_inv(INV) for ph in [Phase.P, Phase.S]
}
velocity_model = get_velocity_model()
station_enus = get_station_enus()
relocator = OptimizingRelocator(station_enus, SASK_EPSG, velocity_model)

enu = relocated_events[event_to_investigate].origin.enu(REPROJECTOR)
athena_source, pick_set = athena_client.get_preferred_origin_and_picks_for_event(
    event_to_investigate
)
stream = (
        athena_client.read_event_waveforms(event_to_investigate)
        .merge()
        .rotate("->ZNE", inventory=INV)
)
hodograms = Hodogram.measure_around_windows(
    stream, Hodogram.get_windows_around_picks(pick_set, 0.05, 0.20)
)
min_pick, rescaled_picks_ms, recentered_sta_enus = relocator._rescale_inputs(pick_set, station_enus)
location_enut_ms = [*enu-relocator.enu_0, 1000*(min_pick - relocated_events[event_to_investigate].origin.time)]

elevations = np.linspace(550,-1000, 2000)
fig,ax = plt.subplots()
for nsl,st_enu in station_enus.items():
    pass

nsl = NSL.parse('PC.RSU01.')
st_enu = station_enus[nsl]
gauss_bell = gaussian(100, 20)
gauss_bell /= sum(gauss_bell)

tt_column = np.array([velocity_model.get_tt_sec(np.array([*enu[:2], elev]),st_enu,Phase.P) for elev in elevations])

smooth_tt = np.convolve(tt_column, gauss_bell, mode='same')

ax.plot(tt_column, elevations)
ax.plot(smooth_tt, elevations, alpha=0.5)
ax.set_ylabel('elevation (m)')
ax.set_xlabel('traveltime (s)')
ax.legend(['ray traced traveltime','BS smoothing'])
ax.set_ylim([-800,-400])
plt.show()















