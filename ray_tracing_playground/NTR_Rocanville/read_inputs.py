from __future__ import annotations

import json
from pathlib import Path

from nmxseis.model.nslc import NSL
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.velocity_model.vm_1d import VelocityModel1D, VMLayer1D
from numpy import  inf
from obspy import read_inventory

BASE_DIR = Path().absolute()
SASK_EPSG = 32613
REPROJECTOR = EPSGReprojector(SASK_EPSG)
INV = read_inventory(BASE_DIR.joinpath("station_xml","NTR_RCN_Full.xml"))


def get_station_enus():
	return REPROJECTOR.get_station_enus(INV, NSL.iter_inv(INV))

def get_fwb_station_enus():
	fwb_inv = read_inventory(BASE_DIR.joinpath('station_xml', 'fwb_inventory.xml'))
	return REPROJECTOR.get_station_enus(fwb_inv, NSL.iter_inv(fwb_inv))


def get_velocity_model():
	with open(BASE_DIR.joinpath("velocity_model.json")) as f:
		velocity_model_json = json.load(f)
	layers = []
	for layer in velocity_model_json:
		layers.append(
			VMLayer1D(
				vp=layer["vp"],
				vs=layer["vs"],
				rho=layer["rho"],
				top=layer["top"] if "top" in layer else inf,
			)
		)
	return VelocityModel1D(layers)

