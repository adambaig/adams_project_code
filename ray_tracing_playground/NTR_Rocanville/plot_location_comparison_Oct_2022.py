import json
from pathlib import Path
import sys

import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory, UTCDateTime

from nmxseis.model.seismic_event import EventOrigin
from nmxseis.model.nslc import NSL
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.core import RelocationResult

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

basepath = Path("NTR_Rocanville")
catalog_jsons = basepath.joinpath("Oct_2022_events").glob("*.json")

relocated_events = {}
athena_events = {}
for catalog_json in catalog_jsons:
    event_id = catalog_json.stem
    with open(catalog_json) as f:
        event_json = json.load(f)
        athena_events[event_id] = EventOrigin(
            lat=event_json["athena_lat"],
            lon=event_json["athena_lon"],
            depth_m=event_json["athena_depth_m"],
            time=UTCDateTime(event_json["origin"]["UTC_datetime"]),
        )
        relocated_events[event_id] = RelocationResult.from_json_dict(event_json)


reproject = EPSGReprojector(32613)

athena_enus = np.array([ev.enu(reproject) for ev in athena_events.values()])
inverted_enus = np.array(
    [reloc_ev.origin.enu(reproject) for reloc_ev in relocated_events.values()]
)

xmin_p = np.percentile(athena_enus[:, 0], 1)
xmax_p = np.percentile(athena_enus[:, 0], 99)
ymin_p = np.percentile(athena_enus[:, 1], 1)
ymax_p = np.percentile(athena_enus[:, 1], 99)

x_range = xmax_p - xmin_p
y_range = ymax_p - ymin_p

xmin = xmin_p - 0.2 * x_range
xmax = xmax_p + 0.2 * x_range
ymin = ymin_p - 0.2 * y_range
ymax = ymax_p + 0.2 * y_range

inv = read_inventory(basepath.joinpath("NTR_RCN_Full.xml"))

nsls = [nsl for nsl in NSL.iter_inv(inv) if nsl.loc != "D0"]
station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))

fig, ((ax_ap, ax_ip), (ax_ad, ax_id)) = plt.subplots(
    2, 2, sharex=True, figsize=[12, 10]
)

ax_ap.plot(
    athena_enus[:, 0],
    athena_enus[:, 1],
    ".",
    color="firebrick",
    mec="0.1",
    zorder=4,
    alpha=0.6,
)
ax_ad.plot(
    athena_enus[:, 0],
    athena_enus[:, 2],
    ".",
    color="firebrick",
    mec="0.1",
    zorder=4,
    alpha=0.6,
)
for nsl in nsls:
    ax_ap.plot(
        station_enus[nsl][0], station_enus[nsl][1], "v", color="w", mec="0.2", ms=6
    )
    ax_ad.plot(
        station_enus[nsl][0], station_enus[nsl][2], "v", color="w", mec="0.2", ms=6
    )
    ax_ip.plot(
        station_enus[nsl][0], station_enus[nsl][1], "v", color="w", mec="0.2", ms=6
    )
    ax_id.plot(
        station_enus[nsl][0], station_enus[nsl][2], "v", color="w", mec="0.2", ms=6
    )
ax_ip.plot(
    inverted_enus[:, 0],
    inverted_enus[:, 1],
    ".",
    color="steelblue",
    mec="0.2",
    zorder=3,
    alpha=0.6,
)
ax_id.plot(
    inverted_enus[:, 0],
    inverted_enus[:, 2],
    ".",
    color="steelblue",
    mec="0.2",
    zorder=3,
    alpha=0.6,
)

#
# xmin = 744000
# xmax = 746000
# ymin = 5598000
# ymax = 5600000

for ax in (ax_ap, ax_ad, ax_ip, ax_id):
    gray_background_with_grid(ax, grid_spacing=2000)

for ax in (ax_ad, ax_id):
    ax.set_aspect(5)
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(-2500, 1000)
    # ax.set_ylim(-1250, 750)
for ax in (ax_ap, ax_ip):
    ax.set_aspect("equal")
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)

ax_ad.invert_xaxis()
ax_id.invert_xaxis()
ax_ad.set_ylabel(r"$\longleftarrow$ Depth")
ax_ad.set_xlabel(r"Easting $\longrightarrow$")
ax_ap.set_ylabel(r"Northing $\longrightarrow$")
ax_id.set_xlabel(r"Easting $\longrightarrow$")
fig.suptitle("NonLinLoc vs tt + hodo located")
plt.show()

fig.savefig(basepath.joinpath("athena_vs_simplex_for_Oct_2022.png"))

depth_error_old = [reloc.get_depth_error_m() for reloc in old_catalog.values()]
depth_error_new = [reloc.get_depth_error_m() for reloc in new_catalog.values()]

fig, (ax_o, ax_n) = plt.subplots(1,2,figsize=[8,4])

ax_o.plot(depth_error_old, old_enus[:,2]%50,'.')
ax_n.plot(depth_error_new, new_enus[:,2]%50,'.')