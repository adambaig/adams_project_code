import json
from pathlib import Path
import sys

import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory, UTCDateTime

from nmxseis.model.seismic_event import EventOrigin
from nmxseis.model.nslc import NSL
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.core import RelocationResult

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

basepath = Path("NTR_Rocanville")
catalog_json = basepath.joinpath("athena_and_tt_hodo_inverted_locations.json")

relocated_events = {}
athena_events = {}
with open(catalog_json) as f:
    catalog = json.load(f)

for event_id, event in catalog.items():
    relocated_events[event_id] = RelocationResult.from_json_dict(event)
    with open(basepath / "event_locations" / f"{event_id}.json") as f:
        event_json = json.load(f)
        athena_events[event_id] = EventOrigin(
            lat=event_json["latitude"],
            lon=event_json["longitude"],
            depth_m=event_json["depth_m"],
            time=UTCDateTime(event_json["time"]),
        )

reproject = EPSGReprojector(32613)

athena_enus = np.array([ev.enu(reproject) for ev in athena_events.values()])
inverted_enus = np.array(
    [reloc_ev.origin.enu(reproject) for reloc_ev in relocated_events.values()]
)

xmin_p = np.percentile(athena_enus[:, 1], 1)
xmax_p = np.percentile(athena_enus[:, 1], 99)
ymin_p = np.percentile(-athena_enus[:, 0], 1)
ymax_p = np.percentile(-athena_enus[:, 0], 99)

x_range = xmax_p - xmin_p
y_range = ymax_p - ymin_p

xmin = xmin_p - 0.2 * x_range
xmax = xmax_p + 0.2 * x_range
ymin = ymin_p - 0.2 * y_range
ymax = ymax_p + 0.2 * y_range

inv = read_inventory(basepath.joinpath("NTR_RCN_Full.xml"))

nsls = [nsl for nsl in NSL.iter_inv(inv) if nsl.loc != "D0"]
station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))

fig, ((ax_ap, ax_ip), (ax_ad, ax_id)) = plt.subplots(2, 2, sharex=True, figsize=[8, 5])

ax_ap.plot(
    athena_enus[:, 1],
    -athena_enus[:, 0],
    ".",
    color="firebrick",
    mec="0.1",
    zorder=4,
    alpha=0.6,
)
ax_ad.plot(
    athena_enus[:, 1],
    athena_enus[:, 2],
    ".",
    color="firebrick",
    mec="0.1",
    zorder=4,
    alpha=0.6,
)
for nsl in nsls:
    ax_ap.plot(
        station_enus[nsl][1], -station_enus[nsl][0], "v", color="w", mec="0.2", ms=6
    )
    ax_ad.plot(
        station_enus[nsl][1], station_enus[nsl][2], "v", color="w", mec="0.2", ms=6
    )
    ax_ip.plot(
        station_enus[nsl][1], -station_enus[nsl][0], "v", color="w", mec="0.2", ms=6
    )
    ax_id.plot(
        station_enus[nsl][1], station_enus[nsl][2], "v", color="w", mec="0.2", ms=6
    )
ax_ip.plot(
    inverted_enus[:, 1],
    -inverted_enus[:, 0],
    ".",
    color="steelblue",
    mec="0.2",
    zorder=3,
    alpha=0.6,
)
ax_id.plot(
    inverted_enus[:, 1],
    inverted_enus[:, 2],
    ".",
    color="steelblue",
    mec="0.2",
    zorder=3,
    alpha=0.6,
)

for ax in (ax_ap, ax_ad, ax_ip, ax_id):
    gray_background_with_grid(ax, grid_spacing=500)

for ax in (ax_ad, ax_id):
    ax.set_aspect("equal")
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(-2500, 1000)

for ax in (ax_ap, ax_ip):
    ax.set_aspect("equal")
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)

ax_ad.invert_xaxis()
ax_id.invert_xaxis()
ax_ad.set_ylabel(r"$\longleftarrow$ Depth")
ax_ad.set_xlabel(r"Northing $\longrightarrow$")
ax_ap.set_ylabel(r"$\longleftarrow$ Easting ")
ax_id.set_xlabel(r"Northing $\longrightarrow$")
fig.suptitle("NonLinLoc vs tt + hodo located")
plt.show()

fig.savefig(basepath.joinpath("athena_vs_simplex_for_group.png"))
