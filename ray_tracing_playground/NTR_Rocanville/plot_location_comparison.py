import json
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory

from nmxseis.model.nslc import NSL
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.core import RelocationResult

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid


basepath = Path("NTR_Rocanville")
catalog_json = basepath.joinpath("athena_and_tt_hodo_inverted_locations.json")

with open(catalog_json) as f:
    catalog = json.load(f)


athena_enus = np.array([event["athena_source_enu"] for event in catalog.values()])
inverted_enus = np.array([event["inverted_source_enu"] for event in catalog.values()])

inv = read_inventory(basepath.joinpath("NTR_RCN_Full.xml"))
reproject = EPSGReprojector(32613)
nsls = [nsl for nsl in NSL.iter_inv(inv) if nsl.loc != "D0"]
station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))

fig, ((ax_ap, ax_ad), (ax_ip, ax_id)) = plt.subplots(2, 2, sharey=True, figsize=[8, 8])

ax_ap.plot(
    athena_enus[:, 0],
    athena_enus[:, 1],
    ".",
    color="firebrick",
    mec="0.1",
    zorder=4,
    alpha=0.6,
)
ax_ad.plot(
    athena_enus[:, 2],
    athena_enus[:, 1],
    ".",
    color="firebrick",
    mec="0.1",
    zorder=4,
    alpha=0.6,
)
for nsl in nsls:
    ax_ap.plot(*station_enus[nsl][:2], "v", color="w", mec="0.2", ms=6)
    ax_ad.plot(*station_enus[nsl][1:][::-1], ">", color="w", mec="0.2", ms=6)
    ax_ip.plot(*station_enus[nsl][:2], "v", color="w", mec="0.2", ms=6)
    ax_id.plot(*station_enus[nsl][1:][::-1], ">", color="w", mec="0.2", ms=6)
ax_ip.plot(
    inverted_enus[:, 0],
    inverted_enus[:, 1],
    ".",
    color="royalblue",
    mec="0.2",
    zorder=3,
    alpha=0.6,
)
ax_id.plot(
    inverted_enus[:, 2],
    inverted_enus[:, 1],
    ".",
    color="royalblue",
    mec="0.2",
    zorder=3,
    alpha=0.6,
)

for ax in (ax_ap, ax_ad, ax_ip, ax_id):
    gray_background_with_grid(ax, grid_spacing=2000)

for ax in (ax_ad, ax_id):
    ax.set_aspect(0.2)

for ax in (ax_ap, ax_ip):
    ax.set_aspect("equal")


ax_ad.invert_xaxis()
ax_id.invert_xaxis()

ax_ap.set_xlabel(r"Easting $\longrightarrow$")
ax_ad.set_xlabel(r"Depth $\longrightarrow$")
ax_ap.set_ylabel(r"Northing $\longrightarrow$")
ax_ip.set_xlabel(r"Easting $\longrightarrow$")
ax_id.set_xlabel(r"Depth $\longrightarrow$")
ax_ip.set_ylabel(r"Northing $\longrightarrow$")
fig.suptitle("NonLinLoc vs tt + hodo located")


plt.show()


fig.savefig(basepath.joinpath("athena_vs_simplex_with_hodo.png"))
