import json
import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from nmxseis.model.nslc import NSL
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.core import RelocationResult
from obspy import read_inventory, UTCDateTime

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

colormap = cm.get_cmap("turbo")

site = "NTR_Rocanville"
basepath = Path(site)
catalog_json = basepath.joinpath("athena_and_tt_hodo_inverted_locations.json")
reproject = EPSGReprojector(32613)

relocated_events = {}
athena_events = {}
with open(catalog_json) as f:
    catalog = json.load(f)

for event_id, event in catalog.items():
    relocated_events[event_id] = RelocationResult.from_json_dict(event)
    with open(basepath / "event_locations" / f"{event_id}.json") as f:
        event_json = json.load(f)
        athena_events[event_id] = EventOrigin(
            lat=event_json["latitude"],
            lon=event_json["longitude"],
            depth_m=event_json["depth_m"],
            time=UTCDateTime(event_json["time"]),
        )

for event in catalog.values():
    event["reloc_result"] = RelocationResult.from_json_dict(event)

hrz_errors = [ev["reloc_result"].get_horizontal_error_m() for ev in catalog.values()]
vrt_errors = [ev["reloc_result"].get_depth_error_m() for ev in catalog.values()]

athena_enus = np.array([ev.enu(reproject) for ev in athena_events.values()])
inverted_enus = np.array(
    [reloc_ev.origin.enu(reproject) for reloc_ev in relocated_events.values()]
)

xmin_p = np.percentile(athena_enus[:, 1], 1)
xmax_p = np.percentile(athena_enus[:, 1], 99)
ymin_p = np.percentile(-athena_enus[:, 0], 1)
ymax_p = np.percentile(-athena_enus[:, 0], 99)

x_range = xmax_p - xmin_p
y_range = ymax_p - ymin_p

xmin = xmin_p - 0.2 * x_range
xmax = xmax_p + 0.2 * x_range
ymin = ymin_p - 0.2 * y_range
ymax = ymax_p + 0.2 * y_range

inv = read_inventory(basepath.joinpath("NTR_RCN_Full.xml"))

nsls = [nsl for nsl in NSL.iter_inv(inv) if nsl.loc != "D0"]
station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))

fig, (ax_h, ax_v) = plt.subplots(2, sharex=True, figsize=[4, 8])
for nsl in nsls:
    ax_h.plot(
        station_enus[nsl][1], -station_enus[nsl][0], "v", color="w", mec="0.2", ms=6
    )
    ax_v.plot(
        station_enus[nsl][1], station_enus[nsl][2], "v", color="w", mec="0.2", ms=6
    )

scatter_args = {
    "c": vrt_errors,
    "cmap": "turbo",
    "marker": "o",
    "vmin": 0,
    "vmax": 50,
    "edgecolor": "0.2",
    "zorder": 3,
    "alpha": 0.6,
}

ax_h.scatter(inverted_enus[:, 1], -inverted_enus[:, 0], **scatter_args)
scatter = ax_v.scatter(inverted_enus[:, 1], inverted_enus[:, 2], **scatter_args)
for ax in (ax_h, ax_v):
    gray_background_with_grid(ax, grid_spacing=500)

ax_v.set_aspect("equal")
ax_v.set_xlim(xmin, xmax)
ax_v.set_ylim(-2500, 1000)

ax_h.set_aspect("equal")
ax_h.set_xlim(xmin, xmax)
ax_h.set_ylim(ymin, ymax)

ax_v.invert_xaxis()
ax_v.set_ylabel(r"$\longleftarrow$ Depth")
ax_v.set_xlabel(r"Northing $\longrightarrow$")
ax_h.set_ylabel(r"$\longleftarrow$ Easting ")

clb = fig.colorbar(scatter, ax=[ax_v, ax_h], orientation="horizontal")
clb.set_label("vertical error (m)")
fig.savefig(basepath.joinpath("error_plots.png"))
plt.show()
