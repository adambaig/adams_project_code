import json
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from nmxseis.numerics.relocation.core import RelocationResult

site = "NTR_Rocanville"

base_dir = Path(site)
with open(base_dir.joinpath("athena_and_tt_hodo_inverted_locations_with_boundary_cheating_e9.json")) as f:
    catalog = json.load(f)

for event in catalog.values():
    event["reloc_result"] = RelocationResult.from_json_dict(event)

hrz_errors = [ev["reloc_result"].get_horizontal_error_m() for ev in catalog.values()]
vrt_errors = [ev["reloc_result"].get_depth_error_m() for ev in catalog.values()]

error_bins = np.linspace(0, 150, 30)

fig, (ax_h, ax_v) = plt.subplots(1, 2, figsize=[10, 6], sharey=True)

ax_h.hist(hrz_errors, error_bins)
ax_h.set_xlabel("horizontal error (m)")
ax_v.hist(vrt_errors, error_bins)
ax_v.set_xlabel("vertical error (m)")
fig.savefig(base_dir.joinpath("error_histograms_with_bndy_cheate9.png"))
plt.show()
