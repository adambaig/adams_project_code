import json
from pathlib import Path

import numpy as np
from NocMeta.Meta import NOC_META
from nmxseis.interact.athena import AthenaClient
from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D
from obspy import read, read_inventory, UTCDateTime



hodo_scale = 100

base_dir = Path("NTR_Rocanville")
athena_code = "NTR_RCN"
STATION_XML = base_dir.joinpath("NTR_RCN_Full.xml")
inv = read_inventory(STATION_XML)
surface_nsls = [nsl for nsl in NSL.iter_inv(inv) if "T" in nsl.sta]

athena_config = NOC_META[athena_code]
athena_client = AthenaClient(
    rf'http://{athena_config["athIP"]}', athena_config["athApi"]
)

pick_error = {Phase.P: 0.010, Phase.S: 0.020}
pick_errors = {
    (nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}

with open(base_dir.joinpath("velocity_model.json")) as f:
    velocity_model_json = json.load(f)

layers = []
for layer in velocity_model_json:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )

velocity_model = VelocityModel1D(layers)
EPSG = NOC_META[athena_code]["epsg"]
reproject = EPSGReprojector(EPSG)


def raytracer(source_enu, receiver_enu, phase):
    return Raypath.isotropic_ray_trace(source_enu, receiver_enu, velocity_model, phase)


def tt_function(source_enu, receiver_enu, phase):

    return raytracer(source_enu, receiver_enu, phase).traveltime_sec


def _tt_lookup_from_station(source_enu, station, phase, station_enus):


station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
nsls = list(station_enus.keys())
average_station_location = np.average(list(station_enus.values()), axis=0)
locator = OptimizingRelocator(tt_function, station_enus, epsg=EPSG, raytracer=raytracer)

event_list = []

for week in [
    UTCDateTime(2022, 10, 6),
    UTCDateTime(2022, 10, 13),
    UTCDateTime(2022, 10, 20),
    UTCDateTime(2022, 10, 27),
    UTCDateTime(2022, 11, 3),
]:
    graph_ql_event_query = f"""
    {{eventList(
            startTime:{week.timestamp},
            endTime:{week.timestamp + 7 * 86400},
            analysisType: MANUAL) {{
            events{{
              preferredOrigin{{
                timeRms
                time
              }}
              id
            }}
        }}
    }}
    """
    event_dict = athena_client.query_gql(graph_ql_event_query)
    event_list = [*event_list, *event_dict["eventList"]["events"]]

for event_id in [e["id"] for e in event_list if int(e["id"]) <= 36279]:
    outfile = base_dir.joinpath("Oct_2022_events", f"{event_id}.json")
    if outfile.is_file():
        continue
    print(event_id)
    athena_source, pick_set = athena_client.get_preferred_origin_and_picks_for_event(
        event_id
    )
    source_enu = athena_source.enu(reproject)

    if (
        test_path := base_dir.joinpath("event_waveforms", f"{event_id}.mseed")
    ).is_file():
        stream = read(test_path)
    else:
        stream = (
            athena_client.read_event_waveforms(event_id)
            .merge()
            .rotate("->ZNE", inventory=inv)
        )
    hodograms = Hodogram.measure_around_windows(
        stream, Hodogram.get_windows_around_picks(pick_set, 0.05, 0.20)
    )
    reloc_result = locator.relocate(
        pick_set,
        pick_errors=pick_errors,
        hodograms=hodograms,
        locator_options={"maxfev": 2000, "maxiter": 2000},
    )
    min_pick, rescaled_picks, rescaled_stations = locator._rescale_inputs(
        pick_set, station_enus
    )
    location = reloc_result.origin.enu(reproject)
    pick_set.dump(
        base_dir.joinpath(
            "athena_picks",
            f'{event_id.zfill(10)}_{UTCDateTime(athena_source.time).strftime("%Y%m%d.%H%M%S.%f")}.picks',
        )
    )
    catalog = {
        **reloc_result.to_json_serializable(),
        "athena_lat": athena_source.lat,
        "athena_lon": athena_source.lon,
        "athena_depth_m": athena_source.depth_m,
    }
    with open(outfile, "w") as f:
        json.dump(catalog, f)
