import matplotlib

matplotlib.use("Qt5Agg")

import json
import os
from pathlib import Path
import numpy as np
from obspy import read_inventory, UTCDateTime

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.nslc import NSL
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

from NocMeta.Meta import NOC_META


athena_code = "NTR_RCN"
STATION_XML = os.path.join("NTR_Rocanville", "NTR_RCN_Full.xml")
inventory = read_inventory(STATION_XML)
surface_nsls = [nsl for nsl in NSL.iter_inv(inventory) if "T" in nsl.sta]

athena_config = NOC_META[athena_code]
athena_client = AthenaClient(
    rf'http://{athena_config["athIP"]}', athena_config["athApi"]
)

with open(os.path.join("NTR_Rocanville", "velocity_model.json")) as f:
    velocity_model_json = json.load(f)

epsg = NOC_META[athena_code]["epsg"]

layers = []
for layer in velocity_model_json:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )
velocity_model = VelocityModel1D(layers)
start_time = UTCDateTime(2022, 7, 1)
end_time = UTCDateTime(2022, 8, 1)

event_list_query = f"""{{eventList(
    latitudeMinimum:50.38292,
    latitudeMaximum:50.49968,
    longitudeMinimum:-101.79498,
    longitudeMaximum:-101.66452,
    startTime:{start_time.timestamp},
    endTime:{end_time.timestamp},
    analysisType: MANUAL) {{
	events {{
	  id
        preferredOrigin{{
        timeRms
	    }}
	  }}
    }}
}}
"""

t_rms = {
    v["id"]: v["preferredOrigin"]["timeRms"]
    for v in athena_client.query_gql(event_list_query)["eventList"]["events"]
}

with open(Path("NTR_Rocanville").joinpath("t_rms_athena.json"), "w") as f:
    json.dump(t_rms, f)
