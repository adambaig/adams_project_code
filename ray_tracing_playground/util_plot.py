import matplotlib.pyplot as plt
import numpy as np


def plot_around_minimum(x, obj_function, size, n_samples=101):
    dx = np.linspace(-size, size, n_samples)
    y1 = [obj_function(x + [d, 0, 0, 0]) for d in dx]
    y2 = [obj_function(x + [0, d, 0, 0]) for d in dx]
    y3 = [obj_function(x + [0, 0, d, 0]) for d in dx]
    y4 = [obj_function(x + [0, 0, 0, d]) for d in dx]
    fig, ax = plt.subplots()
    ax.plot(dx, y1)
    ax.plot(dx, y2)
    ax.plot(dx, y3)
    ax.plot(dx, y4)

    plt.show()


def plot_simplex_path_3d(minimizer):
    fig, ((ax_01, ax_off), (ax_02, ax_12)) = plt.subplots(2, 2)
    x = np.array(minimizer.allvecs)[:, 0]
    y = np.array(minimizer.allvecs)[:, 1]
    z = np.array(minimizer.allvecs)[:, 2]
    t = np.array(minimizer.allvecs)[:, 3]
    for ax in (ax_01, ax_02, ax_12):
        ax.set_aspect("equal")
    ax_01.plot(x, y, zorder=3)
    ax_02.plot(x, z, zorder=3)
    ax_12.plot(y, z, zorder=3)
    ax_01.plot(x[0], y[0], "o", zorder=4)
    ax_02.plot(x[0], z[0], "o", zorder=4)
    ax_12.plot(y[0], z[0], "o", zorder=4)
    ax_01.plot(x[-1], y[-1], "*", zorder=4)
    ax_02.plot(x[-1], z[-1], "*", zorder=4)
    ax_12.plot(y[-1], z[-1], "*", zorder=4)
    plt.show()
