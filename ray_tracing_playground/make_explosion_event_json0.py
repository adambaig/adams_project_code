import os
import json


import numpy as np
import obspy
import pyproj

from nmxseis.model.phase import Phase
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

event_t0 = obspy.UTCDateTime(os.path.basename(seedfile).split("_")[0])

with open(r"synthetic/228_stations_epsg26911.csv") as f:
    _head = f.readline()
    lines = f.readlines()


explosion = MomentTensor.from_vector(array([1, 1, 1, 0, 0, 0]))

inv = obspy.read_inventory("synthetic_Paramount_2_28_example\\synthetic_sta.xml")

stations = {}
for network in inv:
    for station in network:
        stations[f"{network.code}.{station.code}"] = {
            "latitude": station.latitude,
            "longitude": station.longitude,
            "elevation": station.elevation,
        }

eastings, northings = reproject(
    *zip(*[(v["longitude"], v["latitude"]) for v in stations.values()])
)

for i_station, station in enumerate(stations.values()):
    station["enu"] = np.array(
        [eastings[i_station], northings[i_station], station["elevation"]]
    )


velocity_model = VelocityModel1D([VMLayer1D(rho=2500, vp=5000, vs=3000)])
event_location = {
    "depth": 2.1,
    "time": 1637615497.695214,
    "latitude": 54.401317823038646,
    "longitude": -116.93805538849605,
}
easting, northing = reproject(event_location["longitude"], event_location["latitude"])
event_enu = [easting, northing, -1000 * event_location["depth"]]

source = {
    **event_location,
    "enu": np.array(event_enu),
    "moment_magnitude": 0,
    "stress_drop": 1.0e4,  # static stress drop
}

ray.traveltime_sec

phase.value
channel_code = {"P": "CPZ", "S": "CPE"}
i = 0
arrivals = []
for station_id, station in stations.items():
    for phase in [Phase.P]:
        i += 1
        arrival_id = str(6780 + i)
        ray = Raypath.isotropic_ray_trace(
            source["enu"], station["enu"], velocity_model, phase
        )
        arrival_time = obspy.UTCDateTime(source["time"] + ray.traveltime_sec).timestamp
        pick_id = str(1234 + i)
        channel_id = str(3456 + i)
        arrival = {
            "id": arrival_id,
            "pick": {
                "id": pick_id,
                "phase": phase.value,
                "time": arrival_time,
                "channel": {
                    "id": channel_id,
                    "channelPath": {
                        "networkCode": "CV",
                        "stationCode": station_id.split(".")[1],
                        "locationCode": "00",
                        "channelCode": channel_code[phase.value],
                    },
                },
            },
        }
        arrivals.append(arrival)


event_json = {
    "id": "1",
    "name": "20211028-2",
    "preferredOrigin": {
        "id": "2",
        "eventId": "1",
        "createdAt": obspy.UTCDateTime(now).timestamp,
        "depth": 2.1,
        "time": obspy.UTCDateTime(source["time"]).timestamp,
        "latitude": source["latitude"],
        "longitude": source["longitude"],
        "arrivals": arrivals,
    },
}

with open(r"synthetic_explosion/synthetic_event.json", "w") as f:
    json.dump(event_json, f)
