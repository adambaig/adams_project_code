from pathlib import Path

import numpy as np
from nmxseis.numerics.geometry import GridXYZ
from nmxseis.numerics.velocity_model.vm_1d import VM1DTTInterpolator, VM1DIncomingEnuInterpolator, \
	VM1DRayPropertyInterpolator

from read_inputs import read_velocity_model, get_station_enus_from_MO, read_station_csv

if __name__ == '__main__':
	velocity_model = read_velocity_model()
	downhole_station_enus = read_station_csv()
	station_enus = {**get_station_enus_from_MO(), **downhole_station_enus}

	station_enus_array = np.array([np.array(v) for v in downhole_station_enus.values()])

	elevations = np.hstack([-2000,
	                        np.arange(min(station_enus_array[:, 2]) - 150,
	                                  max(station_enus_array[:, 2]) + 151, 15),
	                        550, 1000])

	min_east = min(station_enus_array[:, 0])
	max_east = max(station_enus_array[:, 0])
	min_north = min(station_enus_array[:, 1])
	max_north = max(station_enus_array[:, 1])

	eastings = np.hstack(
		[
			np.arange(-10000 + min_east, -1000 + min_east, 1000),
			np.arange(-1000 + min_east, -300 + min_east, 100),
			np.arange(-300 + min_east, 300 + max_east, 50),
			np.arange(300 + max_east, 1000 + max_east, 100),
			np.arange(1000 + max_east, 10000 + max_east, 1000)
		]
	)
	northings = np.hstack(
		[
			np.arange(-10000 + min_north, -1000 + min_north, 1000),
			np.arange(-1000 + min_north, -300 + min_north, 100),
			np.arange(-300 + min_north, 300 + max_north, 50),
			np.arange(300 + max_north, 1000 + max_north, 100),
			np.arange(1000 + max_north, 10000 + max_north, 1000)
		]
	)

	grid = GridXYZ(xs=eastings, ys=northings, zs=elevations)

	tt_interp, hodo_interp = VM1DRayPropertyInterpolator.from_vm(
		(VM1DTTInterpolator, VM1DIncomingEnuInterpolator),
		velocity_model=velocity_model,
		station_enus=station_enus,
		grid=grid,
	)
	tt_interp.dump_to_npz(Path().joinpath('interpolators','interp_tt_mosaic.npz'))
	hodo_interp.dump_to_npz(Path().joinpath('interpolators','interp_hodo_mosaic.npz'))