import json
import time
from pathlib import Path

import numpy as np
from nmxseis.interact.stationxml import ENUStreamMapper
from nmxseis.model.hodogram import Hodogram, VerticalArrayHodogramCollection, VerticalArray
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from nmxseis.numerics.geodesy import EPSGReprojector

from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from nmxseis.numerics.relocation.optimizing_relocator import (
	OptimizingRelocatorHint as Hint,
)
from nmxseis.numerics.velocity_model.vm_1d import (
    VM1DIncomingEnuInterpolator,
    VM1DRayPropertyInterpolator,
    VM1DTTInterpolator,
)
from nmxseis.numerics.util import float_err
from obspy import read, read_inventory

from read_inputs import read_velocity_model





def strip_err(number_with_maybe_error):
	if isinstance(number_with_maybe_error, float_err):
		return number_with_maybe_error.val
	return number_with_maybe_error


hodo_scale = 100

base_dir = Path("Mosaic")
STATION_XML = base_dir.joinpath("updated_station.xml")
MOSAIC_EPSG = 32613
inv = read_inventory(STATION_XML)

interp_folder = base_dir.joinpath("interpolators")

tt_fn = interp_folder.joinpath("interp_tt_mosaic.npz")
hodo_fn = interp_folder.joinpath("interp_hodo_mosaic.npz")

tt_interp = VM1DTTInterpolator.load_npz(tt_fn)
hodo_interp = VM1DIncomingEnuInterpolator.load_npz(hodo_fn)

pick_error = {Phase.P: 0.0010, Phase.S: 0.0020}
pick_errors = {
	(nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
	(nsl, ph): 1000 * pick_error[ph]
	for nsl in NSL.iter_inv(inv)
	for ph in [Phase.P, Phase.S]
}

vm1d = read_velocity_model(
	file_path=Path("Mosaic").joinpath("metric_velocity_model.json")
)

reproject = EPSGReprojector(MOSAIC_EPSG)
station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
nsls = list(station_enus.keys())

initial_locator = OptimizingRelocator(
	station_enus,
	MOSAIC_EPSG,
	# vm=vm1d
	tt_interpolator=tt_interp,
	ray_interpolator=hodo_interp,
	hodogram_weight=0.1
)

pick_files = list(base_dir.joinpath("picks").glob("*.picks"))
events = {}
array_16_enus = {
	station: enu for station, enu in station_enus.items() if station.sta[:2] == "16"
}
array_16 = VerticalArray(nsls=array_16_enus.keys(), inv=inv, reprojector=reproject)
enz_mapper = ENUStreamMapper(inv)

fmin = 40
fmax = 400
doFilter = True
prePick = 0.001
postPick = 0.01
minThresh = 0.8

time_raytracing = time_interpolating = 0.
number_of_interpolations = 0

for pick_file in pick_files:
	pick_set = PickSet.load(pick_file)
	event_id, timestamp = pick_file.stem.split("_")
	seed_file = list(base_dir.joinpath("event_seeds_12z").glob(f"{event_id}*.mseed"))[0]
	stream = read(seed_file).rotate("->ZNE", inventory=inv)
	if doFilter:
		stream.filter("bandpass", freqmin=fmin, freqmax=fmax)
	windows = Hodogram.get_windows_around_picks(pick_set, prePick, postPick)

	hodograms = Hodogram.measure_around_windows(stream, windows)
	p_16_hodograms = {
		(nsl, ph): h
		for (nsl, ph), h in hodograms.items()
		if ph.is_P and nsl.sta[:2] == "16"
		# and h.linearity>minThresh
	}
	p_16_windows = {
		(nsl, ph): h
		for (nsl, ph), h in windows.items()
		if ph.is_P and nsl.sta[:2] == "16"
	}

	array_hodograms = VerticalArrayHodogramCollection.from_windows_and_stream(
		p_16_windows,
		stream,
		array_16,
	)

	starting_enut = array_hodograms.get_starting_enut(pick_set, vm1d)

	starttime = time.time()
	hint = Hint(
		hodograms=p_16_hodograms,
		pick_errors=pick_errors,
		locator_options={"maxfev": 2000, "maxiter": 2000, "return_all": True},
		initial_enut=starting_enut
	)
	try:
		reloc_result = initial_locator.relocate(pick_set, hint)
		print(f'{event_id} located')
		events[event_id] = {
			**reloc_result.to_json_serializable(),
			'starting_loc': starting_enut.tolist()
		}
	except NotImplementedError:
		print(f'{event_id} failed to locate')
	time_raytracing += time.time() - starttime



with open(base_dir.joinpath('locations_from_interp.json'), 'w') as f:
	json.dump(events, f)
