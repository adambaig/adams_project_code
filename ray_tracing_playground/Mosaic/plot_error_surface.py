import sys
from pathlib import Path

import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from obspy import read, read_inventory, UTCDateTime

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

from read_inputs import (
    read_synthetic_catalog,
    homogeneous_velocity_model,
    read_velocity_model,
)
from braskem.locate_synthetics_tt_hodo import (
    get_hodo_windows,
    measure_hodograms_for_event,
)

hodo_scale = 100

base_dir = Path("synthetics")
STATION_XML = base_dir.joinpath("synth_station.xml")
SASK_UTM_EPSG = 26913
inv = read_inventory(STATION_XML)
real_locations = read_synthetic_catalog(base_dir.joinpath("synthetic_catalog.csv"))

pick_error = {Phase.P: 0.010, Phase.S: 0.020}
pick_errors = {
    (nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
    (nsl, ph): 1000 * pick_error[ph]
    for nsl in NSL.iter_inv(inv)
    for ph in [Phase.P, Phase.S]
}

velocity_model = read_velocity_model()

reproject = EPSGReprojector(SASK_UTM_EPSG)


def raytracer(source_enu, receiver_enu, phase):
    return Raypath.isotropic_ray_trace(source_enu, receiver_enu, velocity_model, phase)


def tt_function(source_enu, receiver_enu, phase):
    return raytracer(source_enu, receiver_enu, phase).traveltime_sec


station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
nsls = list(station_enus.keys())
locator = OptimizingRelocator(
    tt_function,
    station_enus,
    epsg=SASK_UTM_EPSG,
    raytracer=raytracer,
    hodogram_weight=1,
)
pick_files = list(base_dir.joinpath("picks").glob("*.picks"))

pick_file = pick_files[1]

pick_set = PickSet.load(pick_file)
event_id = pick_file.stem.split("_")[0]
seed_file = base_dir.joinpath(
    "mseed", f'{pick_file.stem.split("_")[1].replace(".", "_")}.mseed'
)
stream = read(seed_file).rotate("->ZNE", inventory=inv)
hodograms = measure_hodograms_for_event(
    stream, get_hodo_windows(pick_set, 0.0005, 0.0020)
)
p_hodograms = {(nsl, ph): h for (nsl, ph), h in hodograms.items() if ph.is_P}
hodogram_error_radians = assign_hodogram_errors(hodograms)
reloc_result = locator.relocate(
    pick_set,
    pick_errors=pick_errors,
    hodograms=p_hodograms,
    hodogram_errors_radians=hodogram_error_radians,
    locator_options={"maxfev": 2000, "maxiter": 2000},
)
location = reloc_result.origin.enu(reproject)

min_pick, rescaled_picks, recentered_stations = locator._rescale_inputs(
    pick_set,
    station_enus,
)
local_enut_ms = locator._translate_to_local_enut(
    location, reloc_result.origin.time, min_pick
)
input_location = real_locations[seed_file.name]["enu"]
real_enut_ms = locator._translate_to_local_enut(
    input_location, reloc_result.origin.time, min_pick
)
minimum_combo_chi = locator.combined_chi_sq(
    local_enut_ms,
    rescaled_picks,
    p_hodograms,
    recentered_stations,
    pick_errors_ms=pick_errors_ms,
    hodogram_errors_radians=hodogram_error_radians,
)

hodo_combo_chi = locator.hodo_chi_sq(
    local_enut_ms[:3],
    p_hodograms,
    recentered_stations,
    hodogram_errors_radians=hodogram_error_radians,
)

other_combo_chi = locator.hodo_chi_sq(
    real_enut_ms[:3],
    p_hodograms,
    recentered_stations,
    hodogram_errors_radians=hodogram_error_radians,
)

fig, (ax_p, ax_d) = plt.subplots(1, 2, sharey=True)

for nsl in nsls:
    ax_p.plot(*station_enus[nsl][:2], "v", color="w", mec="0.2", ms=10)
    ax_d.plot(*station_enus[nsl][1:][::-1], ">", color="w", mec="0.2", ms=10)
ax_p.plot(*location[:2], "o", color="gold", mec="0.2", zorder=5)
ax_d.plot(*location[1:][::-1], "o", color="gold", mec="0.2", zorder=5)
ax_p.plot(*input_location[:2], "*", color="orangered", mec="0.2", zorder=5)
ax_d.plot(*input_location[1:][::-1], "*", color="orangered", mec="0.2", zorder=5)
for (nsl, ph), hg in hodograms.items():
    if ph.is_S:
        continue
    uv = hg.unit_vector_enu
    ax_p.plot(
        station_enus[nsl][0] + hodo_scale * uv[0] * np.array([1, -1]),
        station_enus[nsl][1] + hodo_scale * uv[1] * np.array([1, -1]),
        lw=2,
        color="k",
        alpha=hg.linearity if ph.is_P else hg.planarity,
    )
    ax_d.plot(
        station_enus[nsl][2] + hodo_scale * uv[2] * np.array([1, -1]),
        station_enus[nsl][1] + hodo_scale * uv[1] * np.array([1, -1]),
        lw=2,
        color="k",
        alpha=hg.linearity if ph.is_P else hg.planarity,
    )

spacing = 30
xmin, xmax = 720600, 721400
ymin, ymax = 5611000, 5612600
zmin, zmax = ax_d.get_xlim()
xrange = np.arange(xmin, xmax, spacing)
yrange = np.arange(ymin, ymax, spacing)
zrange = np.arange(zmin, zmax, spacing)


def get_slices_of_combo_chi_sq():
    chi_combo_plan = np.zeros([len(xrange), len(yrange)])
    chi_combo_depth = np.zeros([len(zrange), len(yrange)])
    for ix, x in enumerate(xrange):
        for iy, y in enumerate(yrange):
            global_enu = np.array([x, y, location[2]])
            local_enut = locator._translate_to_local_enut(
                global_enu, reloc_result.origin.time, min_pick
            )
            chi_combo_plan[ix, iy] = locator.combined_chi_sq(
                local_enut,
                rescaled_picks,
                hodograms,
                recentered_stations,
                pick_errors_ms=pick_errors_ms,
                hodogram_errors_radians=hodogram_error_radians,
            )
    for iy, y in enumerate(yrange):
        for iz, z in enumerate(zrange):
            global_enu = np.array([location[0], y, z])
            local_enut = locator._translate_to_local_enut(
                global_enu, reloc_result.origin.time, min_pick
            )
            chi_combo_depth[iz, iy] = locator.combined_chi_sq(
                local_enut,
                rescaled_picks,
                hodograms,
                recentered_stations,
                pick_errors_ms=pick_errors_ms,
                hodogram_errors_radians=hodogram_error_radians,
            )
    return chi_combo_plan, chi_combo_depth


def plot_slices(chi_plan, chi_depth):
    min_c = min(np.amin(chi_depth), np.amin(chi_plan))
    log_max_c = max(
        np.amax(np.log10(chi_depth - min_c)), np.amax(np.log10(chi_plan - min_c))
    )
    colors = ax_d.pcolor(
        zrange, yrange, np.log10(chi_depth - min_c).T, vmin=0, vmax=log_max_c, alpha=0.6
    )
    ax_p.pcolor(
        xrange, yrange, np.log10(chi_plan - min_c).T, vmin=0, vmax=log_max_c, alpha=0.6
    )
    return colors


chi_combo_plan, chi_combo_depth = get_slices_of_combo_chi_sq()
colors = plot_slices(chi_combo_plan, chi_combo_depth)
for ax in (ax_p, ax_d):
    ax.set_aspect("equal")
    gray_background_with_grid(ax, grid_spacing=1000)

ax_d.invert_xaxis()

plt.show()
