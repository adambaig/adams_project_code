from pathlib import Path

from obspy import read

seed_files = Path("Mosaic").joinpath("event_seeds").glob("*.mseed")
out_dir = Path("Mosaic").joinpath("event_seeds_12z")

for seed_file in list(seed_files):
    st = read(seed_file)
    for tr in st:
        if tr.stats.channel == "GPN":
            tr.stats.channel = "GP1"
        if tr.stats.channel == "GPE":
            tr.stats.channel = "GP2"
    out_file = out_dir.joinpath(seed_file.name)
    st.write(out_file, format="MSEED")
