from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from obspy import read, read_inventory, UTCDateTime


from read_inputs import (
    read_synthetic_catalog,
    read_velocity_model,
    homogeneous_velocity_model,
)

hodo_scale = 100

BASE_DIR = Path("homogeneous_synthetics")
STATION_XML = BASE_DIR.joinpath("synth_station.xml")
SASK_UTM_EPSG = 26913
inv = read_inventory(STATION_XML)
real_locations = read_synthetic_catalog(BASE_DIR.joinpath("synthetic_catalog.csv"))


pick_error = {Phase.P: 0.001, Phase.S: 0.002}
pick_errors = {
    (nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
    (nsl, ph): 1000 * pick_error[ph]
    for nsl in NSL.iter_inv(inv)
    for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
    (nsl, ph): 2 for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}

velocity_model = homogeneous_velocity_model()

reproject = EPSGReprojector(SASK_UTM_EPSG)


def raytracer(source_enu, receiver_enu, phase):
    return Raypath.isotropic_ray_trace(source_enu, receiver_enu, velocity_model, phase)


def tt_function(source_enu, receiver_enu, phase):
    return raytracer(source_enu, receiver_enu, phase).traveltime_sec


station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
nsls = list(station_enus.keys())
locator = OptimizingRelocator(
    tt_function,
    station_enus,
    epsg=SASK_UTM_EPSG,
    raytracer=raytracer,
)
pick_files = list(BASE_DIR.joinpath("picks").glob("*.picks"))

pick_file = pick_files[1]
origin_time = UTCDateTime(
    "".join((pf_split := pick_file.stem.split("_")[1].split("."))[:2])
    + "."
    + pf_split[2]
)
pick_set = PickSet.load(pick_file)
event_id = pick_file.stem.split("_")[0]
seed_file = BASE_DIR.joinpath(
    "mseed", f'{pick_file.stem.split("_")[1].replace(".", "_")}.mseed'
)
stream = read(seed_file).rotate("->ZNE", inventory=inv)
hodograms = Hodogram.measure_around_windows(
    stream, Hodogram.get_windows_around_picks(pick_set, 0.0005, 0.0020)
)
p_hodograms = {(nsl, ph): h for (nsl, ph), h in hodograms.items() if ph.is_P}
min_pick, rescaled_picks, recentered_stations = locator._rescale_inputs(
    pick_set,
    station_enus,
)
input_location = real_locations[seed_file.name]["enu"]
real_enut_ms = locator._translate_to_local_enut(input_location, origin_time, min_pick)
combo_chi_sq = locator.combined_chi_sq(
    real_enut_ms,
    rescaled_picks,
    p_hodograms,
    recentered_stations,
    pick_errors_ms=pick_errors_ms,
)

hodo_chi_sq = locator.hodo_chi_sq(
    real_enut_ms[:3],
    p_hodograms,
    recentered_stations,
)

tt_chi_sq = locator.tt_chi_sq(
    real_enut_ms, rescaled_picks, recentered_stations, pick_errors_ms=pick_errors_ms
)
