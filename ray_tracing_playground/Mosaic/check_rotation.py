from pathlib import Path

import matplotlib.pyplot as plt
from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet

import numpy as np
from obspy import read, read_inventory


basedir = Path('Mosaic')
inv = read_inventory(basedir.joinpath('updated_station.xml'))
st = read(basedir.joinpath('event_seeds_12z', '00085_20210923.150241.056999.mseed'))
picks = PickSet.load(basedir.joinpath('picks', '00085_20210923.150241.056999.picks'))

nsl = NSL.parse('XW.1607.00')

tr_3c = nsl.select_from_stream(st).rotate('->ZNE', inventory=inv)

tr_3c.plot()

windows = Hodogram.get_windows_around_picks(picks, 0.005, 0.02)
hodogram = Hodogram.from_windowed_phase(tr_3c, Phase.P, windows[nsl, Phase.P])
