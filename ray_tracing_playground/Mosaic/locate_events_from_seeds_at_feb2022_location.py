import json
import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from nmxseis.model.hodogram import Hodogram, VerticalArrayHodogramCollection
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from nmxseis.model.station_groups import VerticalArray
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from obspy import read, read_inventory

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

from read_inputs import read_velocity_model, read_old_catalog

hodo_scale = 100

base_dir = Path("Mosaic")
LOGIKDATA_DIR = Path(r"C:\logiklyst\PSM\logikdata\MOC_EST")
STATION_XML = LOGIKDATA_DIR.joinpath("MOC_EST_Full.xml")
SASK_UTM_EPSG = 26913
inv = read_inventory(STATION_XML)

pick_error = {Phase.P: 0.0010, Phase.S: 0.0020}
pick_errors = {
    (nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
    (nsl, ph): 1000 * pick_error[ph]
    for nsl in NSL.iter_inv(inv)
    for ph in [Phase.P, Phase.S]
}

velocity_model = read_velocity_model(
    file_path=Path("Mosaic").joinpath("metric_velocity_model.json")
)

reproject = EPSGReprojector(SASK_UTM_EPSG)
old_catalog = read_old_catalog(base_dir.joinpath("mosaic_catalog.csv"))


station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
nsls = list(station_enus.keys())
locator = OptimizingRelocator(
    tt_function,
    station_enus,
    epsg=SASK_UTM_EPSG,
    raytracer=raytracer,
    hodogram_weight=0.1,
)
pick_files = list(base_dir.joinpath("picks").glob("*.picks"))
events = {}
array_16_enus = {
    station: enu for station, enu in station_enus.items() if station.sta[:2] == "16"
}
array_16 = VerticalArray(nsls=array_16_enus.keys(), inv=inv, reprojector=reproject)

for pick_file in pick_files[:1]:
    pick_set = PickSet.load(pick_file)
    event_id, timestamp = pick_file.stem.split("_")
    seed_file = list(base_dir.joinpath("event_seeds_12z").glob(f"{event_id}*.mseed"))[0]
    stream = read(seed_file).rotate("->ZNE", inventory=inv)
    windows = Hodogram.get_windows_around_picks(pick_set, 0.0005, 0.0020)
    hodograms = Hodogram.measure_around_windows(stream, windows)
    p_16_hodograms = {
        (nsl, ph): h
        for (nsl, ph), h in hodograms.items()
        if ph.is_P and nsl.sta[:2] == "16"
    }
    p_16_windows = {
        (nsl, ph): h
        for (nsl, ph), h in windows.items()
        if ph.is_P and nsl.sta[:2] == "16"
    }
    if int(event_id) not in old_catalog:
        continue
    old_cat_event = old_catalog[int(event_id)]
    starting_enut = np.array(
        [
            *old_cat_event["enu"],
            old_cat_event["time"] - min(p.time for p in pick_set.picks),
        ]
    )
    reloc_result = locator.relocate(
        pick_set,
        pick_errors=pick_errors,
        hodograms=hodograms,
        initial_enut=starting_enut,
        locator_options={"maxfev": 2000, "maxiter": 2000},
    )
    location = reloc_result.origin.enu(reproject)
    min_pick, rescaled_picks, recentered_stations = locator._rescale_inputs(
        pick_set,
        station_enus,
    )
    local_enut_ms = locator._translate_to_local_enut(
        location, reloc_result.origin.time, min_pick
    )
    events[event_id] = reloc_result.to_json_serializable()
