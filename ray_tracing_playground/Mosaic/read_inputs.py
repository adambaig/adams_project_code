import json
from pathlib import Path

from nmxseis.model.nslc import NSL
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.velocity_model.vm_0d import VelocityModelHomogeneous
from nmxseis.numerics.velocity_model.vm_1d import VMLayer1D, VelocityModel1D

import numpy as np
from obspy import UTCDateTime, read_inventory

BASE_DIR = Path(r"C:\Users\adambaig\Project\ray_tracing_playground\Mosaic")
STATION_XML = BASE_DIR.joinpath('station_xmls','MOC_EST_Full.xml')
SASK_UTM_EPSG = 3158
INV = read_inventory(STATION_XML)
REPROJECT = EPSGReprojector(SASK_UTM_EPSG)
BB_INV = read_inventory(BASE_DIR.joinpath('station_xmls','MO.xml'))
PRODUCTION_EPSG = 3158
PRODUCTION_REPROJECT = EPSGReprojector(PRODUCTION_EPSG)

def homogeneous_velocity_model():
    return VelocityModelHomogeneous(vp=5000, vs=3000, rho=2500)

def read_velocity_model(file_path=BASE_DIR.joinpath("metric_velocity_model.json")):
    with open(file_path) as f:
        velocity_model_json = json.load(f)
    layers = []
    for layer in velocity_model_json:
        layers.append(
            VMLayer1D(
                vp=layer["vp"],
                vs=layer["vs"],
                rho=layer["rho"],
                top=layer["top"] if "top" in layer else np.inf,
            )
        )
    return VelocityModel1D(layers)



correction = {11: 1, 10: 1, 13: -1}
offset = {11: 0, 10: 0, 13: 514.1976}


def read_old_catalog(filepath=Path("mosaic_catalog.csv")):
    with open(filepath) as f:
        _ = f.readline()
        lines = f.readlines()
    events = {}
    for line in lines:
        split_line = line.split(",")
        event_id = int(split_line[2])
        events[event_id] = {
            "enu": np.array(
                [offset[c] + correction[c] * float(split_line[c]) for c in [10, 11, 13]]
            ),
            "time": UTCDateTime(split_line[3]),
            "mw": float(split_line[16]),
            "corner_freq": float(split_line[17]),
            "stress_drop": float(split_line[18]) * 1e6,
        }
    return events


def read_synthetic_catalog(pathname):
    with open(pathname) as f:
        _ = f.readline()
        lines = f.readlines()
    events = {}
    for line in lines:
        split_line = line.split(",")
        event_id = split_line[0]
        events[event_id] = {
            "enu": np.array([float(split_line[c]) for c in [1, 2, 3]]),
            "mw": float(split_line[4]),
            "stress_drop": float(split_line[5]),
        }
    return events

def get_station_enus():
    return REPROJECT.get_station_enus(INV, NSL.iter_inv(INV))

def get_station_enus_from_MO():
    return PRODUCTION_REPROJECT.get_station_enus(BB_INV, NSL.iter_inv(BB_INV))

def read_station_csv():
    with open(BASE_DIR.joinpath('station_coords_csv','mosaic_downhole_stations.csv')) as f:
        _head = f.readline()
        lines = f.readlines()
    station_enus = {}
    net = 'MO'
    for line in lines:
        split_line = line.split(',')
        sta = split_line[1]
        loc = split_line[2].zfill(2)
        enu = np.array([float(v) for v in split_line[3:6]])
        station_enus[NSL.parse(f'{net}.{sta}.{loc}')] = enu
    return station_enus


