import json
from read_inputs import get_station_enus

station_enus = get_station_enus()
out_station = {}

for nsl, enu in station_enus.items():
	out_station[f'{nsl}'] = enu.tolist()

with open('station_enus.json', 'w') as f:
	json.dump(out_station, f)