import json
import sys
from pathlib import Path

import matplotlib.pyplot as plt
from nmxseis.model.nslc import NSL
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.core import RelocationResult
import numpy as np
from obspy import read, read_inventory

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid
from read_inputs import read_old_catalog


base_dir = Path("Mosaic")
STATION_XML = base_dir.joinpath("updated_station.xml")
SASK_UTM_EPSG = 26913
inv = read_inventory(STATION_XML)
with open(base_dir.joinpath('selectively_flipped_locations_from_interp.json')) as f:
    catalog = json.load(f)

old_catalog = read_old_catalog(base_dir.joinpath("mosaic_catalog.csv"))

reproject = EPSGReprojector(SASK_UTM_EPSG)

from obspy import UTCDateTime
for event in catalog.values():
    event["origin"]["UTC_datetime"] = UTCDateTime(event["origin"]["UTC_datetime"])

station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
nsls = list(station_enus.keys())

location_enus = np.array(
    [EventOrigin(*v["origin"].values()).enu(reproject) for v in catalog.values()]
)
mws = np.array(
    [v["mw"] for k,v in old_catalog.items() if str(k).zfill(5) in catalog]
)
comparison_enus = np.array(
    [v["enu"] for k, v in old_catalog.items() if str(k).zfill(5) in catalog]
)

start_loc_enus = np.array(
    [v["starting_loc"] for v in catalog.values()]
)

fig, (ax_p, ax_d) = plt.subplots(1, 2, sharey=True, figsize=[8,8])

station_kwargs = {"color": "w", "mec": "0.2", "ms": 10}
location_kwargs = {"color": "royalblue", "mec": "0.2", "zorder": 5, "alpha": 0.6}
old_catalog_kwargs = {"color": "firebrick", "mec": "0.2", "zorder": 4, "alpha": 0.6}

scatter_kwargs = {"cmap": 'turbo', "edgecolor": "0.2","zorder": 6, "alpha": 0.5}

for nsl in nsls:
    ax_p.plot(*station_enus[nsl][:2], "v", **station_kwargs)
    ax_d.plot(*station_enus[nsl][1:][::-1], ">", **station_kwargs)

ax_p.plot(location_enus[:, 0], location_enus[:, 1], ".", **location_kwargs)
ax_d.plot(location_enus[:, 2], location_enus[:, 1], ".", **location_kwargs)
ax_p.plot(start_loc_enus[:, 0], start_loc_enus[:, 1], ".", **old_catalog_kwargs)
ax_d.plot(start_loc_enus[:, 2], start_loc_enus[:, 1], ".", **old_catalog_kwargs)

for loc, start in zip(location_enus, start_loc_enus):
	ax_p.plot([start[0], loc[0]], [start[1], loc[1]], zorder=-1, lw=-1, color='0.2')
	ax_d.plot([start[2], loc[2]], [start[1], loc[1]], zorder=-1, lw=-1, color='0.2')

for ax in (ax_p, ax_d):
    ax.set_aspect("equal")
    gray_background_with_grid(ax, grid_spacing=100)

ax_d.invert_xaxis()
fig.savefig(base_dir.joinpath("selectively_flipped_locations_from_interp_only.png"))

depth_error =[RelocationResult.from_json_dict(reloc).get_depth_error_m() for reloc in catalog.values()]
fig_e,ax_e = plt.subplots()
ax_e.hist(depth_error, np.arange(0,20,0.5))
ax_e.set_xlabel('depth error (m)')

fig_e.savefig(base_dir.joinpath("depth_error_interp_only.png"))


for ev_id, event in catalog.items():
    catalog[ev_id]["reloc_result"] = RelocationResult.from_json_dict(event)

fig,ax = plt.subplots()
ax.plot(location_enus[:, 1], location_enus[:, 2], ".", **location_kwargs)
ax.set_xlabel('northing (m)')
ax.set_ylabel('elevation (m)')
# ax.set_ylim([-351.15, -351.13])
plt.show()