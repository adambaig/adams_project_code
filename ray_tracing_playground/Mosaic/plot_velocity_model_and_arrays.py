from pathlib import Path

import matplotlib.pyplot as plt

from read_inputs import get_station_enus, read_velocity_model

station_enus = get_station_enus()

velocity_model = read_velocity_model(Path('metric_velocity_model.json'))

fig, ax = plt.subplots(figsize=[6, 8])
vp_curve = [[velocity_model.layers[0].vp, 550]]
vs_curve = [[velocity_model.layers[0].vs, 550]]
for i_layer,layer in enumerate(velocity_model.layers[:-1]):
    vp_curve.append(
        [layer.vp, velocity_model.layers[i_layer + 1].top]
    )
    vp_curve.append(
        [velocity_model.layers[i_layer + 1].vp, velocity_model.layers[i_layer + 1].top]
    )
    vs_curve.append(
        [layer.vs, velocity_model.layers[i_layer + 1].top]
    )
    vs_curve.append(
        [velocity_model.layers[i_layer + 1].vs, velocity_model.layers[i_layer + 1].top]
    )

vp_curve.append([velocity_model.layers[-1].vp, -1000])
vs_curve.append([velocity_model.layers[-1].vs, -1000])
ax.plot([a[0] for a in vp_curve], [a[1] for a in vp_curve], color="royalblue", lw=2)
ax.plot([a[0] for a in vs_curve], [a[1] for a in vs_curve], color="firebrick", lw=2)
ax.legend(["$v_p$", "$v_s$"])

ax.set_facecolor("0.9")
for nsl, enu in {nsl:enu for nsl, enu in station_enus.items() if nsl.sta=='GR114'}.items():
    ax.plot(6000, enu[2], "v", color="0.3", markeredgecolor="k", zorder=int(nsl.loc))
# ax.text(22000, -station["tvdss"] - 50, "BF16", ha="center", va="top")
#
# for station in gr114.values():
#     ax.plot(25000, -station["tvdss"], "v", color="0.5", markeredgecolor="k")
# ax.text(25000, -station["tvdss"] - 50, "GR114", ha="center", va="top")
#



# ax.set_ylim([-2500, 1700])
# ax.set_xlim([0, 28000])
ax.set_xlabel("velocity (m/s)")
ax.set_ylabel("elevation relative to sea level (m)")


# fig.savefig("velocity_model_and_arrays.png")
fig.show()