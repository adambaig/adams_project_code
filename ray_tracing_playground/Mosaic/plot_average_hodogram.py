import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from nmxseis.model.hodogram import Hodogram, LinearArrayHodograms
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from nmxseis.numerics.geodesy import EPSGReprojector
from obspy import read, read_inventory

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

from read_inputs import read_velocity_model

hodo_scale = 100

base_dir = Path("Mosaic")
STATION_XML = base_dir.joinpath("updated_station.xml")
SASK_UTM_EPSG = 26913
inv = read_inventory(STATION_XML)

pick_error = {Phase.P: 0.010, Phase.S: 0.020}
pick_errors = {
    (nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
    (nsl, ph): 1000 * pick_error[ph]
    for nsl in NSL.iter_inv(inv)
    for ph in [Phase.P, Phase.S]
}

velocity_model = read_velocity_model(
    file_path=Path("Mosaic").joinpath("metric_velocity_model.json")
)

reproject = EPSGReprojector(SASK_UTM_EPSG)

station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
nsls = list(station_enus.keys())

pick_files = list(base_dir.joinpath("picks").glob("*.picks"))
events = {}

array_16_enus = {
    station: enu for station, enu in station_enus.items() if station.sta[:2] == "16"
}


for pick_file in pick_files:

    pick_set = PickSet.load(pick_file)
    event_id, timestamp = pick_file.stem.split("_")
    seed_file = list(base_dir.joinpath("event_seeds_12z").glob(f"{event_id}*.mseed"))[0]
    stream = read(seed_file).rotate("->ZNE", inventory=inv)
    windows = Hodogram.get_windows_around_picks(pick_set, 0.0005, 0.0020)
    hodograms = Hodogram.measure_around_windows(stream, windows)
    p_16_hodograms = {
        (nsl, ph): h
        for (nsl, ph), h in hodograms.items()
        if ph.is_P and nsl.sta[:2] == "16"
    }
    p_16_windows = {
        (nsl, ph): h
        for (nsl, ph), h in windows.items()
        if ph.is_P and nsl.sta[:2] == "16"
    }
    array_hodograms = LinearArrayHodograms.from_individual_hodograms(
        p_16_hodograms, array_16_enus
    )
    array_hodograms_from_stream = LinearArrayHodograms.from_windows_and_stream(
        p_16_windows, stream, array_16_enus
    )

    fig, (ax_p, ax_d) = plt.subplots(1, 2, sharey=True)

    for nsl in nsls:
        ax_p.plot(*station_enus[nsl][:2], "v", color="w", mec="0.2", ms=10)
        ax_d.plot(*station_enus[nsl][1:][::-1], ">", color="w", mec="0.2", ms=10)

    for (nsl, ph), hg in hodograms.items():
        if ph.is_S:
            continue
        uv = hg.unit_vector_enu
        ax_p.plot(
            station_enus[nsl][0] + hodo_scale * uv[0] * np.array([1, -1]),
            station_enus[nsl][1] + hodo_scale * uv[1] * np.array([1, -1]),
            lw=2,
            color="k",
            alpha=hg.linearity if ph.is_P else hg.planarity,
        )
        ax_d.plot(
            station_enus[nsl][2] + hodo_scale * uv[2] * np.array([1, -1]),
            station_enus[nsl][1] + hodo_scale * uv[1] * np.array([1, -1]),
            lw=2,
            color="k",
            alpha=hg.linearity if ph.is_P else hg.planarity,
        )
    top_nsl = NSL.parse("XW.1601.00")
    ax_p.plot(
        station_enus[top_nsl][0]
        + hodo_scale * array_hodograms.unit_vector_en[0] * np.array([1, -1]),
        station_enus[top_nsl][1]
        + hodo_scale * array_hodograms.unit_vector_en[1] * np.array([1, -1]),
        lw=3,
        color="orangered",
        zorder=3,
    )
    ax_p.plot(
        station_enus[top_nsl][0]
        + hodo_scale
        * array_hodograms_from_stream.unit_vector_en[0]
        * np.array([1, -1]),
        station_enus[top_nsl][1]
        + hodo_scale
        * array_hodograms_from_stream.unit_vector_en[1]
        * np.array([1, -1]),
        lw=3,
        color="deepskyblue",
        zorder=4,
    )
    for ax in (ax_p, ax_d):
        ax.set_aspect("equal")
        gray_background_with_grid(ax, grid_spacing=100)

    ax_d.invert_xaxis()
    fig.savefig(base_dir.joinpath("image_avg_hodogram", f"{event_id}.png"))
    plt.close(fig)
