import json
import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from nmxseis.model.hodogram import Hodogram, VerticalArrayHodogramCollection
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from nmxseis.model.station_groups import VerticalArray
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from nmxseis.interact.stationxml import ENUStreamMapper
from obspy import read, read_inventory, UTCDateTime as UTC

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")

from generalPlots import gray_background_with_grid

from read_inputs import read_velocity_model

hodo_scale = 100

base_dir = Path("Mosaic").joinpath("synthetics")
STATION_XML = base_dir.joinpath("synth_station.xml")
SASK_UTM_EPSG = 26913
inv = read_inventory(STATION_XML)

pick_error = {Phase.P: 0.0010, Phase.S: 0.0020}
pick_errors = {
    (nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
    (nsl, ph): 1000 * pick_error[ph]
    for nsl in NSL.iter_inv(inv)
    for ph in [Phase.P, Phase.S]
}

velocity_model = read_velocity_model(
    file_path=Path("Mosaic").joinpath("metric_velocity_model.json")
)

reproject = EPSGReprojector(SASK_UTM_EPSG)


def raytracer(source_enu, receiver_enu, phase):
    return Raypath.isotropic_ray_trace(source_enu, receiver_enu, velocity_model, phase)


def tt_function(source_enu, receiver_enu, phase):
    return raytracer(source_enu, receiver_enu, phase).traveltime_sec


station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
nsls = list(station_enus.keys())
locator = OptimizingRelocator(
    tt_function,
    station_enus,
    epsg=SASK_UTM_EPSG,
    raytracer=raytracer,
    hodogram_weight=0.1,
)
pick_files = list(base_dir.joinpath("picks").glob("*.picks"))
events = {}
north_array_enus = {k: v for k, v in station_enus.items() if v[1] > 5612000}
north_array = VerticalArray(north_array_enus.keys(), inv=inv, reprojector=reproject)
enz_mapper = ENUStreamMapper(inv)
for pick_file in pick_files[:1]:
    pick_set = PickSet.load(pick_file)
    event_id, timestamp = pick_file.stem.split("_")
    seed_file = base_dir.joinpath("mseed", f"{timestamp.replace('.','_')}.mseed")
    stream = read(seed_file).rotate("->ZNE", inventory=inv)

    windows = Hodogram.get_windows_around_picks(pick_set, 0.0005, 0.0020)
    hodograms = Hodogram.measure_around_windows(stream, windows)
    p_hodograms_north = {
        (nsl, ph): h
        for (nsl, ph), h in hodograms.items()
        if ph.is_P and nsl in north_array_enus
    }
    p_windows_north = {
        (nsl, ph): h
        for (nsl, ph), h in windows.items()
        if ph.is_P and nsl in north_array_enus
    }

    array_hodograms = VerticalArrayHodogramCollection.from_windows_and_stream(
        p_windows_north, stream, north_array
    )
    starting_rut = north_array.get_starting_rut(pick_set, velocity_model)

    array_hodograms.resolve_180_ambiguity(test_rz=starting_rut[:2])

    starting_enut = np.array(
        [
            np.sin(array_hodograms.array_trend_deg) * starting_rut[0]
            + north_array.en[0],
            np.cos(array_hodograms.array_trend_deg) * starting_rut[0]
            + north_array.en[1],
            *starting_rut[1:],
        ]
    )

    reloc_result = locator.relocate(
        pick_set,
        pick_errors=pick_errors,
        hodograms=hodograms,
        initial_enut=starting_enut,
        locator_options={"maxfev": 2000, "maxiter": 2000},
    )
    location = reloc_result.origin.enu(reproject)

    min_pick, rescaled_picks, recentered_stations = locator._rescale_inputs(
        pick_set,
        station_enus,
    )
    local_enut_ms = locator._translate_to_local_enut(
        location, reloc_result.origin.time, min_pick
    )

    fig, (ax_p, ax_d) = plt.subplots(1, 2, sharey=True)

    for nsl in nsls:
        ax_p.plot(*station_enus[nsl][:2], "v", color="w", mec="0.2", ms=10)
        ax_d.plot(*station_enus[nsl][1:][::-1], ">", color="w", mec="0.2", ms=10)
    ax_p.plot(*location[:2], "o", color="gold", mec="0.2", zorder=5)
    ax_d.plot(*location[1:][::-1], "o", color="gold", mec="0.2", zorder=5)
    for (nsl, ph), hg in hodograms.items():
        if ph.is_S:
            continue
        uv = hg.unit_vector_enu
        ax_p.plot(
            station_enus[nsl][0] + hodo_scale * uv[0] * np.array([1, -1]),
            station_enus[nsl][1] + hodo_scale * uv[1] * np.array([1, -1]),
            lw=2,
            color="k",
            alpha=hg.linearity if ph.is_P else hg.planarity,
        )
        ax_d.plot(
            station_enus[nsl][2] + hodo_scale * uv[2] * np.array([1, -1]),
            station_enus[nsl][1] + hodo_scale * uv[1] * np.array([1, -1]),
            lw=2,
            color="k",
            alpha=hg.linearity if ph.is_P else hg.planarity,
        )

    for ax in (ax_p, ax_d):
        ax.set_aspect("equal")
        gray_background_with_grid(ax, grid_spacing=1000)

    ax_d.invert_xaxis()
    fig.savefig(base_dir.joinpath("location_image", f"{event_id}.png"))
    plt.close(fig)
    events[event_id] = reloc_result.to_json_serializable()

# with open('rotated_locations_new_start_loc.json', 'w') as f:
#     json.dump(events, f)
