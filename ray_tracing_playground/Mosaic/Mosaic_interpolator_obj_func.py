import json
from itertools import product
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from nmxseis.model.hodogram import Hodogram, VerticalArray, VerticalArrayHodogramCollection
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics._internal_util import dot_last_axis
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from nmxseis.numerics.relocation.optimizing_relocator import (
	OptimizingRelocatorHint as Hint,
)
from nmxseis.numerics.velocity_model.vm_1d import (
	VMLayer1D,
	VelocityModel1D,
	VM1DIncomingEnuInterpolator,
	VM1DTTInterpolator,
	INTERP_PHASE_ORDER
)
from obspy import read, read_inventory, UTCDateTime

base_dir = Path()


def read_velocity_model(file_path=base_dir.joinpath("metric_velocity_model.json")):
	with open(file_path) as f:
		velocity_model_json = json.load(f)
	layers = []
	for layer in velocity_model_json:
		layers.append(
			VMLayer1D(
				vp=layer["vp"],
				vs=layer["vs"],
				rho=layer["rho"],
				top=layer["top"] if "top" in layer else np.inf,
			)
		)
	return VelocityModel1D(layers)


interp_folder = base_dir.joinpath("interpolators")
STATION_XML = base_dir.joinpath("updated_station.xml")
MOSAIC_EPSG = 32613
inv = read_inventory(STATION_XML)

with open(base_dir.joinpath('locations_from_interp.json')) as f:
	catalog = json.load(f)

trapped_events = {k: v for k, v in catalog.items() if 351.13 < v['origin']['depth_m'] < 351.15}

tt_fn = interp_folder.joinpath("interp_tt_mosaic.npz")
hodo_fn = interp_folder.joinpath("interp_hodo_mosaic.npz")

tt_interp = VM1DTTInterpolator.load_npz(tt_fn)
hodo_interp = VM1DIncomingEnuInterpolator.load_npz(hodo_fn)
velocity_model = read_velocity_model(
	file_path=base_dir.joinpath("metric_velocity_model.json")
)
pick_error = {Phase.P: 0.0010, Phase.S: 0.0020}
pick_errors = {
	(nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
	(nsl, ph): 1000 * pick_error[ph]
	for nsl in NSL.iter_inv(inv)
	for ph in [Phase.P, Phase.S]
}
reproject = EPSGReprojector(MOSAIC_EPSG)
station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
nsls = list(station_enus.keys())
pick_files = base_dir.joinpath("picks").glob("*.picks")
seed_files = base_dir.joinpath("event_seeds_12z").glob("*.mseed")
locator = OptimizingRelocator(
	station_enus,
	MOSAIC_EPSG,
	tt_interpolator=tt_interp,
	ray_interpolator=hodo_interp
)
array_16_enus = {
	station: enu for station, enu in station_enus.items() if station.sta[:2] == "16"
}
array_16 = VerticalArray(nsls=array_16_enus.keys(), inv=inv, reprojector=reproject)

fmin = 40
fmax = 400
doFilter = True
prePick = 0.001
postPick = 0.01
minThresh = 0.8

for ev_id, event in trapped_events.items():
	pick_set = PickSet.load([pf for pf in pick_files if pf.name.startswith(ev_id)][0])
	stream = read([sd for sd in seed_files if sd.name.startswith(ev_id)][0]).rotate("->ZNE",
	                                                                                inventory=inv)
	stream.filter("bandpass", freqmin=fmin, freqmax=fmax)
	windows = Hodogram.get_windows_around_picks(pick_set, prePick, postPick)
	hodograms = Hodogram.measure_around_windows(stream, windows)
	p_16_hodograms = {
		(nsl, ph): h
		for (nsl, ph), h in hodograms.items()
		if ph.is_P and nsl.sta[:2] == "16"
		# and h.linearity>minThresh
	}
	p_16_windows = {
		(nsl, ph): h
		for (nsl, ph), h in windows.items()
		if ph.is_P and nsl.sta[:2] == "16"
	}
	array_hodograms = VerticalArrayHodogramCollection.from_windows_and_stream(
		p_16_windows,
		stream,
		array_16,
	)
	starting_rut = array_16.get_starting_rut(pick_set, velocity_model)

	array_hodograms.resolve_180_ambiguity(test_rz=starting_rut[:2])

	starting_enut = np.array(
		[
			np.sin(np.deg2rad(array_hodograms.array_trend_deg)) * starting_rut[0] + array_16.en[0],
			np.cos(np.deg2rad(array_hodograms.array_trend_deg)) * starting_rut[0] + array_16.en[1],
			*starting_rut[1:],
		]
	)
	hint = Hint(
		hodograms=p_16_hodograms,
		pick_errors=pick_errors,
		locator_options={"maxfev": 2000, "maxiter": 2000},
		initial_enut=starting_enut
	)

	nsl_order = tuple(sorted(station_enus.keys()))
	locator_options = hint.locator_options
	t_ref_sec = min(pick.time.timestamp for pick in pick_set.picks)
	sta_loc_arr = np.stack([station_enus[nsl] for nsl in nsl_order])
	enu_ref_m = sta_loc_arr.mean(axis=0)
	tt_interpolator = tt_interp.reindex(nsl_order)
	ray_interpolator = hodo_interp.reindex(nsl_order)
	nsta = len(nsl_order)
	npha = len(INTERP_PHASE_ORDER)


	def to_local(enut: np.ndarray) -> np.ndarray:
		out = enut.copy()
		out[:3] -= enu_ref_m
		# out[3] -= t_ref_sec
		out[3] *= 1000
		return out


	def to_global(enut_lms: np.ndarray) -> np.ndarray:
		out = enut_lms.copy()
		out[3] /= 1000
		out[:3] += enu_ref_m
		# out[3] += t_ref_sec
		return out


	pick_validity_arr = np.zeros((nsta, npha), dtype=bool)
	pick_sigma_arr_ms = np.full((nsta, npha), 10, dtype=np.float64)
	pick_time_arr_lms = np.zeros((nsta, npha), dtype=np.float64)

	# which hodograms are valid to be summed in the loss
	hodo_validity_arr = np.zeros((nsta, npha), dtype=bool)
	hodos_arr = np.zeros((nsta, npha, 3), dtype=np.float64)
	# ones not to blow up division on invalid values; they are masked after-the-fact
	hodo_errs_rad = np.full((nsta, npha), np.pi / 6, dtype=np.float64)

	for pick in pick_set.picks:
		nx = nsl_order.index(pick.nsl)
		px = INTERP_PHASE_ORDER.index(pick.phase)
		pick_validity_arr[nx, px] = True

		key = pick.nsl, pick.phase

		if hint.pick_errors and key in hint.pick_errors:
			pick_sigma_arr_ms[nx, px] = hint.pick_errors[key] * 1000
			pick_time_arr_lms[nx, px] = 1000 * (pick.time.timestamp - t_ref_sec)

		if hint.hodograms and key in hint.hodograms:
			hodo_validity_arr[nx, px] = True
			hodos_arr[nx, px, :] = hint.hodograms[key].unit_vector_enu
			hodo_errs_rad[nx, px] = np.radians(hint.hodograms[key].error_deg)


	def get_raytraced_arrs(src_enu: np.ndarray) -> tuple[np.ndarray, np.ndarray]:
		"""
		Gets arrays by ray-tracing the vm directly. Used when we are outside the interpolator
		grid.
		"""
		tts = np.zeros((nsta, 2))
		enus = np.zeros((nsta, 2, 3))
		for (nx, nsl), (px, pha) in product(
				enumerate(nsl_order), enumerate(INTERP_PHASE_ORDER)
		):
			ray = velocity_model.raytrace(src_enu, station_enus[nsl], pha)
			tts[nx, px] = ray.get_tt_sec()
			enus[nx, px] = ray.get_unit_incoming_enu()
		return tts, enus


	def objective(enut_ms):
		χ2 = 0.0
		src_enu = to_global(enut_ms)[:3]

		if tt_interpolator is not None:
			tt_from_src = tt_interpolator.get_tt_arr(src_enu)
			hodo_from_src = None
		else:
			tt_from_src, hodo_from_src = get_raytraced_arrs(src_enu)

		want_arrivals = 1000 * tt_from_src + enut_ms[3]
		have_arrivals = pick_time_arr_lms

		χ2 += (
				(((want_arrivals - have_arrivals) / pick_sigma_arr_ms) ** 2) * pick_validity_arr
		).sum()

		if hodo_from_src is None and ray_interpolator is not None:
			hodo_from_src = ray_interpolator.get_unit_incoming_enu(src_enu=src_enu)
		assert hodo_from_src is not None
		hodo_dot = dot_last_axis(hodo_from_src, hodos_arr)
		χ2 += (((np.arccos(hodo_dot) / hodo_errs_rad) ** 2) * hodo_validity_arr).sum()

		return χ2


	origin = EventOrigin(
		lat=event['origin']['lat'],
		lon=event['origin']['lon'],
		depth_m=event['origin']['depth_m'],
		time=UTCDateTime(event['origin']['UTC_datetime'])
	)
	enut = [*origin.enu(reproject), origin.time - min(pick_set.picks).time]
	local_enut_ms = to_local(enut)

	vector_around = np.arange(-10, 11, 0.1)
	fig, ax = plt.subplots()
	ax.plot(vector_around, [objective(np.array(local_enut_ms + [v, 0, 0, 0])) for v in vector_around])
	ax.plot(vector_around, [objective(np.array(local_enut_ms + [0, v, 0, 0])) for v in vector_around])
	ax.plot(vector_around, [objective(np.array(local_enut_ms + [0, 0, v, 0])) for v in vector_around])
	ax.plot(vector_around, [objective(np.array(local_enut_ms + [0, 0, 0, v])) for v in vector_around])
