import numpy as np
from scipy.integrate import solve_ivp

def two_point_ray_tracing(x0, y0, z0, x1, y1, z1, vmodel, ds=0.01):
    """
    Performs two-point ray tracing for seismic waves in a 3D velocity model.

    Parameters:
    - x0, y0, z0: coordinates of the starting point of the ray
    - x1, y1, z1: coordinates of the ending point of the ray
    - vmodel: 3D array of velocity values (m/s) at each grid point
    - ds: step size for numerical integration

    Returns:
    - x, y, z: arrays of coordinates of the ray
    - t: array of travel times along the ray
    """
    # Define the initial conditions for the ODE solver
    y0 = np.array([x0, y0, z0, x1 - x0, y1 - y0, z1 - z0, 0.0])

    # Define the ODE system to solve
    def ode_system(t, y):
        x, y, z, u, v, w, s = y
        vxyz = np.interp([x, y, z], grid, vmodel)  # interpolate velocity values from the grid
        c = np.sqrt(np.sum(vxyz**2))
        return [u/c, v/c, w/c, vxyz[0], vxyz[1], vxyz[2], 1.0]

    # Define the event function to stop the ODE solver when the ray reaches the ending point
    def event_function(t, y):
        return np.linalg.norm(y[:3] - np.array([x1, y1, z1]))

    event_function.terminal = True

    # Solve the ODE system with the event function
    solution = solve_ivp(ode_system, (0, 1e6), y0, events=event_function, dense_output=True, rtol=1e-6, atol=1e-9)

    # Interpolate the solution to get the ray coordinates and travel times
    t_eval = np.arange(0, solution.t[-1], ds)
    y_eval = solution.sol(t_eval)
    x, y, z,px,py,pz = y_eval[:3]
    t = np.cumsum(y_eval[6] * np.gradient(t_eval))

    return x, y, z, t


if __name__ == "__main__":
