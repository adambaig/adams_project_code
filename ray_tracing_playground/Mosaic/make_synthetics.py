import sys
from pathlib import Path

from nmxseis.model.source_radius import Madariaga
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.simulation import generate_random_dc_moment_tensor
from numpy.random import seed
import numpy as np
from obspy import read_inventory

from read_inputs import (
    read_velocity_model,
    read_old_catalog,
    homogeneous_velocity_model,
)

sys.path.append(r"C:\Users\adambaig\Project\Fervo")

from forward import SimulationArray, SimulationSuite, SimulationSource, NoiseLevel

seed(1138)
STATION_XML = Path("updated_station.xml")
SASK_UTM_EPSG = 26913
MOMENT_TENSOR = generate_random_dc_moment_tensor()
inventory = read_inventory(STATION_XML)

center_enu = np.array([7.20978514e05, 5.61239838e06, -2.2800000e02])

reproject = EPSGReprojector(SASK_UTM_EPSG)

array = SimulationArray.from_inv(STATION_XML, reproject)
velocity_model = homogeneous_velocity_model()
old_events = read_old_catalog()

simulation_sources = [
    SimulationSource(
        moment_tensor=MOMENT_TENSOR,
        magnitude=-2,
        stress_drop=1e5,
        enu=center_enu
        + np.array(
            [
                100 * np.cos(theta := np.pi * 2 * np.random.rand()),
                100 * np.sin(theta),
                0,
            ]
        ),
    )
    for event in list(old_events.values())[:10]
]

simulation_suite = SimulationSuite(
    simulation_sources=simulation_sources,
    simulation_array=array,
    noise_level=NoiseLevel(m_per_s=1e-9),
    velocity_model=velocity_model,
    sample_rate_hz=4000,
    trace_length_s=1,
    semblance_weighted_stack_factor=1,
    radius_model=Madariaga,
    ch_list=["E", "N", "Z"],
)

simulation_suite.run_simulations(Path("homogeneous_synthetics"))
