from pathlib import Path

from nmxseis.numerics.velocity_model.vm_1d import VM1DTTInterpolator
import numpy as np


from read_inputs import get_station_enus, read_velocity_model


interp_dir = Path('interpolators')
velocity_model = read_velocity_model(Path('metric_velocity_model.json'))
station_enus = get_station_enus()
nsl_order = sorted(tuple(station_enus.keys()))
enut =np.array([ 2.88753909e+05, 5.61826984e+06, -1.97333129e+02, 1.67781907e+09])

interpolator = (
        VM1DTTInterpolator.load_from_npz_dir(interp_dir, nsl_order)
    )

interpolator