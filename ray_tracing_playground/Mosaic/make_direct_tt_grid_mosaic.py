from pathlib import Path

import numpy as np

from nmxseis.numerics.velocity_model.vm_1d import VM1DInterpolator


from read_inputs import read_velocity_model, get_station_enus, BASE_DIR

velocity_model = read_velocity_model()
station_enus = get_station_enus()

station_elevations = [v[2] for v in station_enus.values()]

elevations = np.hstack([-2000,
                        np.arange(min(station_elevations)-150,
                                  max(station_elevations)+151, 15),
                        550])



radial_grid = np.hstack([np.arange(0, 400, 10), np.arange(400, 1000, 100), np.arange(1000, 11000, 1000)])
out_json = BASE_DIR.joinpath('tt_ray_grid.json')

VM1DInterpolator.make_json_grids(
	velocity_model,
	station_enus,
	radial_grid,
	elevations,
	out_path=out_json,
	sin_incoming_too=True
)

