import json
from pathlib import Path

from nmxseis.model.hodogram import Hodogram, VerticalArrayHodogramCollection, _VerticalArray
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator, OptimizingRelocatorHint as Hint
import numpy as np
from obspy import read, read_inventory

from read_inputs import read_velocity_model

hodo_scale = 100

base_dir = Path("Mosaic")
STATION_XML = base_dir.joinpath("updated_station.xml")
SASK_UTM_EPSG = 26913
inv = read_inventory(STATION_XML)

pick_error = {Phase.P: 0.0010, Phase.S: 0.0020}
pick_errors = {
    (nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(inv) for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
    (nsl, ph): 1000 * pick_error[ph]
    for nsl in NSL.iter_inv(inv)
    for ph in [Phase.P, Phase.S]
}

velocity_model = read_velocity_model(
    file_path=Path("Mosaic").joinpath("metric_velocity_model.json")
)
reproject = EPSGReprojector(SASK_UTM_EPSG)
station_enus = reproject.get_station_enus(inv, NSL.iter_inv(inv))
array_16_enus = {
    station: enu for station, enu in station_enus.items() if station.sta[:2] == "16"
}
array_16 = _VerticalArray(nsls=array_16_enus.keys(), inv=inv, reprojector=reproject)


nsls = list(station_enus.keys())
locator = OptimizingRelocator(
	station_enus,
	SASK_UTM_EPSG,
	tt_provider=velocity_model,
	incoming_ray_enu_provider=velocity_model
)
pick_files = list(base_dir.joinpath("picks").glob("*.picks"))
events = {}
prePick=0.001
postPick=0.01
minThresh=0.8
for pick_file in pick_files:
    pick_set = PickSet.load(pick_file)
    event_id, timestamp = pick_file.stem.split("_")
    print(event_id)
    seed_file = list(base_dir.joinpath("event_seeds_12z").glob(f"{event_id}*.mseed"))[0]
    stream = read(seed_file).rotate("->ZNE", inventory=inv).filter('bandpass', freqmin=40, freqmax=400)

    windows = Hodogram.get_windows_around_picks(pick_set, prePick, postPick)
    hodograms = Hodogram.measure_around_windows(stream, windows)
    p_16_hodograms = {
        (nsl, ph): h
        for (nsl, ph), h in hodograms.items()
        if ph.is_P and nsl.sta[:2] == "16"
        # and h.linearity>minThresh
    }
    p_16_windows = {
        (nsl, ph): h
        for (nsl, ph), h in windows.items()
        if ph.is_P and nsl.sta[:2] == "16"
    }

    array_hodograms = VerticalArrayHodogramCollection.from_windows_and_stream(
        p_16_windows,
        stream,
        array_16,
    )
    starting_rut = array_16.get_starting_rut(pick_set, velocity_model)

    array_hodograms.resolve_180_ambiguity(test_rz=starting_rut[:2])

    starting_enut = np.array(
        [
            np.sin(array_hodograms.array_trend_deg) * starting_rut[0] + array_16.en[0],
            np.cos(array_hodograms.array_trend_deg) * starting_rut[0] + array_16.en[1],
            *starting_rut[1:],
        ]
    )

    reloc_result = locator.relocate(
        pick_set,
        Hint(
            hodograms=p_16_hodograms,
            pick_errors=pick_errors,
            locator_options={"maxfev": 2000, "maxiter": 2000},
            initial_enut=starting_enut
        ),
    )
    location = reloc_result.origin.enu(reproject)

    min_pick, rescaled_picks, recentered_stations = locator._rescale_inputs(
        pick_set,
        station_enus,
    )
    local_enut_ms = locator._translate_to_local_enut(
        location, reloc_result.origin.time, min_pick
    )
    events[event_id] = reloc_result.to_json_serializable()

# with open('rotated_locations_new_params.json','w') as f:
#     json.dump(events, f)
