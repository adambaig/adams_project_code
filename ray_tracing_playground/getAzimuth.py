import numpy as np
import os
from obspy import read,Stream,read_inventory,UTCDateTime
import pandas as pd
import matplotlib.pyplot as plt
import math
import sys

from nmxseis.model.phase import Phase

sys.path.append('/Users/bwitten/NMX/sms_moment_tensor/src/sms_moment_tensor')
from MT_math import trend_plunge_to_unit_vector

R2D=180/np.pi

def unit_vector_to_trend_plunge(u):
    if u[2] > 0:
        u = -u
    trend = np.arctan2(u[0], u[1]) * R2D
    plunge = np.arctan(-u[2] / np.sqrt(u[0] * u[0] + u[1] * u[1])) * R2D
    return [trend % 360, plunge]

def waveform_pca(waveform):
    """
    calculates the two- or three-dimensional principal components of a three-component waveform
    returned eigenvectors are in the order or "E", "N", "U" with missing keys excluded
    Args:
        waveform: a three-component waveform packed as a dictionary with keys "E", "N", and "U"
    Returns
        eigenvalues in increasing order
        eigenvectors in a 3x3 matrix with the columns corresponding to the order of the eigenvalues
    """
    wave_mat =  np.array([waveform[c] - np.mean(waveform[c]) for c in ["E", "N", "U"] if c in waveform])
    eig_vals, eig_vecs = np.linalg.eig(wave_mat @ wave_mat.T)
    i_sort = np.argsort(eig_vals)
    return eig_vals[i_sort], eig_vecs[:, i_sort]

def calc_hodogram(waveform, phase):
    i_vector = 2 if phase.is_P else 0
    i_vector_2d = 1 if phase.is_P else 0
    eig_vals, eig_vecs = waveform_pca(waveform)
    eig_vals_2d, eig_vecs_2d = waveform_pca({k:v for k,v in waveform.items() if k~='U'})
    linearity = 1 - eig_vals[1] / eig_vals[2] # Montalbetti and Kanasewich, 1970
    planarity = 1 - eig_vals[1] / eig_vals[0] # Vidale, 1986
    trend, plunge = unit_vector_to_trend_plunge(eig_vecs[:, i_vector])
    linearity_2d = 1 - eig_vals_2d[0] / eig_vals_2d[1]
    trend_2d, _ = unit_vector_to_trend_plunge([*eig_vecs_2d[:, i_vector_2d], 0])
    return {
        "trend": float(trend % 360),
        "plunge": float(plunge),
        "linearity": linearity,
        "planarity": planarity
        "2D trend": trend_2d % 360,
        "2D linearity": linearity_2d,
    }



def getPAzimuth(st,picks_info,ax,ax1,prePick=0.01,postPick=0.03,linThresh=0.8):
    # print(st)
    datafull={}
    datafull['E']=[]
    datafull['N']=[]
    datafull['Z']=[]
    outHodogram={}
    dt=st[0].stats.sampling_rate
    npts=int(dt*(postPick+prePick))

    for index, row in picks_info.iterrows():
        station=row["nsl"].split('.')[1][-2::]
        # print(station)
        phase=row["phase"]
        pick=UTCDateTime(row["pick"])

        # print(station)
        if station=='1610' or phase=='S':
            continue
        # print(station)
        # print(st)
        stationSt=st.select(location=station)
        # print(station)
        if len(stationSt) != 3:
            print("Missing component")
            print(stationSt)
            continue
        stationSt.trim((pick)-prePick,(pick)+postPick)
        data={}
        for comp in ['E','N','Z']:
            tr=stationSt.select(component=comp)[0]
            data[comp]=tr.data
        hodogram=calc_hodogram(data)
        aa=trend_plunge_to_unit_vector(hodogram['trend'], hodogram['plunge'])
        # print(aa)
        # print(station,phase,hodogram['trend'],hodogram['linearity'])
        # print(pick)
        # print(row['pick'])
        if hodogram['linearity'] > linThresh:
            for comp in ['E','N','Z']:
                tr=stationSt.select(component=comp)[0]
                datafull[comp].append(tr.data[0:npts])
            outHodogram[station]=hodogram
            plotAz(ax,hodogram['trend'],color='lightgrey')
            plotDip(ax1,hodogram['plunge'],color='lightgrey')
        else:
            plotAz(ax,hodogram['trend'],color='lightgrey',linestyle ='dashed')
            plotDip(ax1,hodogram['plunge'],color='lightgrey',linestyle ='dashed')
    for comp in ['E','N','Z']:
        datafull[comp]=np.sum(datafull[comp],axis=0)
        # print(type(datafull[comp]))
        if isinstance(datafull[comp],float):
            return None
    # print(datafull)
    hodogram=calc_hodogram(datafull)
    outHodogram['event']=hodogram
    # np.sum(datafull['E'],axis=0)
    plotAz(ax,hodogram['trend'],color='k',linestyle ='dashed')
    plotDip(ax1,hodogram['plunge'],color='k',linestyle ='dashed')
        # print(tr)
    # data=[]

    return outHodogram
def plotAz(ax,angle,color='k',linestyle ='solid'):
    x=0
    y=0
    length=1
    endy = y + length * math.sin(math.radians(angle))
    endx = length * math.cos(math.radians(angle))
    ax.plot([x, endx], [y, endy],color,linestyle =linestyle)
    length=-1
    endy = y + length * math.sin(math.radians(angle))
    endx = length * math.cos(math.radians(angle))
    ax.plot([x, endx], [y, endy],color,linestyle =linestyle)

def plotDip(ax,angle,color='k',linestyle ='solid'):
    x=0
    y=0
    length=1
    endy = y + length * math.sin(math.radians(angle))
    endx = length * math.cos(math.radians(angle))
    ax.plot([x, endx], [y, endy],color,linestyle =linestyle)


def main():
    seedDir='/Users/bwitten/NMX/Mosiac/test/dataForJP/'
    jpPicks='/Users/bwitten/NMX/Mosiac/test/picks_for_ben'
    invFile='/Users/bwitten/NMX/Mosiac/test/mosaic_inventory.xml'
    inventory= read_inventory(invFile)
    seedFiles=sorted(os.listdir(seedDir))
    jppickFiles=sorted(os.listdir(jpPicks))


    fmin=40
    fmax=400
    pickcolnames = ['nsl', 'phase', 'pick']


    for ii,(f,p) in enumerate(zip(seedFiles,jppickFiles)):
        # print(p)
        # print(f)
        # fig = plt.figure()
        fig, (ax, ax2) = plt.subplots(1, 2)
        # ax = fig.add_subplot(1, 2, 1)
        circ = plt.Circle((0, 0), radius=1, edgecolor='k', facecolor='None')
        circ1 = plt.Circle((0, 0), radius=1, edgecolor='k', facecolor='None')
        ax.add_patch(circ)
        ax.set_xlim(-1,1)
        ax.set_ylim(-1,1)
        ax.axis('equal')
        ax2.add_patch(circ1)
        ax2.set_xlim(0,1)
        ax2.set_ylim(-1,1)
        # ax2.axis('equal')
        ax2.set_aspect(1)
        ax2.invert_yaxis()
        # print(f)
        # print(p)
        picks_info = pd.read_csv(jpPicks+'/'+p, names=pickcolnames)
        # print(picks_info)
        # exit()
        st=read(seedDir+f)
        # print(st)
        # print('1')
        st = st.select(station="16*")
        for tr in st.select(station="1610"):
            st.remove(tr)
        # for tr in st:
        #     station=tr.stats.station
        #     print(station)
        #     stationSt=st.select(station=station)
        #     if len(stationSt) != 3:
        #         for tr in st.select(station=station):
        #             st.remove(tr)
        # print(st)
        for tr in st:
            # print(tr.stats)
            if tr.stats.channel=='GPN':
                tr.stats.channel='GP1'
            if tr.stats.channel=='GPE':
                tr.stats.channel='GP2'
            station = tr.stats.station
            location = station[-2:]
            tr.stats.station = 'BF16'
            tr.stats.location = location
        # exit()
        # print(st)
        # exit()
        st.rotate(method="->ZNE", inventory=inventory)
        # st._rotate_to_zne(inventory,components='Z12')
        # print(st)
        # exit()
        # st = st.select(station="16*")

        st.sort()
        # st.rotate(method="->ZNE", inventory=inventory)
        # st.filter("bandpass",freqmin=fmin,freqmax=fmax)
        # print(st)
        hodogram=getPAzimuth(st,picks_info,ax,ax2)
        hodogramsAz=[]
        hodogramsDip=[]
        if hodogram is not None:
            for key in hodogram:
                if key !='event':
                    hodogramsAz.append(hodogram[key]['trend']%180)
                    hodogramsDip.append(hodogram[key]['plunge'])
            plotAz(ax,np.mean(hodogramsAz),color='r',linestyle ='dashed')
            plotDip(ax2,np.mean(hodogramsDip),color='r',linestyle ='dashed')
            plt.show()
            # exit()



        # exit()

if __name__ == "__main__":
    main()
