import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
from obspy import Stream, Trace

from nmxseis.model.basic import Window
from nmxseis.numerics._internal_utils_plotting import inject_fig_ax

E_COLOR = "deepskyblue"
N_COLOR = "orange"
Z_COLOR = "0.1"
P_COLOR = "springgreen"
SV_COLOR = "mediumorchid"
SH_COLOR = "k"


@inject_fig_ax(plt.subplots)
def plot_trace(
    trace: Trace,
    *,
    ax: plt.Axes,
    norm=None,
    trace_offset: float = 0,
    **kwargs,
):
    if norm is None:
        data = trace.data
    else:
        data = trace.data / norm
    ax.plot(
        [trace.stats.starttime + t for t in trace.times()],
        data + trace_offset,
        **kwargs,
    )


@inject_fig_ax(plt.subplots)
def plot_3C_trace(tr_3c: Stream, *, ax: plt.Axes, **kwargs):
    norm = max([max(abs(trace.data)) for trace in tr_3c])
    comp_color = {
        **{
            "Z": Z_COLOR,
            "E": E_COLOR,
            "N": N_COLOR,
            "1": E_COLOR,
            "2": N_COLOR,
            "3": Z_COLOR,
        },
        **{"P": P_COLOR, "V": SV_COLOR, "H": SH_COLOR},
    }

    def _comp_style(comp):
        return "--" if comp in "123" else "-"

    for i_tr, trace in enumerate(tr_3c):
        comp = trace.stats.channel[-1]
        plot_trace(
            trace,
            ax=ax,
            norm=norm,
            **kwargs,
            linestyle=_comp_style(comp),
            color=comp_color[comp],
            trace_offset=i_tr,
        )


def add_window_to_trace(window: Window, ax, **kwargs):
    ylim = ax.get_ylim()
    window_rect = Rectangle(
        (window[0], ylim[0]),
        window[1] - window[0],
        ylim[1] - ylim[0],
        alpha=0.2,
        color="lightblue",
        **kwargs,
    )
    ax.add_patch(window_rect)


@inject_fig_ax(plt.subplots)
def plot_hodogram(
    tr_3c: Stream, window: Window, ax: plt.Axes, directions="EN", **kwargs
):
    ax.set_aspect("equal")
    windowed_data = tr_3c.copy().trim(starttime=window[0], endtime=window[1])
    x_data = windowed_data.select(channel=f"??{directions[0]}")[0]
    y_data = windowed_data.select(channel=f"??{directions[1]}")[0]
    ax.plot(x_data.data, y_data.data, **kwargs)
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    max_lim = max(abs(np.array([*xlim, *ylim])))
    ax.plot([-max_lim, max_lim], [0, 0], "k:")
    ax.plot([0, 0], [-max_lim, max_lim], "k:")
    ax.set_xlim([-max_lim, max_lim])
    ax.set_ylim([-max_lim, max_lim])
    del windowed_data
