import os

from numpy import array, average
from numpy import pi as π
from numpy.linalg import norm
from obspy import read_inventory, Stream
import pyproj


import matplotlib.pyplot as plt

from realtime_source_parameters.engine import rotate_to_raypath

from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.formulas import moment_to_mw
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.simulation.event_waveform import simulate_waveforms_for_array
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

from nmxseis.numerics.source_parameters import _rotate_stream_to_p_rays


INV_DIR = "synthetic_Paramount_2_28_example"
EXPLOSION = MomentTensor.from_vector(array([1, 1, 1, 0, 0, 0]))
EVENT_LOCATION = {
    "depth": 2.1,
    "time": 1637615497.695214,
    "latitude": 54.401317823038646,
    "longitude": -116.93805538849605,
}
MW = 0
CORNER_FREQUENCIES = {Phase.P: 30, Phase.S: 20}
VELOCITY_MODEL = VelocityModel1D([VMLayer1D(rho=2500, vp=5000, vs=3000)])

INV = read_inventory(os.path.join(INV_DIR, "synthetic_sta.xml"))
reproject = pyproj.Proj("epsg:26911")
easting, northing = reproject(EVENT_LOCATION["longitude"], EVENT_LOCATION["latitude"])
event_enu = array([easting, northing, -1000 * EVENT_LOCATION["depth"]])


stream: Stream
rays: dict[[NSL, Phase], Raypath]
mw_estimates: dict[str, float]


stream, rays = simulate_waveforms_for_array(
    EXPLOSION,
    MW,
    CORNER_FREQUENCIES,
    event_enu,
    INV,
    VELOCITY_MODEL,
    reproject,
    nt=8000,
    dt=0.00025,
    Qp=1000,
    Qs=1000,
    noise_rms=0,
    ch_prefix="CP",
    units="V",
)

stream.remove_response(inventory=INV, output="DISP").detrend("linear")

stream.plot()

rotated_stream = _rotate_stream_to_p_rays(
    stream, {nsl: ray for (nsl, ph), ray in rays.items() if ph.is_P}, INV
)
vp = VELOCITY_MODEL[0].vp
rho = VELOCITY_MODEL[0].rho
mw_estimates = {}

for trace in rotated_stream.select(channel="??P"):
    station = INV.get_coordinates(trace.get_id().replace("CPP", "CPZ"))
    easting, northing = reproject(station["longitude"], station["latitude"])
    station_enu = array([easting, northing, station["elevation"]])
    distance = norm(station_enu - event_enu)
    spectra = MotionSpectrum.from_dis_trace(trace).smooth(1, 1000, 33)
    moment = 4 * π * rho * vp**3 * average(spectra.dis[:10]) * distance
    mw_estimates[trace.get_id()] = moment_to_mw(moment)
