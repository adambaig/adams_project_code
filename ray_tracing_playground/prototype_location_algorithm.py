from collections import defaultdict
import json
from pathlib import Path
import random

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from obspy import read, read_inventory, UTCDateTime

from nmxseis.interact.basic import PickSet
from nmxseis.model.basic import Window
from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

from optimizers import Locator

from plotting import plot_3C_trace, add_window_to_trace, plot_hodogram

cscale = cm.get_cmap("magma")


base_dir = Path("braskem")
with open(base_dir.joinpath("velocity_model.json")) as f:
    velocity_model_json = json.load(f)
layers = []
for layer in velocity_model_json:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )

velocity_model = VelocityModel1D(layers)

inv = read_inventory(base_dir.joinpath("FBK_Full.xml"))
reproject = EPSGReprojector(event_json["epsg"])


def raytracer(source_enu, receiver_enu, phase):
    return Raypath.isotropic_ray_trace(source_enu, receiver_enu, velocity_model, phase)


def tt_function(source_enu, receiver_enu, phase):
    return raytracer(source_enu, receiver_enu, phase).traveltime_sec


with open(base_dir.joinpath("event_locations", f"{event_id}.json")) as f:
    event_json = json.load(f)

average_station_location = np.average(list(station_enus.values()), axis=0)
locator = Locator(raytracer, tt_function, average_station_location)


source = EventOrigin(
    lat=event_json["latitude"],
    lon=event_json["longitude"],
    depth_m=event_json["depth_m"],
    time=event_json["time"],
)
source_enu = source.enu(reproject)
pick_set = PickSet.load(base_dir.joinpath("synthetic_picks", f"{event_id}.csv"))
br_pick_set = PickSet(picks=[p for p in pick_set.picks if p.nsl.net == "BR"])
hodograms: dict[(NSL, Phase), Hodogram] = defaultdict()

# stream = read(base_dir.joinpath("synthetic_waveforms", f"{event_id}.mseed"))
# stream_3c = NSL.partition_stream(stream)

station_enus = reproject.get_station_enus(inv, stream_3c.keys())
rays: dict[(NSL, Phase), Raypath] = defaultdict()


fig, ((ax_h, ax_t), (ax_c, ax_dum)) = plt.subplots(
    2, 2, sharex=True, sharey=True, figsize=[12, 12]
)
for ax in ax_h, ax_t, ax_c:
    ax.set_aspect("equal")
    ax.plot(*source_enu[:2], "*", color="orangered", mec="0.1", zorder=4, ms=10)
    for nsl in selected_stations:
        ax.plot(*station_enus[nsl][:2], "v", color="w", mec="0.2", ms=10)


selected_keys = random.sample(list(hodograms.keys()), 15)

selected_keys = list([])

# random_keys = [(NSL(net='BR', sta='ESM04', loc='', sta_info=None), Phase.P),
#  (NSL(net='SC', sta='MAC09', loc='00', sta_info=None), Phase.S),
#  (NSL(net='SC', sta='MAC13', loc='00', sta_info=None), Phase.P),
#  (NSL(net='SC', sta='MAC13', loc='01', sta_info=None), Phase.S),
#  (NSL(net='SC', sta='MAC14', loc='00', sta_info=None), Phase.P)]

random_hodos = {k: hodograms[k] for k in selected_keys}
random_picks = PickSet(
    picks=[p for p in pick_set.phase_picks if (p.nsl, p.phase) in selected_keys]
)

n_grid = 50

xlim = ax_h.get_xlim()
ylim = ax_h.get_ylim()
xgrid = np.linspace(*xlim, n_grid)
ygrid = np.linspace(*ylim, n_grid)
objective_field_hodo = np.zeros([len(xgrid), len(ygrid)])
objective_field_tt = np.zeros([len(xgrid), len(ygrid)])
for i_x, x in enumerate(xgrid):
    for i_y, y in enumerate(ygrid):
        for (nsl, phase), hodogram in random_hodos.items():
            test_ray = raytracer(
                np.array([x, y, source_enu[-1]]), station_enus[nsl], phase
            )
            if phase.is_P:
                objective_field_hodo[i_x, i_y] += abs(
                    hodogram.dot_product_with_ray(test_ray)
                )
            pick = [p for p in random_picks.picks if p.phase == phase and p.nsl == nsl][
                0
            ]
            objective_field_tt[i_x, i_y] += (
                source.time + test_ray.traveltime_sec - pick.time.timestamp
            ) ** 2
objective_field_hodo /= len(random_keys)
objective_field_tt /= len(random_keys)

ax_h.pcolor(xgrid, ygrid, (2 - objective_field_hodo.T), zorder=-1, shading="auto")
ax_h.set_title("Hodogram residual")
ax_t.pcolor(xgrid, ygrid, objective_field_tt.T, zorder=-1, shading="auto")
ax_t.set_title("traveltme residual")
ax_c.pcolor(
    xgrid,
    ygrid,
    (2 - objective_field_hodo.T) * objective_field_tt.T,
    zorder=-1,
    shading="auto",
)
ax_c.set_title("combined residual")
ax_dum.axis("off")


location = locator.locate_tt(random_picks, station_enus)
for ax in ax_h, ax_t, ax_c:
    ax.plot(*location[0][:2], "o", color="gold")
plt.show()


pass
