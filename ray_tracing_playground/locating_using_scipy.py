from glob import glob
import json

import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory
import pyproj as pr
from scipy.optimize import minimize

from sms_ray_modelling.raytrace import isotropic_ray_trace, get_layer

inv = read_inventory(r"Mosaic/updated_station.xml")
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:32613")

stations = {}
for net in inv:
    for station in net:
        east, north = pr.transform(
            latlon_proj, out_proj, station.longitude, station.latitude
        )
        stations[f"{net.code}.{station.code}.00"] = {
            "abs z": station.elevation,
            "abs e": east,
            "abs n": north,
        }
bf_16_e = np.average([s["abs e"] for code, s in stations.items() if "XW.16" in code])
bf_16_n = np.average([s["abs n"] for code, s in stations.items() if "XW.16" in code])

for station in stations.values():
    station["e"] = station["abs e"] - bf_16_e
    station["n"] = station["abs n"] - bf_16_n
    station["z"] = station["abs z"]


def tt_residual(rzt, picks, stations, velocity_model):
    xyz = {"e": rzt[0], "n": 0, "z": rzt[1]}
    residual = 0
    n_obs = 0
    for station, phase_pick in picks.items():
        for phase, pick in phase_pick.items():
            if phase in ["P", "S"]:
                n_obs += 1
                t_xyz = isotropic_ray_trace(
                    xyz, stations[station], velocity_model, phase
                )["traveltime"]
                residual += (t_xyz - pick + rzt[2]) ** 2
    return residual / n_obs


def get_starting_rzt(picks, stations, velocity_model):
    r_starts = []
    t_starts = []
    min_time = np.inf
    for station, station_picks in picks.items():
        if "P" in station_picks and "S" in station_picks:
            layer = get_layer(
                stations[station]["z"],
                velocity_model,
            )
            ps_separation = station_picks["S"] - station_picks["P"]
            t_starts.append(2 * station_picks["P"] - station_picks["S"])
            r_starts.append(
                ps_separation * layer["vp"] * layer["vs"] / (layer["vp"] - layer["vs"])
            )
            if station_picks["P"] < min_time:
                z_start = stations[station]["z"]
                min_time = station_picks["P"]
    return [np.average(r_starts), z_start, np.average(t_starts)]


def rescale_tt(picks):
    min_pick = min([min(v) for v in [p.values() for p in picks.values()]])
    rescaled = {}
    for station, phasepick in picks.items():
        rescaled[station] = {}
        for phase, pick in phasepick.items():
            rescaled[station][phase] = pick - min_pick
    return rescaled


pick_file = r"Mosaic\picks\00085_20210923.150241.056999.picks"
picks = {}
with open(pick_file) as f:
    pick_lines = f.readlines()
    for line in pick_lines:
        net_stat_loc, phase, time_str = line.split(",")
        pick_time = float(time_str)
        if net_stat_loc not in picks:
            picks[net_stat_loc] = {}
        picks[net_stat_loc][phase] = pick_time

with open("Mosaic\metric_velocity_model.json") as f:
    velocity_model = json.load(f)


scaled_tt = rescale_tt(picks)
start_rzt = get_starting_rzt(scaled_tt, stations, velocity_model)

minimizer = minimize(
    tt_residual,
    start_rzt,
    args=(scaled_tt, stations, velocity_model),
    method="Nelder-Mead",
)
final_rzt = minimizer.x


t_min = np.sqrt(tt_residual(final_rzt, scaled_tt, stations, velocity_model))


r_values = np.linspace(0, 500, 100)
z_values = np.linspace(-600, 0, 120)
t_values = np.linspace(-0.1, 0, 104)
residual_map_rz = np.zeros([len(r_values), len(z_values)])
residual_map_rt = np.zeros([len(r_values), len(t_values)])

for i_r, r in enumerate(r_values):
    for i_z, z in enumerate(z_values):
        residual_map_rz[i_r, i_z] = np.sqrt(
            tt_residual([r, z, final_rzt[2]], scaled_tt, stations, velocity_model)
        )

for i_r, r in enumerate(r_values):
    for i_t, t in enumerate(t_values):
        residual_map_rt[i_r, i_t] = np.sqrt(
            tt_residual([r, final_rzt[1], t], scaled_tt, stations, velocity_model)
        )


np.amax(residual_map_rz)
np.amin(residual_map_rz)
fig, (ax_rz, ax_rt) = plt.subplots(1, 2, figsize=[12, 5])
t_min
ax_rz.pcolor(r_values, z_values, residual_map_rz.T, vmin=t_min, vmax=0.1)
ax_rz.plot(final_rzt[0], final_rzt[1], "wo", markeredgecolor="k")
ax_rz.set_xlabel("horizontal_distance (m)")
ax_rz.set_ylabel("elevation relative to sealevel (m)")


ax_rt.pcolor(r_values, t_values, residual_map_rt.T, vmin=t_min, vmax=0.1)
ax_rt.plot(final_rzt[0], final_rzt[2], "wo", markeredgecolor="k")
ax_rt.set_xlabel("horizontal_distance (m)")
ax_rt.set_ylabel("origin time relative to earliest pick (s)")


fig.savefig("trial_event.png")


r, t, z = minimizer.x
locations = {}

for pick_file in glob(r"Mosaic\picks\*.picks"):
    picks = {}
    with open(pick_file) as f:
        pick_lines = f.readlines()
        for line in pick_lines:
            net_stat_loc, phase, time_str = line.split(",")
            pick_time = float(time_str)
            if net_stat_loc not in picks:
                picks[net_stat_loc] = {}
            picks[net_stat_loc][phase] = pick_time
    scaled_tt = rescale_tt(picks)
    start_rzt = get_starting_rzt(scaled_tt, stations, velocity_model)
    minimizer = minimize(
        tt_residual,
        start_rzt,
        args=(scaled_tt, stations, velocity_model),
        method="Nelder-Mead",
    )
    r, z, t = minimizer.x
    locations[pick_file] = {"r": r, "t": t, "z": z}

with open("Mosaic//simplex_locations.json", "w") as f:
    json.dump(locations, f)
