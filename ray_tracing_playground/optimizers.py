from __future__ import annotations


from dataclasses import dataclass
from typing import Callable

import numpy as np
from obspy import UTCDateTime as UTC
from scipy.optimize import minimize, OptimizeResult

from nmxseis.interact.basic import PickSet
from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.numerics.ray_modeling import Raypath

TraveltimeFunc = Callable[[np.ndarray, np.ndarray, Phase], float]
RayTracer = Callable[[np.ndarray, np.ndarray, Phase], Raypath]


@dataclass
class Locator:

    raytracer: RayTracer
    tt_function: TraveltimeFunc
    enu_0: np.ndarray  # some location in the middle of the array
    hodo_weight: float = 1
    # logger: Logger | None = None

    def hodogram_residual(
        self,
        enu: np.ndarray,
        hodograms: dict[[NSL, Phase], Hodogram],
        station_enus: dict[NSL, np.ndarray],
    ):
        hodo_sum = 0
        for (nsl, phase), hodogram in hodograms.items():
            weight = hodogram.linearity if phase.is_P else hodogram.planarity
            hodo_sum += weight * abs(
                np.dot(
                    self.ray_tracer(station_enus[nsl], enu, phase).unit_incoming_enu,
                    hodogram.unit_vector_enu,
                )
            )
        return 2 - hodo_sum / len(hodograms)  # ensure residual is always greater than 1

    def tt_residual_3D(
        self,
        enut: np.ndarray,
        rescaled_picks: dict[[NSL, Phase], float],
        rescaled_station_enus: dict[NSL, np.ndarray],
    ):
        residual = 0
        n_obs = len(rescaled_picks)
        for (nsl, phase), time in rescaled_picks.items():
            t_xyz = self.tt_function(enut[:-1], rescaled_station_enus[nsl], phase)
            residual += (t_xyz - time + enut[-1]) ** 2
        return residual / n_obs

    def tt_hodo_residual_3D(
        self,
        enut: np.ndarray,
        rescaled_picks: dict[[NSL, Phase], float],
        rescaled_station_enus: dict[NSL, np.ndarray],
        hodograms: dict[[NSL, Phase], Hodogram],
    ) -> float:
        return (
            self.tt_residual(
                enut, rescaled_picks, rescaled_station_enus, self.tt_function
            )
            * self.hodogram_residual(
                enut[:3], hodograms, rescaled_station_enus, self.raytracer
            )
            ** self.hodo_weight
        )

    def locate_combined(
        self,
        picks: PickSet,
        hodograms: Hodogram,
        station_enus: dict[NSL, np.ndarray],
        initial_enut: np.ndarray | None = None,
    ) -> tuple[np.ndarray, UTC, np.ndarray]:
        min_pick, rescaled_picks, rescaled_stations = self.rescale_inputs(
            picks, station_enus, self.enu_0
        )
        if initial_enut is None:
            initial_enut = self.get_starting_enut(rescaled_picks, rescaled_stations)
        minimizer = minimize(
            self.tt_hodo_residual_3D,
            initial_enut,
            args=(rescaled_picks, rescaled_stations, hodograms),
            method="Nelder-Mead",
        )
        return self.scale_output(minimizer, min_pick)

    def locate_tt(
        self,
        picks: PickSet,
        station_enus: dict[NSL, np.ndarray],
        initial_enut: np.ndarray | None = None,
    ) -> tuple[np.ndarray, UTC, np.ndarray]:
        min_pick, rescaled_picks, rescaled_stations = self.rescale_inputs(
            picks,
            station_enus,
        )
        if initial_enut is None:
            initial_enut = self.get_starting_enut(rescaled_picks, rescaled_stations)
        else:
            initial_enut[:3] -= self.enu_0
            initial_enut[3] -= min_pick.timestamp
        minimizer = minimize(
            self.tt_residual_3D,
            initial_enut,
            args=(rescaled_picks, rescaled_stations),
            method="Nelder-Mead",
        )
        return self.scale_output(minimizer, min_pick)

    def scale_output(
        self, minimizer: OptimizeResult, min_pick: UTC
    ) -> tuple[UTC, dict[[NSL, Phase], float], dict[NSL, np.ndarray]]:
        error_ellipsoid = np.zeros([4, 4])
        return (
            minimizer.x[:3] + self.enu_0,
            UTC(minimizer.x[3] + min_pick.timestamp),
            error_ellipsoid,
        )

    def rescale_inputs(
        self, picks: PickSet, station_enus: dict[NSL, np.ndarray]
    ) -> tuple[UTC, dict[[NSL, Phase], float], dict[NSL, np.ndarray]]:
        min_pick = min([p.time for p in picks.picks])
        rescaled_picks = {(p.nsl, p.phase): (p.time - min_pick) for p in picks.picks}
        rescaled_stations = {
            nsl: (enu - self.enu_0) for nsl, enu in station_enus.items()
        }
        return min_pick, rescaled_picks, rescaled_stations

    @staticmethod
    def get_starting_enut(
        rescaled_picks: dict[[NSL, Phase], float],
        rescaled_stations: dict[NSL, np.ndarray],
    ) -> np.ndarray:
        """
        very crude way to get a starting location.... origin time is the difference of the first two
        picks before the first pick, location is a weighed average of station locations, with the weights
        determined by inverse fly time
        """
        pick_times = [t for t in rescaled_picks.values()]
        crude_origin_time = -sorted(pick_times)[1]
        picked_enus, weights = zip(
            *[
                (rescaled_stations[nsl], 1 / (t - crude_origin_time))
                for (nsl, p), t in rescaled_picks.items()
            ]
        )
        crude_origin_enu = np.average(picked_enus, weights=weights, axis=0)
        return np.array([*crude_origin_enu, crude_origin_time])

    def translate_to_local_enut(
        self, global_enu: np.ndarray, absolute_origin_time: UTC, min_pick: UTC
    ) -> np.ndarray:
        local_enu = global_enu - self.enu_0
        origin_time = absolute_origin_time - min_pick
        return np.array([*local_enu, origin_time])
