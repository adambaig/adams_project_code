from datetime import datetime
import json
import os

import matplotlib.pyplot as plt

catalog = r"C:\Users\adambaig\Project\Mosaic_Esterhazy\Mosaic_catalog_01_04.csv"
with open(catalog) as f:
    _head = f.readline()
    lines = f.readlines()
    events = {}
    for line in lines:
        split_line = line.split(",")
        pick_id = split_line[2]
        events[pick_id] = {
            "datetime": datetime.strptime(split_line[3], "%Y-%m-%dT%H:%M:%S.%fZ"),
            "northing": float(split_line[11]),
            "easting": float(split_line[10]),
            "hrz_distance": float(split_line[12]),
            "elevation": 522.02 - float(split_line[13]),
            "moment magnitudes": float(split_line[16]),
            "corner frequency": float(split_line[17]),
            "static stress drop": float(split_line[18]),
        }


with open("Mosaic//simplex_locations.json") as f:
    locations = json.load(f)


fig, ax = plt.subplots()

for pick_file, location in locations.items():
    pick_id = os.path.basename(pick_file).split("_")[0].lstrip("0")
    if pick_id in events:
        ax.plot(
            events[pick_id]["hrz_distance"],
            events[pick_id]["elevation"],
            "o",
            color="firebrick",
            alpha=0.4,
        )
        ax.plot(location["r"], location["z"], "o", color="royalblue", alpha=0.4)
        ax.plot(
            [events[pick_id]["hrz_distance"], location["r"]],
            [events[pick_id]["elevation"], location["z"]],
            "0.5",
            zorder=-2,
        )
ax.set_xlabel("horizontal distance (m)")
ax.set_ylabel("elevation relative to sea level (m)")

fig.savefig("comparison_blue_is_simplex.png")
