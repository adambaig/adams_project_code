import json
from pathlib import Path

import numpy as np
from obspy import read_inventory, UTCDateTime

from nmxseis.interact.athena import AthenaClient
from nmxseis.interact.basic import PickSet
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick import Pick
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.simulation import generate_random_dc_moment_tensor
from nmxseis.numerics.simulation.event_waveform import simulate_waveforms_for_array
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

from NocMeta.Meta import NOC_META

base_dir = Path("braskem")
athena_code = "FBK"
STATION_XML = base_dir.joinpath("FBK_Full.xml")
inventory = read_inventory(STATION_XML)
surface_nsls = [nsl for nsl in NSL.iter_inv(inventory) if "T" in nsl.sta]

athena_config = NOC_META[athena_code]
athena_client = AthenaClient(
    rf'http://{athena_config["athIP"]}', athena_config["athApi"]
)

with open(base_dir.joinpath("velocity_model.json")) as f:
    velocity_model_json = json.load(f)

epsg = NOC_META[athena_code]["epsg"]

layers = []
for layer in velocity_model_json:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )
velocity_model = VelocityModel1D(layers)

start_time = UTCDateTime(2022, 7, 1)
end_time = UTCDateTime(2022, 8, 1)

event_list_query = f"""{{eventList(
        latitudeMinimum:-9.64631,
        latitudeMaximum:-9.62676,
        longitudeMinimum:-35.76408,
        longitudeMaximum:-35.72846,
        startTime:{start_time.timestamp},
        endTime:{end_time.timestamp},
        analysisType: MANUAL) {{
        events {{
          id
        }}
    }}
}}
"""

event_ids = [
    v["id"] for v in athena_client.query_gql(event_list_query)["eventList"]["events"]
]

mag = 0.314
corners = {Phase.P: 20, Phase.S: 14}
NOISE_RMS = 1.0e-8
reproject = EPSGReprojector(epsg)

for event_id in event_ids:
    source, pick_set = athena_client.get_preferred_origin_and_picks_for_event(event_id)
    print(f"simulating event {event_id}")
    event_enu = source.enu(reproject)
    random_dc_mt = generate_random_dc_moment_tensor()
    out_stream, rays = simulate_waveforms_for_array(
        *(random_dc_mt, mag, corners, event_enu, inventory, velocity_model, reproject),
        nt=8000,
        dt=0.001,
        Qp=500,
        Qs=500,
        noise_rms=NOISE_RMS,
        ch_prefix="HH",
        units="V",
        ev_origin_time=UTCDateTime(source.time),
        correct_for_response=False,
    )
    picks = []
    for (nsl, ph), ray in rays.items():
        picks.append(
            Pick(nsl=nsl, phase=ph, time=UTCDateTime(source.time + ray.traveltime_sec))
        )
    PickSet(picks=picks).dump(base_dir.joinpath("synthetic_picks", f"{event_id}.picks"))
    with open(base_dir.joinpath("event_locations", f"{event_id}.json"), "w") as f:
        json.dump(
            {
                "epsg": epsg,
                "latitude": source.lat,
                "longitude": source.lon,
                "depth_m": source.depth_m,
                "time": source.time,
            },
            f,
        )

    out_stream.write(
        base_dir.joinpath("synthetic_waveforms", f"{event_id}.mseed"),
        format="MSEED",
    )
#     out_stream[30:33].plot()
#
# plt.show()
