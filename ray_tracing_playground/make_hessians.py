import json

from numpy import zeros
from sympy import expand, Matrix, var

var("x1, x2, x3, x4")
var("H11, H12, H22, H13, H23, H33, H14, H24, H34, H44")


H2 = Matrix([[H11, H12], [H12, H22]])
H3 = Matrix([[H11, H12, H13], [H12, H22, H23], [H13, H23, H33]])
H4 = Matrix(
    [
        [H11, H12, H13, H14],
        [H12, H22, H23, H24],
        [H13, H23, H33, H34],
        [H14, H24, H34, H44],
    ]
)

system2, system3, system4 = [], [], []
for ii in (-1, 0, 1):
    for jj in (-1, 0, 1):
        x = Matrix([[ii * x1, jj * x2]])
        result = x.multiply(0.5 * H2).multiply(x.transpose())[0, 0]
        system2.append(expand(result).__str__())
        for kk in (-1, 0, 1):
            x = Matrix([[ii * x1, jj * x2, kk * x3]])
            result = x.multiply(0.5 * H3).multiply(x.transpose())[0, 0]
            system3.append(expand(result).__str__())
            for ll in (-1, 0, 1):
                x = Matrix([[ii * x1, jj * x2, kk * x3, ll * x4]])
                result = x.multiply(0.5 * H4).multiply(x.transpose())[0, 0]
                system4.append(expand(result).__str__())

h_terms = ["H11", "H12", "H22", "H13", "H23", "H33", "H14", "H24", "H34", "H44"]
A_matrixes = {2: zeros([9, 4]), 3: zeros([27, 7]), 4: zeros([81, 11])}
sign = {"+": 1, "-": -1}
for n_dim, system in zip([2, 3, 4], [system2, system3, system4]):
    A_matrixes[n_dim][:, 0] = 1
    for ii, line in enumerate(system):
        if line == "0":
            continue
        split_line = line.split()
        terms = split_line[::2]
        signs = ["+", *split_line[1::2]]
        for i_term, term in enumerate(terms):
            i_col = [(jj + 1) for jj, ht in enumerate(h_terms) if ht in term][0]
            A_matrixes[n_dim][ii, i_col] = (
                float(term.split("*")[0]) * sign[signs[i_term]]
            )
    A_matrixes[n_dim] = A_matrixes[n_dim].tolist()


with open("hessians_inverse_matrixes.json", "w") as f:
    json.dump(A_matrixes, f)
