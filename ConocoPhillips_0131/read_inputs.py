from pathlib import Path

from nmxseis.numerics.velocity_model.vm_1d import VelocityModel1D
from pandas import read_csv

VELOCITY_MODEL = VelocityModel1D.from_json(Path('smoothed_velocity_model.json'))

CATALOG = read_csv(Path('PAD0131_final_catalog_toDeliver.csv'))