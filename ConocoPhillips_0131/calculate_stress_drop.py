from nmxseis.numerics.formulas import calculate_eshelby_stress_drop, convert_radius_corner_madariaga, convert_radius_corner_brune
from nmxseis.model.phase import Phase

from read_inputs import VELOCITY_MODEL, CATALOG

for index, row in CATALOG.iterrows():
	if (fc:=row['Fc'])>0:
		layer = VELOCITY_MODEL.look_up_enu((row['Easting'], row['Northing'], -row['TVDSS']))
		brune_rad = convert_radius_corner_brune(fc, layer.vp)
		madar_rad = convert_radius_corner_madariaga(fc, layer.vs, Phase.P)
		CATALOG.loc[index,'stress_drop_B'] = calculate_eshelby_stress_drop(row['M0'], brune_rad)
		CATALOG.loc[index,'stress_drop_M'] = calculate_eshelby_stress_drop(row['M0'], madar_rad)
	else:
		CATALOG.loc[index,'stress_drop_B'] = -999.0
		CATALOG.loc[index,'stress_drop_M'] = -999.0

CATALOG.to_csv('PAD0131_final_catalog_toDeliver_with_stress_drop.csv')