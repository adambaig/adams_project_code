import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt
import numpy as np
from pyproj import Transformer

transformer = Transformer.from_crs("epsg:32125", "epsg:4326")
inverse_transformer = Transformer.from_crs("epsg:4326", "epsg:32125")
lat_center = 35.81738
lon_center = -98.15564

easting_center, northing_center = inverse_transformer.transform(lat_center, lon_center)

# f = open("Wells.csv")
# head = f.readline()
# lines = f.readlines()
# f.close()
#
# line = lines[0]
#
# for i_well, line in enumerate(lines):
#     coordinates = line.split('"')[1].split()
#     g = open("Well_" + str(i_well + 1) + ".csv", "w")
#     g.write("easting (m), northing (m) \n")
#     coordinate = coordinates[4]
#     for coordinate in coordinates:
#         lon, lat, elev = [float(s) for s in coordinate.split(",")]
#         easting, northing = transformer.transform(lat, lon)
#         g.write("%.2f, %.2f\n" % (easting, northing))
#     g.close()
#
#
# fig, ax = plt.subplots()
# ax.set_aspect("equal")

wells = {}
for well_file in glob.glob("fake_well_*.csv"):
    f = open(well_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    well_id = well_file[10]
    g = open("fake_well_lat_lon" + well_id + ".csv", "w")
    g.write("easting, northing\n")
    latitude, longitude = np.zeros(len(lines)), np.zeros(len(lines))
    for i_line, line in enumerate(lines):
        if line != "":
            easting, northing = [float(s) for s in line.split(",")[:2]]
            lat, lon = transformer.transform(
                easting + easting_center, northing + northing_center
            )
            g.write("%.9f, %.9f\n" % (lat, lon))
    g.close()
