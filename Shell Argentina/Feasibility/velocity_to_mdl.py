import matplotlib

matplotlib.use("Qt5agg")
import matplotlib.pyplot as plt
import numpy as np

from pfi_fs.configuration import geology


Q, velocity_model = geology("geology.cfg")

f = open("velocity_model.mdl", "w")
surface_elevation_m = 350
end_line = " 0.00 0.00 0.00 ' '\n"
for layer in velocity_model:
    if "top" not in layer:
        depth = 0
    else:
        depth = (surface_elevation_m - layer["top"]) / 1000.0
        depth_str = (" %.3f" % depth)[:5]
        vp_str = ("%.3f" % (layer["vp"] / 1000))[:4]
        vs_str = (" %.3f" % (layer["vs"] / 1000))[:5]
        f.write(vp_str + vs_str + depth_str + end_line)
f.close()
