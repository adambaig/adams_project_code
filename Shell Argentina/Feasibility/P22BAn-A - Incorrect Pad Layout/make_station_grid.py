import numpy as np


def place_stations(writeout=False):
    target_depth = 3746
    lateral_length = 3000
    pad_width = 800
    xwide = target_depth + pad_width / 2
    ywide = target_depth + lateral_length / 2
    nx = 6
    ny = 10
    easting_ctr, northing_ctr = 108833.52, 5761839.87

    xrange = np.linspace(easting_ctr - xwide, easting_ctr + xwide, nx)
    yrange = np.linspace(northing_ctr - ywide, northing_ctr + ywide, ny)

    stations = []
    jj = -1
    for ix in xrange:
        for iy in yrange:
            jj += 1
            stations.append({"name": str(jj), "x": ix, "y": iy, "z": 900.0})
    writeout = True
    if writeout:
        fileout = "Artis_grid_%i.csv" % (nx * ny)
        g = open(fileout, "w")
        g.write("station, northing (m), easting (m), tvdss (m)\n")
        for station in stations:
            g.write(
                station["name"]
                + ",%.1f,%.1f,%.1f\n" % (station["y"], station["x"], station["z"])
            )
        g.close()
    return stations


def place_random_stations(writeout=False):
    target_depth = 2746
    lateral_length = 3000
    pad_width = 800
    elevation = 400
    xwide = target_depth + pad_width / 2
    ywide = target_depth + lateral_length / 2
    nx = 10
    ny = 12
    easting_ctr, northing_ctr = 0, 0
    xrange = np.linspace(easting_ctr - xwide, easting_ctr + xwide, nx)
    yrange = np.linspace(northing_ctr - ywide, northing_ctr + ywide, ny)
    x_spacing = 400
    y_spacing = 400
    stations = []
    jj = -1
    for ix in xrange:
        for iy in yrange:
            jj += 1
            stations.append(
                {
                    "name": str(jj),
                    "x": ix + x_spacing * np.random.randn(),
                    "y": iy + y_spacing * np.random.randn(),
                    "z": elevation,
                }
            )
    writeout = True
    if writeout:
        fileout = "Shell_Ar_A_random_grid_%i.csv" % (nx * ny)
        g = open(fileout, "w")
        g.write("station, northing (m), easting (m), tvdss (m)\n")
        for station in stations:
            g.write(
                station["name"]
                + ",%.1f,%.1f,%.1f\n" % (station["y"], station["x"], station["z"])
            )
        g.close()
    return stations
