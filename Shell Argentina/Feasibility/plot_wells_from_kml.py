import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
import pyproj as pr

f = open(r"Feasibility\wells_both_pads.txt")
lines = f.readlines()
f.close()
well_coords = []
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init="epsg:2081")

wells = {}
line = lines[::16][0]
for i_line, line in enumerate(lines[::16]):
    coords = line.split('"')[1].split()
    n_coords = len(coords)
    eastings, northings, elevations = (
        np.zeros(n_coords),
        np.zeros(n_coords),
        np.zeros(n_coords),
    )
    coord = coords[0]
    for i_coord, coord in enumerate(coords):
        lon, lat, elevations[i_coord] = [float(s) for s in coord.split(",")]
        eastings[i_coord], northings[i_coord] = pr.transform(
            latlon_proj, out_proj, lon, lat
        )
    wells[i_line] = {
        "northing": northings,
        "easting": eastings,
        "elevation": elevations,
    }
    if min(eastings) < 2520000:
        prefix = "P22BAn-A_"
    else:
        prefix = "P02SB-B_"
    # g = open(prefix+str(i_line).zfill(3)+'.csv','w')
    # g.write('easting (m),northing (m),elevation (m)\n')
    # for east,north,elev in zip(eastings, northings, elevations):
    #     g.write('%.2f,%.2f,%.2f\n' % (east, north, elev))
    # g.close()


fig, ax = plt.subplots()

ax.set_aspect("equal")
for ii in range(10):
    ax.plot(wells[ii]["easting"], wells[ii]["northing"])

plt.show()
