import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (
    MultipleLocator,
    FormatStrFormatter,
    AutoMinorLocator,
    LogLocator,
)

import glob


# assumes input files are mulitple realizations of M0s


def sqa(x):
    return np.squeeze(np.array(x))


center_east, center_north = 0, 0
colorbar = {"m_min": -1.8, "m_max": -1.4}
wells = {}
for well in glob.glob("fake_well_?.csv"):
    f = open(well)
    head = f.readline()
    lines = f.readlines()
    f.close()
    wells[well.split(".")[0][-1]] = {
        "easting": [float(line.split(",")[0]) for line in lines],
        "northing": [float(line.split(",")[1]) for line in lines],
    }

f = open("Q100//Detectability Map//detectability_parameters.csv")
quad = float(f.readline().split(",")[1])
slope = float(f.readline().split(",")[1])
threshold_13 = float(f.readline().split(",")[1])
f.close()

decoherence_correction = 0.05
fig, ax = plt.subplots(1, 3, sharey=True, figsize=[14, 9])
f1, a1 = plt.subplots(1, 3, sharey=True, figsize=[14, 9])
f2, a2 = plt.subplots(1, 3, sharey=True, figsize=[14, 9])

for i_scenario, n_stations in enumerate(["15", "30", "60"]):
    csvs = glob.glob(
        "Q100//Detectability Map//"
        + n_stations
        + "_stations_1?_?e?n//synthetic_catalog_*.csv"
    )
    image_stats = []
    location_error = {}
    location_bias = {}
    m_detect = {}

    for csv in csvs:
        position = csv.split("\\")[-2].split("_")[3]
        nnodes = csv.split("\\")[-2].split("_")[2]
        nstations = csv.split("\\")[-2].split("_")[0]
        threshold = threshold_13 / np.sqrt(float(nnodes) / 13.0)
        f = open(csv)
        lines = f.readlines()
        f.close()
        for line in lines:
            lspl = line.split(",")
            true_loc = np.array([float(s) for s in lspl[2:5]])
            magnitude, stress_drop = [float(s) for s in lspl[5:7]]
            m11, m22, m33, m12, m13, m23 = [float(s) for s in lspl[7:13]]
            amplitude = float(lspl[13])
            image_loc = np.array([float(s) for s in lspl[14:17]])
            if abs(magnitude) < 1e-7:
                image_stats.append(
                    {
                        "position": position,
                        "nstations": nstations,
                        "nnodes": nnodes,
                        "magnitude": magnitude,
                        "amplitude": amplitude,
                        "location shift": true_loc - image_loc,
                    }
                )

        location_error[position] = {
            "vertical": np.std(
                np.array(
                    [
                        i_s["location shift"][2]
                        for i_s in image_stats
                        if i_s["position"] == position
                    ]
                )
            ),
            "horizontal": np.std(
                np.sqrt(
                    [
                        x**2 + y**2
                        for x, y in [
                            i_s["location shift"][:2]
                            for i_s in image_stats
                            if i_s["position"] == position
                        ]
                    ]
                )
            ),
        }
        location_bias[position] = {
            "vertical": np.median(
                np.array(
                    [
                        i_s["location shift"][2]
                        for i_s in image_stats
                        if i_s["position"] == position
                    ]
                )
            ),
            "horizontal": np.median(
                np.sqrt(
                    [
                        x**2 + y**2
                        for x, y in [
                            i_s["location shift"][:2]
                            for i_s in image_stats
                            if i_s["position"] == position
                        ]
                    ]
                )
            ),
        }
        avg_amplitude = np.average(
            np.array(
                [i_s["amplitude"] for i_s in image_stats if i_s["position"] == position]
            )
        )
        m_detect[position] = (
            -slope
            + np.sqrt(slope * slope - 4 * quad * (np.log10(avg_amplitude / threshold)))
        ) / 2.0 / quad + decoherence_correction
    print(m_detect["0e0n"])
    n_rows = len(m_detect)
    A_matrix, rhs_detectability = np.zeros([n_rows, 5]), np.zeros(n_rows)
    for i_row, (position, mag) in enumerate(m_detect.items()):
        x = int(position[0]) * 800.0
        y = int(position[2]) * 800.0
        A_matrix[i_row, :] = [1, x, y, x * x, y * y]
        rhs_detectability[i_row] = mag

    rhs_vrt_error, rhs_hrz_error = np.zeros(n_rows), np.zeros(n_rows)
    for i_row, (position, error) in enumerate(location_error.items()):
        rhs_vrt_error[i_row] = error["vertical"]
        rhs_hrz_error[i_row] = error["horizontal"]

    detectability_coeffs = np.linalg.lstsq(A_matrix, rhs_detectability)[0]
    vertical_error_coeffs = np.linalg.lstsq(A_matrix, rhs_vrt_error)[0]
    horizontal_error_coeffs = np.linalg.lstsq(A_matrix, rhs_hrz_error)[0]

    def interp_field(x, y, coeffs):
        return (
            coeffs[0]
            + coeffs[1] * abs(x)
            + coeffs[2] * abs(y)
            + coeffs[3] * x * x
            + coeffs[4] * y * y
        )

    stations = {}
    f = open("Encana_OK_random_grid_" + n_stations + ".csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        id = lspl[0]
        stations[id] = {
            "easting": float(lspl[2]) - center_east,
            "northing": float(lspl[1]) - center_north,
            "elevation": float(lspl[3]),
        }

    ax[i_scenario].set_aspect("equal")
    x_pos = np.linspace(-1600, 1600, 101)
    y_pos = np.linspace(-2400, 2400, 241)
    detect_mag = np.zeros([len(x_pos), len(y_pos)])
    hrz_error = np.zeros([len(x_pos), len(y_pos)])
    vrt_error = np.zeros([len(x_pos), len(y_pos)])

    for i_x, x in enumerate(x_pos):
        for i_y, y in enumerate(y_pos):
            detect_mag[i_x, i_y] = interp_field(x, y, detectability_coeffs)
            hrz_error[i_x, i_y] = interp_field(x, y, horizontal_error_coeffs)
            vrt_error[i_x, i_y] = interp_field(x, y, vertical_error_coeffs)
    ax[i_scenario].pcolor(
        x_pos,
        y_pos,
        detect_mag.T,
        zorder=0,
        cmap="viridis",
        vmin=colorbar["m_min"],
        vmax=colorbar["m_max"],
    )
    a1[i_scenario].pcolor(
        x_pos, y_pos, hrz_error.T, zorder=0, cmap="inferno", vmin=0, vmax=30
    )
    a2[i_scenario].pcolor(
        x_pos, y_pos, vrt_error.T, zorder=0, cmap="inferno", vmin=0, vmax=30
    )
    for axis in [ax[i_scenario], a1[i_scenario], a2[i_scenario]]:
        for tag, well in wells.items():
            axis.plot(
                well["easting"],
                well["northing"],
                c="0.8",
                lw=4,
                zorder=3,
                alpha=0.5,
            )
            axis.plot(
                well["easting"],
                well["northing"],
                c="0.2",
                lw=2,
                zorder=4,
                alpha=0.5,
            )
        # for id, station in stations.items():
        #     axis.plot(
        #         station["easting"],
        #         station["northing"],
        #         "h",
        #         markeredgecolor="w",
        #         c="k",
        #         zorder=5,
        #     )
        axis.set_xlabel("easting (m)")
        axis.set_title(n_stations + " " + nnodes + "-node superstations")
    for axis in [ax[0], a1[0], a2[0]]:
        axis.set_ylabel("northing (m)")
    print(csv)
    print(
        "vertical error %.1f: "
        % np.median([e["vertical"] for k, e in location_error.items()])
    )
    print(
        "horizontal error %.1f: "
        % np.median([e["horizontal"] for k, e in location_error.items()])
    )
    print(
        "vertical bias %.1f: "
        % np.median([e["vertical"] for k, e in location_bias.items()])
    )
    print(
        "horizonal bias %.1f: "
        % np.median([e["horizontal"] for k, e in location_bias.items()])
    )
magnitude_range = np.linspace(colorbar["m_min"], colorbar["m_max"], 100)
fig_cb_detect = plt.figure(figsize=[8, 1])
cb_detect = fig_cb_detect.add_axes([0.1, 0.5, 0.8, 0.4])
cb_detect.pcolor(magnitude_range, [0, 1], np.vstack([magnitude_range, magnitude_range]))
cb_detect.set_yticks([])
cb_detect.set_xlabel("magnitude of detectability")

error_range = np.linspace(0, 30, 100)
fig_cb_error = plt.figure(figsize=[8, 1])
cb_error = fig_cb_error.add_axes([0.1, 0.5, 0.8, 0.4])
cb_error.pcolor(
    error_range, [0, 1], np.vstack([error_range, error_range]), cmap="inferno"
)
cb_error.set_yticks([])
cb_error.set_xlabel("location error (m)")
