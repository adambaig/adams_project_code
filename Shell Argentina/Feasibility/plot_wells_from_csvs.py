import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt
import numpy as np

p02sb_wells = {}


p22ban_wells = glob.glob("P22BAn-A*.csv")

fig, ax = plt.subplots(1, 2)
[a.set_aspect("equal") for a in ax]
for i_pad, pad in enumerate(["P02SB-B", "P22BAn-A"]):
    for csv in glob.glob(pad + "*.csv"):
        f = open(csv)
        head = f.readline()
        lines = f.readlines()
        f.close()
        well = csv.split(".")[0].split("_")[1]
        n_lines = len(lines)
        east, north = np.zeros(len(lines)), np.zeros(len(lines))
        for i_line, line in enumerate(lines):
            lspl = line.split(",")
            east[i_line] = float(lspl[0])
            north[i_line] = float(lspl[1])

        ax[i_pad].plot(east, north, "k")

plt.show()
