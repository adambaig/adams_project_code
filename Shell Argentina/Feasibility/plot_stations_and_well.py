import matplotlib

matplotlib.use("Qt5agg")
import glob
import matplotlib.pyplot as plt
import numpy as np
from pfi_fs.configuration import geology


pad = "P02SB-B"


if pad == "P02SB-B":
    center_east, center_north = 2545000, 5743800
else:
    center_east, center_north = 2500700, 5770700

dr = pad + "//"

wells = {}
for well in glob.glob(dr + pad + "_*.csv"):
    f = open(well)
    head = f.readline()
    lines = f.readlines()
    f.close()
    wells[well.split(".")[0][-1]] = {
        "easting": np.array([float(line.split(",")[0]) for line in lines])
        - center_east,
        "northing": np.array([float(line.split(",")[1]) for line in lines])
        - center_north,
    }

fig, ax = plt.subplots(1, 3, figsize=[20, 7], sharex=True, sharey=True)
for axis in ax:
    for tag, well in wells.items():
        axis.plot(
            well["easting"],
            well["northing"],
            c="0.8",
            lw=4,
            zorder=3,
            alpha=0.5,
        )
        axis.plot(
            well["easting"],
            well["northing"],
            c="0.2",
            lw=2,
            zorder=4,
            alpha=0.5,
        )
        axis.set_xlabel("easting (m)")

station_files = glob.glob(dr + "Shell_Ar_?_random_grid_??.csv")
for i_file, station_file in enumerate(station_files):
    f = open(station_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    n_lines = len(lines)
    east, north = np.zeros(n_lines), np.zeros(n_lines)
    for i_line, line in enumerate(lines):
        lspl = line.split(",")
        north[i_line], east[i_line] = [float(s) for s in lspl[1:3]]
    a = ax[i_file]
    a.set_aspect("equal")
    a.plot(east, north, "v", zorder=6)

Q, velocity_model = geology("geology.cfg")

tops = [400]
vp = [velocity_model[0]["vp"], velocity_model[0]["vp"]]
vs = [velocity_model[0]["vs"], velocity_model[0]["vs"]]

for layer in velocity_model[1:]:
    for ii in range(2):
        tops.append(layer["top"])
        vp.append(layer["vp"])
        vs.append(layer["vs"])
tops.append(layer["top"] - 1000)
v_fig, v_ax = plt.subplots()
v_ax.plot(vp, tops, "firebrick")
v_ax.plot(vs, tops, "royalblue")
v_ax.plot([0, 6500], [-2751, -2751], "darkgoldenrod", lw=3)
v_ax.plot([0, 6500], [-2346, -2346], "darkgoldenrod", lw=3)
v_ax.text(200, -2300, "target depth P22BAn-B\n2746 m TVD")
v_ax.text(200, -3200, "target depth P02SB-B\n3151 m TVD")
v_ax.plot([0, 6500], [tops[0], tops[0]], "k")
v_ax.set_ylim([-4000, 1000])
v_ax.set_xlim([0, 6500])
v_ax.legend(["$v_p$", "$v_s$"], loc=7)
v_ax.set_xlabel("velocity (m/s)")
v_ax.set_ylabel("elevation above sea level (m)")

plt.show()
