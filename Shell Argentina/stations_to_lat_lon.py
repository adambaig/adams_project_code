import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt
import numpy as np
import os
from pyproj import Transformer


transformer = Transformer.from_crs("epsg:2081", "epsg:4326")
inverse_transformer = Transformer.from_crs("epsg:2081", "epsg:32125")

easting_center, northing_center = 585932.2877188409, 275577.1406804584
northing_center = {"SB": 2545000, "BA": 2500700}
easting_center = {"SB": 5743800, "BA": 5770700}
stations = {}
#
# transformer.transform(easting_center, northing_center)


stations["SB"] = glob.glob(
    r"C:\Users\adambaig\Project\Shell Argentina\Feasibility\P02SB-B\Shell*_??.csv"
)
stations["BA"] = glob.glob(
    r"C:\Users\adambaig\Project\Shell Argentina\Feasibility\P22BAn-A\Shell*_??.csv"
)


for pad in ["SB", "BA"]:
    for station_file in stations[pad]:
        base, ext = os.path.split(station_file)
        out_file = (
            ext.replace(".csv", "_latlon.csv")
            .replace("_B_", "_P02SB-B_")
            .replace("_A_", "_P22BAn-A_")
        )
        f = open(station_file)
        head = f.readline()
        lines = f.readlines()
        f.close()
        g = open(out_file, "w")
        g.write("station,latitude,longitude, elevation (m)\n")
        for line in lines:
            lspl = line.split(",")
            easting, northing = [float(s) for s in lspl[1:3]]
            lat, lon = transformer.transform(
                easting + easting_center[pad], northing + northing_center[pad]
            )
            g.write(lspl[0] + ",")
            g.write("%.5f, %.5f,400\n" % (lat, lon))
        g.close()
