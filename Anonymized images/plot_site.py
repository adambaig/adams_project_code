import matplotlib

matplotlib.use("Qt4agg")


import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from read_inputs import read_events, read_stations, read_wells_kml
from datetime import datetime, timedelta


minorLocator1 = MultipleLocator(250)
minorLocator2 = MultipleLocator(250)
events = read_events()
stations = read_stations()
wells = read_wells_kml()

easting_avg = np.average([v["easting"] for (k, v) in events.items()])
northing_avg = np.average([v["northing"] for (k, v) in events.items()])

rot_ang = 172

R = np.matrix(
    [
        [np.cos(rot_ang * np.pi / 180.0), -np.sin(rot_ang * np.pi / 180.0)],
        [np.sin(rot_ang * np.pi / 180.0), np.cos(rot_ang * np.pi / 180.0)],
    ]
)


def sqa(x):
    return np.squeeze(np.array(x))


fig, ax = plt.subplots()
ax.set_aspect("equal")
east = np.array([v["easting"] - easting_avg for (k, v) in events.items()]) / 3.28
north = np.array([v["northing"] - northing_avg for (k, v) in events.items()]) / 3.28
mag = np.array([v["magnitude"] for (k, v) in events.items()])
time = np.array(
    [datetime.strptime(k, "%Y-%m-%dT%H:%M:%SZ") for (k, v) in events.items()]
)
sta_east = np.array([v["easting"] - easting_avg for (k, v) in stations.items()]) / 3.28
sta_north = (
    np.array([v["northing"] - northing_avg for (k, v) in stations.items()]) / 3.28
)
isort = np.argsort(mag)
days = np.array([(s - time[-1]).days for s in time])
h1, h2 = [sqa(x) for x in R * np.matrix([east, north])]
h1_sta, h2_sta = [sqa(x) for x in R * np.matrix([sta_east, sta_north])]

np.average(days)
scatter = ax.scatter(
    h1[isort],
    h2[isort],
    c=mag[isort],
    s=(mag[isort] + 2) * 40,
    vmin=-1,
    vmax=2,
    marker=".",
    edgecolor="0.5",
    linewidth=0.25,
    zorder=2,
    cmap="inferno",
)
ax.plot(h1_sta, h2_sta, "v", color="forestgreen", markeredgecolor="k")
for pad in wells.keys():
    if pad == "Lucile":
        for well in wells[pad].keys():
            if not (well.split()[-1] in ["1H", "2H", "3H"]):
                h1_well, h2_well = [
                    sqa(x)
                    for x in R
                    * np.matrix(
                        np.vstack(
                            [
                                (wells[pad][well]["easting"] - easting_avg) / 3.28,
                                (wells[pad][well]["northing"] - northing_avg) / 3.28,
                            ]
                        )
                    )
                ]
                ax.plot(h1_well, h2_well, "royalblue", lw=2, zorder=3, alpha=0.9)
                ax.plot(h1_well, h2_well, "0.2", lw=4, zorder=2, alpha=0.9)


ax.xaxis.set_minor_locator(minorLocator1)
ax.yaxis.set_minor_locator(minorLocator2)
# ax.set_xticklabels([])
# ax.set_yticklabels([])
ax.set_xlim([-3200, -1400])
ax.set_ylim([-1800, 400])
ax.grid(True, which="both")
ax.tick_params(axis="x", which="both", color=(0, 0, 0, 0))
ax.tick_params(axis="y", which="both", color=(0, 0, 0, 0))
ax.set_facecolor("0.9")
cb = fig.colorbar(scatter)
cb.set_label("magnitude")

plt.show()
