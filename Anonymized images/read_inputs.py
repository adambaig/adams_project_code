import numpy as np
import utm
import glob


def read_events():
    event_file = "events.csv"
    f = open(event_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[3]
        events[event_id] = {}
        latitude, longitude = [float(s) for s in lspl[6:8]]
        events[event_id]["magnitude"] = float(lspl[10])
        events[event_id]["depth"] = float(lspl[12])
        (
            events[event_id]["easting"],
            events[event_id]["northing"],
            d1,
            d2,
        ) = utm.from_latlon(latitude, longitude)
    return events


def read_stations():
    station_file = "channels.csv"
    f = open(station_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        id = lspl[1]
        stations[id] = {}
        latitude, longitude = [float(s) for s in lspl[7:9]]
        stations[id]["elevation"] = float(lspl[9])
        stations[id]["easting"], stations[id]["northing"], d1, d2 = utm.from_latlon(
            latitude, longitude
        )
    return stations


def read_wells_kml():
    well_dir = "O:\\O&G\\NOC\\KML\\Newfield\\2019_ESA\\Row1 Wells and Buffer\\"
    wells = {}
    for well_kml in glob.glob(well_dir + "*Wells.kml"):
        pad = well_kml.split("\\")[-1].split()[0]
        f = open(well_kml)
        kmlfile = f.read()
        f.close()
        wells[pad] = {}
        well = kmlfile.split("<Folder")[2]
        for well_traj in kmlfile.split("<Folder")[2:]:
            well = well_traj.split("name>")[1][:-2]
            coords = well_traj.split("coordinates>")[1].split()
            coord = coords[12]
            wells[pad][well] = {}
            easting, northing = [], []
            for coord in coords[:-1]:
                long, lat, d = [float(s) for s in coord.split(",")]
                east, north, d1, d2 = utm.from_latlon(lat, long)
                easting.append(east)
                northing.append(north)
            wells[pad][well]["easting"] = np.array(easting)
            wells[pad][well]["northing"] = np.array(northing)
    return wells


wells = read_wells_kml()
