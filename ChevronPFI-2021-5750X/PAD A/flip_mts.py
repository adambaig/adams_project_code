from read_inputs import read_template_matching_catalog, read_noise_mts, read_wells
from sms_moment_tensor.MT_math import (
    decompose_MT,
    trend_plunge_to_unit_vector,
    mt_to_sdr,
    clvd_iso_dc,
    mt_matrix_to_vector,
)
from generalPlots import gray_background_with_grid
from sklearn.cluster import AgglomerativeClustering
from shapely.geometry.polygon import Polygon
from shapely.geometry import Point
import mplstereonet as mpls
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import os
import json
from datetime import datetime
import matplotlib

matplotlib.use("Qt5agg")


def norm_MT(mt):
    return mt / np.linalg.norm(mt)


def moment(event):
    return 10 ** (1.5 * event["magnitude"] + 9)


def scaled_mt(mt, moment_value):
    moment = 10 ** (1.5 * event["magnitude"] + 9)
    return np.sqrt(2) * mt * moment / np.linalg.norm(event["dc MT"])


def make_simple_gradient_from_white_cmap(end_color):
    red, green, blue = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, 1.0, 1.0), (1, red, red)],
        "green": [(0.0, 1.0, 1.0), (1, green, green)],
        "blue": [(0.0, 1.0, 1.0), (1, blue, blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


UPPER_FAULT_MIN_ELEVATION = -2050
LOWER_FAULT_MAX_ELEVATION = -2200
CUTOFF_PERCENTILE = 95

filename = r"Catalogs\relocCatalog_all_QCed_format_before_flip.csv"

noise = read_noise_mts()
# events = read_template_matching_catalog(filename)
with open("all_flipped_events_including_TM.json") as f:
    events = json.load(f)

wells = read_wells()
mt_r = [v["dc R"] for v in events.values()]
noise_r = [v["dc R"] for v in noise.values()]
cutoff_r = np.percentile(noise_r, CUTOFF_PERCENTILE)


for event_id, event in events.items():
    events[event_id] = {**event, **decompose_MT(event["dc MT"])}
good_mts = {k: v for k, v in events.items() if v["dc R"] > cutoff_r}

"""
lower_events = {k: v for k, v in events.items() if v["elevation"] < LOWER_FAULT_MAX_ELEVATION}
fig, ax = plt.subplots()
ax.set_aspect("equal")
for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k")
ax.plot([v["easting"] for v in lower_events.values()], [v["northing"] for v in lower_events.values()], ".")

mini_lower_fault_polygon = fig.ginput(n=-1, timeout=-1)

with open('mini_lower_fault_polygon.json','w') as f:
    json.dump(mini_lower_fault_polygon,f)

plt.show()

upper_fault_polygon = [[520303, 6020469], [520476, 6020457], [520399, 6019603], [520250, 6019657]]
with open("fault_polygon_upper.json",'w') as f:
    json.dump(upper_fault_polygon,f)
"""

with open("fault_polygon_xy.json") as f:
    lower_fault_polygon = Polygon(json.load(f))

with open("fault_polygon_upper.json") as f:
    upper_fault_polygon = Polygon(json.load(f))


upper_fault_events = {
    k: v
    for k, v in events.items()
    if upper_fault_polygon.contains(Point(v["easting"], v["northing"]))
    and v["elevation"] > UPPER_FAULT_MIN_ELEVATION
}

lower_fault_events = {
    k: v
    for k, v in events.items()
    if lower_fault_polygon.contains(Point(v["easting"], v["northing"]))
    and v["elevation"] < LOWER_FAULT_MAX_ELEVATION
}

not_fault_events = {
    k: v
    for k, v in events.items()
    if not (k in upper_fault_events) and not (k in lower_fault_events)
}


not_fault_good_mts = {k: v for k, v in not_fault_events.items() if v["dc R"] > cutoff_r}


n_mt = len(not_fault_good_mts)
if os.path.exists("mt_dot_product_pairs_not_fault_TM.json"):
    with open("mt_dot_product_pairs_not_fault.json", "r") as read_file:
        dot_product_dict = json.load(read_file)
    dot_product_pairs = np.array(dot_product_dict["dot_product_pairs"])
else:
    dot_product_pairs = np.zeros([n_mt, n_mt])
    for i1, (id1, mt1) in enumerate(not_fault_good_mts.items()):
        for i2, (id2, mt2) in enumerate(not_fault_good_mts.items()):
            dc1_norm = mt1["dc MT"] / np.linalg.norm(mt1["dc MT"])
            dc2_norm = mt2["dc MT"] / np.linalg.norm(mt2["dc MT"])
            dot_product_pairs[i1, i2] = np.tensordot(dc1_norm, dc2_norm)
    dot_product_dict = {"dot_product_pairs": dot_product_pairs.tolist()}
    with open("mt_dot_product_pairs_not_fault_TM.json", "w") as write_file:
        json.dump(dot_product_dict, write_file)

distance_mat = abs(1 - abs(dot_product_pairs))

colors = [
    "firebrick",
    "forestgreen",
    "darkgoldenrod",
    "steelblue",
    "indigo",
    "darkorange",
    "turquoise",
    "black",
]


f1, a1 = mpls.subplots(1, 3, figsize=[16, 5])
for i_axis, axis in enumerate(["p", "b", "t"]):
    a1[i_axis].density_contourf(
        [v[f"{axis}_plunge"] for v in not_fault_good_mts.values()],
        [v[f"{axis}_trend"] for v in not_fault_good_mts.values()],
        measurement="lines",
    )
# f1.savefig("unflipped_axes.png")


n_clusters = 6
clustering = AgglomerativeClustering(
    n_clusters=n_clusters, affinity="precomputed", linkage="complete"
).fit(distance_mat)
labels = clustering.labels_
for i_mt, (id, mt) in enumerate(not_fault_good_mts.items()):
    mt["mt cluster"] = labels[i_mt]
flip = [-1, -1, 1, 1, 1, -1, 1, 1]
for event in events.values():
    event["is flipped"] = False
for i_cluster in range(n_clusters):
    mt_cluster = {
        k: v for k, v in not_fault_good_mts.items() if v["mt cluster"] == i_cluster
    }

    custom_cmap = make_simple_gradient_from_white_cmap(colors[i_cluster])
    mt_avg = flip[i_cluster] * sum(
        [v["dc MT"] / np.linalg.norm(v["dc MT"]) for k, v in mt_cluster.items()]
    )

    p_trend, b_trend, t_trend = [], [], []
    p_plunge, b_plunge, t_plunge = [], [], []

    for id, mt in mt_cluster.items():
        mt["old dc MT"] = mt["dc MT"]
        mt["old gen MT"] = mt["gen MT"]
        if np.tensordot(mt_avg, mt["dc MT"]) < 0:
            mt["dc MT"] = -mt["dc MT"]
            mt["gen MT"] = -mt["gen MT"]
        decomp = decompose_MT(mt["dc MT"])
        p_trend.append(decomp["p_trend"])
        b_trend.append(decomp["b_trend"])
        t_trend.append(decomp["t_trend"])
        p_plunge.append(decomp["p_plunge"])
        b_plunge.append(decomp["b_plunge"])
        t_plunge.append(decomp["t_plunge"])
    f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = a3[0].density_contourf(
        p_plunge, p_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_b = a3[1].density_contourf(
        b_plunge, b_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_t = a3[2].density_contourf(
        t_plunge, t_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    f3.savefig(f"agglomerative_clustering_{i_cluster}.png")


for event in not_fault_good_mts.values():
    if np.tensordot(event["old dc MT"], event["dc MT"]) < 0:
        event["is flipped"] = True


len([k for k, v in not_fault_events.items() if v["is flipped"]])

cluster_mts = [
    sum(
        mt["dc MT"] / np.linalg.norm(mt["dc MT"])
        for mt in not_fault_good_mts.values()
        if mt["mt cluster"] == ii
    )
    for ii in range(n_clusters)
]

low_q_not_fault = {k: v for k, v in not_fault_events.items() if v["dc R"] < cutoff_r}
for event in low_q_not_fault.values():
    dot_products = np.zeros(n_clusters)
    for i_cluster in range(n_clusters):
        dot_products[i_cluster] = np.tensordot(
            norm_MT(event["dc MT"]), norm_MT(cluster_mts[i_cluster])
        )
    i_cluster_belongs = np.argmax(abs(dot_products))
    if dot_products[i_cluster_belongs] < 0:
        event["dc MT"] = -event["dc MT"]
        event["gen MT"] = -event["gen MT"]
        event["is flipped"] = True
    # event["mt cluster"] = i_cluster_belongs


# fault mts very well behaved: not using agglomerative clustering
for fault_events in [lower_fault_events, upper_fault_events]:
    f1, a1 = mpls.subplots(1, 3, figsize=[16, 5])
    for i_axis, axis in enumerate(["p", "b", "t"]):
        a1[i_axis].density_contourf(
            [
                v[f"{axis}_plunge"]
                for v in fault_events.values()
                if v["dc R"] > cutoff_r
            ],
            [v[f"{axis}_trend"] for v in fault_events.values() if v["dc R"] > cutoff_r],
            measurement="lines",
        )

sum_good_fault_mts_lower = sum(
    mt["dc MT"] / np.linalg.norm(mt["dc MT"])
    for mt in lower_fault_events.values()
    if mt["dc R"] > cutoff_r
)

for event in lower_fault_events.values():
    dot_product = np.tensordot(
        norm_MT(event["dc MT"]), norm_MT(sum_good_fault_mts_lower)
    )
    if dot_product < 0:  # sum lower faults agrees with stress
        event["dc MT"] = -event["dc MT"]
        event["gen MT"] = -event["gen MT"]
        event["is flipped"] = True

sum_good_fault_mts_upper = sum(
    mt["dc MT"] / np.linalg.norm(mt["dc MT"])
    for mt in upper_fault_events.values()
    if mt["dc R"] > cutoff_r
)

for event in upper_fault_events.values():
    dot_product = np.tensordot(
        norm_MT(event["dc MT"]), norm_MT(sum_good_fault_mts_upper)
    )
    if dot_product > 0:  # sum upper fault disagrees with stress
        event["dc MT"] = -event["dc MT"]
        event["gen MT"] = -event["gen MT"]
        event["is flipped"] = True


all_flipped_events = {
    **upper_fault_events,
    **lower_fault_events,
    **not_fault_good_mts,
    **low_q_not_fault,
}
event.keys()
# make_objects_json_serializable
for event in all_flipped_events.values():
    event["dc MT"] = event["dc MT"].tolist()
    event["gen MT"] = event["gen MT"].tolist()
    event["utc datetime"] = datetime.strftime(event["time"], "%Y-%m-%dT%H:%M:%S.%fZ")
    event.pop("time")
    if "mt cluster" in event:
        event.pop("mt cluster")
    if "old dc MT" in event:
        event.pop("old dc MT")
        event.pop("old gen MT")


for event in all_flipped_events.values():
    for key_to_pop in [
        "clvd",
        "iso",
        "dc",
        "p_trend",
        "p_plunge",
        "b_trend",
        "b_plunge",
        "t_trend",
        "t_plunge",
        "lambda_1",
        "lambda_2",
        "lambda_3",
    ]:
        event.pop(key_to_pop)


f = open("all_flipped_events_including_TM.json", "w")
json.dump(all_flipped_events, f)
f.close()
