from datetime import datetime, timedelta
import json
import os

import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import pytz
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

from pfi_qi.QI_analysis import (
    parent_child,
    rt_diffusivity,
    fracture_dimensions,
    strain_grid,
    output_isosurface,
    rotate_all_events_to_well_coords,
)

from pfi_qi.engineering import (
    sorted_stage_list,
    breaktimes_by_stage,
    find_perf_center,
    calc_well_trend,
    make_unique_stage_list,
)
from pfi_qi.rotations import rotate_from_cardinal

from plotting_routines import (
    rt_with_treatment,
    plot_stage,
    add_dimension_arrows_abs_coords,
)
from read_inputs import read_wells, read_treatment, classify_events_by_treatment


def read_or_load_json(read_function, json_file, *args):
    if os.path.isfile(json_file):
        with open(json_file, "r") as read_file:
            dictionary = json.load(read_file)
        return dictionary
    dictionary = read_function(*args)
    with open(json_file, "w") as write_file:
        json.dump(dictionary, write_file)
    return dictionary


PI = np.pi
D2R = PI / 180.0
UTC = pytz.utc
TIMEZONE = pytz.timezone("America/Edmonton")
UPPER_FAULT_MIN_ELEVATION = -2050
LOWER_FAULT_MAX_ELEVATION = -2200
magnitude_of_completeness = -1.05


with open("all_flipped_events_including_TM.json") as f:
    events = json.load(f)
# "timestamp" is expected as a key
for event in events.values():
    event["timestamp"] = UTC.localize(
        datetime.strptime(event["utc datetime"], "%Y-%m-%dT%H:%M:%S.%fZ")
    )

events = read_flipped_catalog(
    "Catalogs\\relocCatalog_all_QCed_format_after_flip_final_fault_tagged_final_updateFault_170m_final.csv"
)

for event in events.values():
    event["timestamp"] = event["time"]

wells = read_wells()
treatment = read_or_load_json(read_treatment, "treatment_data.json")
sorted_stages = read_or_load_json(
    sorted_stage_list, "sorted_stage_list.json", treatment
)
break_times = read_or_load_json(
    breaktimes_by_stage, "break_times.json", sorted_stages, treatment
)

with open("fault_polygon_lower.json") as f:
    lower_fault_polygon = Polygon(json.load(f))
with open("mini_lower_fault_polygon.json") as f:
    lower_fault_mini_polygon = Polygon(json.load(f))
with open("fault_polygon_upper.json") as f:
    upper_fault_polygon = Polygon(json.load(f))

lower_fault_events = {
    k: v
    for k, v in events.items()
    if (
        lower_fault_polygon.contains(Point(v["easting"], v["northing"]))
        and v["elevation"] < LOWER_FAULT_MAX_ELEVATION
    )
    or lower_fault_mini_polygon.contains(Point(v["easting"], v["northing"]))
}
upper_fault_events = {
    k: v
    for k, v in events.items()
    if upper_fault_polygon.contains(Point(v["easting"], v["northing"]))
    and v["elevation"] > UPPER_FAULT_MIN_ELEVATION
}

all_fault_events = {**upper_fault_events, **lower_fault_events}
frac_events = {
    k: v
    for k, v in events.items()
    if not (k in all_fault_events) and v["stage"] != "-1"
}
diffusivity = rt_diffusivity(
    frac_events, wells, break_times, csv_out="diffusivity_out.csv"
)

# add event classification tags
for stage in sorted_stages:
    stage_name = stage["stage"]
    well = stage["well"]
    stage_events = {
        k: v
        for k, v in frac_events.items()
        if v["well"] == well and v["stage"] == stage_name
    }
    if len(stage_events) == 0:
        continue
    stage_data = {
        k: v
        for k, v in treatment.items()
        if v["well"] == well and v["stage"] == stage_name
    }
    well_stage_identifier = f"Well {well}: Stage {stage_name}"
    if diffusivity[well_stage_identifier]["horizontal"]:
        classify_events_by_treatment(
            stage_events,
            stage_data,
            break_times[well][stage_name],
            diffusivity=diffusivity[well_stage_identifier],
        )
    else:
        classify_events_by_treatment(
            stage_events, stage_data, break_times[well][stage_name]
        )


# make RT plots for all stages
for stage in sorted_stages:
    stage_name = stage["stage"]
    well = stage["well"]
    stage_events = {
        k: v
        for k, v in frac_events.items()
        if v["well"] == well and v["stage"] == stage_name
    }
    well_stage_identifier = f"Well {well}: Stage {stage_name}"
    if (
        well_stage_identifier in diffusivity
        and diffusivity[well_stage_identifier]["horizontal"]
    ):
        stage_data = {
            k: v
            for k, v in treatment.items()
            if v["well"] == well and v["stage"] == stage_name
        }
        fig1 = rt_with_treatment(
            stage_events,
            stage_data,
            diffusivity[well_stage_identifier]["horizontal"],
            diffusivity[well_stage_identifier]["t0"],
            title=well_stage_identifier,
        )
        fig1.savefig(f"figures//rt_plots//rt_w{well}_s{stage_name}.png")
        plt.close(fig1)


# remove IS, PRE, and POST events
events_without_is = {
    k: v
    for k, v in frac_events.items()
    if v["treatment_code"] not in ["IS", "PRE", "POST"]
}
# impose MC on complete dataset
frac_complete_events = {
    k: v
    for k, v in events_without_is.items()
    if event["Mw"] > magnitude_of_completeness
}

# calculate fracture dimensions and plot against individual plan view stages
frac_dimensions = fracture_dimensions(
    frac_complete_events, wells, return_arrows_too=True, csv_out="frac_dimensions.csv"
)
for stage in sorted_stages:
    stage_name = stage["stage"]
    well = stage["well"]
    well_stage_identifier = f"Well {well}: Stage {stage_name}"
    stage_events = {
        k: v
        for k, v in frac_events.items()
        if v["well"] == well
        and v["stage"] == stage_name
        and v["Mw"] > magnitude_of_completeness
    }
    if well_stage_identifier in frac_dimensions:
        fig_en = plot_stage(stage_events, wells)
        ax = add_dimension_arrows_abs_coords(
            fig_en.axes[0], frac_dimensions[well_stage_identifier]["arrows"]
        )
        fig_en.savefig(
            f"figures//Cardinal stages with dimensions//en_w{well}_s{stage_name}.png"
        )
        plt.close(fig_en)


list(stage_events.values())[0]
