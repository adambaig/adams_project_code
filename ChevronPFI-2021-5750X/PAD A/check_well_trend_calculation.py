import json
import numpy as np

from pfi_qi.engineering import calc_well_trend


from read_inputs import read_wells

with open("wells.json") as f:
    wells = json.load(f)


calc_well_trend(wells["10"])
wells["10"]["Stage 1"]

well = wells["10"]


wells = read_wells()

top_easts, bottom_easts, top_norths, bottom_norths = np.array(
    [
        (
            s[perf]["top easting"],
            s[perf]["bottom easting"],
            s[perf]["top northing"],
            s[perf]["bottom northing"],
        )
        for s in [stage for k, stage in well.items() if "Stage" in k]
        for perf in s.keys()
        if "n_clusters" not in perf
    ]
).T

for well in wells.values():
    # well['easting'] = well['easting'].tolist()
    well["northing"] = well["northing"].tolist()
    well["elevation"] = well["elevation"].tolist()
    well["md"] = well["md"].tolist()
well.keys()

calc_well_trend(wells["10"])

with open("reformatted_wells.json", "w") as f:
    json.dump(wells, f)
