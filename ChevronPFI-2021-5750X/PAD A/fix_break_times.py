import json

with open("break_times.json") as f:
    break_times = json.load(f)

with open("break_times_archive.json") as f:
    break_times_archive = json.load(f)

break_times
for well, stage_times in break_times.items():
    for stage, time in stage_times.items():
        if not (time):
            break_times[well][stage] = break_times_archive[well][stage]
break_times

with open("break_times.json", "w") as f:
    json.dump(break_times, f)
