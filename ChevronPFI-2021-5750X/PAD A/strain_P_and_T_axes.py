from datetime import datetime
import json
import os

import matplotlib.pyplot as plt
from matplotlib import cm, colors
import numpy as np
from scipy.ndimage import zoom
from scipy.interpolate import RectBivariateSpline
import pytz

from read_inputs import (
    read_wells,
    read_flipped_catalog,
    read_tops,
    read_velocity_model,
    read_strain,
)
from pfi_qi.QI_analysis import strain_grid, output_isosurface
from generalPlots import gray_background_with_grid
from plotting_routines import plot_perfs
from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge
from sms_moment_tensor.stress_inversions import decompose_stress

TIMEZONE = pytz.timezone("America/Edmonton")
UTC = pytz.utc

GRID_SPACING = 50
MIN_NEIGHBOURS = 5
SEARCH_RADIUS = 200

wells = read_wells()
events = read_flipped_catalog(
    "Catalogs\\relocCatalog_all_QCed_format_after_flip_final_fault_tagged_final_updateFault_170m_final.csv"
)

frac_events = {k: v for k, v in events.items() if v["stage"] != "-1"}
good_frac_mts = {k: v for k, v in frac_events.items() if v["MT conf"] > 0.95}

ref_depth = np.average([np.percentile(well["elevation"], 1) for well in wells.values()])
good_frac_east = [v["easting"] for v in good_frac_mts.values()]
good_frac_north = [v["northing"] for v in good_frac_mts.values()]
east_grid = np.arange(
    min(good_frac_east), max(good_frac_east) + GRID_SPACING, GRID_SPACING
)
north_grid = np.arange(
    min(good_frac_north), max(good_frac_north) + GRID_SPACING, GRID_SPACING
)

velocity_model = read_velocity_model()
strain, grid = strain_grid(
    good_frac_mts,
    GRID_SPACING,
    MIN_NEIGHBOURS,
    SEARCH_RADIUS,
    "total_strain_tensor_out.csv",
    velocity_model,
    tensor=True,
    mw_key="Mw",
)

e_grid = np.unique(grid[:, 0])
n_grid = np.unique(grid[:, 1])
z_grid = np.unique(grid[:, 2])
n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)
quiver_east, quiver_north = [], []
t_axis, p_axis = {"east": [], "north": []}, {"east": [], "north": []}
for i_e, e in enumerate(e_grid):
    for i_n, n in enumerate(n_grid):
        grid_strain = strain[i_e, i_n, 13]
        if np.linalg.det(grid_strain) > 1e-30:
            quiver_east.append(e)
            quiver_north.append(n)
            R, stress_axes = decompose_stress(strain[i_e, i_n, 11, :, :])
            t_axis["east"].append(GRID_SPACING * stress_axes["s1"][0])
            t_axis["north"].append(GRID_SPACING * stress_axes["s1"][1])
            p_axis["east"].append(GRID_SPACING * stress_axes["s3"][0])
            p_axis["north"].append(GRID_SPACING * stress_axes["s3"][1])

color = {"p": "firebrick", "t": "royalblue"}
for pt, axis in zip(["p", "t"], [p_axis, t_axis]):
    fig, ax = plt.subplots(figsize=[16, 16])
    ax.set_aspect("equal")
    for well in wells.values():
        ax.plot(well["easting"], well["northing"], "k")
    ax.quiver(
        quiver_east,
        quiver_north,
        axis["east"],
        axis["north"],
        pivot="mid",
        zorder=5,
        headlength=0,
        headaxislength=0,
        headwidth=0,
        linewidth=0.5,
        color=color[pt],
    )
    gray_background_with_grid(ax)
    fig.savefig(f"{pt}_axis.png")
