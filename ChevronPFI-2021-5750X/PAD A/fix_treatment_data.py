from datetime import datetime
import json
import os
import pytz

UTC = pytz.utc
TIMEZONE = pytz.timezone("America/Edmonton")

from read_inputs import well_lookup, decorate_well_name

with open("treatment_data_old.json") as f:
    old_treatment_data = json.load(f)

well_8_data, well_9_data = {}, {}


def read_treatment_csv(csv):
    well = well_lookup[decorate_well_name(os.path.basename(csv).split("_")[0])]
    f = open(csv)
    _head = f.readline()
    lines = f.readlines()
    f.close()
    treatment_data = {}
    for line in lines:
        lspl = line.split(",")
        stage = int(lspl[8])
        dt = TIMEZONE.localize(datetime.strptime(lspl[0], "%Y-%m-%d %H:%M:%S"))
        utc_timestamp = datetime.strftime(dt.astimezone(UTC), "%Y%m%d%H%M%S.000000")
        treatment_data[utc_timestamp] = {
            "well": well,
            "stage": lspl[8],
            "pressure": float(lspl[10]),
            "slurry_rate": float(lspl[11]),
            "sand_in_formation": float(lspl[7]),
            "downhole_proppant_concentration": float(lspl[19]) if lspl[19] else 0.0,
        }
    return treatment_data


well_lookup

well_8_keys = [k for k, v in old_treatment_data.items() if v["well"] == "8"]
stage_9_37_keys = [
    k for k, v in old_treatment_data.items() if v["well"] == "9" and v["stage"] == "37"
]
{v: k for k, v in well_lookup.items()}["8"]
well_8_data = read_treatment_csv("Treatment Data//100013106118W500_prop_conc.csv")
well_9_data = read_treatment_csv("Treatment Data//100083106118W500_prop_conc.csv")


for key in [*well_8_keys, *stage_9_37_keys]:
    old_treatment_data.pop(key)

new_9_37_data = {k: v for k, v in well_9_data.items() if v["stage"] == "37"}
treatment_data = {**old_treatment_data, **new_9_37_data, **well_8_data}


with open("treatment_data.json", "w") as f:
    json.dump(treatment_data, f)
