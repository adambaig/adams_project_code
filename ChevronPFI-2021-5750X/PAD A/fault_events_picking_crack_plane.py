import json

import matplotlib.pyplot as plt
import numpy as np
import mplstereonet as mpls
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon


from generalPlots import gray_background_with_grid
from mtPlots import plot_general_sdr_rosettes, plot_strike_dip_rake_rosettes_from_MTs
from sms_moment_tensor.MT_math import mt_to_sdr, strike_dip_to_normal
from sms_moment_tensor.plotting import stereonet_plot_stress_tensor, mohr_circle_plot
from sms_moment_tensor.stress_inversions import (
    Michael_stress_inversion,
    resolve_shear_and_normal_stress,
    decompose_stress,
    iterate_Micheal_stress_inversion,
)
from read_inputs import read_initial_catalog, read_noise_mts, read_wells

CUTOFF_PERCENTILE = 95
UPPER_FAULT_MIN_ELEVATION = -2050
LOWER_FAULT_MAX_ELEVATION = -2200
PI = np.pi

with open("fault_polygon_xy.json") as f:
    lower_fault_polygon = Polygon(json.load(f))

with open("fault_polygon_upper.json") as f:
    upper_fault_polygon = Polygon(json.load(f))

with open("all_flipped_events.json") as f:
    events = json.load(f)

noise = read_noise_mts()
wells = read_wells()
mt_r = [v["dc R"] for v in events.values()]
noise_r = [v["dc R"] for v in noise.values()]
cutoff_r = np.percentile(noise_r, CUTOFF_PERCENTILE)
wells = read_wells()
upper_fault_orientation = strike_dip_to_normal(0, 90)
lower_fault_orientation = strike_dip_to_normal(20, 90)
lower_fault_events = {
    k: v
    for k, v in events.items()
    if lower_fault_polygon.contains(Point(v["easting"], v["northing"]))
    and v["elevation"] < LOWER_FAULT_MAX_ELEVATION
    and v["dc R"] > cutoff_r
}
upper_fault_events = {
    k: v
    for k, v in events.items()
    if upper_fault_polygon.contains(Point(v["easting"], v["northing"]))
    and v["elevation"] > UPPER_FAULT_MIN_ELEVATION
    and v["dc R"] > cutoff_r
}

for fault_events, orientation in zip(
    [upper_fault_events, lower_fault_events],
    [upper_fault_orientation, lower_fault_orientation],
):
    for event in fault_events.values():
        strike1, dip1, rake1, strike2, dip2, rake2 = mt_to_sdr(event["dc MT"])
        dot1 = np.dot(orientation, strike_dip_to_normal(strike1, dip1))
        dot2 = np.dot(orientation, strike_dip_to_normal(strike2, dip2))
        if abs(dot1) > abs(dot2):
            event["true strike"] = strike1
            event["true dip"] = dip1
            event["true rake"] = rake1
        else:
            event["true strike"] = strike2
            event["true dip"] = dip2
            event["true rake"] = rake2

circle_x, circle_y = [], []
for i_rad in np.arange(0, PI + PI / 100, PI / 100):
    circle_x.append(np.cos(i_rad))
    circle_y.append(np.sin(i_rad))
circle_x = np.array(circle_x)
circle_y = np.array(circle_y)


colors = {"upper": "forestgreen", "lower": "firebrick"}

alphas = {"upper": 0.8, "lower": 0.05}
stress_inversions = {}
for fault_events, label in zip(
    [upper_fault_events, lower_fault_events], ["upper", "lower"]
):
    n_events = len(fault_events)
    strikes, dips, rakes = np.array(
        [
            (v["true strike"], v["true dip"], v["true rake"])
            for v in fault_events.values()
        ]
    ).T
    stress_inversions[label] = Michael_stress_inversion(strikes, dips, rakes)
    shape_ratio, stress_axes = decompose_stress(stress_inversions[label])
    sigmas, taus = np.zeros(n_events), np.zeros(n_events)
    for i_event, event in enumerate(fault_events.values()):
        normal = strike_dip_to_normal(event["true strike"], event["true dip"])
        sigmas[i_event], taus[i_event] = resolve_shear_and_normal_stress(
            stress_inversions[label], normal
        )
    f_mc, a_mc = plt.subplots(figsize=[8, 4])
    a_mc.set_aspect("equal")
    a_mc.plot(circle_x, circle_y, "0.3", zorder=10)
    a_mc.plot(
        shape_ratio * circle_x + 1 - shape_ratio,
        shape_ratio * circle_y,
        "0.3",
        zorder=10,
    )
    a_mc.plot(
        (1 - shape_ratio) * circle_x - shape_ratio,
        (1 - shape_ratio) * circle_y,
        "0.3",
        zorder=10,
    )
    (p_event,) = a_mc.plot(
        sigmas, taus, "o", color=colors[label], alpha=alphas[label], markeredgecolor="k"
    )
    # a_mc.plot(sigmas_stable, taus_stable, ".", color="lavender", markeredgecolor='k')
    a_mc.arrow(-1.1, 0, 2.2, 0, head_width=0.06, head_length=0.1, fc="k", ec="k")
    a_mc.arrow(-1.1, 0, 0, 1.1, head_width=0.06, head_length=0.1, fc="k", ec="k")
    a_mc.text(-1.1, 1.22, "$\\tau$", ha="center")
    a_mc.text(1.21, 0.0, "${\\sigma}$", va="center")
    a_mc.text(-0.98, -0.1, "$\\sigma_3$")
    a_mc.text(-0.98, -0.1, "$\\sigma_3$")
    a_mc.text(1 - 2 * shape_ratio + 0.03, -0.1, "$\\sigma_2$")
    a_mc.text(1.02, -0.1, "$\\sigma_1$")
    a_mc.axis("off")
    a_mc.set_xlim(-1.3, 1.3)
    a_mc.set_ylim(-0.15, 1.3)
    f_mc.savefig(f"figures//stress_inversion//MC_{label}.png")
    fig1 = stereonet_plot_stress_tensor(stress_inversions[label])
    fig1.savefig(f"figures//stress_inversion//stress_stereonet_{label}.png")
    fig2 = plot_general_sdr_rosettes(
        [v["true strike"] for v in fault_events.values()],
        [v["true dip"] for v in fault_events.values()],
        [v["true rake"] for v in fault_events.values()],
        color=colors[label],
    )
    fig2.savefig(f"figures//stress_inversion//rosette_{label}.png")


all_fault_events = {**upper_fault_events, **lower_fault_events}
strikes, dips, rakes = np.array(
    [
        (v["true strike"], v["true dip"], v["true rake"])
        for v in all_fault_events.values()
    ]
).T
stress_inversion = Michael_stress_inversion(strikes, dips, rakes)

fig, ax = plt.subplots(figsize=[8, 4])
mohr_circle_plot(
    ax,
    lower_fault_events,
    stress_inversion,
    plot_frame=True,
    color="firebrick",
    markeredgecolor="k",
    alpha=0.05,
    zorder=1,
)
mohr_circle_plot(
    ax,
    upper_fault_events,
    stress_inversion,
    plot_frame=False,
    color="forestgreen",
    markeredgecolor="k",
    alpha=0.8,
    zorder=2,
)
fig.savefig(f"figures//stress_inversion//joint_invesion.png")

for event in {**lower_fault_events, **upper_fault_events}.values():
    for key in ["strike", "dip", "rake"]:
        event[key] = event[f"true {key}"]

iterated_stress_tensor, iterations = {}, {}
iterated_stress_tensor["lower"], iterations["lower"] = iterate_Micheal_stress_inversion(
    lower_fault_events, n_iterations=1000, output_iterations=True
)
iterated_stress_tensor["upper"], iterations["upper"] = iterate_Micheal_stress_inversion(
    upper_fault_events, n_iterations=1000, output_iterations=True
)


import json


iterative_results = {
    "stress_tensor": {k: v.tolist() for k, v in iterated_stress_tensor.items()},
    "iterations": iterations,
}


with open("iterative_stress_tensors_faults.json", "w") as f:
    json.dump(iterative_results, f)
