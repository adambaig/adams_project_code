import json

import matplotlib.pyplot as plt
import numpy as np
import mplstereonet as mpls
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from generalPlots import gray_background_with_grid
from sms_moment_tensor.MT_math import mt_to_sdr
from obspy.imaging.mopad_wrapper import beach
from read_inputs import read_initial_catalog, read_noise_mts, read_wells

from mtPlots import plot_strike_dip_rake_rosettes_from_MTs

CUTOFF_PERCENTILE = 95
UPPER_FAULT_MIN_ELEVATION = -2050
LOWER_FAULT_MAX_ELEVATION = -2200

with open("fault_polygon_xy.json") as f:
    lower_fault_polygon = Polygon(json.load(f))

with open("fault_polygon_upper.json") as f:
    upper_fault_polygon = Polygon(json.load(f))

with open("all_flipped_events_including_TM.json") as f:
    events = json.load(f)

events
noise = read_noise_mts()
wells = read_wells()
mt_r = [v["dc R"] for v in events.values()]
noise_r = [v["dc R"] for v in noise.values()]
cutoff_r = np.percentile(noise_r, CUTOFF_PERCENTILE)

wells = read_wells()

{k: v for k, v in events.items() if v["Mw"] > 2.5}

fault_events = {
    k: v
    for k, v in events.items()
    if lower_fault_polygon.contains(Point(v["easting"], v["northing"]))
    and v["elevation"] < LOWER_FAULT_MAX_ELEVATION
}
upper_fault_events = {
    k: v
    for k, v in events.items()
    if upper_fault_polygon.contains(Point(v["easting"], v["northing"]))
    and v["elevation"] > UPPER_FAULT_MIN_ELEVATION
}
not_fault_events = {
    k: v
    for k, v in events.items()
    if not (k in fault_events) and not (k in upper_fault_events)
}

color = {"lower": "firebrick", "upper": "forestgreen", "other": "royalblue"}
for group, label in zip(
    [fault_events, upper_fault_events, not_fault_events], ["lower", "upper", "other"]
):
    fig, ax = plt.subplots(figsize=[12, 12])
    ax.set_aspect("equal")
    for well in wells.values():
        ax.plot(well["easting"], well["northing"], "k")
    for event in group.values():
        if event["dc R"] > cutoff_r:
            strike, dip, rake = mt_to_sdr(event["dc MT"], conjugate=False)
            beachball = beach(
                (strike, dip, rake),
                xy=(event["easting"], event["northing"]),
                width=80,
                facecolor=color[label],
                linewidth=0.25,
            )
            ax.add_collection(beachball)
            ax.plot(event["easting"], event["northing"], ".", alpha=0, zorder=-10)
    gray_background_with_grid(ax)
    fig.savefig(f"{label}_beachballs.png")


fig, ax = plt.subplots(figsize=[12, 12])
ax.set_aspect("equal")
for event in upper_fault_events.values():
    if event["dc R"] > cutoff_r:
        strike, dip, rake = mt_to_sdr(event["dc MT"], conjugate=False)
        beachball = beach(
            (strike, dip, rake),
            xy=(event["easting"], event["northing"]),
            width=80,
            facecolor=color[label],
            linewidth=0.25,
        )
        ax.add_collection(beachball)
        ax.plot(event["easting"], event["northing"], ".", alpha=0, zorder=-10)

x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()

for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k")

ax.set_xlim([x1, x2])
ax.set_ylim([y1, y2])


for group, label in zip(
    [fault_events, upper_fault_events, not_fault_events], ["lower", "upper", "other"]
):
    print(f"{label} {len([v for v in group.values() if v['dc R']>cutoff_r])}")
    fig = plot_strike_dip_rake_rosettes_from_MTs(
        {k: v for k, v in group.items() if v["dc R"] > cutoff_r}, color=color[label]
    )
    fig.savefig(f"{label}_rosettes.png")
