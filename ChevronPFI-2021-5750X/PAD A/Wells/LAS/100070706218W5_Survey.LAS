# WELL TRACE FROM PETREL 
# WELL NAME:              100070706218W500
# DEFINITIVE SURVEY:      Final Directional Survey
# WELL HEAD X-COORDINATE: 520629.59000000 (m)
# WELL HEAD Y-COORDINATE: 6022062.66000000 (m)
# WELL DATUM (KB, KB, from MSL): 906.50000000 (m)
# WELL TYPE:              UNDEFINED
# MD AND TVD ARE REFERENCED (=0) AT WELL DATUM AND INCREASE DOWNWARDS
# ANGLES ARE GIVEN IN DEGREES
# XYZ TRACE IS GIVEN IN COORDINATE SYSTEM CANADA NAD27 (NTRN CAN) UTM11N CM 117W (Canada Western BU NAD27 (NATRAN Canada) UTM11N Lon range 120W - 114W) [Chevron,720354]
# AZIM_TN: azimuth in True North 
# AZIM_GN: azimuth in Grid North 
# DX DY ARE GIVEN IN GRID NORTH IN m-UNITS
# DEPTH (Z, tvd_z) GIVEN IN m-UNITS
#===============================================================================================================================================
      MD            X            Y            Z           TVD           DX          DY        AZIM_TN        INCL         DLS        AZIM_GN
#===============================================================================================================================================
 0.0000000000 520629.59000 6022062.6600 906.50000000 0.0000000000 -0.000000016 -0.000000004 0.0000000000 0.0000000000 0.0000000000 359.74212227
 3251.0000000 520629.59000 6022062.6600 -2344.500000 3251.0000000 -0.000000016 -0.000000004 0.0000000000 0.0000000000 0.0000000000 359.74212227
