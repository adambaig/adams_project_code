from datetime import datetime, timedelta
import glob
import os
import json
import pytz

import numpy as np

from sms_moment_tensor.MT_math import mt_vector_to_matrix

TIMEZONE = pytz.timezone("America/Edmonton")
UTC = pytz.utc
PI = np.pi
ROOT_DIR = "C:\\Users\\adambaig\\Project\\ChevronPFI-2021-5750X\\PAD A\\"


well_lookup = {
    "102/16-30-061-18W5/0": "7",
    "100/01-31-061-18W5/0": "8",
    "100/08-31-061-18W5/0": "9",
    "102/09-31-061-18W5/0": "10",
    "102/16-31-061-18W5/0": "11",
    "102/01-06-062-18W5/0": "12",
    "102/08-06-062-18W5/0": "13",
    "-1": "-1",
}


HP_MIN_CONCENTRATION = 120
LP_MIN_CONCENTRATION = 5


def decorate_well_name(wellname):
    return f"{wellname[:3]}/{wellname[3:5]}-{wellname[5:7]}-{wellname[7:10]}-{wellname[10:12]}W5/0"


def read_wells():
    wells = {}
    well_files = glob.glob(f"{ROOT_DIR}Wells\\*.txt")
    for well_file in well_files:
        f = open(well_file)
        _head = [f.readline() for i in range(17)]
        lines = f.readlines()
        f.close()
        n_lines = len(lines)
        easting, northing, elevation, md = (
            np.zeros(n_lines),
            np.zeros(n_lines),
            np.zeros(n_lines),
            np.zeros(n_lines),
        )
        for i_line, line in enumerate(lines):
            md[i_line], easting[i_line], northing[i_line], elevation[i_line] = [
                float(s) for s in line.split()[:4]
            ]
        wellname = well_lookup[
            decorate_well_name(os.path.basename(well_file).split(".")[0])
        ]
        wells[wellname] = {
            "easting": easting,
            "northing": northing,
            "elevation": elevation,
            "md": md,
        }
    perfs = assign_stages(wells)
    for well in wells.keys():
        wells[well] = {**wells[well], **perfs[well]}
    return wells


def assign_stages(wells):
    f = open(
        os.path.join(
            ROOT_DIR,
            "Treatment Data",
            "Perf Times Summary report data.xlsx - Perf Summary.csv",
        )
    )
    f.readline()
    f.readline()
    lines = f.readlines()
    f.close()
    perfs = {}
    for line in lines:
        lspl = line.split(",")
        well_name = lspl[0].split()[1]
        if well_name not in perfs:
            perfs[well_name] = {}
        timestamp = line.split(",")[5]
        stage = f"Stage {lspl[2]}"
        top_md = float(lspl[6])
        bottom_md = float(lspl[7])
        n_clusters = int(lspl[9])
        perfs[well_name][stage] = {
            "block": {
                "timestamp": timestamp,
                "top_east": np.interp(
                    top_md, wells[well_name]["md"], wells[well_name]["easting"]
                ),
                "top_north": np.interp(
                    top_md, wells[well_name]["md"], wells[well_name]["northing"]
                ),
                "top_elevation": np.interp(
                    top_md, wells[well_name]["md"], wells[well_name]["elevation"]
                ),
                "bottom_east": np.interp(
                    bottom_md, wells[well_name]["md"], wells[well_name]["easting"]
                ),
                "bottom_north": np.interp(
                    bottom_md, wells[well_name]["md"], wells[well_name]["northing"]
                ),
                "bottom_elevation": np.interp(
                    bottom_md, wells[well_name]["md"], wells[well_name]["elevation"]
                ),
            },
            "n_clusters": n_clusters,
        }
    return perfs


def read_treatment():
    treatment_csvs = glob.glob(f"{ROOT_DIR}Treatment Data\\1*_prop_conc.csv")
    treatment_data = {}
    for csv in treatment_csvs:
        well = well_lookup[decorate_well_name(os.path.basename(csv).split("_")[0])]
        f = open(csv)
        _head = f.readline()
        lines = f.readlines()
        f.close()
        for line in lines:
            lspl = line.split(",")
            stage = int(lspl[8])
            dt = TIMEZONE.localize(datetime.strptime(lspl[0], "%Y-%m-%d %H:%M:%S"))
            utc_timestamp = datetime.strftime(dt.astimezone(UTC), "%Y%m%d%H%M%S.000000")
            treatment_data[utc_timestamp] = {
                "well": well,
                "stage": lspl[8],
                "pressure": float(lspl[10]),
                "slurry_rate": float(lspl[11]),
                "sand_in_formation": float(lspl[7]),
                "downhole_proppant_concentration": float(lspl[19]) if lspl[19] else 0.0,
            }
    return treatment_data


def add_missing_stages_to_treatment_json():
    with open("treatment_data.json") as f:
        treatment_data = json.load(f)
    missing_stage_csvs = glob.glob("Treatment Data//10?_*.csv")
    for csv in missing_stage_csvs:
        well = csv.split()[4]
        stage = csv.split()[8].split(".")[0]
        f = open(csv)
        _head = f.readline()
        lines = f.readlines()
        f.close()
        for line in lines:
            lspl = line.split(",")
            dt = TIMEZONE.localize(datetime.strptime(lspl[0], "%Y-%m-%d %H:%M:%S"))
            utc_timestamp = datetime.strftime(dt.astimezone(UTC), "%Y%m%d%H%M%S.000000")
            treatment_data[utc_timestamp] = {
                "well": well,
                "stage": stage,
                "pressure": float(lspl[4]),
                "slurry_rate": float(lspl[6]),
                "sand_in_formation": float(lspl[14]),
                "downhole_proppant_concentration": float(lspl[13]) if lspl[13] else 0.0,
            }
    with open("treatment_data.json", "w") as f:
        json.dump(treatment_data, f)
    return treatment_data


def read_treatment_patch_times():
    treatment_csvs = glob.glob(f"{ROOT_DIR}Treatment Data\\1*_prop_conc.csv")
    treatment_data = {}
    starts = get_treatment_start_times()
    for csv in treatment_csvs:
        well = well_lookup[decorate_well_name(os.path.basename(csv).split("_")[0])]
        f = open(csv)
        _head = f.readline()
        lines = f.readlines()
        f.close()
        old_stage = 0
        for line in lines:
            lspl = line.split(",")
            stage = int(lspl[8])
            if stage > old_stage:
                start_dt = datetime.strptime(
                    starts[f"Well {well} Stage {stage}"], "%Y%m%d%H%M%S.000000"
                )
                i_seconds = 0
                stage_old = stage
            i_seconds += 1
            utc_time = start_dt + timedelta(0, i_seconds)
            utc_timestamp = datetime.strftime(utc_time, "%Y%m%d%H%M%S.000000")
            treatment_data[utc_timestamp] = {
                "well": well,
                "stage": lspl[8],
                "pressure": float(lspl[10]),
                "slurry_rate": float(lspl[11]),
                "sand_in_formation": float(lspl[7]),
                "downhole_proppant_concentration": float(lspl[19]) if lspl[19] else 0.0,
            }
    return treatment_data


def get_treatment_start_times():
    start = {}
    treatment_csvs = glob.glob(f"{ROOT_DIR}Treatment Data\\1*0.csv")
    for csv in treatment_csvs:
        well = well_lookup[decorate_well_name(os.path.basename(csv).split("_")[0])]
        f = open(csv)
        _head = f.readline()
        lines = f.readlines()
        f.close()
        stage_old = 0
        for line in lines:
            lspl = line.split(",")
            stage = int(lspl[8])
            if stage > stage_old:
                dt = TIMEZONE.localize(datetime.strptime(lspl[0], "%Y-%m-%d %H:%M:%S"))
                utc_timestamp = datetime.strftime(
                    dt.astimezone(UTC), "%Y%m%d%H%M%S.000000"
                )
                start[f"Well {well} Stage {stage}"] = utc_timestamp
                stage_old = stage
    return start


def read_initial_catalog(filename):
    events = {}
    f = open(filename)
    _head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        events[event_id] = {
            "time": UTC.localize(datetime.strptime(lspl[1], "%Y-%m-%d %H:%M:%S.%f")),
            "easting": float(lspl[3]),
            "northing": float(lspl[4]),
            "elevation": -float(lspl[5]),
            "snr": float(lspl[6]),
            "stack amp": float(lspl[8]),
            "spectal mw": float(lspl[9]),
            "gen MT": mt_vector_to_matrix([float(s) for s in lspl[12:18]]),
            "gen R": float(lspl[18]),
            "gen CN": float(lspl[19]),
            "dc MT": mt_vector_to_matrix([float(s) for s in lspl[20:26]]),
            "dc R": float(lspl[26]),
            "dc CN": float(lspl[27]),
        }

    return events


def read_template_matching_catalog(filename):
    events = {}
    f = open(filename)
    _head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        events[event_id] = {
            "time": UTC.localize(datetime.strptime(lspl[1], "%Y-%m-%d %H:%M:%S.%f")),
            "easting": float(lspl[3]),
            "northing": float(lspl[4]),
            "elevation": -float(lspl[5]),
            "log snr": float(lspl[6]),
            "easting uncertainty": float(lspl[7]),
            "northing uncertainty": float(lspl[8]),
            "elevation uncertainty": float(lspl[9]),
            "Mw": float(lspl[10]),
            "seismic moment": float(lspl[11]),
            "well": lspl[12],
            "well id": int(lspl[13]),
            "stage": lspl[14],
            "well_stage_id": int(lspl[15]),
            "horizontal distance": float(lspl[16]),
            "total distance": float(lspl[17]),
            "elapsed time": float(lspl[18]),
            "gen MT": mt_vector_to_matrix([float(s) for s in lspl[19:25]]),
            "gen R": float(lspl[25]),
            "gen CN": float(lspl[26]),
            "MT conf": float(lspl[27]),
            "clvd": float(lspl[28]),
            "iso": float(lspl[29]),
            "dc": float(lspl[30]),
            "dc MT": mt_vector_to_matrix([float(s) for s in lspl[31:37]]),
            "dc R": float(lspl[37]),
            "dc CN": float(lspl[38]),
        }

    return events


def read_flipped_catalog(filename):
    events = {}
    f = open(filename)
    _head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        events[event_id] = {
            "time": UTC.localize(datetime.strptime(lspl[1], "%Y-%m-%d %H:%M:%S.%f")),
            "easting": float(lspl[3]),
            "northing": float(lspl[4]),
            "elevation": -float(lspl[5]),
            "log snr": float(lspl[6]),
            "easting uncertainty": float(lspl[7]),
            "northing uncertainty": float(lspl[8]),
            "elevation uncertainty": float(lspl[9]),
            "Mw": float(lspl[10]),
            "seismic moment": float(lspl[11]),
            "well": lspl[12],
            "well id": int(lspl[13]),
            "stage": lspl[14],
            "well_stage_id": int(lspl[15]),
            "horizontal distance": float(lspl[16]),
            "total distance": float(lspl[17]),
            "elapsed time": float(lspl[18]),
            "gen MT": mt_vector_to_matrix([float(s) for s in lspl[19:25]]),
            "gen R": float(lspl[25]),
            "gen CN": float(lspl[26]),
            "clvd": float(lspl[27]),
            "iso": float(lspl[28]),
            "dc": float(lspl[29]),
            "dc MT": mt_vector_to_matrix([float(s) for s in lspl[30:36]]),
            "dc R": float(lspl[36]),
            "dc CN": float(lspl[37]),
            "MT conf": float(lspl[38]),
            "strike": float(lspl[39]),
            "dip": float(lspl[40]),
            "rake": float(lspl[41]),
            "aux strike": float(lspl[42]),
            "aux dip": float(lspl[43]),
            "aux rake": float(lspl[44]),
            "p trend": float(lspl[45]),
            "p plunge": float(lspl[46]),
            "b trend": float(lspl[47]),
            "b plunge": float(lspl[48]),
            "t trend": float(lspl[49]),
            "t plunge": float(lspl[50]),
            "event type": lspl[51],
            "treatment counter": int(lspl[52]),
        }

    return events


def read_final_catalog(filename):
    events = {}
    f = open(filename)
    _head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        events[event_id] = {
            "timestamp": UTC.localize(
                datetime.strptime(lspl[1], "%Y-%m-%d %H:%M:%S.%f")
            ),
            "easting": float(lspl[3]),
            "northing": float(lspl[4]),
            "elevation": -float(lspl[5]),
            "log snr": float(lspl[6]),
            "easting uncertainty": float(lspl[7]),
            "northing uncertainty": float(lspl[8]),
            "elevation uncertainty": float(lspl[9]),
            "Mw": float(lspl[10]),
            "seismic moment": float(lspl[11]),
            "well": well_lookup[lspl[12]],
            "well id": int(lspl[13]),
            "stage": lspl[14],
            "well_stage_id": int(lspl[15]),
            "horizontal distance": float(lspl[16]),
            "total distance": float(lspl[17]),
            "elapsed time": float(lspl[18]),
            "gen MT": mt_vector_to_matrix([float(s) for s in lspl[19:25]]),
            "gen R": float(lspl[25]),
            "gen CN": float(lspl[26]),
            "clvd": float(lspl[27]),
            "iso": float(lspl[28]),
            "dc": float(lspl[29]),
            "dc MT": mt_vector_to_matrix([float(s) for s in lspl[30:36]]),
            "dc R": float(lspl[36]),
            "dc CN": float(lspl[37]),
            "MT conf": float(lspl[38]),
            "strike": float(lspl[39]),
            "dip": float(lspl[40]),
            "rake": float(lspl[41]),
            "aux strike": float(lspl[42]),
            "aux dip": float(lspl[43]),
            "aux rake": float(lspl[44]),
            "p trend": float(lspl[45]),
            "p plunge": float(lspl[46]),
            "b trend": float(lspl[47]),
            "b plunge": float(lspl[48]),
            "t trend": float(lspl[49]),
            "t plunge": float(lspl[50]),
            "event type": lspl[51].strip("\n"),
        }

    return events


def read_noise_mts(filename=r"Catalogs\relocCatalogNoise_all.csv"):
    noise = {}
    f = open(filename)
    _head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        noise_id = lspl[0]
        noise[noise_id] = {
            "dc MT": mt_vector_to_matrix([float(s) for s in lspl[14:20]]),
            "dc R": float(lspl[20]),
        }
    return noise


def classify_treatment(treatment_data_for_stage, tzero=None, interval=60):
    from scipy.signal import medfilt

    EPS = 1e-7
    if not tzero:
        tzero = pick_breakdown_time(treatment_data_for_stage)
    proppant = np.array(
        [
            v["downhole_proppant_concentration"]
            for v in treatment_data_for_stage.values()
        ]
    )
    times = np.array(list(treatment_data_for_stage.keys()))
    i_zero = np.where(times == tzero)[0][0]
    med_proppant = medfilt(proppant, 111)
    zone = [
        {"counter": 0, "state": "PRE"},
        {"time": tzero, "counter": 1, "state": "FL"},
    ]
    threshold_1 = LP_MIN_CONCENTRATION
    threshold_2 = HP_MIN_CONCENTRATION
    check_thresh1 = med_proppant - threshold_1
    check_thresh2 = med_proppant - threshold_2
    counter = 1

    for i_time, time in enumerate(times[i_zero::interval]):
        ii = i_time * interval + i_zero
        if (
            check_thresh1[ii] * check_thresh1[ii - interval] < EPS
            or check_thresh2[ii] * check_thresh2[ii - interval] < EPS
        ):
            counter += 1
            zone.append(zone_state(time, counter, med_proppant[ii]))

    zone[-1]["state"] = "POST"
    return zone


def classify_events_by_treatment(
    stage_events, stage_data, break_time, diffusivity=None
):
    if len(stage_events) == 0:
        return None
    treatment_zones = classify_treatment(stage_data, tzero=break_time)
    for event_id, event in stage_events.items():
        zone = in_zone(treatment_zones, event["utc datetime"])
        event["treatment_counter"] = zone["counter"]
        event["treatment_code"] = zone["state"]

    if diffusivity is not None:
        hrz_diff = diffusivity["horizontal"]
        t0 = diffusivity["t0"]
        for event in stage_events.values():
            hrz_diffusivity = (
                event["along trend"] ** 2 / (event["timestamp"] - t0).seconds / 4 / PI
            )
            if hrz_diffusivity > hrz_diff:
                event["treatment_code"] = "IS"
    return None


def in_zone(zones, timestamp):
    z_times = np.array(
        [datetime.strptime(z["time"], "%Y%m%d%H%M%S.%f") for z in zones[1:]]
    )
    i_zone = np.where(z_times < datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ"))[
        0
    ]
    if len(i_zone) == 0:
        return zones[0]
    return zones[i_zone[-1] + 1]


def zone_state(time, counter, med_proppant):
    if med_proppant > HP_MIN_CONCENTRATION:
        return {
            "time": time,
            "counter": counter,
            "state": "HP",
        }
    if med_proppant > LP_MIN_CONCENTRATION:
        return {
            "time": time,
            "counter": counter,
            "state": "LP",
        }

    return {
        "time": time,
        "counter": counter,
        "state": "FL",
    }


def read_tops():
    TOPS_DIR = "Horizons\\"
    tops = {}
    for tops_file in glob.glob(f"{TOPS_DIR}*.shx"):
        f = open(tops_file)
        header = [f.readline() for i in range(20)]
        lines = f.readlines()
        f.close()
        top = os.path.basename(tops_file).split(".")[0].lower()
        east_indexes = np.unique([int(line.split()[3]) for line in lines])
        north_indexes = np.unique([int(line.split()[4]) for line in lines])
        n_east, n_north = len(east_indexes), len(north_indexes)
        mean_tvdss = np.average([float(line.split()[2]) for line in lines])
        tops[top] = {
            "top_grid": -mean_tvdss * np.ones([n_east, n_north]),
            "e_grid": np.zeros(n_east),
            "n_grid": np.zeros(n_north),
        }
        for line in lines:
            lspl = line.split()
            i_east = np.argwhere(east_indexes == int(lspl[3]))[0][0]
            i_north = np.argwhere(north_indexes == int(lspl[4]))[0][0]
            tops[top]["top_grid"][i_east, i_north] = -float(lspl[2])
            tops[top]["e_grid"][i_east] = float(lspl[0])
            tops[top]["n_grid"][i_north] = float(lspl[1])
    return tops


def read_velocity_model():
    with open("velocity model\PAD_15-01_final-optimal_Vel.csv") as f:
        _head = f.readline()
        lines = f.readlines()
    velocity_model = []
    for line in lines:
        lspl = line.split(",")
        top = -float(lspl[0])
        vp = float(lspl[1])
        velocity_model.append(
            {"top": top, "vp": vp, "vs": vp / np.sqrt(3), "rho": 310 * vp**0.25}
        )
    return velocity_model


def read_strain(strain_file, tensor=False):
    f = open(strain_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    n_lines = len(lines)
    grid_points = np.zeros([n_lines, 3])
    if not tensor:
        strain_flat = np.zeros(n_lines)
    else:
        strain_flat = np.zeros([n_lines, 6])
    for i_line, line in enumerate(lines):
        lspl = line.split(",")
        grid_points[i_line, :] = [float(s) for s in lspl[:3]]
        if not tensor:
            strain_flat[i_line] = float(lspl[3])
        else:
            strain_flat[i_line, :] = [float(s) for s in lspl[3:]]
    e_grid = np.unique(grid_points[:, 0])
    n_grid = np.unique(grid_points[:, 1])
    z_grid = np.unique(grid_points[:, 2])
    n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)

    return np.reshape(strain_flat, [n_e, n_n, n_z]), grid_points
