from datetime import datetime
import json
import os

import matplotlib.pyplot as plt
from matplotlib import cm, colors
import numpy as np
from scipy.ndimage import zoom
from scipy.interpolate import RectBivariateSpline
import pytz

from read_inputs import (
    read_wells,
    read_flipped_catalog,
    read_tops,
    read_velocity_model,
    read_strain,
)
from pfi_qi.QI_analysis import strain_grid, output_isosurface
from generalPlots import gray_background_with_grid
from plotting_routines import plot_perfs


TIMEZONE = pytz.timezone("America/Edmonton")
UTC = pytz.utc

GRID_SPACING = 40
MIN_NEIGHBOURS = 5
SEARCH_RADIUS = 80
GRID_DIR = "strain_grids"
MOVIE_DIR = "strain_movie"
tops = read_tops()
duvernay = RectBivariateSpline(
    tops["duvernay"]["e_grid"], tops["duvernay"]["n_grid"], tops["duvernay"]["top_grid"]
)
duvernay_base = RectBivariateSpline(
    tops["duvernay_base"]["e_grid"],
    tops["duvernay_base"]["n_grid"],
    tops["duvernay_base"]["top_grid"],
)
events = read_flipped_catalog(
    "Catalogs\\relocCatalog_all_QCed_format_after_flip_final_fault_tagged_final_updateFault_170m_final.csv"
)
wells = read_wells()


def _add_datetimes(dictionary):
    for key in [k for k in dictionary.keys() if k.split("_")[-1] == "time"]:
        new_key = "_".join(key.split("_")[:1]) + "_datetime"
        yyyymmddhhmmss, decimal_seconds = str(dictionary[key]).split(".")
        microseconds = decimal_seconds.ljust(6, "0")
        dictionary[new_key] = UTC.localize(
            datetime.strptime(yyyymmddhhmmss + microseconds, "%Y%m%d%H%M%S%f")
        )


with open("sorted_stage_list.json") as f:
    sorted_stages = json.load(f)

for event in events.values():
    event["magnitude"] = event["Mw"]


frac_events = {k: v for k, v in events.items() if v["stage"] != "-1"}
complete_events = {k: v for k, v in frac_events.items() if v["Mw"] > -1.05}
velocity_model = read_velocity_model()

strain, grid = strain_grid(
    complete_events,
    GRID_SPACING,
    MIN_NEIGHBOURS,
    SEARCH_RADIUS,
    os.path.join(GRID_DIR, "total_strain_magnitude_out.csv"),
    velocity_model,
)

# strain, grid = read_strain("strain_grids\\strain_magnitude_out_0328.csv")


e_grid = np.unique(grid[:, 0])
n_grid = np.unique(grid[:, 1])
z_grid = np.unique(grid[:, 2])
n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)
vertexes, volume_tags, volume = output_isosurface(
    strain, 1e-9, grid, os.path.join(GRID_DIR, "total_isosurface.csv")
)
strain_flat = strain.reshape([n_e * n_n * n_z])
non_zero_strain = [v for v in strain_flat if v > 1e-12]
vmin = -9
vmax = np.percentile(np.log10(non_zero_strain), 99.9)
color_scale = cm.ScalarMappable(
    norm=colors.Normalize(vmin=vmin, vmax=vmax), cmap="inferno_r"
)
strain_interpolate = zoom(strain, 4)
depth_interp_factor = 10
strain_depth_interpolate = zoom(strain, [1, 1, depth_interp_factor])
z_grid_interp = zoom(z_grid, depth_interp_factor)
in_duvernay = np.zeros([n_e, n_n, depth_interp_factor * n_z])
for i_e, e in enumerate(e_grid):
    for i_n, n in enumerate(n_grid):
        duvernay_top = duvernay(e, n)
        duvernay_bottom = duvernay_base(e, n)
        for i_z, z in enumerate(z_grid_interp):
            if z > duvernay_bottom and z < duvernay_top:
                in_duvernay[i_e, i_n, i_z] = 1

duvernay_volume = (
    len(np.where(strain_depth_interpolate * in_duvernay > 10**vmin)[0])
    * GRID_SPACING**3
    / depth_interp_factor
)


def _plot_strain_contour(strain_slice):
    figure, axis = plt.subplots(figsize=[16, 16])
    axis.set_aspect("equal")
    contour_colors = []
    levels = np.linspace(vmin, vmax, 9)
    for level in levels:
        contour_colors.append(color_scale.to_rgba(level))

    contours = axis.contourf(
        e_grid,
        n_grid,
        np.log10(strain_slice).T,
        vmin=vmin,
        levels=levels,
        vmax=vmax,
        colors=contour_colors,
        extend="max",
    )
    for well in wells.values():
        axis.plot(well["easting"], well["northing"], "k")
    gray_background_with_grid(axis)
    cb = figure.colorbar(contours)
    ticks = cb.get_ticks()
    cb.set_ticklabels([f"{10**tick:.1e}".replace("-", "$-$") for tick in ticks])
    cb.ax.tick_params(labelsize=20)
    cb.set_label("strain", fontsize=24)
    return figure


fig = _plot_strain_contour(strain[:, :, 19])

x1, x2 = fig.axes[0].get_xlim()
y1, y2 = fig.axes[0].get_ylim()

ax = fig.axes[0]
ax.set_xlim([x1, x2])
ax.set_ylim([y1, y2])
ax.text(
    0.1,
    0.20,
    f"{duvernay_volume/10**8:.3f} X10$^8$ m$^3$ in Duvernay",
    fontsize=20,
    ha="left",
    transform=ax.transAxes,
)

fig.savefig(os.path.join(MOVIE_DIR, "strain_rough_all_frac.png"))

for i_stage, stage in enumerate(sorted_stages):
    _add_datetimes(stage)

    events_up_to_stage_inclusive = {
        k: v for k, v in complete_events.items() if v["time"] < stage["end_datetime"]
    }
    stage_number = str(i_stage).zfill(4)
    if len(events_up_to_stage_inclusive) > 0:
        strain_file = os.path.join(GRID_DIR, f"strain_magnitude_out_{stage_number}.csv")
        if os.path.isfile(strain_file):
            cumulative_strain, samegrid = read_strain(strain_file)
        else:
            cumulative_strain, same_grid = strain_grid(
                events_up_to_stage_inclusive,
                grid,
                5,
                80,
                strain_file,
                velocity_model,
            )
        try:
            vertexes, volume_tags, volume = output_isosurface(
                cumulative_strain_depth_interp,
                1e-9,
                grid,
                os.path.join(GRID_DIR, f"strain_isosurface_{stage_number}.csv"),
            )
        except:
            pass
    else:
        cumulative_strain = np.zeros(strain.shape)

    cumulative_strain_depth_interp = zoom(
        cumulative_strain, [1, 1, depth_interp_factor]
    )
    duvernay_volume = (
        len(np.where(cumulative_strain_depth_interp * in_duvernay > 10**vmin)[0])
        * GRID_SPACING**3
        / depth_interp_factor
    )

    fig_cumulative = _plot_strain_contour(cumulative_strain[:, :, 19])

    ax = fig_cumulative.axes[0]
    ax.set_xlim([x1, x2])
    ax.set_ylim([y1, y2])
    ax.text(
        0.1,
        0.3,
        f"Well {stage['well']}: Stage {stage['stage']}",
        ha="left",
        transform=ax.transAxes,
        fontsize=20,
    )
    ax.text(
        0.1,
        0.25,
        datetime.strftime(stage["end_datetime"], "%b %d, %Y, %H:%M:%S"),
        fontsize=20,
        ha="left",
        transform=ax.transAxes,
    )
    ax.text(
        0.1,
        0.20,
        f"{duvernay_volume/10**8:.3f} X10$^8$ m$^3$ in Duvernay",
        fontsize=20,
        ha="left",
        transform=ax.transAxes,
    )

    plot_perfs(
        ax, wells[stage["well"]][f'Stage {stage["stage"]}'], s=200, color="k", zorder=23
    )
    plot_perfs(
        ax,
        wells[stage["well"]][f'Stage {stage["stage"]}'],
        s=120,
        color="orangered",
        zorder=24,
    )

    for previous_stage in sorted_stages[:i_stage]:
        plot_perfs(
            ax,
            wells[previous_stage["well"]][f'Stage {previous_stage["stage"]}'],
            s=100,
            color="0.3",
            zorder=22,
            alpha=0.3,
        )

    fig_cumulative.savefig(os.path.join(MOVIE_DIR, f"cumulative_strain_{stage_number}"))
    plt.close(fig)
