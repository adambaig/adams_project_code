# import matplotlib
#
# matplotlib.use("Qt5agg")
from copy import deepcopy
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import pytz

from pfi_qi.QI_analysis import (
    parent_child,
    rt_diffusivity,
    fracture_dimensions,
    strain_grid,
    output_isosurface,
    rotate_all_events_to_well_coords,
)
from pfi_qi.engineering import (
    sorted_stage_list,
    breaktimes_by_stage,
    find_perf_center,
    calc_well_trend,
    make_unique_stage_list,
)
from pfi_qi.rotations import rotate_from_cardinal
from sms_moment_tensor.MT_math import decompose_MT, clvd_iso_dc, mt_to_sdr

from read_inputs import (
    read_events,
    read_catalog,
    get_velocity_model,
    read_wells,
    read_treatment,
    read_or_load_json,
    read_diverter_drops,
    read_parent_stages_2016,
    classify_treatment,
    classify_events_by_treatment,
    write_catalog,
)
from plotting_stuff import (
    rt_with_treatment,
    plot_rotated_stage,
    plot_stage,
    add_dimension_arrows_abs_coords,
)

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")

treatment = read_or_load_json(read_treatment, "treatment_data.json")

sorted_stages = read_or_load_json(
    sorted_stage_list, "sorted_stage_list.json", treatment
)
break_times = read_or_load_json(
    breaktimes_by_stage, "break_times.json", sorted_stages, treatment
)
velocity_model = get_velocity_model()
events = read_catalog("Catalogs//events_with_missing_included.csv")
wells = read_wells()
diverter_drops = read_diverter_drops()

magnitude_of_completeness = -1.25

parent_stages_2016 = read_parent_stages_2016()

# This block is dealing with the split stage.

split_stage = {
    k: v for k, v in events.items() if v["well"] == "1" and v["stage"] == "05"
}
for event_id, event in split_stage.items():
    if event["timestamp"] < utc.localize(datetime(2019, 12, 9, 9, 0)):
        event["stage"] = "05.0"
    else:
        event["stage"] = "05.5"
pump_wells = deepcopy(wells)
pump_wells["1"]["Stage 05.0"] = pump_wells["1"]["Stage 05"]
pump_wells["1"]["Stage 05.5"] = pump_wells["1"]["Stage 05"]
pump_wells["1"].pop("Stage 05")


# find frac events (still may have some IS), calculate diffusivity

frac_events = {k: v for k, v in events.items() if v["well"] != "-1"}


unique_stages = make_unique_stage_list(sorted_stages)
diffusivity = rt_diffusivity(
    frac_events,
    pump_wells,
    break_times,
    csv_out="rt_curve.csv",
    unique_stages=unique_stages,
    out_timezone=timezone,
)

for stage in sorted_stages:
    stage_name = stage["stage"]
    well = stage["well"]
    stage_events = {
        k: v
        for k, v in frac_events.items()
        if v["well"] == well and v["stage"] == stage_name
    }
    if len(stage_events) == 0:
        continue
    stage_data = {
        k: v
        for k, v in treatment.items()
        if v["well"] == well and v["stage"] == stage_name
    }
    well_stage_identifier = f"Well {well}: Stage {stage_name}"
    if diffusivity[well_stage_identifier]["horizontal"]:
        classify_events_by_treatment(
            stage_events, stage_data, diffusivity=diffusivity[well_stage_identifier]
        )
    else:
        classify_events_by_treatment(stage_events, stage_data)


## identify all IS events and exlude them from calculations of
# frac_dimensions and parent_child table`

events_without_is = {
    k: v
    for k, v in frac_events.items()
    if v["treatment_code"] not in ["IS", "PRE", "POST"]
}
complete_events = {
    k: v
    for k, v in events_without_is.items()
    if v["magnitude"] > magnitude_of_completeness
}

frac_dimensions = fracture_dimensions(
    complete_events,
    pump_wells,
    csv_out="frac_dimensions.csv",
    unique_stages=unique_stages,
    return_arrows_too=True,
)


parent_child(events_without_is, pump_wells, sorted_stages, csv_out_dir="parent_child//")
parent_child(
    events_without_is,
    pump_wells,
    sorted_stages,
    other_parents=parent_stages_2016,
    csv_out_dir="parent_2016_child//",
)

rotate_all_events_to_well_coords(events_without_is, pump_wells)
# make rt plots

for stage in sorted_stages:
    stage_name = stage["stage"]
    well = stage["well"]
    stage_events = {
        k: v
        for k, v in frac_events.items()
        if v["well"] == well and v["stage"] == stage_name
    }
    well_stage_identifier = f"Well {well}: Stage {stage_name}"
    if (
        well_stage_identifier in diffusivity
        and diffusivity[well_stage_identifier]["horizontal"]
    ):
        stage_data = {
            k: v
            for k, v in treatment.items()
            if v["well"] == well and v["stage"] == stage_name
        }
        if well == "1" and stage_name in [v["stage"] for v in diverter_drops.values()]:
            drops = {
                k: v for k, v in diverter_drops.items() if v["stage"] == stage_name
            }
        else:
            drops = None
        fig1 = rt_with_treatment(
            stage_events,
            stage_data,
            diffusivity[well_stage_identifier]["horizontal"],
            diffusivity[well_stage_identifier]["t0"],
            title=well_stage_identifier,
            diverter=drops,
        )
        fig1.savefig(f"figures//rt_plots//rt_w{well}_s{stage_name}.png")
        plt.close(fig1)


for stage in sorted_stages:
    stage_name = stage["stage"]
    well = stage["well"]
    well_stage_identifier = f"Well {well}: Stage {stage_name}"
    stage_events = {
        k: v
        for k, v in frac_events.items()
        if v["well"] == well
        and v["stage"] == stage_name
        and v["magnitude"] > magnitude_of_completeness
    }
    if well_stage_identifier in frac_dimensions:
        fig_en = plot_stage(stage_events, pump_wells)
        ax = add_dimension_arrows_abs_coords(
            fig_en.axes[0], frac_dimensions[well_stage_identifier]["arrows"]
        )
        fig_en.savefig(
            f"figures//Cardinal stages with dimensions//en_w{well}_s{stage_name}.png"
        )
        plt.close(fig_en)


for event in [
    v for v in events.values() if v["well"] == "-1" and v["treatment_code"] == "XX"
]:
    event["treatment_code"] = "IS"
    event["treatment_counter"] = -1
    event["n_diverters"] = 0


write_catalog(events, "Catalogs//Chevron_15_06_May10_2020.csv")
