from datetime import datetime
import json
import pytz

import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np

from pfi_qi.engineering import pick_breakdown_time

TIMEZONE = pytz.timezone("America/Edmonton")
UTC = pytz.utc

with open("treatment_data.json") as f:
    treatment_data = json.load(f)


with open("break_times.json") as f:
    break_times = json.load(f)


def plot_stage(treatment_data_for_stage, utc_break_time):
    utc_times = [
        UTC.localize(datetime.strptime(t, "%Y%m%d%H%M%S.%f"))
        for t in treatment_data_for_stage.keys()
    ]
    break_time = UTC.localize(datetime.strptime(utc_break_time, "%Y%m%d%H%M%S.%f"))
    slurry = (
        np.array([v["slurry_rate"] for v in treatment_data_for_stage.values()]) / 20.0
    )
    proppant = (
        np.array(
            [
                v["downhole_proppant_concentration"]
                for v in treatment_data_for_stage.values()
            ]
        )
        / 500.0
    )
    pressure = (
        np.array([v["pressure"] for v in treatment_data_for_stage.values()]) / 100.0
    )
    fig = plt.figure(figsize=[12, 6])
    ax2 = fig.add_axes([0.1, 0.1, 0.7, 0.8])
    ax4 = fig.add_axes([0.9, 0.1, 0, 0.8])
    ax2.plot(utc_times, proppant, c="darkgoldenrod", lw=2)
    ax2.plot(utc_times, pressure, c="firebrick", lw=2)
    ax2.plot([break_time, break_time], [0, 1], "k")
    ax2.set_ylabel("pressure (MPa)", color="firebrick")
    ax2.set_yticks(np.linspace(0, 1, 6))
    ax2.set_yticklabels(np.linspace(0, 100, 6))
    ax3 = ax2.twinx()
    ax3.plot(utc_times, slurry, c="royalblue", lw=2)
    ax3.set_ylabel("slurry rate (m$^3$/min)", color="royalblue")
    ax3.set_yticks(np.linspace(0, 1, 5))
    ax3.set_yticklabels(np.linspace(0, 20, 5))
    ax4.yaxis.set_ticks_position("right")
    ax4.yaxis.set_label_position("right")
    ax4.set_yticks(np.linspace(0, 1, 6))
    ax4.set_yticklabels(np.linspace(0, 500, 6))
    ax4.set_xticks([])
    ax4.set_ylabel("Proppant Concentration (kg/m$^3$)", color="darkgoldenrod")
    ax2.xaxis.set_major_formatter(md.DateFormatter("%b %d\n%H:%M", tz=TIMEZONE))
    ax2.set_xlabel("local time")
    return fig


missing_well_stages = ["8_25", "12_29"]
for well_stage in missing_well_stages:
    well, stage = well_stage.split("_")
    treatment_data_for_stage = {
        k: v
        for k, v in treatment_data.items()
        if v["well"] == well and v["stage"] == stage
    }
    break_times[well][stage] = pick_breakdown_time(treatment_data_for_stage)
    fig = plot_stage(treatment_data_for_stage, break_times[well][stage])
    fig.savefig(
        f"figures//treatment_plots//treatment_data_well_{well.zfill(2)}_{stage.zfill(2)}.png"
    )


with open("break_times.json", "w") as f:
    json.dump(break_times, f)
