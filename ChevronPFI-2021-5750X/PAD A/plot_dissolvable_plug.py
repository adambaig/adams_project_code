from datetime import datetime
import json
import os
import pytz

from matplotlib import cm, colors
import matplotlib.pyplot as plt
from matplotlib import patches
import numpy as np
from read_inputs import read_wells, read_flipped_catalog
from plotting_routines import (
    plot_perfs,
    plot_stage,
    plot_events,
    plot_wells,
    get_frame,
    set_frame,
)
from generalPlots import gray_background_with_grid
from pfi_qi.engineering import calc_well_trend, find_perf_center
from pfi_qi.rotations import rotate_from_cardinal

UTC = pytz.utc
FIG_DIR = r"figures\disolvable_plug_stages"

events = read_flipped_catalog(
    "Catalogs\\relocCatalog_all_QCed_format_after_flip_final_fault_tagged_final_updateFault_170m_final.csv"
)
wells = read_wells()


def _add_datetimes(dictionary):
    for key in [k for k in dictionary.keys() if k.split("_")[-1] == "time"]:
        new_key = "_".join(key.split("_")[:1]) + "_datetime"
        yyyymmddhhmmss, decimal_seconds = str(dictionary[key]).split(".")
        microseconds = decimal_seconds.ljust(6, "0")
        dictionary[new_key] = UTC.localize(
            datetime.strptime(yyyymmddhhmmss + microseconds, "%Y%m%d%H%M%S%f")
        )


with open("sorted_stage_list.json") as f:
    sorted_stages = json.load(f)
for stage in sorted_stages:
    _add_datetimes(stage)


well = 9
dissolvable_plug_stages = [
    "11",
    "17",
    "20",
    "31",
    "37",
    "40",
    "42",
    "43",
    "44",
    "45",
    "46",
    "52",
    "53",
    "56",
]
previous_colors = ["0.3", "0.7"]
well_9_stages = np.unique([v["stage"] for v in well_events.values()])
for stage in well_9_stages:
    found_stage = [
        s for s in sorted_stages if s["well"] == str(well) and s["stage"] == stage
    ][0]
    stage_events = {
        k: v for k, v in events.items() if v["well id"] == well and v["stage"] == stage
    }
    other_events = {
        k: v
        for k, v in events.items()
        if v["time"] < found_stage["end_datetime"]
        and v["time"] > found_stage["start_datetime"]
        and v["stage"] == "-1"
    }
    perf_data = wells[str(well)][f"Stage {stage}"]
    fig, ax = plt.subplots()
    ax.set_aspect("equal")
    plot_events(ax, stage_events, marker="o", color="firebrick", linestyle="None")
    plot_perfs(ax, perf_data, color="orangered", s=200, zorder=3, edgecolor="k")
    frame = get_frame(ax)
    plot_wells(ax, wells, color="k", zorder=1)
    for previous_stage in range(1, int(stage)):
        previous_perf_data = wells[str(well)][f"Stage {previous_stage}"]
        plot_perfs(
            ax,
            previous_perf_data,
            color=previous_colors[previous_stage % 2],
            s=150,
            zorder=3,
            edgecolor="k",
        )
    # plot_events(ax, other_events, marker="o", color="0.8", linestyle="None", markeredgecolor="k")
    set_frame(ax, frame)
    gray_background_with_grid(ax, grid_spacing=25)
    ax.set_title(f"Well {well}, Stage {stage}")
    # fig.savefig(os.path.join(FIG_DIR, f'well_{str(well).zfill(2)}_stage_{stage.zfill(2)}.png'),bbox_inches='tight')

well_events = {k: v for k, v in events.items() if v["well id"] == well}


well_trend_rad = calc_well_trend(wells["9"])
well_9_stages = np.unique([v["stage"] for v in well_events.values()])
for stage in well_9_stages:
    stage_events = {
        k: v for k, v in events.items() if v["well id"] == well and v["stage"] == stage
    }
    center = find_perf_center(stage, wells["9"])
    rotate_from_cardinal(stage_events, well_trend_rad, center)

along_well_bins = np.linspace(-170, 171, 35)
fig, axes = plt.subplots(3, figsize=[6, 12], sharex=True)

dissolvable_plug_worked = ["11", "20"]
dissolvable_plug_failed = [
    v for v in dissolvable_plug_stages if v not in dissolvable_plug_worked
]
other_well_9_stages = [v for v in well_9_stages if v not in dissolvable_plug_stages]

colors = ["firebrick", "forestgreen", "royalblue"]
stage_width = 53
for ax, stage_group, color in zip(
    axes,
    [dissolvable_plug_failed, dissolvable_plug_worked, other_well_9_stages],
    colors,
):
    along_well = [
        v["along trend"] for v in well_events.values() if v["stage"] in stage_group
    ]
    ax.hist(
        along_well,
        bins=along_well_bins,
        color=color,
        edgecolor="0.2",
        zorder=3,
    )
    y1, y2 = ax.get_ylim()
    zone_rectangle = plt.Rectangle(
        (-stage_width / 2, y1),
        stage_width,
        y2 - y1,
        facecolor="0.7",
        edgecolor="0.3",
        zorder=1,
    )
    ax.add_patch(zone_rectangle)
    print(np.median(along_well))
    # ax.add_text(0.7, 0.)

axes[0].set_title("Suspect stages where plug failure looks to have occurred")
axes[1].set_title("Suspect stages where plug may have held (11 and 20)")
axes[2].set_title("Other stages on well 9")
axes[2].set_xlabel(
    "toe          distance along well from perf center (m)          heel"
)
fig.text(
    0.04,
    0.5,
    "numbers of frac-assocated microseismic events",
    rotation=90,
    va="center",
    fontsize=14,
)
fig.savefig("well_9_dissolvable_plug.png", bbox_inches="tight")
