from read_inputs import (
    read_wells,
    read_treatment,
    classify_events_by_treatment,
    read_flipped_catalog,
)
from plotting_routines import (
    rt_with_treatment,
    plot_stage,
    add_dimension_arrows_abs_coords,
)
from datetime import datetime, timedelta
import json
import os
import sys

import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import pytz
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

from pfi_qi.QI_analysis import (
    parent_child,
    rt_diffusivity,
    fracture_dimensions,
    strain_grid,
    output_isosurface,
    rotate_all_events_to_well_coords,
)

from pfi_qi.engineering import (
    sorted_stage_list,
    breaktimes_by_stage,
    find_perf_center,
    calc_well_trend,
    make_unique_stage_list,
)
from pfi_qi.rotations import rotate_from_cardinal


def read_or_load_json(read_function, json_file, *args):
    if os.path.isfile(json_file):
        with open(json_file, "r") as read_file:
            dictionary = json.load(read_file)
        return dictionary
    dictionary = read_function(*args)
    with open(json_file, "w") as write_file:
        json.dump(dictionary, write_file)
    return dictionary


PI = np.pi
D2R = PI / 180.0
UTC = pytz.utc
TIMEZONE = pytz.timezone("America/Edmonton")
UPPER_FAULT_MIN_ELEVATION = -2050
LOWER_FAULT_MAX_ELEVATION = -2200
magnitude_of_completeness = -1.05


dir_path = os.path.dirname(os.path.realpath(__file__))
if not os.path.isdir(os.path.join(dir_path, "figures")):
    os.makedirs(os.path.join(dir_path, "figures"))
if not os.path.isdir(os.path.join(dir_path, "figures", "rt_plots")):
    os.makedirs(os.path.join(dir_path, "figures", "rt_plots"))
if not os.path.isdir(
    os.path.join(dir_path, "figures", "Cardinal stages with dimensions")
):
    os.makedirs(os.path.join(dir_path, "figures", "Cardinal stages with dimensions"))


wells = read_wells()
treatment = read_or_load_json(read_treatment, "treatment_data.json")
sorted_stages = read_or_load_json(
    sorted_stage_list, "sorted_stage_list.json", treatment
)
break_times = read_or_load_json(
    breaktimes_by_stage, "break_times.json", sorted_stages, treatment
)


wells["9"]

inputCatalog = r"Catalogs\final_catalog.csv"
events = read_flipped_catalog(inputCatalog)

list(events.values())[0]

for event in events.values():
    event["utc datetime"] = event["time"]
    event["timestamp"] = event["utc datetime"]
    if event["event type"] == "Frac Event":
        event["treatment_code"] = "IS"
    elif event["event type"] == "Lower Fault Event":
        event["treatment_code"] = "Lower Fault Event"
    elif event["event type"] == "Upper Fault Event":
        event["treatment_code"] = "Upper Fault Event"
    event["treatment_counter"] = -1
    temp_well_name = event["well"]
    event["well"] = str(event["well id"])
    event["well id"] = str(temp_well_name)
    #

frac_events = {
    k: v
    for k, v in events.items()
    if v["stage"] != "-1" and v["horizontal distance"] < 170.0
}

print(len(frac_events))

diffusivity = rt_diffusivity(
    frac_events, wells, break_times, csv_out="diffusivity_out.csv"
)


# add event classification tags
for stage in sorted_stages:
    stage_name = stage["stage"]
    well = stage["well"]
    stage_events = {
        k: v
        for k, v in frac_events.items()
        if v["well"] == well and v["stage"] == stage_name
    }
    if len(stage_events) == 0:
        print("Well {} stage {} has no events".format(well, stage_name))
        continue
    stage_data = {
        k: v
        for k, v in treatment.items()
        if v["well"] == well and v["stage"] == stage_name
    }
    well_stage_identifier = f"Well {well}: Stage {stage_name}"
    if "horizontal" in diffusivity[well_stage_identifier]:
        classify_events_by_treatment(
            stage_events,
            stage_data,
            break_times[well][stage_name],
            diffusivity=diffusivity[well_stage_identifier],
        )
    else:
        classify_events_by_treatment(
            stage_events, stage_data, break_times[well][stage_name]
        )

# eventTags = []
# for k, v in events.items():
#     try:
#         tag=v["treatment_code"]
#     except:
#         tag='None'
#     counter = v['treatment_counter']
#     eventTags.append([k,tag,counter])
#
# outTagFile = r'E:\Projects\PFI\2021_Chevron_15-01\PFI_Scripts\QI\EventWithTag.csv'
# np.savetxt(outTagFile,eventTags,delimiter=',',fmt='%s',header='id, eventTag, eventCounter')
#
# exit()

# # make RT plots for all stages
# for stage in sorted_stages:
#     stage_name = stage["stage"]
#     well = stage["well"]
#     stage_events = {k: v for k, v in frac_events.items() if v["well"] == well and v["stage"] == stage_name}
#     well_stage_identifier = f"Well {well}: Stage {stage_name}"
#     if well_stage_identifier in diffusivity and diffusivity[well_stage_identifier]["horizontal"]:
#         stage_data = {k: v for k, v in treatment.items() if v["well"] == well and v["stage"] == stage_name}
#         fig1 = rt_with_treatment(
#             stage_events,
#             stage_data,
#             diffusivity[well_stage_identifier]["horizontal"],
#             diffusivity[well_stage_identifier]["t0"],
#             title=well_stage_identifier,
#         )
#         fig1.savefig(f"figures//rt_plots//rt_w{well}_s{stage_name}.png")
#         plt.close(fig1)


# remove IS, PRE, and POST events
events_without_is = {
    k: v
    for k, v in frac_events.items()
    if v["treatment_code"] not in ["IS", "PRE", "POST"]
}
# impose MC on complete dataset
frac_complete_events = {
    k: v for k, v in events_without_is.items() if v["Mw"] > magnitude_of_completeness
}
print("Number of frac events is {}".format(len(frac_events)))
print("Number of events without IS, PRE, POST is {}".format(len(events_without_is)))
print("Number of frac_complete_events is {}".format(len(frac_complete_events)))


# calculate fracture dimensions and plot against individual plan view stages
frac_dimensions = fracture_dimensions(
    frac_complete_events, wells, return_arrows_too=True, csv_out="frac_dimensions.csv"
)
for stage in sorted_stages:
    stage_name = stage["stage"]
    well = stage["well"]
    well_stage_identifier = f"Well {well}: Stage {stage_name}"
    if well_stage_identifier != "Well 7: Stage 22":
        continue
    stage_events = {
        k: v
        for k, v in frac_events.items()
        if v["well"] == well
        and v["stage"] == stage_name
        and v["Mw"] > magnitude_of_completeness
    }
    if well_stage_identifier in frac_dimensions:
        fig_en = plot_stage(stage_events, wells)
        ax = add_dimension_arrows_abs_coords(
            fig_en.axes[0], frac_dimensions[well_stage_identifier]["arrows"]
        )
        fig_en.savefig(
            f"figures//Cardinal stages with dimensions//en_w{well}_s{stage_name}.png"
        )
        plt.close(fig_en)
