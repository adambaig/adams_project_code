import json

import matplotlib.pyplot as plt
import numpy as np
import mplstereonet as mpls
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon


from generalPlots import gray_background_with_grid
from mtPlots import plot_general_sdr_rosettes, plot_strike_dip_rake_rosettes_from_MTs
from sms_moment_tensor.MT_math import mt_to_sdr, strike_dip_to_normal
from sms_moment_tensor.plotting import stereonet_plot_stress_tensor, mohr_circle_plot
from sms_moment_tensor.stress_inversions import (
    Michael_stress_inversion,
    resolve_shear_and_normal_stress,
    decompose_stress,
    iterate_Micheal_stress_inversion,
)
from read_inputs import read_initial_catalog, read_noise_mts, read_wells

CUTOFF_PERCENTILE = 95
UPPER_FAULT_MIN_ELEVATION = -2050
LOWER_FAULT_MAX_ELEVATION = -2200
PI = np.pi

with open("fault_polygon_xy.json") as f:
    lower_fault_polygon = Polygon(json.load(f))

with open("fault_polygon_upper.json") as f:
    upper_fault_polygon = Polygon(json.load(f))

with open("all_flipped_events_including_TM.json") as f:
    events = json.load(f)

noise = read_noise_mts()
wells = read_wells()
mt_r = [v["dc R"] for v in events.values()]
noise_r = [v["dc R"] for v in noise.values()]
cutoff_r = np.percentile(noise_r, CUTOFF_PERCENTILE)
wells = read_wells()
lower_fault_events = {
    k: v
    for k, v in events.items()
    if lower_fault_polygon.contains(Point(v["easting"], v["northing"]))
    and v["elevation"] < LOWER_FAULT_MAX_ELEVATION
    and v["dc R"] > cutoff_r
}
upper_fault_events = {
    k: v
    for k, v in events.items()
    if upper_fault_polygon.contains(Point(v["easting"], v["northing"]))
    and v["elevation"] > UPPER_FAULT_MIN_ELEVATION
    and v["dc R"] > cutoff_r
}



 for event in {**lower_fault_events, **upper_fault_events}.values():
     event['strike'],event['dip'], event['rake'], event['aux strike'], event['aux dip'],event['aux rake'] = mt_to_sdr(event['dc MT'])

iterated_stress_tensor, iterations = {}, {}
iterated_stress_tensor["lower"], iterations["lower"] = iterate_Micheal_stress_inversion(
    lower_fault_events, n_iterations=1000, output_iterations=True
)
iterated_stress_tensor["upper"], iterations["upper"] = iterate_Micheal_stress_inversion(
    upper_fault_events, n_iterations=1000, output_iterations=True
)


iterative_results = {
    "stress_tensor": {k: v.tolist() for k, v in iterated_stress_tensor.items()},
    "iterations": iterations,
}


with open("iterative_stress_tensors_faults.json", "w") as f:
    json.dump(iterative_results, f)


fig,ax = plt.subplots()
ax.set_asp
