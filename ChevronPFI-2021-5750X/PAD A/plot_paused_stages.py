from datetime import datetime
import json
import os

from matplotlib import cm, colors
import matplotlib.pyplot as plt
import pytz

from read_inputs import read_wells, read_flipped_catalog
from building_block_plotting import (
    plot_events,
    plot_wells,
    get_frame,
    set_frame,
)
from plotting_routines import plot_perfs, plot_stage
from generalPlots import gray_background_with_grid

UTC = pytz.utc
OUTFIGURE_FOLDER = "paused_stages"

events = read_flipped_catalog(
    "Catalogs\\relocCatalog_all_QCed_format_after_flip_final_fault_tagged_final_updateFault_170m_final.csv"
)
wells = read_wells()


def _add_datetimes(dictionary):
    for key in [k for k in dictionary.keys() if k.split("_")[-1] == "time"]:
        new_key = "_".join(key.split("_")[:1]) + "_datetime"
        yyyymmddhhmmss, decimal_seconds = str(dictionary[key]).split(".")
        microseconds = decimal_seconds.ljust(6, "0")
        dictionary[new_key] = UTC.localize(
            datetime.strptime(yyyymmddhhmmss + microseconds, "%Y%m%d%H%M%S%f")
        )


with open("sorted_stage_list.json") as f:
    sorted_stages = json.load(f)
for stage in sorted_stages:
    _add_datetimes(stage)


well_tags = [7, 7, 9, 9]
stages = ["31", "64", "23", "37"]
for well, stage in zip(well_tags, stages):
    found_stage = [
        s for s in sorted_stages if s["well"] == str(well) and s["stage"] == stage
    ][0]
    stage_events = {
        k: v for k, v in events.items() if v["well id"] == well and v["stage"] == stage
    }
    before_events = {
        k: v for k, v in stage_events.items() if v["treatmnet counter"] < 4
    }
    after_events = {k: v for k, v in stage_events.items() if v["treatmnet counter"] > 4}
    other_events = {
        k: v
        for k, v in events.items()
        if v["time"] < found_stage["end_datetime"]
        and v["time"] > found_stage["start_datetime"]
        and v["stage"] == "-1"
    }
    perf_data = wells[str(well)][f"Stage {stage}"]

    fig, ax = plt.subplots()
    ax.set_aspect("equal")
    plot_events(
        ax,
        before_events,
        marker="o",
        color="firebrick",
        linestyle="None",
        markeredgecolor="k",
    )
    plot_events(
        ax,
        after_events,
        marker="o",
        color="royalblue",
        linestyle="None",
        markeredgecolor="k",
    )
    plot_perfs(ax, perf_data, color="k", s=250, zorder=2)
    plot_perfs(ax, perf_data, color="orangered", s=200, zorder=3)
    frame = get_frame(ax)
    plot_events(
        ax, other_events, marker="o", color="0.8", linestyle="None", markeredgecolor="k"
    )
    plot_wells(ax, wells, color="k", zorder=1)
    set_frame(ax, frame)
    gray_background_with_grid(ax, grid_spacing=50)
    fig.savefig(os.path.join(OUTFIGURE_FOLDER, f"well_{well}_stage_{stage}.png"))
