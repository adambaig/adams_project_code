import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap

from generalPlots import gray_background_with_grid

from read_inputs import read_wells


wells = read_wells()
well = wells["7"]
tab10 = get_cmap("tab10")


def get_tab10_color(num_string):
    return tab10(((float(num_string[-1])) / 10 - 0.01) % 1)


fig, ax = plt.subplots(figsize=[12, 12])
ax.set_aspect("equal")
for wellname, well in wells.items():
    ax.plot(well["easting"], well["northing"], "0.5", lw=2, zorder=1)
    for stage, block in {k: v for k, v in well.items() if "Stage" in k}.items():
        zone = block["block"]
        ax.plot(
            [zone["top_east"], zone["bottom_east"]],
            [zone["top_north"], zone["bottom_north"]],
            color=get_tab10_color(stage),
            lw=4,
            zorder=2,
        )
        if stage == "Stage 1":
            ax.text(zone["bottom_east"] + 50, zone["bottom_north"], wellname)
gray_background_with_grid(ax)

fig.savefig("wells_and_stages.png")


fig_depth, ax_depth = plt.subplots(figsize=[12, 12])
ax_depth.set_aspect("equal")
for well in wells.values():
    ax_depth.plot(well["easting"], well["elevation"], "0.5", lw=2, zorder=1)
    for stage, block in {k: v for k, v in well.items() if "Stage" in k}.items():
        zone = block["block"]
        ax_depth.plot(
            [zone["top_east"], zone["bottom_east"]],
            [zone["top_elevation"], zone["bottom_elevation"]],
            color=get_tab10_color(stage),
            lw=4,
            zorder=2,
        )

gray_background_with_grid(ax_depth)

fig_depth.savefig("wells_and_stages_depth.png")
