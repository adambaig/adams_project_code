import numpy as np

from read_inputs import read_flipped_catalog

events = read_flipped_catalog(
    "Catalogs\\relocCatalog_all_QCed_format_after_flip_final_fault_tagged_final_updateFault_170m_final.csv"
)


[
    k
    for k, v in events.items()
    if v["treatment counter"] != -1 and v["event type"] == "IS"
]
