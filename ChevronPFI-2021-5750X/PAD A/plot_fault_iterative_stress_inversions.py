import json
import os

import matplotlib.pyplot as plt
import numpy as np
import mplstereonet as mpls
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge
from sms_moment_tensor.plotting import mohr_circle_plot
from sms_moment_tensor.stress_inversions import decompose_stress, most_unstable_sdr
from mtPlots import plot_general_sdr_rosettes
from read_inputs import read_noise_mts

CUTOFF_PERCENTILE = 95
UPPER_FAULT_MIN_ELEVATION = -2050
LOWER_FAULT_MAX_ELEVATION = -2200
FIGURE_DIR = r"figures\stress_inversion"
noise = read_noise_mts()
with open("all_flipped_events_no_TM.json") as f:
    events = json.load(f)

with open("iterative_stress_tensors_faults.json") as f:
    stress_inversions = json.load(f)

mt_r = [v["dc R"] for v in events.values()]
noise_r = [v["dc R"] for v in noise.values()]
cutoff_r = np.percentile(noise_r, CUTOFF_PERCENTILE)

fault_polygon = {}
with open("fault_polygon_xy.json") as f:
    fault_polygon["lower"] = Polygon(json.load(f))

with open("fault_polygon_upper.json") as f:
    fault_polygon["upper"] = Polygon(json.load(f))

fault_events = {
    "lower": {
        k: v
        for k, v in events.items()
        if fault_polygon["lower"].contains(Point(v["easting"], v["northing"]))
        and v["elevation"] < LOWER_FAULT_MAX_ELEVATION
        and v["dc R"] > cutoff_r
    },
    "upper": {
        k: v
        for k, v in events.items()
        if fault_polygon["upper"].contains(Point(v["easting"], v["northing"]))
        and v["elevation"] > UPPER_FAULT_MIN_ELEVATION
        and v["dc R"] > cutoff_r
    },
}

axis_color = {"s1": "firebrick", "s2": "forestgreen", "s3": "royalblue"}
axis_symbol = {"s1": "^", "s2": "o", "s3": "s"}
fault_color = {"lower": "firebrick", "upper": "forestgreen"}
fault_alpha = {"lower": 0.05, "upper": 0.8}
for key in ["lower", "upper"]:
    stress_tensor = stress_inversions["stress_tensor"][key]
    iterations = stress_inversions["iterations"][key]
    n_iterations = len(iterations)
    iteration_axes = {
        "s1": {"trend": np.zeros(n_iterations), "plunge": np.zeros(n_iterations)},
        "s2": {"trend": np.zeros(n_iterations), "plunge": np.zeros(n_iterations)},
        "s3": {"trend": np.zeros(n_iterations), "plunge": np.zeros(n_iterations)},
    }
    iteration_ratios = np.zeros(n_iterations)
    for i_iteration, iteration in enumerate(iterations):
        iteration_ratios[i_iteration], axes = decompose_stress(iteration)
        for axis in ["s1", "s2", "s3"]:
            (
                iteration_axes[axis]["trend"][i_iteration],
                iteration_axes[axis]["plunge"][i_iteration],
            ) = unit_vector_to_trend_plunge(axes[axis])
    fig, ax = mpls.subplots()
    stress_ratio, stress_axes = decompose_stress(stress_tensor)
    for axis in ["s1", "s2", "s3"]:
        trend, plunge = unit_vector_to_trend_plunge(stress_axes[axis])
        ax.line(
            plunge,
            trend,
            axis_symbol[axis],
            color=axis_color[axis],
            markeredgecolor="k",
            zorder=12,
        )
        ax.line(
            iteration_axes[axis]["plunge"],
            iteration_axes[axis]["trend"],
            ".",
            color=axis_color[axis],
            alpha=0.05,
            zorder=10,
        )
    ax.grid()
    fig_hist, ax_hist = plt.subplots()
    r_bins = np.linspace(0, 1, 201)
    ax_hist.hist(iteration_ratios, r_bins, color=fault_color[key])
    y1, y2 = ax_hist.get_ylim()
    ax_hist.plot([stress_ratio, stress_ratio], [y1, y2], "k")
    ax_hist.set_ylim([y1, y2])
    fig_mc, ax_mc = plt.subplots(figsize=[8, 4])
    for event in fault_events[key].values():
        event["true strike"], event["true dip"], event["true rake"] = most_unstable_sdr(
            event["dc MT"], stress_tensor
        )

    mohr_circle_plot(
        ax_mc,
        fault_events[key],
        stress_tensor,
        alpha=fault_alpha[key],
        color=fault_color[key],
        markeredgecolor="k",
    )
    fig_rosette = plot_general_sdr_rosettes(
        [v["true strike"] for v in fault_events[key].values()],
        [v["true dip"] for v in fault_events[key].values()],
        [v["true rake"] for v in fault_events[key].values()],
        color=fault_color[key],
    )

    fig.savefig(os.path.join(FIGURE_DIR, f"stress_axes_{key}.png"))
    fig_rosette.savefig(os.path.join(FIGURE_DIR, f"favoured_plane_rosettes_{key}.png"))
    fig_hist.savefig(os.path.join(FIGURE_DIR, f"ratio_histogram_{key}.png"))
    fig_mc.savefig(os.path.join(FIGURE_DIR, f"mohr_coulomb_{key}.png"))
