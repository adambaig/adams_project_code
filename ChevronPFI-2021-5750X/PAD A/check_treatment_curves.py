from datetime import datetime
import json
import pytz

import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np

TIMEZONE = pytz.timezone("America/Edmonton")
UTC = pytz.utc

with open("treatment_data.json") as f:
    treatment_data = json.load(f)


with open("break_times.json") as f:
    break_times = json.load(f)


def plot_stage(treatment_data_for_stage, utc_break_time):
    utc_times = [
        UTC.localize(datetime.strptime(t, "%Y%m%d%H%M%S.%f"))
        for t in treatment_data_for_stage.keys()
    ]
    break_time = UTC.localize(datetime.strptime(utc_break_time, "%Y%m%d%H%M%S.%f"))
    slurry = (
        np.array([v["slurry_rate"] for v in treatment_data_for_stage.values()]) / 20.0
    )
    proppant = (
        np.array(
            [
                v["downhole_proppant_concentration"]
                for v in treatment_data_for_stage.values()
            ]
        )
        / 500.0
    )
    pressure = (
        np.array([v["pressure"] for v in treatment_data_for_stage.values()]) / 100.0
    )
    fig = plt.figure(figsize=[12, 6])
    ax2 = fig.add_axes([0.1, 0.1, 0.7, 0.8])
    ax4 = fig.add_axes([0.9, 0.1, 0, 0.8])
    ax2.plot(utc_times, proppant, c="darkgoldenrod", lw=2)
    ax2.plot(utc_times, pressure, c="firebrick", lw=2)
    ax2.plot([break_time, break_time], [0, 1], "k")
    ax2.set_ylabel("pressure (MPa)", color="firebrick")
    ax2.set_yticks(np.linspace(0, 1, 6))
    ax2.set_yticklabels(np.linspace(0, 100, 6))
    ax3 = ax2.twinx()
    ax3.plot(utc_times, slurry, c="royalblue", lw=2)
    ax3.set_ylabel("slurry rate (m$^3$/min)", color="royalblue")
    ax3.set_yticks(np.linspace(0, 1, 5))
    ax3.set_yticklabels(np.linspace(0, 20, 5))
    ax4.yaxis.set_ticks_position("right")
    ax4.yaxis.set_label_position("right")
    ax4.set_yticks(np.linspace(0, 1, 6))
    ax4.set_yticklabels(np.linspace(0, 500, 6))
    ax4.set_xticks([])
    ax4.set_ylabel("Proppant Concentration (kg/m$^3$)", color="darkgoldenrod")
    ax2.xaxis.set_major_formatter(md.DateFormatter("%b %d\n%H:%M", tz=TIMEZONE))
    ax2.set_xlabel("local time")
    return fig


wells = np.unique([v["well"] for v in treatment_data.values()])
failed_stages = []
# for well in wells:
for well in ["8"]:
    well_data = {k: v for k, v in treatment_data.items() if v["well"] == well}
    stages = np.unique([v["stage"] for v in well_data.values()])
    # for stage in stages:
    for stage in ["42", "58"]:
        stage_data = {k: v for k, v in well_data.items() if v["stage"] == stage}

        utc_break_time = break_times[well][stage]
        if utc_break_time:
            fig = plot_stage(stage_data, utc_break_time)
            fig.savefig(
                f"figures//treatment_plots//treatment_data_well_{well.zfill(2)}_{stage.zfill(2)}.png"
            )
        else:
            failed_stages.append(f"Well_{well}_Stage_{stage}")
        plt.close(fig)
