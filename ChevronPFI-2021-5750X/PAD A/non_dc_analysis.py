import matplotlib.pyplot as plt
import numpy as np
from mtPlots import makeBaseMap, hammer_projection, mtToLatLon

from read_inputs import read_wells, read_flipped_catalog

events = read_flipped_catalog(
    "Catalogs\\relocCatalog_all_QCed_format_after_flip_final_fault_tagged.csv"
)
wells = read_wells()


alphas = {"Lower Fault Event": 0.2, "Upper Fault Event": 1, "Frac Event": 0.4}

colors = {
    "Lower Fault Event": "firebrick",
    "Upper Fault Event": "forestgreen",
    "Frac Event": "royalblue",
}


def moment_to_potency_tensor(compliance, moment_tensor):
    return np.tensordot(compliance, moment_tensor, axes=([2, 3], [0, 1]))


for label in ["Lower Fault Event", "Upper Fault Event", "Frac Event"]:
    fig, ax = plt.subplots(figsize=[4, 10])
    makeBaseMap(ax)
    event_group = {
        k: v
        for k, v in events.items()
        if v["event type"] == label and v["MT conf"] > 0.95
    }
    x_lune, y_lune = hammer_projection(
        *np.array([mtToLatLon(event["gen MT"]) for event in event_group.values()]).T
    )
    scatter = ax.plot(x_lune, y_lune, ".", color=colors[label], alpha=alphas[label])
    ax.set_aspect("equal")
    ax.set_axis_off()
    fig.savefig(label.replace(" ", "_") + ".png")
