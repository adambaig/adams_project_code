from datetime import datetime
import json
import random
import os
import pytz

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from generalPlots import gray_background_with_grid

from read_inputs import read_noise_mts, read_wells, read_flipped_catalog

UTC = pytz.utc

CUTOFF_PERCENTILE = 95
UPPER_FAULT_MIN_ELEVATION = -2050
LOWER_FAULT_MAX_ELEVATION = -2200
PI = np.pi
TAB10 = cm.get_cmap("tab10")
with open("fault_polygon_lower.json") as f:
    lower_fault_polygon = Polygon(json.load(f))
with open("mini_lower_fault_polygon.json") as f:
    lower_fault_mini_polygon = Polygon(json.load(f))
with open("fault_polygon_upper.json") as f:
    upper_fault_polygon = Polygon(json.load(f))
#
# with open("all_flipped_events_including_TM.json") as f:
#     events = json.load(f)
events = read_flipped_catalog(
    r"Catalogs\relocCatalog_all_QCed_format_after_flip_final_fault_tagged.csv"
)
noise = read_noise_mts()
wells = read_wells()
mt_r = [v["dc R"] for v in events.values()]
noise_r = [v["dc R"] for v in noise.values()]
cutoff_r = np.percentile(noise_r, CUTOFF_PERCENTILE)
wells = read_wells()
lower_fault_events = {
    k: v
    for k, v in events.items()
    if (
        lower_fault_polygon.contains(Point(v["easting"], v["northing"]))
        and v["elevation"] < LOWER_FAULT_MAX_ELEVATION
    )
    or lower_fault_mini_polygon.contains(Point(v["easting"], v["northing"]))
}
upper_fault_events = {
    k: v
    for k, v in events.items()
    if upper_fault_polygon.contains(Point(v["easting"], v["northing"]))
    and v["elevation"] > UPPER_FAULT_MIN_ELEVATION
}


all_fault_events = {**upper_fault_events, **lower_fault_events}

frac_events = {
    k: v
    for k, v in events.items()
    if not (k in all_fault_events) and v["stage"] != "-1"
}

with open("sorted_stage_list.json") as f:
    sorted_stages = json.load(f)

fig, ax = plt.subplots(figsize=[12, 12])
ax.set_aspect("equal")
for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k", zorder=-3)

shuffle_order = list(enumerate(frac_events))
random.shuffle(shuffle_order)
_, shuffled_events = zip(*shuffle_order)

ax.scatter(
    [events[event]["easting"] for event in shuffled_events],
    [events[event]["northing"] for event in shuffled_events],
    c=[(float(events[event]["well id"]) - 6.5) / 10 for event in shuffled_events],
    cmap="tab10",
    vmin=0,
    vmax=1,
    edgecolor="0.3",
    linewidth=0.25,
)
x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()


def plot_perfs(ax, perf_data, **kwargs):
    n_clusters = perf_data["n_clusters"]
    block = perf_data["block"]
    ax.scatter(
        np.linspace(block["bottom_east"], block["top_east"], n_clusters),
        np.linspace(block["bottom_north"], block["top_north"], n_clusters),
        marker=(7, 1, 0),
        **kwargs,
    )


def plot_events(ax, events, **kwargs):
    ax.scatter(
        [event["easting"] for event in events.values()],
        [event["northing"] for event in events.values()],
        **kwargs,
    )


for i_stage, stage in enumerate(sorted_stages):
    if f"Stage {stage['stage']}" in wells[stage["well"]]:
        f_frame, a_frame = plt.subplots(figsize=[12, 12])
        for well in wells.values():
            a_frame.plot(well["easting"], well["northing"], "k", zorder=-6)
        for previous_stage in sorted_stages[:i_stage]:
            if f"Stage {previous_stage['stage']}" in wells[previous_stage["well"]]:
                previous_perf_data = wells[previous_stage["well"]][
                    f'Stage {previous_stage["stage"]}'
                ]
                plot_perfs(a_frame, previous_perf_data, zorder=-3, color="0.2")
            stage_start = UTC.localize(
                datetime.strptime(
                    str(stage["start_time"]).split(".")[0], "%Y%m%d%H%M%S"
                )
            )
            previous_events = {
                k: v for k, v in frac_events.items() if v["time"] < stage_start
            }
            plot_events(a_frame, previous_events, s=7, marker=".", color="0.2")
        stage_color = TAB10((float(stage["well"]) - 6.5) / 10)
        stage_events = {
            k: v
            for k, v in frac_events.items()
            if v["stage"] == stage["stage"] and str(v["well id"]) == stage["well"]
        }
        if len(stage_events) > 0:
            plot_events(
                a_frame,
                stage_events,
                color=stage_color,
                s=20,
                edgecolor="k",
                marker="o",
            )
        perf_data = wells[stage["well"]][f'Stage {stage["stage"]}']
        plot_perfs(a_frame, perf_data, zorder=-2, color="k", s=12)
        plot_perfs(a_frame, perf_data, zorder=-1, color="0.8", s=8)

        gray_background_with_grid(a_frame)
        a_frame.set_xlim([x1, x2])
        a_frame.set_ylim([y1, y2])
        f_frame.savefig(
            os.path.join("figures", "movie_image", f"frame_{str(i_stage).zfill(3)}.png")
        )


shuffle_order = list(enumerate(frac_events))
random.shuffle(shuffle_order)
_, shuffled_events = zip(*shuffle_order)

ax.scatter(
    [events[event]["easting"] for event in shuffled_events],
    [events[event]["northing"] for event in shuffled_events],
    c=[(float(events[event]["well id"]) - 6.5) / 10 for event in shuffled_events],
    cmap="tab10",
    vmin=0,
    vmax=1,
    edgecolor="0.3",
    linewidth=0.25,
)
gray_background_with_grid(ax)
fig.savefig("all_frac_events.png")
