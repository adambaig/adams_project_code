import matplotlib.pyplot as plt
import numpy as np

from read_inputs import read_initial_catalog, read_noise_mts

filename = r"Catalogs\relocCatalog_all_initial_31672_MT_Mw.csv"

noise = read_noise_mts()
events = read_initial_catalog(filename)

mt_r = [v["dc R"] for v in events.values()]
noise_r = [v["dc R"] for v in noise.values()]


bins = np.arange(0, 1, 0.001)
fig, ax = plt.subplots()
_, _, p_mt = ax.hist(mt_r, bins, fc="steelblue", alpha=0.6)
_, _, p_noise = ax.hist(noise_r, bins, fc="firebrick", alpha=0.6)

r_95 = np.percentile(noise_r, 95)
n_mt = len(np.where(mt_r > r_95)[0])
y1, y2 = ax.get_ylim()

ax.plot([r_95, r_95], [y1, y2], "forestgreen")
ax.text(1.05 * r_95, 0.55 * y2, f"{n_mt} events above 95% confidence")
ax.legend([p_mt, p_noise], ["events", "noise"])
ax.set_xlabel("R value")
ax.set_ylim([y1, y2])
fig.savefig("mt_confidence")
