import json

import matplotlib.pyplot as plt
import numpy as np
from read_inputs import read_wells

from generalPlots import gray_background_with_grid

with open("formation_tops.json") as f:
    layers = json.load(f)

wells = read_wells()

for name, layer in layers.items():
    e_grid = layer["e_grid"]
    n_grid = layer["n_grid"]
    topography = np.array(layer["top_grid"])
    fig, ax = plt.subplots(figsize=[12, 12])
    ax.set_aspect("equal")
    for well in wells.values():
        ax.plot(well["easting"], well["northing"], "k")
    x1, x2 = ax.get_xlim()
    y1, y2 = ax.get_ylim()
    ieast_1 = np.argwhere(e_grid < x1)[-1][0]
    ieast_2 = np.argwhere(e_grid > x2)[0][0]
    inorth_1 = np.argwhere(n_grid < y1)[-1][0]
    inorth_2 = np.argwhere(n_grid > y2)[0][0]
    vmin = np.amin([topography[ieast_1:ieast_2, inorth_1:inorth_2]])
    vmax = np.amax([topography[ieast_1:ieast_2, inorth_1:inorth_2]])

    topo_grid = ax.pcolor(e_grid, n_grid, topography.T, vmin=vmin, vmax=vmax)
    ax.set_title(f"{name}")
    ax.set_xlim([x1, x2])

    ax.set_ylim([y1, y2])
    gray_background_with_grid(ax)
    fig.colorbar(topo_grid)
    fig.savefig(f"figures//layers//{name}.png")
