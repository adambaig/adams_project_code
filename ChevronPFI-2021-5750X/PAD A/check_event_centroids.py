from datetime import datetime, timedelta
import json
import os

import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import pytz
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

from pfi_qi.QI_analysis import (
    parent_child,
    rt_diffusivity,
    fracture_dimensions,
    strain_grid,
    output_isosurface,
    rotate_all_events_to_well_coords,
)

from pfi_qi.engineering import (
    sorted_stage_list,
    breaktimes_by_stage,
    find_perf_center,
    calc_well_trend,
    make_unique_stage_list,
)
from pfi_qi.rotations import rotate_from_cardinal

from plotting_routines import (
    rt_with_treatment,
    plot_stage,
    add_dimension_arrows_abs_coords,
)
from read_inputs import (
    read_wells,
    read_treatment,
    classify_events_by_treatment,
    read_final_catalog,
)


def read_or_load_json(read_function, json_file, *args):
    if os.path.isfile(json_file):
        with open(json_file, "r") as read_file:
            dictionary = json.load(read_file)
        return dictionary
    dictionary = read_function(*args)
    with open(json_file, "w") as write_file:
        json.dump(dictionary, write_file)
    return dictionary


PI = np.pi
D2R = PI / 180.0
UTC = pytz.utc
TIMEZONE = pytz.timezone("America/Edmonton")
UPPER_FAULT_MIN_ELEVATION = -2050
LOWER_FAULT_MAX_ELEVATION = -2200
magnitude_of_completeness = -1.05


with open("all_flipped_events_including_TM.json") as f:
    events = json.load(f)


# "timestamp" is expected as a key

wells = read_wells()
treatment = read_or_load_json(read_treatment, "treatment_data.json")
sorted_stages = read_or_load_json(
    sorted_stage_list, "sorted_stage_list.json", treatment
)
break_times = read_or_load_json(
    breaktimes_by_stage, "break_times.json", sorted_stages, treatment
)


inputCatalog = r"Catalogs\relocCatalog_all_QCed_format_after_flip_final_fault_tagged_final_updateFault_170m_final (3).csv"
events = read_final_catalog(inputCatalog)

for event in events.values():
    event["utc datetime"] = event["timestamp"].strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    if event["event type"] == "Frac Event":
        event["treatment_code"] = "IS"
    elif event["event type"] == "Lower Fault Event":
        event["treatment_code"] = "Lower Fault Event"
    elif event["event type"] == "Upper Fault Event":
        event["treatment_code"] = "Upper Fault Event"
    event["treatment_counter"] = -1
frac_events = {
    k: v
    for k, v in events.items()
    if v["event type"] in ["HP", "LP", "FL"]
    and v["stage"] != "-1"
    and v["horizontal distance"] < 170.0
}

np.unique([v["event type"] for v in events.values()])

# remove IS, PRE, and POST events

# impose MC on complete dataset
frac_complete_events = {
    k: v for k, v in frac_events.items() if v["Mw"] > magnitude_of_completeness
}


# calculate fracture dimensions and plot against individual plan view stages
frac_dimensions = fracture_dimensions(
    frac_complete_events, wells, return_arrows_too=True, csv_out="frac_dimensions.csv"
)

#
stage_name = "22"
well = "7"


well_stage_identifier = f"Well {well}: Stage {stage_name}"
well_trend = calc_well_trend(wells[well])
perf_center = find_perf_center("22", wells[well])
stage_events = {
    k: v
    for k, v in frac_events.items()
    if v["well"] == well
    and v["stage"] == stage_name
    and v["Mw"] > magnitude_of_completeness
}
len(stage_events)

if well_stage_identifier in frac_dimensions:
    fig_en = plot_stage(stage_events, wells)
    ax = add_dimension_arrows_abs_coords(
        fig_en.axes[0], frac_dimensions[well_stage_identifier]["arrows"]
    )
    ax.plot(
        frac_dimensions[well_stage_identifier]["centroid easting"],
        frac_dimensions[well_stage_identifier]["centroid northing"],
        "k+",
        ms=30,
        lw=16,
    )
    well_centered_centroid = {
        "centroid": {
            "along trend": frac_dimensions[well_stage_identifier][
                "centroid along well"
            ],
            "perp to trend": frac_dimensions[well_stage_identifier][
                "centroid across well"
            ],
        }
    }
    rotate_from_cardinal(well_centered_centroid, well_trend, perf_center, reverse=True)
    ax.plot(
        well_centered_centroid["centroid"]["easting"],
        well_centered_centroid["centroid"]["northing"],
        "b+",
        ms=30,
        lw=16,
    )
    fig_en.savefig(f"figures//check_centroids.png")
