from datetime import datetime, timedelta
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import pytz

from pfi_qi.engineering import calc_well_trend, find_perf_center
from pfi_qi.rotations import rotate_perf_clusters, rotate_from_cardinal
from generalPlots import gray_background_with_grid

from sms_moment_tensor.MT_math import mt_to_sdr

PI = np.pi
D2R = PI / 180.0
UTC = pytz.utc
TIMEZONE = pytz.timezone("America/Edmonton")
tab10 = cm.get_cmap("tab10")
symbol = {"FL": "^", "LP": "s", "HP": "o", "PRE": ".", "POST": ".", "IS": "o"}


def rt_with_treatment(
    stage_events,
    stage_data,
    diffusivity,
    t0,
    title=None,
    y_axis="along trend",
):
    utc_times = [
        UTC.localize(datetime.strptime(t, "%Y%m%d%H%M%S.%f")) for t in stage_data.keys()
    ]
    slurry = np.array([v["slurry_rate"] for v in stage_data.values()]) / 20.0
    proppant = (
        np.array([v["downhole_proppant_concentration"] for v in stage_data.values()])
        / 500.0
    )
    pressure = np.array([v["pressure"] for v in stage_data.values()]) / 100.0
    fig = plt.figure(figsize=[12, 12])
    ax1 = fig.add_axes([0.1, 0.52, 0.7, 0.38])
    ax2 = fig.add_axes([0.1, 0.1, 0.7, 0.38])
    ax4 = fig.add_axes([0.9, 0.1, 0, 0.38])
    stage = np.unique([v["stage"] for v in stage_events.values()])[0]
    mod_stage = int(stage[-1])
    stage_color = tab10((mod_stage + 0.1) / 10)
    if "treatment_code" in list(stage_events.values())[0]:
        fluid_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "FL"
        }
        lp_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "LP"
        }
        hp_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "HP"
        }
        pre_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "PRE"
        }
        post_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "POST"
        }
        is_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "IS"
        }

        for events in (
            fluid_events,
            lp_events,
            hp_events,
            pre_events,
            post_events,
        ):
            if len(events) == 0:
                continue
            treatment_code = list(events.values())[0]["treatment_code"]
            ax1.plot(
                [v["timestamp"] for v in events.values()],
                [abs(v[y_axis]) for v in events.values()],
                symbol[treatment_code],
                c=stage_color,
                markeredgecolor="k",
                linewidth=0.25,
                zorder=3,
                ms=10,
                alpha=0.8,
            )
        if len(is_events) > 0:
            ax1.plot(
                [v["timestamp"] for v in is_events.values()],
                [abs(v[y_axis]) for v in is_events.values()],
                "o",
                c="w",
                markeredgecolor="k",
                linewidth=0.25,
                zorder=3,
                ms=10,
                alpha=0.8,
            )
    else:
        ax1.plot(
            [v["timestamp"] for v in stage_events.values()],
            [abs(v[y_axis]) for v in stage_events.values()],
            "o",
            c=stage_color,
            markeredgecolor="k",
            linewidth=0.25,
            zorder=3,
            ms=10,
            alpha=0.8,
        )
    ax1.set_xticklabels([])
    for ax in ax1, ax2:
        ax.set_facecolor("lightgrey")
    envelope_times = np.arange(
        t0,
        max([v["timestamp"] for v in stage_events.values()]),
        dtype="datetime64[s]",
    )
    if diffusivity:
        ax1.plot(
            envelope_times,
            np.sqrt(
                4
                * np.pi
                * diffusivity
                * np.array(
                    [
                        np.timedelta64(t - np.datetime64(t0), "s")
                        .item()
                        .total_seconds()
                        for t in envelope_times
                    ]
                )
            ),
            linestyle="--",
            lw="2",
            c="0.1",
        )
    ax2.plot(utc_times, proppant, c="darkgoldenrod", lw=2)
    ax2.plot(utc_times, pressure, c="firebrick", lw=2)
    ax2.set_ylabel("pressure (MPa)", color="firebrick")
    ax2.set_yticks(np.linspace(0, 1, 6))
    ax2.set_yticklabels(np.linspace(0, 100, 6))
    ax3 = ax.twinx()
    ax3.plot(utc_times, slurry, c="royalblue", lw=2)
    ax3.set_ylabel("slurry rate (m$^3$/min)", color="royalblue")
    ax3.set_yticks(np.linspace(0, 1, 5))
    ax3.set_yticklabels(np.linspace(0, 20, 5))
    ax4.yaxis.set_ticks_position("right")
    ax4.yaxis.set_label_position("right")
    ax4.set_yticks(np.linspace(0, 1, 6))
    ax4.set_yticklabels(np.linspace(0, 500, 6))
    ax4.set_xticks([])
    ax4.set_ylabel("Proppant Concentration (kg/m$^3$)", color="darkgoldenrod")
    ax2.xaxis.set_major_formatter(md.DateFormatter("%b %d\n%H:%M", tz=TIMEZONE))
    ax2.set_xlabel("local time")
    if title is not None:
        ax1.set_title(title)
    for ax in ax2, ax3, ax4:
        ax.set_ylim([0, 1])
    x1, x2 = ax2.get_xlim()
    x3, x4 = ax1.get_xlim()
    for ax in ax1, ax2:
        ax.set_xlim([x1, max(x2, x4)])
    y1, y2 = ax1.get_ylim()
    ax1.set_ylim([0, y2])
    ax1.set_ylabel(f"{y_axis} distance (m)")
    return fig


def plot_perfs(ax, perf_data, **kwargs):
    if "n_clusters" in perf_data:
        n_clusters = perf_data["n_clusters"]
        block = perf_data["block"]
        mid_eastings = np.linspace(block["bottom_east"], block["top_east"], n_clusters)
        min_northings = np.linspace(
            block["bottom_north"], block["top_north"], n_clusters
        )
    else:
        mid_eastings = [
            (v["top_east"] + v["bottom_east"]) / 2
            for k, v in perf_data.items()
            if "Cluster" in k
        ]
        mid_northings = [
            (v["top_north"] + v["bottom_north"]) / 2
            for k, v in perf_data.items()
            if "Cluster" in k
        ]

    ax.scatter(mid_eastings, mid_northings, marker=(7, 1, 0), **kwargs)


def plot_events(ax, events, **kwargs):
    ax.plot(
        [v["easting"] for v in events.values()],
        [v["northing"] for v in events.values()],
        **kwargs,
    )


def plot_wells(ax, wells, **kwargs):
    for well in wells.values():
        ax.plot(well["easting"], well["northing"], **kwargs)


def get_frame(ax):
    x1, x2 = ax.get_xlim()
    y1, y2 = ax.get_ylim()
    return {"x1": x1, "x2": x2, "y1": y1, "y2": y2}


def set_frame(ax, frame):
    ax.set_xlim(frame["x1"], frame["x2"])
    ax.set_ylim(frame["y1"], frame["y2"])


def plot_stage(
    stage_events, wells, title=None, grid_spacing=50, limits=None, color="auto"
):
    stage = np.unique([v["stage"] for v in stage_events.values()])[0]
    well_id = np.unique([v["well id"] for v in stage_events.values()])[0]
    well_trend = calc_well_trend(wells[str(well_id)])
    mod_stage = int(stage[-1])
    stage_color = tab10((mod_stage + 0.1) / 10) if color == "auto" else color
    print(stage_color)
    fig, ax = plt.subplots(figsize=[12, 12])
    ax.set_aspect("equal")

    if "treatment_code" in list(stage_events.values())[0]:
        fluid_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "FL"
        }
        lp_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "LP"
        }
        hp_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "HP"
        }
        pre_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "PRE"
        }
        post_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "POST"
        }
        is_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "IS"
        }

        for events in (fluid_events, lp_events, hp_events, post_events):
            if len(events) == 0:
                continue
            treatment_code = list(events.values())[0]["treatment_code"]
            plot_events(
                ax,
                events,
                symbol[treatment_code],
                c=stage_color,
                markeredgecolor="k",
                linewidth=0.25,
                zorder=3,
                ms=10,
                alpha=0.8,
            )
        for events in (pre_events, is_events):
            if len(events) == 0:
                continue
            treatment_code = list(events.values())[0]["treatment_code"]
            plot_events(
                ax,
                events,
                symbol[treatment_code],
                c="w",
                markeredgecolor="k",
                linewidth=0.25,
                zorder=3,
                ms=10,
                alpha=0.8,
            )
    else:
        plot_events(
            ax,
            events,
            "o",
            c=stage_color,
            markeredgecolor="k",
            linewidth=0.25,
            zorder=3,
            ms=10,
            alpha=0.8,
        )
    plot_perfs(ax, wells[str(well_id)][f"Stage {stage}"])
    if limits is not None:
        ax.set_xlim(limits["x1"], limits["x2"])
        ax.set_ylim(limits["y1"], limits["y2"])
    x1, x2 = ax.get_xlim()
    y1, y2 = ax.get_ylim()
    ax.plot(
        wells[str(well_id)]["easting"],
        wells[str(well_id)]["northing"],
        lw=4,
        c="0.7",
        zorder=0,
    )
    ax.plot(
        wells[str(well_id)]["easting"],
        wells[str(well_id)]["northing"],
        lw=5,
        c="0.3",
        zorder=-1,
    )
    for well2 in wells.values():
        ax.plot(well2["easting"], well2["northing"], lw=2, c="0.8", zorder=-2)
        ax.plot(well2["easting"], well2["northing"], lw=2.5, c="0.4", zorder=-3)
    ax.set_xlim([x1, x2])
    ax.set_ylim([y1, y2])
    ax = gray_background_with_grid(ax, grid_spacing=50)
    ax.set_title(title)
    return fig


def add_dimension_arrows_abs_coords(axis, arrows):
    axis.arrow(
        arrows["lengthwise"]["easting"][0],
        arrows["lengthwise"]["northing"][0],
        np.diff(arrows["lengthwise"]["easting"])[0],
        np.diff(arrows["lengthwise"]["northing"])[0],
        zorder=27,
        lw=3,
        color="k",
    )
    axis.arrow(
        arrows["lengthwise"]["easting"][0],
        arrows["lengthwise"]["northing"][0],
        np.diff(arrows["lengthwise"]["easting"])[0],
        np.diff(arrows["lengthwise"]["northing"])[0],
        zorder=28,
        lw=1,
        color="w",
    )
    axis.arrow(
        arrows["widthwise"]["easting"][0],
        arrows["widthwise"]["northing"][0],
        np.diff(arrows["widthwise"]["easting"])[0],
        np.diff(arrows["widthwise"]["northing"])[0],
        zorder=27,
        lw=3,
        color="k",
    )
    axis.arrow(
        arrows["widthwise"]["easting"][0],
        arrows["widthwise"]["northing"][0],
        np.diff(arrows["widthwise"]["easting"])[0],
        np.diff(arrows["widthwise"]["northing"])[0],
        zorder=28,
        lw=1,
        color="w",
    )
    return axis
