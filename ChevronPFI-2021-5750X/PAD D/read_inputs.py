from datetime import datetime, timedelta
import glob
import os
import json
import pytz

import numpy as np

from pfi_qi.engineering import pick_breakdown_time
from sms_moment_tensor.MT_math import mt_vector_to_matrix

TIMEZONE = pytz.timezone("America/Edmonton")
UTC = pytz.utc
PI = np.pi
ROOT_DIR = "C:\\Users\\adambaig\\Project\\ChevronPFI-2021-5750X\\PAD D\\"

#
# well_lookup = {
#     "100/01-35-061-19W5/0": "12",
#     "103/04-36-061-19W5/0": "11",
#     "102/06-36-061-19W5/0": "10",
#     "103/06-36-061-19W5/0": "9",
#     "104/10-36-061-19W5/0": "8",
#     "106/10-36-061-19W5/0": "7",
# }


HP_MIN_CONCENTRATION = 120
LP_MIN_CONCENTRATION = 5


def decorate_well_name(wellname):
    return f"{wellname[:3]}/{wellname[3:5]}-{wellname[5:7]}-{wellname[7:10]}-{wellname[10:12]}W5/0"


def read_unflipped_catalog(filename):
    events = {}
    with open(filename) as f:
        _head = f.readline()
        lines = f.readlines()
    for line in lines:
        split_line = line.split(",")
        event_id = split_line[0]
        events[event_id] = {
            "UTC Date Time": UTC.localize(
                datetime.strptime(split_line[1], "%Y-%m-%d %H:%M:%S.%f")
            ),
            "serial time": float(split_line[2]),
            "easting": float(split_line[3]),
            "northing": float(split_line[4]),
            "elevation": -float(split_line[5]),
            "SNR": float(split_line[6]),
            "raw amp": float(split_line[7]),
            "reloc amp": float(split_line[8]),
            "Mw": float(split_line[9]),
            "corner frequency": float(split_line[10]),
            "attenuation Q": float(split_line[11]),
            "general MT": mt_vector_to_matrix([float(v) for v in split_line[12:18]]),
            "general MT R": float(split_line[18]),
            "general MT CN": float(split_line[19]),
            "DC MT": mt_vector_to_matrix([float(v) for v in split_line[20:26]]),
            "DC MT R": float(split_line[26]),
            "DC MT CN": float(split_line[27]),
            "MT confidence": float(split_line[28]),
            "simple stack amplitude": float(split_line[29]),
            "log SNR": float(split_line[30]),
            "easting uncertainty": float(split_line[31]),
            "northing uncertainty": float(split_line[32]),
            "depth uncertainty": float(split_line[33]),
            "well": split_line[34],
            "well id": int(split_line[35]),
            "stage": split_line[36],
            "well stage id": int(split_line[37]),
            "horizontal distance from stage": float(split_line[38]),
            "total distance from stage": float(split_line[39]),
            "elapsed time": float(split_line[40]),
            "event type": split_line[41].strip(),
        }
    return events


def read_final_catalog(
    filename=r"Catalog\Chevron_15-03_finalCatalog_withTagTreatmentCounter_final_wStageId_FinalWellPath_format.csv",
):
    events = {}
    with open(filename) as f:
        _head = f.readline()
        lines = f.readlines()
    for line in lines:
        split_line = line.split(",")
        event_id = split_line[0]
        events[event_id] = {
            "UTC Date Time": UTC.localize(
                datetime.strptime(split_line[1], "%Y-%m-%d %H:%M:%S.%f")
            ),
            "easting": float(split_line[3]),
            "northing": float(split_line[4]),
            "elevation": -float(split_line[5]),
            "log SNR": float(split_line[6]),
            "easting uncertainty": float(split_line[7]),
            "northing uncertainty": float(split_line[8]),
            "elevation uncertainty": float(split_line[9]),
            "Mw": float(split_line[10]),
            "seismic moment": float(split_line[11]),
            "well name": split_line[12],
            "well id": split_line[13],
            "stage": split_line[14],
            "stage id": int(split_line[15]),
            "horizontal distance from perf": float(split_line[16]),
            "total distance from perf": float(split_line[17]),
            "elapsed time": float(split_line[18]),
            "general MT": mt_vector_to_matrix([float(v) for v in split_line[19:25]]),
            "general MT R": float(split_line[25]),
            "general MT CN": float(split_line[26]),
            "clvd": float(split_line[27]),
            "iso": float(split_line[28]),
            "dc": float(split_line[29]),
            "DC MT": mt_vector_to_matrix([float(v) for v in split_line[30:36]]),
            "DC MT R": float(split_line[36]),
            "DC MT CN": float(split_line[37]),
            "MT confidence": float(split_line[38]),
            "strike": float(split_line[39]),
            "dip": float(split_line[40]),
            "rake": float(split_line[41]),
            "auxilliary strike": float(split_line[42]),
            "auxilliary dip": float(split_line[43]),
            "auxilliary rake": float(split_line[44]),
            "p trend": float(split_line[45]),
            "p plunge": float(split_line[46]),
            "b trend": float(split_line[47]),
            "b plunge": float(split_line[48]),
            "t trend": float(split_line[49]),
            "t plunge": float(split_line[50]),
            "event type": split_line[51].strip(),
            # 'treatment counter': int(float(split_line[52]))
        }
    return events


def read_treatment():
    treatment_csvs = glob.glob(f"{ROOT_DIR}\\PAD13-02 D1 Completion\\Well*\\*.csv")
    treatment_data = {}
    for csv in treatment_csvs:
        path_split = csv.split(os.sep)
        well = path_split[-2].split("#")[1]
        stage = path_split[-1].split()[-1].split(".")[0]
        f = open(csv)
        _head = f.readline()
        lines = f.readlines()
        f.close()
        for line in lines:
            lspl = line.split(",")
            dt = TIMEZONE.localize(datetime.strptime(lspl[0], "%Y-%m-%d %H:%M:%S"))
            utc_timestamp = datetime.strftime(dt.astimezone(UTC), "%Y%m%d%H%M%S.000000")
            treatment_data[utc_timestamp] = {
                "well": well,
                "stage": stage,
                "pressure": float(lspl[4]),
                "slurry_rate": float(lspl[6]),
                "downhole_proppant_concentration": float(lspl[13]),
            }
    return treatment_data


def read_wells():
    wells = {}
    well_files = glob.glob(f"{ROOT_DIR}PAD13-02 D1 Wells Survey\\*.las")
    for well_file in well_files:
        f = open(well_file)
        _head = [f.readline() for i in range(17)]
        lines = f.readlines()
        f.close()
        n_lines = len(lines)
        easting, northing, elevation, md = (
            np.zeros(n_lines),
            np.zeros(n_lines),
            np.zeros(n_lines),
            np.zeros(n_lines),
        )
        for i_line, line in enumerate(lines):
            md[i_line], easting[i_line], northing[i_line], elevation[i_line] = [
                float(s) for s in line.split()[:4]
            ]
        wellname = well_lookup[
            decorate_well_name(os.path.basename(well_file).split("_")[0])
        ]
        wells[wellname] = {
            "easting": easting.tolist(),
            "northing": northing.tolist(),
            "elevation": elevation.tolist(),
            "md": md.tolist(),
        }
    perfs = assign_stages(wells)
    for well in wells.keys():
        wells[well] = {**wells[well], **perfs[well]}
    return wells


def assign_stages(wells):
    filename = os.path.join(
        ROOT_DIR, "PAD13-02 D1 Completion", "Perf Times Summary report data.csv"
    )
    with open(filename) as f:
        _head = [f.readline() for i in range(2)]
        lines = f.readlines()
    perfs = {}
    for line in lines:
        lspl = line.split(",")
        well_name = lspl[0].split()[1]
        if well_name not in perfs:
            perfs[well_name] = {}
        timestamp = lspl[5]
        n_clusters = int(lspl[9])
        gun_size = float(lspl[11]) / 1000
        stage = f"Stage {lspl[2]}"
        top_zone_md = float(lspl[6])
        bottom_zone_md = float(lspl[7])
        top_mds = np.linspace(top_zone_md, bottom_zone_md - gun_size, n_clusters)
        bottom_mds = np.linspace(top_zone_md + gun_size, bottom_zone_md, n_clusters)
        perfs[well_name][stage] = {"timestamp": timestamp}
        for i_cluster, (top_md, bottom_md) in enumerate(zip(top_mds, bottom_mds)):
            cluster_name = f"Cluster {i_cluster+1}"
            perfs[well_name][stage][cluster_name] = {
                "top_east": np.interp(
                    top_md, wells[well_name]["md"], wells[well_name]["easting"]
                ),
                "top_north": np.interp(
                    top_md, wells[well_name]["md"], wells[well_name]["northing"]
                ),
                "top_elevation": np.interp(
                    top_md, wells[well_name]["md"], wells[well_name]["elevation"]
                ),
                "bottom_east": np.interp(
                    bottom_md, wells[well_name]["md"], wells[well_name]["easting"]
                ),
                "bottom_north": np.interp(
                    bottom_md, wells[well_name]["md"], wells[well_name]["northing"]
                ),
                "bottom_elevation": np.interp(
                    bottom_md, wells[well_name]["md"], wells[well_name]["elevation"]
                ),
            }
    return perfs


misfit_classifications = {
    "11_27": [
        {"counter": 0, "state": "PRE"},
        {"time": "20210627174240.000000", "counter": 1, "state": "FL"},
        {"time": "20210627175306.000000", "counter": 2, "state": "LP"},
        {"time": "20210627175838.000000", "counter": 3, "state": "HP"},
        {"time": "20210627190913.000000", "counter": 4, "state": "POST"},
    ],
    "11_29": [
        {"counter": 0, "state": "PRE"},
        {"time": "20210628031604.000000", "counter": 1, "state": "FL"},
        {"time": "20210628032904.000000", "counter": 2, "state": "LP"},
        {"time": "20210628033504.000000", "counter": 3, "state": "HP"},
        {"time": "20210628044203.000000", "counter": 4, "state": "POST"},
    ],
    "7_29": [
        {"counter": 0, "state": "PRE"},
        {"time": "20210710092902.000000", "counter": 1, "state": "FL"},
        {"time": "20210710094202.000000", "counter": 2, "state": "LP"},
        {"time": "20210710094702.000000", "counter": 3, "state": "HP"},
        {"time": "20210710105502.000000", "counter": 4, "state": "POST"},
    ],
    "8_21": [
        {"counter": 0, "state": "PRE"},
        {"time": "20210708134132.000000", "counter": 1, "state": "FL"},
        {"time": "20210708135432.000000", "counter": 2, "state": "LP"},
        {"time": "20210708140032.000000", "counter": 3, "state": "HP"},
        {"time": "20210708150932.000000", "counter": 4, "state": "POST"},
    ],
    "9_11": [
        {"counter": 0, "state": "PRE"},
        {"time": "20210706070836.000000", "counter": 1, "state": "FL"},
        {"time": "20210706072436.000000", "counter": 2, "state": "LP"},
        {"time": "20210706073036.000000", "counter": 3, "state": "HP"},
        {"time": "20210706083736.000000", "counter": 4, "state": "POST"},
    ],
    "9_24": [
        {"counter": 0, "state": "PRE"},
        {"time": "20210709074612.000000", "counter": 1, "state": "FL"},
        {"time": "20210709080124.000000", "counter": 2, "state": "LP"},
        {"time": "20210709080733.000000", "counter": 3, "state": "HP"},
        {"time": "20210709091637.000000", "counter": 4, "state": "POST"},
    ],
}


def classify_treatment(treatment_data_for_stage, tzero=None, interval=60):
    from scipy.signal import medfilt

    EPS = 1e-7
    well = list(treatment_data_for_stage.values())[0]["well"]
    stage = list(treatment_data_for_stage.values())[0]["stage"]
    if f"{well}_{stage}" in misfit_classifications:
        return misfit_classifications[f"{well}_{stage}"]
    if not tzero:
        tzero = pick_breakdown_time(treatment_data_for_stage)
    proppant = np.array(
        [
            v["downhole_proppant_concentration"]
            for v in treatment_data_for_stage.values()
        ]
    )
    times = np.array(list(treatment_data_for_stage.keys()))
    i_zero = np.where(times == tzero)[0][0]
    med_proppant = medfilt(proppant, 111)
    zone = [
        {"counter": 0, "state": "PRE"},
        {"time": tzero, "counter": 1, "state": "FL"},
    ]
    threshold_1 = LP_MIN_CONCENTRATION
    threshold_2 = HP_MIN_CONCENTRATION
    check_thresh1 = med_proppant - threshold_1
    check_thresh2 = med_proppant - threshold_2
    counter = 1

    for i_time, time in enumerate(times[i_zero::interval]):
        ii = i_time * interval + i_zero
        if (
            check_thresh1[ii] * check_thresh1[ii - interval] < EPS
            or check_thresh2[ii] * check_thresh2[ii - interval] < EPS
        ):
            counter += 1
            zone.append(zone_state(time, counter, med_proppant[ii]))

    zone[-1]["state"] = "POST"
    return zone


def classify_events_by_treatment(
    stage_events, stage_data, break_time, diffusivity=None
):
    if len(stage_events) == 0:
        return None
    treatment_zones = classify_treatment(stage_data, tzero=break_time)
    for event_id, event in stage_events.items():
        zone = in_zone(treatment_zones, event["utc datetime"])
        event["treatment_counter"] = zone["counter"]
        event["treatment_code"] = zone["state"]

    if diffusivity is not None:
        hrz_diff = diffusivity["horizontal"]
        t0 = diffusivity["t0"]
        for event in stage_events.values():
            hrz_diffusivity = (
                event["along trend"] ** 2 / (event["timestamp"] - t0).seconds / 4 / PI
            )
            if hrz_diffusivity > hrz_diff:
                event["treatment_code"] = "IS"
    return None


def in_zone(zones, timestamp):
    z_times = np.array(
        [datetime.strptime(z["time"], "%Y%m%d%H%M%S.%f") for z in zones[1:]]
    )
    i_zone = np.where(z_times < datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ"))[
        0
    ]
    if len(i_zone) == 0:
        return zones[0]
    return zones[i_zone[-1] + 1]


def zone_state(time, counter, med_proppant):
    if med_proppant > HP_MIN_CONCENTRATION:
        return {
            "time": time,
            "counter": counter,
            "state": "HP",
        }
    if med_proppant > LP_MIN_CONCENTRATION:
        return {
            "time": time,
            "counter": counter,
            "state": "LP",
        }

    return {
        "time": time,
        "counter": counter,
        "state": "FL",
    }


def read_tops():
    TOPS_DIR = r"C:\Users\adambaig\Project\ChevronPFI-2021-5750X\Horizons"
    tops = {}
    for tops_file in glob.glob(os.path.join(TOPS_DIR, "*.shx")):
        f = open(tops_file)
        header = [f.readline() for i in range(20)]
        lines = f.readlines()
        f.close()
        top = os.path.basename(tops_file).split(".")[0].lower()
        east_indexes = np.unique([int(line.split()[3]) for line in lines])
        north_indexes = np.unique([int(line.split()[4]) for line in lines])
        n_east, n_north = len(east_indexes), len(north_indexes)
        mean_tvdss = np.average([float(line.split()[2]) for line in lines])
        tops[top] = {
            "top_grid": -mean_tvdss * np.ones([n_east, n_north]),
            "e_grid": np.zeros(n_east),
            "n_grid": np.zeros(n_north),
        }
        for line in lines:
            lspl = line.split()
            i_east = np.argwhere(east_indexes == int(lspl[3]))[0][0]
            i_north = np.argwhere(north_indexes == int(lspl[4]))[0][0]
            tops[top]["top_grid"][i_east, i_north] = -float(lspl[2])
            tops[top]["e_grid"][i_east] = float(lspl[0])
            tops[top]["n_grid"][i_north] = float(lspl[1])
    return tops


def read_velocity_model(filename):
    with open(filename) as f:
        _head = f.readline()
        lines = f.readlines()
    velocity_model = []
    for line in lines:
        lspl = line.split(",")
        top = -float(lspl[0])
        vp = float(lspl[1])
        velocity_model.append(
            {"top": top, "vp": vp, "vs": vp / np.sqrt(3), "rho": 310 * vp**0.25}
        )
    return velocity_model


def read_amplitudes(filename):
    with open(filename) as f:
        _head = f.readline()
        lines = f.readlines()
    return {line.split(",")[0]: float(line.split(",")[1]) for line in lines}


def read_frac_dimensions():
    with open("frac_dimensions.csv") as f:
        _head = [f.readline() for i in range(3)]
        lines = f.readlines()
        frac_dimensions = {}
        for line in lines:
            split_line = line.split(",")
            well = split_line[0]
            stage = split_line[1]
            frac_dimensions[f"well {well} stage {stage}"] = {
                "n_events": int(split_line[3]),
                "half-length": float(split_line[4]),
                "half-width": float(split_line[5]),
                "half-height": float(split_line[6]),
                "azimuth": float(split_line[7]),
                "centroid": {
                    "along well": float(split_line[8]),
                    "across well": float(split_line[9]),
                    "relative elevation": float(split_line[10]),
                    "easting": float(split_line[11]),
                    "northing": float(split_line[12]),
                    "elevation": float(split_line[13]),
                },
            }
        return frac_dimensions


def is_numerical_string(string):
    try:
        float(string)
    except:
        return False
    return True


def read_stations():
    with open(r"MT_test_inputs\Chevron_13-02_Stations.csv") as f:
        _head = f.readline()
        lines = f.readlines()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        station_name = lspl[0].split("_")[0]
        stations[station_name] = {
            "e": float(lspl[1]),
            "n": float(lspl[2]),
            "z": float(lspl[3]),
        }
    return stations


def read_strain(strain_file, tensor=False):
    f = open(strain_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    n_lines = len(lines)
    grid_points = np.zeros([n_lines, 3])
    if not tensor:
        strain_flat = np.zeros(n_lines)
    else:
        strain_flat = np.zeros([n_lines, 6])
    for i_line, line in enumerate(lines):
        lspl = line.split(",")
        grid_points[i_line, :] = [float(s) for s in lspl[:3]]
        if not tensor:
            strain_flat[i_line] = float(lspl[3])
        else:
            strain_flat[i_line, :] = [float(s) for s in lspl[3:]]
    e_grid = np.unique(grid_points[:, 0])
    n_grid = np.unique(grid_points[:, 1])
    z_grid = np.unique(grid_points[:, 2])
    n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)

    return np.reshape(strain_flat, [n_e, n_n, n_z]), grid_points
