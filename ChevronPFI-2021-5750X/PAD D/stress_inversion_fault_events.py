import os
import json

from generalPlots import gray_background_with_grid
import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np

from mtPlots import plot_strike_dip_rake_rosettes_from_MTs
from sms_moment_tensor.stress_inversions import (
    iterate_Micheal_stress_inversion,
    determine_SH_max,
    decompose_stress,
    Simpson_Aphi,
)
from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge, mt_to_sdr
from obspy.imaging.beachball import beach

from read_inputs import read_final_catalog

CUTOFF_PERCENTILE = 0.95
MT_SIZE = 60
GRID_SPACING = 50
PI = np.pi


events = read_final_catalog(
    r"Catalog\Chevron_15-03_finalCatalog_withTagTreatmentCounter_final_wStageId.csv"
)

event_types = np.unique([v["event type"].strip() for v in events.values()])

with open("wells.json") as f:
    wells = json.load(f)

axis_color = {"s1": "firebrick", "s2": "forestgreen", "s3": "royalblue"}
r_bins = np.linspace(0, 1, 101)

good_mts = {k: v for k, v in events.items() if v["MT confidence"] > CUTOFF_PERCENTILE}

iterated_stress_tensor, iterations = {}, {}
good_mt_subdivided = {
    k: v for k, v in good_mts.items() if "PAD 13-02 Lower Fault" in v["event type"]
}

len(good_mt_subdivided)

for event in good_mt_subdivided.values():
    (
        event["strike"],
        event["dip"],
        event["rake"],
        event["aux strike"],
        event["aux dip"],
        event["aux rake"],
    ) = mt_to_sdr(event["DC MT"])
iterated_stress_tensor, iterations = iterate_Micheal_stress_inversion(
    good_mt_subdivided, n_iterations=1000, output_iterations=True
)
fig, ax = mpls.subplots()
f_hist, a_hist = plt.subplots()
r_iterations, axes_iteration, sh_max_iteration = [], [], []
for iteration in iterations:
    R, stress_axes = decompose_stress(iteration)
    r_iterations.append(R)
    axes_iteration.append(stress_axes)
    for axis, vector in stress_axes.items():
        trend, plunge = unit_vector_to_trend_plunge(vector)
        ax.line(
            plunge,
            trend,
            ".",
            alpha=0.03,
            color=axis_color[axis],
            zorder=3,
        )
    sh_max_iteration.append(determine_SH_max(iteration))
ax.grid()
fig.savefig(os.path.join("figures", f"stress_axes_lower_fault.png"))
a_hist.hist(
    r_iterations,
    r_bins,
    facecolor="forestgreen",
    edgecolor="0.1",
    zorder=2,
)
y1, y_max = a_hist.get_ylim()
R, stress_axes = decompose_stress(iterated_stress_tensor)
a_hist.plot([R, R], [0, y_max], "k")
a_hist.set_xlim([0, 1])
a_hist.set_ylim([0, y_max])
a_hist.set_xlabel("stress ratio, $R = 1 - \phi$")
a_hist.set_ylabel("count")
f_hist.savefig(os.path.join("figures", f"stress_ratio_lower_fault.png"))


R, stress_axes = decompose_stress(iterated_stress_tensor)
print(R)
for axis, unit_vector in stress_axes.items():
    print(axis, [f"{v:.1f}" for v in unit_vector_to_trend_plunge(unit_vector)])
