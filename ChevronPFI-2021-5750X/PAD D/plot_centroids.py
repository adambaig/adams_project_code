import json
import os

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np

from pfi_qi.engineering import find_perf_center
from generalPlots import gray_background_with_grid
from read_inputs import read_final_catalog, read_frac_dimensions

tab10 = cm.get_cmap("tab10")

with open("wells.json") as f:
    wells = json.load(f)

frac_dimensions = read_frac_dimensions()


fig, ax = plt.subplots(figsize=[12, 12])
ax.set_aspect("equal")
for well_id, well in wells.items():
    ax.plot(well["easting"], well["northing"], "0.2", zorder=-2)
    well_color = tab10(float(well_id) / 10 - 0.01)
    for stage_number in [k.split()[1] for k in well.keys() if "Stage" in k]:
        perf_center = find_perf_center(stage_number, well)
        ax.plot(
            perf_center["easting"],
            perf_center["northing"],
            "+",
            color=well_color,
            zorder=0,
        )
        well_stage = f'well {well_id} stage {stage_number.lstrip("0")}'
        if well_stage in frac_dimensions:
            centroid = frac_dimensions[well_stage]["centroid"]
            ax.plot(
                centroid["easting"],
                centroid["northing"],
                "o",
                color=well_color,
                markeredgecolor="0.2",
                zorder=0,
            )
            ax.plot(
                [centroid["easting"], perf_center["easting"]],
                [centroid["northing"], perf_center["northing"]],
                color=well_color,
                zorder=-1,
            )

gray_background_with_grid(ax)

fig.savefig("centroids.png", bbox_inches="tight")
