import json
import numpy as np
from read_inputs import read_wells

from pfi_qi.engineering import calc_well_trend


wells["11"]["Stage 10"]


wells = read_wells()
calc_well_trend(wells["11"])


well_schema = Schema(
    {
        str: {
            "easting": [float],
            "northing": [float],
            "elevation": [float],
            "md": [float],
            str: {
                "timestamp": str,
                str: {
                    "top_east": float,
                    "top_north": float,
                    "top_elevation": float,
                    "bottom_east": float,
                    "bottom_north": float,
                    "bottom_elevation": float,
                },
            },
        }
    }
)


well_schema.validate(wells["11"])

well = wells["11"]
well

top_easts, bottom_easts, top_norths, bottom_norths = np.array(
    [
        (
            s[perf]["top_east"],
            s[perf]["bottom_east"],
            s[perf]["top_north"],
            s[perf]["bottom_north"],
        )
        for s in [stage for k, stage in well.items() if "Stage" in k]
        for perf in s.keys()
        if "n_clusters" not in perf and "timestamp" not in perf
    ]
).T


[
    s[perf]["top_east"]
    for s in [stage for k, stage in well.items() if "Stage" in k]
    for perf in s.keys()
    if "timestamp" not in perf
]
