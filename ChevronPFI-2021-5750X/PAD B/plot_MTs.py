import json

import matplotlib.pyplot as plt

from matplotlib import cm
import numpy as np
from obspy.imaging.beachball import beach

from sms_moment_tensor.MT_math import mt_to_sdr

from mtPlots import (
    plot_strike_dip_rake_rosettes,
    makeBaseMap,
    hammer_projection,
    mtToLatLon,
    plot_lune_from_events_dictionary,
)
from generalPlots import gray_background_with_grid

from read_inputs import read_final_catalog

CUTOFF_PERCENTILE = 0.95
MT_SIZE = 60
GRID_SPACING = 50
TAB10 = cm.get_cmap("tab10")


def well_to_tab10(well):
    i_well = int(well) - 6.5
    i_well / 10
    return TAB10(i_well % 10 / 10)


events = read_final_catalog()

fig, ax = plt.subplots(figsize=[4, 8])

plot_lune_from_events_dictionary(ax, events, marker=".")


with open("wells.json") as f:
    wells = json.load(f)

event_colors = {
    "Frac Event": "firebrick",
    "Lower Fault Event": "forestgreen",
    "PAD A Fault Event": "royalblue",
    "Upper N Fault Event": "goldenrod",
    "Upper S Fault Event": "indigo",
}


good_mts = {k: v for k, v in events.items() if v["MT confidence"] > 0.95}
event_types = np.unique([v["event type"] for v in good_mts.values()])
good_mt_subdivided = {}
for event_type in event_types:
    if "Fault" in event_type:
        good_mt_subdivided[event_type] = {
            k: v for k, v in good_mts.items() if v["event type"] == event_type
        }
        for event in good_mt_subdivided[event_type].values():
            event["dc MT"] = event["DC MT"]
        fig = plot_strike_dip_rake_rosettes(
            good_mt_subdivided[event_type], color=event_colors[event_type]
        )
        event_type_name = event_type.replace(" ", "_")
        fig.savefig(f"figures//sdr_rosette_{event_type_name}.png")

good_mt_subdivided["Frac Event"] = {
    k: v for k, v in good_mts.items() if "Fault" not in v["event type"]
}
for event in good_mt_subdivided["Frac Event"].values():
    event["dc MT"] = event["DC MT"]

fig = plot_strike_dip_rake_rosettes(good_mt_subdivided["Frac Event"], color="firebrick")
fig.savefig(f"figures//sdr_rosette_Frac_Event_even_the_bad_ones.png")

fig, ax = plt.subplots(figsize=[14, 14])
ax.set_aspect("equal")

for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k")

ax.plot(
    [v["easting"] for v in events.values()],
    [v["northing"] for v in events.values()],
    ".",
    color="0.5",
    zorder=-2,
)


for event in good_mts.values():
    if "Fault" in event["event type"]:
        event_color = event_colors[event["event type"]]
    else:
        event_color = "firebrick"
    strike, dip, rake = mt_to_sdr(event["DC MT"], conjugate=False)
    beachball = beach(
        (strike, dip, rake),
        xy=(event["easting"], event["northing"]),
        width=MT_SIZE,
        facecolor=event_color,
        linewidth=0.25,
        zorder=4,
    )
    ax.add_collection(beachball)

gray_background_with_grid(ax)

fig.savefig("high_confidence_mts.png", bbox_inches="tight")


event_wells = np.unique(
    [v["well id"] for v in good_mt_subdivided["Frac Event"].values()]
)

for well in event_wells:
    well_events = {
        k: v
        for k, v in good_mt_subdivided["Frac Event"].items()
        if v["well id"] == well
    }
    if well == "-1":
        fig = plot_strike_dip_rake_rosettes(well_events, color="0.4")
        fig.savefig("figures//rosette_off_zone.png")
    else:
        fig = plot_strike_dip_rake_rosettes(
            well_events, color=well_to_tab10(well), orientation="vertical"
        )
        fig.savefig(f"figures//rosette_{well}.png")
