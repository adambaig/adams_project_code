import cv2
import numpy as np
import glob

movie_dir = r"strain_movie"
img_array = []
for filename in glob.glob(movie_dir + "\cumulative_strain*.png"):
    img = cv2.imread(filename)
    height, width, layers = img.shape
    size = (width, height)
    img_array.append(img)

out = cv2.VideoWriter(
    "cumulative_strain_frac.avi", cv2.VideoWriter_fourcc(*"DIVX"), 5, size
)

for i in range(len(img_array)):
    out.write(img_array[i])
out.release()
