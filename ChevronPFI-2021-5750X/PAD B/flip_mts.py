import os
import json
from datetime import datetime
import matplotlib

from sms_moment_tensor.MT_math import (
    decompose_MT,
    trend_plunge_to_unit_vector,
    mt_to_sdr,
    clvd_iso_dc,
    mt_matrix_to_vector,
)
from read_inputs import read_unflipped_catalog, read_wells
from generalPlots import gray_background_with_grid
from sklearn.cluster import AgglomerativeClustering
from shapely.geometry.polygon import Polygon
from shapely.geometry import Point
import numpy as np
import mplstereonet as mpls
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

# matplotlib.use("Qt5agg")


def norm_MT(mt):
    return mt / np.linalg.norm(mt)


def moment(event):
    return 10 ** (1.5 * event["magnitude"] + 9)


def scaled_mt(mt, moment_value):
    moment = 10 ** (1.5 * event["magnitude"] + 9)
    return np.sqrt(2) * mt * moment / np.linalg.norm(event["dc MT"])


def make_simple_gradient_from_white_cmap(end_color):
    red, green, blue = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, 1.0, 1.0), (1, red, red)],
        "green": [(0.0, 1.0, 1.0), (1, green, green)],
        "blue": [(0.0, 1.0, 1.0), (1, blue, blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


CUTOFF_PERCENTILE = 95 / 100

filename = r"catalogs\PADB_final_catalog_all_wStageId_wDist2Stages_tagged.csv"

events = read_unflipped_catalog(filename)
wells = read_wells()


for event_id, event in events.items():
    events[event_id] = {**event, **decompose_MT(event["DC MT"])}
good_mts = {k: v for k, v in events.items() if v["MT confidence"] > CUTOFF_PERCENTILE}
event_types = np.unique([v["event type"] for v in good_mts.values()])

good_mt_subdivided = {}
for event_type in event_types:
    good_mt_subdivided[event_type] = {
        k: v for k, v in good_mts.items() if v["event type"] == event_type
    }

for event_type, subset_events in good_mt_subdivided.items():
    print(event_type, len(subset_events))
    f1, a1 = mpls.subplots(1, 3, figsize=[16, 5])
    for i_axis, axis in enumerate(["p", "b", "t"]):
        a1[i_axis].density_contourf(
            [v[f"{axis}_plunge"] for v in subset_events.values()],
            [v[f"{axis}_trend"] for v in subset_events.values()],
            measurement="lines",
        )


# only frac events need subdivision and flipping
aggregate_mts_not_frac = {}
for event_type in event_types:
    if event_type != "Frac Event":
        aggregate_mts_not_frac[event_type] = sum(
            [
                v["DC MT"] / np.linalg.norm(v["DC MT"])
                for v in events.values()
                if v["event type"] == event_type
                and v["MT confidence"] > CUTOFF_PERCENTILE
            ]
        )
        for event in [v for v in events.values() if v["event type"] == event_type]:
            if np.tensordot(event["DC MT"], aggregate_mts_not_frac[event_type]) > 0:
                event["is flipped"] = False
            else:
                event["is flipped"] = True
                event["DC MT"] = -event["DC MT"]
                event["general MT"] = -event["general MT"]


good_frac_mts = good_mt_subdivided["Frac Event"]

colors = [
    "firebrick",
    "forestgreen",
    "darkgoldenrod",
    "steelblue",
    "indigo",
    "darkorange",
    "turquoise",
    "black",
]

n_mt = len(good_frac_mts)
if os.path.exists("mt_dot_product_pairs_frac_mt.json"):
    with open("mt_dot_product_pairs_not_fault_TM.json", "r") as read_file:
        dot_product_dict = json.load(read_file)
    dot_product_pairs = np.array(dot_product_dict["dot_product_pairs"])
else:
    dot_product_pairs = np.zeros([n_mt, n_mt])
    for i1, (id1, mt1) in enumerate(good_frac_mts.items()):
        for i2, (id2, mt2) in enumerate(good_frac_mts.items()):
            dc1_norm = mt1["DC MT"] / np.linalg.norm(mt1["DC MT"])
            dc2_norm = mt2["DC MT"] / np.linalg.norm(mt2["DC MT"])
            dot_product_pairs[i1, i2] = np.tensordot(dc1_norm, dc2_norm)
    dot_product_dict = {"dot_product_pairs": dot_product_pairs.tolist()}
    with open("mt_dot_product_pairs_not_fault_TM.json", "w") as write_file:
        json.dump(dot_product_dict, write_file)


distance_mat = abs(1 - abs(dot_product_pairs))
fig, ax = plt.subplots()
ax.set_aspect("equal")
ax.pcolor(distance_mat)
n_clusters = 6
clustering = AgglomerativeClustering(
    n_clusters=n_clusters,
    compute_distances=True,
    affinity="precomputed",
    linkage="complete",
).fit(distance_mat)
labels = clustering.labels_
for i_mt, (id, mt) in enumerate(good_frac_mts.items()):
    mt["mt cluster"] = labels[i_mt]

flip = [-1, 1, -1, 1, -1, 1, 1, 1]
len(good_frac_mts)
for event in events.values():
    event["is flipped"] = False
for i_cluster in range(n_clusters):
    mt_cluster = {
        k: v for k, v in good_frac_mts.items() if v["mt cluster"] == i_cluster
    }

    custom_cmap = make_simple_gradient_from_white_cmap(colors[i_cluster])
    mt_avg = flip[i_cluster] * sum(
        [v["DC MT"] / np.linalg.norm(v["DC MT"]) for k, v in mt_cluster.items()]
    )

    p_trend, b_trend, t_trend = [], [], []
    p_plunge, b_plunge, t_plunge = [], [], []

    for id, mt in mt_cluster.items():
        mt["old dc MT"] = mt["DC MT"]
        mt["old gen MT"] = mt["general MT"]
        if np.tensordot(mt_avg, mt["DC MT"]) < 0:
            mt["DC MT"] = -mt["DC MT"]
            mt["general MT"] = -mt["general MT"]
        decomp = decompose_MT(mt["DC MT"])
        p_trend.append(decomp["p_trend"])
        b_trend.append(decomp["b_trend"])
        t_trend.append(decomp["t_trend"])
        p_plunge.append(decomp["p_plunge"])
        b_plunge.append(decomp["b_plunge"])
        t_plunge.append(decomp["t_plunge"])
    f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = a3[0].density_contourf(
        p_plunge, p_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_b = a3[1].density_contourf(
        b_plunge, b_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_t = a3[2].density_contourf(
        t_plunge, t_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    f3.savefig(f"agglomerative_clustering_{i_cluster}.png")

for event in good_frac_mts.values():
    if np.tensordot(event["old dc MT"], event["DC MT"]) < 0:
        event["is flipped"] = True


frac_events = {k: v for k, v in events.items() if v["event type"] == "Frac Event"}

cluster_mts = [
    sum(
        mt["DC MT"] / np.linalg.norm(mt["DC MT"])
        for mt in good_frac_mts.values()
        if mt["mt cluster"] == ii
    )
    for ii in range(n_clusters)
]

low_q_frac = {
    k: v for k, v in frac_events.items() if v["MT confidence"] < CUTOFF_PERCENTILE
}
for event in low_q_frac.values():
    dot_products = np.zeros(n_clusters)
    for i_cluster in range(n_clusters):
        dot_products[i_cluster] = np.tensordot(
            norm_MT(event["DC MT"]), norm_MT(cluster_mts[i_cluster])
        )
    i_cluster_belongs = np.argmax(abs(dot_products))
    if dot_products[i_cluster_belongs] < 0:
        event["DC MT"] = -event["DC MT"]
        event["general MT"] = -event["general MT"]
        event["is flipped"] = True

for event in events.values():
    for key_to_pop in [
        "p_trend",
        "p_plunge",
        "b_trend",
        "b_plunge",
        "t_trend",
        "t_plunge",
        "lambda_1",
        "lambda_2",
        "lambda_3",
        "mt cluster",
        "old dc MT",
        "old gen MT",
        "dc MT",
        "gen MT",
    ]:
        if key_to_pop in event:
            event.pop(key_to_pop)

for event in events.values():
    event["utc datetime"] = datetime.strftime(
        event["UTC Date Time"], "%Y-%m-%dT%H:%M:%S.%fZ"
    )
    event.pop("UTC Date Time")

for event in events.values():
    try:
        event["DC MT"] = event["DC MT"].tolist()
    except:
        pass
    try:
        event["general MT"] = event["general MT"].tolist()
    except:
        pass


with open("flipped_events.json", "w") as f:
    json.dump(events, f)
