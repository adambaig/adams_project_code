import matplotlib.pyplot as plt
import numpy as np
import pickle

from read_inputs import read_velocity_model, read_amplitudes, read_stations, read_wells
from generalPlots import gray_background_with_grid
from sms_ray_modelling.raytrace import isotropic_ray_trace

from sms_moment_tensor.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)


amps = read_amplitudes(r"MT_test_inputs\4190090359240.csv")
starting_velocity_model = read_velocity_model(
    r"MT_test_inputs\PAD1302_startingModel.csv"
)
final_velocity_model = read_velocity_model(r"MT_test_inputs\final-optimal_Vel.csv")
source = {"e": 518041.25, "n": 6018405.0, "z": -2150.25}
stations_from_csv = read_stations()
wells = read_wells()


def getStationsFromTTT(TTT):
    outstations = []
    for ii, key in enumerate(TTT):
        if isinstance(TTT[key], dict):
            station = {
                "e": float(TTT[key]["easting"]),
                "n": float(TTT[key]["northing"]),
                "z": -1 * float(TTT[key]["depth"]),
                "name": key,
                "loc": "VC",
            }
            outstations.append(station)
    return outstations


def getVelfromTTT(TTT):
    velocity_model = []
    top = -1 * TTT["originz"]
    d1 = TTT["dz"]
    vel = TTT["vel1D"]
    for vp in vel:
        vs = vp / 1.73205080757
        rho = 310 * (vp**0.25)
        curlayer = {"vp": vp, "top": top, "vs": vs, "rho": rho}
        top = top - d1
        velocity_model.append(curlayer)
    return velocity_model


def getRayPaths(source, station, velocity_model):
    p_raypath = isotropic_ray_trace(source, station, velocity_model, "P")
    s_raypath = isotropic_ray_trace(source, station, velocity_model, "S")
    cos_incoming = np.sqrt(
        1.0
        - (p_raypath["hrz_slowness"]["e"] ** 2 + p_raypath["hrz_slowness"]["n"] ** 2)
        * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
    )
    return p_raypath, s_raypath, cos_incoming


def smooth_regularly_sampled_velocity_model(velocity_model, smoothing_distance=500):
    smoothed_velocity_model = []
    initial_dz = abs(velocity_model[0]["top"] - velocity_model[1]["top"])
    n_smoothing_layers = int(smoothing_distance / initial_dz)
    i_layer = n_smoothing_layers // 2
    for i_layer, layer in enumerate(
        velocity_model[n_smoothing_layers // 2 : -n_smoothing_layers // 2]
    ):
        layers_to_smooth = velocity_model[i_layer : i_layer + n_smoothing_layers]
        average_vp = np.average([v["vp"] for v in layers_to_smooth])
        average_vs = np.average([v["vs"] for v in layers_to_smooth])
        average_rho = np.average([v["rho"] for v in layers_to_smooth])
        smoothed_velocity_model.append(
            {
                "top": layer["top"],
                "vp": average_vp,
                "vs": average_vs,
                "rho": average_rho,
            }
        )
    return smoothed_velocity_model


with open("optimal-TTT.pkl", "rb") as f:
    TTT = pickle.load(f, encoding="latin1")

amps_used = {k: v for k, v in amps.items() if k[0] not in ["A", "S"]}

velocity_model = getVelfromTTT(TTT)
stations = getStationsFromTTT(TTT)
smooth_model = smooth_regularly_sampled_velocity_model(velocity_model)

A_matrix, amplitudes = [], []
for station_name in amps_used:
    station = [v for v in stations if v["name"] == station_name][0]
    raypath = isotropic_ray_trace(source, station, smooth_model, "P")
    A_matrix.append(inversion_matrix_row(raypath, "P"))

for station_name, amp in amps_used.items():
    station = [v for v in stations if v["name"] == station_name][0]
    p_raypath = isotropic_ray_trace(source, station, smooth_model, "P")
    s_raypath = isotropic_ray_trace(source, station, smooth_model, "S")
    cos_incoming = np.sqrt(
        1.0
        - (p_raypath["hrz_slowness"]["e"] ** 2 + p_raypath["hrz_slowness"]["n"] ** 2)
        * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
    )
    amplitudes.append(
        back_project_amplitude(p_raypath, s_raypath, amp, "P", Q=60) / cos_incoming
    )


moment_tensor = solve_moment_tensor(A_matrix, amplitudes)


fig, ax = plt.subplots(figsize=[12, 12])
ax.set_aspect("equal")
ax.plot(source["e"], source["n"], "go", markeredgecolor="k")
for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k", zorder=-3)
for station_name, amp in zip(amps_used.keys(), amplitudes):
    station = [v for v in stations if v["name"] == station_name][0]
    if np.iscomplex(amp):
        ax.plot(station["e"], station["n"], "kv")
    else:
        ax.plot(station["e"], station["n"], "wv", markeredgecolor="k")
    ax.text(station["e"] + 30, station["n"], station_name)
gray_background_with_grid(ax)
fig.savefig("complex_amplitudes.png")


far_station = "B1043"
near_station = "D1012"

fig, axes = plt.subplots(1, 2, figsize=[12, 6])
station = stations_from_csv[far_station]
p_raypath = isotropic_ray_trace(source, station, velocity_model, "P")
s_raypath = isotropic_ray_trace(source, station, velocity_model, "S")
cos_incoming = np.sqrt(
    1.0
    - (p_raypath["hrz_slowness"]["e"] ** 2 + p_raypath["hrz_slowness"]["n"] ** 2)
    * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
)
complex_amp = (
    back_project_amplitude(p_raypath, s_raypath, amp, "P", Q=60) / cos_incoming
)
