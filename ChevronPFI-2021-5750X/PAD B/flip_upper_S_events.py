import json
import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np

from sms_moment_tensor.MT_math import decompose_MT

CUTOFF_PERCENTILE = 0.95

with open("flipped_events.json") as f:
    events = json.load(f)

upper_S_fault_events = {
    k: v for k, v in events.items() if v["event type"] == "Upper S Fault Event"
}


event_types = np.unique([v["event type"] for v in good_mts.values()])

for event in upper_S_fault_events.values():
    event["DC MT"] = (-np.array(event["DC MT"])).tolist()
    event["general MT"] = (-np.array(event["general MT"])).tolist()

for event_id, event in events.items():
    events[event_id] = {**event, **decompose_MT(event["DC MT"])}
good_mts = {k: v for k, v in events.items() if v["MT confidence"] > CUTOFF_PERCENTILE}

good_mt_subdivided = {}
for event_type in event_types:
    good_mt_subdivided[event_type] = {
        k: v for k, v in good_mts.items() if v["event type"] == event_type
    }

for event_type, subset_events in good_mt_subdivided.items():
    print(event_type, len(subset_events))
    f1, a1 = mpls.subplots(1, 3, figsize=[16, 5])
    for i_axis, axis in enumerate(["p", "b", "t"]):
        a1[i_axis].density_contourf(
            [v[f"{axis}_plunge"] for v in subset_events.values()],
            [v[f"{axis}_trend"] for v in subset_events.values()],
            measurement="lines",
        )

with open("flipped_events.json", "w") as f:
    json.dump(events, f)
