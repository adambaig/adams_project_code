from generalPlots import gray_background_with_grid
from mtPlots import plot_strike_dip_rake_rosettes
from sms_moment_tensor.stress_inversions import (
    iterate_Micheal_stress_inversion,
    determine_SH_max,
    decompose_stress,
    Simpson_Aphi,
)
from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge, mt_to_sdr
from obspy.imaging.beachball import beach
import numpy as np
import mplstereonet as mpls
import json
import os

import matplotlib.pyplot as plt


CUTOFF_PERCENTILE = 0.95
MT_SIZE = 60
GRID_SPACING = 50
PI = np.pi


with open("flipped_events.json") as f:
    events = json.load(f)

with open("wells.json") as f:
    wells = json.load(f)
axis_color = {"s1": "firebrick", "s2": "forestgreen", "s3": "royalblue"}
r_bins = np.linspace(0, 1, 101)
event_colors = {
    "Frac Event": "firebrick",
    "Lower Fault Event": "forestgreen",
    "PAD A Fault Event": "royalblue",
    "Upper N Fault Event": "goldenrod",
    "Upper S Fault Event": "indigo",
}
good_mts = {k: v for k, v in events.items() if v["MT confidence"] > CUTOFF_PERCENTILE}
event_types = np.unique([v["event type"] for v in good_mts.values()])
good_mt_subdivided = {}
iterated_stress_tensor, iterations = {}, {}
for event_type in event_types:
    good_mt_subdivided[event_type] = {
        k: v for k, v in good_mts.items() if v["event type"] == event_type
    }
    for event in good_mt_subdivided[event_type].values():
        (
            event["strike"],
            event["dip"],
            event["rake"],
            event["aux strike"],
            event["aux dip"],
            event["aux rake"],
        ) = mt_to_sdr(event["DC MT"])
    (
        iterated_stress_tensor[event_type],
        iterations[event_type],
    ) = iterate_Micheal_stress_inversion(
        good_mt_subdivided[event_type], n_iterations=1000, output_iterations=True
    )
    event_type_name = event_type.replace(" ", "_").lower()
    fig, ax = mpls.subplots()
    f_hist, a_hist = plt.subplots()
    r_iterations, axes_iteration, sh_max_iteration = [], [], []
    for iteration in iterations[event_type]:
        R, stress_axes = decompose_stress(iteration)
        r_iterations.append(R)
        axes_iteration.append(stress_axes)
        for axis, vector in stress_axes.items():
            trend, plunge = unit_vector_to_trend_plunge(vector)
            ax.line(
                plunge,
                trend,
                ".",
                alpha=0.03,
                color=axis_color[axis],
                zorder=3,
            )
        sh_max_iteration.append(determine_SH_max(iteration))
    ax.grid()
    fig.savefig(os.path.join("figures", f"stress_axes_{event_type_name}.png"))
    a_hist.hist(
        r_iterations,
        r_bins,
        facecolor=event_colors[event_type],
        edgecolor="0.1",
        zorder=2,
    )
    y1, y_max = a_hist.get_ylim()
    a_hist.plot([R, R], [0, y_max], "k")
    a_hist.set_xlim([0, 1])
    a_hist.set_ylim([0, y_max])
    a_hist.set_xlabel("stress ratio, $R = 1 - \phi$")
    a_hist.set_ylabel("count")
    f_hist.savefig(os.path.join("figures", f"stress_ratio_{event_type_name}.png"))


for group, st in iterated_stress_tensor.items():
    R, stress_axes = decompose_stress(st)
    print(group, R)
    for axis, unit_vector in stress_axes.items():
        print(axis, unit_vector_to_trend_plunge(unit_vector))
