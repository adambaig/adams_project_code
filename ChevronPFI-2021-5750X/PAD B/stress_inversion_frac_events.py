import os
import json


import matplotlib.pyplot as plt
from matplotlib import cm, colors
import numpy as np
from obspy.imaging.beachball import beach
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from scipy.ndimage import zoom

from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge, mt_to_sdr
from sms_moment_tensor.stress_inversions import (
    iterate_Micheal_stress_inversion,
    determine_SH_max,
    decompose_stress,
    Simpson_Aphi,
)
from generalPlots import gray_background_with_grid
from mtPlots import plot_strike_dip_rake_rosettes

import custom_colormaps
from read_inputs import read_wells, read_final_catalog

PI = np.pi
D2R = PI / 180.0
UPPER_FAULT_MIN_ELEVATION = -2050
LOWER_FAULT_MAX_ELEVATION = -2200
GRID_SPACING = 100
SEARCH_RADIUS = 200
MINIMUM_EVENTS = 20
STRESS_AXIS_SCALE = 15
MT_SIZE = 60
MT_CONFIDENCE_CUTOFF = 0.95
stress_map = custom_colormaps.stress_map()
events = read_final_catalog()

wells = read_wells()

frac_events = {k: v for k, v in events.items() if not "Fault" in v["event type"]}
good_frac_mts = {
    k: v for k, v in frac_events.items() if v["MT confidence"] > MT_CONFIDENCE_CUTOFF
}


def r_nearest_neighbours(grid_point, events, radius):
    radius_sq = radius * radius
    ref_east, ref_north = grid_point["east"], grid_point["north"]
    cull_events = {
        k: v
        for k, v in events.items()
        if (
            (v["easting"] - ref_east) ** 2 + (v["northing"] - ref_north) ** 2
            < radius_sq
        )
    }
    return cull_events


good_frac_east = [v["easting"] for v in good_frac_mts.values()]
good_frac_north = [v["northing"] for v in good_frac_mts.values()]

east_grid = np.arange(
    min(good_frac_east), max(good_frac_east) + GRID_SPACING, GRID_SPACING
)
north_grid = np.arange(
    min(good_frac_north), max(good_frac_north) + GRID_SPACING, GRID_SPACING
)
quiver_east, quiver_north, trend_north, trend_east, a_phi = [], [], [], [], []
grid_east, grid_north, stress_tensors, iteration_realizations = [], [], [], []
max_plunge = np.zeros([len(east_grid), len(north_grid)])


stress_tensors = []

for i_east, east_point in enumerate(east_grid):
    for i_north, north_point in enumerate(north_grid):
        grid_point = {"east": east_point, "north": north_point}
        nn_events = r_nearest_neighbours(grid_point, good_frac_mts, SEARCH_RADIUS)
        if len(nn_events) >= MINIMUM_EVENTS:
            stress_tensor, iterations = iterate_Micheal_stress_inversion(
                nn_events, 100, output_iterations=True
            )
            stress_tensors.append(stress_tensor)
            sh_max = determine_SH_max(stress_tensor)
            average_east = np.average(
                np.array([v["easting"] for k, v in nn_events.items()])
            )
            average_north = np.average(
                np.array([v["northing"] for k, v in nn_events.items()])
            )
            quiver_east.append(average_east)
            quiver_north.append(average_north)
            grid_east.append(east_point)
            grid_north.append(north_point)
            R, stress_axes = decompose_stress(stress_tensor)
            trend_s2, plunge_s2 = unit_vector_to_trend_plunge(stress_axes["s2"])
            trend_east.append(STRESS_AXIS_SCALE * np.sin(np.pi * sh_max / 180.0))
            trend_north.append(STRESS_AXIS_SCALE * np.cos(np.pi * sh_max / 180.0))
            a_phi.append(Simpson_Aphi(stress_tensor))
            stress_tensors.append(stress_tensor)
            iteration_realizations.append(iterations)
            max_plunge[i_east, i_north] = (
                180
                * np.arcsin(max([abs(axis[2]) for axis in stress_axes.values()]))
                / PI
            )


with open("frac_stress_inversions.csv", "w") as f:
    f.write("easting, northing,")
    for i in range(1, 4):
        f.write(f"sigma {i} trend, sigma {i} plunge,")
    f.write("stress ratiom, sh_max, a_phi\n")
    for east, north, stress_tensor in zip(grid_east, grid_north, stress_tensors):
        R, stress_axes = decompose_stress(stress_tensor)
        sh_max = determine_SH_max(stress_tensor)
        a_phi = Simpson_Aphi(stress_tensor)
        f.write(f"{east:.1f},{north:.1f},")
        for axis in ["s1", "s2", "s3"]:
            trend, plunge = unit_vector_to_trend_plunge(stress_axes[axis])
            f.write(f"{trend:.1f},{plunge:.1f},")
        f.write(f"{R:.3f}, {sh_max:.1f}, {a_phi:.3f}\n")


fig, ax = plt.subplots(figsize=[16, 16])
ax.set_aspect("equal")
for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k")

for event in good_frac_mts.values():
    beachball = beach(
        np.array([event["strike"], event["dip"], event["rake"]]),
        xy=(event["easting"], event["northing"]),
        width=MT_SIZE,
        facecolor="darkgoldenrod",
        alpha=0.8,
        linewidth=0.25,
        zorder=3,
    )
    ax.add_collection(beachball)

ax.quiver(
    grid_east,
    grid_north,
    trend_east,
    trend_north,
    color=stress_map(np.array(a_phi) / 3),
    pivot="mid",
    width=0.003,
    zorder=5,
    headlength=0,
    headaxislength=0,
    headwidth=0,
    scale=1 / STRESS_AXIS_SCALE,
    scale_units="x",
    edgecolor="k",
    linewidth=0.5,
)
gray_background_with_grid(ax)

fig.savefig("stress_inversion_ticks.png")
