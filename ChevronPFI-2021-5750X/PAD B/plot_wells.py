from generalPlots import gray_background_with_grid
from read_inputs import read_wells, well_lookup
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np

TAB10 = cm.get_cmap("tab10")
wells = read_wells()
reverse_well_lookup = {v: k for k, v in well_lookup.items()}


def get_tab10_color(stage_name):
    stage_int = int(stage_name[-1])
    return TAB10(((stage_int - 0.1) / 10) % 1)


fig, ax = plt.subplots(figsize=[10, 10])
ax.set_aspect("equal")
for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k")
    for stage, clusters in {k: v for k, v in well.items() if "Stage" in k}.items():
        color = get_tab10_color(stage)
        for cluster_name, cluster in {
            k: v for k, v in clusters.items() if "Cluster" in k
        }.items():
            ax.plot(
                [cluster["top easting"], cluster["bottom easting"]],
                [cluster["top northing"], cluster["bottom northing"]],
                color=color,
                lw=10,
                zorder=4,
            )
        ax.text(
            cluster["top easting"] + 20,
            cluster["top northing"] + 20,
            stage.split()[-1],
            fontsize=8,
        )
gray_background_with_grid(ax)
