from pfi_fs.simulate_events import generate_1Csuperstation_waveforms

for directory_name in [
    "15_stations_13_center",
    "30_stations_13_center",
    "60_stations_19_center",
]:
    nsuper, _, nstations, position = directory_name.split("_")
    sim_config = "example_simulation_00.cfg"
    station_file = "example_stations_015.csv"

    generate_1Csuperstation_waveforms(
        "example_curve_00x00y_source.cfg",
        sim_config,
        "homogeneous.cfg",
        station_file,
        directory_name=directory_name,
        plot_out=True,
    )
