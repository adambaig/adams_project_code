import numpy as np

from sms_moment_tensor.MT_math import mt_to_sdr, decompose_MT

from read_inputs import read_flipped_catalog, write_catalog

out_file = "Catalog//ConocoPhillips_A13_0_360.csv"
events = read_catalog()
for id, event in events.items():
    decomp = decompose_MT(event["dc MT"])
    strike, dip, rake, aux_strike, aux_dip, aux_rake = mt_to_sdr(event["dc MT"])
    events[id] = {
        **event,
        **decomp,
        "strike": strike,
        "dip": dip,
        "rake": rake,
        "aux strike": aux_strike,
        "aux dip": aux_dip,
        "aux rake": aux_rake,
        "gen MT norm": event["gen MT"] / np.linalg.norm(event["gen MT"]),
        "dc MT norm": event["dc MT"] / np.linalg.norm(event["dc MT"]),
    }
out = write_catalog(events, out_file)
