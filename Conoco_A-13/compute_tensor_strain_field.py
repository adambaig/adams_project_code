import numpy as np
import matplotlib.pyplot as plt
from pfi_qi.QI_analysis import strain_grid
from generalPlots import gray_background_with_grid
from read_inputs import read_flipped_catalog, get_velocity_model

events = read_flipped_catalog()
PI = np.pi
D2R = PI / 180.0

velocity_model = get_velocity_model()

for event in events.values():
    event["elevation"] = -event["depth"]
    event["magnitude"] = event["moment magnitude"]
mt_good = {k: v for k, v in events.items() if v["mt confidence"] > 0.95}


grid_spacing = 20
min_neighbours = 5
max_radius = 40

strain_tensor, grid_object = strain_grid(
    mt_good,
    grid_spacing,
    min_neighbours,
    max_radius,
    "tensor.csv",
    velocity_model,
    tensor=True,
)
