import numpy as np
import matplotlib.pyplot as plt
from obspy import UTCDateTime

from pfi_qi.QI_analysis import calculate_dimensions, fracture_dimensions
from pfi_qi.engineering import calc_well_trend
from pfi_qi.rotations import rotate_from_cardinal

from generalPlots import gray_background_with_grid
from read_inputs import read_catalog, read_wells

PI = np.pi
D2R = PI / 180.0
center = {"easting": 566690.7491049999, "northing": 6283335.8436}
angle = -20.1 * D2R
magnitude_of_completeness = -1.3
catalogs = {
    "P_only": r"Catalog\ConocoPhillips_A13_PFI_FinalCatalog_Ponly.csv",
    "P_and_S": r"Catalog\ConocoPhillips_A13_PFI_FinalCatalog_PS_S-statics.csv",
}
wells = read_wells()
for tag, catalog in catalogs.items():
    events = read_catalog(filename=catalog)

    def add_elevation(events):
        for event in events.values():
            event["elevation"] = -event["depth"]

    add_elevation(events)
    events = rotate_from_cardinal(events, angle, center)
    wells = rotate_from_cardinal(wells, angle, center)
    frac_events = {
        k: v
        for k, v in events.items()
        if v["perp to trend"] > -1500
        and v["perp to trend"] < 1700
        and v["depth"] < 1450
        and v["moment magnitude"] > magnitude_of_completeness
        and v["well"] != "-1.0"
        and v["hrz distance from perf center"] < 500
    }
    dimensions = fracture_dimensions(
        frac_events, wells, csv_out=f"frac_dimensions_{tag}.csv"
    )
