import matplotlib.pyplot as plt
import numpy as np
from read_inputs import read_catalog, get_velocity_model

events = read_catalog()
velocity_model = get_velocity_model()


def add_elevation(events):
    for event in events.values():
        event["elevation"] = -event["depth"]


add_elevation(events)


def add_vs_source(events):
    tops = np.array([layer["top"] for layer in velocity_model[1:]])
    for event in events.values():
        elevation = event["elevation"]
        if elevation > tops[0]:
            event["vs_source"] = velocity_model[0]["vs"]
        else:
            i_layer = np.where(tops > elevation)[0][-1] + 1
            event["vs_source"] = velocity_model[i_layer]["vs"]


add_vs_source(events)
spec_fit_events = {k: v for k, v in events.items() if v["corner frequency"] > 0}


for event in spec_fit_events.values():
    event["radius"] = 0.32 * event["vs_source"] / event["corner frequency"]
    event["stress drop"] = 7 * event["moment"] / 16 / event["radius"] ** 3

moments = [v["moment"] for v in spec_fit_events.values()]
stress_drops = [v["stress drop"] for v in spec_fit_events.values()]
radiuses = [v["radius"] for v in spec_fit_events.values()]


fig, ax = plt.subplots()
ax.loglog(radiuses, moments, ".", alpha=0.2)

x_min, x_max = 3, 300
for d_sigma in [1e4, 1e5, 1e6, 1e7]:
    y1 = 16 * d_sigma * x_min**3 / 7
    y2 = 16 * d_sigma * x_max**3 / 7
    ax.loglog([x_min, x_max], [y1, y2])

ax.set_xlim([x_min, x_max])
ax.set_xlabel("source radius (m)")
ax.set_ylabel("moment")

fig.savefig("scaling_for_CoP_events.png")
