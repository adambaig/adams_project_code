import matplotlib.pyplot as plt
import numpy as np

# from sms_moment_tensor.MT_math import pt_to_sdr, sdr_to_mt, decompose_MT, mt_to_sdr, clvd_iso_dc

# from plotting_stuff import plot_strike_dip_rake_rosettes, plot_strain_stereonets, plot_strain_stereonets_points
from read_inputs import read_flipped_catalog, read_catalog


def plot_strike_dip_rake_rosettes(
    events, mt_key="dc MT", bin_degrees=15, color="steelblue"
):
    n_events = 2 * len(events)
    strikes, dips, rakes = (
        np.zeros(n_events),
        np.zeros(n_events),
        np.zeros(n_events),
    )
    for i_event, event in enumerate(events.values()):
        (
            strikes[2 * i_event],
            dips[2 * i_event],
            rakes[2 * i_event],
            strikes[2 * i_event + 1],
            dips[2 * i_event + 1],
            rakes[2 * i_event + 1],
        ) = mt_to_sdr(event[mt_key])
    strikes_180 = strikes % 180
    strikes_360 = strikes_180 + 180
    mirrored_strikes = np.hstack([strikes_180, strikes_360])
    fig = plt.figure(figsize=[16, 5])
    ax_strike = fig.add_axes([0.05, 0.1, 0.38, 0.8], projection="polar")
    ax_dip = fig.add_axes([0.44, 0.1, 0.12, 0.8], projection="polar")
    ax_rake = fig.add_axes([0.57, 0.1, 0.38, 0.8], projection="polar")
    bin_degrees = 5
    strike_bins = np.arange(0, 360.0001, bin_degrees)
    n_strike, _ = np.histogram(mirrored_strikes, strike_bins)
    ax_strike.bar(
        D2R * (strike_bins[:-1] + bin_degrees / 2),
        n_strike / 2.0,
        width=D2R * bin_degrees,
        color=color,
    )
    ax_strike.set_theta_zero_location("N")
    ax_strike.set_theta_direction(-1)
    ax_strike.set_thetagrids(
        np.arange(0, 360, 45), ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
    )

    dip_bins = np.arange(0, 90.0001, bin_degrees)
    n_dip, _ = np.histogram(dips, dip_bins)
    ax_dip.bar(
        D2R * (dip_bins[:-1] + bin_degrees / 2),
        n_dip,
        width=D2R * bin_degrees,
        color=color,
    )
    ax_dip.set_theta_direction(-1)
    ax_dip.set_thetamax(90)

    rake_bins = np.arange(-180, 180.0001, bin_degrees)
    n_rake, _ = np.histogram(rakes, rake_bins)
    ax_rake.bar(
        D2R * (rake_bins[:-1] + bin_degrees / 2),
        n_rake,
        width=D2R * bin_degrees,
        color=color,
    )
    ax_rake.set_thetagrids(np.arange(0, 360, 90), ["LL", "TH", "RL", "NR"])
    return fig


PI = np.pi
D2R = PI / 180.0

conf_threshold = 0.95
flipped = read_flipped_catalog()
unflipped = read_catalog()
list(events.values())[0]
best_flipped = {k: v for k, v in flipped.items() if v["mt confidence"] > conf_threshold}
best_unflipped = {
    k: v for k, v in unflipped.items() if v["mt confidence"] > conf_threshold
}
strain_axes_flipped = plot_strain_stereonets(
    best_flipped, mt_key="dc MT", color="firebrick"
)

strain_axes_unflipped = plot_strain_stereonets(
    best_unflipped, mt_key="dc MT", color="forestgreen"
)

flipped_rosettes = plot_strike_dip_rake_rosettes(best_events)
flipped_rosettes = plot_strike_dip_rake_rosettes(best_unflipped)

strain_axes_flipped = plot_strain_stereonets_points(
    best_flipped, mt_key="dc MT", color="firebrick"
)

strain_axes_unflipped = plot_strain_stereonets_points(
    best_unflipped, mt_key="dc MT", color="forestgreen"
)

unflipped = read_catalog()
for id, event in unflipped.items():
    decomp = decompose_MT(event["dc MT"])
    unflipped[id] = {**event, **decomp}
unflipped_ss_axes = plot_strain_stereonets(
    {
        k: v
        for k, v in unflipped.items()
        if v["b_plunge"] > 0 and v["mt confidence"] > 0.95
    },
    mt_key="dc MT",
    color="firebrick",
)


# plt.hist([v["b plunge"] for v in events.values()])

# flipped_strain = plot_strain_stereonets(best_events, mt_key="dc flip", color="firebrick")
# flipped_rosette = plot_strike_dip_rake_rosettes(events, mt_key="dc MT", color="firebrick")
#
#
# strain_axes.savefig("unflipped.png")
# flipped_strain.savefig("flipped.png")
