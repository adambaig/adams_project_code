from datetime import datetime, timedelta
import matplotlib.cm as cm
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
import pytz

# from generalPlots import gray_background_with_grid
from pfi_qi.engineering import calc_well_trend, find_perf_center
from pfi_qi.rotations import rotate_perf_clusters, rotate_from_cardinal
from sms_moment_tensor.MT_math import mt_to_sdr, decompose_MT

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")
tab10 = cm.get_cmap("tab10")

well_colors = {
    "A-13-L": tab10(0.05),
    "A-A13-L": tab10(0.15),
    "A-B13-L": tab10(0.25),
    "A-C13-L": tab10(0.35),
    "A-D13-L": tab10(0.45),
    "A-E13-L": tab10(0.55),
    "A-F13-L": tab10(0.65),
    "A-G13-L": tab10(0.85),
    "A-H13-L": tab10(0.95),
}


def plot_strike_dip_rake_rosettes(
    events, mt_key="dc MT", bin_degrees=15, color="steelblue"
):
    n_events = 2 * len(events)
    strikes, dips, rakes = (
        np.zeros(n_events),
        np.zeros(n_events),
        np.zeros(n_events),
    )
    for i_event, event in enumerate(events.values()):
        (
            strikes[2 * i_event],
            dips[2 * i_event],
            rakes[2 * i_event],
            strikes[2 * i_event + 1],
            dips[2 * i_event + 1],
            rakes[2 * i_event + 1],
        ) = mt_to_sdr(event[mt_key])
    strikes_180 = strikes % 180
    strikes_360 = strikes_180 + 180
    mirrored_strikes = np.hstack([strikes_180, strikes_360])
    fig = plt.figure(figsize=[16, 5])
    ax_strike = fig.add_axes([0.05, 0.1, 0.38, 0.8], projection="polar")
    ax_dip = fig.add_axes([0.44, 0.1, 0.12, 0.8], projection="polar")
    ax_rake = fig.add_axes([0.57, 0.1, 0.38, 0.8], projection="polar")
    bin_degrees = 5
    strike_bins = np.arange(0, 360.0001, bin_degrees)
    n_strike, _ = np.histogram(mirrored_strikes, strike_bins)
    ax_strike.bar(
        D2R * (strike_bins[:-1] + bin_degrees / 2),
        n_strike / 2.0,
        width=D2R * bin_degrees,
        color=color,
    )
    ax_strike.set_theta_zero_location("N")
    ax_strike.set_theta_direction(-1)
    ax_strike.set_thetagrids(
        np.arange(0, 360, 45), ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
    )

    dip_bins = np.arange(0, 90.0001, bin_degrees)
    n_dip, _ = np.histogram(dips, dip_bins)
    ax_dip.bar(
        D2R * (dip_bins[:-1] + bin_degrees / 2),
        n_dip,
        width=D2R * bin_degrees,
        color=color,
    )
    ax_dip.set_theta_direction(-1)
    ax_dip.set_thetamax(90)

    rake_bins = np.arange(-180, 180.0001, bin_degrees)
    n_rake, _ = np.histogram(rakes, rake_bins)
    ax_rake.bar(
        D2R * (rake_bins[:-1] + bin_degrees / 2),
        n_rake,
        width=D2R * bin_degrees,
        color=color,
    )
    ax_rake.set_thetagrids(np.arange(0, 360, 90), ["LL", "TH", "RL", "NR"])
    return fig


def make_simple_gradient_from_white_cmap(end_color):
    red, green, blue = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, 1.0, 1.0), (1, red, red)],
        "green": [(0.0, 1.0, 1.0), (1, green, green)],
        "blue": [(0.0, 1.0, 1.0), (1, blue, blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


def plot_strain_stereonets(events, mt_key="dc MT", color="steelblue"):
    p_trend, b_trend, t_trend = [], [], []
    p_plunge, b_plunge, t_plunge = [], [], []
    custom_cmap = make_simple_gradient_from_white_cmap(color)
    for id, event in events.items():
        decomp = decompose_MT(event[mt_key])
        p_trend.append(decomp["p_trend"])
        b_trend.append(decomp["b_trend"])
        t_trend.append(decomp["t_trend"])
        p_plunge.append(decomp["p_plunge"])
        b_plunge.append(decomp["b_plunge"])
        t_plunge.append(decomp["t_plunge"])

    fig, ax = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = ax[0].density_contourf(
        p_plunge, p_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_b = ax[1].density_contourf(
        b_plunge, b_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_t = ax[2].density_contourf(
        t_plunge, t_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    return fig


def plot_strain_stereonets_points(
    events,
    mt_key="dc MT",
):
    p_trend, b_trend, t_trend = [], [], []
    p_plunge, b_plunge, t_plunge = [], [], []

    for id, event in events.items():
        decomp = decompose_MT(event[mt_key])
        p_trend.append(decomp["p_trend"])
        b_trend.append(decomp["b_trend"])
        t_trend.append(decomp["t_trend"])
        p_plunge.append(decomp["p_plunge"])
        b_plunge.append(decomp["b_plunge"])
        t_plunge.append(decomp["t_plunge"])

    fig, ax = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = ax[0].line(
        p_plunge,
        p_trend,
    )
    cax_b = ax[1].line(
        b_plunge,
        b_trend,
    )
    cax_t = ax[2].line(t_plunge, t_trend, mp)
    return fig


def plot_stage(stage_events, wells, title=None, grid_spacing=50, limits=None):
    stage = np.unique([v["stage"] for v in stage_events.values()])[0]
    well_id = np.unique([v["well"] for v in stage_events.values()])[0]
    well_trend = calc_well_trend(wells[well_id])
    mod_stage = int(stage[-1])
    stage_color = tab10((mod_stage + 0.1) / 10)
    fig, ax = plt.subplots(figsize=[12, 12])
    ax.set_aspect("equal")
    for perf, cluster in wells[well_id]["Stage " + stage].items():
        cluster["easting"] = 0.5 * (cluster["top_east"] + cluster["bottom_east"])
        cluster["northing"] = 0.5 * (cluster["top_north"] + cluster["bottom_north"])
        cluster["elevation"] = 0.5 * (
            cluster["top_elevation"] + cluster["bottom_elevation"]
        )
    if "treatment_code" in list(stage_events.values())[0]:
        fluid_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "FL"
        }
        lp_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "LP"
        }
        hp_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "HP"
        }
        pre_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "PRE"
        }
        post_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "POST"
        }
        is_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "IS"
        }

        for events in (fluid_events, lp_events, hp_events, post_events):
            if len(events) == 0:
                continue
            treatment_code = list(events.values())[0]["treatment_code"]
            ax.plot(
                [v["easting"] for v in events.values()],
                [v["northing"] for v in events.values()],
                symbol[treatment_code],
                c=stage_color,
                markeredgecolor="k",
                linewidth=0.25,
                zorder=3,
                ms=10,
                alpha=0.8,
            )
        for events in (pre_events, is_events):
            if len(events) == 0:
                continue
            treatment_code = list(events.values())[0]["treatment_code"]
            ax.plot(
                [v["easting"] for v in is_events.values()],
                [v["northing"] for v in is_events.values()],
                symbol[treatment_code],
                c="w",
                markeredgecolor="k",
                linewidth=0.25,
                zorder=3,
                ms=10,
                alpha=0.8,
            )
    else:
        ax.plot(
            [v["easting"] for v in stage_events.values()],
            [v["northing"] for v in stage_events.values()],
            "o",
            c=stage_color,
            markeredgecolor="k",
            linewidth=0.25,
            zorder=3,
            ms=10,
            alpha=0.8,
        )
    ax.plot(
        [v["easting"] for v in wells[well_id]["Stage " + stage.zfill(2)].values()],
        [v["northing"] for v in wells[well_id]["Stage " + stage.zfill(2)].values()],
        marker=(7, 1, 0),
        c=stage_color,
        ms=16,
        markeredgecolor="k",
        linewidth=0.25,
        zorder=2,
    )
    if limits is not None:
        ax.set_xlim(limits["x1"], limits["x2"])
        ax.set_ylim(limits["y1"], limits["y2"])
    x1, x2 = ax.get_xlim()
    y1, y2 = ax.get_ylim()
    ax.plot(
        wells[well_id]["easting"],
        wells[well_id]["northing"],
        lw=4,
        c="0.7",
        zorder=0,
    )
    ax.plot(
        wells[well_id]["easting"],
        wells[well_id]["northing"],
        lw=5,
        c="0.3",
        zorder=-1,
    )
    for well2 in wells.values():
        ax.plot(well2["easting"], well2["northing"], lw=2, c="0.8", zorder=-2)
        ax.plot(well2["easting"], well2["northing"], lw=2.5, c="0.4", zorder=-3)
    ax.set_xlim([x1, x2])
    ax.set_ylim([y1, y2])
    # ax = gray_background_with_grid(ax, grid_spacing=50)
    ax.set_title(title)
    return fig


def add_dimension_arrows_abs_coords(axis, arrows):
    axis.arrow(
        arrows["lengthwise"]["easting"][0],
        arrows["lengthwise"]["northing"][0],
        np.diff(arrows["lengthwise"]["easting"])[0],
        np.diff(arrows["lengthwise"]["northing"])[0],
        zorder=27,
        lw=3,
        color="k",
    )
    axis.arrow(
        arrows["lengthwise"]["easting"][0],
        arrows["lengthwise"]["northing"][0],
        np.diff(arrows["lengthwise"]["easting"])[0],
        np.diff(arrows["lengthwise"]["northing"])[0],
        zorder=28,
        lw=1,
        color="w",
    )
    axis.arrow(
        arrows["widthwise"]["easting"][0],
        arrows["widthwise"]["northing"][0],
        np.diff(arrows["widthwise"]["easting"])[0],
        np.diff(arrows["widthwise"]["northing"])[0],
        zorder=27,
        lw=3,
        color="k",
    )
    axis.arrow(
        arrows["widthwise"]["easting"][0],
        arrows["widthwise"]["northing"][0],
        np.diff(arrows["widthwise"]["easting"])[0],
        np.diff(arrows["widthwise"]["northing"])[0],
        zorder=28,
        lw=1,
        color="w",
    )
    return axis
