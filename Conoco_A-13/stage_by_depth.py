import numpy as np
import matplotlib.pyplot as plt
from obspy import UTCDateTime

from pfi_qi.QI_analysis import calculate_dimensions, fracture_dimensions
from pfi_qi.engineering import calc_well_trend
from pfi_qi.rotations import rotate_from_cardinal

from generalPlots import gray_background_with_grid
from read_inputs import read_catalog, read_wells

PI = np.pi
D2R = PI / 180.0
center = {"easting": 566690.7491049999, "northing": 6283335.8436}
angle = -20.1 * D2R
magnitude_of_completeness = -1.3
wells = read_wells()
events = read_catalog()

events_PS = read_catalog(
    filename="Catalog\ConocoPhillips_A13_PFI_FinalCatalog_PS_S-statics.csv"
)
# import json
#
# for well in wells.values():
#     well['easting'] = well['easting'].tolist()
#     well['northing'] = well['northing'].tolist()
#     well['elevation'] = well['elevation'].tolist()
#     well['measured_depth'] = well['measured_depth'].tolist()
#
#
# with open('wells.json','w') as f:
#     json.dump(wells,f )


def add_elevation(events):
    for event in events.values():
        event["elevation"] = -event["depth"]


add_elevation(events)
add_elevation(events_PS)


events = rotate_from_cardinal(events, angle, center)
events_PS = rotate_from_cardinal(events_PS, angle, center)
wells = rotate_from_cardinal(wells, angle, center)

frac_events = {
    k: v
    for k, v in events.items()
    if v["perp to trend"] > -1500
    and v["perp to trend"] < 1700
    and v["depth"] < 1450
    and v["moment magnitude"] > magnitude_of_completeness
}
west_fault_events = {k: v for k, v in events.items() if v["perp to trend"] <= -1500}
east_fault_events = {k: v for k, v in events.items() if v["perp to trend"] >= 1700}
deep_fault_events = {k: v for k, v in events.items() if v["depth"] >= 1450}
stage_events = {k: v for k, v in frac_events.items() if v["well"] != "-1.0"}
dimensions = fracture_dimensions(stage_events, wells, csv_out="frac_dimensions.csv")

frac_PS = {
    k: v
    for k, v in events_PS.items()
    if v["perp to trend"] > -1500
    and v["perp to trend"] < 1700
    and v["depth"] < 1450
    and v["moment magnitude"] > magnitude_of_completeness
}
stage_events_PS = {k: v for k, v in frac_PS.items() if v["well"] != "-1.0"}
stage = "34"
well = "A-C13-L"
set_events = {
    k: v
    for k, v in stage_events.items()
    if v["stage"] == stage
    and v["well"] == well
    and v["hrz distance from perf center"] < 500
}

time = [v["elapsed time"] for v in set_events.values()]
elev = [v["elevation"] for v in set_events.values()]

set_events_PS = {
    k: v
    for k, v in stage_events_PS.items()
    if v["stage"] == stage
    and v["well"] == well
    and v["hrz distance from perf center"] < 500
}

time_PS = [v["elapsed time"] for v in set_events_PS.values()]
elev_PS = [v["elevation"] for v in set_events_PS.values()]

fig, ax1 = plt.subplots(1, figsize=[10, 6])

x1, x2 = ax1.get_xlim()
perf_evel = wells[well]["Stage " + stage]["cluster 0"]["bottom_elevation"]
ax1.plot([x1, x2], [perf_evel, perf_evel], "k")
for id in set_events:
    id2 = [
        k
        for k, v in set_events_PS.items()
        if abs(v["elapsed time"] - set_events[id]["elapsed time"]) < 10
    ][0]
    ax1.plot(
        set_events[id]["elapsed time"],
        set_events[id]["elevation"],
        "o",
        color="royalblue",
        zorder=12,
    )
    ax1.plot(
        set_events_PS[id2]["elapsed time"],
        set_events_PS[id2]["elevation"],
        "o",
        color="firebrick",
        zorder=13,
    )
    ax1.plot(
        [set_events[id]["elapsed time"], set_events_PS[id2]["elapsed time"]],
        [set_events[id]["elevation"], set_events_PS[id2]["elevation"]],
        "k",
        zorder=11,
    )


ax1.set_title("P only")
ax1.set_xlabel("elapsed time")
ax1.set_ylabel("elevation (m)")
ax1.set_xlim([x1, 1800])

ax2.plot(time_PS, elev_PS, "o")
x1, x2 = ax2.get_xlim()
perf_evel = wells[well]["Stage " + stage]["cluster 0"]["bottom_elevation"]
ax2.plot([x1, x2], [perf_evel, perf_evel], "k")
ax2.set_title("P and S")
ax2.set_xlabel("elapsed time")
ax2.set_ylabel("elevation (m)")
ax2.set_xlim([x1, 1800])

fig.savefig("stage_C_34")

UTC = [UTCDateTime(v["UTC"]) for v in events.values()]

# stage_east = [v["easting"] for v in events.values() if v["well"]==well and v["stage"]==stage]
# stage_north = [v["northing"] for v in events.values() if v["well"]==well and v["stage"]==stage]
perp = np.array([v["perp to trend"] for v in events.values()])
along = np.array([v["along trend"] for v in events.values()])

# stage_index = []
fig, ax = plt.subplots(figsize=[12, 12])
ax.set_aspect("equal")
for well in wells.values():
    ax.plot(well["perp to trend"], well["along trend"], "0.2", zorder=-2)

# ax.plot(stage_east, stage_north, 'bo', markeredgecolor='k')
ax.plot(
    perp[np.where(perp > -1500)[0]],
    along[np.where(perp > -1500)[0]],
    ".",
    alpha=0.05,
    c="forestgreen",
)
ax.plot(
    perp[np.where(perp < -1500)[0]],
    along[np.where(perp < -1500)[0]],
    ".",
    alpha=0.1,
    c="orangered",
)
gray_background_with_grid(ax)
fig.savefig("hows_this.png")
