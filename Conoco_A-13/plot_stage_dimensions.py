import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np

D2R = np.pi / 180

tab10 = cm.get_cmap("tab10")
well_colors = {
    "A-13-L": tab10(0.05),
    "A-A13-L": tab10(0.15),
    "A-B13-L": tab10(0.25),
    "A-C13-L": tab10(0.35),
    "A-D13-L": tab10(0.45),
    "A-E13-L": tab10(0.55),
    "A-F13-L": tab10(0.65),
    "A-G13-L": tab10(0.85),
    "A-H13-L": tab10(0.95),
}
frac_dimensions = {
    "P_only": r"frac_dimensions_P_only.csv",
    "P_and_S": r"frac_dimensions_P_and_S.csv",
}

for tag, frac_dimension_file in frac_dimensions.items():
    f = open(frac_dimension_file)
    header = [f.readline() for i in range(3)]
    lines = f.readlines()
    f.close()

    stage_dimensions = {}
    for line in lines:
        lspl = line.split(",")
        well = lspl[0]
        stage = lspl[1]
        if float(stage) < 10:
            stage = "0" + stage
        well_stage_identifier = f"Well {well}: Stage {stage}"
        stage_dimensions[well_stage_identifier] = {
            "well": well,
            "stage": stage,
            "unique well stage id": int(lspl[2]),
            "n_events": lspl[3],
            "half-length": float(lspl[4]),
            "half-width": float(lspl[5]),
            "half-height": float(lspl[6]),
            "azimuth": float(lspl[7]),
            "centroid": {
                "along well": float(lspl[8]),
                "across well": float(lspl[9]),
                "elevation": float(lspl[10]),
            },
        }

    fig_length_hist, ax_length_hist = plt.subplots(
        3, 3, figsize=[12, 12], sharex=True, sharey=True
    )
    fig_width_hist, ax_width_hist = plt.subplots(
        3, 3, figsize=[12, 12], sharex=True, sharey=True
    )
    fig_height_hist, ax_height_hist = plt.subplots(
        3, 3, figsize=[12, 12], sharex=True, sharey=True
    )
    max_scale = [500, 500, 500]
    key = ["half-length", "half-width", "half-height"]
    for i_ax, ax in enumerate([ax_length_hist, ax_width_hist, ax_height_hist]):
        for i_well, well in enumerate(
            [
                "A-E13-L",
                "A-D13-L",
                "A-C13-L",
                "A-H13-L",
                "A-G13-L",
                "A-F13-L",
                "A-B13-L",
                "A-A13-L",
                "A-13-L",
            ]
        ):
            i_1 = i_well // 3
            i_2 = i_well % 3
            a = ax[i_1, i_2]
            half_dim = [
                d[key[i_ax]] for d in stage_dimensions.values() if d["well"] == well
            ]
            bins = np.linspace(0, max_scale[i_ax], 40)
            a.hist(half_dim, bins, facecolor=well_colors[well])
            a.text(0.9, 0.8, well, ha="right", transform=a.transAxes)
        ax[2, 0].set_xlabel("distance (m)")
        ax[2, 1].set_xlabel("distance (m)")
        ax[2, 2].set_xlabel("distance (m)")
        ax[0, 0].set_ylabel("# of stages")
        ax[1, 0].set_ylabel("# of stages")
        ax[2, 0].set_ylabel("# of stages")
    fig_length_hist.savefig(
        f"Figures/stage_halflength_histograms_{tag}.png",
        bbox_inches="tight",
        transparent=True,
    )
    fig_width_hist.savefig(
        f"Figures/stage_halfwidth_histograms_{tag}.png",
        bbox_inches="tight",
        transparent=True,
    )
    fig_height_hist.savefig(
        f"Figures/stage_halfheight_histograms_{tag}.png",
        bbox_inches="tight",
        transparent=True,
    )

    fig_rosette = plt.figure(figsize=[12, 12])
    for i_well, well in enumerate(
        [
            "A-E13-L",
            "A-D13-L",
            "A-C13-L",
            "A-H13-L",
            "A-G13-L",
            "A-F13-L",
            "A-B13-L",
            "A-A13-L",
            "A-13-L",
        ]
    ):
        subaxes = 331 + i_well
        ax_rosette = fig_rosette.add_subplot(subaxes, projection="polar")
        azimuths_to_180 = np.array(
            [v["azimuth"] for v in stage_dimensions.values() if v["well"] == well]
        )
        azimuths = np.hstack([azimuths_to_180, azimuths_to_180 + 180])
        plotting_azimuths = (90 - azimuths) % 360
        bin_number = 37
        bins = np.linspace(0, 2 * np.pi, bin_number)
        n_az, _, _ = plt.hist(plotting_azimuths * D2R, bins, visible=False)
        width = 2 * np.pi / bin_number
        # bars = ax.bar(bins[:-1], n_rake, width=width, facecolor="royalblue", label='All Events')
        bars = ax_rosette.bar(bins[:-1], n_az, width=width, facecolor=well_colors[well])
        lines, labels = plt.thetagrids(
            [0, 90, 180, 270],
            [
                "E",
                "N",
                "W",
                "S",
            ],
        )
        ax_rosette.set_title(well, loc="left")
    fig_rosette.savefig(
        f"Figures//stage_azimuths_{tag}.png", bbox_inches="tight", transparent=True
    )
