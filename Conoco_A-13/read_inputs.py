import glob
import numpy as np
import os
from nmxseis.numerics.moment_tensor import MomentTensor

# from sms_moment_tensor.MT_math import mt_vector_to_matrix, mt_matrix_to_vector


def read_catalog(filename="Catalog\ConocoPhillips_A13_PFI_FinalCatalog_Ponly.csv"):
    f = open(filename)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        id = lspl[0]
        events[id] = {
            "UTC": lspl[1],
            "MST": lspl[2],
            "easting": float(lspl[3]),
            "northing": float(lspl[4]),
            "depth": float(lspl[5]),
            "moment magnitude": float(lspl[6]),
            "moment": float(lspl[7]),
            "corner frequency": float(lspl[8]),
            "log stack": float(lspl[9]),
            "easting uncertainty": float(lspl[10]),
            "northing uncertainty": float(lspl[11]),
            "depth uncertainty": float(lspl[12]),
            "well": lspl[13].replace("013-", "13-"),
            "well id": lspl[14],
            "stage": lspl[15].zfill(2),
            "stage id": lspl[16],
            "within stage": lspl[17],
            "asset id": lspl[18],
            "hrz distance from perf center": float(lspl[19]),
            "total distance from perf center": float(lspl[20]),
            "elapsed time": float(lspl[21]),
            "gen MT": MomentTensor.from_vector(
                np.array([float(s) for s in lspl[22:28]])
            ),
            "gen R": float(lspl[28]),
            "gen CN": float(lspl[29]),
            "clvd percentage": float(lspl[30]),
            "iso percentage": float(lspl[31]),
            "dc percentage": float(lspl[32]),
            "dc MT": MomentTensor.from_vector(
                np.array([float(s) for s in lspl[33:39]])
            ),
            "dc R": float(lspl[39]),
            "dc CN": float(lspl[40]),
            "mt confidence": float(lspl[41]),
            "strike": float(lspl[42]),
            "dip": float(lspl[43]),
            "rake": float(lspl[44]),
            "aux strike": float(lspl[45]),
            "aux dip": float(lspl[46]),
            "aux rake": float(lspl[47]),
            "p trend": float(lspl[48]),
            "p plunge": float(lspl[49]),
            "b trend": float(lspl[50]),
            "b plunge": float(lspl[51]),
            "t trend": float(lspl[52]),
            "t plunge": float(lspl[53]),
            "flipped": lspl[54].strip().upper() == "TRUE",
        }
    return events


def write_catalog(events, file_name):
    f = open(file_name, "w")
    f.write("event ID,UTC time,MST time,easting,northing,depth,")
    f.write("moment magnitude,moment,corner frequency,log stack,well,")
    f.write("well id,stage,within stage,asset id,gen EE,gen NN,gen ZZ,")
    f.write("gen EN,gen EZ,gen NZ,gen EE norm,gen NN norm, gen ZZ norm,")
    f.write("gen EN norm,gen EZ norm, gen NZ norm,gen R,gen CN,clvd,iso,dc,")
    f.write("dc EE,dc NN, dc ZZ,dc EN,dc EZ,dc NZ,")
    f.write(
        "dc EE norm,dc NN norm, dc ZZ norm,dc EN norm,dc EZ norm,dc NZ norm,dc R, dc CN,mt confidence,"
    )
    f.write("strike,dip,rake,aux strike,aux dip,aux rake,")
    f.write("p trend,p plunge,b trend,b plunge,t trend,t plunge,flipped\n")

    for id, event in events.items():
        f.write(f'{id},{event["UTC"]},{event["MST"]},{event["easting"]},')
        f.write(f'{event["northing"]},{event["depth"]},{event["moment magnitude"]},')
        f.write(f'{event["moment"]},{event["corner frequency"]},{event["log stack"]},')
        f.write(f'{event["well"]},{event["well_id"]},{event["stage"]},')
        f.write(f'{event["within stage"]},{event["asset id"]},')
        [f.write(f"{m},") for m in mt_matrix_to_vector(event["gen MT"])]
        [f.write(f"{m},") for m in mt_matrix_to_vector(event["gen MT norm"])]
        f.write(f'{event["gen R"]},{event["gen CN"]},')
        f.write(f'{event["clvd"]},{event["iso"]},{event["dc"]},')
        [f.write(f"{m},") for m in mt_matrix_to_vector(event["dc MT"])]
        [f.write(f"{m},") for m in mt_matrix_to_vector(event["dc MT norm"])]
        f.write(f'{event["dc R"]},{event["dc CN"]},{event["mt confidence"]},')
        f.write(f'{event["strike"]%360},{event["dip"]},{event["rake"]},')
        f.write(f'{event["aux strike"]%360},{event["aux dip"]},{event["aux rake"]},')
        f.write(f'{event["p_trend"]%360},{event["p_plunge"]},')
        f.write(f'{event["b_trend"]%360},{event["b_plunge"]},')
        f.write(f'{event["t_trend"]%360},{event["t_plunge"]},')
        f.write(f'{event["flipped"]}\n')

    f.close()
    return file_name


def read_flipped_catalog(filename="Catalog//ConocoPhillips_A13_PFI_FlippedMT.csv"):
    f = open(filename)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        id = lspl[0]
        events[id] = {
            "UTC": lspl[1],
            "MST": lspl[2],
            "easting": float(lspl[3]),
            "northing": float(lspl[4]),
            "depth": float(lspl[5]),
            "moment magnitude": float(lspl[6]),
            "moment": float(lspl[7]),
            "corner frequency": float(lspl[8]),
            "log stack": float(lspl[9]),
            "well": lspl[10].replace("013-", "13-"),
            "well_id": lspl[11],
            "stage": lspl[12].zfill(2),
            "within stage": lspl[13],
            "asset id": lspl[14],
            "gen MT": MomentTensor.from_vector(
                np.array([float(s) for s in lspl[15:21]])
            ),
            "gen R": float(lspl[27]),
            "gen CN": float(lspl[28]),
            "clvd": float(lspl[29]),
            "iso": float(lspl[30]),
            "dc": float(lspl[31]),
            "dc MT": MomentTensor.from_vector(
                np.array([float(s) for s in lspl[32:38]])
            ),
            "dc R": float(lspl[44]),
            "dc CN": float(lspl[45]),
            "mt confidence": float(lspl[46]),
            "p trend": float(lspl[-7]) % 360,
            "p plunge": float(lspl[-6]),
            "b trend": float(lspl[-5]) % 360,
            "b plunge": float(lspl[-4]),
            "t trend": float(lspl[-3]) % 360,
            "t plunge": float(lspl[-2]),
        }
    return events


def get_velocity_model():
    f = open("optimal-vel.csv")
    head = f.readline()
    first_line = f.readline()
    lines = f.readlines()
    f.close()
    lspl = first_line.split(",")
    velocity_model = [
        {
            "vp": float(lspl[1]),
            "vs": float(lspl[2]),
            "rho": 310 * float(lspl[1]) ** 0.25,
        }
    ]
    top = 1200
    for line in lines:
        lspl = line.split(",")
        top -= float(lspl[0]) * 10
        velocity_model.append(
            {
                "vp": float(lspl[1]),
                "vs": float(lspl[2]),
                "top": top,
                "rho": 310 * float(lspl[1]) ** 0.25,
            }
        )
    return velocity_model


def get_perf_centers():
    f = open("StageLocations.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    perfs = {}
    for line in lines:
        lspl = line.split(",")
        well = lspl[0].replace("A-", "a-")
        stage = lspl[2]
        easting = float(lspl[4])
        northing = float(lspl[5])


def read_wells():
    base_dir = "wells\\"
    wells = {}
    # perfs = get_perfs()
    for well_file in glob.glob(base_dir + "*.well"):
        f = open(well_file)
        head = f.readline()
        lines = f.readlines()
        f.close()
        n_well = len(lines)
        easting, northing, tvdss, md = (
            np.zeros(n_well),
            np.zeros(n_well),
            np.zeros(n_well),
            np.zeros(n_well),
        )
        for ii, line in enumerate(lines):
            lspl = line.split(",")
            md[ii], easting[ii], northing[ii], tvdss[ii] = [float(s) for s in lspl[:4]]
        well = os.path.basename(well_file).split(".")[0].replace("a-", "A-")
        wells[well] = {
            "measured_depth": md,
            "easting": easting,
            "northing": northing,
            "elevation": -tvdss,
        }
    f = open("StageLocations.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    perfs = {}

    for line in lines:
        lspl = line.split(",")
        well = lspl[0]
        stage = "Stage " + lspl[2].zfill(2)
        easting = float(lspl[4])
        northing = float(lspl[5])
        depth = float(lspl[6])
        if well not in perfs:
            perfs[well] = {}
        perfs[well][stage] = {
            "cluster 0": {
                "top_east": easting,
                "bottom_east": easting,
                "top_north": northing,
                "bottom_north": northing,
                "top_elevation": -depth,
                "bottom_elevation": -depth,
            }
        }
    for well in wells.keys():
        wells[well].update(perfs[well])

    return wells


def read_perfs(filename="Catalog//relocCatalog_logSNR_out_Ponly_Perfs_update.csv"):
    f = open(filename)
    head = f.readline()
    lines = f.readlines()
    f.close()
    imaged_perfs, actual_perfs = {}, {}
    for line in lines:
        lspl = line.split(",")
        imaged_perfs[lspl[0]] = {
            "easting": float(lspl[2]),
            "northing": float(lspl[3]),
            "elevation": -float(lspl[4]),
        }
        actual_perfs[lspl[0]] = {
            "easting": float(lspl[9]),
            "northing": float(lspl[10]),
            "elevation": -float(lspl[11]),
        }
    return imaged_perfs, actual_perfs


def read_stations():
    with open("PreStackStationLocations.csv") as f:
        head = [f.readline() for i in range(2)]
        lines = f.readlines()
    stations = {}
    for line in lines:
        split_line = line.split(",")
        if split_line[0][-2:] == "00":
            stations[split_line[0]] = {
                "easting": float(split_line[1]),
                "northing": float(split_line[2]),
                "elevation": float(split_line[3]),
            }
    return stations
