import numpy as np
import matplotlib.pyplot as plt
from obspy import UTCDateTime

from pfi_qi.QI_analysis import calculate_dimensions, fracture_dimensions
from pfi_qi.engineering import calc_well_trend
from pfi_qi.rotations import rotate_from_cardinal
from generalPlots import gray_background_with_grid

from plotting_stuff import plot_stage, add_dimension_arrows_abs_coords
from read_inputs import read_catalog, read_wells

PI = np.pi
D2R = PI / 180.0
center = {"easting": 566690.7491049999, "northing": 6283335.8436}
angle = -20.1 * D2R
magnitude_of_completeness = -1.3

catalogs = {
    "P_only": r"Catalog\ConocoPhillips_A13_PFI_FinalCatalog_Ponly.csv",
    "P_and_S": r"Catalog\ConocoPhillips_A13_PFI_FinalCatalog_PS_S-statics.csv",
}


wells = read_wells()
wells = rotate_from_cardinal(wells, angle, center)

events = read_catalog()
events = rotate_from_cardinal(events, angle, center)


def add_elevation(events):
    for event in events.values():
        event["elevation"] = -event["depth"]


add_elevation(events)
frac_events = {
    k: v
    for k, v in events.items()
    if v["perp to trend"] > -1500
    and v["perp to trend"] < 1700
    and v["depth"] < 1450
    and v["moment magnitude"] > magnitude_of_completeness
    and v["well"] != "-1.0"
    and v["hrz distance from perf center"] < 500
}
dimensions = fracture_dimensions(frac_events, wells, return_arrows_too=True)
unique_stages = np.unique([f'{v["well"]}_{v["stage"]}' for v in frac_events.values()])
stage = unique_stages[12]
for stage in unique_stages:
    well, stage_name = stage.split("_")
    well_stage_identifier = f"Well {well}: Stage {stage_name}"
    stage_events = {
        k: v
        for k, v in frac_events.items()
        if v["well"] == well
        and v["stage"] == stage_name
        and v["moment magnitude"] > magnitude_of_completeness
    }
    if well_stage_identifier in dimensions:
        fig_en = plot_stage(stage_events, wells)
        ax = add_dimension_arrows_abs_coords(
            fig_en.axes[0], dimensions[well_stage_identifier]["arrows"]
        )
        fig_en.savefig(
            f"Figures//Cardinal stages with dimensions//en_w{well}_s{stage_name}.png"
        )
        plt.close(fig_en)
