import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize
from mtPlots import makeBaseMap, hammer_projection, mtToLatLon

from read_inputs import read_flipped_catalog

events = {k: v for k, v in read_flipped_catalog().items() if v["mt confidence"] > 0.95}

r_imp = np.array([(v["gen R"] - v["dc R"]) for v in events.values()])


# depth = np.array([(v["depth"]) for v in events.values()])

# i_order = np.argsort(r_imp)

# fig, ax = plt.subplots(figsize=[6,12])
# makeBaseMap(ax)
# lon,lat = np.array([mtToLatLon(event["gen MT"]) for event in events.values()]).T
# x_lune, y_lune = hammer_projection(lat,lon)
# scatter = ax.scatter( x_lune[i_order],y_lune[i_order], c=r_imp[i_order], s=3)
# ax.set_aspect('equal')
# ax.set_axis_off()
# cb = fig.colorbar(scatter, ax=ax)
# cb.set_label('R improvement from dc to general solution')
# fig.savefig('lune_plot.png', bbox_inches="tight")
#
#
# fig, ax = plt.subplots(figsize=[6,12])
# makeBaseMap(ax)
# # lon,lat = np.array([mtToLatLon(event["gen MT"]) for event in events.values()]).T
# # x_lune, y_lune = hammer_projection(lat,lon)
# scatter = ax.scatter( x_lune[i_order],y_lune[i_order], c=depth[i_order], s=3, cmap='gnuplot_r', vmin=1000, vmax=1500)
# ax.set_aspect('equal')
# ax.set_axis_off()
# cb = fig.colorbar(ScalarMappable(norm=Normalize(vmin=1000, vmax = 1500), cmap='gnuplot'), ax=ax)
# cb.set_label('TVDss (m)')
# ticks = cb.get_ticks()
# cb.ax.set_yticklabels([int(t) for t in ticks[::-1]])
# # cb = fig.colorbar(scatter, ax=ax)
# fig.savefig('lune_plot_depth.png', bbox_inches="tight")
#

tab10 = cm.get_cmap("tab10")
well_colors = {
    "A-13-L": tab10(0.05),
    "A-A13-L": tab10(0.15),
    "A-B13-L": tab10(0.25),
    "A-C13-L": tab10(0.35),
    "A-D13-L": tab10(0.45),
    "A-E13-L": tab10(0.55),
    "A-F13-L": tab10(0.65),
    "A-G13-L": tab10(0.85),
    "A-H13-L": tab10(0.95),
}


f3, a3 = plt.subplots(3, 6, figsize=[12, 12])
for i_well, well in enumerate(
    [
        "A-E13-L",
        "A-D13-L",
        "A-C13-L",
        "A-H13-L",
        "A-G13-L",
        "A-F13-L",
        "A-B13-L",
        "A-A13-L",
        "A-13-L",
    ]
):
    i_1 = i_well // 3
    i_2 = 2 * (i_well % 3) + i_1 % 2
    a = a3[i_1, i_2]

    well_events = {k: v for k, v in events.items() if v["well"] == well}
    makeBaseMap(a)
    lon, lat = np.array(
        [mtToLatLon(event["gen MT"]) for event in well_events.values()]
    ).T
    x_lune, y_lune = hammer_projection(lat, lon)
    scatter = a.plot(x_lune, y_lune, ".", color=well_colors[well], alpha=0.1)
    a.set_aspect("equal")
    a.set_axis_off()
    a3[i_1, (i_2 + 1) % 6].set_axis_off()
f3.savefig("lunes_by_well.png", bbox_inches="tight", transparent=True)
