import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as mcolors
from obspy import UTCDateTime
import scipy.stats as st

from pfi_qi.QI_analysis import calculate_dimensions, fracture_dimensions
from pfi_qi.engineering import calc_well_trend
from pfi_qi.rotations import rotate_from_cardinal

from generalPlots import gray_background_with_grid
from read_inputs import read_catalog, read_wells

PI = np.pi
D2R = PI / 180.0
center = {"easting": 566690.7491049999, "northing": 6283335.8436}
angle = -20.1 * D2R
magnitude_of_completeness = -1.3

catalogs = {
    "P_only": r"Catalog\ConocoPhillips_A13_PFI_FinalCatalog_Ponly.csv",
    "P_and_S": r"Catalog\ConocoPhillips_A13_PFI_FinalCatalog_PS_S-statics.csv",
}


wells = read_wells()
wells = rotate_from_cardinal(wells, angle, center)

tab10 = cm.get_cmap("tab10")
well_colors = {
    "A-13-L": tab10(0.05),
    "A-A13-L": tab10(0.15),
    "A-B13-L": tab10(0.25),
    "A-C13-L": tab10(0.35),
    "A-D13-L": tab10(0.45),
    "A-E13-L": tab10(0.55),
    "A-F13-L": tab10(0.65),
    "A-G13-L": tab10(0.85),
    "A-H13-L": tab10(0.95),
}


def make_simple_gradient_from_white_cmap(end_color):
    red, green, blue = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, 1.0, 1.0), (1, red, red)],
        "green": [(0.0, 1.0, 1.0), (1, green, green)],
        "blue": [(0.0, 1.0, 1.0), (1, blue, blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


def add_elevation(events):
    for event in events.values():
        event["elevation"] = -event["depth"]


for tag, catalog in catalogs.items():
    events = read_catalog(filename=catalog)
    add_elevation(events)
    events = rotate_from_cardinal(events, angle, center)
    frac_events = {
        k: v
        for k, v in events.items()
        if v["perp to trend"] > -1500
        and v["perp to trend"] < 1700
        and v["depth"] < 1450
        and v["moment magnitude"] > magnitude_of_completeness
    }
    west_fault_events = {k: v for k, v in events.items() if v["perp to trend"] <= -1500}
    east_fault_events = {k: v for k, v in events.items() if v["perp to trend"] >= -1700}
    deep_fault_events = {k: v for k, v in events.items() if v["depth"] <= 1450}
    stage_events = {k: v for k, v in frac_events.items() if v["well"] != "-1.0"}
    for event in stage_events.values():
        well = wells[event["well"]]
        event["r"] = event["perp to trend"] - np.interp(
            event["along trend"], well["along trend"], well["perp to trend"]
        )
        event["z"] = event["elevation"] - np.interp(
            event["along trend"], well["along trend"], well["elevation"]
        )

    dimensions = fracture_dimensions(
        stage_events, wells, csv_out=f"frac_dimensions_{tag}.csv"
    )
    UTC = [UTCDateTime(v["UTC"]) for v in events.values()]

    # stage_east = [v["easting"] for v in events.values() if v["well"]==well and v["stage"]==stage]
    # stage_north = [v["northing"] for v in events.values() if v["well"]==well and v["stage"]==stage]
    perp = np.array([v["perp to trend"] for v in events.values()])
    along = np.array([v["along trend"] for v in events.values()])

    fig, ax = plt.subplots(3, 3, figsize=[12, 12], sharex=True, sharey=True)
    for i_well, well in enumerate(
        [
            "A-E13-L",
            "A-D13-L",
            "A-C13-L",
            "A-H13-L",
            "A-G13-L",
            "A-F13-L",
            "A-B13-L",
            "A-A13-L",
            "A-13-L",
        ]
    ):
        i_1 = i_well // 3
        i_2 = i_well % 3
        a = ax[i_1, i_2]
        well_events = {
            k: v
            for k, v in stage_events.items()
            if v["well"] == well and v["hrz distance from perf center"] < 500
        }
        a.plot(
            np.array([v["r"] for v in well_events.values()]),
            [v["elapsed time"] for v in well_events.values()],
            ".",
            color="0.2",
            alpha=0.1,
            zorder=3,
        )
        xx, yy = np.mgrid[-500:500:200j, 0:18000:200j]
        positions = np.vstack([xx.ravel(), yy.ravel()])
        values_A = np.vstack(
            [
                np.array([v["r"] for v in well_events.values()]),
                [v["elapsed time"] for v in well_events.values()],
            ]
        )
        kernel_A = st.gaussian_kde(values_A)
        contour_A = np.reshape(kernel_A(positions).T, xx.shape)
        positions = np.vstack([xx.ravel(), yy.ravel()])
        cf_set_A = a.contourf(
            xx,
            yy,
            contour_A,
            cmap=make_simple_gradient_from_white_cmap(well_colors[well]),
            zorder=-10,
            alpha=0.9,
        )
        a.text(480, 1000, well, ha="right")
        a.set_yticks(np.arange(0, 18001, 3600))
        diff_sq_neg = [
            v["r"] ** 2 / v["elapsed time"] for v in well_events.values() if v["r"] > 0
        ]
        diff_sq_pos = [
            v["r"] ** 2 / v["elapsed time"] for v in well_events.values() if v["r"] <= 0
        ]
        diff_neg = np.sqrt(np.percentile((diff_sq_neg), 95))
        diff_pos = np.sqrt(np.percentile((diff_sq_pos), 95))
        a.plot(np.sqrt(np.arange(18000)) * diff_pos, np.arange(0, 18000), "k:")
        a.plot(-np.sqrt(np.arange(18000)) * diff_neg, np.arange(0, 18000), "k:")
        a.text(480, 17000, f"D = {diff_pos:.1f} m$^2$/s", ha="right")
        a.text(-480, 17000, f"D = {diff_neg:.1f} m$^2$/s", ha="left")
    a.set_xlim([-500, 500])
    a.set_ylim([18000, 0])
    for a in [ax[0, 0], ax[0, 1], ax[0, 2]]:
        a.set_yticklabels([0, 1, 2, 3, 4, 5])
    fig.text(0.05, 0.5, "elapsed time (hrs)", rotation=90, fontsize=15, va="center")
    fig.text(0.5, 0.05, "distance from well (m)", rotation=0, fontsize=15, ha="center")
    fig.savefig(f"{tag}_r_vs_t.png", transparent=True)

    fig, ax = plt.subplots(3, 3, figsize=[12, 12], sharex=True, sharey=True)
    for i_well, well in enumerate(
        [
            "A-E13-L",
            "A-D13-L",
            "A-C13-L",
            "A-H13-L",
            "A-G13-L",
            "A-F13-L",
            "A-B13-L",
            "A-A13-L",
            "A-13-L",
        ]
    ):
        i_1 = i_well // 3
        i_2 = i_well % 3
        a = ax[i_1, i_2]
        well_events = {
            k: v
            for k, v in stage_events.items()
            if v["well"] == well and v["hrz distance from perf center"] < 500
        }
        a.plot(
            [v["elapsed time"] for v in well_events.values()],
            np.array([v["z"] for v in well_events.values()]),
            ".",
            color="0.2",
            alpha=0.1,
            zorder=3,
        )
        xx, yy = np.mgrid[0:18000:200j, -500:500:200j]
        positions = np.vstack([xx.ravel(), yy.ravel()])
        values_A = np.vstack(
            [
                [v["elapsed time"] for v in well_events.values()],
                np.array([v["z"] for v in well_events.values()]),
            ]
        )
        kernel_A = st.gaussian_kde(values_A)
        contour_A = np.reshape(kernel_A(positions).T, xx.shape)
        positions = np.vstack([xx.ravel(), yy.ravel()])
        cf_set_A = a.contourf(
            xx,
            yy,
            contour_A,
            cmap=make_simple_gradient_from_white_cmap(well_colors[well]),
            zorder=-10,
            alpha=0.9,
        )
        a.text(1000, 400, well)
        a.plot([0, 18000], [0, 0], "k--", zorder=-2)
        a.set_xticks(np.arange(0, 18001, 3600))
    a.set_ylim([-500, 500])
    a.set_xlim([0, 18000])
    for a in [ax[0, 2], ax[1, 2], ax[2, 2]]:
        a.set_xticklabels([0, 1, 2, 3, 4, 5])
    fig.text(
        0.5, 0.05, "elevation above well (m)", rotation=0, fontsize=15, ha="center"
    )
    fig.text(0.05, 0.5, "elapsed time (hrs)", rotation=90, fontsize=15, va="center")
    fig.savefig(f"{tag}_z_vs_t.png", transparent=True)
