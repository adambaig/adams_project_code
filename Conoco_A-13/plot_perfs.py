import numpy as np
import matplotlib.pyplot as plt

from pfi_qi.rotations import rotate_from_cardinal
from generalPlots import gray_background_with_grid

from read_inputs import read_wells, read_perfs

PI = np.pi
D2R = PI / 180.0
angle = -20.1 * D2R
center = {"easting": 566690.7491049999, "northing": 6283335.8436}

imaged_perfs, actual_perfs = read_perfs()

wells = read_wells()
wells = rotate_from_cardinal(wells, angle, center)
imaged_perfs = rotate_from_cardinal(imaged_perfs, angle, center)
actual_perfs = rotate_from_cardinal(actual_perfs, angle, center)

fig_plan, ax_plan = plt.subplots(figsize=[4, 8])
ax_plan.set_aspect("equal")
(imaged,) = ax_plan.plot(
    [v["perp to trend"] for v in imaged_perfs.values()],
    [v["along trend"] for v in imaged_perfs.values()],
    ".",
    color="royalblue",
    zorder=3,
)
(actual,) = ax_plan.plot(
    [v["perp to trend"] for v in actual_perfs.values()],
    [v["along trend"] for v in actual_perfs.values()],
    ".",
    color="firebrick",
    zorder=3,
)
for well in wells.values():
    ax_plan.plot(well["perp to trend"], well["along trend"], color="0.2", zorder=-3)

ax_plan = gray_background_with_grid(ax_plan)
fig_plan.savefig("perfs_plan.png")

fig_depth, ax_depth = plt.subplots(figsize=[4, 4])
ax_depth.set_aspect("equal")
ax_depth.plot(
    [v["perp to trend"] for v in imaged_perfs.values()],
    [v["elevation"] for v in imaged_perfs.values()],
    ".",
    color="royalblue",
    zorder=3,
)
ax_depth.plot(
    [v["perp to trend"] for v in actual_perfs.values()],
    [v["elevation"] for v in actual_perfs.values()],
    ".",
    color="firebrick",
    zorder=3,
)
for well in wells.values():
    ax_depth.plot(well["perp to trend"], well["elevation"], color="0.2", zorder=-3)

ax_depth.set_ylim(-1500, -500)
ax_depth = gray_background_with_grid(ax_depth)
fig_depth.savefig("perfs_depth.png")

fig_legend, ax_legend = plt.subplots()
ax_legend.legend([actual, imaged], ["measured perfs", "imaged perfs"])
fig_legend.savefig("legend.png")

n_stages = 0
for well in wells.values():
    n_stages += len([k for k in well.keys() if "Stage" in k])
    print(n_stages)
