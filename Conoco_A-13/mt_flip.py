import json

import matplotlib.pyplot as plt
import numpy as np
from sklearn.cluster import AgglomerativeClustering

from sms_moment_tensor.MT_math import (
    pt_to_sdr,
    sdr_to_mt,
    decompose_MT,
    mt_to_sdr,
    clvd_iso_dc,
    trend_plunge_to_unit_vector,
)

from plotting_stuff import plot_strike_dip_rake_rosettes, plot_strain_stereonets
from read_inputs import read_catalog, write_catalog

PI = np.pi
D2R = PI / 180.0

out_file = "Catalog//ConocoPhillips_A13_PFI_FlippedMT_round_2.csv"
events = read_catalog()


SH_max = 70


def scaled_mt(event, mt_key):
    moment = 10 ** (1.5 * event["moment magnitude"] + 9)
    return np.sqrt(2) * event[mt_key] * moment / np.linalg.norm(event[mt_key])


refence_vector = trend_plunge_to_unit_vector(SH_max, 0)


out_events = {}
for id, event in events.items():
    decomp = decompose_MT(event["dc MT"])
    if (
        decomp["b_plunge"] > 45 or decomp["p_plunge"] < 45
    ):  # strike slip ish or thrust-ish
        p_unit_vector = trend_plunge_to_unit_vector(
            decomp["p_trend"], decomp["p_plunge"]
        )
        if np.dot(refence_vector, p_unit_vector) < 0:
            event["dc flip"] = -event["dc MT"]
            event["gen flip"] = -event["gen MT"]
            event["flipped"] = True
        else:
            event["dc flip"] = event["dc MT"]
            event["gen flip"] = event["gen MT"]
            event["flipped"] = False
    elif decomp["p_plunge"] > 45:
        b_unit_vector = trend_plunge_to_unit_vector(
            decomp["b_trend"], decomp["b_plunge"]
        )
        if np.dot(refence_vector, b_unit_vector) < 0:
            event["dc flip"] = -event["dc MT"]
            event["gen flip"] = -event["gen MT"]
            event["flipped"] = True
        else:
            event["dc flip"] = event["dc MT"]
            event["gen flip"] = event["gen MT"]
            event["flipped"] = False
    event["dc MT"] = scaled_mt(event, "dc flip")
    event["gen MT"] = scaled_mt(event, "gen flip")
    decomp_after_flip = decompose_MT(event["dc flip"])
    strike, dip, rake, aux_strike, aux_dip, aux_rake = mt_to_sdr(event["dc flip"])
    clvd, iso, dc = clvd_iso_dc(event["gen MT"])
    out_events[id] = {
        **event,
        **decomp_after_flip,
        "clvd": clvd,
        "iso": iso,
        "dc": dc,
        "strike": strike,
        "dip": dip,
        "rake": rake,
        "aux strike": aux_strike,
        "aux dip": aux_dip,
        "aux rake": aux_rake,
        "gen MT norm": event["gen MT"] / np.linalg.norm(event["gen MT"]),
        "dc MT norm": event["dc MT"] / np.linalg.norm(event["dc MT"]),
    }


out = write_catalog(out_events, out_file)


# flipped_strain = plot_strain_stereonets(best_events, mt_key="dc flip", color="firebrick")
# flipped_rosette = plot_strike_dip_rake_rosettes(events, mt_key="dc MT", color="firebrick")
#
#
# strain_axes.savefig("unflipped.png")
# flipped_strain.savefig("flipped.png")
