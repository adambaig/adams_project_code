from collections import OrderedDict
from datetime import datetime
from itertools import accumulate
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.dates import DateFormatter
from matplotlib.colors import Normalize
import operator

from pfi_qi.rotations import rotate_from_cardinal

from read_inputs import read_flipped_catalog


PI = np.pi
D2R = PI / 180.0
center = {"easting": 566690.7491049999, "northing": 6283335.8436}
angle = -20.1 * D2R
magnitude_of_completeness = -1.3

events = {
    k: v
    for k, v in rotate_from_cardinal(read_flipped_catalog(), angle, center).items()
    if v["perp to trend"] > -1500
    and v["perp to trend"] < 1700
    and v["depth"] < 1450
    and v["moment magnitude"] > magnitude_of_completeness
}


tab10 = cm.get_cmap("tab10")
well_colors = {
    "A-13-L": tab10(0.05),
    "A-A13-L": tab10(0.15),
    "A-B13-L": tab10(0.25),
    "A-C13-L": tab10(0.35),
    "A-D13-L": tab10(0.45),
    "A-E13-L": tab10(0.55),
    "A-F13-L": tab10(0.65),
    "A-G13-L": tab10(0.85),
    "A-H13-L": tab10(0.95),
}

f3, a3 = plt.subplots(3, figsize=[12, 8], sharey=True)
well_order = [
    "A-E13-L",
    "A-D13-L",
    "A-C13-L",
    "A-H13-L",
    "A-G13-L",
    "A-F13-L",
    "A-B13-L",
    "A-A13-L",
    "A-13-L",
]
patches = [[], [], []]
for i_well, well in enumerate(well_order):
    well_events = {k: v for k, v in events.items() if v["well"] == well}
    i_1 = i_well // 3
    i_2 = i_well % 3
    a = a3[i_1]
    sorted_events = OrderedDict(
        sorted(well_events.items(), key=lambda x: operator.getitem(x[1], "MST"))
    )

    (p,) = a.plot(
        [
            datetime.strptime(v["MST"], "%Y-%m-%d %H:%M:%S.%f")
            for v in sorted_events.values()
        ],
        list(accumulate([v["moment"] * 1e-12 for v in sorted_events.values()])),
        color=well_colors[well],
    )
    patches[i_1].append(p)

date_format = DateFormatter("%b %d")
for ii in range(3):
    x1, x2 = a3[ii].get_xlim()
    a3[ii].set_xlim(x1, x1 + 45)
    a3[ii].legend(patches[ii], well_order[ii * 3 : ii * 3 + 3], loc=2)
    a3[ii].xaxis.set_major_formatter(date_format)

f3.text(
    0.05,
    0.5,
    "cumulative moment release ($10^{12}$ N$\cdot$m)",
    va="center",
    rotation=90,
)

f3.savefig("cumulative_moment_release.png")
