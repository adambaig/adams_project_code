from scipy.spatial import ConvexHull
from read_inputs import read_stations


stations = read_stations()

points = [[v["easting"], v["northing"]] for v in stations.values()]


area = ConvexHull(points).volume  # I know this looks wrong


n_geophones = len(stations) * 19 * 6
1e6 * n_geophones / area
