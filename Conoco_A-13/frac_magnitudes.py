from collections import OrderedDict
from datetime import datetime
from itertools import accumulate
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.dates import DateFormatter
from matplotlib.colors import Normalize
import operator

from pfi_qi.rotations import rotate_from_cardinal

from read_inputs import read_flipped_catalog


PI = np.pi
D2R = PI / 180.0
center = {"easting": 566690.7491049999, "northing": 6283335.8436}
angle = -20.1 * D2R
magnitude_of_completeness = -1.3

all_frac_events = {
    k: v
    for k, v in rotate_from_cardinal(read_flipped_catalog(), angle, center).items()
    if v["perp to trend"] > -1500 and v["perp to trend"] < 1700 and v["depth"] < 1450
}

tab10 = cm.get_cmap("tab10")

fig, ax = plt.subplots()

magbins = np.arange(-2.4, 1.5, 0.1)

ax.hist(
    [v["moment magnitude"] for v in all_frac_events.values()],
    bins=magbins,
    edgecolor="0.8",
    facecolor=tab10(0.15),
)

ax.set_yscale("log")
ax.set_xlabel("moment magnitude")
ax.set_ylabel("Number of events")
ax.set_title("A-13-L magnitude distribution")
fig.savefig("A13L_mags.png")
len(
    [
        v["moment magnitude"]
        for v in all_frac_events.values()
        if v["moment magnitude"] > -0.5
    ]
)
