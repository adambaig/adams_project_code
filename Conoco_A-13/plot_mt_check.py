import numpy as np
import matplotlib.pyplot as plt
from obspy.imaging.mopad_wrapper import beach

from sms_moment_tensor.MT_math import mt_matrix_to_vector, mt_vector_to_matrix


def gray_background_with_grid(axis, grid_spacing=250):
    axis.set_facecolor("lightgrey")
    axis.set_aspect("equal")
    x1, x2 = axis.get_xlim()
    y1, y2 = axis.get_ylim()
    grid_spacing2 = 2 * grid_spacing
    x_minor_ticks = np.arange(
        np.floor(x1 / grid_spacing) * grid_spacing,
        np.ceil(x2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    x_major_ticks = np.arange(
        np.floor(x1 / grid_spacing2) * grid_spacing2,
        np.ceil(x2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    y_minor_ticks = np.arange(
        np.floor(y1 / grid_spacing) * grid_spacing,
        np.ceil(y2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    y_major_ticks = np.arange(
        np.floor(y1 / grid_spacing2) * grid_spacing2,
        np.ceil(y2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    axis.set_xticks(x_minor_ticks, minor=True)
    axis.set_yticks(y_minor_ticks, minor=True)
    axis.set_xticks(x_major_ticks, minor=False)
    axis.set_yticks(x_major_ticks, minor=False)

    axis.grid(which="both")
    axis.grid(which="minor", alpha=1)
    axis.set_xlim([x1, x2])
    axis.set_ylim([y1, y2])
    axis.set_yticklabels([])
    axis.set_xticklabels([])
    axis.tick_params(which="both", color=(0, 0, 0, 0))
    axis.set_axisbelow(True)

    return axis


f = open("relocCatalog.csv")
line = f.readline()
lines = f.readlines()
f.close()

events = {}
for line in lines:
    lspl = line.split(",")
    id = lspl[0]
    events[id] = {
        "datetime": lspl[1],
        "easting": float(lspl[2]),
        "northing": float(lspl[3]),
        "elevation": -float(lspl[4]),
        "snr": float(lspl[5]),
        "raw amp": float(lspl[6]),
        "reloc amp": float(lspl[7]),
        "gen MT": mt_vector_to_matrix([float(v) for v in lspl[8:14]]),
        "gen R": float(lspl[14]),
        "gen CN": float(lspl[15]),
        "dc MT": mt_vector_to_matrix([float(v) for v in lspl[16:22]]),
        "dc R": float(lspl[22]),
        "dc CN": float(lspl[23]),
    }


eastings = np.array([v["easting"] for v in events.values()])
northings = np.array([v["northing"] for v in events.values()])
fig, ax = plt.subplots(figsize=[12, 12])

for event in events.values():
    beachball = beach(
        mt_matrix_to_vector(event["dc MT"]) / np.linalg.norm(event["dc MT"]),
        xy=(event["easting"], event["northing"]),
        width=100,
    )
    ax.add_collection(beachball)

min_east, max_east = min(eastings), max(eastings)
min_north, max_north = min(northings), max(northings)

east_range = max_east - min_east
north_range = max_north - min_north


ax.set_xlim(min_east - 0.05 * east_range, max_east + 0.05 * east_range)
ax.set_ylim(min_north - 0.05 * north_range, max_north + 0.05 * north_range)
ax.set_aspect("equal")
ax = gray_background_with_grid(ax)

fig.savefig("normalized_MTs.png")
