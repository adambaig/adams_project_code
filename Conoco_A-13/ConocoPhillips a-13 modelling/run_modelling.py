from pfi_fs.simulate_events import generate_1Csuperstation_central3C_waveforms


for directory_name in [
    "62_stations_19",
]:
    nsuper, _, nstations = directory_name.split("_")
    sim_config = "simulation" + nstations + ".cfg"
    source_config = "source.cfg"
    station_file = "stations.csv"
    generate_1Csuperstation_central3C_waveforms(
        source_config,
        sim_config,
        "geology.cfg",
        station_file,
        directory_name=directory_name,
        plot_out=True,
    )
