import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
import pyproj as pr


latlon_proj = pr.Proj(init="epsg:4236")
out_proj = pr.Proj(init="epsg:26710")

f = open("wells_lat_lon_elev.csv")
head = f.readline()
lines = f.readlines()
f.close()

wells = {}
for i_line, line in enumerate(lines):
    coords = line.split()
    n_coords = len(coords)
    lat, lon, elev = np.zeros(n_coords), np.zeros(n_coords), np.zeros(n_coords)
    for i_coord, coord in enumerate(coords):
        lon[i_coord], lat[i_coord], elev[i_coord] = [
            float(s.replace('"', "")) for s in coord.split(",")
        ]
    easting, northing = pr.transform(latlon_proj, out_proj, lon, lat)
    f = open(f"well_{i_line+1}.csv", "w")
    f.write("easting (m), northing (m), elev (m)\n")
    wells[i_line] = {"easting": easting, "northing": northing, "elevation": elev}
    for i_coord in range(n_coords):
        f.write(
            f"{easting[i_coord]:.3f}, {northing[i_coord]:.3f}, {elev[i_coord]:.3f}\n"
        )
    f.close()


f = open("possible_patch_locations.csv")
lines = f.readlines()
f.close()
n_stations = len(lines[::10])


f = open("stations_relative.csv", "w")
f.write("easting (m), northing (m), elev (m)\n")
easting, northing = np.zeros(n_stations), np.zeros(n_stations)
station_elev = 930
for i_station, line in enumerate(lines[::10]):
    lon[i_station], lat[i_station] = [
        float(s.replace('"', "")) for s in line.split(",")
    ]
easting, northing = pr.transform(latlon_proj, out_proj, lon, lat)

east_center = np.average(easting)
north_center = np.average(northing)

stations = {}
for i_station in range(n_stations):
    stations[i_station] = {
        "easting": easting[i_station] - east_center,
        "northing": northing[i_station] - north_center,
        "elevation": station_elev,
    }
    f.write(
        f"{i_station},{stations[i_station]['northing']:.3f}, {stations[i_station]['easting']:.3f}, {station_elev:.3f}\n"
    )
f.close()

fig, ax = plt.subplots()
ax.set_aspect("equal")
[ax.plot(well["easting"], well["northing"], "0.25", lw=2) for well in wells.values()]
[
    ax.plot(
        station["easting"],
        station["northing"],
        "h",
        color="forestgreen",
        markeredgecolor="k",
    )
    for station in stations.values()
]
plt.show()
