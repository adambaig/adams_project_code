import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt
import numpy as np

f = open("stations_36.csv")
head = f.readline()
lines = f.readlines()
f.close()

stations = {}
for line in lines:
    lspl = line.split(",")
    if lspl[-1] != "n\n":
        station_id = lspl[0]
        stations[station_id] = {
            "easting": float(lspl[2]),
            "northing": float(lspl[1]),
            "elevation": float(lspl[3]),
        }

wells = {}
well_files = glob.glob("well_*.csv")
for well_file in well_files:
    f = open(well_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    well_id = well_file.split("_")[-1].split(".")[0]
    wells[well_id] = {
        "easting": [float(l.split(",")[0]) for l in lines],
        "northing": [float(l.split(",")[1]) for l in lines],
        "elevation": [float(l.split(",")[2]) for l in lines],
    }


fig, ax = plt.subplots()
ax.set_aspect("equal")
for station_id, station in stations.items():
    ax.plot(station["easting"], station["northing"], "wo", markeredgecolor="k")
    ax.text(station["easting"], station["northing"] - 100, station_id)

for well in wells.values():
    ax.plot(well["easting"], well["northing"], "0.2", lw=2, zorder=-2)


plt.show()
