# all units in SI unless otherwise noted.
# simulations are in a rotated coordinate frame, x --> across well, y --> along well

file_prefix = "CoP_P_only_"

scenarios = [
    #    {"geophones_per_string": 6, "nodes_per_superstation": 13, "mn_stations": [3, 5]},
    #    {"geophones_per_string": 6, "nodes_per_superstation": 13, "mn_stations": [5, 6]},# use this one
    #    {"geophones_per_string": 6, "nodes_per_superstation": 19, "station_file": "stations_relative.csv"},
    {
        "geophones_per_string": 6,
        "nodes_per_superstation": 19,
        "station_file": "stations.csv",
    },
    # {"geophones_per_string": 6, "nodes_per_superstation": 31, "mn_stations": [6, 10]},
    #    {"geophones_per_string": 6, "nodes_per_superstation": 19, "mn_stations": [10, 12]},
]

do_steps = {
    "initial plotting": 1,
    "detectability curve modelling": 0,
    "detectability curve imaging": 0,
    "curve fitting": 0,
    "detectability map modelling": 0,
    "detectability map imaging": 0,
    "detectability plotting": 0,
    "error simulation": 0,
    "error imaging": 0,
    "error reporting": 0,
}

geology_config = "geology.cfg"

n_threads = 6

well_dimensions = {
    "tvd": 2210,
    "pad_width": 500,
    "well_length": 1500,
    "stage_half_length": 500,
    "well_head_elevation": 930,
}

simulation = {
    "sample_rate": 0.002,
    "t_shift": 0.5,
    "trace_length": 5,
    "bandpass_freqs": [20, 50],
    "noise_ppsd": "PPSD_05pct_TX.EF04.~.HHZ_20200114T000000_20200128T235959.csv",
    "semblance_weighted_stack_factor": 10,
}

source = {
    "stress_drop": 3.0e5,
    "mechanism": [1, -1, 0, 0, 0, 0],
    "min_magnitude": -3,
    "max_magnitude": 0,
    "magnitude_inc": 0.1,
}

imaging_parameters = {
    "resample_frequency": 125,
    "input_delta_z": 10,
    "input_starting_depth": -well_dimensions["well_head_elevation"],
    "max_hrz_distance": 10000,
    "hrz_spacing_tt_grid": 100,
    "static_sigma": 0.003,
    "n_image_grid": [50, 50, 50],
    "d_image_grid": [4, 4, 4],
    "pre_padding_samples": 125,
}

display_parameters = {
    "SI_units": True,  # Note, display only! All computation in SI regardless of value
    "grid_xy": [400, 400],
    "n_xy": [4, 6],
    "well_files": [
        "well_1.csv",
        "well_2.csv",
        "well_3.csv",
        "well_4.csv",
        "well_5.csv",
        "well_6.csv",
        "well_7.csv",
        "well_8.csv",
        "well_9.csv",
    ],
    "well_trend_deg": -19,
    "colors": ["firebrick", "royalblue", "forestgreen", "darkgoldenrod"],
    "undetectability": -2.4,
    "large_mag": -0.8,
    "waveform_plots": True,
    "M_colorbar_min": -1.7,
    "M_colorbar_max": -1.3,
    "elevation_range": [1100, -1500],  # in display units (i.e. ft or m)
    "velocity_range": [0, 6500],  # ditto
    "well_center": {"easting": 566347.9, "northing": 6282889.7},
}

location_error_simulations = {"n_realizations": 20, "M_simulation": 0}
