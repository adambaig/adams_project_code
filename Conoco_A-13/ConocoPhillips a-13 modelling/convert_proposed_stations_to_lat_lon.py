import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
import pyproj as pr
import utm

latlon_proj = pr.Proj(init="epsg:4236")
in_proj = pr.Proj(init="epsg:26710")

for station_file in ["stations_41.csv", "stations_30.csv", "stations_24.csv"]:
    f = open(station_file)
    head = f.readline()
    lines = f.readlines()
    f.close()

    g = open(station_file.replace(".", "_lat_lon."), "w")
    g.write("Latitude, Longitude\n")
    for line in lines:
        lspl = line.split(",")
        station_id = lspl[0]
        northing, easting = [float(s) for s in lspl[1:3]]

        lat, lon = utm.to_latlon(easting, northing, 10, "N")
        g.write(f"{lat:.6f}, {lon:.6f}\n")

    g.close()
