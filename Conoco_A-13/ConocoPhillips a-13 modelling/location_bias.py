import glob
import matplotlib.pyplot as plt
import numpy as np
import os

errors = {}

error_catalogs = glob.glob("error_catalogs/*.csv")

for cat in error_catalogs:
    f = open(cat)
    head = f.readline()
    lines = f.readlines()
    f.close()
    basename = os.path.basename(cat)
    n_super = basename.split("_")[2]
    n_dy = basename.split(".")[0][-1]
    if n_super not in errors:
        errors[n_super] = {}
    errors[n_super][n_dy] = {
        "vrt_error": [float(line.split(",")[-1]) for line in lines],
        "z": [float(line.split(",")[-4]) for line in lines],
    }


fig, ax = plt.subplots(nrows=2, figsize=[12, 8], sharex=True, sharey=True)

parts_41 = ax[0].violinplot(
    [s["z"] for s in errors["41"].values()],
    [float(s) for s in errors["41"].keys()],
    showmedians=True,
)
parts_36 = ax[1].violinplot(
    [s["z"] for s in errors["36"].values()],
    [float(s) for s in errors["36"].keys()],
    showmedians=True,
)
for a in ax:
    a.plot([0, 5], [-1280, -1280], "k-.")
    a.set_ylabel("relocated elevation (m)")

for pc in parts_36["bodies"]:
    pc.set_facecolor("springgreen")
    pc.set_edgecolor("forestgreen")
for pc in parts_41["bodies"]:
    pc.set_facecolor("seagreen")
    pc.set_edgecolor("forestgreen")

ax[1].set_xticklabels(np.arange(-400, 2100, 400))
ax[1].set_xlabel("distance from center to toe (m)")
ax[0].set_title("41 stations")
ax[1].set_title("36 stations")
f2, a2 = plt.subplots(nrows=2, figsize=[12, 8], sharex=True, sharey=True)
error_41 = a2[0].violinplot(
    [s["vrt_error"] for s in errors["41"].values()],
    [float(s) for s in errors["41"].keys()],
    showmedians=True,
)
error_36 = a2[1].violinplot(
    [s["vrt_error"] for s in errors["36"].values()],
    [float(s) for s in errors["36"].keys()],
    showmedians=True,
)
for a in a2:
    a.set_ylabel("vertical error (m)")
fig.savefig("error_catalogs//relocated_z.png")
a2[1].set_xticklabels(np.arange(-400, 2100, 400))
a2[1].set_xlabel("distance from center to toe (m)")
for pc in error_36["bodies"]:
    pc.set_facecolor("crimson")
    pc.set_edgecolor("firebrick")
for pc in error_41["bodies"]:
    pc.set_facecolor("tomato")
    pc.set_edgecolor("firebrick")
a2[0].set_title("41 stations")
a2[1].set_title("36 stations")
f2.savefig("error_catalogs//error_z.png")
