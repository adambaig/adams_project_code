import json

import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np
from sklearn.cluster import AgglomerativeClustering
import mplstereonet as mpls

from sms_moment_tensor.MT_math import (
    pt_to_sdr,
    sdr_to_mt,
    decompose_MT,
    mt_to_sdr,
    clvd_iso_dc,
    trend_plunge_to_unit_vector,
)
from pfi_qi.rotations import rotate_from_cardinal

from read_inputs import read_catalog, write_catalog

PI = np.pi
D2R = PI / 180.0


def make_simple_gradient_from_white_cmap(end_color):
    red, green, blue = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, 1.0, 1.0), (1, red, red)],
        "green": [(0.0, 1.0, 1.0), (1, green, green)],
        "blue": [(0.0, 1.0, 1.0), (1, blue, blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


def norm_MT(mt):
    return mt / np.linalg.norm(mt)


MT_CONFIDENCE = 0.99

out_file = "Catalog//ConocoPhillips_A13_PFI_FlippedMT_round_3.csv"
events = read_catalog()
center = {"easting": 566690.7491049999, "northing": 6283335.8436}
angle = -20.1 * D2R
magnitude_of_completeness = -1.3

# crude event filtering
all_frac_events = {
    k: v
    for k, v in rotate_from_cardinal(events, angle, center).items()
    if v["perp to trend"] > -1500 and v["perp to trend"] < 1700 and v["depth"] < 1450
}


good_frac_mts = {k: v for k, v in events.items() if v["mt confidence"] > MT_CONFIDENCE}

n_mt = len(good_frac_mts)
all_normalized_mts = np.zeros([n_mt, 3, 3])
for i_mt, mt in enumerate(good_frac_mts.values()):
    all_normalized_mts[i_mt, :, :] = mt["dc MT"] / np.linalg.norm(mt["dc MT"])

dot_product_pairs = np.tensordot(
    all_normalized_mts, all_normalized_mts, axes=([1, 2], [1, 2])
)

distance_mat = abs(1 - abs(dot_product_pairs))


# can play with number of clusters
n_clusters = 8
clustering = AgglomerativeClustering(
    n_clusters=n_clusters,
    compute_distances=True,
    affinity="precomputed",
    linkage="complete",
).fit(distance_mat)
labels = clustering.labels_
for i_mt, (id, mt) in enumerate(good_frac_mts.items()):
    mt["mt cluster"] = labels[i_mt]
colors = [
    "firebrick",
    "forestgreen",
    "darkgoldenrod",
    "steelblue",
    "indigo",
    "darkorange",
    "turquoise",
    "black",
]

# change the elemvents of flip to -1 to flip the cluster,
# also 8 elements corresponds to n_clusters =8

# flip = [1, 1, -1, -1, -1, -1, 1, 1]
flip = [1, 1, 1, 1, 1, 1, 1, 1]
for event in events.values():
    event["is flipped"] = False
for i_cluster in range(n_clusters):
    mt_cluster = {
        k: v for k, v in good_frac_mts.items() if v["mt cluster"] == i_cluster
    }

    custom_cmap = make_simple_gradient_from_white_cmap(colors[i_cluster])
    mt_avg = flip[i_cluster] * sum(
        [v["dc MT"] / np.linalg.norm(v["dc MT"]) for k, v in mt_cluster.items()]
    )

    p_trend, b_trend, t_trend = [], [], []
    p_plunge, b_plunge, t_plunge = [], [], []

    for id, mt in mt_cluster.items():
        mt["old dc MT"] = mt["dc MT"]
        mt["old gen MT"] = mt["gen MT"]
        if np.tensordot(mt_avg, mt["dc MT"]) < 0:
            mt["dc MT"] = -mt["dc MT"]
            mt["gen MT"] = -mt["gen MT"]
        decomp = decompose_MT(mt["dc MT"])
        p_trend.append(decomp["p_trend"])
        b_trend.append(decomp["b_trend"])
        t_trend.append(decomp["t_trend"])
        p_plunge.append(decomp["p_plunge"])
        b_plunge.append(decomp["b_plunge"])
        t_plunge.append(decomp["t_plunge"])
    f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = a3[0].density_contourf(
        p_plunge, p_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_b = a3[1].density_contourf(
        b_plunge, b_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_t = a3[2].density_contourf(
        t_plunge, t_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    f3.savefig(f"agglomerative_clustering_{i_cluster}.png")

for event in good_frac_mts.values():
    if np.tensordot(event["old dc MT"], event["dc MT"]) < 0:
        event["is flipped"] = True

cluster_mts = [
    sum(
        mt["dc MT"] / np.linalg.norm(mt["dc MT"])
        for mt in good_frac_mts.values()
        if mt["mt cluster"] == ii
    )
    for ii in range(n_clusters)
]

low_q_frac = {k: v for k, v in events.items() if v["mt confidence"] < MT_CONFIDENCE}
for event in low_q_frac.values():
    dot_products = np.zeros(n_clusters)
    for i_cluster in range(n_clusters):
        dot_products[i_cluster] = np.tensordot(
            norm_MT(event["dc MT"]), norm_MT(cluster_mts[i_cluster])
        )
    i_cluster_belongs = np.argmax(abs(dot_products))
    if dot_products[i_cluster_belongs] < 0:
        event["dc MT"] = -event["dc MT"]
        event["gen MT"] = -event["gen MT"]
        event["is flipped"] = True


# make JSON serializable
for event in events.values():
    event["dc MT"] = event["dc MT"].tolist()
    event["gen MT"] = event["gen MT"].tolist()

# still moke JSON serializable
for event in events.values():
    for key_to_pop in [
        "flipped",
        "along trend",
        "perp to trend",
        "mt cluster",
        "old gen MT",
        "old dc MT",
        "strike",
        "dip",
        "rake",
        "aux strike",
        "aux dip",
        "aux rake",
        "p trend",
        "p plunge",
        "b trend",
        "b plunge",
        "t trend",
        "t plunge",
    ]:
        if key_to_pop in event:
            event.pop(key_to_pop)


for event in events.values():
    sdrs = mt_to_sdr(event["dc MT"])
    event["strike"] = sdrs[0]
    event["dip"] = sdrs[1]
    event["rake"] = sdrs[2]
    event["aux strike"] = sdrs[3]
    event["aux dip"] = sdrs[4]
    event["aux rake"] = sdrs[5]


with open("reflipped_events_less_normal_v2.json", "w") as f:
    json.dump(events, f)


# flipped_strain = plot_strain_stereonets(best_events, mt_key="dc flip", color="firebrick")
# flipped_rosette = plot_strike_dip_rake_rosettes(events, mt_key="dc MT", color="firebrick")
#
#
# strain_axes.savefig("unflipped.png")
# flipped_strain.savefig("flipped.png")
