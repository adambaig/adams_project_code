from collections import defaultdict
from glob import glob
import json
import os

import matplotlib.pyplot as plt
import numpy as np
from obspy import read, UTCDateTime

from realtime_source_parameters.athena_query_helper import (
    get_single_event_preferred_origin,
)
from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)
from nmxseis.interact.athena import AthenaClient
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase

TEST_DATA_FBK_STATION_XML = os.path.join("Braskem", "FBK_Full.xml")
TEST_DATA_FBK_CONFIG = os.path.join("Braskem", "fbk_config.json")
config = load_config_from_file(TEST_DATA_FBK_CONFIG)
inventory = get_station_inventory_from_file(TEST_DATA_FBK_STATION_XML)
athena_client = AthenaClient(config.get_athena_base_url(), config.get_athena_api_key())
waveform_dir = r"Braskem\GMM_dataset\waveforms"


def get_nsl_from_channel_path(channel_path):
    return NSL(
        net=channel_path["networkCode"],
        sta=channel_path["stationCode"],
        loc=channel_path["locationCode"],
    )


min_buffer, max_buffer = 0.3, 0.8
pick_color = {Phase.P: "cyan", Phase.S: "green"}
failed_jsons = glob(os.path.join("Braskem", "failed_sp_json", "*.json"))


for event_json in failed_jsons:
    event_id = os.path.basename(event_json).split(".")[0].lstrip("0")
    event = get_single_event_preferred_origin(athena_client, event_id)
    origin = event.get_origin()
    arrivals = defaultdict(dict)
    theoreticals = defaultdict(dict)
    seed_name = os.path.join(waveform_dir, f"{event_id}.MSEED")
    waveform_stream = read(seed_name)
    nsls = np.unique([NSL.from_trace(tr) for tr in waveform_stream])
    for arrival in origin.get_arrivals():
        pick = arrival.get_pick()
        nsl = get_nsl_from_channel_path(arrival.get_pick().get_channel_path())
        if nsl in nsls:
            arrivals[nsl][Phase.parse(pick.get_phase())] = UTCDateTime(pick.get_time())
            theoreticals[nsl][Phase.parse(pick.get_phase())] = UTCDateTime(
                pick.get_time() - arrival.get_residual()
            )

    date_str = str(UTCDateTime(origin.get_time())).split("T")[0]
    fig, ax = plt.subplots(figsize=[12, 8])

    ax.set_ylim(-1, len(nsls))
    ax.set_title(f"{event_id} {date_str}")
    ax.set_facecolor("0.1")
    pick_times = [list(arrivals[nsl].values())[0] for nsl in nsls]
    max_pick, min_pick = max(pick_times), min(pick_times)
    taper_fraction = 0.1 / (max_pick - min_pick + max_buffer + min_buffer)
    waveform_stream.rotate("->ZNE", inventory=inventory)
    waveform_stream.trim(
        starttime=UTCDateTime(min_pick - min_buffer),
        endtime=UTCDateTime(max_pick + max_buffer),
    ).taper(taper_fraction, type="cosine").filter(
        "highpass", freq=5, corners=2, zerophase=True
    )

    for station_int, nsl in enumerate(nsls):
        station_name = nsl.__str__()
        stream_3c = waveform_stream.select(station=nsl.sta)
        if len(stream_3c) == 3:
            z_comp = stream_3c.select(channel="??Z")[0].data
            e_comp = stream_3c.select(channel="??E")[0].data
            n_comp = stream_3c.select(channel="??N")[0].data
            norm = 2 * max(
                [
                    max(abs(z_comp)),
                    max(abs(e_comp)),
                    max(abs(n_comp)),
                ]
            )
            time_axis = [
                UTCDateTime(t + min_pick.timestamp - min_buffer)
                for t in stream_3c[0].times()
            ]
            ax.plot(time_axis, station_int + z_comp / norm, color="0.9", alpha=0.8)
            ax.plot(
                time_axis, station_int + e_comp / norm, color="deepskyblue", alpha=0.8
            )
            ax.plot(time_axis, station_int + n_comp / norm, color="orange", alpha=0.8)
            station_picks = {ph: time for ph, time in arrivals[nsl].items()}
            theory_picks = {ph: time for ph, time in theoreticals[nsl].items()}
            for ph, p_time in station_picks.items():
                ax.plot(
                    [p_time, p_time],
                    [station_int - 0.5, station_int + 0.5],
                    color=pick_color[ph],
                )
                ax.plot(
                    [theory_picks[ph], theory_picks[ph]],
                    [station_int - 0.5, station_int + 0.5],
                    ":",
                    color=pick_color[ph],
                )
    ax.set_xlim(min_pick.timestamp - min_buffer, max_pick.timestamp + max_buffer)
    ax.set_yticks(range(len(nsls)))
    ax.set_yticklabels([nsl.__str__() for nsl in nsls])

    xticks = ax.get_xticks()
    ax.set_xticklabels([str(UTCDateTime(t)).split("T")[1][:-6] for t in xticks])
    ax.set_xlabel("UTC time")
    fig.savefig(rf"Braskem\figures\failed_waveform_images\{event_id.zfill(6)}.png")


def plot_rotated_waveforms_and_windows(
    rotated_traces, signal_windows, noise_windows, arrivals
):
    nsls = np.unique([NSL.from_trace(tr) for tr in rotated_traces])
