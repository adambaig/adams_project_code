from obspy import read_inventory, UTCDateTime
from obspy.core.inventory import Inventory, Network, Station, Channel, Site


old_inventory = read_inventory("Mosaic\mosaic_inventory_old.xml")


inv = Inventory(networks=[], source="from scratch")
networks = []
new_station_codes = []
old_network = old_inventory[0]
old_station = old_network[0]
new_channel_props = {
    "GP1": {
        "code": "GPE",
        "azimuth": 90.0,
    },
    "GP2": {
        "code": "GPN",
        "azimuth": 0.0,
    },
    "GPZ": {
        "code": "GPZ",
        "azimuth": 0.0,
    },
}
for old_network in old_inventory:
    stations = []
    networks.append(Network(code=old_network.code, start_date=UTCDateTime(2020, 1, 1)))
    for old_station in old_network:
        channels = []
        for old_channel in old_station:
            channels.append(
                Channel(
                    code=new_channel_props[old_channel.code]["code"],
                    location_code="00",
                    latitude=old_channel.latitude,
                    longitude=old_channel.longitude,
                    elevation=-old_channel.elevation,
                    depth=old_channel.depth,
                    azimuth=new_channel_props[old_channel.code]["azimuth"],
                    dip=old_channel.dip,
                    sample_rate=old_channel.sample_rate,
                    response=old_channel.response,
                )
            )
            new_station_code = old_station.code[2:] + old_channel.location_code
            if new_station_code not in new_station_codes:
                new_station_codes.append(new_station_code)
                stations.append(
                    Station(
                        code=new_station_code,
                        latitude=old_channel.latitude,
                        longitude=old_channel.longitude,
                        elevation=-old_channel.elevation,
                        site=Site(name=new_station_code),
                    )
                )
        for channel in channels:
            stations[-1].channels.append(channel)
    for station in stations:
        networks[-1].stations.append(station)
for network in networks:
    inv.networks.append(network)


inv.write(
    "Mosaic//updated_station_rotated_to_ENZ.xml", format="stationxml", validate=True
)
