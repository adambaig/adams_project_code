import json
from pathlib import Path

import matplotlib.pyplot as plt
from nmxseis.numerics.formulas import calculate_moment_from_stress_drop, moment_to_mw

basedir = Path("Mosaic")
kwargs = {
	"A": {"color": "firebrick", "zorder": 4, "mec": "0.2"},
	"B": {"color": "royalblue", "zorder": 3, "mec": "0.2"},
	"C": {"color": "darkgoldenrod", "zorder": 2, "mec": "0.2"}
}

source_parameters = {}
for event_json in basedir.joinpath("source_params_jsons").glob("*.json"):
	event_id = event_json.stem.split('_')[-1]
	source_parameters[event_id] = json.loads(event_json.read_text())

sp_val = source_parameters.items()

app_stress = {k: sp["apparent_stress"] for k, sp in sp_val}
mags = {k: sp["mw"] for k, sp in sp_val}
moments = {k: sp["moment"] for k, sp in sp_val}
radii = {k: sp["radius"] for k, sp in sp_val}
energy = {k: sp["radiated_energy"] for k, sp in sp_val}
potencies = {k: sp["potency"] for k, sp in sp_val}
a_events = {k for k, sp in sp_val if sp["quality_score"] == 'A'}
b_events = {k for k, sp in sp_val if sp["quality_score"] == 'B'}
c_events = {k for k, sp in sp_val if sp["quality_score"] == 'C'}
fig_rm, ax_rm = plt.subplots()
patches_rm = {}
for quality, event_subset in zip(["A", "B", "C"], [a_events, b_events, c_events]):
	m = [v for k, v in mags.items() if k in event_subset]
	r = [v for k, v in radii.items() if k in event_subset]
	patches_rm[quality], = ax_rm.semilogx(r, m, 'o', **kwargs[quality])
rad_min, rad_max = ax_rm.get_xlim()
ylim = ax_rm.get_ylim()
for ds in [1e4,1e5,1e6,1e7,1e8]:
	mw_min = moment_to_mw(calculate_moment_from_stress_drop(rad_min, ds))
	mw_max = moment_to_mw(calculate_moment_from_stress_drop(rad_max, ds))
	ax_rm.semilogx([rad_min, rad_max],[mw_min, mw_max], 'k',zorder=1)
ax_rm.set_xlim([rad_min, rad_max])
ax_rm.set_ylim(*ylim)
ax_rm.set_xlabel('radius (m)')
ax_rm.set_ylabel('moment magnitude')
ax_rm.set_facecolor('0.95')
ax_rm.legend([patches_rm[c] for c in ['A', 'B','C']], ['A', 'B', 'C'])
fig_rm.savefig(basedir.joinpath('radius_moment.png'))


fig_pe, ax_pe = plt.subplots()
patches_pe = {}
for quality, event_subset in zip(["A", "B", "C"], [a_events, b_events, c_events]):
	p = [v for k, v in potencies.items() if k in event_subset]
	e = [v for k, v in energy.items() if k in event_subset]
	patches_pe[quality], = ax_pe.loglog(p, e, 'o', **kwargs[quality])

ptc_min, ptc_max = ax_pe.get_xlim()
ylim = ax_pe.get_ylim()
for app_str in [1e4,1e5,1e6,1e7,1e8]:
	nrg_min = app_str*ptc_min
	nrg_max = app_str*ptc_max
	ax_pe.loglog([ptc_min, ptc_max], [nrg_min, nrg_max],'k',zorder=1)
ax_pe.set_xlim([ptc_min, ptc_max])
ax_pe.set_ylim(*ylim)
ax_pe.set_xlabel('potency (m$^3$)')
ax_pe.set_ylabel('energy (J)')
ax_pe.set_facecolor('0.95')
ax_pe.legend([patches_pe[c] for c in ['A', 'B','C']], ['A', 'B', 'C'])
fig_pe.savefig(basedir.joinpath('potency_energy.png'))
plt.show()
