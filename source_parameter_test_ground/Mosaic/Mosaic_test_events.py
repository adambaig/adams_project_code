import glob
import json
import os
import obspy
import matplotlib.pyplot as plt
import numpy as np

import pyproj

from realtime_source_parameters.event_origin_processor import EventOriginProcessor
from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)
from realtime_source_parameters.athena_query_helper import get_single_event_origin

from realtime_source_parameters.models.event_wrapper import EventWrapper

from realtime_source_parameters.engine import (
    get_velocity_lookup_and_raytracer,
    rotate_stream_to_raypaths,
    get_station_locations,
    get_windows,
    get_hypocentral_distance,
    get_highpass,
)

from realtime_source_parameters.spectral_measurements import (
    get_signal_and_noise_spectra,
    calculate_mean_spectrum,
    get_useable_bandwidth_for_moment_and_stress_drop,
    find_stress_drop,
    determine_preliminary_corner_and_energy_flux,
    compensate_for_attenuation_ray,
    kappa_correction,
)
from realtime_source_parameters.util import (
    is_arrival_matching_at_the_same_location,
    get_simple_phase,
)


from realtime_source_parameters.estk_helpers import (
    fourier_spectra,
    # motion_filt,
    determine_gaps_and_bandwidth,
    gapcheck,
)


config = load_config_from_file(r"Mosaic\Mosaic_config.json")
epsg_code = config.get_epsg_code()
velocity_model_config = config.get_velocity_model_config()
processing_config = config.get_processing_config()
processor = EventOriginProcessor(None, None, config)
processing_config.get_min_snr()
fourier_spectra_config = config.get_fourier_spectra_config()
event_jsons = glob.glob("Mosaic//event_jsons//*.json")
inventory = get_station_inventory_from_file(
    r"Mosaic\updated_station_rotated_to_ENZ.xml"
)
source_parameters = {}
for event_json_file in event_jsons:
    with open(event_json_file) as f:
        event_json = json.load(f)
    event = EventWrapper.from_event_json(event_json)
    seedfile = os.path.join(
        "Mosaic",
        "event_seeds",
        os.path.basename(event_json_file).replace("json", "mseed"),
    )
    waveforms_stream = obspy.read(seedfile)

    origin = event.get_origin()
    arrivals = origin.get_arrivals()
    origin_time = origin.get_time()
    event_latitude = origin.get_latitude()
    event_longitude = origin.get_longitude()
    event_depth_km = origin.get_depth()

    p_window_pre = processing_config.get_p_window_pre()
    p_window_post = processing_config.get_p_window_post()
    s_window_pre = processing_config.get_s_window_pre()
    s_window_post = processing_config.get_s_window_post()

    reproject = pyproj.Proj("epsg:" + str(epsg_code))
    stations = get_station_locations(arrivals, inventory, reproject)
    easting, northing = reproject(event_longitude, event_latitude, inverse=False)
    event_depth = event_depth_km * 1000.0
    event_location = {"e": easting, "n": northing, "z": -event_depth}
    velocity_lookup, raytracer = get_velocity_lookup_and_raytracer(
        velocity_model_config
    )
    Q = velocity_model_config.get_Q()
    kappa = processing_config.get_kappa()
    lowpass_percent = processing_config.get_lowpass_percent()
    raypaths = {}
    for station_id, station in stations.items():
        station["rock properties"] = velocity_lookup(station)
        station["is_on_surface"] = False
        for phase in ["P", "S"]:
            raypaths[f"{station_id}.{phase}"] = raytracer(
                event_location, station, phase
            )
    event_location["rock properties"] = velocity_lookup(event_location)
    for net_stat_loc, station in stations.items():
        network_code, station_code, location_code = net_stat_loc.split(".")
        station_traces = waveforms_stream.select(
            network=network_code, station=station_code, location=location_code
        )
        station["hypo_distance"] = get_hypocentral_distance(station, event_location)
        station["highpass"] = get_highpass(
            processing_config, origin, station["hypo_distance"]
        )
        station["lowpass"] = (
            (1.0 - lowpass_percent / 100.0) * station_traces[0].stats.sampling_rate / 2
        )

    rotated_traces = rotate_stream_to_raypaths(
        waveforms_stream,
        stations,
        event_location,
        raytracer,
        inventory=inventory,
    )

    noise_windows, signal_windows = {}, {}
    for arrival in arrivals:
        pick = arrival.get_pick()
        channel_path = pick.get_channel_path()
        phase = pick.get_phase()
        net_stat_loc_phase = ".".join(
            [
                *[
                    channel_path[code]
                    for code in ["networkCode", "stationCode", "locationCode"]
                ],
                phase,
            ]
        )
        net_stat_loc = net_stat_loc_phase[:-2]
        (
            signal_windows[net_stat_loc_phase],
            noise_windows[net_stat_loc_phase],
        ) = get_windows(
            arrival, arrivals, raytracer, event_location, stations, processing_config
        )
        stations[net_stat_loc][f"{phase} window"] = signal_windows[net_stat_loc_phase]

    (
        acceleration_spectra,
        acceleration_noise_spectra,
        freqs,
    ) = get_signal_and_noise_spectra(
        rotated_traces,
        signal_windows,
        noise_windows,
        fourier_spectra_config.get_parameter_list(),
    )
    signal_to_noise_ratios = {}
    for net_stat_loc_phase in acceleration_spectra:
        signal_to_noise_ratios[net_stat_loc_phase] = [
            s / n if s > 0 and n > 0 else -999.0
            for s, n in zip(
                acceleration_spectra[net_stat_loc_phase],
                acceleration_noise_spectra[net_stat_loc_phase],
            )
        ]

    signal_to_noise_ratios_by_phase = {
        "P": {k: v for k, v in signal_to_noise_ratios.items() if k[-1] == "P"},
        "S": {k: v for k, v in signal_to_noise_ratios.items() if k[-1] != "P"},
    }
    (
        acceleration_spectra_corrected,
        displacement_spectra_corrected,
    ) = compensate_for_attenuation_ray(
        acceleration_spectra, freqs, raypaths, Q, origin, stations, event_location
    )

    # """
    # delete me
    # """
    # origin_time = origin.get_time()
    # arrivals = origin.get_arrivals()
    # acceleration_spectra_corrected = {"P": {}, "S": {}}
    # displacement_spectra_corrected = {"P": {}, "S": {}}
    # net_stat_loc_phase = "XW.1605.00.V"
    # acc_spectrum = acceleration_spectra[net_stat_loc_phase]
    # network, station, location, phase = net_stat_loc_phase.split(".")
    # channel_path = {
    #     "networkCode": network,
    #     "stationCode": station,
    #     "locationCode": location,
    # }
    # simple_phase = get_simple_phase(phase)
    # net_stat_loc_simplephase = ".".join([network, station, location, simple_phase])
    #
    # traveltime = [
    #     arrival.get_pick().get_time() - origin_time
    #     for arrival in arrivals
    #     if arrival.get_pick().get_phase() == simple_phase
    #     and is_arrival_matching_at_the_same_location(arrival, channel_path)
    # ][0]
    #
    # """
    # """
    acc_spectrum = {"P curve": 0, "S curve": 0, "P sigma": 0, "S sigma": 0}
    disp_spectrum = {"P curve": 0, "S curve": 0, "P sigma": 0, "S sigma": 0}

    for phase in ["P", "S"]:
        (
            acc_spectrum[f"{phase} curve"],
            acc_spectrum[f"{phase} sigma"],
        ) = calculate_mean_spectrum(
            acceleration_spectra_corrected[phase],
            signal_to_noise_ratios_by_phase[phase],
            freqs,
            stations,
            processing_config,
        )
        (
            disp_spectrum[f"{phase} curve"],
            disp_spectrum[f"{phase} sigma"],
        ) = calculate_mean_spectrum(
            displacement_spectra_corrected[phase],
            signal_to_noise_ratios_by_phase[phase],
            freqs,
            stations,
            processing_config,
        )

    freqs_for_moment = {}
    freqs_for_stress_drop = {}
    flat_displacement_samples = {}
    acceleration_spectrum_around_corner = {}
    for curve in ["P curve", "S curve"]:
        phase = curve.split()[0]
        (
            flat_displacement_samples[phase],
            freqs_for_moment[phase],
            acceleration_spectrum_around_corner[phase],
            freqs_for_stress_drop[phase],
        ) = get_useable_bandwidth_for_moment_and_stress_drop(
            acc_spectrum[curve],
            disp_spectrum[curve],
            freqs,
            stations,
            processing_config,
        )
    if (
        flat_displacement_samples["P"] is None
        and flat_displacement_samples["S"] is None
    ):
        continue
    if flat_displacement_samples["P"] is None:
        moment = 10.0 ** np.nanmean(np.log10(flat_displacement_samples["S"]))
    elif flat_displacement_samples["S"] is None:
        moment = 10.0 ** np.nanmean(np.log10(flat_displacement_samples["P"]))
    else:
        moment = 10.0 ** np.nanmean(
            np.log10(
                np.hstack(
                    [flat_displacement_samples["P"], flat_displacement_samples["S"]]
                )
            )
        )
    moment_magnitude = (2.0 / 3.0) * (np.log10(moment) - 9.05)

    stress_drop_by_phase, corner_frequency, source_radius_by_phase = {}, {}, {}
    for phase in ["P", "S"]:
        if acceleration_spectrum_around_corner[phase] is not None:
            curve = f"{phase} curve"
            (
                stress_drop_by_phase[phase],
                source_radius_by_phase[phase],
                corner_frequency[phase],
            ) = find_stress_drop(
                acceleration_spectrum_around_corner[phase],
                freqs_for_stress_drop[phase],
                moment,
                event_location["rock properties"],
                phase,
                processing_config,
                model="Brune",
            )
        else:
            stress_drop_by_phase[phase] = None
            source_radius_by_phase[phase] = None
            corner_frequency[phase] = None

    source_parameters[event.get_event_id()] = {
        "Mw": moment_magnitude,
        "corner frequency P": corner_frequency["P"],
        "corner_frequency S": corner_frequency["S"],
        "Ml": origin.get_ml(),
    }
#
#
# max_gap, min_gap, max_freq = determine_gaps_and_bandwidth(acc_spectrum['P curve'])
# minimum_bandwidth = processing_config.get_minimum_bandwidth_number_of_bins()
# if max_gap > minimum_bandwidth:
#     velocity_cumulative_sum = 0.0
#     displacement_cumulative_sum = 0.0
#     for j in range(min_gap, max_freq_index - 1):
#         unit_conv0 = 2 * np.pi * float(freqs[j])
#         unit_conv1 = 2 * np.pi * float(freqs[j + 1])
#         velocity_cumulative_sum = (
#             velocity_cumulative_sum
#             + ((acc_spec[j + 1] / unit_conv1) ** 2 + (acc_spec[j] / unit_conv0) ** 2)
#             * (float(freqs[j + 1]) - float(freqs[j]))
#             / 2
#         )
#         displacement_cumulative_sum = (
#             displacement_cumulative_sum
#             + ((acc_spec[j + 1] / unit_conv1 ** 2) ** 2 + (acc_spec[j] / unit_conv0 ** 2) ** 2)
#             * (float(freqs[j + 1]) - float(freqs[j]))
#             / 2
#         )
# else:
#     logging.error("no consistent signal detected over %i bins", minimum_bandwidth)
#     return None, None
#
# corner_frequency = np.sqrt(velocity_cumulative_sum / displacement_cumulative_sum) / (2.0 * np.pi)
# band_limited_energy_flux = velocity_cumulative_sum
