import json
from pathlib import Path

from nmxseis.interact.stationxml import ENUStreamMapper
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from nmxseis.numerics.formulas import calculate_scalar_potency
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.ray_modeling.core import rotate_stream_to_ray_enus
from nmxseis.numerics.relocation.core import RelocationResult
from nmxseis.numerics.source_parameters.evaluator import (
    SParamsCascadingEvaluator,
)
from nmxseis.numerics.velocity_model.vm_1d import VelocityModel1D, VMLayer1D
import numpy as np
from nmxseis.util.obspy_tools import remove_response_where_possible
from obspy import read, read_inventory


reproject = EPSGReprojector(32613)
basedir = Path("Mosaic")
ray_trace_dir = Path(r"C:\Users\adambaig\Project\ray_tracing_playground\Mosaic")
inventory = read_inventory(ray_trace_dir.joinpath("updated_station.xml"))
seed_dir = ray_trace_dir.joinpath("event_seeds_12z")
pick_dir = ray_trace_dir.joinpath("picks")
location_json = ray_trace_dir.joinpath(
    "rotated_locations_new_start_loc_raytrace_then_interpolate.json"
)
catalog = json.loads(location_json.read_text())
velocity_model = VelocityModel1D(
    [
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
        for layer in (
            json.loads(ray_trace_dir.joinpath("metric_velocity_model.json").read_text())
        )
    ]
)

sp_config = {
    "qualities": {
        "A": {"min_snr": 3, "min_samples_per_bin": 5, "f_smooth_min": 10},
        "B": {"min_snr": 3, "min_samples_per_bin": 3, "f_smooth_min": 50},
        "C": {"min_snr": 1.5, "min_samples_per_bin": 2, "f_smooth_min": 100},
    },
    "lowpass_nyq_backoff": 0.2,
    "min_d_hypo_m": 0,
    "max_d_hypo_m": 10000,
    "Q": {"P": 60, "S": 60},
    "kappa": 0,
    "f_smooth_max": 2000,
    "f_smooth_n_bins": 41,
    "radius_model": "Madariaga",
    "spectrum_model": "Brune",
}
sp_evaluator = SParamsCascadingEvaluator(velocity_model, reproject, sp_config)

for event_id, event in catalog.items():
    reloc_result = RelocationResult.from_json(event)
    source = reloc_result.origin
    seed_file = list(seed_dir.glob(f"{event_id.zfill(5)}*.mseed"))[0]
    picks_file = pick_dir.joinpath(f"{seed_file.stem}.picks")
    pick_set = PickSet.load(picks_file)
    stream = read(seed_file)
    station_enus = reproject.get_station_enus(inventory)
    esm = ENUStreamMapper(inventory)
    remove_response_where_possible(stream, inventory, output="ACC")
    rotated_stream, _ = rotate_stream_to_ray_enus(
        stream,
        velocity_model.get_ray_arrival_enus(
            source.enu(reproject), station_enus, Phase.P
        ),
        enz_mapper=esm,
    )

    source_parameters, diag = sp_evaluator.evaluate(
        source, rotated_stream, pick_set.to_arrivals_dict()
    )
    print(f"finished event {event_id}")

    potency = calculate_scalar_potency(
        source_parameters.moment,
        velocity_model.look_up_enu(source.enu(reproject)).shear_modulus,
    )
    sp_results = {
        "mw": source_parameters.mw.val,
        "mw_uncertainty": source_parameters.mw.unc,
        "moment": source_parameters.moment.val,
        "moment_uncertainty": source_parameters.moment.unc,
        "potency": source_parameters.scalar_potency.val,
        "potency_uncertainty": source_parameters.scalar_potency.unc,
        "p corner": source_parameters.fc_P.val
        if source_parameters.fc_P is not None
        else None,
        "s corner": source_parameters.fc_S.val
        if source_parameters.fc_S is not None
        else None,
        "quality_score": sp_evaluator.current_quality,
        "lat": source.lat,
        "lon": source.lon,
        "depth": source.depth_m,
        "origin time": source.time.timestamp,
        "enu": source.enu(reproject).tolist(),
        # "ml": event.get_origin().get_ml(),
        "stress_drop": source_parameters.static_stress_drop.stress_drop.val
        if source_parameters.static_stress_drop is not None
        else None,
        "stress_drop_uncertainty": source_parameters.static_stress_drop.stress_drop.unc
        if source_parameters.static_stress_drop is not None
        else None,
        "radiated_energy": source_parameters.radiated_energy.total.val,
        "radiated_energy_uncertainty": source_parameters.radiated_energy.total.unc,
        "radiated_energy_p": source_parameters.radiated_energy.p_energy.val,
        "radiated_energy_s": source_parameters.radiated_energy.s_energy.val,
        "radius": source_parameters.static_stress_drop.radius.val
        if source_parameters.static_stress_drop is not None
        else None,
        "radius_uncertainty": source_parameters.static_stress_drop.radius.unc
        if source_parameters.static_stress_drop is not None
        else None,
        "apparent_stress": source_parameters.apparent_stress.val,
        "apparent_stress_uncertainty": source_parameters.apparent_stress.unc,
        "sw_efficiency": source_parameters.sw_efficiency.val
        if source_parameters.static_stress_drop is not None
        else None,
        "sw_efficiency_uncertainty": source_parameters.sw_efficiency.unc
        if source_parameters.static_stress_drop is not None
        else None,
    }

    # with open(
    #         basedir.joinpath(
    #             "source_params_jsons",
    #             f"sp_results_{event_id.zfill(6)}.json",
    #         ),
    #         "w",
    # ) as f:
    #     json.dump(sp_results, f)
