from pathlib import Path

import matplotlib.pyplot as plt
from nmxseis.model.basic import Window
from nmxseis.model.nslc import NSL
from nmxseis.model.pick_set import PickSet
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
import numpy as np
from scipy.fftpack import fft, fftfreq, rfft, rfftfreq
from obspy import read, read_inventory



base_dir = Path('Mosaic')
seed_dir = base_dir.joinpath(r'C:\Users\adambaig\Project\ray_tracing_playground\Mosaic\event_seeds_12z')
pick_dir = base_dir.joinpath('picks')
stream = read(seed_dir.joinpath('00085_20210923.150241.056999.mseed'))
picks = PickSet.load(pick_dir.joinpath('00085_20210923.150241.056999.picks'))
inventory = read_inventory(base_dir.joinpath('updated_station.xml'))
nsl = NSL.parse('XW.1606.00')

tr_3c = nsl.select_from_stream(stream).copy().rotate('->ZNE', inventory=inventory).detrend()

p_pick = [p.time for p in picks.picks if p.nsl==nsl and p.phase.is_P][0]
s_pick = [p.time for p in picks.picks if p.nsl==nsl and p.phase.is_S][0]

p_s = s_pick - p_pick

window = Window([p_pick - 0.1 * p_s, p_pick + 0.9 * p_s])

inv_3c = nsl.select_from_inv(inventory)
sensitivity = inv_3c.channels[0].response.instrument_sensitivity.value

e_comp = tr_3c.select(channel='*E')[0].trim(starttime=window[0], endtime=window[1])
decon_e_comp = nsl.select_from_stream(stream).copy().remove_response(inventory).rotate('->ZNE', inventory=inventory).detrend().select(channel='*E')[0].trim(starttime=window[0], endtime=window[1])

div_kwags = {"color":'k', "linestyle":'-.'}
decon_kwags ={"color":'k',"lw":0.5}
fig_tr,ax_tr = plt.subplots()
ax_tr.plot(e_comp.times(), e_comp.data/sensitivity,**div_kwags)
ax_tr.plot(decon_e_comp.times(), decon_e_comp.data + 1e-9, **decon_kwags)


fig_sp,ax_sp = plt.subplots()
freqs = fftfreq(e_comp.stats.npts, e_comp.stats.delta)
i_freq = np.where(freqs>0)[0]
d_spec_from_div_trace = abs(rfft(e_comp.data)[i_freq])/sensitivity/(2*np.pi*freqs[i_freq])/e_comp.stats.npts

motion_spec = MotionSpectrum.from_vel_trace(decon_e_comp)
p1, = ax_sp.loglog(freqs[i_freq], d_spec_from_div_trace, **div_kwags)
p2, = ax_sp.loglog(motion_spec.freqs, abs(motion_spec.dis), zorder=-1, lw=0.5, color='r')
smooth_spec = motion_spec.smooth(10,1000,41)
p3, = ax_sp.loglog(smooth_spec.freqs, smooth_spec.dis, **decon_kwags)

# smooth_spec2 = motion_spec.smooth(50,1000,401)
# p4, = ax_sp.loglog(smooth_spec2.freqs, smooth_spec2.dis, lw=0.5,color='b')
ax_sp.legend((p1, p2, p3,
              # p4
              ),
             ['dumb fft and window',
              'motion spec',
              'smooth motion spec 41 samples',
              # 'smooth motion spec 401 samples'
              ])
ax_sp.set_ylabel('displacement spectrum m$\cdot$s')
ax_sp.set_xlabel('frequency (Hz)')
# fig_spv,ax_spv = plt.subplots()
# v_spec_from_div_trace = abs(fft(e_comp.data)[i_freq])/sensitivity/e_comp.stats.npts
#
# motion_spec = MotionSpectrum.from_vel_trace(decon_e_comp)
# p1, = ax_spv.loglog(freqs[i_freq], v_spec_from_div_trace, **div_kwags)
# p2, = ax_spv.loglog(motion_spec.freqs, abs(motion_spec.vel)/e_comp.stats.npts, zorder=-1, lw=0.5, color='r')
# smooth_spec = motion_spec.smooth(10,1000,41)
# p3, = ax_spv.loglog(smooth_spec.freqs, smooth_spec.vel, **decon_kwags)
#
# smooth_spec2 = motion_spec.smooth(10,1000,81)
# p4, = ax_spv.loglog(smooth_spec2.freqs, smooth_spec2.vel, lw=0.5,color='b')
# ax_spv.set_ylabel('velocity spectrum (m/s)$\cdot$s')
# ax_spv.set_xlabel('frequency (Hz)')
# ax_spv.legend((p1, p2, p3, p4), ['dumb fft and window', 'motion spec', 'smooth motion spec 41 samples','smooth motion spec 81 samples'])



plt.show()




