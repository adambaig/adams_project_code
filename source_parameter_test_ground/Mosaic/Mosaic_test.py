import json
import os
import obspy
import matplotlib.pyplot as plt
import numpy as np

import pyproj

from realtime_source_parameters.event_origin_processor import EventOriginProcessor
from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)
from realtime_source_parameters.athena_query_helper import get_single_event_origin

from realtime_source_parameters.models.event_wrapper import EventWrapper

from realtime_source_parameters.engine import (
    get_velocity_lookup_and_raytracer,
    rotate_stream_to_raypaths,
    get_station_locations,
    get_windows,
    get_hypocentral_distance,
    get_highpass,
)

from realtime_source_parameters.spectral_measurements import (
    get_signal_and_noise_spectra,
    calculate_mean_spectrum,
    get_useable_bandwidth_for_moment_and_stress_drop,
    find_stress_drop,
    determine_preliminary_corner_and_energy_flux,
    compensate_for_attenuation_ray,
    kappa_correction,
)
from realtime_source_parameters.util import (
    is_arrival_matching_at_the_same_location,
    get_simple_phase,
)


from realtime_source_parameters.estk_helpers import (
    fourier_spectra,
    # motion_filt,
    determine_gaps_and_bandwidth,
    gapcheck,
)


seedfile = r"Mosaic\Mosaic_Esterhazy_20210924004105000.mseed"
waveforms_stream = obspy.read(seedfile)
inventory = get_station_inventory_from_file(r"Mosaic\mosaic_inventory.xml")
waveforms_stream = waveforms_stream.select(station="16*")
for tr in waveforms_stream:
    if tr.stats.channel == "GPN":
        tr.stats.channel = "GP1"
    if tr.stats.channel == "GPE":
        tr.stats.channel = "GP2"
    station = tr.stats.station
    location = station[-2:]
    tr.stats.station = "BF16"
    tr.stats.location = location

with open(r"Mosaic\Mosaic_Esterhazy_20210924004105000.json") as f:
    event_json = json.load(f)
config = load_config_from_file(r"Mosaic\Mosaic_config.json")
epsg_code = config.get_epsg_code()
event = EventWrapper.from_event_json(event_json)
velocity_model_config = config.get_velocity_model_config()
processing_config = config.get_processing_config()
processor = EventOriginProcessor(None, None, config)
processing_config.get_min_snr()
fourier_spectra_config = config.get_fourier_spectra_config()
# source_parameters = processor.calculate_source_parameters(event, inventory, waveforms_stream)

origin = event.get_origin()
arrivals = origin.get_arrivals()
origin_time = origin.get_time()
event_latitude = origin.get_latitude()
event_longitude = origin.get_longitude()
event_depth_km = origin.get_depth()

p_window_pre = processing_config.get_p_window_pre()
p_window_post = processing_config.get_p_window_post()
s_window_pre = processing_config.get_s_window_pre()
s_window_post = processing_config.get_s_window_post()

reproject = pyproj.Proj("epsg:" + str(epsg_code))
stations = get_station_locations(arrivals, inventory, reproject)
easting, northing = reproject(event_longitude, event_latitude, inverse=False)
event_depth = event_depth_km * 1000.0
event_location = {"e": easting, "n": northing, "z": -event_depth}
velocity_lookup, raytracer = get_velocity_lookup_and_raytracer(velocity_model_config)
rotate_traces = rotate_stream_to_raypaths(
    waveforms_stream, stations, event_location, raytracer, inventory=inventory
)
Q = velocity_model_config.get_Q()

kappa = processing_config.get_kappa()
lowpass_percent = processing_config.get_lowpass_percent()
raypaths = {}
for station_id, station in stations.items():
    station["rock properties"] = velocity_lookup(station)
    station["is_on_surface"] = False
    for phase in ["P", "S"]:
        raypaths[f"{station_id}.{phase}"] = raytracer(event_location, station, phase)
event_location["rock properties"] = velocity_lookup(event_location)
for net_stat_loc, station in stations.items():
    network_code, station_code, location_code = net_stat_loc.split(".")
    station_traces = waveforms_stream.select(
        network=network_code, station=station_code, location=location_code
    )
    station["hypo_distance"] = get_hypocentral_distance(station, event_location)
    station["highpass"] = get_highpass(
        processing_config, origin, station["hypo_distance"]
    )
    station["lowpass"] = (
        (1.0 - lowpass_percent / 100.0) * station_traces[0].stats.sampling_rate / 2
    )

rotated_traces = rotate_stream_to_raypaths(
    waveforms_stream,
    stations,
    event_location,
    raytracer,
    inventory=inventory,
)


noise_windows, signal_windows = {}, {}
for arrival in arrivals:
    pick = arrival.get_pick()
    channel_path = pick.get_channel_path()
    phase = pick.get_phase()
    net_stat_loc_phase = ".".join(
        [
            *[
                channel_path[code]
                for code in ["networkCode", "stationCode", "locationCode"]
            ],
            phase,
        ]
    )
    net_stat_loc = net_stat_loc_phase[:-2]
    (
        signal_windows[net_stat_loc_phase],
        noise_windows[net_stat_loc_phase],
    ) = get_windows(
        arrival, arrivals, raytracer, event_location, stations, processing_config
    )
    stations[net_stat_loc][f"{phase} window"] = signal_windows[net_stat_loc_phase]


(
    acceleration_spectra,
    acceleration_noise_spectra,
    freqs,
) = get_signal_and_noise_spectra(
    rotated_traces,
    signal_windows,
    noise_windows,
    fourier_spectra_config.get_parameter_list(),
)
signal_to_noise_ratios = {}
for net_stat_loc_phase in acceleration_spectra:
    signal_to_noise_ratios[net_stat_loc_phase] = [
        s / n if s > 0 and n > 0 else -999.0
        for s, n in zip(
            acceleration_spectra[net_stat_loc_phase],
            acceleration_noise_spectra[net_stat_loc_phase],
        )
    ]

signal_to_noise_ratios_by_phase = {
    "P": {k: v for k, v in signal_to_noise_ratios.items() if k[-1] == "P"},
    "S": {k: v for k, v in signal_to_noise_ratios.items() if k[-1] != "P"},
}

(
    acceleration_spectra_corrected,
    displacement_spectra_corrected,
) = compensate_for_attenuation_ray(
    acceleration_spectra, freqs, raypaths, Q, origin, stations, event_location
)

acc_spectrum = {"P curve": 0, "S curve": 0, "P sigma": 0, "S sigma": 0}
disp_spectrum = {"P curve": 0, "S curve": 0, "P sigma": 0, "S sigma": 0}

for phase in ["P", "S"]:
    (
        acc_spectrum[f"{phase} curve"],
        acc_spectrum[f"{phase} sigma"],
    ) = calculate_mean_spectrum(
        acceleration_spectra_corrected[phase],
        signal_to_noise_ratios_by_phase[phase],
        freqs,
        stations,
        processing_config,
    )
    (
        disp_spectrum[f"{phase} curve"],
        disp_spectrum[f"{phase} sigma"],
    ) = calculate_mean_spectrum(
        displacement_spectra_corrected[phase],
        signal_to_noise_ratios_by_phase[phase],
        freqs,
        stations,
        processing_config,
    )


plt.loglog(freqs, disp_spectrum["S curve"])

freqs_for_moment = {}
freqs_for_stress_drop = {}
flat_displacement_samples = {}
acceleration_spectrum_around_corner = {}
for curve in ["P curve", "S curve"]:
    phase = curve.split()[0]
    (
        flat_displacement_samples[phase],
        freqs_for_moment[phase],
        acceleration_spectrum_around_corner[phase],
        freqs_for_stress_drop[phase],
    ) = get_useable_bandwidth_for_moment_and_stress_drop(
        acc_spectrum[curve],
        disp_spectrum[curve],
        freqs,
        stations,
        processing_config,
    )


if flat_displacement_samples["P"] is None:
    moment = 10.0 ** np.nanmean(np.log10(flat_displacement_samples["S"]))
elif flat_displacement_samples["S"] is None:
    moment = 10.0 ** np.nanmean(np.log10(flat_displacement_samples["P"]))
else:
    moment = 10.0 ** np.nanmean(
        np.log10(
            np.hstack([flat_displacement_samples["P"], flat_displacement_samples["S"]])
        )
    )
moment_magnitude = (2.0 / 3.0) * np.log10(moment) - 6.05

stress_drop_by_phase, corner_frequency, source_radius_by_phase = {}, {}, {}
for phase in ["P", "S"]:
    if acceleration_spectrum_around_corner[phase] is not None:
        curve = f"{phase} curve"
        (
            stress_drop_by_phase[phase],
            source_radius_by_phase[phase],
            corner_frequency[phase],
        ) = find_stress_drop(
            acceleration_spectrum_around_corner[phase],
            freqs_for_stress_drop[phase],
            moment,
            event_location["rock properties"],
            phase,
            processing_config,
            model="Brune",
        )
    else:
        stress_drop_by_phase[phase] = None
        source_radius_by_phase[phase] = None
        corner_frequency[phase] = None


source_parameters[event.get_event_id()] = {
    "Mw": moment_magnitude,
    "corner frequency P": corner_frequency["P"],
    "corner_frequency S": corner_frequency["S"],
    "Ml": origin.get_ml(),
}
