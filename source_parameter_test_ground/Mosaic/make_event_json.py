from datetime import datetime, timedelta
from glob import glob
import json
import os

from obspy import UTCDateTime, read, read_inventory
import pyproj as pr

epsg_code = "32613"
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{epsg_code}")
utm_bf16_easting, utm_bf16_northing = pr.transform(
    latlon_proj, out_proj, -101.875725, 50.61373
)

center_bf16_ft = {"e": 62034.833333333336, "n": 39167.25}
center_bf16_m = {k: v * 0.3084 for k, v in center_bf16_ft.items()}

offset = {
    "e": utm_bf16_easting - center_bf16_m["e"],
    "n": utm_bf16_northing - center_bf16_m["n"],
}

catalog = r"C:\Users\adambaig\Project\Mosaic_Esterhazy\Mosaic_catalog_01_04.csv"
with open(catalog) as f:
    _head = f.readline()
    lines = f.readlines()
    events = {}
    for line in lines:
        split_line = line.split(",")
        pick_id = split_line[2]
        events[pick_id] = {
            "datetime": datetime.strptime(split_line[3], "%Y-%m-%dT%H:%M:%S.%fZ"),
            "northing": float(split_line[11]),
            "easting": float(split_line[10]),
            "elevation": -float(split_line[13]),
            "moment magnitudes": float(split_line[16]),
            "corner frequency": float(split_line[17]),
            "static stress drop": float(split_line[18]),
        }
pick_files = glob("Mosaic//picks//*.picks")
for pick_file in pick_files:
    event_id = os.path.basename(pick_file).split("_")[0].lstrip("0")
    if event_id in events:
        with open(pick_file) as f:
            _head = f.readline()
            lines = f.readlines()
        split_line = lines[0].split(",")
        pick_id_counter = 1234
        channel_id_counter = 3456
        arrival_id_counter = 4567
        arrivals = []
        for line in lines:
            split_line = line.split(",")
            phase = split_line[1]
            network, station, location = split_line[0].split(".")
            pick_id_counter += 1
            arrival_id_counter += 1
            channel_id_counter += 1
            p_pick_time = float(split_line[2])
            channelPath = {
                "networkCode": network,
                "stationCode": station,
                "locationCode": location,
                "channelPath": "GP1" if phase == "P" else "GPZ",
            }
            arrivals.append(
                {
                    "id": str(arrival_id_counter),
                    "pick": {
                        "id": str(pick_id_counter),
                        "phase": phase,
                        "time": p_pick_time,
                        "channel": {
                            "id": str(channel_id_counter),
                            "channelPath": channelPath,
                        },
                    },
                }
            )
        northing_m = events[pick_id]["northing"]
        easting_m = events[pick_id]["easting"]
        depth_km = (522.02 - events[pick_id]["elevation"]) / 1000
        event_longitude, event_latitude = pr.transform(
            out_proj,
            latlon_proj,
            easting_m,
            northing_m,
        )
        event_json = {
            "id": pick_id,
            "name": pick_id,
            "preferredOrigin": {
                "id": "2",
                "eventId": pick_id,
                "createdAt": UTCDateTime.now().timestamp,
                "time": UTCDateTime(events[event_id]["datetime"]).timestamp,
                "depth": depth_km,
                "latitude": event_latitude,
                "longitude": event_longitude,
                "arrivals": arrivals,
            },
        }
        with open(
            os.path.join(
                "Mosaic",
                "event_jsons",
                os.path.basename(pick_file).replace(".picks", ".json"),
            ),
            "w",
        ) as f:
            json.dump(event_json, f)
