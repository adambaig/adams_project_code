#%%
from ARC_FAS_analysis.User_Inputs_SF import SF_INPUTS

locals().update(SF_INPUTS)
import sys

sys.path.append(EngSeisTools)
import warnings

warnings.filterwarnings("ignore")
import numpy as np
import pandas as pd
from estk.Tools.spectralFitting import (
    attenCorr2,
    calcEi,
    Kappa_correction,
    calcFcEst,
    calcSource,
    plotMw,
)

#%%


def main(
    FAS_FilePath,
    FASSNR_FilePath,
    FileDir,
    Q,
    Vs,
    b1,
    bt,
    b2,
    R1,
    R2,
    kappa,
    FASSNR_min,
    VsQ,
    rho,
    Qmin=1.0,
):

    FASTable = pd.read_csv(FAS_FilePath)
    FASSNRTable = pd.read_csv(FASSNR_FilePath)
    FAS_Corr, FDS_Corr = attenCorr2(
        FileDir, FASTable, Q, VsQ, b1, bt, b2, R1, R2, plotit=0, Qmin=Qmin
    )
    Ei, std = calcEi(FAS_Corr, FASSNRTable, RHYPMIN, RHYPMAX, SNR=FASSNR_min)
    Ei2 = calcFcEst(Ei, FileDir)
    Ei2.to_csv(FileDir + r"Ei2.csv", index=False)
    Ao, Do = Kappa_correction(Ei2, FileDir, kappa, m=100)
    calcSource(Ao, Do, FileDir, std, beta=Vs, rho=rho)  # Ao,Do =
    plotMw(FileDir)


if __name__ == "__main__":
    main(
        FAS_FilePath,
        FASSNR_FilePath,
        FileDir,
        Q,
        Vs,
        b1,
        bt,
        b2,
        R1,
        R2,
        kappa,
        FASSNR_min,
        VsQ,
        rho,
    )

Ei = Ei2
hr = [
    "#EventID",
    "OriginTime",
    "Latitude",
    "Longitude",
    "Depth(km)",
    "Magnitude",
    "MagType",
    "fhp",
    "Tusable",
    "flp",
    "Fc",
    "fmax4mo",
    "fmin4ds",
]

i = 0
h = hr[0]
freq = Ei.columns.values[10:-3]
Ao = pd.DataFrame()
Do = pd.DataFrame()
for h in hr:
    Ao[h] = Ei[h]
    Do[h] = Ei[h]

for i in Ei.index.values:
    tmpEi = Ei.loc[i]
    for f in range(len(freq)):
        if tmpEi[freq[f]] > 0.0:
            tmpAo = 10.0 ** (
                np.log10(tmpEi[freq[f]])
                + (np.pi * kappa * float(freq[f]) / np.log(10.0))
            )
            Ao.loc[i, freq[f]] = tmpAo
            Do.loc[i, freq[f]] = (tmpAo) / (2.0 * np.pi * float(freq[f])) ** 2.0
        else:
            Ao.loc[i, freq[f]] = -999.0
            Do.loc[i, freq[f]] = -999.0

Radpat = 0.55
Vpart = 0.707
FreeS = 2.0
beta = 3.4
rho = 2.8
R0 = 1
qcplt = 1
finalfig = 1
Ao.columns.values[13:]
ei_X = [float(f) for f in freq]
dstrialplt = [
    0.00001,
    0.00003,
    0.0001,
    0.0003,
    0.001,
    0.003,
    0.01,
    0.03,
    0.1,
    0.3,
    1.0,
    3.0,
    10.0,
    30.0,
    100.0,
    300.0,
    1000.0,
]

A0 = Ao.loc[i, freq[0] : freq[-1]]
D0 = Do.loc[i, freq[0] : freq[-1]]
fc = Ao.loc[i, "Fc"]
Velmdl = pd.read_csv(FileDir + r"/4_FAS_Analysis/Tables/Vel-Density.csv")
vtmp = Velmdl.loc[
    (Velmdl["Depth to Top (km)"] < Ao["Depth(km)"][i])
    & (Velmdl["Depth to Bottom (km)"] >= Ao["Depth(km)"][i])
].reset_index(drop=True)

maxmo = Ao.loc[i, "fmax4mo"]
Tuse = 1.0 / Ao.loc[i, "Tusable"]
minds = Ao.loc[i, "fmin4ds"]
Tusemax = 0.9 * Ao.loc[i, "flp"]
indxfmin4mo = next(x[0] for x in enumerate(ei_X) if x[1] > Tuse)
indxfmax4mo = next(x[0] for x in enumerate(ei_X) if x[1] > maxmo)
indxfmin4ds = next(x[0] for x in enumerate(ei_X) if x[1] > minds)
indxfmax4ds = next(x[0] for x in enumerate(ei_X) if x[1] > Tusemax)

ei_X
