#%%
"""
This is a dictionary file to serve as a user input file for SpectralFit.py,
a program that performs spectral fitting on waveforms
"""
SF_INPUTS = {
    # Path to tools:
    "EngSeisTools": r"C:\Users\adambaig\python_modules\sms_estk",  # r"YOURPATHTOENGSEISTOOLSHERE"
    "FileDir": r"ARC_FAS_analysis",
    # Path to input/output directories:
    # Inputs:
    "FAS_FilePath": r"ARC_FAS_analysis/4_FAS_Analysis/Tables/FASDatTableV.csv",
    "FASSNR_FilePath": r"ARC_FAS_analysis/4_FAS_Analysis/Tables/FASSNRTableV.csv",
    # Ouputs:
    # UPDATE THIS DIRECTORY :)
    # Distance Filter:
    "RHYPMIN": 0,
    "RHYPMAX": 50,
    # Attenuation Coefficients
    # Ensure the area you are spectrally fitting has had its attenuation parameters determined.
    "R1": 100.0,  # Transition 1, [if R1 > max(Rhyp), linear], [if R1 == R2 & R1 < max(Rhyp), bilinear], [if R2 > R1 & R1 < Max(Rhyp), trilinear].
    "R2": 100.0,
    "b1": -1.0,
    "bt": 0.0,
    "b2": -1.0,
    "Q": [176.65, 0.64],  # [82.99,1.42]
    "VsQ": 3.04,  # Vs used during Q Modeling
    "Qmin": 1,
    # SpectralFitting Parameters:
    "kappa": 0.0042,
    "Rpat": 0.55,
    "Vpart": 0.707,
    "FreeSAmp": 2,
    "FASSNR_min": 3.0,
    # Adjust to match velocity at the depth of your event from velocity model.
    "Vs": 3.04,  # km/s
    "rho": 2.63,  # g/cm^3  #if unspecified in model, estimate from your 'Vs' as: rho = 0.23*(Vp/0.0254/12)**0.25*1000
}
