import json
from shapely.geometry import Polygon, Point

import matplotlib.pyplot as plt
import numpy as np
from obspy import UTCDateTime

with open(r"Braskem\Braskem_AOI.json") as f:
    aoi = json.load(f)

with open(r"Braskem\sp_tuned_B_results.json") as f:
    sp_results = json.load(f)

with open(r"Braskem\event_locations.json") as f:
    event_locations = json.load(f)

for event_time, source_parameters in sp_results.items():
    location = [
        e
        for e in event_locations.values()
        if str(UTCDateTime(e["origin time"])) == event_time
    ][0]
    sp_results[event_time] = {**source_parameters, **location}


aoi_polygon = Polygon(aoi["geometry"]["coordinates"])

a_data = {k: v for k, v in sp_results.items() if v["quality_score"] == "A"}
b_data = {k: v for k, v in sp_results.items() if v["quality_score"] == "B"}
c_data = {k: v for k, v in sp_results.items() if v["quality_score"] == "C"}
a_mags = [
    v["mw"] for v in a_data.values() if aoi_polygon.contains(Point(v["lon"], v["lat"]))
]
b_mags = [
    v["mw"] for v in b_data.values() if aoi_polygon.contains(Point(v["lon"], v["lat"]))
]
c_mags = [
    v["mw"] for v in c_data.values() if aoi_polygon.contains(Point(v["lon"], v["lat"]))
]


m_bins = np.arange(-2.5, 1, 0.1)
fig, ax = plt.subplots()
ax.hist(c_mags, m_bins, facecolor="royalblue", alpha=0.7)
ax.hist(b_mags, m_bins, facecolor="forestgreen", alpha=0.7)
ax.hist(a_mags, m_bins, facecolor="firebrick", alpha=0.7)

n = []
for m in m_bins:
    a_mags_above_m = [mw for mw in a_mags if mw > m]
    b_mags_above_m = [mw for mw in b_mags if mw > m]
    c_mags_above_m = [mw for mw in c_mags if mw > m]
    n.append(len(a_mags_above_m) + len(b_mags_above_m))


fig, ax = plt.subplots(figsize=[6, 6])
ax.semilogy(m_bins, n, "o")
i_bounds = [10, 26]
ax.semilogy(
    [m_bins[i_bounds[0]], m_bins[i_bounds[1]]],
    [1.3 * n[i_bounds[0]], 1.3 * n[i_bounds[1]]],
    "k",
)


(m_bins[i_bounds[1]] - m_bins[i_bounds[0]]) / (
    np.log10(n[i_bounds[0]]) - np.log10(n[i_bounds[1]])
)

ax.grid()
ax.set_xlabel("moment magnitude")
ax.set_ylabel("# events above magnitude")

fig.savefig("fmd.png")

colors = {"A": "forestgreen", "B": "lightsteelblue", "C": "darkgoldenrod"}
zorders = {"A": 4, "B": 3, "C": 2}
fig, ax = plt.subplots(figsize=[6, 6])
ax.set_aspect("equal")
for subset, label in zip([a_data, b_data, c_data], ["A", "B", "C"]):
    aoi_data = {
        k: v
        for k, v in subset.items()
        if aoi_polygon.contains(Point(v["lon"], v["lat"]))
    }
    ax.plot(
        [v["mw"] for v in aoi_data.values()],
        [v["ml"] for v in aoi_data.values()],
        "o",
        color=colors[label],
        markeredgecolor="0.2",
        zorder=zorders[label],
    )


xlim = ax.get_xlim()
ylim = ax.get_ylim()
ax_min = min(*xlim, *ylim)
ax_max = max(*xlim, *ylim)
ax.plot([ax_min, ax_max], [ax_min, ax_max], "k--", zorder=0)
ax.set_xlim([ax_min, ax_max])
ax.set_ylim([ax_min, ax_max])
ax.set_xlabel("moment magnitude")
ax.set_ylabel("local magnitude")
fig.savefig("mw_vs_ml.png")
