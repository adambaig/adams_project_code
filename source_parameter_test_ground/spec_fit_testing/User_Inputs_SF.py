#%%
"""
This is a dictionary file to serve as a user input file for SpectralFit.py,
a program that performs spectral fitting on waveforms 
"""
SF_INPUTS = {
    # Path to tools:
    "EngSeisTools": r"C:\Users\MarkNovakovic\Documents\Nanometrics\B_TODO\GitWorld\sms_estk\\",  # r"YOURPATHTOENGSEISTOOLSHERE"
    # Path to input/output directories:
    # Inputs:
    "FAS_FilePath": r"C:\Users\MarkNovakovic\Documents\Nanometrics\B_TODO\ESTK_Testing\4_FAS_Analysis\Tables\FASDatTableV.csv",
    "FASSNR_FilePath": r"C:\Users\MarkNovakovic\Documents\Nanometrics\B_TODO\ESTK_Testing\4_FAS_Analysis\Tables\FASSNRTableV.csv",
    # Ouputs:
    # UPDATE THIS DIRECTORY :)
    "FileDir": r"C:\Users\MarkNovakovic\Documents\Nanometrics\B_TODO\ESTK_Testing\\",
    # Distance Filter:
    "RHYPMIN": 0,
    "RHYPMAX": 200,
    # Attenuation Coefficients
    # Ensure the area you are spectrally fitting has had its attenuation parameters determined.
    "R1": 50.0,  # Transition 1, [if R1 > max(Rhyp), linear], [if R1 == R2 & R1 < max(Rhyp), bilinear], [if R2 > R1 & R1 < Max(Rhyp), trilinear].
    "R2": 150.0,
    "b1": -1.3,
    "bt": 0.0,
    "b2": -0.5,
    "Q": [163.12, 0.92],  # [82.99,1.42]
    "VsQ": 3.47,  # Vs used during Q Modeling
    "Qmin": 1,
    "R0": 1,
    "min_nrec": 1,
    "applyAdjustment": True,
    "depthAdjustmentcsv": r"Path\To\Adjustment\depthAdjustment.csv",
    "nslc2correctcsv": r"Path\To\Stations2AdjustList\nslc2correct.csv",
    "nslc2usecsv": r"Path\To\Stations2Use\nslc2use.csv",
    # SpectralFitting Parameters:
    "kappa": 0.0059,
    "Rpat": 0.55,
    "Vpart": 0.707,
    "FreeSAmp": 2,
    "FASSNR_min": 3.0,
    # Adjust to match velocity at the depth of your event from velocity model.
    "Vs": 3.47,  # km/s
    "rho": 2.8,  # g/cm^3  #if unspecified in model, estimate from your 'Vs' as: rho = 0.23*(Vp/0.0254/12)**0.25*1000
}
