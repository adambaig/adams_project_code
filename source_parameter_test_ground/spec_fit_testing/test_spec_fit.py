import os

import pandas as pd
from estk.Tools.spectralFitting import (
    filtFASNSLC,
    depthAdjustment,
    attenCorr2,
    calcEi,
    Kappa_correction,
    calcFcEst,
    calcSource,
    plotMw,
)

basedir = r"C:\Users\adambaig\Project\Artis 9-18\Magnitude Calibration\Magnitude\4_FAS_Analysis\Tables"

FAS_FilePath = os.path.join(basedir, "FASDatTableALL.csv")
FASSNR_FilePath = os.path.join(basedir, "FASSNRTableALL.csv")


def SpecralFit_Logik(
    FAS_FilePath,
    FASSNR_FilePath,
    FileDir,
    Q,
    Vs,
    b1,
    bt,
    b2,
    R1,
    R2,
    kappa,
    FASSNR_min,
    velmdl,
    VsQ,
    rho,
    Qmin=1.0,
    R0=1,
    applyAdjustment=False,
    depthAdjustmentcsv=None,
    nslc2correctcsv=None,
    min_nrec=3,
    nslc2use=None,
):
    FASTable = pd.read_csv(FAS_FilePath)
    FASSNRTable = pd.read_csv(FASSNR_FilePath)
    if nslc2use != None:
        FASTable, FASSNRTable = filtFASNSLC(FASTable, FASSNRTable, nslc2use)
    if applyAdjustment == True:
        FASTable = depthAdjustment(
            FileDir, FASTable, depthAdjustmentcsv, nslc2correctcsv
        )
    FAS_Corr, FDS_Corr = attenCorr2(
        FileDir, FASTable, Q, VsQ, b1, bt, b2, R1, R2, plotit=0, Qmin=Qmin, R0=R0
    )
    Ei, std = calcEi(
        FAS_Corr, FASSNRTable, 0.0, 200.0, SNR=FASSNR_min, min_nrec=min_nrec
    )
    Ei2 = calcFcEst(Ei, FileDir)
    Ao, Do = Kappa_correction(Ei2, FileDir, kappa, m=100)
    calcSource(Ao, Do, FileDir, std, velmdl=velmdl, beta=Vs, rho=rho, R0=R0)
    plotMw(FileDir)
