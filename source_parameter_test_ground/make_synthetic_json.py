import os
import json

import numpy as np
import obspy
from sms_ray_modelling.raytrace import isotropic_ray_trace

now = obspy.UTCDateTime()
obspy.UTCDateTime(1636741880.054365)
event_t0 = obspy.UTCDateTime(os.path.basename(seedfile).split("_")[0])

with open(r"synthetic/228_stations_epsg26911.csv") as f:
    _head = f.readline()
    lines = f.readlines()

stations = {}
for line in lines:
    split_line = line.split(",")
    stations[split_line[0].split(".")[1]] = {
        "e": float(split_line[1]),
        "n": float(split_line[2]),
        "z": -float(split_line[3]),
    }

source = {
    "e": np.average([v["e"] for k, v in stations.items()]),
    "n": np.average([v["n"] for k, v in stations.items()]),
    "z": -2100,
    "moment_magnitude": 0,
    "stress_drop": 1.0e6,  # static stress drop
}


velocity_model = [{"rho": 2734.0, "vp": 6052.0, "vs": 3303.0}]
channel_code = {"P": "CPZ", "S": "CPE"}
i = 0
arrivals = []
for station_id, station in stations.items():
    for phase in ["P", "S"]:
        i += 1
        arrival_id = str(6780 + i)
        ray = isotropic_ray_trace(source, station, velocity_model, phase)
        arrival_time = (event_t0 + ray["traveltime"]).timestamp
        pick_id = str(1234 + i)
        channel_id = str(3456 + i)
        arrival = {
            "id": arrival_id,
            "pick": {
                "id": pick_id,
                "phase": phase,
                "time": arrival_time,
                "channel": {
                    "id": channel_id,
                    "channelPath": {
                        "networkCode": "CV",
                        "stationCode": station_id,
                        "locationCode": "00",
                        "channelCode": channel_code[phase],
                    },
                },
            },
        }
        arrivals.append(arrival)


longitude, latitude = -116.93805538849605, 54.401317823038646
event_json = {
    "id": "1",
    "name": "20211028-2",
    "preferredOrigin": {
        "id": "2",
        "eventId": "1",
        "createdAt": now.timestamp,
        "depth": 2.1,
        "time": event_t0.timestamp,
        "latitude": latitude,
        "longitude": longitude,
        "arrivals": arrivals,
    },
}

with open(r"synthetic/synthetic_event.json", "w") as f:
    json.dump(event_json, f)
