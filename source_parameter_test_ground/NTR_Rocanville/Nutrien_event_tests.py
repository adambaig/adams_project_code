import json
import os

import matplotlib.pyplot as plt
from NocMeta.Meta import NOC_META
import numpy as np
from obspy import UTCDateTime
import pyproj

from realtime_source_parameters.event_origin_processor import EventOriginProcessor
from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)
from realtime_source_parameters.athena_query_helper import (
    get_single_event_origin,
    get_new_events_since,
)
from realtime_source_parameters.waveforms_query_helper import get_waveforms_for_origin

from realtime_source_parameters.engine import (
    get_velocity_lookup_and_raytracer,
    rotate_stream_to_raypaths,
    get_station_locations,
    get_windows,
    get_hypocentral_distance,
    get_highpass,
)

from realtime_source_parameters.spectral_measurements import (
    get_signal_and_noise_spectra,
    calculate_mean_spectrum,
    get_useable_bandwidth_for_moment_and_stress_drop,
    find_stress_drop,
    determine_preliminary_corner_and_energy_flux,
    compensate_for_attenuation_ray,
    kappa_correction,
)
from realtime_source_parameters.util import (
    is_arrival_matching_at_the_same_location,
    get_simple_phase,
)


from realtime_source_parameters.estk_helpers import (
    fourier_spectra,
    # motion_filt,
    determine_gaps_and_bandwidth,
    gapcheck,
)

from nmxseis.interact.athena import AthenaClient
from nmxseis.interact.fdsn import FDSNClient

NTR_STATION_XML = os.path.join("NTR_Rocanville", "NTR_RCN_Full.xml")
NTR_CONFIG = os.path.join("NTR_Rocanville", "ntr_rocanville_config.json")
config = load_config_from_file(NTR_CONFIG)
inventory = get_station_inventory_from_file(NTR_STATION_XML)
athena_client = AthenaClient(config.get_athena_base_url(), config.get_athena_api_key())
events = get_new_events_since(
    athena_client, UTCDateTime("2022-01-16").timestamp, "MANUAL", 14
)
velocity_model_config = config.get_velocity_model_config()
processing_config = config.get_processing_config()
fourier_spectra_config = config.get_fourier_spectra_config()

processor = EventOriginProcessor(None, None, config)
velocity_lookup, raytracer = get_velocity_lookup_and_raytracer(velocity_model_config)
source_parameters = {}
# waveforms_stream.plot()
for event in events:
    waveforms_stream = get_waveforms_for_origin(
        FDSNClient(config.get_apollo_server_ip()), event.get_origin(), 5, 10
    )
    origin = event.get_origin()
    arrivals = origin.get_arrivals()
    origin_time = origin.get_time()
    event_latitude = origin.get_latitude()
    event_longitude = origin.get_longitude()
    event_depth_km = origin.get_depth()
    reproject = pyproj.Proj("epsg:" + str(config.get_epsg_code()))
    stations = get_station_locations(arrivals, inventory, reproject)
    easting, northing = reproject(event_longitude, event_latitude, inverse=False)
    event_depth = event_depth_km * 1000.0
    event_location = {"e": easting, "n": northing, "z": -event_depth}
    Q = velocity_model_config.get_Q()
    kappa = processing_config.get_kappa()
    lowpass_percent = processing_config.get_lowpass_percent()
    raypaths = {}
    for station_id, station in stations.items():
        station["rock properties"] = velocity_lookup(station)
        for phase in ["P", "S"]:
            raypaths[f"{station_id}.{phase}"] = raytracer(
                event_location, station, phase
            )
    event_location["rock properties"] = velocity_lookup(event_location)
    for net_stat_loc, station in stations.items():
        network_code, station_code, location_code = net_stat_loc.split(".")
        station_traces = waveforms_stream.select(
            network=network_code, station=station_code, location=location_code
        )
        station["hypo_distance"] = get_hypocentral_distance(station, event_location)
        station["highpass"] = get_highpass(
            processing_config, origin, station["hypo_distance"]
        )
        station["lowpass"] = (
            (1.0 - lowpass_percent / 100.0) * station_traces[0].stats.sampling_rate / 2
        )
    try:
        rotated_traces = rotate_stream_to_raypaths(
            waveforms_stream,
            stations,
            event_location,
            raytracer,
            inventory=inventory,
        )
    except:
        print(f"failed to rotate event {event.get_event_id()}")
        continue

    noise_windows, signal_windows = {}, {}
    for arrival in arrivals:
        pick = arrival.get_pick()
        channel_path = pick.get_channel_path()
        phase = pick.get_phase()
        net_stat_loc_phase = ".".join(
            [
                *[
                    channel_path[code]
                    for code in ["networkCode", "stationCode", "locationCode"]
                ],
                phase,
            ]
        )
        net_stat_loc = net_stat_loc_phase[:-2]
        (
            signal_windows[net_stat_loc_phase],
            noise_windows[net_stat_loc_phase],
        ) = get_windows(
            arrival, arrivals, raytracer, event_location, stations, processing_config
        )
        stations[net_stat_loc][f"{phase} window"] = signal_windows[net_stat_loc_phase]

    (
        acceleration_spectra,
        acceleration_noise_spectra,
        freqs,
    ) = get_signal_and_noise_spectra(
        rotated_traces,
        signal_windows,
        noise_windows,
        fourier_spectra_config.get_parameter_list(),
    )

    signal_to_noise_ratios = {}
    for net_stat_loc_phase in acceleration_spectra:
        signal_to_noise_ratios[net_stat_loc_phase] = [
            s / n if s > 0 and n > 0 else -999.0
            for s, n in zip(
                acceleration_spectra[net_stat_loc_phase],
                acceleration_noise_spectra[net_stat_loc_phase],
            )
        ]

    signal_to_noise_ratios_by_phase = {
        "P": {k: v for k, v in signal_to_noise_ratios.items() if k[-1] == "P"},
        "S": {k: v for k, v in signal_to_noise_ratios.items() if k[-1] != "P"},
    }

    (
        acceleration_spectra_corrected,
        displacement_spectra_corrected,
    ) = compensate_for_attenuation_ray(
        acceleration_spectra, freqs, raypaths, Q, origin, stations, event_location
    )

    acc_spectrum = {"P curve": 0, "S curve": 0, "P sigma": 0, "S sigma": 0}
    disp_spectrum = {"P curve": 0, "S curve": 0, "P sigma": 0, "S sigma": 0}

    for phase in ["P", "S"]:
        (
            acc_spectrum[f"{phase} curve"],
            acc_spectrum[f"{phase} sigma"],
        ) = calculate_mean_spectrum(
            acceleration_spectra_corrected[phase],
            signal_to_noise_ratios_by_phase[phase],
            freqs,
            stations,
            processing_config,
        )
        (
            disp_spectrum[f"{phase} curve"],
            disp_spectrum[f"{phase} sigma"],
        ) = calculate_mean_spectrum(
            displacement_spectra_corrected[phase],
            signal_to_noise_ratios_by_phase[phase],
            freqs,
            stations,
            processing_config,
        )

    freqs_for_moment = {}
    freqs_for_stress_drop = {}
    flat_displacement_samples = {}
    acceleration_spectrum_around_corner = {}
    for curve in ["P curve", "S curve"]:
        phase = curve.split()[0]
        (
            flat_displacement_samples[phase],
            freqs_for_moment[phase],
            acceleration_spectrum_around_corner[phase],
            freqs_for_stress_drop[phase],
        ) = get_useable_bandwidth_for_moment_and_stress_drop(
            acc_spectrum[curve],
            disp_spectrum[curve],
            freqs,
            stations,
            processing_config,
        )

    if (
        flat_displacement_samples["P"] is None
        and flat_displacement_samples["S"] is None
    ):
        continue
    if flat_displacement_samples["P"] is None:
        moment = 10.0 ** np.nanmean(np.log10(flat_displacement_samples["S"]))
    elif flat_displacement_samples["S"] is None:
        moment = 10.0 ** np.nanmean(np.log10(flat_displacement_samples["P"]))
    else:
        moment = 10.0 ** np.nanmean(
            np.log10(
                np.hstack(
                    [flat_displacement_samples["P"], flat_displacement_samples["S"]]
                )
            )
        )
    moment_magnitude = (2.0 / 3.0) * np.log10(moment) - 6.05

    stress_drop_by_phase, corner_frequency, source_radius_by_phase = {}, {}, {}
    for phase in ["P", "S"]:
        if acceleration_spectrum_around_corner[phase] is not None:
            curve = f"{phase} curve"
            (
                stress_drop_by_phase[phase],
                source_radius_by_phase[phase],
                corner_frequency[phase],
            ) = find_stress_drop(
                acceleration_spectrum_around_corner[phase],
                freqs_for_stress_drop[phase],
                moment,
                event_location["rock properties"],
                phase,
                processing_config,
                model="Brune",
            )
        else:
            stress_drop_by_phase[phase] = None
            source_radius_by_phase[phase] = None
            corner_frequency[phase] = None

    ax = {}
    fig, (ax["P"], ax["S"]) = plt.subplots(
        1, 2, figsize=[12, 7], sharex=True, sharey=True
    )
    for phase in ["P", "S"]:
        if max(disp_spectrum[f"{phase} curve"]) > 0:
            plot_freqs, plot_spectrum = np.array(
                [
                    (freq, spec)
                    for freq, spec in zip(freqs, disp_spectrum[f"{phase} curve"])
                    if spec > 0
                ]
            ).T
            ax[phase].loglog(plot_freqs, plot_spectrum, "k", zorder=1)
    y1, y2 = ax["P"].get_ylim()
    x1, x2 = ax["P"].get_xlim()
    for phase in ["P", "S"]:
        for net_stat_loc_phase, d_spectrum in displacement_spectra_corrected[
            phase
        ].items():
            simple_phase = "P" if phase == "P" else "S"
            net_stat_loc = net_stat_loc_phase[:-2]
            useable_period = np.diff(stations[net_stat_loc][f"{simple_phase} window"])
            effective_lowcut = max(
                stations[net_stat_loc]["highpass"], 1 / useable_period
            )
            is_in_applicable_range = (
                stations[net_stat_loc_phase[:-2]]["hypo_distance"]
                <= processing_config.get_max_hypo_distance_m()
                and stations[net_stat_loc_phase[:-2]]["hypo_distance"]
                >= processing_config.get_min_hypo_distance_m()
            )
            k_corrected_displacement = kappa_correction(kappa, d_spectrum, freqs)
            if is_in_applicable_range:
                plot_freqs, plot_spectrum = [], []
                for i_freq, (freq, spec) in enumerate(
                    zip(freqs, k_corrected_displacement)
                ):
                    is_in_appropriate_bandwidth = (
                        freq > 1.25 * effective_lowcut
                        and freq < 0.75 * stations[net_stat_loc]["lowpass"]
                    )
                    has_good_signal = (
                        spec > 0
                        and signal_to_noise_ratios_by_phase[simple_phase][
                            net_stat_loc_phase
                        ][i_freq]
                        > 3
                    )
                    if is_in_appropriate_bandwidth and has_good_signal:
                        plot_freqs.append(freq)
                        plot_spectrum.append(spec)
                if len(plot_freqs) > 1:
                    ax[simple_phase].loglog(
                        plot_freqs, plot_spectrum, alpha=0.8, zorder=-2
                    )

    for a in ax.values():
        a.loglog([x1, x2], [moment, moment], "r:")
        a.set_xlim(x1, x2)
        a.grid()
    ax["S"].text(
        0.95,
        0.84,
        f"RTSP Mw {moment_magnitude:.2f}",
        transform=ax["S"].transAxes,
        ha="right",
    )
    ax["P"].set_ylabel("corrected displacment spectrum (N$\cdot$m$\cdot$s)")
    ax["P"].set_xlabel("frequency (Hz)")
    ax["S"].set_xlabel("frequency (Hz)")
    ax["P"].set_title("P spectra")
    ax["S"].set_title("S spectra")
    source_parameters[event.get_event_id()] = {
        "Mw": moment_magnitude,
        "corner frequency P": corner_frequency["P"],
        "corner_frequency S": corner_frequency["S"],
        "Ml": origin.get_ml(),
    }
    fig.savefig(f"NTR_Rocanville//QC_plots//qc_plots_{event.get_event_id()}.png")
