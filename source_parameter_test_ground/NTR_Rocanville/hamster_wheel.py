import json
from pathlib import Path

import numpy as np
from nmxseis.model.nslc import NSL
from nmxseis.model.pick_set import PickSet
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.formulas import calculate_scalar_potency
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.source_parameters.core import SPQualityCascadeFailed
from nmxseis.numerics.source_parameters.evaluator import SParamsCascadingEvaluator
from nmxseis.numerics.velocity_model.vm_1d import VelocityModel1D, VMLayer1D
from obspy import read_inventory, read, UTCDateTime

base_dir = Path('NTR_Rocanville')
json_dir = Path(r'C:\Users\adambaig\Project\source_parameter_test_ground\NTR_Rocanville\event_locations')
station_xml = base_dir.joinpath("NTR_RCN_Full.xml")
inventory = read_inventory(station_xml)


exclude_nsls = [NSL.parse('TX.PB09.00'),NSL.parse('TX.PB14.00'),NSL.parse('TX.PB19.00')]



with open(base_dir.joinpath("velocity_model.json")) as f:
    velocity_model_json = json.load(f)

source_parameters = {}
reproject = EPSGReprojector(32613)

layers = []
for layer in velocity_model_json:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )
velocity_model = VelocityModel1D(layers)




def get_nsl_from_channel_path(channel_path):
    return NSL(
        net=channel_path["networkCode"],
        sta=channel_path["stationCode"],
        loc=channel_path["locationCode"],
    )

noc_sp_config = {
			"qualities": {
				"A": {"min_snr": 3, "min_samples_per_bin": 5, "f_smooth_min": 0.1},
				"B": {"min_snr": 3, "min_samples_per_bin": 3, "f_smooth_min": 0.5},
				"C": {"min_snr": 1.5, "min_samples_per_bin": 2, "f_smooth_min": 1},
			},
			"lowpass_nyq_backoff": 0.2,
			"min_d_hypo_m": 0,
			"max_d_hypo_m": 50000,
			"Q": {"P": 500, "S": 500},
			"kappa": 0.0,
			"f_smooth_max": 100,
			"f_smooth_n_bins": 41,
			"radius_model": "Madariaga",
			"spectrum_model": "Brune",
			"f_highpass": "static",
		}


sp_evaluator = SParamsCascadingEvaluator(velocity_model, reproject, noc_sp_config)
event_location_jsons = list(json_dir.glob('*.json'))

for location_json in event_location_jsons[:1]:
    event_id = location_json.stem
    stream = read(base_dir / 'event_waveforms' / f'{event_id}.mseed')
    with open(location_json) as f:
        location = json.load(f)
    source = EventOrigin(
        lat=location['latitude'],
        lon=location['longitude'],
        depth_m=location['depth_m'],
        time=UTCDateTime(location['time'])
    )
    picks_file = list(base_dir.joinpath('athena_picks').glob(f'{event_id.zfill(10)}*.picks'))[0]
    pick_set = PickSet.load(picks_file)
    print(f"evaluating event {event_id}")

    try:
        source_parameters = sp_evaluator.evaluate(source, stream, inventory, pick_set.to_arrivals_dict(),
                                                  exclude_nsls=exclude_nsls)
        print(f"finished event {event_id}")
    except SPQualityCascadeFailed:
        print(f"evaluation failed {event_id}")
        continue

    potency = calculate_scalar_potency(
        source_parameters.moment,
        velocity_model.look_up_enu(source.enu(reproject)).shear_modulus,
    )
    apparent_stress = source_parameters.radiated_energy.calc_apparent_stress(potency)
    sw_efficiency = (
        None
        if source_parameters.static_stress_drop is None
        else source_parameters.radiated_energy.calc_sw_efficiency(
            potency, source_parameters.static_stress_drop
        )
    )
    sp_results = {
        "mw": source_parameters.mw.val,
        "mw_uncertainty": source_parameters.mw.unc,
        "moment": source_parameters.moment.val,
        "moment_uncertainty": source_parameters.moment.unc,
        "potency": potency.val,
        "potency_uncertainty": potency.unc,
        "p corner": source_parameters.fc_P.val if source_parameters.fc_P is not None else None,
        "s corner": source_parameters.fc_S.val if source_parameters.fc_S is not None else None,
        "quality_score": sp_evaluator.current_quality,
        "lat": source.lat,
        "lon": source.lon,
        "depth": source.depth_m,
        "origin time": source.time.timestamp,
        "enu": source.enu(reproject).tolist(),
        # "ml": event.get_origin().get_ml(),
        "stress_drop": source_parameters.static_stress_drop.stress_drop.val
        if source_parameters.static_stress_drop is not None
        else None,
        "stress_drop_uncertainty": source_parameters.static_stress_drop.stress_drop.unc
        if source_parameters.static_stress_drop is not None
        else None,
        "radiated_energy": source_parameters.radiated_energy.total.val,
        "radiated_energy_uncertainty": source_parameters.radiated_energy.total.unc,
        "radiated_energy_p": source_parameters.radiated_energy.p_energy.val,
        "radiated_energy_s": source_parameters.radiated_energy.s_energy.val,
        "radius": source_parameters.static_stress_drop.radius.val
        if source_parameters.static_stress_drop is not None
        else None,
        "radius_uncertainty": source_parameters.static_stress_drop.radius.unc
        if source_parameters.static_stress_drop is not None
        else None,
        "apparent_stress": apparent_stress.val,
        "apparent_stress_uncertainty": apparent_stress.unc,
        "sw_efficiency": sw_efficiency.val if source_parameters.static_stress_drop is not None else None,
        "sw_efficiency_uncertainty": sw_efficiency.unc
        if source_parameters.static_stress_drop is not None
        else None,
    }

    with open(
            base_dir.joinpath(
                "source_param_jsons",
                f"new_sp_results_{event_id.zfill(6)}.json",
            ),
            "w",
    ) as f:
        json.dump(sp_results, f)
