import json
from nmxseis.numerics.formulas import gardners_relation

with open(r"NTR_Rocanville\velocity_model\VelocityProfile_average.csv") as f:
    _head = f.readline()
    lines = f.readlines()

velocity_model = []
for line in lines:
    split_line = line.split(",")
    velocity_model.append(
        {
            "top": -1000 * float(split_line[0]),
            "vp": 1000 * float(split_line[1]),
            "vs": 1000 * float(split_line[2]),
            "rho": gardners_relation(1000 * float(split_line[1])).tolist(),
        }
    )

velocity_model[0].pop("top")


with open("NTR_Rocanville//rocanville_velocity_model.json", "w") as f:
    json.dump(velocity_model, f)
