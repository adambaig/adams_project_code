from collections import defaultdict
import json
import os

import numpy as np
from obspy import UTCDateTime

from realtime_source_parameters.offline_inputs_helper import (
    get_station_inventory_from_file,
)
from realtime_source_parameters.athena_query_helper import (
    get_single_event_preferred_origin,
)

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.model.source_radius import Madariaga
from nmxseis.numerics.formulas import brune_source_model
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.source_parameters import (
    compute_source_parameters,
    FSmoothMinTooHigh,
    QualityTooLowError,
    SourceParametersError,
    NoGoodStationsError,
)
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

from NocMeta.Meta import NOC_META

STATION_XML = os.path.join("NTR_RCN_Full.xml")

inventory = get_station_inventory_from_file(STATION_XML)
athena_config = NOC_META["NTR_RCN"]
athena_client = AthenaClient(
    rf'http://{athena_config["athIP"]}', athena_config["athApi"]
)
config = athena_config["source_param_config"]
with open("velocity_model.json") as f:
    velocity_model_json = json.load(f)

source_parameters = {}

epsg = NOC_META["NTR_RCN"]["epsg"]

layers = []
for layer in velocity_model_json:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )
velocity_model = VelocityModel1D(layers)

f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, velocity_model, phase
)
f_highpass = lambda x: 1


def get_nsl_from_channel_path(channel_path):
    return NSL(
        net=channel_path["networkCode"],
        sta=channel_path["stationCode"],
        loc=channel_path["locationCode"],
    )


def next_quality(current_quality, sorted_qualities):
    i_next_quality = sorted_qualities.index(current_quality) + 1
    if i_next_quality == len(sorted_qualities):
        raise SourceParametersError
    return sorted_qualities[i_next_quality]


def average_stress_drop(stress_drops):
    valid_phases = stress_drops.keys()
    if len(valid_phases) == 1:
        only_phase = list(valid_phases)[0]
        return stress_drops[only_phase].stress_drop, stress_drops[only_phase].radius
    if stress_drops[Phase.P] is None and stress_drops[Phase.S] is None:
        return None, None
    if stress_drops[Phase.S] is None:
        return stress_drops[Phase.P].stress_drop, stress_drops[Phase.P].radius
    if stress_drops[Phase.P] is None:
        return stress_drops[Phase.S].stress_drop, stress_drops[Phase.S].radius
    avg_stress_drop = np.sqrt(
        stress_drops[Phase.P].stress_drop * stress_drops[Phase.S].stress_drop
    )
    avg_radius = np.sqrt(stress_drops[Phase.P].radius * stress_drops[Phase.S].radius)
    return avg_stress_drop, avg_radius


event_list_query = """{eventList(
    latitudeMinimum:50.38292,
    latitudeMaximum:50.49968,
    longitudeMinimum:-101.79498,
    longitudeMaximum:-101.66452,
    analysisType: MANUAL) {
	events {
	  id
	}
    }
}
"""

event_ids = [
    v["id"] for v in athena_client.query_gql(event_list_query)["eventList"]["events"]
]

event_id = "3"


stream = athena_client.read_event_waveforms(event_id)

event = get_single_event_preferred_origin(athena_client, event_id)
arrivals = defaultdict(dict)
origin = event.get_origin()
for arrival in origin.get_arrivals():
    pick = arrival.get_pick()
    nsl = get_nsl_from_channel_path(arrival.get_pick().get_channel_path())
    arrivals[nsl][Phase.parse(pick.get_phase())] = UTCDateTime(pick.get_time())
nsls = [NSL.from_trace(tr) for tr in stream]
source = EventOrigin(
    time=origin.get_time(),
    lat=origin.get_latitude(),
    lon=origin.get_longitude(),
    depth_m=1000 * origin.get_depth(),
)
vm_at_source = velocity_model.look_up_enu(source.enu(NOC_META["NTR_RCN"]["epsg"]))
current_quality = "A"
sorted_qualities = sorted(list(config["qualities"].keys()))
f_smooth_min_adjustment = 1

while True:
    try:
        try:
            source_parameters = compute_source_parameters(
                stream=stream.copy(),
                nsls=nsls,
                inventory=inventory,
                arrivals=arrivals,
                f_raytrace=f_raytrace,
                event_origin=source,
                epsg=epsg,
                source_velocities=vm_at_source,
                Q=config["Q"],
                kappa=config["kappa"],
                f_highpass=lambda x: 0.3,
                lowpass_pct=config["lowpass_pct"],
                min_snr=config["qualities"][current_quality]["min_snr"],
                min_d_hypo_m=config["min_d_hypo_m"],
                max_d_hypo_m=config["max_d_hypo_m"],
                min_samples_per_bin=config["qualities"][current_quality][
                    "min_samples_per_bin"
                ],
                f_smooth_min=config["qualities"][current_quality]["f_smooth_min"]
                / f_smooth_min_adjustment,
                f_smooth_max=config["f_smooth_max"],
                f_smooth_n_bins=config["f_smooth_n_bins"],
                n_trial_drops=config["n_trial_drops"],
                radius_relationship=Madariaga,
                f_spectrum_model=brune_source_model,
            )
            break
        except FSmoothMinTooHigh:
            f_smooth_min_adjustment *= 2
        except QualityTooLowError:
            f_smooth_min_adjustment = 1
            current_quality = next_quality(current_quality, sorted_qualities)
    except NoGoodStationsError:
        source_parameters = None
        break
    except SourceParametersError:
        source_parameters = None
        break
if source_parameters is None:
    sp_results[event_id] = {
        "quality_score": "F",
        "lat": source.lat,
        "lon": source.lon,
        "depth": source.depth_m,
        "origin time": source.time,
        "enu": source.enu(epsg).tolist(),
        "ml": event.get_origin().get_ml(),
    }
    print(f"{event_id}: source paramters failed")
print(f"{event_id}: source parameters quality {current_quality}")
stress_drop, radius = average_stress_drop(source_parameters.static_stress_drops)
sp_results[event_id] = {
    "mw": float(source_parameters.mw),
    "uncertainty": float(source_parameters.mw_uncertainty),
    "p corner": float(source_parameters.fc_P)
    if source_parameters.fc_P is not None
    else None,
    "s corner": float(source_parameters.fc_S)
    if source_parameters.fc_S is not None
    else None,
    "quality_score": current_quality,
    "lat": source.lat,
    "lon": source.lon,
    "depth": source.depth_m,
    "origin time": source.time,
    "enu": source.enu(epsg).tolist(),
    "ml": event.get_origin().get_ml(),
    "stress_drop": None if stress_drop is None else float(stress_drop),
    "radius": None if radius is None else float(radius),
}
