import json
from pathlib import Path

import numpy as np
from nmxseis.interact.athena import AthenaClient
from nmxseis.model.nslc import NSL
from nmxseis.model.pick_set import PickSet
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.formulas import calculate_scalar_potency
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.source_parameters.core import SPQualityCascadeFailed
from nmxseis.numerics.source_parameters.evaluator import SParamsCascadingEvaluator
from nmxseis.numerics.velocity_model.vm_1d import VelocityModel1D, VMLayer1D
from NocMeta.Meta import NOC_META
from obspy import read_inventory, read, UTCDateTime

athena_code = "NTR_RCN"
base_dir = Path("NTR_Rocanville")
pick_dir = Path(
    r"C:\Users\adambaig\Project\ray_tracing_playground\NTR_Rocanville\athena_picks"
)
json_dir = Path(
    r"C:\Users\adambaig\Project\ray_tracing_playground\NTR_Rocanville\Oct_2022_events"
)
station_xml = base_dir.joinpath("NTR_RCN_Full.xml")
inventory = read_inventory(station_xml)
surface_nsls = [nsl for nsl in NSL.iter_inv(inventory) if "T" in nsl.sta]


athena_config = NOC_META[athena_code]
athena_client = AthenaClient(
    rf'http://{athena_config["athIP"]}', athena_config["athApi"]
)

with open(base_dir.joinpath("velocity_model.json")) as f:
    velocity_model_json = json.load(f)

source_parameters = {}
reproject = EPSGReprojector(32613)

layers = []
for layer in velocity_model_json:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )
velocity_model = VelocityModel1D(layers)


noc_sp_config = {
    "qualities": {
        "A": {"min_snr": 3, "min_samples_per_bin": 5, "f_smooth_min": 0.1},
        "B": {"min_snr": 3, "min_samples_per_bin": 3, "f_smooth_min": 0.5},
        "C": {"min_snr": 1.5, "min_samples_per_bin": 2, "f_smooth_min": 1},
    },
    "lowpass_nyq_backoff": 0.2,
    "min_d_hypo_m": 0,
    "max_d_hypo_m": 10000,
    "Q": {"P": 500, "S": 500},
    "kappa": 0,
    "f_smooth_max": 100,
    "f_smooth_n_bins": 41,
    "radius_model": "Madariaga",
    "spectrum_model": "Brune",
}


def get_nsl_from_channel_path(channel_path):
    return NSL(
        net=channel_path["networkCode"],
        sta=channel_path["stationCode"],
        loc=channel_path["locationCode"],
    )


sp_evaluator = SParamsCascadingEvaluator(velocity_model, reproject, noc_sp_config)
event_location_jsons = json_dir.glob("*.json")

for location_json in event_location_jsons:
    event_id = location_json.stem
    stream = athena_client.read_event_waveforms(event_id).rotate(
        "->ZNE", inventory=inventory
    )
    ath_source, pick_set = athena_client.get_preferred_origin_and_picks_for_event(
        event_id
    )
    with open(location_json) as f:
        location = json.load(f)
    source = EventOrigin(
        lat=location["origin"]["lat"],
        lon=location["origin"]["lon"],
        depth_m=location["origin"]["depth_m"],
        time=UTCDateTime(location["origin"]["UTC_datetime"]),
    )
    picks_file = list(pick_dir.glob(f"{event_id.zfill(10)}*.picks"))[0]
    pick_set = PickSet.load(picks_file)
    print(f"evaluating event {event_id}")

    try:
        source_parameters = sp_evaluator.evaluate(
            source, stream, inventory, pick_set.to_arrivals_dict()
        )
        print(f"finished event {event_id}")
    except SPQualityCascadeFailed:
        print(f"evaluation failed {event_id}")
        continue

    potency = calculate_scalar_potency(
        source_parameters.moment,
        velocity_model.look_up_enu(source.enu(reproject)).shear_modulus,
    )
    apparent_stress = source_parameters.radiated_energy.calc_apparent_stress(potency)
    sw_efficiency = (
        None
        if source_parameters.static_stress_drop is None
        else source_parameters.radiated_energy.calc_sw_efficiency(
            potency, source_parameters.static_stress_drop
        )
    )
    sp_results = {
        "mw": source_parameters.mw.val,
        "mw_uncertainty": source_parameters.mw.unc,
        "moment": source_parameters.moment.val,
        "moment_uncertainty": source_parameters.moment.unc,
        "potency": potency.val,
        "potency_uncertainty": potency.unc,
        "p corner": source_parameters.fc_P.val
        if source_parameters.fc_P is not None
        else None,
        "s corner": source_parameters.fc_S.val
        if source_parameters.fc_S is not None
        else None,
        "quality_score": sp_evaluator.current_quality,
        "lat": source.lat,
        "lon": source.lon,
        "depth": source.depth_m,
        "origin time": source.time.timestamp,
        "enu": source.enu(reproject).tolist(),
        "stress_drop": source_parameters.static_stress_drop.stress_drop.val
        if source_parameters.static_stress_drop is not None
        else None,
        "stress_drop_uncertainty": source_parameters.static_stress_drop.stress_drop.unc
        if source_parameters.static_stress_drop is not None
        else None,
        "radiated_energy": source_parameters.radiated_energy.total.val,
        "radiated_energy_uncertainty": source_parameters.radiated_energy.total.unc,
        "radiated_energy_p": source_parameters.radiated_energy.p_energy.val,
        "radiated_energy_s": source_parameters.radiated_energy.s_energy.val,
        "radius": source_parameters.static_stress_drop.radius.val
        if source_parameters.static_stress_drop is not None
        else None,
        "radius_uncertainty": source_parameters.static_stress_drop.radius.unc
        if source_parameters.static_stress_drop is not None
        else None,
        "apparent_stress": apparent_stress.val,
        "apparent_stress_uncertainty": apparent_stress.unc,
        "sw_efficiency": sw_efficiency.val
        if source_parameters.static_stress_drop is not None
        else None,
        "sw_efficiency_uncertainty": sw_efficiency.unc
        if source_parameters.static_stress_drop is not None
        else None,
    }

    with open(
        base_dir.joinpath(
            "source_param_jsons",
            f"sp_results_{event_id.zfill(6)}.json",
        ),
        "w",
    ) as f:
        json.dump(sp_results, f)
