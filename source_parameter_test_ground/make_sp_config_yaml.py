import yaml

noc_sp_config = {
			"qualities": {
				"A": {"min_snr": 3, "min_samples_per_bin": 5, "f_smooth_min": 0.1},
				"B": {"min_snr": 3, "min_samples_per_bin": 3, "f_smooth_min": 0.5},
				"C": {"min_snr": 1.5, "min_samples_per_bin": 2, "f_smooth_min": 1},
			},
			"lowpass_nyq_backoff": 0.2,
			"min_d_hypo_m": 0,
			"max_d_hypo_m": 50000,
			"Q": {"P": 500, "S": 500},
			"kappa": 0.0,
			"f_smooth_max": 100,
			"f_smooth_n_bins": 41,
			"radius_model": "Madariaga",
			"spectrum_model": "Brune",
			"f_highpass": "static",
		}


with open('ntr_rcn_staging_spconfig.yaml', 'w') as f:
	yaml.dump(noc_sp_config, f)