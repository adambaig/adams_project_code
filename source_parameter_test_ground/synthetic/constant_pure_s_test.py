from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import romb
from obspy import Stream, UTCDateTime
from numpy.random import seed
from numpy.fft import fft, fftfreq

from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.formulas import mw_to_moment
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.moment_tensor.inversion import inversion_matrix_row
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.simulation.event_waveform import (
    simulate_waveforms_for_array,
    simulate_waveform_from_source,
)
from nmxseis.numerics.simulation import generate_random_dc_moment_tensor
from nmxseis.numerics.source_parameters import (
    get_signal_and_noise_spectra,
    get_signal_and_noise_windows,
    _rotate_stream_to_p_rays,
)
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D
from realtime_source_parameters.estk_helpers import fourier_spectra

StationArrivals = dict[NSL, dict[Phase, UTCDateTime]]

# seed(443)

synth_mt = MomentTensor.from_vector(np.array([0, 0, 0, 1, 0, 0]))

layer = VMLayer1D(rho=2500, vp=5000, vs=3000)
velocity_model = VelocityModel1D([layer])
f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, velocity_model, phase
)

event_enu = np.array([0, 0, 0])

corner_frequnencies = {
    Phase.P: 20,
    Phase.S: 20,
}

mag = -1
DEFAULT_S_WIN_PRE = 0.2
DEFAULT_P_WIN_PRE = 0.1
percents = []
receiver_locations_enu = []
theory, romberg, median_level = [], [], []
max_trace, distance = [], []
distances = np.arange(10, 1001, 10)
theory, romberg, median_level = (
    np.zeros(len(distances)),
    np.zeros(len(distances)),
    np.zeros(len(distances)),
)
for i_dist, distance in enumerate(distances):
    receiver_enu = event_enu + np.array([0, distance, 0])
    p_ray = f_raytrace(event_enu, receiver_enu, Phase.P)
    s_ray = f_raytrace(event_enu, receiver_enu, Phase.S)

    waveforms = simulate_waveform_from_source(
        synth_mt,
        mag,
        corner_frequnencies,
        p_ray,
        s_ray,
        event_enu,
        receiver_enu,
        nt=8000,
        dt=0.001,
        Qp=np.inf,
        Qs=np.inf,
        noise_rms=1e-15,
        units="D",
    )
    accel = waveforms.copy().differentiate().differentiate()
    disp_trace = waveforms.select(channel="*E")[0]
    plot_trace = accel.select(channel="*E")[0]
    null_nsl = NSL.from_trace(accel[0])
    arrivals = {
        null_nsl: {Phase.P: p_ray.traveltime_sec, Phase.S: s_ray.traveltime_sec}
    }
    rotated_stream = p_ray.rotate_waveform(accel)
    signal_windows, noise_windows = get_signal_and_noise_windows(
        arrivals,
        event_enu,
        {null_nsl: receiver_enu},
        f_raytrace,
        p_win_pre=DEFAULT_P_WIN_PRE,
        s_win_pre=DEFAULT_S_WIN_PRE,
    )
    spectra, noise_spectra = get_signal_and_noise_spectra(
        rotated_stream, signal_windows, noise_windows, *(1, 1000, 31)
    )

    sv_theory_coeff = inversion_matrix_row(s_ray, Phase.SV) @ synth_mt.vec
    sh_theory_coeff = inversion_matrix_row(s_ray, Phase.SH) @ synth_mt.vec
    theory[i_dist] = (
        np.hypot(sv_theory_coeff, sh_theory_coeff)
        * mw_to_moment(mag)
        / layer.rho
        / layer.vs**3
        / s_ray.geometrical_spreading
        / 4
        / np.pi
    )
    romberg[i_dist] = romb(disp_trace.data[:513], 0.001)
    median_level[i_dist] = np.median(spectra[null_nsl, Phase.S].dis[:15])


s_spectrum = spectra[null_nsl, Phase.S]
fig, ax = plt.subplots()
ax.loglog(s_spectrum.freqs, s_spectrum.dis)


fig, (ax1, ax2) = plt.subplots(2, sharex=True, figsize=[5, 10])
ax1.plot(distances, (theory - median_level) / theory)
ax2.plot(distances, (theory - romberg) / theory)
ax2.set_xlabel("distance (m)")
ax1.set_ylabel("relative disagreement for plateau")
ax2.set_ylabel("realtive disagreement for integration")
fig.savefig("comparison_plateau_estimates.png", bbox_inches="tight")

fig_comp = plt.figure(figsize=[8, 8])
ax_trace = fig_comp.add_axes([0.1, 0.84, 0.8, 0.1])
ax_spec = fig_comp.add_axes([0.1, 0.1, 0.8, 0.65])
ax_trace.plot(
    (time_axis := disp_trace.times()), 1e9 * (trace_data := disp_trace.data), "0.1"
)
ylim = ax_trace.get_ylim()
signal_window_patch = plt.Rectangle(
    xy=((win := signal_windows[null_nsl, Phase.S])[0], ylim[0]),
    width=np.diff(win),
    height=np.diff(ylim),
    facecolor="lightblue",
    alpha=0.7,
)
ax_trace.add_patch(signal_window_patch)
ax_spec.loglog(s_spectrum.freqs, s_spectrum.dis)
ax_spec.loglog(s_spectrum.freqs, theory[-1] * (flat := np.ones(len(s_spectrum.freqs))))
ax_spec.loglog(s_spectrum.freqs, romberg[-1] * flat)
ax_spec.loglog(s_spectrum.freqs, median_level[-1] * flat)
i_win = [np.argmin(abs(time_axis - w)) for w in win]
windowed_trace = disp_trace.copy().trim(
    starttime=UTCDateTime(time_axis[i_win[0]]), endtime=UTCDateTime(time_axis[i_win[1]])
)
estk_disp, estk_freq, _, _ = fourier_spectra(windowed_trace, 2, 0.01, 0, 3, 0.05)

# ax_spec.loglog(estk_freq[0], estk_disp[0])
ax_spec.legend(
    [
        "smoothed spectrum",
        "theoretical plateau (under Romberg integration)",
        "Romberg integration",
        "crude plateau measurement",
        # "ESTK spectral smoothing",
    ]
)
ax_spec.set_xlabel("frequency (Hz)")
ax_spec.set_ylabel("displacement spectrum (m$\cdot$s)")
ax_trace.set_xlabel("time (s)")
ax_trace.set_ylabel("disp (nm)")

fig_comp.savefig("spectra_comparison.png")
windowed_trace.plot()
