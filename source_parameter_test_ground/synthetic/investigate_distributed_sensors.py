from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
from obspy import Stream, UTCDateTime
from numpy.random import seed

# from nmxseis.numerics.modeling import

from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.formulas import mw_to_moment, moment_to_mw
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.moment_tensor.inversion import inversion_matrix_row
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.simulation.event_waveform import (
    simulate_waveforms_for_array,
    simulate_waveform_from_source,
)
from nmxseis.numerics.simulation import generate_random_dc_moment_tensor
from nmxseis.numerics.source_parameters import (
    get_signal_and_noise_spectra,
    get_signal_and_noise_windows,
    _rotate_stream_to_p_rays,
)
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D


StationArrivals = dict[NSL, dict[Phase, UTCDateTime]]

seed(443)
synth_mt = generate_random_dc_moment_tensor()

layer = VMLayer1D(rho=2500, vp=5000, vs=3000)
velocity_model = VelocityModel1D([layer])
f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, velocity_model, phase
)

event_enu = np.array([0, 0, 0])
n_samp = 100000
mag = -1
p_samples = np.zeros(n_samp)
s_samples = np.zeros(n_samp)
p_coeffs = np.zeros(n_samp)
s_coeffs = np.zeros(n_samp)
receiver_locations_enu = 1000 * np.random.randn(3 * n_samp).reshape([n_samp, 3])
for ii in range(n_samp):
    receiver_enu = receiver_locations_enu[ii, :]
    p_ray = f_raytrace(event_enu, receiver_enu, Phase.P)
    s_ray = f_raytrace(event_enu, receiver_enu, Phase.S)
    sv_theory_coeff = inversion_matrix_row(s_ray, Phase.SV) @ synth_mt.vec
    sh_theory_coeff = inversion_matrix_row(s_ray, Phase.SH) @ synth_mt.vec
    p_theory_coeef = inversion_matrix_row(p_ray, Phase.P) @ synth_mt.vec
    theory_s = np.hypot(sv_theory_coeff, sh_theory_coeff) * mw_to_moment(mag)
    theory_p = p_theory_coeef * mw_to_moment(mag)
    p_samples[ii] = theory_p
    s_samples[ii] = theory_s
    p_coeffs[ii] = p_theory_coeef
    s_coeffs[ii] = np.hypot(sh_theory_coeff, sv_theory_coeff)

moment_to_mw(np.sqrt(np.mean(s_samples**2)) / np.sqrt(6 / 15))

moment_to_mw(np.sqrt(np.mean(p_samples**2)) / np.sqrt(4 / 15))


np.sqrt(np.mean(p_coeffs**2)) / np.sqrt(4 / 15)
np.sqrt(np.mean(s_coeffs**2)) / np.sqrt(6 / 15)
