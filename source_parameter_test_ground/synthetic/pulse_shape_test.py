import matplotlib.pyplot as plt
import numpy as np
from numpy import pi as π
from numpy import array
from obspy import Trace, Stream, UTCDateTime
from obspy.core.trace import Stats
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.simulation.event_waveform import _calc_pulse

from realtime_source_parameters.estk_helpers import fourier_spectra


def createTraceStats(n1, o1, d1, component):
    # make a stats file for a seed trace
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = "00"
    stat.location = component["location"]
    return stat


out_trace = Stream()


time_series = np.arange(0, 8.0, 0.00025)

test_corner_freq = 10


def unit_brune_spectrum(frequencies, corner_frequency, output="D"):
    frequencies = array(frequencies)
    base_amp_spectrum = 1.0 / (1 + (frequencies / corner_frequency) ** 2)
    unit_transform = {"D": 1, "V": 2 * π * frequencies, "A": (2 * π * frequencies) ** 2}
    amp_spectrum = unit_transform[output] * base_amp_spectrum
    return amp_spectrum


for n_spectral_samples in [12, 31, 62]:
    for initiation_time in [3.9, 4.1, 4.4, 5.4]:
        pulse_1 = _calc_pulse(
            time_series[1],
            len(time_series),
            initiation_time,
            np.inf,
            test_corner_freq,
            "A",
        )
        tr_1 = Trace(
            data=pulse_1,
            header=createTraceStats(
                len(time_series),
                UTCDateTime.now(),
                time_series[1] - time_series[0],
                {
                    "component": "Z",
                    "channel": "HHZ",
                    "station": "FAKE",
                    "location": "00",
                },
            ),
        )

        spectra = MotionSpectrum.from_acc_trace(tr_1).smooth(
            1, 1000, n_spectral_samples
        )
        fig, ax = plt.subplots()
        ax.loglog(spectra.freqs, spectra.acc)
        ax.loglog(
            spectra.freqs, unit_brune_spectrum(spectra.freqs, test_corner_freq, "A")
        )
        ax.loglog(
            spectra.freqs,
            abs(
                spectra.acc - unit_brune_spectrum(spectra.freqs, test_corner_freq, "A")
            ),
        )
        ax.set_xlabel("frequency (Hz)")
        ax.set_ylabel("acc spec (m.s)")
        ax.legend(["MotionSpectrum", "theory", "|difference|"])
