from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
from obspy import Stream, UTCDateTime
from numpy.random import seed

# from nmxseis.numerics.modeling import

from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.formulas import mw_to_moment
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.moment_tensor.inversion import inversion_matrix_row
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.simulation.event_waveform import (
    simulate_waveforms_for_array,
    simulate_waveform_from_source,
)
from nmxseis.numerics.simulation import generate_random_dc_moment_tensor
from nmxseis.numerics.source_parameters import (
    get_signal_and_noise_spectra,
    get_signal_and_noise_windows,
    _rotate_stream_to_p_rays,
)
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D


StationArrivals = dict[NSL, dict[Phase, UTCDateTime]]

seed(443)
synth_mt = generate_random_dc_moment_tensor()

explosion = MomentTensor.from_vector(np.array([1, 1, 1, 0, 0, 0]))


layer = VMLayer1D(rho=2500, vp=5000, vs=3000)
velocity_model = VelocityModel1D([layer])
f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, velocity_model, phase
)

event_enu = np.array([0, 0, 0])

corner_frequnencies = {
    Phase.P: 200,
    Phase.S: 200,
}

mag = -1
DEFAULT_S_WIN_PRE = 0.2
DEFAULT_P_WIN_PRE = 0.1
percents = []
receiver_locations_enu = []
for ii in range(1000):
    if ii % 100 == 0:
        print(ii)
    receiver_enu = event_enu + 1000 * np.random.normal(size=3)
    p_ray = f_raytrace(event_enu, receiver_enu, Phase.P)
    s_ray = f_raytrace(event_enu, receiver_enu, Phase.S)

    waveforms = simulate_waveform_from_source(
        synth_mt,
        mag,
        corner_frequnencies,
        p_ray,
        s_ray,
        event_enu,
        receiver_enu,
        nt=16000,
        dt=0.0005,
        Qp=np.inf,
        Qs=np.inf,
        noise_rms=1e-15,
        units="D",
    )
    waveforms.differentiate().differentiate()
    null_nsl = NSL.from_trace(waveforms[0])
    arrivals = {
        null_nsl: {Phase.P: p_ray.traveltime_sec, Phase.S: s_ray.traveltime_sec}
    }
    rotated_stream = p_ray.rotate_waveform(waveforms)

    signal_windows, noise_windows = get_signal_and_noise_windows(
        arrivals,
        event_enu,
        {null_nsl: receiver_enu},
        f_raytrace,
        p_win_pre=DEFAULT_P_WIN_PRE,
        s_win_pre=DEFAULT_S_WIN_PRE,
        s_win_post=2,
    )
    spectra, noise_spectra = get_signal_and_noise_spectra(
        rotated_stream, signal_windows, noise_windows, *(1, 1000, 31)
    )

    sv_theory_coeff = inversion_matrix_row(s_ray, Phase.SV) @ synth_mt.vec
    sh_theory_coeff = inversion_matrix_row(s_ray, Phase.SH) @ synth_mt.vec
    p_theory_coeef = inversion_matrix_row(p_ray, Phase.P) @ synth_mt.vec
    theory_s = (
        np.hypot(sv_theory_coeff, sh_theory_coeff)
        * mw_to_moment(mag)
        / layer.rho
        / layer.vs**3
        / s_ray.geometrical_spreading
        / 4
        / np.pi
    )
    theory_p = (
        p_theory_coeef
        * mw_to_moment(mag)
        / layer.rho
        / layer.vp**3
        / p_ray.geometrical_spreading
        / 4
        / np.pi
    )
    median_level_s = np.median(spectra[null_nsl, Phase.S].dis[:15])
    median_level_p = np.median(spectra[null_nsl, Phase.P].dis[:15])
    percent = 100 * abs(theory_s - median_level_s) / theory_s
    percents.append(percent)
    receiver_locations_enu.append(receiver_enu)
    # fig, ax = plt.subplots()
    # ax.loglog(spectra[null_nsl, Phase.P].freqs, spectra[null_nsl, Phase.P].dis)
    #
    # ax.loglog(spectra[null_nsl, Phase.P].freqs[:15], median_level_p * np.ones(15), color="k")
    # ax.loglog(spectra[null_nsl, Phase.P].freqs[:15], abs(theory_p * np.ones(15)), color="r")
    # ax.set_title(f"P agree within {percent:.1f}%")
    # ax.text(
    #     0.1,
    #     0.1,
    #     f"recever at e: {receiver_enu[0]:.0f}, n: {receiver_enu[1]:.0f}, u: {receiver_enu[2]:.0f}",
    #     transform=ax.transAxes,
    # )

rotated_stream.copy().trim(
    starttime=UTCDateTime((win := signal_windows[null_nsl, Phase.P])[0]),
    endtime=UTCDateTime(win[1]),
).plot()


def takeoff_angle(enu):
    e_coord, n_coord, u_coord = enu
    return 180 * np.arctan(u_coord / np.hypot(e_coord, n_coord)) / np.pi


def total_distance(enu):
    e_coord, n_coord, u_coord = enu
    return np.hypot(np.hypot(e_coord, n_coord), u_coord)


fig, ax = plt.subplots()
ax.plot([r[2] for r in receiver_locations_enu], percents, ".")
ax.set_xlabel("vertical distance (m)")
ax.set_ylabel("percent misfit")


fig, ax = plt.subplots()
ax.plot([takeoff_angle(r) for r in receiver_locations_enu], percents, ".")
ax.set_xlabel("takeoff angle")
ax.set_ylabel("percent misfit")

fig, ax = plt.subplots()
ax.plot([total_distance(r) for r in receiver_locations_enu], percents, ".")
ax.set_xlabel("total distance (m)")
ax.set_ylabel("percent misfit")
fig.savefig("synthetic//S_wave_plateau_misfit.png")
