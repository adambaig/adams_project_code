from collections import defaultdict
import json

import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from numpy import array
import numpy as np
from obspy import read_inventory, UTCDateTime
import pyproj

from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.formulas import (
    moment_to_mw,
    mw_to_moment,
    calculate_source_radius_from_stress_drop,
    radius_corner_frequency_madariaga,
    convert_radius_corner_brune,
)
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.moment_tensor.inversion import inversion_matrix_row
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.simulation.event_waveform import (
    simulate_waveforms_for_array,
    simulate_waveform_from_source,
)
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

from nmxseis.numerics.source_parameters import compute_source_parameters
from nmxseis.numerics.source_parameters import (
    get_signal_and_noise_windows,
    get_signal_and_noise_spectra,
    _rotate_stream_to_p_rays,
    _correct_for_kappa,
    _correct_for_anelastic_attenuation,
    _correct_for_geom_spreading,
    _StationInfo,
)
from nmxseis.numerics.simulation import generate_random_dc_moment_tensor

DEFAULT_S_WIN_PRE = 0.2
DEFAULT_P_WIN_PRE = 0.1

TAB10 = get_cmap("tab10")

inv = read_inventory("synthetic\\synthetic_Paramount_2_28_example\\synthetic_sta.xml")
reproject = pyproj.Proj("epsg:26911")
event = SeismicEvent(
    depth_m=2100,
    origin_time=UTCDateTime(1637615497.695214),
    lat=54.401317823038646,
    lon=-116.93805538849605,
    reproject=reproject,
)
stations = {}
for network in inv:
    for station in network:
        stations[f"{network.code}.{station.code}.00"] = {
            "latitude": station.latitude,
            "longitude": station.longitude,
            "elevation": station.elevation,
        }

eastings, northings = reproject(
    *zip(*[(v["longitude"], v["latitude"]) for v in stations.values()])
)
for i_station, station in enumerate(stations.values()):
    station["enu"] = np.array(
        [eastings[i_station], northings[i_station], station["elevation"]]
    )

event_location = {
    "depth": 2.1,
    "time": 1637615497.695214,
    "latitude": 54.401317823038646,
    "longitude": -116.93805538849605,
}
easting, northing = reproject(event_location["longitude"], event_location["latitude"])
event_enu = [easting, northing, -1000 * event_location["depth"]]

layer = VMLayer1D(rho=2500, vp=5000, vs=3000)
velocity_model = VelocityModel1D([layer])
easting, northing = reproject(event_location["longitude"], event_location["latitude"])
event_enu = array([easting, northing, -1000 * event_location["depth"]])
mag = 2
stress_drop = 1e6
moment = mw_to_moment(mag)
source_radius = calculate_source_radius_from_stress_drop(moment, stress_drop)
corner_frequnencies = {
    Phase.P: convert_radius_corner_brune(source_radius, layer.vp),
    Phase.S: convert_radius_corner_brune(source_radius, layer.vs),
}


f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, velocity_model, phase
)
f_highpass = lambda x: 1

min_snr = {"A": 5, "B": 3, "C": 1.5}
min_samples_per_bin = {"A": 5, "B": 3, "C": 1}

random_dc_mt = generate_random_dc_moment_tensor()
out_stream, rays = simulate_waveforms_for_array(
    *(
        random_dc_mt,
        mag,
        corner_frequnencies,
        event_enu,
        inv,
        velocity_model,
        reproject,
    ),
    nt=16000,
    dt=0.00025,
    Qp=1000,
    Qs=1000,
    noise_rms=1e-12,
    ch_prefix="CP",
    units="V",
    ev_origin_time=event.origin_time,
)
nsls = [NSL.from_trace(tr) for tr in out_stream.select(channel="*Z")]

# free surface amp
for tr in out_stream:
    tr.data = 2 * tr.data
arrivals = defaultdict(dict)
for nsl in nsls:
    for phase in [Phase.P, Phase.S]:
        ray = f_raytrace(event_enu, stations[nsl.__str__()]["enu"], phase)
        arrivals[nsl][phase] = UTCDateTime(
            ray.traveltime_sec + event.origin_time.timestamp
        )

for quality in ["A", "B", "C"]:
    source_parameters = compute_source_parameters(
        stream=out_stream,
        nsls=[NSL.from_trace(tr) for tr in out_stream],
        inventory=inv,
        arrivals=arrivals,
        f_raytrace=f_raytrace,
        event=event,
        ev_vs=layer.vs,
        ev_vp=layer.vp,
        Q=1000,
        kappa=0,
        f_highpass=f_highpass,
        lowpass_pct=20,
        min_snr=min_snr[quality],
        min_d_hypo_m=0,
        max_d_hypo_m=1000000,
        min_samples_per_bin=min_samples_per_bin[quality],
        f_smooth_min=1,
        f_smooth_max=1000,
        f_smooth_n_bins=31,
        n_trial_drops=100,
    )
    if source_parameters is not None:
        mw_out = source_parameters.mw
        mw_unc = source_parameters.mw_uncertainty
        cf_p_out = source_parameters.fc_P
        cf_s_out = source_parameters.fc_S
        quality_score = quality
        break


station_enus = {
    NSL(net=(ks := k.split("."))[0], sta=ks[1], loc=ks[2]): v["enu"]
    for k, v in stations.items()
}
# out_stream.remove_response(inventory=inv, output="ACC").detrend(type="linear")
rotated_traces = _rotate_stream_to_p_rays(
    out_stream, {nsl: ray for (nsl, ph), ray in rays.items() if ph.is_P}, inv
)
signal_windows, noise_windows = get_signal_and_noise_windows(
    *(arrivals, event_enu, station_enus, f_raytrace),
    p_win_pre=DEFAULT_P_WIN_PRE,
    s_win_pre=DEFAULT_S_WIN_PRE,
)
spectra, noise_spectra = get_signal_and_noise_spectra(
    rotated_traces, signal_windows, noise_windows, *(1, 1000, 31)
)


def get_station_number_color(station_number):
    nsl_number = (float(station_number) - 0.5) % 10
    return TAB10((nsl_number) / 10)


fig, (ax_p, ax_s) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=[12, 8])
ax_p.set_ylabel("displacement spectrum (m$\cdot$s)")
for ax, phase in zip([ax_p, ax_s], [Phase.P, Phase.S]):
    ax.set_xlabel("frequency (Hz)")
    for nsl in nsls:
        ax.loglog(
            spectra[nsl, phase].freqs,
            spectra[nsl, phase].dis,
            color=get_station_number_color(nsl.sta[1]),
        )
        raypath = f_raytrace(event_enu, station_enus[nsl], phase)
        if phase.is_P:
            plateau = abs(
                moment
                * np.dot(random_dc_mt._vector, inversion_matrix_row(raypath, Phase.P))
            ) / (4 * np.pi * raypath.geometrical_spreading * layer.vp**3 * layer.rho)
        else:
            sv_plateau = abs(
                moment
                * np.dot(random_dc_mt._vector, inversion_matrix_row(raypath, Phase.SV))
            )
            sh_plateau = abs(
                moment
                * np.dot(random_dc_mt._vector, inversion_matrix_row(raypath, Phase.SH))
            )
            plateau = np.hypot(sv_plateau, sh_plateau) / (
                4 * np.pi * raypath.geometrical_spreading * layer.vs**3 * layer.rho
            )
        ax.loglog(
            [1, 10],
            [2 * plateau, 2 * plateau],
            color=get_station_number_color(nsl.sta[1]),
        )

fig.savefig("displacement_spectra.png")


#
# fig, (ax_p, ax_s) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=[12, 8])
# ax_p.set_ylabel("displacement spectrum (m$\cdot$s)")
# for ax, phase in zip([ax_p, ax_s], [Phase.P, Phase.S]):
#     ax.set_xlabel("frequency (Hz)")


fig, (ax_p, ax_s) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=[12, 8])
for ax in [ax_s, ax_p]:
    ax.set_xlabel("frequency (Hz)")
ax_p.set_ylabel("source spectrum (N$\cdot$m$\cdot$s)")
for (nsl, phase), spec in source_parameters.corrected_individual_spectra.items():
    if phase.is_P:
        ax_p.loglog(spec.freqs, spec.dis, color=get_station_number_color(nsl.sta))
    else:
        ax_s.loglog(spec.freqs, spec.dis, color=get_station_number_color(nsl.sta))

p_plateaus, s_plateaus = [], []
for station_name, station in stations.items():
    p_raypath = f_raytrace(event_enu, station["enu"], Phase.P)
    p_plateau = abs(
        moment * np.dot(random_dc_mt._vector, inversion_matrix_row(p_raypath, Phase.P))
    )
    ax_p.loglog(
        [1, 10],
        [p_plateau, p_plateau],
        color=get_station_number_color(station_name.split(".")[1]),
    )
    s_raypath = f_raytrace(event_enu, station["enu"], Phase.S)
    sv_plateau = abs(
        moment * np.dot(random_dc_mt._vector, inversion_matrix_row(s_raypath, Phase.SV))
    )
    sh_plateau = abs(
        moment * np.dot(random_dc_mt._vector, inversion_matrix_row(s_raypath, Phase.SH))
    )
    s_plateau = np.hypot(sv_plateau, sh_plateau)
    ax_s.loglog(
        [1, 10],
        [s_plateau, s_plateau],
        color=get_station_number_color(station_name.split(".")[1]),
    )
    p_plateaus.append(p_plateau)
    s_plateaus.append(s_plateau)


fig.savefig("source_spectra.png")


sta_infos = {}
for nsl, st_enu in station_enus.items():
    hypo = np.linalg.norm(st_enu - event.enu)
    nsl_stream = nsl.select_from_stream(out_stream)
    sta_infos[nsl] = _StationInfo(
        nsl=nsl,
        enu=st_enu,
        highpass=f_highpass(float(hypo)),
        lowpass=(1.0 - 20 / 100.0) * nsl_stream[0].stats.sampling_rate / 2,
        is_on_surface=inv.select(**nsl.as_obspy_dict())
        .select(starttime=event.origin_time, channel="*Z")[0][0][0]
        .depth
        < 1,
        ev_hyp=hypo,
    )

gs_corrected = _correct_for_geom_spreading(spectra, rays, sta_infos)

fig, (ax_p, ax_s) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=[12, 8])
for ax in [ax_s, ax_p]:
    ax.set_xlabel("frequency (Hz)")
ax_p.set_ylabel("source spectrum (N$\cdot$m$\cdot$s)")
for (nsl, phase), spec in gs_corrected.items():
    if phase.is_P:
        ax_p.loglog(spec.freqs, spec.dis, color=get_station_number_color(nsl.sta))
    else:
        ax_s.loglog(spec.freqs, spec.dis, color=get_station_number_color(nsl.sta))

p_plateaus, s_plateaus = [], []
for station_name, station in stations.items():
    p_raypath = f_raytrace(event_enu, station["enu"], Phase.P)
    p_plateau = abs(
        moment * np.dot(random_dc_mt._vector, inversion_matrix_row(p_raypath, Phase.P))
    )
    ax_p.loglog(
        [1, 10],
        [p_plateau, p_plateau],
        color=get_station_number_color(station_name.split(".")[1]),
    )
    s_raypath = f_raytrace(event_enu, station["enu"], Phase.S)
    sv_plateau = abs(
        moment * np.dot(random_dc_mt._vector, inversion_matrix_row(s_raypath, Phase.SV))
    )
    sh_plateau = abs(
        moment * np.dot(random_dc_mt._vector, inversion_matrix_row(s_raypath, Phase.SH))
    )
    s_plateau = np.hypot(sv_plateau, sh_plateau)
    ax_s.loglog(
        [1, 10],
        [s_plateau, s_plateau],
        color=get_station_number_color(station_name.split(".")[1]),
    )
    p_plateaus.append(p_plateau)
    s_plateaus.append(s_plateau)

2 * gs_corrected[nsl, Phase.P].acc / spectra[nsl, Phase.P].acc / (
    4 * np.pi * layer.vp**3 * layer.rho * rays[nsl, Phase.S].geometrical_spreading
)

moment_to_mw(np.sqrt(np.ma.mean(np.array(p_plateaus) ** 2)) / Phase.P.radiation_pattern)

moment_to_mw(np.sqrt(sum(s * s for s in s_plateaus) / 15) / np.sqrt(2 / 5))
