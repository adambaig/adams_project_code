from collections import defaultdict


import matplotlib.pyplot as plt
from numpy import array, ones
from numpy import pi as π
from numpy.ma import median

from nmxseis.model.phase import Phase
from nmxseis.numerics.formulas import mw_to_moment
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.simulation.event_waveform import (
    simulate_waveform_from_source,
)
from nmxseis.numerics.velocity_model.vm_1d import Ray1D, VelocityModel1D, VMLayer1D


p_ray: Ray1D
s_ray: Ray1D
sv_moment_tensor: MomentTensor
sh_moment_tensor: MomentTensor
motion_spectrum: dict[str, MotionSpectrum]
theory_plateau: float

SOURCE_ENU = array([0, 0, 0])
RECEIVER_ENU = array([0, 1000, 0])
LAYER = VMLayer1D(rho=2500, vp=5000, vs=3000)
VELOCITY_MODEL = VelocityModel1D([LAYER])
CORNER_FREQUENCIES = {Phase.P: 200, Phase.S: 200}
MW = -1

sv_moment_tensor = MomentTensor.from_vector(array([0, 0, 0, 0, 0, 1]))
sh_moment_tensor = MomentTensor.from_vector(array([0, 0, 0, 1, 0, 0]))
p_ray = VELOCITY_MODEL.raytrace(SOURCE_ENU, RECEIVER_ENU, Phase.P)
s_ray = VELOCITY_MODEL.raytrace(SOURCE_ENU, RECEIVER_ENU, Phase.S)
motion_spectrum = defaultdict()
smooth_spectrum = defaultdict()

for moment_tensor, channel, comp in zip(
    [sv_moment_tensor, sh_moment_tensor],
    ["CHZ", "CHE"],
    ["SV", "SH"],
):
    waveforms = simulate_waveform_from_source(
        moment_tensor,
        MW,
        CORNER_FREQUENCIES,
        p_ray,
        s_ray,
        nt=8000,
        dt=0.00025,
        Qp=1000,
        Qs=1000,
        noise_rms=1e-15,
        units="V",
    )
    motion_spectrum[comp] = MotionSpectrum.from_vel_trace(
        waveforms.select(channel=channel)[0]
    )
    smooth_spectrum[comp] = MotionSpectrum.from_vel_trace(
        waveforms.select(channel=channel)[0]
    ).smooth(1, 1000, 31)
    # waveforms.plot()

theory = (
    mw_to_moment(MW) / LAYER.rho / LAYER.vs**3 / s_ray.geometrical_spreading / 4 / π
)
median_level = defaultdict(dict)
median_level["SV"] = median(smooth_spectrum["SV"].dis[:15])
median_level["SH"] = median(smooth_spectrum["SH"].dis[:15])

for comp in ["SV", "SH"]:
    percent = 100 * abs(theory - median_level[comp]) / theory
    fig, ax = plt.subplots()
    ax.loglog(motion_spectrum[comp].freqs[:-1], abs(motion_spectrum[comp].dis)[:-1]/4000)
    ax.loglog(smooth_spectrum[comp].freqs, smooth_spectrum[comp].dis)
    ax.loglog(
        smooth_spectrum[comp].freqs[:15], median_level[comp] *ones(15), color="k"
    )
    ax.loglog(smooth_spectrum[comp].freqs[:15], abs(theory * ones(15)), color="r")
    ax.set_title(f"pure {comp} agree within {percent:.1f}%")
    fig.savefig(f"pure_{comp}_test.png")

plt.show()