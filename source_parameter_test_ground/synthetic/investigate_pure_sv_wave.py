from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fft, ifft, fftfreq
from obspy import UTCDateTime, read_inventory
import pyproj

# from nmxseis.numerics.modeling import

from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.formulas import mw_to_moment
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.moment_tensor.inversion import inversion_matrix_row
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.simulation.event_waveform import (
    simulate_waveforms_for_array,
    simulate_waveform_from_source,
)
from nmxseis.numerics.source_parameters import compute_source_parameters
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

inv = read_inventory("synthetic_Paramount_2_28_example\\synthetic_sta_plus_one.xml")

list(NSL.iter_inv(inv))[-1].sta_info[0].depth


reproject = pyproj.Proj("epsg:26911")
event = SeismicEvent(
    depth_m=-1000,
    origin_time=UTCDateTime(1637615497.695214),
    lat=54.01,
    lon=-117,
    reproject=reproject,
)

event.enu

synth_mt = MomentTensor.from_vector(np.array([0, 0, 0, 0, 0, 1]))

layer = VMLayer1D(rho=2500, vp=5000, vs=3000)
velocity_model = VelocityModel1D([layer])
f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, velocity_model, phase
)


receiver_enu = event.enu + np.array([0, 1000, 0])
p_ray = f_raytrace(event.enu, receiver_enu, Phase.P)
s_ray = f_raytrace(event.enu, receiver_enu, Phase.S)

corner_frequnencies = {
    Phase.P: 200,
    Phase.S: 200,
}

mag = -1
waveforms = simulate_waveform_from_source(
    synth_mt,
    mag,
    corner_frequnencies,
    p_ray,
    s_ray,
    event.enu,
    receiver_enu,
    nt=8000,
    dt=0.00025,
    Qp=1000,
    Qs=1000,
    noise_rms=1e-15,
    units="V",
)

s_time = s_ray.traveltime_sec
ps_separation = p_ray.traveltime_sec - s_time
waveforms.plot()
sv_stream = waveforms.select(channel="*Z")[0]

nt = 4096
sv_waveform = waveforms.select(channel="*Z")[0].data[1900 : 1900 + nt]
dt = waveforms[0].stats.delta
freq = fftfreq(nt, dt)
row = inversion_matrix_row(s_ray, Phase.SV)


sv_motion_spectrum = MotionSpectrum.from_vel_trace(sh_stream).smooth(1, 1000, 31)


median_level = np.median(sh_motion_spectrum._dis[:15])

theory = (
    mw_to_moment(mag)
    / layer.rho
    / layer.vs**3
    / s_ray.geometrical_spreading
    / 4
    / np.pi
)

abs(theory - median_level) / theory

fig, ax = plt.subplots()
ax.loglog(sv_motion_spectrum.freqs, sv_motion_spectrum._dis)
ax.loglog(sv_motion_spectrum.freqs[:15], median_level * np.ones(15), color="k")
ax.loglog(sv_motion_spectrum.freqs[:15], abs(theory * np.ones(15)), color="r")
fig.savefig("SV_test.png")
