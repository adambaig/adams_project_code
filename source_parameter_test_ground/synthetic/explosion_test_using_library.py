from collections import defaultdict
import json

from numpy import array
import numpy as np
from obspy import read_inventory, UTCDateTime
import pyproj


import matplotlib.pyplot as plt


from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.numerics.formulas import moment_to_mw
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.simulation.event_waveform import (
    simulate_waveforms_for_array,
    simulate_waveform_from_source,
)
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

from nmxseis.numerics.source_parameters import _rotate_stream_to_p_rays

INV = read_inventory("synthetic\\synthetic_Paramount_2_28_example\\synthetic_sta.xml")
EXPLOSION = MomentTensor.from_vector(array([1, 1, 1, 0, 0, 0]))
EVENT_LOCATION = {
    "depth": 2.1,
    "time": 1637615497.695214,
    "latitude": 54.401317823038646,
    "longitude": -116.93805538849605,
}
MW = 0
DT = 0.00025
CORNER_FREQUENCIES = {Phase.P: 100, Phase.S: 20}
VELOCITY_MODEL = VelocityModel1D([VMLayer1D(rho=2500, vp=5000, vs=3000)])

# INV = read_inventory(os.path.join(INV_DIR, "synthetic_sta.xml"))
reproject = pyproj.Proj("epsg:26911")
easting_loc, northing_loc = reproject(
    EVENT_LOCATION["longitude"], EVENT_LOCATION["latitude"]
)
event_enu = array([easting_loc, northing_loc, -1000 * EVENT_LOCATION["depth"]])

stream, rays = simulate_waveforms_for_array(
    *(EXPLOSION, MW, CORNER_FREQUENCIES, event_enu, INV, VELOCITY_MODEL, reproject),
    nt=16000,
    dt=DT,
    Qp=np.inf,
    Qs=np.inf,
    noise_rms=0,
    ch_prefix="CP",
    units="D",
    ev_origin_time=UTCDateTime(EVENT_LOCATION["time"]),
)


f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, VELOCITY_MODEL, phase
)
f_highpass = lambda x: 1

nsls = [NSL.from_trace(tr) for tr in stream.select(channel="*Z")]


arrivals = defaultdict(dict)
for nsl in nsls:
    ray = f_raytrace(event_enu, stations[nsl.__str__()]["enu"], Phase.P)
    arrivals[nsl][Phase.P] = ray.traveltime_sec + EVENT_LOCATION["time"]


rotated_stream = _rotate_stream_to_p_rays(
    stream, {nsl: ray for (nsl, ph), ray in rays.items() if ph.is_P}, INV
)

vp = VELOCITY_MODEL[0].vp
rho = VELOCITY_MODEL[0].rho
mw_estimates = {}

for trace in rotated_stream.select(channel="??P"):
    station = INV.get_coordinates(trace.get_id().replace("CPP", "CPZ"))
    easting, northing = reproject(station["longitude"], station["latitude"])
    station_enu = array([easting, northing, station["elevation"]])
    distance = np.linalg.norm(station_enu - event_enu)
    p_time = arrivals[NSL.from_trace(trace)][Phase.P] - EVENT_LOCATION["time"]
    i_p_time = int(p_time // DT)
    moment = (
        4
        * np.pi
        * rho
        * vp**3
        * romb(trace.copy()[i_p_time - 100 : i_p_time + 413], DT)
        * distance
    )
    mw_estimates[trace.get_id()] = moment_to_mw(np.sqrt(1.5) * moment)

from scipy.integrate import romb

trace.plot()

romb()
mw_estimates
NSL.from_trace(trace)

fig, ax = plt.subplots()
ax.loglog(spectra.freqs, spectra.dis)
