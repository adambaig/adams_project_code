from collections import defaultdict
import json


import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from numpy import array
import numpy as np
from obspy import read_inventory, UTCDateTime
import pyproj


from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.formulas import (
    moment_to_mw,
    mw_to_moment,
    calculate_source_radius_from_stress_drop,
    radius_corner_frequency_madariaga,
    convert_radius_corner_brune,
)
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.moment_tensor.inversion import inversion_matrix_row
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.simulation import generate_random_dc_moment_tensor
from nmxseis.numerics.simulation.event_waveform import (
    simulate_waveforms_for_array,
    simulate_waveform_from_source,
)
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

from nmxseis.numerics.source_parameters import compute_source_parameters
from nmxseis.numerics.source_parameters.plotting import make_spectra_QC_plot


np.random.seed(2)


TAB10 = get_cmap("tab10")
NOISE_RMS = 3e-7


def get_station_number_color(station_number):
    nsl_number = (float(station_number) - 0.5) % 10
    return TAB10((nsl_number) / 10)


inv = read_inventory("synthetic\\synthetic_Paramount_2_28_example\\synthetic_sta.xml")
reproject = pyproj.Proj("epsg:26911")

event = SeismicEvent(
    depth_m=2100,
    origin_time=UTCDateTime(1637615497.695214),
    lat=54.401317823038646,
    lon=-116.93805538849605,
    reproject=reproject,
)
stations = {}
for network in inv:
    for station in network:
        stations[f"{network.code}.{station.code}.00"] = {
            "latitude": station.latitude,
            "longitude": station.longitude,
            "elevation": station.elevation,
        }

eastings, northings = reproject(
    *zip(*[(v["longitude"], v["latitude"]) for v in stations.values()])
)
for i_station, station in enumerate(stations.values()):
    station["enu"] = np.array(
        [eastings[i_station], northings[i_station], station["elevation"]]
    )
event_location = {
    "depth": 2.1,
    "time": 1637615497.695214,
    "latitude": 54.401317823038646,
    "longitude": -116.93805538849605,
}
easting, northing = reproject(event.lon, event.lat)
event_enu = [easting, northing, -1000 * event_location["depth"]]

source = {
    **event_location,
    "enu": np.array(event_enu),
    "moment_magnitude": 0,
    "stress_drop": 1.0e4,  # static stress drop
}

layer = VMLayer1D(rho=2500, vp=5000, vs=3000)
velocity_model = VelocityModel1D([layer])

n_iterations = 100

mags = np.random.sample(n_iterations) * 3 - 1
stress_drops = 10 ** (np.random.sample(n_iterations) * 2 + 5)

moments = mw_to_moment(mags)

radii = calculate_source_radius_from_stress_drop(moments, stress_drops)
corner_frequencies = [
    {
        Phase.P: convert_radius_corner_brune(r, layer.vp),
        Phase.S: convert_radius_corner_brune(r, layer.vs),
    }
    for r in radii
]


f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, velocity_model, phase
)
f_highpass = lambda x: 1

mw_out, mw_unc = [], []
cf_p_out, cf_s_out = [], []
quality_score = []
min_snr = {"A": 5, "B": 3, "C": 1.5}
min_samples_per_bin = {"A": 5, "B": 3, "C": 2}
f_smooth_min = {"A": 1, "B": 10, "C": 30}

for mag, corners in zip(mags, corner_frequencies):
    random_dc_mt = generate_random_dc_moment_tensor()
    out_stream, rays = simulate_waveforms_for_array(
        *(random_dc_mt, mag, corners, event_enu, inv, velocity_model, reproject),
        nt=8000,
        dt=0.00025,
        Qp=1000,
        Qs=1000,
        noise_rms=NOISE_RMS,
        ch_prefix="CP",
        units="V",
        ev_origin_time=UTCDateTime(source["time"]),
    )
    nsls = [NSL.from_trace(tr) for tr in out_stream.select(channel="*Z")]

    # free surface amp
    for tr in out_stream:
        tr.data = 2 * tr.data
    arrivals = defaultdict(dict)
    for nsl in nsls:
        for phase in [Phase.P, Phase.S]:
            ray = f_raytrace(source["enu"], stations[nsl.__str__()]["enu"], phase)
            arrivals[nsl][phase] = UTCDateTime(ray.traveltime_sec + source["time"])

    for quality in ["A", "B", "C"]:
        source_parameters = compute_source_parameters(
            stream=out_stream.copy(),
            nsls=[NSL.from_trace(tr) for tr in out_stream],
            inventory=inv,
            arrivals=arrivals,
            f_raytrace=f_raytrace,
            event=event,
            ev_vs=layer.vs,
            ev_vp=layer.vp,
            Q=1000,
            kappa=0,
            f_highpass=f_highpass,
            lowpass_pct=20,
            min_snr=min_snr[quality],
            min_d_hypo_m=0,
            max_d_hypo_m=1000000,
            min_samples_per_bin=min_samples_per_bin[quality],
            f_smooth_min=f_smooth_min[quality],
            f_smooth_max=1000,
            f_smooth_n_bins=31,
            n_trial_drops=100,
        )
        if source_parameters is not None:
            mw_out.append(source_parameters.mw)
            mw_unc.append(source_parameters.mw_uncertainty)
            cf_p_out.append(source_parameters.fc_P)
            cf_s_out.append(source_parameters.fc_S)
            quality_score.append(quality)
            break

colors = {"A": "forestgreen", "B": "lightblue", "C": "darkgoldenrod"}
zorders = {"A": 1, "B": 2, "C": 3}
fig, ax = plt.subplots(figsize=[6, 6])
ax.set_aspect("equal")
patches = {}
for m_in, m_out, m_err, q in zip(mags, mw_out, mw_unc, quality_score):
    p = ax.errorbar(
        m_in,
        m_out,
        yerr=2 * m_err,
        color=colors[q],
        marker="o",
        capsize=3,
        markeredgecolor="0.2",
    )
    if q not in patches:
        patches[q] = p
x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()
ax.plot([-10, 20], [-10, 20], ":", color="0.5")
ax.set_xlim([ax_min := min(x1, y1), ax_max := max(x2, y2)])
ax.set_ylim([ax_min, ax_max])
ax.set_xlabel("input mw")
ax.set_ylabel("output mw")
fig.savefig("Mw_comparison.png")


fig, (ax_cf_p, ax_cf_s) = plt.subplots(1, 2, figsize=[12, 6], sharex=True, sharey=True)
for ax, cf_in, cf_out in zip(
    [ax_cf_p, ax_cf_s],
    [
        [c[Phase.P] for c in corner_frequencies],
        [c[Phase.S] for c in corner_frequencies],
    ],
    [cf_p_out, cf_s_out],
):
    ax.set_aspect("equal")
    for c_in, c_out, q in zip(cf_in, cf_out, quality_score):
        if c_out is not None:
            ax.loglog(c_in, c_out, "o", color=colors[q], markeredgecolor="0.2")
    x1, x2 = ax.get_xlim()
    y1, y2 = ax.get_ylim()
    ax.plot([0.1, 10000], [0.1, 10000], ":", color="0.5")
    ax.set_xlim([ax_min := min(x1, y1), ax_max := max(x2, y2)])
    ax.set_ylim([ax_min, ax_max])
    ax.set_xlabel("input corner freq (Hz)")
    ax.set_ylabel("output corner freq (Hz)")
ax_cf_p.set_title("P corners")
ax_cf_s.set_title("S corners")
fig.savefig("corner_comparison.png")


fig, ax = plt.subplots()
p_A = ax.plot(0, 0, "o", color="forestgreen", markeredgecolor="0.2")
p_B = ax.plot(0, 0, "o", color="lightblue", markeredgecolor="0.2")
p_C = ax.plot(0, 0, "o", color="darkgoldenrod", markeredgecolor="0.2")
ax.legend([p_A[0], p_B[0], p_C[0]], ["A", "B", "C"])
fig.savefig("legend.png")

fig, ax = plt.subplots(figsize=[8, 8])
ax.plot(mw_out, mw_unc, ".")

fig, (ax_p, ax_s) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=[12, 8])
for (nsl, phase), spec in source_parameters.corrected_individual_spectra.items():
    if phase.is_P:
        ax_p.loglog(spec.freqs, spec.dis, color=get_station_number_color(nsl.sta))
    else:
        ax_s.loglog(spec.freqs, spec.dis, color=get_station_number_color(nsl.sta))


moment = mw_to_moment(mags[-1])
for ax, phase in zip([ax_p, ax_s], [Phase.P, Phase.S]):
    ax.loglog((spec := source_parameters.mean_spectra[phase]).freqs, spec.dis, "k")
    ax.loglog(spec.freqs, moment * np.ones(len(spec.freqs)), "0.4")
    ax.grid("on")
    ax.set_xlabel("frequency (Hz)")
ax_p.set_ylabel("source spectrum N$\cdot$m$\cdot$s")
ax_p.set_title("P spectra")
ax_s.set_title("S spectra")

p_plateaus, s_plateaus = [], []
for station_name, station in stations.items():
    p_raypath = f_raytrace(source["enu"], station["enu"], Phase.P)
    p_plateau = abs(
        moment * np.dot(random_dc_mt._vector, inversion_matrix_row(p_raypath, Phase.P))
    )
    ax_p.loglog(
        [1, 10],
        [p_plateau, p_plateau],
        color=get_station_number_color(station_name.split(".")[1]),
    )
    s_raypath = f_raytrace(source["enu"], station["enu"], Phase.S)
    sv_plateau = abs(
        moment * np.dot(random_dc_mt._vector, inversion_matrix_row(s_raypath, Phase.SV))
    )
    sh_plateau = abs(
        moment * np.dot(random_dc_mt._vector, inversion_matrix_row(s_raypath, Phase.SH))
    )
    s_plateau = np.hypot(sv_plateau, sh_plateau)
    ax_s.loglog(
        [1, 10],
        [s_plateau, s_plateau],
        color=get_station_number_color(station_name.split(".")[1]),
    )
    p_plateaus.append(p_plateau)
    s_plateaus.append(s_plateau)
fig.savefig("example_QC_plot.png")

fig, (ax_p, ax_s) = plt.subplots(1, 2, figsize=[12, 8])
make_spectra_QC_plot(source_parameters, [ax_p, ax_s])
p_plateaus, s_plateaus = [], []
for station_name, station in stations.items():
    p_raypath = f_raytrace(source["enu"], station["enu"], Phase.P)
    p_plateau = abs(
        moment * np.dot(random_dc_mt._vector, inversion_matrix_row(p_raypath, Phase.P))
    )
    ax_p.loglog(
        [1, 10],
        [p_plateau, p_plateau],
        color=get_station_number_color(station_name.split(".")[1]),
    )
    s_raypath = f_raytrace(source["enu"], station["enu"], Phase.S)
    sv_plateau = abs(
        moment * np.dot(random_dc_mt._vector, inversion_matrix_row(s_raypath, Phase.SV))
    )
    sh_plateau = abs(
        moment * np.dot(random_dc_mt._vector, inversion_matrix_row(s_raypath, Phase.SH))
    )
    s_plateau = np.hypot(sv_plateau, sh_plateau)
    ax_s.loglog(
        [1, 10],
        [s_plateau, s_plateau],
        color=get_station_number_color(station_name.split(".")[1]),
    )
    p_plateaus.append(p_plateau)
    s_plateaus.append(s_plateau)
