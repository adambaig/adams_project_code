import json
import os
import obspy
import matplotlib.pyplot as plt
import numpy as np

import pyproj

from realtime_source_parameters.event_origin_processor import EventOriginProcessor
from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)
from realtime_source_parameters.athena_query_helper import get_single_event_origin

from realtime_source_parameters.models.event_wrapper import EventWrapper

from realtime_source_parameters.engine import (
    get_velocity_lookup_and_raytracer,
    rotate_stream_to_raypaths,
    get_station_locations,
    prepare_trace,
    make_spectrum,
    calculate_vel_density,
    brune_fit,
    measure_ps_separation,
    theoretical_ps_separation,
    calc_0mega0_Fc,
    calc_moment,
    fit_spectrum,
    q_compensate,
)

seedfile = r"synthetic\2021-11-22T21-11-37.695214Z_synthetic.mseed"
waveforms_stream = obspy.read(seedfile)
inventory = get_station_inventory_from_file(r"synthetic/synthetic_sta.xml")
config = load_config_from_file(r"synthetic\synthetic_config.json")

with open(r"synthetic/synthetic_event.json") as f:
    event_json = json.load(f)
epsg_code = config.get_epsg_code()
event = EventWrapper.from_event_json(event_json)
velocity_model_config = config.get_velocity_model_config()
processing_config = config.get_processing_config()
processor = EventOriginProcessor(None, None, config)
processing_config.get_init_corner_frequency()
source_parameters = processor.calculate_source_parameters(
    event, inventory, waveforms_stream
)
