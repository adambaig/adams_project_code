from collections import defaultdict
import json
import os

import matplotlib.pyplot as plt
import numpy as np
from obspy import UTCDateTime

input_catalog = defaultdict()
with open(
    os.path.join("Braskem", "synthetic_waveform_data", "synthetic_catalog.csv")
) as f:
    _head = f.readline()
    lines = f.readlines()
    for line in lines:
        split_line = line.split(",")
        input_catalog[split_line[0]] = {
            "origin time": UTCDateTime(float(split_line[1])),
            "latitude": float(split_line[2]),
            "longitude": float(split_line[3]),
            "depth": float(split_line[4]),
            "mw": float(split_line[5]),
            "stress drop": float(split_line[6]),
        }

with open(os.path.join("Braskem", "synthetic_rtsp_results.json")) as f:
    output_dataset = json.load(f)


fig, ax = plt.subplots(figsize=[6, 6])
ax.set_aspect("equal")
for origin_time, event_data in output_dataset.items():
    mw_in = [
        v["mw"] for v in input_catalog.values() if origin_time == str(v["origin time"])
    ][0]
    ax.plot(mw_in, event_data["mw"], "o", mec="0.2")

xlim = ax.get_xlim()
ylim = ax.get_ylim()

min_val = min(*xlim, *ylim)
max_val = max(*xlim, *ylim)
ax.plot([min_val, max_val], [min_val, max_val], "--", color="0.2", zorder=-1)
ax.set_xlabel("input mw")
ax.set_ylabel("output mw")
ax.set_xlim([min_val, max_val])
ax.set_ylim([min_val, max_val])
