from nmxseis.numerics.source_parameters import (
    _SPSpectra,
    _rotate_stream_to_p_rays,
    get_station_enus,
    get_signal_and_noise_spectra,
    get_signal_and_noise_windows,
    _StationInfo,
    get_valid_phases_for_arrivals,
)

stream = waveform_stream.copy()
valid_phases = get_valid_phases_for_arrivals(arrivals)
station_enus = get_station_enus(nsls, inventory, source.reproject)
rays = {
    (nsl, ph): f_raytrace(source.enu, st_enu, ph)
    for nsl, st_enu in station_enus.items()
    # important: always need P rays for PVH rotation
    for ph in set(valid_phases) | {Phase.P}
}


nsls_without_waveforms = []
stream.remove_response(inventory=inventory, output="ACC").detrend(type="linear")
rotated_traces = _rotate_stream_to_p_rays(
    stream, {nsl: ray for (nsl, ph), ray in rays.items() if ph.is_P}, inventory
)

signal_windows, noise_windows = get_signal_and_noise_windows(
    arrivals,
    source.enu,
    station_enus,
    f_raytrace,
    p_win_post=None,
    p_win_pre=0.1,
    s_win_post=None,
    s_win_pre=0.1,
)


sta_infos = {}
for nsl, st_enu in station_enus.items():
    nsl_stream = nsl.select_from_stream(stream)
    if len(nsl_stream) == 0:
        nsls_without_waveforms.append(nsl)
        continue
    hypo = np.linalg.norm(st_enu - source.enu)
    p_window_lowest_freq = (
        1 / np.diff(signal_windows[nsl, Phase.P])
        if (nsl, Phase.P) in signal_windows
        else 0
    )
    s_window_lowest_freq = (
        1 / np.diff(signal_windows[nsl, Phase.S])
        if (nsl, Phase.S) in signal_windows
        else 0
    )
    sta_infos[nsl] = _StationInfo(
        nsl=nsl,
        enu=st_enu,
        highpass={
            Phase.P: max(f_highpass(float(hypo)), p_window_lowest_freq),
            Phase.S: max(f_highpass(float(hypo)), s_window_lowest_freq),
        },
        lowpass=(1.0 - 20 / 100.0) * nsl_stream[0].stats.sampling_rate / 2,
        is_on_surface=inventory.select(
            **nsl.as_obspy_dict()
        )  # TODO probably need to have a lookup for this eventually
        .select(starttime=source.origin_time, channel="*Z")[0][0][0]
        .depth
        < 1,
        ev_hyp=hypo,
    )


spectra, noise_spectra = get_signal_and_noise_spectra(
    rotated_traces,
    signal_windows,
    noise_windows,
    1,
    1000,
    41,
)
Q, kappa = 65, 0
_SPSpectra(
    # fmt: off
        source, sta_infos, rays, arrivals,
        0, np.inf, 3, 3,Q, kappa,
        spectra=spectra,
        noise_spectra=noise_spectra,
    # fmt: on
)

from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.source_parameters import (
    _correct_for_geom_spreading,
    _correct_for_anelastic_attenuation,
    _calculate_mean_spectra,
)

snrs = {
    (nsl, ph): spectra[nsl, ph].acc / noise_spectra[nsl, ph].acc
    for (nsl, ph) in spectra.keys()
}
q_corrected = _correct_for_anelastic_attenuation(
    spectra, Q, source.origin_time, arrivals=arrivals
)
qg_corrected = _correct_for_geom_spreading(
    q_corrected, raypaths=rays, sta_infos=sta_infos
)
qgk_corrected = {
    (nsl, ph): spec.correct_kappa(kappa) for (nsl, ph), spec in qg_corrected.items()
}
qgkr_corrected = {
    (nsl, ph): spec.correct_for_radiation_pattern(ph)
    for (nsl, ph), spec in qgk_corrected.items()
}
mean_qg_corrected, mean_qg_corrected_std = _calculate_mean_spectra(
    qg_corrected,
    sta_infos,
    max_d_hypo_m=np.inf,
    min_d_hypo_m=0,
    min_samples_per_bin=1,
)


mean_qgk_corrected = {
    ph: mean_spec.correct_kappa(kappa) for ph, mean_spec in mean_qg_corrected.items()
}

common_freqs = None
for v in spectra.values():
    if common_freqs is None:
        common_freqs = v.freqs
from numpy.ma import MaskedArray

acc_samples: dict[Phase, list[MaskedArray]] = defaultdict(list)
vel_samples: dict[Phase, list[MaskedArray]] = defaultdict(list)
dis_samples: dict[Phase, list[MaskedArray]] = defaultdict(list)

for (nsl, phase), spec in spectra.items():
    acc_samples[phase].append(spec.acc)
    vel_samples[phase].append(spec.vel)
    dis_samples[phase].append(spec.dis)

mean_spectra = {}
std_spectra = {}
for ph in valid_phases:

    outs = []
    sds = []

    for sd in [acc_samples, vel_samples, dis_samples]:
        samples = sd[ph]
        # TODO snr weighting? would be fairly easy: basically replace ones here with station SNRs
        weights = np.ones(len(samples))
        sample_mean, tot_weight = np.ma.average(
            samples, axis=0, weights=weights, returned=True
        )

        # TODO keep based on cumulative SNR?
        keep_mask = tot_weight >= 3
        sample_mean = np.ma.masked_where(~keep_mask, sample_mean)
        outs.append(sample_mean)

        std = np.ma.std(samples, axis=0)
        std = np.ma.masked_where(~keep_mask, std)
        sds.append(std)

    mean_spectra[ph] = outs
    std_spectra[ph] = sds

mean_spectra = {
    ph: MotionSpectrum(
        _acc=mean_spectra[ph][0],
        _vel=mean_spectra[ph][1],
        _dis=mean_spectra[ph][2],
        freqs=np.ma.masked_where(mean_spectra[ph][0].mask, common_freqs),
        # TODO having to pass these hidden parameters is kind of a smell. Smoothed spectra should
        #  really be a separate class, derived from, say, AbstractMotionSpectrum that just implements
        #  the integration/differentiation code
        _is_smoothed=True,
    )
    for ph in valid_phases
}


#
# mean_spectra = {
#     ph: MotionSpectrum(
#         _acc=mean_spectra[ph][0],
#         _vel=mean_spectra[ph][1],
#         _dis=mean_spectra[ph][2],
#         freqs=np.ma.masked_where(mean_spectra[ph][0].mask, common_freqs),
#         # TODO having to pass these hidden parameters is kind of a smell. Smoothed spectra should
#         #  really be a separate class, derived from, say, AbstractMotionSpectrum that just implements
#         #  the integration/differentiation code
#         _is_smoothed=True,
#     )
#     for ph in valid_phases
# }


qualities = [s["quality_score"] for s in sp_results.values()]

with open("Braskem\\sp_tuned_B_results.json", "w") as f:
    json.dump(sp_results, f)


a_data = {k: v for k, v in sp_results.items() if v["quality_score"] == "A"}
b_data = {k: v for k, v in sp_results.items() if v["quality_score"] == "B"}
c_data = {k: v for k, v in sp_results.items() if v["quality_score"] == "C"}
f_data = {k: v for k, v in sp_results.items() if v["quality_score"] == "F"}
[k for k, v in sp_results.items() if v["quality_score"] == "F"]

m_bins = np.arange(-2.5, 1, 0.1)
fig, ax = plt.subplots()
ax.hist(
    [v["mw"] for v in c_data.values()], m_bins, facecolor="darkgoldenrod", alpha=0.7
)
ax.hist([v["mw"] for v in b_data.values()], m_bins, facecolor="forestgreen", alpha=0.7)
ax.hist([v["mw"] for v in a_data.values()], m_bins, facecolor="firebrick", alpha=0.7)


waveform_stream.copy().merge(method=1)
