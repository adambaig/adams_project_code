import json
import os

import matplotlib.pyplot as plt
from obspy import UTCDateTime, read, read_inventory

from realtime_source_parameters.event_origin_processor import EventOriginProcessor
from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)
from realtime_source_parameters.models.event_wrapper import EventWrapper

seedfile = r"Mosaic\Mosaic_Esterhazy_20210924004105000.mseed"
waveforms_stream = read(seedfile)
inventory = get_station_inventory_from_file(r"Mosaic\mosaic_inventory.xml")


waveforms_stream = waveforms_stream.select(station="16*")
for tr in waveforms_stream:
    if tr.stats.channel == "GPN":
        tr.stats.channel = "GP1"
    if tr.stats.channel == "GPE":
        tr.stats.channel = "GP2"
    station = tr.stats.station
    location = station[-2:]
    tr.stats.station = "BF16"
    tr.stats.location = location


response = inventory.get_response("XW.BF16.01.GPZ", UTCDateTime(UTCDateTime.now()))

waveforms_stream.detrend()
waveforms_stream.trim(
    starttime=UTCDateTime(2021, 9, 24, 0, 41, 7, 501500),
    endtime=UTCDateTime(2021, 9, 24, 0, 41, 8, 501500),
)
waveforms_stream.rotate("->ZNE", inventory=inventory)
for trace in waveforms_stream:
    trace.data /= response.instrument_sensitivity.value

waveforms_stream.select(location="03").plot()


event = EventWrapper.from_event_json(event_json)
