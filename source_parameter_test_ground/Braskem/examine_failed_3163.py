from collections import defaultdict
from glob import glob
import json
import os

import matplotlib.pyplot as plt
from NocMeta.Meta import NOC_META
import numpy as np
from obspy import UTCDateTime, read
import pyproj

from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)
from realtime_source_parameters.athena_query_helper import (
    get_single_event_origin,
    get_new_events_since,
    get_single_event_preferred_origin,
)

from nmxseis.interact.athena import AthenaClient
from nmxseis.interact.fdsn import FDSNClient
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.source_parameters import compute_source_parameters
from nmxseis.numerics.source_parameters.plotting import make_spectra_QC_plot
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

from read_inputs import read_GMM


TEST_DATA_FBK_STATION_XML = os.path.join("Braskem", "FBK_Full.xml")
TEST_DATA_FBK_CONFIG = os.path.join("Braskem", "fbk_config.json")
config = load_config_from_file(TEST_DATA_FBK_CONFIG)
inventory = get_station_inventory_from_file(TEST_DATA_FBK_STATION_XML)
athena_client = AthenaClient(config.get_athena_base_url(), config.get_athena_api_key())
reproject = pyproj.Proj(f"epsg:{config.get_epsg_code()}")
velocity_model_config = config.get_velocity_model_config()
fourier_spectra_config = config.get_fourier_spectra_config()
processing_config = config.get_processing_config()

layers = []
for layer in velocity_model_config.get_layered_velocity_model():
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )
velocity_model = VelocityModel1D(layers)

waveform_dir = r"Braskem\GMM_dataset\waveforms"
f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, velocity_model, phase
)
f_highpass = lambda x: 0.3


def get_nsl_from_channel_path(channel_path):
    return NSL(
        net=channel_path["networkCode"],
        sta=channel_path["stationCode"],
        loc=channel_path["locationCode"],
    )


min_snr = {"A": 3, "B": 3, "C": 1.5, "D": 0.5}
min_samples_per_bin = {"A": 5, "B": 3, "C": 2, "D": 1}
f_smooth_min = {"A": 1, "B": 5, "C": 10, "D": 10}
sp_results = {}

seed_files = glob(os.path.join(waveform_dir, "*.MSEED"))

event_json = os.path.join("Braskem", "failed_sp_json", "003163.json")
# event_json = os.path.join("Braskem", "output_sp_json", "013729.json")
event_id = os.path.basename(event_json).split(".")[0].lstrip("0")
event = get_single_event_preferred_origin(athena_client, event_id)
arrivals = defaultdict(dict)
origin = event.get_origin()
seed_name = os.path.join(waveform_dir, f"{event_id}.MSEED")
waveform_stream = read(seed_name)
nsls = [NSL.from_trace(tr) for tr in waveform_stream]
for arrival in origin.get_arrivals():
    pick = arrival.get_pick()
    nsl = get_nsl_from_channel_path(arrival.get_pick().get_channel_path())
    if nsl in nsls:
        arrivals[nsl][Phase.parse(pick.get_phase())] = UTCDateTime(pick.get_time())
nsls = [NSL.from_trace(tr) for tr in waveform_stream if NSL.from_trace(tr) in arrivals]
trace_lengths = [tr.stats.npts for tr in waveform_stream]

source = SeismicEvent(
    origin_time=origin.get_time(),
    lat=origin.get_latitude(),
    lon=origin.get_longitude(),
    reproject=reproject,
    depth_m=1000 * origin.get_depth(),
)

vm_at_source = velocity_model.look_up_enu(source.enu)

quality = "C"
for quality in ["A", "B", "C", "D"]:
    source_parameters = compute_source_parameters(
        stream=waveform_stream.copy(),
        nsls=nsls,
        inventory=inventory,
        arrivals=arrivals,
        f_raytrace=f_raytrace,
        event=source,
        ev_vs=vm_at_source.vs,
        ev_vp=vm_at_source.vp,
        Q=velocity_model_config.get_Q(),
        kappa=processing_config.get_kappa(),
        f_highpass=lambda x: 0.3,
        lowpass_pct=processing_config.get_lowpass_percent(),
        min_snr=min_snr[quality],
        min_d_hypo_m=processing_config.get_min_hypo_distance_m(),
        max_d_hypo_m=processing_config.get_max_hypo_distance_m(),
        min_samples_per_bin=min_samples_per_bin[quality],
        f_smooth_min=f_smooth_min[quality],
        f_smooth_max=1000,
        f_smooth_n_bins=41,
        n_trial_drops=100,
    )
    if source_parameters is not None:
        sp_results[str(UTCDateTime(source.origin_time))] = {
            "mw": float(source_parameters.mw),
            "uncertainty": float(source_parameters.mw_uncertainty),
            "p corner": float(source_parameters.fc_P)
            if source_parameters.fc_P is not None
            else None,
            "s corner": float(source_parameters.fc_S)
            if source_parameters.fc_S is not None
            else None,
            "quality_score": quality,
            "lat": source.lat,
            "lon": source.lon,
            "depth": source.depth_m,
            "origin time": source.origin_time,
            "enu": source.enu.tolist(),
            "ml": event.get_origin().get_ml(),
            "arrivals": {
                f"{nsl.__str__()}.{ph.name}": value.timestamp
                for nsl, ph_value in arrivals.items()
                for ph, value in ph_value.items()
            },
        }
        break

from __future__ import annotations

from collections import Counter, defaultdict
from dataclasses import dataclass, field, replace
from logging import Logger, NullHandler
from typing import Callable, Iterable

import numpy as np
from numpy import pi as π
from numpy import sqrt
from numpy.linalg import norm
from numpy.ma import MaskedArray
from obspy import Inventory, Stream, UTCDateTime

from nmxseis.model import Reprojector
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent, SourceParameters
from nmxseis.numerics._internal_util import find_largest_valid_span
from nmxseis.numerics.formulas import moment_to_mw
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.source_parameters._internal import StressDrop

from nmxseis.numerics.source_parameters import (
    get_valid_phases_for_arrivals,
    _calc_sp_spectra,
    get_station_enus,
    _rotate_stream_to_p_rays,
    get_signal_and_noise_windows,
    get_signal_and_noise_spectra,
    _StationInfo,
    _SPSpectra,
    _calc_source_params_from_spectra,
    _calc_prelim_corner,
    _get_valid_dis_spec_for_moment,
    _get_useable_bandwidth_for_stress_drop,
)


Q = velocity_model_config.get_Q()
kappa = processing_config.get_kappa()
lowpass_pct = processing_config.get_lowpass_percent()

min_d_hypo_m = processing_config.get_min_hypo_distance_m()
max_d_hypo_m = processing_config.get_max_hypo_distance_m()

f_smooth_max = 1000
f_smooth_n_bins = 41
n_trial_drops = 100

DEFAULT_S_WIN_PRE = 0.2
DEFAULT_P_WIN_PRE = 0.1
P_WIN_POST_COEF = 0.9
S_WIN_POST_COEF = 1.8

ROLL_BACK_CF_FOR_MOMENT = 1.5
ROLL_UP_CF_FOR_STRESS_DROP = 1.5
ROLL_UP_LOWPASS_FOR_STRESS_DROP = 0.9

P = Phase.P
S = Phase.S

valid_phases = get_valid_phases_for_arrivals(arrivals)
stream = waveform_stream.copy()

p_win_pre = DEFAULT_P_WIN_PRE
p_win_post = None
s_win_pre = DEFAULT_S_WIN_PRE
s_win_post = None
logger = None

station_enus = get_station_enus(nsls, inventory, source.reproject)

rays = {
    (nsl, ph): f_raytrace(source.enu, st_enu, ph)
    for nsl, st_enu in station_enus.items()
    # important: always need P rays for PVH rotation
    for ph in set(valid_phases) | {P}
}

nsls_without_waveforms = []
stream.merge(method=1)
stream.remove_response(inventory=inventory, output="ACC").detrend(type="linear")
rotated_traces = _rotate_stream_to_p_rays(
    stream, {nsl: ray for (nsl, ph), ray in rays.items() if ph.is_P}, inventory
)


signal_windows, noise_windows = get_signal_and_noise_windows(
    arrivals,
    source.enu,
    station_enus,
    f_raytrace,
    p_win_post=None,
    p_win_pre=p_win_pre,
    s_win_post=None,
    s_win_pre=s_win_pre,
)
NSLP = tuple[NSL, Phase]
Window = tuple[UTCDateTime, UTCDateTime]
noise_windows: dict[NSLP, Window] = {}
signal_windows: dict[NSLP, Window] = {}

for nsl, phase_arrivals in arrivals.items():
    if len(phase_arrivals) == 1:
        # TODO we can be cleverer here. We have one arrival: we can scale the separation by
        #  actual_arrival/calculated_arrival
        enu = station_enus[nsl]
        separation = (
            f_raytrace(source.enu, enu, S).traveltime_sec
            - f_raytrace(source.enu, enu, P).traveltime_sec
        )
    else:
        separation = phase_arrivals[S] - phase_arrivals[P]
        if separation < 0:
            continue
    for ph, t_arrival in phase_arrivals.items():
        win_pre = s_win_pre * separation if ph.is_S else p_win_pre * separation

        if ph.is_S:
            if s_win_post is None:
                win_post = S_WIN_POST_COEF * separation
            else:
                win_post = s_win_post
        else:
            if p_win_post is None:
                win_post = P_WIN_POST_COEF * separation
            else:
                win_post = p_win_post

        signal_window = (t_arrival - win_pre, t_arrival + win_post)
        signal_windows[nsl, ph] = signal_window

        noise_end = t_arrival - win_pre
        if ph.is_S:
            noise_end -= separation
        noise_window = (noise_end - win_pre - win_post, noise_end)
        noise_windows[nsl, ph] = noise_window

from plotting import plot_rotated_waveforms_and_windows

plot_rotated_waveforms_and_windows(
    rotated_traces, signal_windows, noise_windows, arrivals
)

sta_infos = {}
for nsl, st_enu in station_enus.items():
    nsl_stream = nsl.select_from_stream(stream)
    if len(nsl_stream) == 0:
        nsls_without_waveforms.append(nsl)
        continue
    hypo = np.linalg.norm(st_enu - source.enu)
    p_window_lowest_freq = (
        1 / np.diff(signal_windows[nsl, Phase.P])
        if (nsl, Phase.P) in signal_windows
        else 0
    )
    s_window_lowest_freq = (
        1 / np.diff(signal_windows[nsl, Phase.S])
        if (nsl, Phase.S) in signal_windows
        else 0
    )
    sta_infos[nsl] = _StationInfo(
        nsl=nsl,
        enu=st_enu,
        highpass={
            Phase.P: max(f_highpass(float(hypo)), p_window_lowest_freq),
            Phase.S: max(f_highpass(float(hypo)), s_window_lowest_freq),
        },
        lowpass=(1.0 - lowpass_pct / 100.0) * nsl_stream[0].stats.sampling_rate / 2,
        is_on_surface=inventory.select(
            **nsl.as_obspy_dict()
        )  # TODO probably need to have a lookup for this eventually
        .select(starttime=source.origin_time, channel="*Z")[0][0][0]
        .depth
        < 1,
        ev_hyp=hypo,
    )

spectra, noise_spectra = get_signal_and_noise_spectra(
    rotated_traces,
    signal_windows,
    noise_windows,
    f_smooth_min[quality],
    f_smooth_max,
    f_smooth_n_bins,
)


spectra.keys()

fig, (ax_p, ax_s) = plt.subplots(1, 2, sharex="all", sharey="all", figsize=[12, 8])
for (nsl, phase), spec in spectra.items():
    if phase.is_P:
        ax_p.loglog(spec.freqs, spec.dis)
    else:
        ax_s.loglog(spec.freqs, spec.dis)
    for ax in [ax_p, ax_s]:
        ax.grid("on")
        ax.set_xlabel("frequency (Hz)")
    ax_p.set_ylabel("displacement_spectra m$\cdot$s")
ax_p.set_title("P spectra")
ax_s.set_title("S spectra")
fig.savefig("Braskem/disp_spectra_3163.png")
sp = _SPSpectra(
    source,
    sta_infos,
    rays,
    arrivals,
    processing_config.get_min_hypo_distance_m(),
    processing_config.get_max_hypo_distance_m(),
    min_samples_per_bin[quality],
    min_snr[quality],
    Q,
    kappa,
    spectra=spectra,
    noise_spectra=noise_spectra,
    valid_phases=get_valid_phases_for_arrivals(arrivals),
)
sp_spectra.mean_qgkr_corrected[Phase.S].dis
sp.mean_qg_corrected[Phase.P].acc
global_valid_bands = {
    ph: find_largest_valid_span(sp.mean_qg_corrected[ph].acc) for ph in valid_phases
}

prelim_corners = {
    ph: _calc_prelim_corner(sp.mean_qgk_corrected[ph], *global_valid_bands[ph])
    for ph in valid_phases
}

moment_valid_bands = {
    ph: _get_valid_dis_spec_for_moment(
        sp.mean_qgkr_corrected[ph], sp.sta_infos, prelim_corners[ph]
    )
    for ph in valid_phases
}
sdrop_valid_bands = {
    ph: _get_useable_bandwidth_for_stress_drop(
        sp.mean_qgkr_corrected[ph], sp.sta_infos, prelim_corners[ph]
    )
    for ph in valid_phases
}
moment_samples = np.ma.stack([moment_valid_bands[ph] for ph in valid_phases])
M0 = np.ma.median(moment_samples)
mw = moment_to_mw(M0)
np.ma.is_masked(M0)

moment_uncertainty = np.ma.std(moment_samples)
mw_uncertainty = np.ma.std([moment_to_mw(m) for m in moment_samples])
stress_drops: dict[Phase, StressDrop | None] = {
    ph: StressDrop.calc_brune(
        sdrop_valid_bands[ph][0],
        sdrop_valid_bands[ph][1],
        M0,
        vs_or_vp=ev_vs if ph.is_S else ev_vp,
        n_trials=n_trial_drops,
    )
    for ph in valid_phases
}
