from obspy import UTCDateTime

from nmxseis.interact.athena import AthenaClient
from realtime_source_parameters.athena_query_helper import get_new_events_since
from realtime_source_parameters.waveforms_query_helper import get_waveforms_for_origin

from NocMeta.Meta import NOC_META

athena_config = NOC_META["FBK"]
athena_client = AthenaClient(rf'http://{athena_config["athIP"]}', athena_config["athApi"])

events = get_new_events_since(athena_client, UTCDateTime("2022-03-22").timestamp, "MANUAL", 10)
waveform_stream = get_waveforms_for_origin(
    FDSNClient(config.get_apollo_server_ip()), events[3].get_origin(), 1, 2
)

events = get_new_events_since(athena_client, UTCDateTime("2022-03-14").timestamp, "MANUAL", 10

for event in events:
    out_name = f"Braskem_data_{UTCDateTime(event.get_origin().get_time())}.mseed".replace(":", "-")
    waveform_stream = get_waveforms_for_origin(
        FDSNClient(config.get_apollo_server_ip()), event.get_origin(), 1, 2
    )
    waveform_stream.write(rf"Braskem/waveform_data/{out_name}", format="MSEED")
