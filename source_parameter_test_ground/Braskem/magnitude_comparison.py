import glob
import json
import os

import matplotlib.pyplot as plt
import numpy as np

source_parameters = {}
mw_catalogs = glob.glob("Braskem\\qc_plots*\\source_parameters.json")
for catalog in mw_catalogs:
    label = catalog.split("\\")[1].split("qc_plots")[1].lstrip("_")
    if label == "":
        label = "normal"
    with open(catalog) as f:
        source_parameters[label] = json.load(f)

normal_keys = source_parameters["normal"].keys()
longer_s_window_keys = source_parameters["longer_s_window"].keys()
fig, ax = plt.subplots(3, 3, figsize=[12, 12], sharex=True, sharey=True)
datasets = list(source_parameters.keys())
for i_data, dataset_i in enumerate(datasets[:-1]):
    for j_data, dataset_j in enumerate(source_parameters.keys()):
        sub_ax = ax[i_data, j_data - 1]
        if j_data > i_data:
            common_keys = set(source_parameters[dataset_i]) & set(
                source_parameters[dataset_j]
            )
            sub_ax.set_aspect("equal")
            sub_ax.plot(
                [source_parameters[dataset_i][k]["Mw"] for k in common_keys],
                [source_parameters[dataset_j][k]["Mw"] for k in common_keys],
                "o",
            )
            x1, x2 = sub_ax.get_xlim()
            y1, y2 = sub_ax.get_ylim()
            low_point = min(x1, y1)
            high_point = max(x2, y2)
            sub_ax.yaxis.set_label_position("right")
            sub_ax.plot([-4, 4], [-4, 4], "-.", color="0.3")

            sub_ax.set_xlim(low_point, high_point)
            sub_ax.set_ylim(low_point, high_point)
            if i_data == 0:
                sub_ax.set_title(dataset_j.replace("_", " "))
            if j_data == 3:
                sub_ax.set_ylabel(dataset_i.replace("_", " "))
            sub_ax.text(
                0.1,
                0.8,
                f"{len(common_keys)} common events",
                transform=sub_ax.transAxes,
            )

ax[1, 0].set_axis_off()
ax[2, 0].set_axis_off()
ax[2, 1].set_axis_off()
fig.savefig("Braskem_Mw_comparison.png", bbox_inches="tight")
