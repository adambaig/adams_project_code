from obspy import UTCDateTime

from realtime_source_parameters.athena_query_helper import (
    get_new_events_since,
)
from realtime_source_parameters.waveforms_query_helper import get_waveforms_for_origin
from nmxseis.interact.athena import AthenaClient
from nmxseis.interact.fdsn import FDSNClient

ATHENA_URL = "https://athena.1210s.nanometrics.net"
ATHENA_API_KEY = "5e073359-5bb8-40d3-9236-e3c4aecdf0fc"
APOLLO_SERVER_IP = "35.155.92.119:8081"

athena_client = AthenaClient(ATHENA_URL, ATHENA_API_KEY)
events = get_new_events_since(
    athena_client, UTCDateTime("2022-01-20").timestamp, "MANUAL", 14
)
event = [e for e in events if e.get_event_id() == "48403"][0]
waveforms_stream = get_waveforms_for_origin(
    FDSNClient(APOLLO_SERVER_IP), event.get_origin(), 1, 2
)  # data requested for a 3 second window

# ESM04, ESM06, and ESM09 only have one sample, they should have 3000
print(waveforms_stream)
