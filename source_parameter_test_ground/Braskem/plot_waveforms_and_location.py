from __future__ import annotations

from collections import Counter, defaultdict
from glob import glob
import json
import os

import matplotlib.pyplot as plt
from NocMeta.Meta import NOC_META
import numpy as np
from obspy import UTCDateTime, read
import pyproj

from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)
from realtime_source_parameters.athena_query_helper import (
    get_single_event_origin,
    get_new_events_since,
    get_single_event_preferred_origin,
)

from nmxseis.interact.athena import AthenaClient
from nmxseis.interact.fdsn import FDSNClient
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.source_parameters import compute_source_parameters
from nmxseis.numerics.source_parameters.plotting import make_spectra_QC_plot
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D


from dataclasses import dataclass, field, replace
from logging import Logger, NullHandler
from typing import Callable, Iterable

import numpy as np
from numpy import pi as π
from numpy import sqrt
from numpy.linalg import norm
from numpy.ma import MaskedArray
from obspy import Inventory, Stream, UTCDateTime


from generalPlots import gray_background_with_grid

from nmxseis.model import Reprojector
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent, SourceParameters
from nmxseis.numerics._internal_util import find_largest_valid_span
from nmxseis.numerics.formulas import moment_to_mw
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.source_parameters._internal import StressDrop

from nmxseis.numerics.source_parameters import (
    get_valid_phases_for_arrivals,
    _calc_sp_spectra,
    get_station_enus,
    _rotate_stream_to_p_rays,
    get_signal_and_noise_windows,
    get_signal_and_noise_spectra,
    _StationInfo,
    _SPSpectra,
    _calc_source_params_from_spectra,
    _calc_prelim_corner,
    _get_valid_dis_spec_for_moment,
    _get_useable_bandwidth_for_stress_drop,
)


from read_inputs import read_GMM


TEST_DATA_FBK_STATION_XML = os.path.join("Braskem", "FBK_Full.xml")
TEST_DATA_FBK_CONFIG = os.path.join("Braskem", "fbk_config.json")
config = load_config_from_file(TEST_DATA_FBK_CONFIG)
inventory = get_station_inventory_from_file(TEST_DATA_FBK_STATION_XML)
athena_client = AthenaClient(config.get_athena_base_url(), config.get_athena_api_key())
reproject = pyproj.Proj(f"epsg:{config.get_epsg_code()}")
velocity_model_config = config.get_velocity_model_config()
fourier_spectra_config = config.get_fourier_spectra_config()
processing_config = config.get_processing_config()

layers = []
for layer in velocity_model_config.get_layered_velocity_model():
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )
velocity_model = VelocityModel1D(layers)

waveform_dir = r"Braskem\GMM_dataset\waveforms"
f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, velocity_model, phase
)
f_highpass = lambda x: 0.3


def get_nsl_from_channel_path(channel_path):
    return NSL(
        net=channel_path["networkCode"],
        sta=channel_path["stationCode"],
        loc=channel_path["locationCode"],
    )


min_snr = {"A": 3, "B": 3, "C": 1.5, "D": 0.5}
min_samples_per_bin = {"A": 5, "B": 3, "C": 2, "D": 1}
f_smooth_min = {"A": 1, "B": 5, "C": 10, "D": 10}
sp_results = {}

seed_files = glob(os.path.join(waveform_dir, "*.MSEED"))

# event_json = os.path.join("Braskem", "failed_sp_json", "003163.json")
# event_json = os.path.join("Braskem", "output_sp_json", "013729.json")
event_id = "20333"
event = get_single_event_preferred_origin(athena_client, event_id)
arrivals = defaultdict(dict)
origin = event.get_origin()
seed_name = os.path.join(waveform_dir, f"{event_id}.MSEED")
waveform_stream = read(seed_name)
nsls = [NSL.from_trace(tr) for tr in waveform_stream]
for arrival in origin.get_arrivals():
    pick = arrival.get_pick()
    nsl = get_nsl_from_channel_path(arrival.get_pick().get_channel_path())
    if nsl in nsls:
        arrivals[nsl][Phase.parse(pick.get_phase())] = UTCDateTime(pick.get_time())
nsls = [NSL.from_trace(tr) for tr in waveform_stream if NSL.from_trace(tr) in arrivals]
trace_lengths = [tr.stats.npts for tr in waveform_stream]

source = SeismicEvent(
    origin_time=origin.get_time(),
    lat=origin.get_latitude(),
    lon=origin.get_longitude(),
    reproject=reproject,
    depth_m=1000 * origin.get_depth(),
)

vm_at_source = velocity_model.look_up_enu(source.enu)


Q = velocity_model_config.get_Q()
kappa = processing_config.get_kappa()
lowpass_pct = processing_config.get_lowpass_percent()

min_d_hypo_m = processing_config.get_min_hypo_distance_m()
max_d_hypo_m = processing_config.get_max_hypo_distance_m()

f_smooth_max = 1000
f_smooth_n_bins = 41
n_trial_drops = 100

DEFAULT_S_WIN_PRE = 0.2
DEFAULT_P_WIN_PRE = 0.1
P_WIN_POST_COEF = 0.9
S_WIN_POST_COEF = 1.8

ROLL_BACK_CF_FOR_MOMENT = 1.5
ROLL_UP_CF_FOR_STRESS_DROP = 1.5
ROLL_UP_LOWPASS_FOR_STRESS_DROP = 0.9

P = Phase.P
S = Phase.S

valid_phases = get_valid_phases_for_arrivals(arrivals)
stream = waveform_stream.copy()

p_win_pre = DEFAULT_P_WIN_PRE
p_win_post = None
s_win_pre = DEFAULT_S_WIN_PRE
s_win_post = None
logger = None

station_enus = get_station_enus(nsls, inventory, source.reproject)

rays = {
    (nsl, ph): f_raytrace(source.enu, st_enu, ph)
    for nsl, st_enu in station_enus.items()
    # important: always need P rays for PVH rotation
    for ph in set(valid_phases) | {P}
}

nsls_without_waveforms = []
stream.merge(method=1)
stream.remove_response(inventory=inventory, output="ACC").detrend(type="linear")
rotated_traces = _rotate_stream_to_p_rays(
    stream, {nsl: ray for (nsl, ph), ray in rays.items() if ph.is_P}, inventory
)


signal_windows, noise_windows = get_signal_and_noise_windows(
    arrivals,
    source.enu,
    station_enus,
    f_raytrace,
    p_win_post=None,
    p_win_pre=p_win_pre,
    s_win_post=None,
    s_win_pre=s_win_pre,
)


for nsl, phase_arrivals in arrivals.items():
    # this really shouldn't be here... don't include stations without arrivals
    if not phase_arrivals:
        continue

    # we only have one arrival, need to make an estimate
    if len(phase_arrivals) == 1:
        # TODO we can be cleverer here. We have one arrival: we can scale the separation by
        #  actual_arrival/calculated_arrival
        enu = station_enus[nsl]
        separation = (
            f_raytrace(source.enu, enu, S).traveltime_sec
            - f_raytrace(source.enu, enu, P).traveltime_sec
        )
    # otherwise just take the gap
    else:
        separation = phase_arrivals[S] - phase_arrivals[P]
        if separation <= 0:
            continue

    for ph, t_arrival in phase_arrivals.items():
        win_pre = s_win_pre * separation if ph.is_S else p_win_pre * separation

        if ph.is_S:
            if s_win_post is None:
                win_post = S_WIN_POST_COEF * separation
            else:
                win_post = s_win_post
        else:
            if p_win_post is None:
                win_post = P_WIN_POST_COEF * separation
            else:
                win_post = p_win_post

            signal_window = (t_arrival - win_pre, t_arrival + win_post)
            signal_windows[nsl, ph] = signal_window

            noise_end = t_arrival - win_pre
            if ph.is_S:
                noise_end -= separation
            noise_window = (noise_end - win_pre * separation - win_post, noise_end)
            noise_windows[nsl, ph] = noise_window


from plotting import plot_rotated_waveforms_and_windows

fig = plt.figure(figsize=[14, 14])
x1, x2 = 197986.58688787775, 198854.75025504341
y1, y2 = 8932712.230823414, 8934324.638987586

dx = 1.1 * (x2 - x1)
dy = 1.1 * (y2 - y1)
dz = 1300

x1p = (x1 + x2) / 2 - 0.5 * dx
x2p = (x1 + x2) / 2 + 0.5 * dx
y1p = (y1 + y2) / 2 - 0.5 * dy
y2p = (y1 + y2) / 2 + 0.5 * dy
z1 = -1200
z2 = 100
ax_depth1 = fig.add_axes([0.05, 0.05, 0.9 * dx / (dx + dy), 0.9 * dz / (dy + dz)])
ax_depth2 = fig.add_axes(
    [0.05 + 0.9 * dx / (dx + dy), 0.05, 0.9 * dy / (dx + dy), 0.9 * dz / (dy + dz)]
)
ax_plan = fig.add_axes(
    [0.05, 0.05 + 0.9 * dz / (dz + dy), 0.9 * dx / (dx + dy), 0.9 * dy / (dz + dy)]
)

ax_waveforms = fig.add_axes([0.4, 0.5, 0.55, 0.45])

plot_rotated_waveforms_and_windows(
    rotated_traces, signal_windows, noise_windows, arrivals, ax_waveforms
)

from nmxseis.numerics.ray_modeling import _get_isotropic_vertical_slowness


ax_plan.set_aspect("equal")
ax_depth2.set_aspect("equal")
ax_depth1.set_aspect("equal")

ray_color = {Phase.P: "cyan", Phase.S: "forestgreen"}

for (nsl, ph), ray in rays.items():
    ax_plan.plot(
        station_enus[nsl][0],
        station_enus[nsl][1],
        "v",
        color="w",
        markeredgecolor="0.2",
        zorder=1,
    )
    ax_depth1.plot(
        station_enus[nsl][0],
        station_enus[nsl][2],
        "v",
        color="w",
        markeredgecolor="0.2",
        zorder=1,
    )
    ax_depth2.plot(
        station_enus[nsl][1],
        station_enus[nsl][2],
        "v",
        color="w",
        markeredgecolor="0.2",
        zorder=1,
    )
    v_source = vm_at_source.vp if ph.is_P else vm_at_source.vs
    ray
    initial_slowness = np.array(
        [
            ray.hrz_slowness_e,
            ray.hrz_slowness_n,
            np.sign(station_enus[nsl][2] - source.enu[2])
            * _get_isotropic_vertical_slowness(
                ray.hrz_slowness_e, ray.hrz_slowness_n, v_source
            ),
        ]
    )
    traced_ray = ray.one_way_isotropic_ray_trace(
        initial_slowness,
        source.enu,
        velocity_model,
        ph,
        final_elevation=station_enus[nsl][2],
    )
    ax_plan.plot(
        traced_ray.segments[:, 0],
        traced_ray.segments[:, 1],
        color=ray_color[ph],
        zorder=-1,
    )
    ax_depth1.plot(
        traced_ray.segments[:, 0],
        traced_ray.segments[:, 2],
        color=ray_color[ph],
        zorder=-1,
    )
    ax_depth2.plot(
        traced_ray.segments[:, 1],
        traced_ray.segments[:, 2],
        color=ray_color[ph],
        zorder=-1,
    )


ax_plan.plot(
    source.enu[0], source.enu[1], "o", color="orangered", markeredgecolor="k", zorder=1
)
ax_depth1.plot(
    source.enu[0], source.enu[2], "o", color="orangered", markeredgecolor="k", zorder=1
)
ax_depth2.plot(
    source.enu[1], source.enu[2], "o", color="orangered", markeredgecolor="k", zorder=1
)

ax_plan.set_xlim([x1, x2])
ax_plan.set_ylim([y1, y2])
ax_depth1.set_xlim([x1, x2])
ax_depth1.set_ylim([z1, z2])
ax_depth2.set_xlim([y1, y2])
ax_depth2.set_ylim([z1, z2])

for ax in [ax_plan, ax_depth1, ax_depth2]:
    gray_background_with_grid(ax, grid_spacing=100)
fig.savefig(f"Braskem//figures//waveforms_and_location_{event_id}.png")
