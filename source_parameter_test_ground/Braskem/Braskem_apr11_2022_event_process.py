from collections import defaultdict
import json
import os

import matplotlib.pyplot as plt
from NocMeta.Meta import NOC_META
import numpy as np
from obspy import UTCDateTime, read
import pyproj

from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)
from realtime_source_parameters.athena_query_helper import (
    get_single_event_origin,
    get_new_events_since,
    get_single_event_preferred_origin,
)
from realtime_source_parameters.waveforms_query_helper import get_waveforms_for_origin

from nmxseis.interact.athena import AthenaClient
from nmxseis.interact.fdsn import FDSNClient
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.source_parameters import compute_source_parameters
from nmxseis.numerics.source_parameters.plotting import make_spectra_QC_plot
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D


TEST_DATA_FBK_STATION_XML = os.path.join("Braskem", "FBK_Full.xml")
TEST_DATA_FBK_CONFIG = os.path.join("Braskem", "fbk_config.json")
config = load_config_from_file(TEST_DATA_FBK_CONFIG)
inventory = get_station_inventory_from_file(TEST_DATA_FBK_STATION_XML)
athena_client = AthenaClient(config.get_athena_base_url(), config.get_athena_api_key())
events = get_new_events_since(
    athena_client, UTCDateTime("2022-03-14").timestamp, "MANUAL", 50
)
velocity_model_config = config.get_velocity_model_config()
fourier_spectra_config = config.get_fourier_spectra_config()
processing_config = config.get_processing_config()

source_parameters = {}

reproject = pyproj.Proj(f"epsg:{config.get_epsg_code()}")

layers = []
for layer in velocity_model_config.get_layered_velocity_model():
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )
velocity_model = VelocityModel1D(layers)


f_highpass = lambda x: 1


f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, velocity_model, phase
)


def get_nsl_from_channel_path(channel_path):
    return NSL(
        net=channel_path["networkCode"],
        sta=channel_path["stationCode"],
        loc=channel_path["locationCode"],
    )


event = get_single_event_preferred_origin(athena_client, "65502")

mw_out, mw_unc = [], []
cf_p_out, cf_s_out = [], []
quality_score = []
min_snr = {"A": 3, "B": 3, "C": 1.5}
min_samples_per_bin = {"A": 5, "B": 3, "C": 2}
f_smooth_min = {"A": 1, "B": 5, "C": 10}
sp_results = {}

arrivals = defaultdict(dict)
origin = event.get_origin()


for arrival in origin.get_arrivals():
    pick = arrival.get_pick()
    nsl = get_nsl_from_channel_path(arrival.get_pick().get_channel_path())
    arrivals[nsl][Phase.parse(pick.get_phase())] = UTCDateTime(pick.get_time())

seed_name = os.path.join(
    "Braskem",
    "waveform_data",
    f"Braskem_data_{UTCDateTime(origin.get_time())}.mseed".replace(":", "-"),
)

waveform_stream = read(
    r"C:\Users\adambaig\logikdata\FBK\MSEED\Select\20220411.172419.307286_20220411.172619.307286.seed"
)

trace_lengths = [tr.stats.npts for tr in waveform_stream]

nsls = [NSL.from_trace(tr) for tr in waveform_stream]
source = SeismicEvent(
    origin_time=origin.get_time(),
    lat=origin.get_latitude(),
    lon=origin.get_longitude(),
    reproject=reproject,
    depth_m=1000 * origin.get_depth(),
)

vm_at_source = velocity_model.look_up_enu(source.enu)
for quality in ["A", "B", "C"]:
    source_parameters = compute_source_parameters(
        stream=waveform_stream.copy(),
        nsls=nsls,
        inventory=inventory,
        arrivals=arrivals,
        f_raytrace=f_raytrace,
        event=source,
        ev_vs=vm_at_source.vs,
        ev_vp=vm_at_source.vp,
        Q=velocity_model_config.get_Q(),
        kappa=processing_config.get_kappa(),
        f_highpass=lambda x: 0.3,
        lowpass_pct=processing_config.get_lowpass_percent(),
        min_snr=min_snr[quality],
        min_d_hypo_m=processing_config.get_min_hypo_distance_m(),
        max_d_hypo_m=processing_config.get_max_hypo_distance_m(),
        min_samples_per_bin=min_samples_per_bin[quality],
        f_smooth_min=f_smooth_min[quality],
        f_smooth_max=1000,
        f_smooth_n_bins=41,
        n_trial_drops=100,
    )
    if source_parameters is not None:
        sp_results[str(UTCDateTime(source.origin_time))] = {
            "mw": float(source_parameters.mw),
            "uncertainty": float(source_parameters.mw_uncertainty),
            "p corner": float(source_parameters.fc_P)
            if source_parameters.fc_P is not None
            else None,
            "s corner": float(source_parameters.fc_S)
            if source_parameters.fc_S is not None
            else None,
            "quality_score": quality,
            "lat": source.lat,
            "lon": source.lon,
            "depth": source.depth_m,
            "origin time": source.origin_time,
            "enu": source.enu.tolist(),
            "ml": event.get_origin().get_ml(),
        }
        break


fig, ax = plt.subplots(1, 2, figsize=[12, 8], sharex="all", sharey="all")
make_spectra_QC_plot(source_parameters, ax)
fig.text(0.5, 0.95, UTCDateTime(source.origin_time), ha="center", fontsize=14)
fig.text(
    0.5,
    0.92,
    f"Quality {quality[-1]}, $M_w = {source_parameters.mw:.2f}\pm {source_parameters.mw_uncertainty:.2f}$",
    ha="center",
    fontsize=12,
)
fig.savefig("Braskem//getting_better.png")


from nmxseis.numerics.source_parameters import (
    get_station_enus,
    get_signal_and_noise_windows,
    get_signal_and_noise_spectra,
    _SPSpectra,
)
from nmxseis.numerics.source_parameters import (
    get_valid_phases_for_arrivals,
    _rotate_stream_to_p_rays,
    _StationInfo,
)

station_enus = get_station_enus(nsls, inventory, source.reproject)

valid_phases = get_valid_phases_for_arrivals(arrivals), _rotate_stream_to_p_rays
rays = {
    (nsl, ph): f_raytrace(source.enu, st_enu, ph)
    for nsl, st_enu in station_enus.items()
    # important: always need P rays for PVH rotation
    for ph in [Phase.P, Phase.S]
}
stream = waveform_stream.copy()

nsls_without_waveforms = []
stream.remove_response(inventory=inventory, output="ACC").detrend(type="linear")
rotated_traces = _rotate_stream_to_p_rays(
    stream, {nsl: ray for (nsl, ph), ray in rays.items() if ph.is_P}, inventory
)

signal_windows, noise_windows = get_signal_and_noise_windows(
    arrivals,
    source.enu,
    station_enus,
    f_raytrace,
    p_win_post=None,
    p_win_pre=0.1,
    s_win_post=None,
    s_win_pre=0.1,
)
sta_infos = {}
for nsl, st_enu in station_enus.items():
    nsl_stream = nsl.select_from_stream(stream)
    if len(nsl_stream) == 0:
        nsls_without_waveforms.append(nsl)
        continue
    hypo = np.linalg.norm(st_enu - source.enu)
    p_window_lowest_freq = (
        1 / np.diff(signal_windows[nsl, Phase.P])
        if (nsl, Phase.P) in signal_windows
        else 0
    )
    s_window_lowest_freq = (
        1 / np.diff(signal_windows[nsl, Phase.S])
        if (nsl, Phase.S) in signal_windows
        else 0
    )
    sta_infos[nsl] = _StationInfo(
        nsl=nsl,
        enu=st_enu,
        highpass={
            Phase.P: max(f_highpass(float(hypo)), p_window_lowest_freq),
            Phase.S: max(f_highpass(float(hypo)), s_window_lowest_freq),
        },
        lowpass=(1.0 - 20.0 / 100.0) * nsl_stream[0].stats.sampling_rate / 2,
        is_on_surface=inventory.select(
            **nsl.as_obspy_dict()
        )  # TODO probably need to have a lookup for this eventually
        .select(starttime=source.origin_time, channel="*Z")[0][0][0]
        .depth
        < 1,
        ev_hyp=hypo,
    )

for nsl in nsls_without_waveforms:
    logger.warning(f"Got {len(nsls_without_waveforms)} stations without waveforms.")
    station_enus.pop(nsl)

spectra, noise_spectra = get_signal_and_noise_spectra(
    rotated_traces,
    signal_windows,
    noise_windows,
    1,
    1000,
    41,
)

test_spectra = _SPSpectra(
    source,
    sta_infos,
    rays,
    arrivals,
    0,
    np.inf,
    5,
    3,
    65,
    0,
    spectra=spectra,
    noise_spectra=noise_spectra,
)


from nmxseis.numerics.source_parameters import (
    _calculate_mean_spectra,
    find_largest_valid_span,
    _calc_prelim_corner,
    _get_valid_dis_spec_for_moment,
)


valid_phases = (Phase.P, Phase.S)

global_valid_bands = {
    ph: find_largest_valid_span(test_spectra.mean_qg_corrected[ph].acc)
    for ph in valid_phases
}

prelim_corners = {
    ph: _calc_prelim_corner(
        test_spectra.mean_qgk_corrected[ph], *global_valid_bands[ph]
    )
    for ph in valid_phases
}

moment_valid_bands = {
    ph: _get_valid_dis_spec_for_moment(
        test_spectra.mean_qgkr_corrected[ph], test_spectra.sta_infos, prelim_corners[ph]
    )
    for ph in valid_phases
}

from nmxseis.numerics.formulas import moment_to_mw, mw_to_moment

moment_samples = np.ma.stack([moment_valid_bands[ph] for ph in valid_phases])
moment_samples
np.ma.mean(moment_valid_bands[Phase.P])
mw = np.ma.mean([moment_to_mw(moment) for moment in moment_samples])
moment_to_mw(np.ma.median(moment_samples))


fig, ax = plt.subplots()
for nsl in nsls:
    if (nsl, Phase.P) in spectra:
        ax.loglog(spectra[nsl, Phase.P].freqs, spectra[nsl, Phase.P].dis)


fig, ax = plt.subplots()
for nsl in nsls:
    if (nsl, Phase.P) in spectra:
        ax.loglog(
            test_spectra.qgkr_corrected[nsl, Phase.P].freqs,
            test_spectra.qgkr_corrected[nsl, Phase.P].dis,
        )

nsl = NSL.parse("BR.ESM04.")

test = test_spectra.qgkr_corrected[nsl, Phase.P]
test._dis

fig, ax = plt.subplots()
for nsl in nsls:
    if (nsl, Phase.P) in spectra:
        ax.loglog(
            test_spectra.qgk_corrected[nsl, Phase.P].freqs,
            test_spectra.qgk_corrected[nsl, Phase.P].dis
            / Phase.P.avg_radiation_pattern,
        )
