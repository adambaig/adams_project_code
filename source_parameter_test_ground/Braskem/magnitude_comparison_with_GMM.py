import glob
import json
import os
import requests

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import pyproj


from realtime_source_parameters.offline_inputs_helper import (
    get_station_inventory_from_file,
)
from realtime_source_parameters.engine import get_station_locations
from NocMeta.Meta import NOC_META

source_parameters = {}
mw_catalogs = glob.glob("Braskem\\qc_plots*\\source_parameters.json")
for catalog in mw_catalogs:
    label = catalog.split("\\")[1].split("qc_plots")[1].lstrip("_")
    if label == "":
        label = "normal"
    with open(catalog) as f:
        source_parameters[label] = json.load(f)


if not (os.path.isfile(os.path.join(seed_dir, f"{event_id}.seed"))):
    continue


FBK_STATION_XML = os.path.join("Braskem", "FBK_Full.xml")
inventory = get_station_inventory_from_file(FBK_STATION_XML)


reproject = pyproj.Proj(f"epsg:{NOC_META['FBK']['epsg']}")
stations = {}
for net in inventory:
    for station in net:
        for channel in station:
            if "GPZ" in channel.code or "HHZ" in channel.code:
                east, north = reproject(
                    station.longitude, station.latitude, inverse=False
                )
                stations[f"{net.code}.{station.code}"] = {
                    "z": -channel.depth,
                    "e": east,
                    "n": north,
                }


def get_station_color(station_code):
    return cm.get_cmap("tab10")((int(station_code[-2:]) - 0.05) / 10)


station_symbols = {
    "BR.ESM01": "v",
    "BR.ESM02": "s",
    "BR.ESM03": "s",
    "BR.ESM04": "o",
    "BR.ESM05": "s",
    "BR.ESM06": "o",
    "BR.ESM07": "s",
    "BR.ESM08": "v",
    "BR.ESM09": "o",
    "BR.ESM10": "v",
}


def calc_distance(location_1, location_2):
    return np.sqrt(sum([(location_1[c] - location_2[c]) ** 2 for c in ["e", "n", "z"]]))


event_id = "48723"
for event_id, parameters in source_parameters["normal"].items():
    response = requests.get(
        rf"https://{NOC_META['FBK']['athIP']}/events/{event_id}.json?expand=all&apiKey={NOC_META['FBK']['athApi']}"
    )
    event = json.loads(response.content.decode("utf-8"))
    east, north = reproject(
        event["origins"][0]["longitude"], event["origins"][0]["latitude"], inverse=False
    )
    event_location = {"e": east, "n": north, "z": -1000 * event["origins"][0]["depth"]}

    gmm_mags = [
        m for m in event["origins"][0]["magnitudes"] if m["magnitude_type"] == "Mw"
    ]
    if len(gmm_mags) > 0:
        gmm_mw = gmm_mags[0]["value"]
        gmm_station_magnitudes = {
            "BR." + g["amplitude"]["channel"]["station"]["station_code"]: g["value"]
            for g in gmm_mags[0]["station_magnitudes"]
        }
        # print(gmm_mw, parameters['Mw'])
        parameters["GMM Mw"] = gmm_mw
        fig, ax = plt.subplots(figsize=[6, 4])
        for station in gmm_station_magnitudes:
            ax.plot(
                calc_distance(stations[station], event_location),
                gmm_station_magnitudes[station],
                marker=station_symbols[station],
                color=get_station_color(station),
            )
            ax.text(
                calc_distance(stations[station], event_location) + 10,
                gmm_station_magnitudes[station],
                station[-2:].lstrip("0"),
            )
        x1, x2 = ax.get_xlim()
        ax.plot([x1, x2], [parameters["Mw"], parameters["Mw"]], "k--")
        ax.plot([x1, x2], [parameters["GMM Mw"], parameters["GMM Mw"]], "k:")
        ax.set_xlim(x1, x2)
        ax.set_xlabel("distance (m)")
        ax.set_ylabel("moment magnitudes")
        ax.set_title(f"Braskem event id: {event_id}")
        fig.savefig(rf"Braskem\GMM_station_magnitudes\gmm_comparison_{event_id}.png")

fig, ax = plt.subplots()
ax.set_aspect("equal")
ax.plot(
    [p["Mw"] for p in source_parameters["normal"].values() if "GMM Mw" in p],
    [p["GMM Mw"] for p in source_parameters["normal"].values() if "GMM Mw" in p],
    "o",
)
x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()
low_point = min(x1, y1)
high_point = max(x2, y2)
ax.plot([-4, 4], [-4, 4], "-.", color="0.3")
ax.set_xlim(low_point, high_point)
ax.set_ylim(low_point, high_point)
ax.set_xlabel("RTSP Mw")
ax.set_ylabel("GMM Mw")
fig.savefig("Braskem//GMM_Mw_comparison.png")


fig_legend, ax_legend = plt.subplots()
(p1,) = ax_legend.plot(0, 0, "s", color="0.3")
(p2,) = ax_legend.plot(0, 0, "v", color="0.3")
(p3,) = ax_legend.plot(0, 0, "o", color="0.3")
ax_legend.legend(
    [p1, p2, p3], ["surface seismometer", "300 m seismometer", "900 m geophone"]
)
fig_legend.savefig("Braskem//station_legend.png")
