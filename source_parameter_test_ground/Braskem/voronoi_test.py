import matplotlib.pyplot as plt
import numpy as np

from scipy.spatial import Voronoi, voronoi_plot_2d

n= 34
np.random.seed(1138)
xx = np.random.randn(n)
yy = np.random.randn(n)

points = np.array([[x,y] for x,y in zip(xx,yy)])


fig, ax= plt.subplots()
vor = Voronoi(points)
voronoi_plot_2d(vor, ax =ax)
x_r, y_r = np.array([vor.vertices[i] for i in vor.regions[1] if i>-1]).T

ax.plot([*x_r,x_r[0]], [*y_r, y_r[0]],'r')

plt.show()

