import obspy
from obspy.core.inventory import Inventory, Network, Station, Channel, Site, Response
from obspy.clients.nrl import NRL

old_inv = obspy.read_inventory("Braskem\FBK_Full.xml")

inv = Inventory(networks=[], source="Adam")
net = Network(
    code="BR",
    stations=[],
)

response = Response.from_paz(zeros=[], poles=[], stage_gain=1)
stations = old_inv.select(network="BR", channel="[G,H][P,H][1,2,3,E,N,Z]")[0]

dip = {"E": 0.0, "N": 0.0, "Z": 90.0}
azimuth = {"E": 90.0, "N": 0.0, "Z": 0.0}
sample_rate = 1000

for station in stations:
    sta = Station(
        code=f"{station.code}",
        latitude=station.latitude,
        longitude=station.longitude,
        elevation=station.elevation,
        creation_date=obspy.UTCDateTime(2001, 1, 1),
        site=Site(name=station.code),
    )
    for ch in ["E", "N", "Z"]:
        cha = Channel(
            code=f"HH{ch}",
            location_code="",
            latitude=station.latitude,
            longitude=station.longitude,
            elevation=station.elevation,
            depth=station[0].depth,
            dip=dip[ch],
            azimuth=azimuth[ch],
            sample_rate=sample_rate,
            response=response,
        )
        sta.channels.append(cha)
    net.stations.append(sta)


inv.networks.append(net)
inv.write("Braskem///synthetic_FBK.xml", format="stationxml", validate=True)
