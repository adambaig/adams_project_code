from collections import defaultdict
import glob
import json
import os

import numpy as np
from obspy import read, UTCDateTime, Inventory
import pyproj

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.formulas import (
    moment_to_mw,
    mw_to_moment,
    calculate_source_radius_from_stress_drop,
    radius_corner_frequency_madariaga,
    convert_radius_corner_brune,
)
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.moment_tensor.inversion import inversion_matrix_row
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.simulation import generate_random_dc_moment_tensor
from nmxseis.numerics.simulation.event_waveform import (
    simulate_waveforms_for_array,
    simulate_waveform_from_source,
)
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

from nmxseis.numerics.source_parameters import compute_source_parameters
from nmxseis.numerics.source_parameters.plotting import make_spectra_QC_plot


from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)
from realtime_source_parameters.athena_query_helper import (
    get_single_event_origin,
    get_new_events_since,
    get_single_event_preferred_origin,
)

from read_inputs import read_GMM

NOISE_RMS = 0

TEST_DATA_FBK_STATION_XML = os.path.join("Braskem", "synthetic_FBK.xml")
TEST_DATA_FBK_CONFIG = os.path.join("Braskem", "fbk_config.json")
config = load_config_from_file(TEST_DATA_FBK_CONFIG)
inventory = get_station_inventory_from_file(TEST_DATA_FBK_STATION_XML)
athena_client = AthenaClient(config.get_athena_base_url(), config.get_athena_api_key())
reproject = pyproj.Proj(f"epsg:{config.get_epsg_code()}")
velocity_model_config = config.get_velocity_model_config()
fourier_spectra_config = config.get_fourier_spectra_config()
processing_config = config.get_processing_config()
layers = []
for layer in velocity_model_config.get_layered_velocity_model():
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )
velocity_model = VelocityModel1D(layers)

stations = {}
for network in inventory:
    for station in network:
        stations[f"{network.code}.{station.code}."] = {
            "latitude": station.latitude,
            "longitude": station.longitude,
            "elevation": station.elevation,
        }
eastings, northings = reproject(
    *zip(*[(v["longitude"], v["latitude"]) for v in stations.values()])
)
for i_station, station in enumerate(stations.values()):
    station["enu"] = np.array(
        [eastings[i_station], northings[i_station], station["elevation"]]
    )


f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, velocity_model, phase
)

waveform_dir = os.path.join("Braskem", "synthetic_waveform_data", "seed")
pick_dir = os.path.join("Braskem", "synthetic_waveform_data", "picks")
min_snr = {"A": 3, "B": 3, "C": 1.5}
min_samples_per_bin = {"A": 5, "B": 3, "C": 2}
f_smooth_min = {"A": 1, "B": 5, "C": 10}
sp_results = {}
seed_files = glob.glob(os.path.join(waveform_dir, "*.MSEED"))
for seed_name in seed_files:
    event_id = os.path.basename(seed_name).split(".")[0]
    wrapped_event = get_single_event_preferred_origin(athena_client, event_id)
    origin = wrapped_event.get_origin()
    source = SeismicEvent(
        origin_time=origin.get_time(),
        lat=origin.get_latitude(),
        lon=origin.get_longitude(),
        reproject=reproject,
        depth_m=1000 * origin.get_depth(),
    )
    arrivals = defaultdict(dict)
    pickfile = glob.glob(os.path.join(pick_dir, f"{event_id.zfill(10)}_*"))[0]
    with open(pickfile) as f:
        for line in f.readlines():
            line_split = line.split(",")
            arrivals[NSL.parse(line_split[0])][Phase(line_split[1])] = UTCDateTime(
                float(line_split[2])
            )
    nsls = np.unique([nsl for nsl in arrivals.keys()])
    waveform_stream = read(seed_name)
    vm_at_source = velocity_model.look_up_enu(source.enu)
    try:
        for quality in ["A", "B", "C"]:
            source_parameters = compute_source_parameters(
                stream=waveform_stream.copy(),
                nsls=nsls,
                inventory=inventory,
                arrivals=arrivals,
                f_raytrace=f_raytrace,
                event=source,
                ev_vs=vm_at_source.vs,
                ev_vp=vm_at_source.vp,
                Q=velocity_model_config.get_Q(),
                kappa=processing_config.get_kappa(),
                f_highpass=lambda x: 0.3,
                lowpass_pct=processing_config.get_lowpass_percent(),
                min_snr=min_snr[quality],
                min_d_hypo_m=processing_config.get_min_hypo_distance_m(),
                max_d_hypo_m=processing_config.get_max_hypo_distance_m(),
                min_samples_per_bin=min_samples_per_bin[quality],
                f_smooth_min=f_smooth_min[quality],
                f_smooth_max=1000,
                f_smooth_n_bins=41,
                n_trial_drops=100,
            )
            if source_parameters is not None:
                sp_results[str(UTCDateTime(source.origin_time))] = {
                    "mw": float(source_parameters.mw),
                    "uncertainty": float(source_parameters.mw_uncertainty),
                    "p corner": float(source_parameters.fc_P)
                    if source_parameters.fc_P is not None
                    else None,
                    "s corner": float(source_parameters.fc_S)
                    if source_parameters.fc_S is not None
                    else None,
                    "quality_score": quality,
                    "lat": source.lat,
                    "lon": source.lon,
                    "depth": source.depth_m,
                    "origin time": source.origin_time,
                    "enu": source.enu.tolist(),
                    # "ml": event.get_origin().get_ml(),
                    "arrivals": {
                        f"{nsl.__str__()}.{ph.name}": value.timestamp
                        for nsl, ph_value in arrivals.items()
                        for ph, value in ph_value.items()
                    },
                }
                break
    except TypeError:
        source_parameters = None
    if source_parameters is None:
        sp_results[str(UTCDateTime(source.origin_time))] = {
            "quality_score": "F",
            "lat": source.lat,
            "lon": source.lon,
            "depth": source.depth_m,
            "origin time": source.origin_time,
            "enu": source.enu.tolist(),
            # "ml": source.get_origin().get_ml(),
            "arrivals": {
                f"{nsl.__str__()}.{ph.name}": value.timestamp
                for nsl, ph_value in arrivals.items()
                for ph, value in ph_value.items()
            },
        }


with open("synthetic_results.json", "w") as f:
    json.dump(sp_results, f)
