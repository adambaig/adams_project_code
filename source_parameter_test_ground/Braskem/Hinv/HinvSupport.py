import os
import time
from decimal import Decimal
from subprocess import Popen, PIPE, STDOUT
from fnmatch import fnmatch

import numpy as np
from obspy.core.utcdatetime import UTCDateTime
from obspy.core.inventory import read_inventory

from General.BasicFunctions import loadArray2D, listToArray2D
from General.FileTransfer import makeClearDir, addInOutTag
from General.Projections import (
    calcXyzFromAziDipLen,
    calcAziFromXyz,
    calcDipFromXyz,
    calcDegDistViaLonLat,
)

# Convert a station inventory to a hypoInverse stalist.hinv file
def staXml2stalist(
    staXmlPath, stalistPath, acceptChannels=["?H?", "EN?"], realTime=False
):
    inv = read_inventory(staXmlPath)
    # Extract wanted info from the station xml
    info = [
        [
            sta.code,
            net.code,
            cha.location_code,
            cha.code,
            cha.longitude,
            cha.latitude,
            cha.elevation,
            cha.start_date,
            cha.end_date,
        ]
        for net in inv
        for sta in net
        for cha in sta
        if True in [fnmatch(cha.code, pattern) for pattern in acceptChannels]
    ]
    # Create lines for the output file
    lines = []
    for sta, net, loc, cha, lon, lat, ele, start, end in info:
        # If the channel end time is defined as inactivate (and in realtime mode), skip
        if end is not None and realTime:
            if time.time() > end.timestamp + 3600:  # Give an hour lenience
                continue
        # Format NSLC
        net = "{:<3s}".format(net)
        sta = "{:<5s}".format(sta)
        loc = "{:<2s}".format(loc)
        cha = "{:<3s}".format(cha)
        loc = "--" if loc == "  " else loc
        # Format lon/lat/elevation
        lonDeg = "{:>3d}".format(int(abs(lon)))
        lonMin = "{:>7.4f}".format(60 * (abs(lon) % 1))
        latDeg = "{:>3d}".format(int(abs(lat)))
        latMin = "{:>7.4f}".format(60 * (abs(lat) % 1))
        ele = "{:>4d}".format(np.clip(int(round(ele)), -999, 9999)) + "1.0"
        # Get the lon/lat markers
        lonMarker = "W" if lon < 0 else "E"
        latMarker = "N" if lat >= 0 else "S"
        lines.append(
            [
                sta,
                net,
                cha,
                latDeg,
                latMin + latMarker + lonDeg,
                lonMin + lonMarker + ele,
                "    0.00  0.00  0.00  0.00 1  0.00" + loc,
            ]
        )
    # Create file
    outFile = open(stalistPath, "w")
    for line in lines:
        outFile.write(" ".join(line) + "\n")
    outFile.close()


# Generate Hinv inputs...
def formHinvInputs(
    workDir,
    inPickDir,
    inEveSum,
    catchOnlyS=True,
    useMarker=None,
    statDict={},
    inOutTag=None,
):
    """
    workDir: Where Hinv metadata is held
    inPickDir: Where the picks files are to be converted to hypoInv inputs
    inEveSum: Initial locations in event summary format
    catchOnlyS: True if wanting to add a P-pick when only S-picks are present
    useMarker: If not "None" only take picks where the 2nd character of phase is this character
    statDict: Statistics carried over from NLLoc, see NllSupport
    inOutTag: Additional string to attach to the input/output directories
    """
    # Load pick files, linked by its ID
    pickFileIDs = []
    pickSets = []
    pickFiles = [f for f in os.listdir(inPickDir) if f.split(".")[-1] == "picks"]
    for aFile in pickFiles:
        picks = np.genfromtxt(inPickDir + "/" + aFile, delimiter=",", dtype="U32")
        if useMarker is not None:
            picks = np.array(
                [entry for entry in picks if entry[1][1] == useMarker], dtype="U32"
            )
        # Ensure only 1 character for phase going forward
        picks[:, 1] = np.array([phase[0] for phase in picks[:, 1]])
        pickSets.append(picks)
        pickFileIDs.append(int(aFile.split("_")[0]))

    # Make a reference of nsl to available channels
    nslChaRef = {}
    with open(workDir + "/stalist.hinv", "r") as aFile:
        for line in aFile.read().split("\n"):
            if len(line) < 82:
                continue
            nsl = ".".join(
                [
                    line[idxs[0] : idxs[1]].replace(" ", "").replace("--", "")
                    for idxs in [[6, 8], [0, 5], [80, 82]]
                ]
            )
            if nsl in nslChaRef.keys():
                nslChaRef[nsl].append(line[10:13])
            else:
                nslChaRef[nsl] = [line[10:13]]

    # Clear the previous input directory
    hypoInDir = addInOutTag(workDir + "/HinvIn", inOutTag)
    makeClearDir(hypoInDir)

    # Loop through each event making the PHS files
    # Event information
    for sumLine in inEveSum:
        eveID = int(sumLine[0])
        # Load the pick information for this event
        picks = pickSets[pickFileIDs.index(int(sumLine[0]))]
        phases = [entry[1][0] for entry in picks]
        # Form the content for the PHS file
        if eveID in statDict.keys():
            eveStatDict, pickStatDict = statDict[eveID]
        else:
            # Let user know if stats were not pushed through, but some were given
            if len(statDict.keys()) != 0:
                print("Did not have NLL stats for " + str(eveID))
            eveStatDict, pickStatDict = defaultEveStatDict(), {}
        eventLines = formPhsEventLines(sumLine, phases, eveStatDict=eveStatDict)
        arrivalLines = formPhsArrivalLines(
            picks, nslChaRef, catchOnlyS=catchOnlyS, pickStatDict=pickStatDict
        )
        phsContent = "\n".join(eventLines[:2] + arrivalLines + eventLines[2:])
        # Write the file
        phsPath = hypoInDir + "/" + sumLine[0] + ".PHS"
        with open(phsPath, "w") as aFile:
            aFile.write(phsContent)


# Default event statistics dictionary if none provided
def defaultEveStatDict():
    eveStatDict = {
        "Trms": 0.0,
        "minDist": 0.0,
        "aziGap": 0.0,
        "errDep": 0.0,
        "errAzi": np.zeros((3), dtype=float),
        "errDip": np.zeros((3), dtype=float),
        "errLen": np.zeros((3), dtype=float),
    }
    return eveStatDict


# Convert float to int, moving decimal place by 2
def moveDecimalBy2(val):
    return int(float(val) * 100)


# Convert float to int, moving decimal place by 1
def moveDecimalBy1(val):
    return int(float(val) * 10)


# Form the lines relating to the event data (id, origin time, location)
def formPhsEventLines(eveSum, phases, eveStatDict=defaultEveStatDict()):
    pkid, eveLon, eveLat, eveDep, eveMag, eveTimestamp = eveSum[:6]
    pkid = int(pkid)
    eveLon, eveLat = float(eveLon), float(eveLat)
    # Format origin time
    try:
        eveTimeUtc = UTCDateTime(Decimal(eveTimestamp))
    except:
        eveTimeUtc = UTCDateTime(eveTimestamp)
    eveTimeFull = eveTimeUtc.strftime("%Y%m%d%H%M%S%f")[:-4]
    # Format the event longitude/latitude
    markerLon = " " if eveLon < 0 else "E"
    markerLat = " " if eveLat >= 0 else "S"
    eveLatDeg = int(eveLat)
    eveLatMin = (abs(eveLat) % 1) * 60
    eveLonDeg = int(eveLon)
    eveLonMin = (abs(eveLon) % 1) * 60
    eveLine = " " * 164
    # Format each entry of the header
    # Refer to  https://pubs.usgs.gov/of/2002/0171/pdf/of02-171.pdf , pages: 98-99
    for val, idxs, aFormat, preFunc in [
        [eveTimeFull, [0, 16], "{:16s}", str],
        [eveLatDeg, [16, 18], "{:02d}", abs],
        [markerLat, [18, 19], "{:1s}", str],
        [eveLatMin, [19, 23], "{:>4d}", moveDecimalBy2],
        [eveLonDeg, [23, 26], "{:03d}", abs],
        [markerLon, [26, 27], "{:1s}", str],
        [eveLonMin, [27, 31], "{:>4d}", moveDecimalBy2],
        [eveDep, [31, 36], "{:>5d}", moveDecimalBy2],
        [0, [36, 39], "{:03d}", int],
        [len(phases), [39, 42], "{:>3d}", int],
        [eveStatDict["aziGap"], [42, 45], "{:>3d}", int],
        [eveStatDict["minDist"], [45, 48], "{:>3d}", int],
        [np.clip(eveStatDict["Trms"], -9.99, 9.99), [48, 52], "{:>4d}", moveDecimalBy2],
        [eveStatDict["errAzi"][2], [52, 55], "{:>3d}", int],
        [eveStatDict["errDip"][2], [55, 57], "{:>2d}", int],
        [eveStatDict["errLen"][2], [57, 61], "{:>4d}", moveDecimalBy2],
        [eveStatDict["errAzi"][1], [61, 64], "{:>3d}", int],
        [eveStatDict["errDip"][1], [64, 66], "{:>2d}", int],
        [eveStatDict["errLen"][1], [66, 70], "{:>4d}", moveDecimalBy2],
        [0, [70, 73], "{:>3d}", int],
        [eveStatDict["errLen"][0], [76, 80], "{:>4d}", moveDecimalBy2],
        ["#", [81, 82], "{:1s}", str],
        [np.sum(np.array(phases, dtype="U1") == "S"), [82, 85], "{:>3d}", int],
        [0, [85, 89], "{:>4d}", moveDecimalBy2],
        [eveStatDict["errDep"], [89, 93], "{:>4d}", moveDecimalBy2],
        [0, [93, 96], "{:>3d}", int],
        [0, [96, 100], "{:>4d}", int],
        [0, [100, 104], "{:>3d}", int],
        [0, [104, 107], "{:>3d}", int],
        [0, [107, 110], "{:>3d}", int],
        ["Che", [110, 113], "{:3s}", str],
        [len(phases), [118, 121], "{:>3d}", int],
        [pkid, [136, 146], "{:>10d}", int],
    ]:
        # Take rightmost string if somehow went beyond wanted length
        eveLine = (
            eveLine[: idxs[0]]
            + aFormat.format(preFunc(val))[idxs[0] - idxs[1] :]
            + eveLine[idxs[1] :]
        )
    outLines = [eveLine]
    outLines += ["$1", " " * 66 + "{:<10}".format(pkid), "$"]
    return outLines


# Form the lines relating to the arrivals...
# ...could use some time reformatting to read easier
def formPhsArrivalLines(picks, nslChaRef, pickStatDict={}, catchOnlyS=True):
    outLines = []
    pEntries = (
        []
    )  # To hold all entries (formatted for P phase) in case of only S phases
    for pick in picks:
        nsl, phase, pickTimestamp = pick
        net, sta, loc = nsl.split(".")
        # Get an appropriate channel for this station
        if phase[0] == "P":
            cha = sorted(nslChaRef[nsl])[-1]
        else:
            cha = sorted(nslChaRef[nsl])[0]
        if loc == "":
            loc = "--"
        # Extract the pick stats
        staPha = sta + phase[0]
        if staPha in pickStatDict.keys():
            resid, dist, azi = pickStatDict[staPha][:3]
        else:
            resid, dist, azi = 0, 0, 0
        # Clip residual to stop it from going into wrong column
        resid = np.clip(resid, -9.99, 9.99)
        # Format pick time
        pick_time_utc = UTCDateTime(Decimal(pickTimestamp))
        pick_time_til_min = pick_time_utc.strftime("%Y%m%d%H%M")
        pick_time_s_ms_round = str(round(float(pick_time_utc.strftime("%S.%f")), 2))
        pick_time_s_ms = pick_time_s_ms_round.split(".")[0].zfill(2) + "{:0<2}".format(
            pick_time_s_ms_round.split(".")[1]
        )
        # Get the weight code
        if "_Noise" in phase:
            weightCode = "2"
        else:
            weightCode = "0"
        entryP = (
            "{:<5}".format(sta)
            + "{:<2}".format(net)
            + "  "
            + cha
            + "  P "
            + weightCode
            + pick_time_til_min
            + " "
            + pick_time_s_ms
            + " " * 15
            + "6"
            + 61 * " "
            + "{:<2}".format(loc)
        )
        entryS = (
            "{:<5}".format(sta)
            + "{:<2}".format(net)
            + "  "
            + cha
            + " " * 4
            + "6"
            + pick_time_til_min
            + 13 * " "
            + pick_time_s_ms
            + " S "
            + weightCode
            + 61 * " "
            + "{:<2}".format(loc)
        )
        # Update the rms
        if phase[0] == "P":
            entry = entryP
            residIdxs = [34, 38]
        elif phase[0] == "S":
            entry = entryS
            residIdxs = [50, 54]
        else:
            print("Got phase " + phase[0] + " expected P or S in phase creation")
            continue
        # Update the rms, distance and azimuth (event to station) value
        for val, idxs, aFormat, preFunc in [
            [resid, residIdxs, "{:>4d}", moveDecimalBy2],
            [dist, [74, 78], "{:>4d}", moveDecimalBy1],
            [azi, [91, 94], "{:>3d}", int],
        ]:
            # Take rightmost string if somehow went beyond wanted length
            entry = (
                entry[: idxs[0]]
                + aFormat.format(preFunc(val))[idxs[0] - idxs[1] :]
                + entry[idxs[1] :]
            )
        pEntries.append(entryP)
        outLines.append(entry)
        outLines.append("$")

    # If no P phases, add a low weight P phase at earliest time
    seenPhases = np.array([aPhase[0] for aPhase in picks[:, 1]])
    if np.sum(seenPhases == "S") == len(seenPhases) and catchOnlyS:
        # Get entry with earliest time
        entry = pEntries[np.argmin(picks[:, 2].astype(float))]
        # Replace the previous weights
        entry = entry.replace("  P 0", "  P 3").replace("  P 2", "  P 3")
        outLines.append(entry)
        outLines.append("$")
    return outLines


# Parse the hypoOut for event summary infomation
def runHypoInv(
    hinvRunDir, inOutTag=None, hinvExeDir=os.path.dirname(os.path.realpath(__file__))
):

    # Folder name of input/output
    folderIn = addInOutTag("HinvIn", inOutTag)
    folderOut = addInOutTag("HinvOut", inOutTag)

    # Paths of input/output
    hinvInDir = hinvRunDir + "/" + folderIn
    hinvOutDir = hinvRunDir + "/" + folderOut

    # Clear the previously made output files
    makeClearDir(hinvOutDir)

    # Use .exe made for the operating system
    if os.name == "nt":
        exeFile = "hyp2000_windows.exe"
    else:
        exeFile = "hyp2000_linux.exe"

    # Read in the commands to call
    commands = []
    with open(hinvRunDir + "/hypinst.txt", "r") as aFile:
        for line in aFile.read().split("\n"):
            if len(line) == 0:
                continue
            elif line[0] in [" ", "*"]:
                continue
            commands.append(line)

    failFiles = []
    for aFile in sorted(os.listdir(hinvInDir)):
        # Run commands from the hinv directory
        p = Popen(
            [hinvExeDir + "/" + exeFile],
            stdout=PIPE,
            stdin=PIPE,
            stderr=STDOUT,
            cwd=hinvRunDir,
        )
        for cmd in commands:
            # Replace relevant commands with wanted file name
            if cmd[:3] == "PHS":
                cmd = "PHS './" + folderIn + "/" + aFile + "'"
            elif cmd[:3] == "ARC":
                cmd = "ARC './" + folderOut + "/" + aFile + "'"
            p.stdin.write((cmd + "\n").encode("utf-8"))
        p.stdin.close()
        stdOutString = p.stdout.read().decode("utf-8")

        if "ABANDON EVENT" in stdOutString:
            failFiles.append(aFile)

        # Print if there was an error that occured (or file is empty)
        outFilePath = hinvRunDir + "/" + folderOut + "/" + aFile
        if "bad data" in stdOutString.lower():
            print(aFile, stdOutString.replace("COMMAND?", ""))
        elif "error" in stdOutString.lower() or os.stat(outFilePath).st_size == 0:
            print(aFile, stdOutString.replace("COMMAND?", ""))
        # Ensure that the external ID is in the right place
        else:
            with open(outFilePath, "r") as hinvOutFile:
                info = hinvOutFile.readlines()
            # Get the external ID from the last line
            for aLine in info[::-1]:
                aLine = aLine.replace("\n", "").replace(" ", "")
                if len(aLine) == 0:
                    continue
                if aLine[0] == "$":
                    continue
                extID = aLine
                break
            info[0] = info[0][:136] + extID.rjust(10) + info[0][146:]
            with open(outFilePath, "w") as hinvOutFile:
                hinvOutFile.writelines(info)

    # Return the file names of inputs which failed to locate
    return failFiles


# Generate an event summary file from a set of Hinv output files
def getEveSumFromHinvOut(hinvRunDir, inOutTag=None, includeStats=[]):
    hinvOutDir = addInOutTag(hinvRunDir + "/HinvOut", inOutTag)

    # Loop through all output files getting the summary line
    eveSum = []
    for aFile in sorted(os.listdir(hinvOutDir)):
        eveID = aFile.split(".")[0]
        # Read in event location
        with open(hinvOutDir + "/" + aFile) as f:
            info = [x.strip("\n") for x in f.readlines()]
        try:
            eveLine = info[0].replace(" ", "0")
        except:
            print("HINV, Event ID: " + eveID + " was not relocated")
            continue
        eveTime = UTCDateTime().strptime(eveLine[:16] + "0000", "%Y%m%d%H%M%S%f")
        latDeg, latMin, lonDeg, lonMin = (
            int(eveLine[16:18]),
            float(eveLine[19:23]) / 100.0,
            int(eveLine[23:26]),
            float(eveLine[27:31]) / 100.0,
        )
        eveLat, eveLon = latDeg + latMin / 60.0, lonDeg + lonMin / 60.0
        eveDep = float(eveLine[31:36]) * 0.01
        tRms = float(eveLine[48:52]) * 0.01
        # Flags to place event in correct quadrant
        if eveLine[18] == "S":
            eveLat *= -1.0
        if eveLine[26] != "E":
            eveLon *= -1.0
        eveSum.append([eveID, eveLon, eveLat, eveDep, "-9", Decimal(eveTime.timestamp)])
        if "tRms" in includeStats:
            eveSum[-1].append(tRms)
    eveSum = np.array(eveSum, dtype="U32")
    # If all events failed to relocate, ensure proper array dimension
    if 0 in eveSum.shape or len(eveSum) == 0:
        eveSum = np.empty((0, 6 + len(includeStats)), dtype="U32")
    return eveSum


# Get the event summary line from the contents of an arc file
def getArcSumLine(info):
    eveLine = info[0].replace(" ", "0")
    eveTime = UTCDateTime().strptime(eveLine[:16] + "0000", "%Y%m%d%H%M%S%f")
    latDeg, latMin, lonDeg, lonMin = (
        int(eveLine[16:18]),
        float(eveLine[19:23]) / 100.0,
        int(eveLine[23:26]),
        float(eveLine[27:31]) / 100.0,
    )
    eveLat, eveLon = latDeg + latMin / 60.0, lonDeg + lonMin / 60.0
    # Depths can be negative if coming from a different locator
    try:
        eveDep = float(eveLine[31:36]) * 0.01
    except:
        eveDep = -1 * float(eveLine[31:36].replace("-", "0")) * 0.01
    # Flags to place event in correct quadrant
    if eveLine[18] == "S":
        eveLat *= -1.0
    if eveLine[26] != "E":
        eveLon *= -1.0
    eveLon, eveLat, eveDep = (
        "{:.6f}".format(eveLon),
        "{:.6f}".format(eveLat),
        "{:6.3f}".format(eveDep),
    )
    return eveLon, eveLat, eveDep, "-8.00", eveTime


# Copy an arc file to a new path with new name, and external ID updated
def copyHinvWithExtID(inPath, outPath, extID):
    with open(inPath) as f:
        info = [x.strip("\n") for x in f.readlines()]
    # Insert the external ID on the first and second last line
    extID = str(extID).rjust(10)
    info[0] = info[0][:136] + extID + info[0][146:]
    spacer = " " * 66
    if len(info) > 2 and info[-2][:66] == spacer:
        info[-2] = spacer + extID
    # Write the output to wanted path
    outFile = open(outPath, "w")
    outFile.write("\n".join(info))
    outFile.close()


# Get an event summary dictionary from a Hinv output directory
def getEveInfoFromHinv(hinvOutDir):
    # Move decimal two places to the left
    def moveDecimalBy2(temp):
        return float(temp) / 100.0

    # Loop through all output files getting the summary line
    eveSum = []
    statDict = {}  # To hold extra information on error, key is PKID
    for aFile in sorted(os.listdir(hinvOutDir)):
        if aFile.split(".")[-1].lower() not in ["phs", "arc"]:
            continue
        # Load the file
        with open(hinvOutDir + "/" + aFile) as f:
            info = [x.strip("\n") for x in f.readlines()]
        # Get basic event info
        pkid = aFile.split(".")[0].replace("qid", "")
        eveX, eveY, eveDep, eveMag, eveTime = getArcSumLine(info)
        # Get the error ellipsoid information
        eveStatDict = {}
        for key in ["errLen", "errDip", "errAzi"]:
            eveStatDict[key] = np.ones((3)) * -999
        eveLine = info[0].replace(" ", "0")
        for key, keyArrIdx, lineIdxs, postFunc in [
            ["aziGap", None, [42, 45], int],
            ["minDist", None, [45, 48], int],
            ["errAzi", 2, [52, 55], int],
            ["errDip", 2, [55, 57], int],
            ["errLen", 2, [57, 61], moveDecimalBy2],
            ["errAzi", 1, [61, 64], int],
            ["errDip", 1, [64, 66], int],
            ["errLen", 1, [66, 70], moveDecimalBy2],
            ["errLen", 0, [76, 80], moveDecimalBy2],
            ["errDep", None, [89, 93], moveDecimalBy2],
        ]:
            val = postFunc(eveLine[lineIdxs[0] : lineIdxs[1]])
            # If the value is not input to an array
            if keyArrIdx is None:
                eveStatDict[key] = val
            # Put value into specified array position
            else:
                eveStatDict[key][keyArrIdx] = val
        # Fill the azimuth/dip of the minimum error direction
        xyzInter = calcXyzFromAziDipLen(
            eveStatDict["errAzi"][1], eveStatDict["errDip"][1], eveStatDict["errLen"][1]
        )
        xyzMax = calcXyzFromAziDipLen(
            eveStatDict["errAzi"][2], eveStatDict["errDip"][2], eveStatDict["errLen"][2]
        )
        xyzMin = np.cross(xyzInter, xyzMax)
        eveStatDict["errAzi"][0] = calcAziFromXyz(xyzMin)
        eveStatDict["errDip"][0] = calcDipFromXyz(xyzMin)
        # Get the picking information
        pickStatArr = getPickStat(info)
        # Calculate the Trms
        eveStatDict["Trms"] = (
            np.sum(np.array([tRes for nsl, pha, pickTime, tRes in pickStatArr]) ** 2)
            / len(pickStatArr)
        ) ** 0.5
        # Append this event information with pkid as key
        statDict[int(pkid)] = [eveStatDict, pickStatArr]
        eveSum.append([pkid, eveX, eveY, eveDep, "-9", Decimal(eveTime.timestamp)])
    eveSum = np.array(eveSum, dtype="U32")
    if len(eveSum) == 0:
        eveSum = np.empty((0, 6), dtype="U32")
    return eveSum, statDict


# Read pick nsl, phase, time, and residuals
def getPickStat(info):
    pickStat = []
    # Parse lines with phase information
    for aLine in info[1:]:
        if len(aLine) == 0:
            continue
        elif aLine[0] in [" ", "$"]:
            continue
        net, sta, loc = (
            aLine[5:7].replace(" ", ""),
            aLine[:5].replace(" ", ""),
            aLine[111:113],
        )
        loc = loc.replace(" ", "").replace("--", "")
        nsl = ".".join([net, sta, loc])
        # Check if was a P or S pick and collect residual value
        pickTime = UTCDateTime().strptime(aLine[17:29], "%Y%m%d%H%M")
        if "P" in aLine[13:15]:
            phase = aLine[13:15].replace(" ", "")
            seconds = float(aLine[29:34]) / 100.0
            tRes = int(aLine[34:38]) / 100.0
        elif "S" in aLine[46:48]:
            phase = aLine[46:48].replace(" ", "")
            seconds = float(aLine[41:46]) / 100.0
            tRes = int(aLine[50:54]) / 100.0
        else:
            continue
        # Add seconds to the pick time, convert to timestamp
        pickTime += seconds
        pickTime = Decimal(pickTime.timestamp)
        # Add pick to output
        pickStat.append([nsl, phase, pickTime, tRes])
    return pickStat


# Loop through the basic picking information from pick stats
def hinvBasePickStatLoop(pickStatArr):
    for nsl, pha, pickTime, tRes in pickStatArr:
        yield nsl, pha, pickTime, tRes


# Get the hypoinverse reference elevation with the given NSLs being used
def getHinvElevRef(defaultLocLookup, eveLon, eveLat, useNSLs):
    """
    defaultLocLookup: location dictionary lookup output by "formChaLocLookups" of "AtlasSupport.py",
                      with reference elevation of 0 m asl
    """
    # Extract the stations locations which were used for picking
    staLocs = np.array([defaultLocLookup[nsl] for nsl in useNSLs], dtype=float)
    # Get nearest 5 stations if more than 5
    if len(staLocs) > 5:
        # Calculate the distances
        degEpiDists = calcDegDistViaLonLat(staLocs[:, 0], staLocs[:, 1], eveLon, eveLat)
        # Sort and take nearest five
        staLocs = staLocs[np.argsort(degEpiDists)][:5]
    # Take mean of these stations elevations, also flip axis back (to be positive upwards)
    elevRef = np.mean(staLocs[:, 2]) * -1
    return elevRef


# Get velocity model information...
# ...note that it returns the velocity model which is listed first in the crustalModel file
# ...returns in units of km and seconds
def getVelInfo(hypInstPath):
    hinvDir = os.path.dirname(hypInstPath)
    modelPath = getFirstLineFromText(hypInstPath, lineStart="@")
    velPath = getFirstLineFromText("%s/%s" % (hinvDir, modelPath), lineStart="CRH")
    if velPath is None:
        return None
    velPath = hinvDir + "/" + velPath.split(" ")[-1].replace("'", "")
    # Also get the  P to S velocity ratio
    psRatio = float(getFirstLineFromText(hypInstPath, lineStart="POS"))
    # Format the velocity information for use in the moment tensor code
    velArr = listToArray2D(
        np.genfromtxt(velPath, skip_header=1, dtype="U32"), expectCol=2, dtype=float
    )
    velInfo = {"vp": velArr[:, 0], "vs": velArr[:, 0] / psRatio, "top": velArr[:, 1]}
    return velInfo


# Get the first line of text from a file which starts with a given string
def getFirstLineFromText(path, lineStart="DNE"):
    # Return nothing if the file does not exist
    if not os.path.isfile(path):
        return None
    # Otherwise loop through the file lines searching for wanted string
    with open(path, "r") as aFile:
        for aLine in aFile.readlines():
            if aLine.startswith(lineStart):
                return aLine[len(lineStart) :].strip()


# Hack for hypoinverse for unstable solution (very rarely observations will be ignored)
# Move the first/last half of pick times forward/backward 0.01 seconds
def jigglePickTimes(pickDir, eveIDs):
    for aFile in os.listdir(pickDir):
        # Only look at pick files with the given IDs
        if ".picks" not in aFile:
            continue
        aID = int(aFile.split("_")[0])
        if aID not in eveIDs:
            continue
        # Load old pick times
        aPath = pickDir + "/" + aFile
        picks = loadArray2D(aPath, expectCol=3)
        # Jiggle the pick times
        pickTimes = picks[:, 2].astype(float)
        for i in range(len(picks)):
            if i < len(picks) / 2:
                pickTimes[i] += 0.01
            else:
                pickTimes[i] -= 0.01
        picks[:, 2] = pickTimes.astype("U32")
        # Save with new pick times
        np.savetxt(aPath, picks, delimiter=",", fmt="%s")
