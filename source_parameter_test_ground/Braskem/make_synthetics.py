import os

import numpy as np
from obspy import read, UTCDateTime, Inventory
import pyproj

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.formulas import (
    moment_to_mw,
    mw_to_moment,
    calculate_source_radius_from_stress_drop,
    radius_corner_frequency_madariaga,
    convert_radius_corner_brune,
)
from nmxseis.numerics.fourier.motion_spectrum import MotionSpectrum
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.moment_tensor.inversion import inversion_matrix_row
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.simulation import generate_random_dc_moment_tensor
from nmxseis.numerics.simulation.event_waveform import (
    simulate_waveforms_for_array,
    simulate_waveform_from_source,
)
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

from nmxseis.numerics.source_parameters import compute_source_parameters
from nmxseis.numerics.source_parameters.plotting import make_spectra_QC_plot


from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)
from realtime_source_parameters.athena_query_helper import (
    get_single_event_origin,
    get_new_events_since,
    get_single_event_preferred_origin,
)

from read_inputs import read_GMM

NOISE_RMS = 0

TEST_DATA_FBK_STATION_XML = os.path.join("Braskem", "synthetic_FBK.xml")
TEST_DATA_FBK_CONFIG = os.path.join("Braskem", "fbk_config.json")
config = load_config_from_file(TEST_DATA_FBK_CONFIG)
inventory = get_station_inventory_from_file(TEST_DATA_FBK_STATION_XML)
athena_client = AthenaClient(config.get_athena_base_url(), config.get_athena_api_key())
reproject = pyproj.Proj(f"epsg:{config.get_epsg_code()}")
velocity_model_config = config.get_velocity_model_config()
fourier_spectra_config = config.get_fourier_spectra_config()
processing_config = config.get_processing_config()
layers = []
for layer in velocity_model_config.get_layered_velocity_model():
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )
velocity_model = VelocityModel1D(layers)

stations = {}
for network in inventory:
    for station in network:
        stations[f"{network.code}.{station.code}."] = {
            "latitude": station.latitude,
            "longitude": station.longitude,
            "elevation": station.elevation,
        }
eastings, northings = reproject(
    *zip(*[(v["longitude"], v["latitude"]) for v in stations.values()])
)
for i_station, station in enumerate(stations.values()):
    station["enu"] = np.array(
        [eastings[i_station], northings[i_station], station["elevation"]]
    )

np.random.seed(2)

gmm_events = read_GMM()
n_events = len(gmm_events)
mags = np.random.sample(n_events) * 3 - 2
stress_drops = 10 ** (np.random.sample(n_events) * 2 + 5)
moments = mw_to_moment(mags)

radii = calculate_source_radius_from_stress_drop(moments, stress_drops)

f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, velocity_model, phase
)

event_list = {}
for i_event, event_id in enumerate(list(gmm_events.keys())):
    wrapped_event = get_single_event_preferred_origin(athena_client, event_id)
    origin = wrapped_event.get_origin()
    source = SeismicEvent(
        origin_time=origin.get_time(),
        lat=origin.get_latitude(),
        lon=origin.get_longitude(),
        reproject=reproject,
        depth_m=1000 * origin.get_depth(),
    )
    layer = velocity_model._get_layer_by_elevation(source.enu[2])
    corners = {
        Phase.P: convert_radius_corner_brune(radii[i_event], layer.vp),
        Phase.S: convert_radius_corner_brune(radii[i_event], layer.vs),
    }
    random_dc_mt = generate_random_dc_moment_tensor()
    out_stream, rays = simulate_waveforms_for_array(
        *(
            random_dc_mt,
            mags[i_event],
            corners,
            source.enu,
            inventory,
            velocity_model,
            reproject,
        ),
        nt=4000,
        dt=0.001,
        Qp=65,
        Qs=65,
        noise_rms=NOISE_RMS,
        ch_prefix="HH",
        units="V",
        ev_origin_time=UTCDateTime(source.origin_time),
    )

    for tr in out_stream:  # crude surface correction
        if tr.stats.station in ["ESM02", "ESM03", "ESM05", "ESM07"]:
            tr.data = 2 * tr.data

    out_stream.write(f"Braskem//synthetic_waveform_data//{event_id}.MSEED")

    out_pick_file = os.path.join(
        "Braskem",
        "synthetic_waveform_data",
        "picks",
        event_id.zfill(10)
        + UTCDateTime(source.origin_time).strftime("_%Y%m%d_%H%M%S_%f.picks"),
    )
    with open(out_pick_file, "w") as f:
        for (nsl, phase), ray in rays.items():
            f.write(
                f"{nsl},{phase.name},{source.origin_time+ray.traveltime_sec:.3f}000\n"
            )

    out_source = source.__dict__
    keys_to_pop = ["reproject", "source_parameters", "enu"]
    for key in keys_to_pop:
        out_source.pop(key)
    event_list[event_id] = {
        **out_source,
        "mw": mags[i_event],
        "stress_drop": stress_drops[i_event],
    }

with open(
    os.path.join("Braskem", "synthetic_waveform_data", "synthetic_catalog.csv"), "w"
) as f:
    f.write("# event_id, origin_time, lat, lon, depth (m), Mw, stress drop (Pa)\n")
    for i_event, (event_id, event) in enumerate(event_list.items()):
        f.write(
            f"{event_id},{event['origin_time']},{event['lat']},{event['lon']}"
            f",{event['depth_m']},{mags[i_event]},{stress_drops[i_event]}\n"
        )
