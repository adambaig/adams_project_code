from collections import defaultdict
from glob import glob
import json
import os

import matplotlib.pyplot as plt
from NocMeta.Meta import NOC_META
import numpy as np
from obspy import UTCDateTime, read
import pyproj

from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)
from realtime_source_parameters.athena_query_helper import (
    get_single_event_origin,
    get_new_events_since,
    get_single_event_preferred_origin,
)

from nmxseis.interact.athena import AthenaClient
from nmxseis.interact.fdsn import FDSNClient
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.source_parameters import (
    compute_source_parameters,
    _get_valid_dis_spec_for_moment,
)
from nmxseis.numerics.source_parameters.plotting import make_spectra_QC_plot
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D

from read_inputs import read_GMM


TEST_DATA_FBK_STATION_XML = os.path.join("Braskem", "FBK_Full.xml")
TEST_DATA_FBK_CONFIG = os.path.join("Braskem", "fbk_config.json")
config = load_config_from_file(TEST_DATA_FBK_CONFIG)
inventory = get_station_inventory_from_file(TEST_DATA_FBK_STATION_XML)
athena_client = AthenaClient(config.get_athena_base_url(), config.get_athena_api_key())
reproject = pyproj.Proj(f"epsg:{config.get_epsg_code()}")
velocity_model_config = config.get_velocity_model_config()
fourier_spectra_config = config.get_fourier_spectra_config()
processing_config = config.get_processing_config()

layers = []
for layer in velocity_model_config.get_layered_velocity_model():
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            rho=layer["rho"],
            top=layer["top"] if "top" in layer else np.inf,
        )
    )
velocity_model = VelocityModel1D(layers)

waveform_dir = r"Braskem\GMM_dataset\waveforms"
f_raytrace = lambda src_enu, rec_enu, phase: Raypath.isotropic_ray_trace(
    src_enu, rec_enu, velocity_model, phase
)
f_highpass = lambda x: 1


def get_nsl_from_channel_path(channel_path):
    return NSL(
        net=channel_path["networkCode"],
        sta=channel_path["stationCode"],
        loc=channel_path["locationCode"],
    )


min_snr = {"A": 3, "B": 3, "C": 1.5, "D": 0.5}
min_samples_per_bin = {"A": 5, "B": 3, "C": 2, "D": 1}
f_smooth_min = {"A": 1, "B": 5, "C": 10, "D": 10}
sp_results = {}

seed_files = glob(os.path.join(waveform_dir, "*.MSEED"))

for event_json in glob(
    os.path.join("Braskem", "homogeneous", "failed_sp_json", "*.json")
):
    event_id = os.path.basename(event_json).split(".")[0].lstrip("0")
    event = get_single_event_preferred_origin(athena_client, event_id)
    if event is None:
        print(f"{event_id} couldnt be downloaded")
        continue
    arrivals = defaultdict(dict)
    origin = event.get_origin()
    seed_name = os.path.join(waveform_dir, f"{event_id}.MSEED")
    waveform_stream = read(seed_name)
    nsls = [NSL.from_trace(tr) for tr in waveform_stream]
    for arrival in origin.get_arrivals():
        pick = arrival.get_pick()
        nsl = get_nsl_from_channel_path(arrival.get_pick().get_channel_path())
        if nsl in nsls:
            arrivals[nsl][Phase.parse(pick.get_phase())] = UTCDateTime(pick.get_time())

    nsls = [
        NSL.from_trace(tr) for tr in waveform_stream if NSL.from_trace(tr) in arrivals
    ]

    trace_lengths = [tr.stats.npts for tr in waveform_stream]
    if min(trace_lengths) == 1:
        print(f"{seed_name} has 1-length traces; skipping")
        continue
    source = SeismicEvent(
        origin_time=origin.get_time(),
        lat=origin.get_latitude(),
        lon=origin.get_longitude(),
        reproject=reproject,
        depth_m=1000 * origin.get_depth(),
    )

    vm_at_source = velocity_model.look_up_enu(source.enu)
    for quality in ["A", "B", "C", "D"]:
        source_parameters = compute_source_parameters(
            stream=waveform_stream.copy(),
            nsls=nsls,
            inventory=inventory,
            arrivals=arrivals,
            f_raytrace=f_raytrace,
            event=source,
            ev_vs=vm_at_source.vs,
            ev_vp=vm_at_source.vp,
            Q=velocity_model_config.get_Q(),
            kappa=processing_config.get_kappa(),
            f_highpass=lambda x: 0.3,
            lowpass_pct=processing_config.get_lowpass_percent(),
            min_snr=min_snr[quality],
            min_d_hypo_m=processing_config.get_min_hypo_distance_m(),
            max_d_hypo_m=processing_config.get_max_hypo_distance_m(),
            min_samples_per_bin=min_samples_per_bin[quality],
            f_smooth_min=f_smooth_min[quality],
            f_smooth_max=1000,
            f_smooth_n_bins=41,
            n_trial_drops=100,
        )
        if source_parameters is not None:
            sp_results[str(UTCDateTime(source.origin_time))] = {
                "mw": float(source_parameters.mw),
                "uncertainty": float(source_parameters.mw_uncertainty),
                "p corner": float(source_parameters.fc_P)
                if source_parameters.fc_P is not None
                else None,
                "s corner": float(source_parameters.fc_S)
                if source_parameters.fc_S is not None
                else None,
                "quality_score": quality,
                "lat": source.lat,
                "lon": source.lon,
                "depth": source.depth_m,
                "origin time": source.origin_time,
                "enu": source.enu.tolist(),
                "ml": event.get_origin().get_ml(),
                "arrivals": {
                    f"{nsl.__str__()}.{ph.name}": value.timestamp
                    for nsl, ph_value in arrivals.items()
                    for ph, value in ph_value.items()
                },
                "n p valid for moment": 0
                if source_parameters.fc_P is None
                else int(
                    _get_valid_dis_spec_for_moment(
                        source_parameters.mean_spectra[Phase.P],
                        source_parameters.fc_P,
                    ).count()
                ),  # this isn't quite correct as this determination is the engine is from prelim corners
                "n s valid for moment": 0
                if source_parameters.fc_S is None
                else int(
                    _get_valid_dis_spec_for_moment(
                        source_parameters.mean_spectra[Phase.S],
                        source_parameters.fc_S,
                    ).count()
                ),
            }
            break

    if source_parameters is None:
        sp_results[str(UTCDateTime(source.origin_time))] = {
            "quality_score": "F",
            "lat": source.lat,
            "lon": source.lon,
            "depth": source.depth_m,
            "origin time": source.origin_time,
            "enu": source.enu.tolist(),
            "ml": event.get_origin().get_ml(),
            "arrivals": {
                f"{nsl.__str__()}.{ph.name}": value.timestamp
                for nsl, ph_value in arrivals.items()
                for ph, value in ph_value.items()
            },
        }
        print(f"{seed_name}: source paramters failed")
        with open(
            os.path.join(
                "Braskem",
                "homogeneous",
                "failed_sp_json",
                f"{str(event_id).zfill(6)}.json",
            ),
            "w",
        ) as f:
            json.dump(sp_results[str(UTCDateTime(source.origin_time))], f)
        continue
    with open(
        os.path.join(
            "Braskem", "homogeneous", "output_sp_json", f"{str(event_id).zfill(6)}.json"
        ),
        "w",
    ) as f:
        json.dump(sp_results[str(UTCDateTime(source.origin_time))], f)
    os.remove(event_json)
    fig, ax = plt.subplots(1, 2, figsize=[12, 8], sharex="all", sharey="all")
    make_spectra_QC_plot(source_parameters, ax)
    fig.text(0.5, 0.95, UTCDateTime(source.origin_time), ha="center", fontsize=14)
    fig.text(
        0.5,
        0.92,
        f"Quality {quality[-1]}, $M_w = {source_parameters.mw:.2f}\pm {source_parameters.mw_uncertainty:.2f}$",
        ha="center",
        fontsize=12,
    )
    fig.savefig(
        os.path.join(
            "Braskem",
            "homogeneous",
            "figures",
            f'source_param_QC_{os.path.basename(seed_name).split("_")[-1].split(".")[0].zfill(6)}.png',
        )
    )
    plt.close(fig)
