from glob import glob
import json
import os

import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import numpy as np
from obspy import read_inventory, read, UTCDateTime
import pyproj

from generalPlots import gray_background_with_grid
from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)

from realtime_source_parameters.athena_query_helper import (
    get_single_event_origin,
    get_new_events_since,
)
from realtime_source_parameters.waveforms_query_helper import get_waveforms_for_origin
from realtime_source_parameters.engine import get_station_locations

from nmxseis.interact.athena import AthenaClient
from nmxseis.interact.fdsn import FDSNClient

TEST_DATA_FBK_STATION_XML = os.path.join("Braskem", "FBK_Full.xml")
TEST_DATA_FBK_CONFIG = os.path.join("Braskem", "fbk_config.json")
config = load_config_from_file(TEST_DATA_FBK_CONFIG)
inventory = get_station_inventory_from_file(TEST_DATA_FBK_STATION_XML)
athena_client = AthenaClient(config.get_athena_base_url(), config.get_athena_api_key())
events = get_new_events_since(
    athena_client, UTCDateTime("2022-01-16").timestamp, "MANUAL", 14
)

for event in events[:1]:
    waveforms_stream = get_waveforms_for_origin(
        FDSNClient(config.get_apollo_server_ip()), event.get_origin(), 1, 2
    )
    origin = event.get_origin()
    arrivals = origin.get_arrivals()
    origin_time = origin.get_time()
    event_latitude = origin.get_latitude()
    event_longitude = origin.get_longitude()
    event_depth_km = origin.get_depth()
    reproject = pyproj.Proj("epsg:" + str(config.get_epsg_code()))
    stations = get_station_locations(arrivals, inventory, reproject)


arrivals[0].get_pick()


velocity_model = read_velocity_model()

D2R = np.pi / 180.0
tab10 = get_cmap("tab10")

x1, x2 = 197986.58688787775, 198854.75025504341
y1, y2 = 8932712.230823414, 8934324.638987586

dx = 1.1 * (x2 - x1)
dy = 1.1 * (y2 - y1)
dz = 1300

x1p = (x1 + x2) / 2 - 0.5 * dx
x2p = (x1 + x2) / 2 + 0.5 * dx
y1p = (y1 + y2) / 2 - 0.5 * dy
y2p = (y1 + y2) / 2 + 0.5 * dy
z1 = -1200
z2 = 100


pick_files = glob(r"High_quality_events_backup\picks\*.picks")
seed_dir = r"High_quality_events_backup\seeds"
feature_files = glob("waveform_features//*.json")
figure_dir = "waveforms_and_hodogram_figures"
scale = 100


def get_tab10_color(num_string):
    return tab10(float(num_string[-1]) / 10 + 0.1)


for event_file in feature_files[24:25]:
    event_id = os.path.basename(event_file).split("_")[0]
    if not (os.path.isfile(os.path.join(seed_dir, f"{event_id}.seed"))):
        continue
    response = requests.get(
        rf"https://{NOC_META['FBK']['athIP']}/events/{event_id}.json?expand=all&apiKey={NOC_META['FBK']['athApi']}"
    )
    event = json.loads(response.content.decode("utf-8"))
    preferred_origin = [
        origin
        for origin in event["origins"]
        if origin["id"] == event["preferred_origin_id"]
    ][0]
    east, north = pr.transform(
        latlon_proj,
        out_proj,
        preferred_origin["longitude"],
        preferred_origin["latitude"],
    )
    elevation = -1000 * preferred_origin["depth"]
    with open(event_file) as f:
        feature_json = json.load(f)

    fig = plt.figure(figsize=[14, 14])
    ax_depth1 = fig.add_axes([0.05, 0.05, 0.9 * dx / (dx + dy), 0.9 * dz / (dy + dz)])
    ax_depth2 = fig.add_axes(
        [0.05 + 0.9 * dx / (dx + dy), 0.05, 0.9 * dy / (dx + dy), 0.9 * dz / (dy + dz)]
    )
    ax_plan = fig.add_axes(
        [0.05, 0.05 + 0.9 * dz / (dz + dy), 0.9 * dx / (dx + dy), 0.9 * dy / (dz + dy)]
    )

    ax_waveforms = fig.add_axes([0.4, 0.5, 0.55, 0.45])

    ax_plan.set_aspect("equal")
    ax_depth2.set_aspect("equal")
    ax_depth1.set_aspect("equal")

    for station_name, station in stations.items():
        ax_plan.plot(
            station["abs e"],
            station["abs n"],
            "v",
            color=get_tab10_color(station_name[-2]),
            markeredgecolor="0.2",
        )
        ax_depth1.plot(
            station["abs e"],
            station["abs z"],
            "v",
            color=get_tab10_color(station_name[-2]),
            markeredgecolor="0.2",
        )
        ax_depth2.plot(
            station["abs n"],
            station["abs z"],
            "v",
            color=get_tab10_color(station_name[-2]),
            markeredgecolor="0.2",
        )
    ax_plan.plot(east, north, "o", color="orangered", markeredgecolor="k")
    ax_depth1.plot(east, elevation, "o", color="orangered", markeredgecolor="k")
    ax_depth2.plot(north, elevation, "o", color="orangered", markeredgecolor="k")
    ax_plan.set_xlim(x1p, x2p)
    ax_plan.set_ylim(y1p, y2p)
    ax_depth1.set_xlim(x1p, x2p)
    ax_depth1.set_ylim(z1, z2)
    ax_depth2.set_xlim(y1p, y2p)
    ax_depth2.set_ylim(z1, z2)

    for ax in ax_plan, ax_depth1, ax_depth2:
        ax = gray_background_with_grid(ax, grid_spacing=50)
    date_str = str(UTCDateTime(preferred_origin["time"])).split("T")[0]
    ax_waveforms.set_ylim(0, 11)
    ax_waveforms.set_title(f"{event_id} {date_str}")
    ax_waveforms.set_facecolor("0.1")
    picks = [packageArrivals(v) for v in preferred_origin["associations"]]
    pick_times = [p["pick"]["time"] for p in picks]
    theoretical_times = [p["time"] for p in picks]
    max_pick = max([*pick_times, *theoretical_times])
    min_pick = min([*pick_times, *theoretical_times])

    stream = (
        read(os.path.join(seed_dir, f"{event_id}.seed"))
        .detrend()
        .filter("highpass", freq=10)
    )
    stream.rotate("->ZNE", inventory=inv)
    stream.trim(
        starttime=UTCDateTime(min_pick - min_buffer),
        endtime=UTCDateTime(max_pick + max_buffer),
    )
    for station in stations:
        station_name = station.split(".")[1]
        station_int = int(station_name[3:])
        stream_3c = stream.select(station=station_name)
        if len(stream_3c) == 3:
            z_comp = stream_3c.select(channel="??Z")[0].data
            e_comp = stream_3c.select(channel="??E")[0].data
            n_comp = stream_3c.select(channel="??N")[0].data
            norm = 2 * max(
                [
                    max(abs(z_comp)),
                    max(abs(e_comp)),
                    max(abs(n_comp)),
                ]
            )
            time_axis = stream_3c[0].times() + min_pick - min_buffer
            ax_waveforms.plot(
                time_axis, station_int + z_comp / norm, color="0.9", alpha=0.8
            )
            ax_waveforms.plot(
                time_axis, station_int + e_comp / norm, color="deepskyblue", alpha=0.8
            )
            ax_waveforms.plot(
                time_axis, station_int + n_comp / norm, color="orange", alpha=0.8
            )
            station_picks = [
                pick
                for pick in picks
                if pick["pick"]["channelPath"]["stationCode"] == station_name
            ]

            for pick in station_picks:
                phase = pick["pick"]["phase"]
                ax_waveforms.plot(
                    [pick["time"], pick["time"]],
                    [station_int - 0.5, station_int + 0.5],
                    ":",
                    color=pick_color[phase],
                )
                ax_waveforms.plot(
                    [pick["pick"]["time"], pick["pick"]["time"]],
                    [station_int - 0.5, station_int + 0.5],
                    color=pick_color[phase],
                )
    ax_waveforms.set_xlim(min_pick - min_buffer, max_pick + max_buffer)
    ax_waveforms.set_yticks(range(1, 11))
    [
        ax_waveforms.get_yticklabels()[i - 1].set_color(get_tab10_color(str(i)))
        for i in range(1, 11)
    ]
    xticks = ax_waveforms.get_xticks()
    ax_waveforms.set_xticklabels(
        [str(UTCDateTime(t)).split("T")[1][:-6] for t in xticks]
    )
    ax_waveforms.set_xlabel("UTC time")
    fig.savefig(os.path.join(figure_dir, f"{event_id}.png"))
