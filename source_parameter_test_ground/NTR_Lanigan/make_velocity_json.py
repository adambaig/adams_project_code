import json

from nmxseis.numerics.formulas import gardners_relation

with open(r"NTR_Lanigan\NTR_regional.mdl.txt") as f:
    vm_txt = f.readlines()

surface_elevation_m = 500

velocity_model = {}
layers = []
for i_layer, line in enumerate(vm_txt):
    vp_kms, vs_kms, tvd_km = [float(s) for s in line.split()[:3]]
    vp = vp_kms * 1000
    if i_layer == 0:
        layers.append(
            {"vp": vp, "vs": 1000 * vs_kms, "rho": gardners_relation(vp).tolist()}
        )
    else:
        layers.append(
            {
                "vp": vp,
                "vs": 1000 * vs_kms,
                "rho": gardners_relation(vp).tolist(),
                "top": 500 - 1000 * tvd_km,
            }
        )

with open(r"NTR_Lanigan\velocity_model.json", "w") as f:
    json.dump(layers, f)


import os

os.path.isfile(r"C:\Users\adambaig\logikdata\NTR\velocity_model.json")
