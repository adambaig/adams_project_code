import obspy
from obspy.core.inventory import Inventory, Network, Station, Channel, Site, Response
from obspy.clients.nrl import NRL

f = open(r"synthetic/228_stations.csv")
_head = f.readline()
lines = f.readlines()
f.close()


stations = {}
for line in lines:
    split_line = line.split(",")
    stations[split_line[0].split(".")[1]] = {
        "longitude": float(split_line[1]),
        "latitude": float(split_line[2]),
        "elevation": -1000 * float(split_line[3]),
    }

inv = Inventory(networks=[], source="Adam")
net = Network(
    code="CV",
    stations=[],
)

response = Response.from_paz(zeros=[], poles=[], stage_gain=1)


sample_rate = 1000
for station_id, station in stations.items():
    sta = Station(
        code=station_id,
        latitude=station["latitude"],
        longitude=station["longitude"],
        elevation=station["elevation"],
        creation_date=obspy.UTCDateTime(2021, 1, 1),
        site=Site(name=station_id),
    )

    for ch in ["E", "N", "Z"]:
        cha = Channel(
            code=f"CP{ch}",
            location_code="00",
            latitude=station["latitude"],
            longitude=station["longitude"],
            elevation=station["elevation"],
            depth=0,
            dip=dip[ch],
            azimuth=azimuth[ch],
            sample_rate=sample_rate,
            response=response,
        )
        sta.channels.append(cha)
    net.stations.append(sta)
sta = Station(
    code="Fake",
    latitude=54,
    longitude=-117,
    elevation=1000,
    creation_date=obspy.UTCDateTime(2021, 1, 1),
    site=Site(name="fake"),
)
for ch in ["E", "N", "Z"]:
    cha = Channel(
        code=f"CP{ch}",
        location_code="00",
        latitude=54,
        longitude=-117,
        elevation=1000,
        depth=1000,
        dip=dip[ch],
        azimuth=azimuth[ch],
        sample_rate=sample_rate,
        response=response,
    )
    sta.channels.append(cha)
net.stations.append(sta)
inv.networks.append(net)

inv.write("synthetic//synthetic_sta_plus_one.xml", format="stationxml", validate=True)
