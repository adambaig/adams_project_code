import json
import logging
import os

import matplotlib.pyplot as plt
from NocMeta.Meta import NOC_META
import numpy as np

import pyproj

from realtime_source_parameters.event_origin_processor import EventOriginProcessor
from realtime_source_parameters.util import load_config_from_file
from realtime_source_parameters.offline_inputs_helper import (
    get_event_from_json_file,
    get_waveforms_from_miniseed,
    get_station_inventory_from_file,
)
from realtime_source_parameters.athena_query_helper import get_single_event_origin

from realtime_source_parameters.engine import (
    get_velocity_lookup_and_raytracer,
    rotate_stream_to_raypaths,
    get_station_locations,
    get_windows,
    get_hypocentral_distance,
    get_highpass,
    make_dual_spectra_plot,
    moment_to_mw,
)

from realtime_source_parameters.spectral_measurements import (
    get_signal_and_noise_spectra,
    calculate_mean_spectrum,
    get_useable_bandwidth_for_moment_and_stress_drop,
    find_stress_drop,
    determine_preliminary_corner_and_energy_flux,
    compensate_for_attenuation_ray,
    kappa_correction,
)
from realtime_source_parameters.util import (
    is_arrival_matching_at_the_same_location,
    get_simple_phase,
)


from realtime_source_parameters.estk_helpers import (
    fourier_spectra,
    # motion_filt,
    determine_gaps_and_bandwidth,
    gapcheck,
)

from nmxseis.interact.athena import AthenaClient


events_origins_seeds = {
    "52346": {
        "origin": "3964792",
        "seed": "20210625.004418.292659_20210625.004548.292659.seed",
        "spec_fit_mw": 1.34,
    },
    "52348": {
        "origin": "3964822",
        "seed": "20210627.135911.400860_20210627.140041.400860.seed",
        "spec_fit_mw": 1.87,
    },
    "52351": {
        "origin": "3964839",
        "seed": "20210627.225506.345870_20210627.225636.345870.seed",
        "spec_fit_mw": 1.36,
    },
    "52367": {
        "origin": "3965496",
        "seed": "20210712.004111.490092_20210712.004241.490092.seed",
        "spec_fit_mw": 3.04,
    },
    "52372": {
        "origin": "3965392",
        "seed": "20210804.234811.975766_20210804.234941.975766.seed",
        "spec_fit_mw": 1.73,
    },
}


CLIENT = AthenaClient(
    rf"http://{NOC_META['DSA_TEST']['athIP']}", NOC_META["DSA_TEST"]["athApi"], 20
)
TEST_DATA_DSA_STATION_XML = os.path.join("DSA", "station_xml", "DSA_TEST_Full.xml")
TEST_DATA_DSA_CONFIG = os.path.join("DSA", "dsa_test_config.json")
inventory = get_station_inventory_from_file(TEST_DATA_DSA_STATION_XML)
config = load_config_from_file(TEST_DATA_DSA_CONFIG)
processor = EventOriginProcessor(None, None, config)


velocity_model_config = config.get_velocity_model_config()
processing_config = config.get_processing_config()
fourier_spectra_config = config.get_fourier_spectra_config()
processor = EventOriginProcessor(None, None, config)
kappa = processing_config.get_kappa()

Q = velocity_model_config.get_Q()
velocity_lookup, raytracer = get_velocity_lookup_and_raytracer(velocity_model_config)


source_parameters = {}
for event_id, event_files in list(events_origins_seeds.items()):
    test_data_dsm_miniseed = os.path.join("DSA", "seeds", event_files["seed"])
    event = get_single_event_origin(CLIENT, event_id, event_files["origin"])
    waveforms_stream = get_waveforms_from_miniseed(test_data_dsm_miniseed)
    # source_parameters[event_id] = processor.calculate_source_parameters(
    #     event, inventory, waveforms_stream
    # )

    origin = event.get_origin()
    arrivals = origin.get_arrivals()
    origin_time = origin.get_time()
    event_latitude = origin.get_latitude()
    event_longitude = origin.get_longitude()
    event_depth_km = origin.get_depth()
    reproject = pyproj.Proj("epsg:" + str(config.get_epsg_code()))
    stations = get_station_locations(arrivals, inventory, reproject)
    easting, northing = reproject(event_longitude, event_latitude, inverse=False)
    event_depth = event_depth_km * 1000.0
    event_location = {"e": easting, "n": northing, "z": -event_depth}
    lowpass_percent = processing_config.get_lowpass_percent()
    raypaths = {}
    for station_id, station in stations.items():
        station["rock properties"] = velocity_lookup(station)
        for phase in ["P", "S"]:
            raypaths[f"{station_id}.{phase}"] = raytracer(
                event_location, station, phase
            )
    event_location["rock properties"] = velocity_lookup(event_location)
    no_waveform_net_stat_locs = []
    for net_stat_loc, station in stations.items():
        network_code, station_code, location_code = net_stat_loc.split(".")
        station_traces = waveforms_stream.select(
            network=network_code, station=station_code, location=location_code
        )
        if len(station_traces) == 0:
            logging.error("Cannot find waveforms for %s", net_stat_loc)
            no_waveform_net_stat_locs.append(net_stat_loc)
            continue
        station["hypo_distance"] = get_hypocentral_distance(station, event_location)
        station["highpass"] = get_highpass(
            processing_config, origin, get_hypocentral_distance(station, event_location)
        )
        station["lowpass"] = (
            (1.0 - processing_config.get_lowpass_percent() / 100.0)
            * station_traces[0].stats.sampling_rate
            / 2
        )
    for station in no_waveform_net_stat_locs:
        stations.pop(station)

    rotated_traces = rotate_stream_to_raypaths(
        waveforms_stream, stations, event_location, raytracer, inventory=inventory
    )

    noise_windows, signal_windows = {}, {}
    for arrival in arrivals:
        pick = arrival.get_pick()
        channel_path = pick.get_channel_path()
        phase = pick.get_phase()
        net_stat_loc_phase = ".".join(
            [
                *[
                    channel_path[code]
                    for code in ["networkCode", "stationCode", "locationCode"]
                ],
                phase,
            ]
        )
        net_stat_loc = net_stat_loc_phase[:-2]
        (
            signal_windows[net_stat_loc_phase],
            noise_windows[net_stat_loc_phase],
        ) = get_windows(
            arrival, arrivals, raytracer, event_location, stations, processing_config
        )
        if signal_windows[net_stat_loc_phase]:
            stations[net_stat_loc][f"{phase} window"] = signal_windows[
                net_stat_loc_phase
            ]

    (
        acceleration_spectra,
        acceleration_noise_spectra,
        freqs,
    ) = get_signal_and_noise_spectra(
        rotated_traces,
        signal_windows,
        noise_windows,
        fourier_spectra_config.get_parameter_list(),
    )

    signal_to_noise_ratios = {}
    for net_stat_loc_phase in acceleration_spectra:
        signal_to_noise_ratios[net_stat_loc_phase] = [
            s / n if s > 0 or n > 0 else -999.0
            for s, n in zip(
                acceleration_spectra[net_stat_loc_phase],
                acceleration_noise_spectra[net_stat_loc_phase],
            )
        ]

    signal_to_noise_ratios_by_phase = {
        "P": {k: v for k, v in signal_to_noise_ratios.items() if k[-1] == "P"},
        "S": {k: v for k, v in signal_to_noise_ratios.items() if k[-1] != "P"},
    }

    (
        acceleration_spectra_corrected,
        displacement_spectra_corrected,
    ) = compensate_for_attenuation_ray(
        acceleration_spectra, freqs, raypaths, Q, origin, stations, event_location
    )

    if kappa > 0:
        for spectra in [displacement_spectra_corrected, acceleration_spectra_corrected]:
            for phase_spectra in spectra.values():
                for spectrum in phase_spectra.values():
                    spectrum = kappa_correction(kappa, spectrum, freqs)

    acc_spectrum = {"P curve": 0, "S curve": 0, "P sigma": 0, "S sigma": 0}

    disp_spectrum = {"P curve": 0, "S curve": 0, "P sigma": 0, "S sigma": 0}

    for phase in ["P", "S"]:
        (
            acc_spectrum[f"{phase} curve"],
            acc_spectrum[f"{phase} sigma"],
        ) = calculate_mean_spectrum(
            acceleration_spectra_corrected[phase],
            signal_to_noise_ratios_by_phase[phase],
            freqs,
            stations,
            processing_config,
        )
        (
            disp_spectrum[f"{phase} curve"],
            disp_spectrum[f"{phase} sigma"],
        ) = calculate_mean_spectrum(
            displacement_spectra_corrected[phase],
            signal_to_noise_ratios_by_phase[phase],
            freqs,
            stations,
            processing_config,
        )

    freqs_for_moment = {}
    freqs_for_stress_drop = {}
    flat_displacement_samples = {}
    acceleration_spectrum_around_corner = {}

    for curve in ["P curve", "S curve"]:
        phase = curve.split()[0]
        (
            flat_displacement_samples[phase],
            freqs_for_moment[phase],
            acceleration_spectrum_around_corner[phase],
            freqs_for_stress_drop[phase],
        ) = get_useable_bandwidth_for_moment_and_stress_drop(
            acc_spectrum[curve],
            disp_spectrum[curve],
            freqs,
            stations,
            processing_config,
        )

    if (
        flat_displacement_samples["P"] is None
        and flat_displacement_samples["S"] is None
    ):
        continue
    if flat_displacement_samples["P"] is None:
        moment = 10.0 ** np.nanmean(np.log10(flat_displacement_samples["S"]))
    elif flat_displacement_samples["S"] is None:
        moment = 10.0 ** np.nanmean(np.log10(flat_displacement_samples["P"]))
    else:
        moment = 10.0 ** np.nanmean(
            np.log10(
                np.hstack(
                    [flat_displacement_samples["P"], flat_displacement_samples["S"]]
                )
            )
        )

    # moment_magnitude = moment_to_mw(moment)

    fig = make_dual_spectra_plot(
        disp_spectrum, displacement_spectra_corrected, freqs, moment
    )
