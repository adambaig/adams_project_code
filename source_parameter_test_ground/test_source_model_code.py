import numpy as np

from nmxseis.numerics.formulas import brune_source_model, abercrombie_source_model
from nmxseis.model.phase import Phase
from nmxseis.model.source_radius import Madariaga, Brune
from nmxseis.numerics.velocity_model import VelocityModelElement

source_velocity = VelocityModelElement(vp=2000, vs=1400, rho=2400)

radius_model = Madariaga
radius_model.calc_corner_frequency_ratio(source_velocity)
radius_model

model(23, Phase.S)
radius_model.corner_frequency_ratio(source_velocity)

print(radius_model.__name__)

radius_model = Brune


brune_source_model == abercrombie_source_model

model.f

source = brune_source_model(100)

model = np.zeros(999)
for ii in range(1, 1000):
    model[ii - 1] = source

from collections import defaultdict


test = defaultdict(lambda: False)
test
test["a"] = True
test["b"]
