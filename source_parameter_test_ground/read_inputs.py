import json
import os
from pathlib import Path

import numpy as np

from nmxseis.numerics.formulas import gardners_relation
from nmxseis.numerics.velocity_model.vm_1d import VelocityModel1D, VMLayer1D


def read_GMM():
    with open(r"Braskem\GMM_dataset\Output_Compare_PGV_Mw (1).csv") as f:
        _head = f.readline()
        lines = f.readlines()

    event_gm_mw = {}
    for line in lines:
        split_line = line.split(",")
        event_id = split_line[0]
        event_gm_mw[event_id] = {
            "mw (NA15)": float(split_line[5]),
            "athana mw": float(split_line[-3]),
            "mw pe magout": float(split_line[-2]),
            "offline gm mw": float(split_line[-1]),
        }
    return event_gm_mw


def read_do_file():
    events = {}
    with open(os.path.join("Braskem", "GMM_dataset", "Do_Final_QC_goodfits.csv")) as f:
        _head = f.readline()
        lines = f.readlines()
    for line in lines:
        split_line = line.split(",")
        event_id = split_line[0]
        events[event_id] = {"moment": float(split_line[-1]), "mw": float(split_line[6])}
    return events


def read_hinv_model(filename, vp_vs_ratio=np.sqrt(3)):
    with open(filename) as f:
        _head = f.readline()
        lines = f.readlines()
    layers = []
    for i_layer, layer_line in enumerate(lines):
        vp_kms, depth_km = [float(s) for s in layer_line.split()]
        if i_layer == 0:
            layers.append(
                VMLayer1D(
                    (vp := vp_kms * 1000),
                    vs=vp / vp_vs_ratio,
                    rho=gardners_relation(vp),
                )
            )
        else:
            layers.append(
                VMLayer1D(
                    (vp := vp_kms * 1000),
                    vs=vp / vp_vs_ratio,
                    rho=gardners_relation(vp),
                    top=depth_km * 1000,
                )
            )
    return VelocityModel1D(layers)

def read_braskem_vm_1d():
    vm_csv = Path("Braskem", "NewVelocityProfile_average.csv").read_text().split('\n')
    layers = []
    for line in vm_csv[1:]:
        if line == '':
            continue
        depth_km, vp_kms, vs_kms =  [float(s) for s in line.split(',')]
        layers.append(VMLayer1D(vp = vp_kms*1000, vs=vs_kms*1000, rho = float(gardners_relation(vp_kms*1000)), top = -depth_km*1000))
    return VelocityModel1D(layers)

vm = read_braskem_vm_1d()
Path('braskem_1dvm.json').write_text(json.dumps(vm.to_json()))