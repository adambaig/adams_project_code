import json
from pfi_fs.configuration import geology

Q, velocity_model = geology(r"DSA\velocity_model\geology_used.cfg")

velocity_model
for layer in velocity_model:
    layer["vs"] = layer["vp"] / 1.75
    layer["rho"] = 2600


with open(r"DSA\velocity_model\velocity_model_update", "w") as f:
    json.dump(velocity_model, f)
