import matplotlib.pyplot as plt
import numpy as np
from obspy import UTCDateTime

from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase


def plot_rotated_waveforms_and_windows(
    rotated_traces, signal_windows, noise_windows, arrivals, ax=None
):
    from matplotlib.patches import Rectangle

    nsls = np.unique([NSL.from_trace(tr) for tr in rotated_traces])
    if ax is None:
        fig, ax = plt.subplots(figsize=[12, 12])
    min_buffer, max_buffer = 0.5, 1.0
    pick_color = {Phase.P: "cyan", Phase.S: "green"}
    ax.set_ylim(-1, len(nsls))
    # ax.set_title(f"{event_id} {date_str}")
    ax.set_facecolor("0.1")
    pick_times = [list(arrivals[nsl].values())[0] for nsl in nsls]
    min_time = min([v[0] for k, v in noise_windows.items()]).timestamp
    max_time = max([v[0] for k, v in signal_windows.items()]).timestamp
    taper_fraction = 0.2 / (max_time - min_time + max_buffer + min_buffer)
    rotated_traces.trim(
        starttime=UTCDateTime(min_time - min_buffer),
        endtime=UTCDateTime(max_time + max_buffer),
    ).filter("highpass", freq=5, corners=2, zerophase=True)
    for i_nsl, nsl in enumerate(nsls):
        station_name = nsl.__str__()
        stream_3c = rotated_traces.select(station=nsl.sta)
        p_comp = stream_3c.select(channel="??P")[0].data
        v_comp = stream_3c.select(channel="??V")[0].data
        h_comp = stream_3c.select(channel="??H")[0].data
        time_axis = [
            UTCDateTime(t + min_time - min_buffer) for t in stream_3c[0].times()
        ]
        norm = 4 * max(
            [
                max(abs(p_comp)),
                max(abs(v_comp)),
                max(abs(h_comp)),
            ]
        )
        ax.plot(time_axis, i_nsl + 0.25 + p_comp / norm, color="springgreen", alpha=0.8)
        ax.plot(
            time_axis, i_nsl - 0.25 + v_comp / norm, color="mediumorchid", alpha=0.8
        )
        ax.plot(time_axis, i_nsl - 0.25 + h_comp / norm, color="w", alpha=0.8)
        station_picks = {ph: time for ph, time in arrivals[nsl].items()}
        for ph, p_time in station_picks.items():
            ax.plot(
                [p_time, p_time],
                [i_nsl - 0.5, i_nsl + 0.5],
                color=pick_color[ph],
            )
        for step, ph in zip([0, -0.5], [Phase.P, Phase.S]):
            if (nsl, ph) in signal_windows:
                sig_win = signal_windows[nsl, ph]
                p_window = Rectangle(
                    (sig_win[0].timestamp, i_nsl + step),
                    np.diff(sig_win),
                    0.5,
                    facecolor="lightblue",
                    alpha=0.1,
                    ec="lightblue",
                )
                ax.add_patch(p_window)
                nse_win = noise_windows[nsl, ph]
                noise_window = Rectangle(
                    (nse_win[0].timestamp, i_nsl + step),
                    np.diff(nse_win),
                    0.5,
                    facecolor="0.8",
                    alpha=0.1,
                    ec="0.8",
                )
                ax.add_patch(noise_window)
    ax.set_xlim(min_time - min_buffer, max_time + max_buffer)
    ax.set_yticks(range(len(nsls)))
    ax.set_yticklabels([nsl.__str__() for nsl in nsls])

    xticks = ax.get_xticks()
    ax.set_xticklabels([str(UTCDateTime(t)).split("T")[1][:-6] for t in xticks])
    ax.set_xlabel("UTC time")


def plot_signaL_and_noise_spectra(signal, noise):
    fig, ax = plt.subplots()
    ax.loglog(signal.freqs, signal.dis)
    ax.loglog(noise.freqs, noise.dis)
