import json
import os

import numpy as np
from obspy import read_inventory

velocity_model_file = os.path.join("WTX_STAGING", "velocity_model.json")
inventory_file = os.path.join("WTX_STAGING", "WTX_Full.xml")
velocity_model_outfile = os.path.join(
    "WTX_STAGING", "velocity_model_elev_corrected.json"
)
inv = read_inventory(inventory_file)

with open(velocity_model_file) as f:
    velocity_model_json = json.load(f)

avg_elevation = np.average([sta.elevation for net in inv for sta in net])

velocity_model_out = []
for layer in velocity_model_json:
    layer["top"] += avg_elevation
    velocity_model_out.append(layer)

with open(velocity_model_outfile, "w") as f:
    json.dump(velocity_model_out, f)
