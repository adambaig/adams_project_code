import glob
import json
import os

from matplotlib import cm
from matplotlib.colors import Normalize
import matplotlib.pyplot as plt
import numpy as np

from nmxseis.numerics.formulas import mw_to_moment
from generalPlots import gray_background_with_grid

from read_inputs import read_GMM, read_do_file


gmm_dataset = read_GMM()
spec_fit_data = read_do_file()

output_jsons = glob.glob(os.path.join("Braskem", "output_sp_json", "*.json"))
failed_jsons = glob.glob(os.path.join("Braskem", "failed_sp_json", "*.json"))

quality_color = {
    "A": "forestgreen",
    "B": "lightsteelblue",
    "C": "darkgoldenrod",
    "D": "0.2",
}

output_dataset = {}
for outfile in output_jsons:
    event_id = os.path.basename(outfile).split(".")[0].lstrip("0")
    with open(outfile) as f:
        output_dataset[event_id] = json.load(f)


patches = {}
fig, ax = plt.subplots(figsize=[6, 6])
ax.set_aspect("equal")
for event_id, params in spec_fit_data.items():
    if event_id in output_dataset:
        (patch,) = ax.loglog(
            params["moment"],
            mw_to_moment(output_dataset[event_id]["mw"]),
            "o",
            color=quality_color[(qual := output_dataset[event_id]["quality_score"])],
            markeredgecolor="0.2",
        )
        if qual not in patches:
            patches[qual] = patch


xlim = ax.get_xlim()
ylim = ax.get_ylim()

max_ax = max(*xlim, *ylim)
min_ax = min(*xlim, *ylim)
ax_lim = [min_ax, max_ax]

ax.plot(ax_lim, ax_lim, "k--", zorder=-1)
ax.set_xlabel("SpecFit moment (N$\cdot$m)")
ax.set_ylabel("rtsp moment (N$\cdot$m)")
ax.set_xlim(*ax_lim)
ax.set_ylim(*ax_lim)
ax.legend(
    [patches["A"], patches["B"], patches["C"], patches["D"]], ["A", "B", "C", "D"]
)
fig.savefig(os.path.join("Braskem", "moment_comparison.png"))


fig, ax = plt.subplots(figsize=[6, 6])
for event_id, mags in gmm_dataset.items():
    if event_id in output_dataset:
        (patch,) = ax.plot(
            mags[COMPARISON_KEY] - output_dataset[event_id]["mw"],
            output_dataset[event_id]["depth"],
            "o",
            color=quality_color[(qual := output_dataset[event_id]["quality_score"])],
            markeredgecolor="0.2",
        )

ax.set_ylim(*(ax.get_ylim()[::-1]))
ax.legend(
    [patches["A"], patches["B"], patches["C"], patches["D"]], ["A", "B", "C", "D"]
)
ax.set_xlabel(f"{COMPARISON_LABEL} $M_W$ $-$ rtsp $M_W$")
ax.set_ylabel("depth (m)")
fig.savefig("Braskem//m_discrep_with_depth.png")

fig_d, ax_d = plt.subplots(figsize=[7, 6])
ax_d.set_aspect("equal")
for event_id, mags in gmm_dataset.items():
    if event_id in output_dataset:
        ax_d.plot(
            mags[COMPARISON_KEY],
            output_dataset[event_id]["mw"],
            "o",
            color=depth_cmap(output_dataset[event_id]["depth"] / 1000),
            markeredgecolor="0.2",
        )

ax_d.plot(ax_lim, ax_lim, "k--", zorder=-1)
ax_d.set_xlabel(COMPARISON_LABEL)
ax_d.set_ylabel("rtsp $M_W$")
ax_d.set_xlim(*ax_lim)
ax_d.set_ylim(*ax_lim)

cbar = fig_d.colorbar(
    cm.ScalarMappable(norm=Normalize(vmin=0, vmax=1000), cmap="inferno_r")
)
cbar.set_ticks(cbar.get_ticks())
cbar.set_ticklabels(cbar.get_ticks()[::-1])
cbar.set_label("depth (m)")

fig_d.savefig(os.path.join("Braskem", "gmm_comparison_with_depth.png"))


a_data = {k: v for k, v in output_dataset.items() if v["quality_score"] == "A"}
b_data = {k: v for k, v in output_dataset.items() if v["quality_score"] == "B"}
c_data = {k: v for k, v in output_dataset.items() if v["quality_score"] == "C"}
len(a_data)

m_bins = np.arange(-2.5, 1, 0.1)
fig, ax = plt.subplots()
ax.hist(
    (c_mags := [v["mw"] for v in c_data.values()]),
    m_bins,
    facecolor="darkgoldenrod",
    alpha=0.7,
)
ax.hist(
    (b_mags := [v["mw"] for v in b_data.values()]),
    m_bins,
    facecolor="lightsteelblue",
    alpha=0.7,
)
ax.hist(
    (a_mags := [v["mw"] for v in a_data.values()]),
    m_bins,
    facecolor="forestgreen",
    alpha=0.7,
)

ax.set_xlabel("rtsp $M_W$")
ax.set_ylabel("count")
fig.savefig(os.path.join("Braskem", "m_histograms.png"))

dummy_map, ax_dummy = plt.subplots()
ax_dummy.set_aspect("equal")
enus = np.array([v["enu"] for v in output_dataset.values()])
ax_dummy.plot(enus[:, 0], enus[:, 1], ".")
enus = np.array([v["enu"] for v in total_failed_dataset.values()])
ax_dummy.plot(enus[:, 0], enus[:, 1], "k.")
xlim = ax_dummy.get_xlim()
ylim = ax_dummy.get_ylim()

fig_map, ax_map = plt.subplots(2, 3, figsize=[15, 8])
for qual, ax in zip(["A", "B", "C", "D", "F"], np.ndarray.flatten(ax_map)[:5]):
    ax.set_aspect("equal")
    if qual == "F":
        enus = np.array([v["enu"] for v in total_failed_dataset.values()])
    else:
        enus = np.array(
            [v["enu"] for v in output_dataset.values() if v["quality_score"] == qual]
        )
    ax.scatter(
        enus[:, 0],
        enus[:, 1],
        c=-enus[:, 2],
        vmin=0,
        vmax=1000,
        cmap="inferno",
        edgecolor="0.2",
    )
    ax.set_xlim(*xlim)
    ax.set_ylim(*ylim)
    gray_background_with_grid(ax)
    ax.set_title(f"Quality {qual}; n={len(enus)}")
cbar = fig_map.colorbar(
    cm.ScalarMappable(norm=Normalize(vmin=0, vmax=1000), cmap="inferno_r"), ax=ax_map
)
cbar.set_ticks(cbar.get_ticks())
cbar.set_ticklabels(cbar.get_ticks()[::-1])
cbar.set_label("depth (m)")
ax_map[1, 2].axis("off")
fig_map.savefig(os.path.join("Braskem", "quality_map.png"), bbox_inches="tight")


len(np.where(np.array(a_mags) > 0)[0])

fig_b, ax_b = plt.subplots()
for mag_dist in [a_mags, b_mags, c_mags]:
    n = []
    for m in m_bins:
        n.append(len(np.where(np.array(mag_dist) > m)[0]))
    ax_b.semilogy(m_bins, n)
