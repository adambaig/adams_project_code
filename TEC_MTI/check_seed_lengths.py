import glob
import os
from obspy import read


seeds = glob.glob(r"C:\Users\adambaig\logikdata\TEC\MSEED\Nov_2020\*.seed")

out_dir = r"C:\Users\adambaig\logikdata\TEC\MSEED\Nov_2020_merged"

seed = seeds[0]

for seed in seeds:
    st = read(seed)
    st.merge()
    st.write(os.path.join(out_dir, os.path.basename(seed)).replace(".seed", ".mseed"))
