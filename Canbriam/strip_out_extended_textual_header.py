def strip_out_header(filename, new_file, matching_text="Projection parameter 7 value"):
    """
    Strip out the extended textual header of a segy file by finding all lines with
    that matching text and write all but those lines to a new file

    Args:
        filename: path of the input file
        new_file: path of the new file generated without the header
        matching_text: text string to search for to identify the line with the
                        extended textual header
    """
    output = []
    with open(filename, "rb") as reader:
        textual_header_end = False
        for i, line in enumerate(reader):
            if textual_header_end:
                output.append(line)
                continue
            if matching_text in str(line):
                textual_header_end = True
                continue
        with open(new_file, "wb") as writer:
            for line in output:
                writer.write(line)
