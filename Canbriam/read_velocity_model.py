filename = "Calib_Vel_Model.sgy"
output = []
with open(filename, "rb") as reader:
    textualHeaderEnd = False
    for i, line in enumerate(reader):
        if textualHeaderEnd:
            output.append(line)
            continue
        if "Projection parameter 7 value" in str(line):
            textualHeaderEnd = True
            continue
    new_file = "_" + filename
    with open(new_file, "wb") as writer:
        for line in output:
            writer.write(line)
