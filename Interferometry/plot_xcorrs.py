from datetime import datetime, timedelta
import glob
import os

import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter
import numpy as np
import obspy

date_format = DateFormatter("%b %y")
dir = os.path.join("dow", "STACKS", "01", "008_HOURS", "ZZ")
pairs = [
    os.path.split(pair_path)[-1] for pair_path in glob.glob(os.path.join(dir, "*"))
]


def seed_path_to_datetime(path):
    return datetime.strptime(os.path.basename(path), "%Y-%m-%d %H-%M-%S.MSEED")


def n_hours_since_starttime(datetime_object, s_time):
    return int((datetime_object - starttime).total_seconds() / 3600) + 1


for pair in pairs:
    xcorr_seeds = glob.glob(os.path.join(dir, pair, "*.MSEED"))
    starttime = seed_path_to_datetime(xcorr_seeds[0])
    endtime = seed_path_to_datetime(xcorr_seeds[-1])
    n_hours = n_hours_since_starttime(endtime, starttime) + 1
    time_axis = [starttime + timedelta(seconds=i * 3600) for i in range(n_hours)]
    st = obspy.read(xcorr_seeds[0]).decimate(10)
    time_shift_axis = (t := st[0].times()) - t[-1] / 2
    image = np.zeros([n_hours, 2401])
    for seed in xcorr_seeds:
        i_hour = n_hours_since_starttime(seed_path_to_datetime(seed), starttime)
        image[i_hour, :] = obspy.read(seed).decimate(10)[0].data

    fig, ax = plt.subplots(figsize=[12, 8])
    ax.pcolor(time_shift_axis, time_axis, image, cmap="seismic", vmin=-1e-9, vmax=1e-9)
    ax.set_xlabel("time shift (s)")
    fig.savefig(os.path.join("dow", f"{pair}.png"))
