from datetime import datetime

import matplotlib.pyplot as plt
from msnoise.api import get_params, connect
import numpy as np
from obspy import read
from scipy.signal import detrend
from obspy.signal.invsim import cosine_taper


from read_inputs import read_mwcs

basedir = r"Parkfield/"


def normalize(signal):
    return signal / np.linalg.norm(signal)


def plot_snippet(station_pair, date, lag, length, filter="01", days="001_DAYS"):
    date_str = datetime.strftime(date, "%Y-%m-%d")
    stack_dir = rf"{basedir}STACKS/{filter}/{days}/ZZ/"
    ref_dir = rf"{basedir}STACKS/{filter}/REF/ZZ/"
    mwcs = read_mwcs(rf"{basedir}MWCS/{filter}/{days}/ZZ/{station_pair}/{date_str}.txt")
    current_seed = read(rf"{stack_dir}{station_pair}/{date_str}.MSEED")
    ref_seed = read(rf"{ref_dir}{station_pair}.MSEED")

    ref_data = ref_seed[0].data
    current_data = current_seed[0].data
    delta = ref_seed[0].stats.delta
    window_length_samples = int(length / delta)
    ref_time = ref_seed[0].times()
    ref_time -= ref_time[-1] / 2
    i_start = np.argmin(abs(ref_time - lag + length / 2))
    i_end = np.argmin(abs(ref_time - lag - length / 2))
    fig, ax = plt.subplots(figsize=[8, 3])
    taper = cosine_taper(window_length_samples, 0.85)
    ref_sample = taper * ref_data[i_start:i_end]
    current_sample = taper * current_data[i_start:i_end]
    ax.plot(ref_time[i_start:i_end], normalize(ref_sample), "0.1", lw=2)
    ax.plot(ref_time[i_start:i_end], normalize(current_sample), "0.2", lw=1)
    ax.text(
        0.01,
        0.9,
        f'coherence: {mwcs[float(lag)]["coherence"]:.2f}',
        va="top",
        transform=ax.transAxes,
    )
    ax.text(
        0.01,
        0.8,
        f'delay: ${mwcs[float(lag)]["delay"]:.2f}'
        f' \pm ${mwcs[float(lag)]["error"]:.2f} s',
        va="top",
        transform=ax.transAxes,
    )
    y1, y2 = ax.get_ylim()
    ax.set_xlim(ref_time[i_start], ref_time[i_end])
    ax.set_title(date_str)
    ax.set_xlabel("time (s)")
    return fig
