import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read
import os

ZZ_path = os.sep.join(["STACK_0.1_5Hz", "01", "001_DAYS", "ZZ"])
pairs = glob.glob(f"{ZZ_path}{os.sep}*")

for pair in pairs:
    title = pair.split(os.sep)[-1]
    seeds = glob.glob(f"{pair}{os.sep}*")
    n_seeds = len(seeds)
    len(seeds)
    n_samples = 4801
    time_axis = np.arange(-2400, 2401) * 0.05
    cc_section = np.zeros([n_seeds, n_samples])
    for i_seed, seed in enumerate(seeds):
        st = read(seed)
        cc_section[i_seed, :] = st[0].data
    cc_stack = np.average(cc_section, axis=0)
    fig, ax = plt.subplots(figsize=[12, 4])
    for i_seed, seed in enumerate(seeds):
        ax.plot(time_axis, cc_section[i_seed, :], "0.5", lw=0.5)
    ax.plot(time_axis, cc_stack, "k")
    ax.set_xlim([-30, 30])
    ax.set_xlabel("time (s)")
    ax.set_yticklabels([])
    ax.set_title(title)
    i_start, i_end = 1800, 3001
    fig.savefig(f"figures{os.sep}traces_{title}.png")
    f2, a2 = plt.subplots(figsize=[12, 9])
    a2.pcolor(
        time_axis[i_start:i_end],
        range(31),
        cc_section[:, i_start:i_end],
        vmin=-1e-7,
        vmax=1e-7,
    )
    a2.set_xlabel("time (s)")
    f2.savefig(f"figures{os.sep}section_{title}.png")
    plt.close("all")
