import glob
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
from obspy import read
import os
from scipy.fftpack import fft, fftfreq

ZZ_path = os.sep.join(["STACKS", "01", "001_DAYS", "ZZ"])
ref_path = os.sep.join(["STACKS", "01", "REF", "ZZ"])
pairs = glob.glob(f"{ZZ_path}{os.sep}*")

pairs
pair = pairs[0].split(os.sep)[-1]

t_start, t_end = -2.0, 2.0
ref_corr = read(f"{ref_path}{os.sep}{pair}.MSEED")[0].data
test_corr = read(f"{ZZ_path}{os.sep}{pair}{os.sep}2020-10-10.MSEED")[0].data
time_axis = np.arange(-2400, 2401) * 0.05
fig_trace, ax_trace = plt.subplots(figsize=[12, 3])
ax_trace.plot(time_axis, ref_corr)
ax_trace.plot(time_axis, test_corr)
y1, y2 = ax_trace.get_ylim()
window = Rectangle(
    [t_start, y1],
    t_end - t_start,
    y2 - y1,
    facecolor="lightblue",
    alpha=0.3,
    edgecolor="k",
)
ax_trace.add_patch(window)
ax_trace.set_xlim([-30, 30])
ax_trace.set_xlabel("time (s)")
ax_trace.legend(["Reference", "Current"])
fig_trace.savefig("traces_11.png")
# ax.set_ylim([-1e-7,1e-7])
i_win = np.where((time_axis > t_start) & (time_axis < t_end))[0]
win_1 = np.zeros(len(time_axis))
win_ref = np.zeros(len(time_axis))

win_1[i_win] = test_corr[i_win]
win_ref[i_win] = ref_corr[i_win]
cross_spec = fft(win_ref) * np.conj(fft(win_1))
freq = fftfreq(len(time_axis), 0.05)
i_pos = np.where(freq > 0)
fig, ax = plt.subplots(figsize=[12, 3])
ax.plot(freq[i_pos], np.unwrap(np.angle((cross_spec)))[i_pos])
ax.set_xlabel("frequency")
ax.set_ylabel("unwrapped phase")
fig.savefig("x_spec_phase11.png")
