import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()

plt.style.use("ggplot")

from msnoise.api import (
    connect,
    get_results,
    build_movstack_datelist,
    get_params,
    get_t_axis,
    get_stations,
)

db = connect()
start, end, datelist = build_movstack_datelist(db)
params = get_params(db)
taxis = get_t_axis(db)
stations = get_stations(db)


for i_station, station_a in enumerate(stations[:-1]):
    for station_b in stations[i_station + 1 :]:
        sta_a = station_a.sta
        sta_b = station_b.sta
        nts_a = f"{station_a.net}_{sta_a}"
        nts_b = f"{station_b.net}_{sta_b}"
        n, ccfs = get_results(
            db, nts_a, nts_b, 1, "ZZ", datelist, 1, format="matrix", params=params
        )
        df = pd.DataFrame(ccfs, index=pd.DatetimeIndex(datelist), columns=taxis)
        df = df.dropna()
        clim = df.mean(axis="index").quantile(0.8)
        fig, ax = plt.subplots()
        plt.pcolormesh(
            df.columns,
            df.index.to_pydatetime(),
            df.values,
            vmin=-clim,
            vmax=clim,
            rasterized=True,
        )
        plt.colorbar()
        plt.title(f"Interferogram {sta_a} to {sta_b}")
        plt.xlabel("Lag Time (s)")
        plt.ylim(df.index[0], df.index[-1])
        plt.xlim(df.columns[0], df.columns[-1])
        plt.subplots_adjust(left=0.15)
        fig.savefig(f"Inteferograms{os.sep}{sta_a}_{sta_b}.png")
