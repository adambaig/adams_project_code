from datetime import datetime
from glob import glob
import numpy as np

tt_files = glob("Parkfield\\DTT\\02\\001_DAYS\\ZZ\\*.txt")

tts = {}
for tt_file in tt_files:
    f = open(tt_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    date = datetime.strptime(lines[0].split(",")[0], "%Y-%m-%d")
    tts[date] = {}
    for line in lines:
        lspl = line.split(",")
        tts[date][lspl[1]] = {
            "slope": float(lspl[2]),
            "slope error": float(lspl[3]),
            "y-int": float(lspl[4]),
            "y-int error": float(lspl[5]),
            "slope forced through origin": float(lspl[6]),
            "slope forced through origin error": float(lspl[7]),
        }

first_pair = ["_".join(k.split("_")[:2]) for k in tts[date].keys() if k != "ALL"]
second_pair = ["_".join(k.split("_")[2:]) for k in tts[date].keys() if k != "ALL"]
all_unique_stations = sorted(np.unique(np.hstack([first_pair, second_pair])))

g = open("Parkfield\\tomo_lite_results.csv", "w")
dv = {}
g.write("date")
[g.write(f",{sta}") for sta in all_unique_stations]
g.write("\n")
for date in sorted(tts.keys()):
    first_pair = ["_".join(k.split("_")[:2]) for k in tts[date].keys() if k != "ALL"]
    second_pair = ["_".join(k.split("_")[2:]) for k in tts[date].keys() if k != "ALL"]
    unique_stations = np.array(sorted(np.unique(np.hstack([first_pair, second_pair]))))
    n_sta = len(unique_stations)
    A_matrix = np.zeros([len(first_pair) + 1, n_sta])
    dv_v = np.zeros(len(first_pair) + 1)
    dv[date] = {}
    for i_row, (station_1, station_2) in enumerate(zip(first_pair, second_pair)):
        j_1 = np.where(unique_stations == station_1)[0]
        j_2 = np.where(unique_stations == station_2)[0]
        A_matrix[i_row, j_1] = 1
        A_matrix[i_row, j_2] = 1
        dv_v[i_row] = -tts[date][f"{station_1}_{station_2}"][
            "slope forced through origin"
        ]
    A_matrix[-1, :] = np.ones(n_sta) / n_sta
    dv_v[-1] = -tts[date]["ALL"]["slope forced through origin"]
    dv_indexed, _, _, _ = np.linalg.lstsq(A_matrix, dv_v)
    g.write(datetime.strftime(date, "%Y-%m-%d"))
    for v, station in zip(dv_indexed, all_unique_stations):
        if station in unique_stations:
            dv[date][station] = v
            g.write(f",{v:.6e}")
        else:
            g.write(",")
    g.write("\n")
g.close()
