import glob
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap, ScalarMappable
from matplotlib.colors import Normalize
import numpy as np
from obspy import read
import os
from scipy.signal import hilbert

from read_inputs import read_stations

cmap = get_cmap("gnuplot")

ZZ_path = os.sep.join(["STACKS", "01", "001_DAYS", "ZZ"])
pairs = glob.glob(f"{ZZ_path}{os.sep}*")
cos_bell = np.sin(np.arange(0, 10.001, 0.05) * np.pi / 10) ** 2


stations = read_stations()


def calc_interstation_distance(station_key1, station_key2):
    if station_key1 not in stations or station_key2 not in stations:
        return
    return np.sqrt(
        sum(
            [
                (stations[station_key1][c] - stations[station_key2][c]) ** 2
                for c in ["e", "n", "z"]
            ]
        )
    )


fig, ax = plt.subplots()
ax.set_facecolor("0.9")
max_distance = 0
for pair in pairs:
    station1 = "_".join(title.split("_")[:2])
    station2 = "_".join(title.split("_")[2:])
    distance = calc_interstation_distance(station1, station2)
    if distance > max_distance:
        max_distance = distance
    title = pair.split(os.sep)[-1]
    seeds = glob.glob(f"{pair}{os.sep}*")
    n_seeds = len(seeds)
    len(seeds)
    n_samples = 4801
    time_axis = np.arange(-2400, 2401) * 0.05
    cc_section = np.zeros([n_seeds, n_samples])
    for i_seed, seed in enumerate(seeds):
        st = read(seed)
        cc_section[i_seed, :] = st[0].data
    cc_stack = np.average(cc_section, axis=0)

    noise = np.sqrt(
        (np.average(cc_section**2, axis=0) - cc_stack**2) / (n_seeds - 1)
    )
    signal_envelope = abs(hilbert(cc_stack))
    snr = (np.convolve(signal_envelope, cos_bell) / np.convolve(noise, cos_bell))[
        100:-100
    ]
    ax.plot(time_axis, snr, color=cmap(distance / 6000))
    ax.set_xlim(-30, 30)
ax.set_xlabel("time (s)")
ax.set_ylabel("smoothed SNR")
cb = fig.colorbar(ScalarMappable(norm=Normalize(vmin=0, vmax=6000), cmap=cmap), ax=ax)
cb.set_label("interstation distance (m)")
fig.savefig("snr_by_disance.png", bbox_inches="tight")
