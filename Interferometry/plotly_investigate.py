import plotly.graph_objects as go
import pandas as pd


input = "tomo_lite_results.csv"

fig = go.Figure()

vel_df = pd.read_csv(input)
vel_df = vel_df.interpolate(method="linear", axis=0).ffill().bfill()


cols = list(vel_df.columns)

for col in cols[1:]:
    text = []
    for aa in vel_df[col]:
        text.append(str(aa))
    fig.add_trace(
        go.Scatter(
            x=vel_df[cols[0]],
            y=vel_df[col],
            name=str(col),
            text=text,
            # yaxis="y",
        )
    )

# style all the traces
fig.update_traces(
    hoverinfo="name+x+text",
    line={"width": 2},
    marker={"size": 8},
    mode="lines",
    showlegend=True,
)

fig.update_layout(
    title="Interferometry",
    xaxis_title="Date",
    yaxis_title="Velocity variation",
    legend_title="Station",
    xaxis=dict(
        rangeselector=dict(
            buttons=list(
                [
                    dict(count=1, label="1hr", step="hour", stepmode="backward"),
                    dict(count=3, label="3hr", step="hour", stepmode="backward"),
                    dict(count=1, label="1d", step="day", stepmode="backward"),
                    dict(count=10, label="10d", step="day", stepmode="backward"),
                    dict(step="all"),
                ]
            )
        ),
        rangeslider=dict(visible=True),
        type="date",
    ),
)

fig.show()
