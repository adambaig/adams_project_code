from datetime import datetime
import pandas as pd
from glob import glob


def read_stations(file_name):
    f = open(file_name)
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for jj, line in enumerate(lines):
        lspl = line.split(",")
        id = lspl[1] + "_" + lspl[2]
        stations[id] = {"e": float(lspl[3]), "n": float(lspl[4]), "z": float(lspl[5])}
    return stations


def read_mwcs(file_name):
    f = open(file_name)
    lines = f.readlines()
    f.close()
    mwcs = {}
    for line in lines:
        lspl = [float(s) for s in line.split()]
        mwcs[lspl[0]] = {"delay": lspl[1], "error": lspl[2], "coherence": lspl[3]}
    return mwcs


def read_tomo_by_days(file_name, stations):
    dv = {}
    f = open(file_name)
    head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        date = lspl[0]
        days = lspl[1]
        if days not in dv:
            dv[days] = {}
            station_codes = [h.strip() for h in head.split(",")[1:]]
        dv[days][date] = {}
        for i_station, entry in enumerate(lspl[2:]):
            if entry.strip() != "":
                dv[days][date][station_codes[i_station]] = float(entry)
    rows = []
    for i_days, days in enumerate(["001", "005", "010"]):
        for (
            date,
            data,
        ) in dv[days].items():
            for station, location in stations.items():
                rows.append(
                    {
                        "easting": location["e"],
                        "northing": location["n"],
                        "elevation": location["z"],
                        "date": date,
                        "dv": data[station] if station in data else None,
                        "station": station,
                        "days": days,
                    }
                )
    pd_rows = pd.DataFrame([row for row in rows])  # if row["days"] == "001"])
    return pd_rows


def read_tomo(file_name, stations):
    dv = {}
    f = open(file_name)
    head = f.readline()
    lines = f.readlines()
    f.close()

    for i_days, days in enumerate(["001", "005", "010"]):
        dv[days] = {}
        station_codes = [h.strip() for h in head.split(",")[1:]]
        for line in lines:
            lspl = line.split(",")
            date = lspl[0]
            dv[days][date] = {}
            for i_station, entry in enumerate(lspl[1:]):
                if entry.strip() != "":
                    dv[days][date][station_codes[i_station]] = float(entry)
    rows = []
    for i_days, days in enumerate(["001", "005", "010"]):
        for (
            date,
            data,
        ) in dv[days].items():
            for station, location in stations.items():
                rows.append(
                    {
                        "easting": location["e"],
                        "northing": location["n"],
                        "elevation": location["z"],
                        "date": date,
                        "dv": data[station] if station in data else None,
                        "station": station,
                        "days": days,
                    }
                )
    pd_rows = pd.DataFrame([row for row in rows])  # if row["days"] == "001"])
    return pd_rows


def read_dtt(path):
    # files=
    # dtt={}
    rows = []
    for i_days, days in enumerate(["001", "005", "010"]):
        files = glob(path + "/" + days + "_DAYS/ZZ/*.txt")
        for file_name in files:
            f = open(file_name)
            head = f.readline()
            lines = f.readlines()
            f.close()
            date = datetime.strptime(lines[0].split(",")[0], "%Y-%m-%d")
            # dtt[days]={}
            for line in lines:
                lspl = line.split(",")
                stationPair = lspl[1]
                if stationPair != "ALL":
                    stations = stationPair.split("_")
                    station1 = stations[0] + "_" + stations[1]
                    station2 = stations[2] + "_" + stations[3]
                    rows.append(
                        {
                            "station1": station1,
                            "station2": station2,
                            "date": str(lspl[0]),
                            "dvv": -1.0 * float(lspl[6]),
                            "days": days,
                        }
                    )
    pd_rows = pd.DataFrame([row for row in rows])
    return pd_rows
