from datetime import datetime
import matplotlib.pyplot as plt
from matplotlib import cm
import pandas as pd

# import dash
# import dash_core_components as dcc
# import dash_html_components as html
# from dash.dependencies import Input, Output
import plotly.express as px

from read_inputs import read_stations, read_dtt

px.colors.named_colorscales()
stations = read_stations("Parkfield//stations_parkfield.csv")
seismic_r = cm.get_cmap("seismic_r")

dv = {}
f = open("Parkfield//tomo_lite_results.csv")
head = f.readline()
lines = f.readlines()
f.close()

for i_days, days in enumerate(["001", "005", "010"]):
    dv[days] = {}
    station_codes = [h.strip() for h in head.split(",")[1:]]
    for line in lines:
        lspl = line.split(",")
        date = lspl[0]
        dv[days][date] = {}
        for i_station, entry in enumerate(lspl[1:]):
            if entry.strip() != "":
                dv[days][date][station_codes[i_station]] = float(entry)

xmax = max([v["e"] for v in stations.values()])
xmin = min([v["e"] for v in stations.values()])
ymax = max([v["n"] for v in stations.values()])
ymin = min([v["n"] for v in stations.values()])

xrange = xmax - xmin
yrange = ymax - ymin

rows = []
for i_days, days in enumerate(["001", "005", "010"]):
    for (
        date,
        data,
    ) in dv[days].items():
        for station, location in stations.items():
            rows.append(
                {
                    "easting": location["e"],
                    "northing": location["n"],
                    "elevation": location["z"],
                    "date": date,
                    "dv": data[station] if station in data else None,
                    "station": station,
                    "days": days,
                }
            )

pd_rows_001 = pd.DataFrame([row for row in rows if row["days"] == "001"])


dtt_dir = "Parkfield//DTT//02//001_DAYS//ZZ//"

fig = px.scatter(
    pd_rows_001,
    x="easting",
    y="northing",
    animation_frame="date",
    color="dv",
    hover_name="station",
    range_x=[xmin - 0.05 * xrange, xmax + 0.05 * xrange],
    range_y=[ymin - 0.05 * yrange, ymax + 0.05 * yrange],
    color_continuous_scale="rdbu",
    range_color=[-3e-4, 3e-4],
)

fig.update_traces(
    mode="markers",
    marker_size=10,
    marker_line_width=1,
)

fig.update_yaxes(scaleanchor="x", scaleratio=1)

fig.write_html("Parkfield_Test.html")


# pd_rows_005 = pd.DataFrame([row for row in rows if row["days"] == "005"])
# pd_rows_010 = pd.DataFrame([row for row in rows if row["days"] == "010"])
#
# animations = {
#     "1 day": px.scatter(
#         pd_rows_001,
#         x="easting",
#         y="northing",
#         animation_frame="date",
#         color="dv",
#         hover_name="station",
#         range_x=[xmin - 0.05 * xrange, xmax + 0.05 * xrange],
#         range_y=[ymin - 0.05 * yrange, ymax + 0.05 * yrange],
#         color_continuous_scale="rdbu",
#         range_color=[-2e-4, 2e-4],
#     ),
#     "5 days": px.scatter(
#         pd_rows_005,
#         x="easting",
#         y="northing",
#         animation_frame="date",
#         color="dv",
#         hover_name="station",
#         range_x=[xmin - 0.05 * xrange, xmax + 0.05 * xrange],
#         range_y=[ymin - 0.05 * yrange, ymax + 0.05 * yrange],
#         color_continuous_scale="rdbu",
#         range_color=[-2e-4, 2e-4],
#     ),
#     "10 day": px.scatter(
#         pd_rows_010,
#         x="easting",
#         y="northing",
#         animation_frame="date",
#         color="dv",
#         hover_name="station",
#         range_x=[xmin - 0.05 * xrange, xmax + 0.05 * xrange],
#         range_y=[ymin - 0.05 * yrange, ymax + 0.05 * yrange],
#         color_continuous_scale="rdbu",
#         range_color=[-2e-4, 2e-4],
#     ),
# }

# app = dash.Dash(__name__)
#
# app.layout = html.Div(
#     [
#         html.P("Select a duration"),
#         dcc.RadioItems(id="selection", options=[{"label": x, "value": x} for x in animations], value="1 day"),
#         dcc.Graph(id="graph"),
#     ]
# )
#
#
# @app.callback(Output("graph", "figure"), [Input("selection", "value")])
# def display_animated_graph(s):
#     return animations[s]
#
#
# "a is".startswith("a")
#
# app.run_server(debug=True)


# scale = 400
# vmax = 1e-3
# vmin = -vmax
# for date in list(dv['001'].keys()):
#     dtt = read_dtt(f"{dtt_dir}{date}.txt")
#     fig, ax = plt.subplots(figsize=[10, 10])
#     ax.set_aspect("equal")
#
#     for pair, result in dtt.items():
#         if pair != "ALL":
#             station1 = "_".join(pair.split("_")[:2])
#             station2 = "_".join(pair.split("_")[2:])
#             ax.plot(
#                 [stations[s]["e"] for s in [station1, station2]],
#                 [stations[s]["n"] for s in [station1, station2]],
#                 color=seismic_r((-result["slope forced through origin"] + vmax) / 2 / vmax),
#             )
#     scatter = ax.scatter(
#         [stations[k]["e"] for k in dv['001'][date].keys()],
#         [stations[k]["n"] for k in dv['001'][date].keys()],
#         c=list(dv['001'][date].values()),
#         vmax=vmax,
#         vmin=vmin,
#         cmap="seismic_r",
#         edgecolor="0.1",
#         s=scale,
#         zorder=4,
#     )
#
#     ax.set_xlim([xmin - 0.05 * xrange, xmax + 0.05 * xrange])
#     ax.set_ylim([ymin - 0.05 * yrange, ymax + 0.05 * yrange])
#     dt = datetime.strptime(date, "%Y-%m-%d")
#     title = datetime.strftime(dt, "%b %d, %Y")
#     ax.set_title(title)
#     ax = gray_background_with_grid(ax, grid_spacing=2000)
#     cb = fig.colorbar(scatter)
#     cb.set_label("relative velocity variation from ZZ")
#     fig.savefig(datetime.strftime(dt, "Parkfield//tomo_plots//ZZ_%Y%m%d.png"), bbox_inches="tight")
# #     plt.close(fig)
