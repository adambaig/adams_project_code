import plotly.graph_objects as go
import pandas as pd
import plotly.express as px
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Output, Input, State
import dash
import numpy as np
from plotly.subplots import make_subplots
from read_inputs import read_stations, read_dtt, read_tomo, read_tomo_by_days
from matplotlib import cm
from matplotlib.colors import to_hex
from datetime import datetime
from dash.exceptions import PreventUpdate

threshold = 0.0002

px.colors.named_colorscales()
seismic_r = cm.get_cmap("seismic_r")

stations = read_stations("stations_parkfield.csv")
# df_tomo = read_tomo("tomo_lite_results.csv", stations)
df_tomo = read_tomo_by_days("tomo_lite_results_by_days.csv", stations)
dtt_pairs = read_dtt(r"C:\Users\adambaig\Project\Interferometry\dtt_Data\02_new")
# dtt_pairs = read_dtt(r"C:\Users\adambaig\Project\Interferometry\dtt_Data\02")

xmax = max([v["e"] for v in stations.values()])
xmin = min([v["e"] for v in stations.values()])
ymax = max([v["n"] for v in stations.values()])
ymin = min([v["n"] for v in stations.values()])

xrange = xmax - xmin
yrange = ymax - ymin

t1 = datetime.strptime(np.sort(np.unique(df_tomo["date"]))[0], "%Y-%m-%d").timestamp()
t2 = datetime.strptime(np.sort(np.unique(df_tomo["date"]))[1], "%Y-%m-%d").timestamp()
tstep = t2 - t1

app = dash.Dash(__name__)

vel_width_basic = 2
mint = datetime.strptime(min(df_tomo["date"]), "%Y-%m-%d").timestamp()
maxt = datetime.strptime(max(df_tomo["date"]), "%Y-%m-%d").timestamp()

tticks = np.linspace(mint + 8 * 86400, maxt - 8 * 86400, 5)
tlabels = []
for tt in tticks:
    ct = datetime.fromtimestamp(tt).strftime("%Y-%m-%d")
    # ct=ct+'\r'+datetime.fromtimestamp(tt).strftime("%H:%M")
    tlabels.append(ct)

marks = {int(i): {"label": str(j)} for i, j in zip(tticks, tlabels)}

app.layout = html.Div(
    [
        # dcc.Store(id='rawData'),
        dcc.Store(id="NdaySmoothData_dvv"),
        dcc.Store(id="NdaySmoothData_dvv_pairs"),
        dcc.Store(id="singleDayData_dvv"),
        dcc.Store(id="singleDayData_dvv_pairs"),
        dcc.Store(id="firstCall"),
        # dcc.Store(id='timestamp'),
        dcc.Store(id="stationMemory"),
        dcc.Store(id="memory"),
        dcc.Store(id="stationList"),
        # dcc.Store(id='storeDvv'),
        # dcc.Store(id='storeDvvPairs'),
        # dcc.Store(id='storeDvvSmooth'),
        # dcc.Store(id='storeDvvPairsSmooth'),
        html.Div(
            [
                html.P("Days to smooth over:"),
                dcc.RadioItems(
                    id="Ndays",
                    options=[
                        {"label": str(int(i)), "value": i}
                        for i in np.unique(df_tomo["days"])
                    ],
                    value=np.unique(df_tomo["days"])[0],
                    labelStyle={"display": "inline-block", "vertical-align": "bottom"},
                ),
            ],
        ),
        html.Div(
            [
                dcc.Graph(
                    id="stations-plot",
                ),
            ],
            style={"width": "20%", "display": "inline-block", "vertical-align": "top"},
        ),
        html.Div(
            [
                dcc.Graph(
                    id="velocity-graph",
                ),
            ],
            style={"display": "inline-block", "width": "70%"},
        ),
        html.Div(
            [], style={"width": "20%", "display": "inline-block", "padding": "0 20"}
        ),
        html.Div(
            [
                dcc.Slider(
                    id="date-slider",
                    updatemode="drag",
                    min=mint,
                    max=maxt,
                    step=tstep,
                    value=maxt,
                    marks=marks,
                    # tooltip={'always_visible': True}
                ),
            ],
            style={"display": "inline-block", "width": "63%", "padding": "0 0 0 2%"},
        ),
    ],
)
################################################################################
@app.callback(
    [Output("NdaySmoothData_dvv", "data"), Output("NdaySmoothData_dvv_pairs", "data")],
    [
        Input("Ndays", "value"),
        Input("NdaySmoothData_dvv", "modified_timestamp"),
    ],
    [State("NdaySmoothData_dvv", "data")],
)
def filter_data_smoothDays(ndays, ts, dvvs):
    dvvs = df_tomo[df_tomo.days == ndays].to_json(date_format="iso", orient="split")
    dvv_pairs = dtt_pairs[dtt_pairs.days == ndays].to_json(
        date_format="iso", orient="split"
    )
    # print("1",ts)
    return [dvvs, dvv_pairs]


@app.callback(
    [Output("singleDayData_dvv", "data"), Output("singleDayData_dvv_pairs", "data")],
    [
        Input("date-slider", "value"),
        Input("NdaySmoothData_dvv", "data"),
        Input("NdaySmoothData_dvv_pairs", "data"),
    ],
)
def filter_data_showDate(slideDate, dvv_in, dvv_pairs_in):
    pickDate = datetime.fromtimestamp(slideDate).strftime("%Y-%m-%d")
    dvv = pd.read_json(dvv_in, orient="split")
    dvv_pair = pd.read_json(dvv_pairs_in, orient="split")
    dvvs = dvv[dvv.date == pickDate].to_json(date_format="iso", orient="split")
    dvvPairs = dvv_pair[dvv_pair.date == pickDate].to_json(
        date_format="iso", orient="split"
    )
    return [dvvs, dvvPairs]


@app.callback(
    Output("stations-plot", "figure"),
    # Output('firstCall', 'data')],
    [
        Input("singleDayData_dvv", "data"),
        Input("singleDayData_dvv_pairs", "data"),
        Input("firstCall", "modified_timestamp"),
    ],
    [State("firstCall", "data")],
)
def plot_stations_and_pairs(dvv_in, dvv_pairs_in, ts, fs):
    # print(ts)
    fs = fs or {}
    dvvs = pd.read_json(dvv_in, orient="split")
    dvvPairs = pd.read_json(dvv_pairs_in, orient="split")
    # if ts is None:
    fig = px.scatter(
        dvvs,
        x="easting",
        y="northing",
        color="dv",
        hover_name="station",
        range_x=[xmin - 0.05 * xrange, xmax + 0.05 * xrange],
        range_y=[ymin - 0.05 * yrange, ymax + 0.05 * yrange],
        color_continuous_scale="rdbu",
        range_color=[-3e-4, 3e-4],
    )
    fig.update_traces(
        mode="markers",
        marker_size=10,
        marker_line_width=1,
    )
    fig.update_yaxes(scaleanchor="x", scaleratio=1)
    for index, row in dvvPairs.iterrows():
        dfE = dvvs["easting"].tolist()
        dfN = dvvs["northing"].tolist()
        dfv = row["dvv"]
        inde1 = np.where(dvvs["station"] == str(row["station1"]))[0][0]
        inde2 = np.where(dvvs["station"] == str(row["station2"]))[0][0]
        indn1 = np.where(dvvs["station"] == str(row["station1"]))[0][0]
        indn2 = np.where(dvvs["station"] == str(row["station2"]))[0][0]

        col = seismic_r((dfv + 3e-4) / 2 / 3e-4)
        col1 = []
        for ii in col:
            col1.append(ii)
        # print(col1)
        # hexcol='#{:02x}{:02x}{:02x}'.format(col1)
        hexcol = to_hex(col1, keep_alpha=False)
        # print(hexcol)

        fig.add_shape(
            type="line",
            x0=dfE[inde1],
            y0=dfN[inde1],
            x1=dfE[inde2],
            y1=dfN[inde2],
            # color_continuous_scale="rdbu",
            # range_color=[-3e-4, 3e-4],
            line=dict(color=hexcol, width=0.75),
            opacity=0.5,
        )
    #     print(fig)
    # else:
    #     print("second")

    #
    return fig


@app.callback(
    [
        Output("velocity-graph", "figure"),
        Output("memory", "data"),
        Output("stationMemory", "data"),
        Output("stationList", "data"),
    ],
    [
        Input("NdaySmoothData_dvv", "data"),
        Input("date-slider", "value"),
        Input("stations-plot", "clickData"),
        Input("velocity-graph", "clickData"),
        Input("velocity-graph", "relayoutData"),
    ],
    [
        State("memory", "data"),
        State("stationMemory", "data"),
        State("stationList", "data"),
    ],
)
def update_dvv_graph(
    dvv_in,
    slideDate,
    clickData_Station,
    clickData_Velocity,
    relayData,
    storeRange,
    storeStation,
    stationList,
):
    dvvs = pd.read_json(dvv_in, orient="split")
    pickDate = datetime.fromtimestamp(slideDate).strftime("%Y-%m-%d")
    storeRange = relayData
    storeStation = storeStation or None
    dvvs = pd.read_json(dvv_in, orient="split")
    Vxrange = [min(dvvs["date"]), max(dvvs["date"])]
    if storeRange is not None:
        if "xaxis.range[0]" in storeRange:
            Vxrange = [storeRange["xaxis.range[0]"], storeRange["xaxis.range[1]"]]
        elif "xaxis.range" in storeRange:
            Vxrange = storeRange["xaxis.range"]

    ctx = dash.callback_context
    change = str(ctx.triggered[0]["prop_id"])

    fig1 = go.Figure()
    stationList = stationList or np.unique(dvvs["station"])
    stationList = np.asarray(stationList)
    if change == "stations-plot.clickData":
        storeStation = ctx.triggered[0]["value"]["points"][0]["hovertext"]
    elif change == "velocity-graph.clickData":
        storeStation = stationList[
            ctx.triggered[0]["value"]["points"][0]["curveNumber"]
        ]

    if storeStation is not None:
        stationList = np.append(
            stationList[np.where(stationList != storeStation)], storeStation
        )
    # print(dvvs)
    for station in stationList:
        tmp_df = dvvs[dvvs.station == station]

        if str(station) == storeStation:
            vel_width = vel_width_basic * 2
            if abs(np.array(tmp_df["dv"])[-1]) > threshold:
                pcolor = "red"
            else:
                pcolor = "green"
        else:
            vel_width = vel_width_basic
            if abs(np.array(tmp_df["dv"])[-1]) > threshold:
                # print(np.array(tmp_df['dv'])[-1],threshold)
                pcolor = "lightsalmon"
            else:
                pcolor = "lightgrey"
        # showlegend=True
        # if np.max(abs(tmp_df['dv'])) > threshold:
        #     # showlegend=False
        #
        # if showlegend:
        fig1.add_trace(
            go.Scatter(
                x=tmp_df["date"],
                y=tmp_df["dv"],
                name=station,
                showlegend=True,
                mode="lines",
                line={"width": vel_width, "color": pcolor},
            )
        )
        # else:
        #     fig1.add_trace(go.Scatter(
        #         x=tmp_df['date'],
        #         y=tmp_df['dv'],
        #         name=station,
        #         showlegend=showlegend,
        #         mode="lines",
        #         line={"width": vel_width,
        #               'color': pcolor},
        #         ))
        #     inds=np.where(abs(tmp_df['dv']) >= threshold)[0]
        #     # print(inds)
        #     # print((np.array(tmp_df['date'])[inds]))
        #     fig1.add_trace(go.Scatter(
        #         x=np.array(tmp_df['date'])[inds],
        #         y=np.array(tmp_df['dv'])[inds],
        #         name=station,
        #         showlegend=True,
        #         mode="lines",
        #         line={"width": vel_width,
        #               'color': threshcol},
        #         ))
    fig1.add_vline(x=pickDate, line_dash="dash")
    fig1.update_traces(
        hoverinfo="name+x+y",
    )

    fig1.update_layout(
        yaxis_title="Velocity variation",
        legend_title="Station",
        height=1000,
        xaxis=dict(
            range=Vxrange,
            rangeselector=dict(
                buttons=list(
                    [
                        dict(count=1, label="1d", step="day", stepmode="backward"),
                        dict(count=10, label="10d", step="day", stepmode="backward"),
                        dict(count=30, label="30d", step="day", stepmode="backward"),
                        dict(count=90, label="90d", step="day", stepmode="backward"),
                        dict(step="all"),
                    ]
                )
            ),
            rangeslider=dict(visible=True),
            type="date",
            title="Date",
        ),
    )

    return [fig1, storeRange, storeStation, stationList]

    # else:
    #     Vxrange=[min(df_tomo["date"]),max(df_tomo["date"])]


#     ctx = dash.callback_context
#     change=str(ctx.triggered[0]['prop_id'])
#
#     # df=df_tomo[df_tomo.days==ndays]
#     # df_pairs=dtt_pairs[dtt_pairs.days==ndays]
#
#     pickDate=datetime.fromtimestamp(slideDate).strftime("%Y-%m-%d")
#     # df_pairs=df_pairs[df_pairs.date==pickDate]
#
#     fig = px.scatter(
#         dvvs,
#         # df[df.date==pickDate],
#         x="easting",
#         y="northing",
#         color="dv",
#         hover_name="station",
#         range_x=[xmin - 0.05 * xrange, xmax + 0.05 * xrange],
#         range_y=[ymin - 0.05 * yrange, ymax + 0.05 * yrange],
#         color_continuous_scale="rdbu",
#         range_color=[-3e-4, 3e-4],
#     )
#
#     fig.update_traces(
#         mode="markers", marker_size=10, marker_line_width=1,
#     )
#
#     for index, row in dvvPairs.iterrows():
#         dfE=dvvs['easting'].tolist()
#         dfN=dvvs['northing'].tolist()
#         dfv=row['dvv']
#         inde1=np.where(dvvs["station"]==str(row["station1"]))[0][0]
#         inde2=np.where(dvvs["station"]==str(row["station2"]))[0][0]
#         indn1=np.where(dvvs["station"]==str(row["station1"]))[0][0]
#         indn2=np.where(dvvs["station"]==str(row["station2"]))[0][0]
#
#         col=(seismic_r((dfv+3e-4)/2/3e-4))
#         col1=[]
#         for ii in col:
#             col1.append(ii)
#         # print(col1)
#         # hexcol='#{:02x}{:02x}{:02x}'.format(col1)
#         hexcol=to_hex(col1, keep_alpha=False)
#         # print(hexcol)
#
#         fig.add_shape(type="line",
#             x0=dfE[inde1], y0=dfN[inde1], x1=dfE[inde2], y1=dfN[inde2],
#             # color_continuous_scale="rdbu",
#             # range_color=[-3e-4, 3e-4],
#             line=dict(color=hexcol,width=0.75),opacity=0.5
#         )
#
#     fig.update_yaxes(scaleanchor="x", scaleratio=1)
#
#     fig1=go.Figure()
#     stationList=stationList or np.unique(df['station'])
#     stationList=np.asarray(stationList)
#     if change=='stations-plot.clickData':
#         storeStation=ctx.triggered[0]['value']['points'][0]['hovertext']
#     elif change=='velocity-graph.clickData':
#         storeStation=stationList[ctx.triggered[0]['value']['points'][0]['curveNumber']]
#
#     if storeStation is not None:
#         stationList=np.append(stationList[np.where(stationList != storeStation)],storeStation)
#
#     for station in stationList:
#         if str(station)==storeStation:
#             vel_width=vel_width_basic*2
#             pcolor='orange'
#         else:
#             vel_width=vel_width_basic
#             pcolor='lightgrey'
#         tmp_df=dvvs[dvvs.station==station]
#         fig1.add_trace(go.Scatter(
#             x=tmp_df['date'],
#             y=tmp_df['dv'],
#             name=station,
#             mode="lines",
#             line={"width": vel_width,
#                   'color': pcolor},
#             ))
#     fig1.add_vline(x=pickDate, line_dash='dash')
#     fig1.update_traces(
#         hoverinfo="name+x+y",
#     )
#
#     fig1.update_layout(
#     yaxis_title="Velocity variation",
#     legend_title="Station",
#     height=1000,
#     xaxis=dict(
#         range=Vxrange,
#         rangeselector=dict(
#             buttons=list([
#                 dict(count=1,
#                      label="1d",
#                      step="day",
#                      stepmode="backward"),
#                 dict(count=10,
#                      label="10d",
#                      step="day",
#                      stepmode="backward"),
#                 dict(count=30,
#                      label="30d",
#                      step="day",
#                      stepmode="backward"),
#                 dict(count=90,
#                      label="90d",
#                      step="day",
#                      stepmode="backward"),
#                 dict(step="all")
#             ])
#         ),
#         rangeslider=dict(visible=True),
#         type="date",title='Date',
#     ),
#     )
#     return [fig,fig1,storeRange,storeStation,stationList]


# @app.callback(Output('stations-plot', 'figure.data'),
#               [Input('stations-plot', 'figure.data'),
#                Input('singleDayData_dvv_pairs', 'data')],
#              )
# def plot_pairs(fig,dvv_pairs_in):
#     dvvPairs=pd.read_json(dvv_pairs_in, orient='split')
#     for index, row in dvvPairs.iterrows():
#         dfE=dvvs['easting'].tolist()
#         dfN=dvvs['northing'].tolist()
#         dfv=row['dvv']
#         inde1=np.where(dvvs["station"]==str(row["station1"]))[0][0]
#         inde2=np.where(dvvs["station"]==str(row["station2"]))[0][0]
#         indn1=np.where(dvvs["station"]==str(row["station1"]))[0][0]
#         indn2=np.where(dvvs["station"]==str(row["station2"]))[0][0]
#
#         col=(seismic_r((dfv+3e-4)/2/3e-4))
#         col1=[]
#         for ii in col:
#             col1.append(ii)
#         # print(col1)
#         # hexcol='#{:02x}{:02x}{:02x}'.format(col1)
#         hexcol=to_hex(col1, keep_alpha=False)
#         # print(hexcol)
#
#         fig.add_shape(type="line",
#             x0=dfE[inde1], y0=dfN[inde1], x1=dfE[inde2], y1=dfN[inde2],
#             # color_continuous_scale="rdbu",
#             # range_color=[-3e-4, 3e-4],
#             line=dict(color=hexcol,width=0.75),opacity=0.5
#         )
#     return fig

# @app.callback([Output('stations-plot', 'figure'),
#                Output('velocity-graph', 'figure'),
#                Output('memory', 'data'),
#                Output('stationMemory', 'data'),
#                Output('stationList', 'data')],
#               [Input('Ndays', 'value'),
#                Input('date-slider','value'),
#                Input('stations-plot', 'clickData'),
#                Input('velocity-graph', 'clickData'),
#                Input('velocity-graph', 'relayoutData')],
#               [State('memory', 'data'),
#                State('stationMemory', 'data'),
#                State('stationList', 'data'),
#                State('storeDvv', 'data'),
#                State('storeDvvPairs', 'data')],
#              )
# def update_graph(ndays,slideDate,clickData_Station,clickData_Velocity,relayData,
#                  storeRange,storeStation,stationList):
#     storeRange = relayData
#     storeStation=storeStation or None
#     Vxrange=[min(df_tomo["date"]),max(df_tomo["date"])]
#     if storeRange is not None:
#         if('xaxis.range[0]' in storeRange):
#             Vxrange=[storeRange['xaxis.range[0]'],storeRange['xaxis.range[1]']]
#         elif ('xaxis.range' in storeRange):
#             Vxrange=storeRange['xaxis.range']
#         else:
#             Vxrange=[min(df_tomo["date"]),max(df_tomo["date"])]
#     ctx = dash.callback_context
#     change=str(ctx.triggered[0]['prop_id'])
#
#     # df=df_tomo[df_tomo.days==ndays]
#     # df_pairs=dtt_pairs[dtt_pairs.days==ndays]
#
#     pickDate=datetime.fromtimestamp(slideDate).strftime("%Y-%m-%d")
#     # df_pairs=df_pairs[df_pairs.date==pickDate]
#
#     fig = px.scatter(
#         dvvs,
#         # df[df.date==pickDate],
#         x="easting",
#         y="northing",
#         color="dv",
#         hover_name="station",
#         range_x=[xmin - 0.05 * xrange, xmax + 0.05 * xrange],
#         range_y=[ymin - 0.05 * yrange, ymax + 0.05 * yrange],
#         color_continuous_scale="rdbu",
#         range_color=[-3e-4, 3e-4],
#     )
#
#     fig.update_traces(
#         mode="markers", marker_size=10, marker_line_width=1,
#     )
#
#     for index, row in dvvPairs.iterrows():
#         dfE=dvvs['easting'].tolist()
#         dfN=dvvs['northing'].tolist()
#         dfv=row['dvv']
#         inde1=np.where(dvvs["station"]==str(row["station1"]))[0][0]
#         inde2=np.where(dvvs["station"]==str(row["station2"]))[0][0]
#         indn1=np.where(dvvs["station"]==str(row["station1"]))[0][0]
#         indn2=np.where(dvvs["station"]==str(row["station2"]))[0][0]
#
#         col=(seismic_r((dfv+3e-4)/2/3e-4))
#         col1=[]
#         for ii in col:
#             col1.append(ii)
#         # print(col1)
#         # hexcol='#{:02x}{:02x}{:02x}'.format(col1)
#         hexcol=to_hex(col1, keep_alpha=False)
#         # print(hexcol)
#
#         fig.add_shape(type="line",
#             x0=dfE[inde1], y0=dfN[inde1], x1=dfE[inde2], y1=dfN[inde2],
#             # color_continuous_scale="rdbu",
#             # range_color=[-3e-4, 3e-4],
#             line=dict(color=hexcol,width=0.75),opacity=0.5
#         )
#
#     fig.update_yaxes(scaleanchor="x", scaleratio=1)
#
#     fig1=go.Figure()
#     stationList=stationList or np.unique(df['station'])
#     stationList=np.asarray(stationList)
#     if change=='stations-plot.clickData':
#         storeStation=ctx.triggered[0]['value']['points'][0]['hovertext']
#     elif change=='velocity-graph.clickData':
#         storeStation=stationList[ctx.triggered[0]['value']['points'][0]['curveNumber']]
#
#     if storeStation is not None:
#         stationList=np.append(stationList[np.where(stationList != storeStation)],storeStation)
#
#     for station in stationList:
#         if str(station)==storeStation:
#             vel_width=vel_width_basic*2
#             pcolor='orange'
#         else:
#             vel_width=vel_width_basic
#             pcolor='lightgrey'
#         tmp_df=dvvs[dvvs.station==station]
#         fig1.add_trace(go.Scatter(
#             x=tmp_df['date'],
#             y=tmp_df['dv'],
#             name=station,
#             mode="lines",
#             line={"width": vel_width,
#                   'color': pcolor},
#             ))
#     fig1.add_vline(x=pickDate, line_dash='dash')
#     fig1.update_traces(
#         hoverinfo="name+x+y",
#     )
#
#     fig1.update_layout(
#     yaxis_title="Velocity variation",
#     legend_title="Station",
#     height=1000,
#     xaxis=dict(
#         range=Vxrange,
#         rangeselector=dict(
#             buttons=list([
#                 dict(count=1,
#                      label="1d",
#                      step="day",
#                      stepmode="backward"),
#                 dict(count=10,
#                      label="10d",
#                      step="day",
#                      stepmode="backward"),
#                 dict(count=30,
#                      label="30d",
#                      step="day",
#                      stepmode="backward"),
#                 dict(count=90,
#                      label="90d",
#                      step="day",
#                      stepmode="backward"),
#                 dict(step="all")
#             ])
#         ),
#         rangeslider=dict(visible=True),
#         type="date",title='Date',
#     ),
#     )
#     return [fig,fig1,storeRange,storeStation,stationList]

if __name__ == "__main__":
    app.run_server(debug=True)
