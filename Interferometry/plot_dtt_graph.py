from datetime import datetime
import glob
import os
from random import shuffle

from matplotlib.collections import LineCollection
from matplotlib.dates import DateFormatter, date2num, MonthLocator, DayLocator
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np

from read_inputs import read_mwcs, read_stations, read_dtt

min_coh, max_err = 0.65, 0.1
date_format = DateFormatter("%b %y")


stations = read_stations("stations_parkfield.csv")


def make_simple_gradient_from_white_cmap(end_color):
    red, green, blue = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, 1.0, 1.0), (1, red, red)],
        "green": [(0.0, 1.0, 1.0), (1, green, green)],
        "blue": [(0.0, 1.0, 1.0), (1, blue, blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


def distance_from_pair(pair):
    first_pair = "_".join(pair.split("_")[:2])
    second_pair = "_".join(pair.split("_")[2:])
    return np.sqrt(
        sum(
            [
                (stations[first_pair][c] - stations[second_pair][c]) ** 2
                for c in ["e", "n", "z"]
            ]
        )
    )


cmap = make_simple_gradient_from_white_cmap("indigo")

pair_directory = r"Parkfield\MWCS\02\001_DAYS\ZZ\BP_CCRB_BP_SCYB"
station_pair = pair_directory.split(os.sep)[-1]
date = "2004-10-20"
dtt = read_dtt(f"dtt_Data//02//001_DAYS//ZZ//{date}.txt")
dtt[station_pair]
mwcs = read_mwcs(f"{pair_directory}//{date}.txt")
lags = np.array(list(mwcs.keys()))
delays = np.array([v["delay"] for v in mwcs.values()])
errors = np.array([v["error"] for v in mwcs.values()])
coherence = np.array([v["coherence"] for v in mwcs.values()])
fig, ax = plt.subplots(figsize=[12, 8])

scatter = ax.scatter(
    lags, delays, c=coherence, vmin=min_coh, vmax=1, cmap=cmap, edgecolor="0.2"
)
_, __, error_line_collections = ax.errorbar(
    [k for k, v in mwcs.items() if v["error"] > max_err],
    [v["delay"] for v in mwcs.values() if v["error"] > max_err],
    yerr=[v["error"] for v in mwcs.values() if v["error"] > max_err],
    marker="",
    ls="",
)

ax.plot(
    [lags[0], lags[-1]],
    [dtt[station_pair]["slope"] * lags[0], dtt[station_pair]["slope"] * lags[-1]],
    "firebrick",
    lw=2,
)
ax.grid(True)
ax.set_xlabel("lags (s)")
ax.set_ylabel("delay (s)")
ax.set_title(f"{station_pair.replace('_',' ')} {date}")


fig.savefig(rf"figures/{station_pair}_{date.replace('-','_')}.png")
