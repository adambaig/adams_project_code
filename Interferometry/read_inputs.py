from datetime import datetime


def read_stations(file_name="stations.csv"):
    f = open(file_name)
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        id = lspl[1] + "_" + lspl[2]
        stations[id] = {"e": float(lspl[3]), "n": float(lspl[4]), "z": float(lspl[5])}
    return stations


def read_mwcs(file_name):
    f = open(file_name)
    lines = f.readlines()
    f.close()
    mwcs = {}
    for line in lines:
        lspl = [float(s) for s in line.split()]
        mwcs[lspl[0]] = {"delay": lspl[1], "error": lspl[2], "coherence": lspl[3]}
    return mwcs


def read_dtt(file_name, hourly=False):
    f = open(file_name)
    head = f.readline()
    lines = f.readlines()
    f.close()
    if len(lines) == 0:
        return None
    if hourly:
        frmt = "%Y-%m-%d %H:%M:%S"
    else:
        frmt = "%Y-%m-%d"
    date = datetime.strptime(lines[0].split(",")[0], frmt)
    dtt = {}
    for line in lines:
        lspl = line.split(",")
        dtt[lspl[1]] = {
            "slope": float(lspl[2]),
            "slope error": float(lspl[3]),
            "y-int": float(lspl[4]),
            "y-int error": float(lspl[5]),
            "slope forced through origin": float(lspl[6]),
            "slope forced through origin error": float(lspl[7]),
        }
    return dtt
