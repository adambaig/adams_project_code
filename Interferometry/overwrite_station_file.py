import numpy as np
import pyproj as pr
import os
from obspy import read_inventory

from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from NocMeta.Meta import NOC_META

athena_code = "CBMM"
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")


if os.path.isfile(f"Station_xml\{athena_code}_Full.xml"):
    inv = read_inventory(f"Station_xml\{athena_code}_Full.xml")
else:
    inv = formStaXml(outDir="Station_xml///.", athenaCode=athena_code)

stations = {}
for network in inv:
    for station in network:
        station_east, station_north = pr.transform(
            latlon_proj, out_proj, station.longitude, station.latitude
        )
        stations[network.code + "_" + station.code] = {
            "e": station_east,
            "n": station_north,
            "z": station.elevation,
        }

f = open("stations_00.csv")
head = f.readline()
lines = f.readlines()
f.close()

g = open("stations.csv", "w")
g.write(head)
for line in lines:
    lspl = line.split(",")
    net_sta = "_".join(lspl[1:3])
    lspl[3] = f'{stations[net_sta]["e"]:.1f}'
    lspl[4] = f'{stations[net_sta]["n"]:.1f}'
    lspl[5] = f'{stations[net_sta]["z"]:.1f}'
    g.write(",".join(lspl))
g.close()

24 * 65 / 20.0
