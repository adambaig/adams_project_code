from collections import OrderedDict
import pyproj as pr
import os
from obspy import read_inventory

from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from NocMeta.Meta import NOC_META

athena_code = "CBMM"
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")


if os.path.isfile(f"Station_xml\{athena_code}_Full.xml"):
    inv = read_inventory(f"Station_xml\{athena_code}_Full.xml")
else:
    inv = formStaXml(outDir="Station_xml///.", athenaCode=athena_code)

stations = {}
for network in inv:
    for station in network:
        station_east, station_north = pr.transform(
            latlon_proj, out_proj, station.longitude, station.latitude
        )
        stations[network.code + "_" + station.code] = {
            "e": station_east,
            "n": station_north,
            "z": station.elevation,
        }


def populate(data_folder):
    stationdict = {}
    for net_sta, location in OrderedDict(stations).items():
        net, sta = split("_")
        stationdict[net_sta] = [
            net,
            sta,
            location["e"],
            location["n"],
            location["z"],
            "UTM",
            "N/A",
        ]
    return stationdict
