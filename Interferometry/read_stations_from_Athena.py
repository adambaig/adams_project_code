import os

import pyproj

from obspy import Inventory
from nmxseis.interact.athena import AthenaClient
from NocMeta.Meta import NOC_META

ATHENA_CODE = "DOW"
NETWORK_CODES = ["DC"]
HEAD = "ref,net,sta,X,Y,altitude,coordinates,instrument,used\n"
OUTPATH = os.path.join("dow", "stations.csv")

athena_config = NOC_META[ATHENA_CODE]
reproject = pyproj.Proj(f"epsg:{athena_config['epsg']}")
athena_client = AthenaClient(
    rf'http://{athena_config["athIP"]}', athena_config["athApi"]
)

inv = Inventory()
for code in NETWORK_CODES:
    inv += athena_client.read_network_inventory(code)

output = []
ii = 0
for network in inv:
    for station in network:
        ii += 1
        latitude = station.latitude
        longitude = station.longitude
        elevation = station.elevation
        east, north = reproject(longitude, latitude)
        out_line = (
            f"{ii},{network.code},{station.code},"
            f"{east:.1f},{north:.1f},{elevation:.1f},UTM,N/A,1\n"
        )
        output.append(out_line)
        print(f"{station.code}, {station.latitude:.6f}, {station.longitude:.6f}")

with open(OUTPATH, "w") as f:
    f.write(HEAD)
    for line in output:
        f.write(line)
