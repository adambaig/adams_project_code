#!/bin/bash
# usage sh update_stations_table.sh [station_file_name] 
for line in `tail -n +2 $1`
do
	IFS=',' read -r -a strarr <<< "$line"
  msnoise db execute "update stations set X=${strarr[3]}, Y=${strarr[4]}, altitude=${strarr[5]} where net='${strarr[1]}' and sta='${strarr[2]}';"
done
