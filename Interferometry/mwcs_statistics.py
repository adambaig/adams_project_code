from datetime import datetime
import glob
import os
from random import shuffle

from matplotlib.collections import LineCollection
from matplotlib.dates import DateFormatter, date2num, MonthLocator, DayLocator
import matplotlib.pyplot as plt
import numpy as np

from read_inputs import read_mwcs, read_stations

min_coh, max_err = 0.65, 0.1
date_format = DateFormatter("%b %y")


stations = read_stations("dow/stations.csv")


def distance_from_pair(pair):
    first_pair = "_".join(pair.split("_")[:2])
    second_pair = "_".join(pair.split("_")[2:])
    return np.sqrt(
        sum(
            [
                (stations[first_pair][c] - stations[second_pair][c]) ** 2
                for c in ["e", "n", "z"]
            ]
        )
    )


n_reject, distance = [], []
for dir in glob.glob(rf"dow\MWCS\01\008_HOURS\ZZ\DC*"):
    distance.append(distance_from_pair(dir.split(os.sep)[-1]))
    reject = {}
    txt_files = glob.glob(f"{dir}\\*.txt")
    mwcs_shifts = {}
    for txt in txt_files:
        date = datetime.strptime(os.path.basename(txt), "%Y-%m-%d %H-%M-%S.txt")
        mwcs_shifts[date] = read_mwcs(txt)
    error, coherence, delay, lags = [], [], [], []
    for i_row, lag in enumerate(mwcs_shifts[datetime(2021, 7, 1, 0, 0, 0)].keys()):
        if abs(lag) > 19:
            coherence.append(
                np.array([v[lag]["coherence"] for v in mwcs_shifts.values()])
            )
            error.append(np.array([v[lag]["error"] for v in mwcs_shifts.values()]))
            delay.append(np.array([v[lag]["delay"] for v in mwcs_shifts.values()]))
            lags.append(lag * np.ones(len(coherence[-1])))
            dates = np.array([k for k in mwcs_shifts.keys()])
    error = np.ndarray.flatten(np.array(error))
    coherence = np.ndarray.flatten(np.array(coherence))
    i_count = 0
    for err, coh in zip(error, coherence):
        if err > max_err and coh < min_coh:
            i_count += 1
    n_measurements = len(error)
    lags = np.ndarray.flatten(np.array(lags))
    i_sort = shuffle(list(range(len(error))))
    reject[days] = i_count
    n_reject.append(reject)
    print(dir)
    #     ax[i_days].set_xlabel("error (s)")
    #     ax[i_days].set_title(f"{days} days")
    #     ax[i_days].set_facecolor("0.9")
    #     ax[i_days].text(0.9, 0.1, f"{i_count}/{n_measurements} rejected", ha="right", transform=ax[i_days].transAxes)
    # ax[0].set_ylabel("coherence")
    # cb = fig.colorbar(sc, ax=)
    # cb.set_label("lag (s)")
    # fig.savefig(f"figures{os.sep}dtt_metrics_{dir.split(os.sep)[-1]}.png")
    # plt.close(fig)

    # fig, ax = plt.subplots()
    # ax.set_facecolor("0.9")
    # ax.plot(distance, [v["001"] for v in n_reject], "o", color="forestgreen", markeredgecolor="0.1")
    # ax.plot(distance, [v["005"] for v in n_reject], "o", color="firebrick", markeredgecolor="0.1")
    # ax.plot(distance, [v["010"] for v in n_reject], "o", color="royalblue", markeredgecolor="0.1")
    #
    #
    # fig, ax = plt.subplots()
    # ax.set_facecolor("0.9")
    # for date in mwcs_shifts.keys():
    #     ax.plot([k for k in mwcs_shifts[date].keys()][100:130], [v["delay"] for v in mwcs_shifts[date].values()][100:130])

    station_pair = dir.split(os.sep)[-1]
    station_pair_title = station_pair.replace("_", "").replace("BP", "")
    coherence_2D = np.zeros([len(mwcs_shifts), len(mwcs_shifts[date])])
    error_2D = np.zeros([len(mwcs_shifts), len(mwcs_shifts[date])])

    for i_date, (date, mwcs) in enumerate(mwcs_shifts.items()):
        coherence_2D[i_date, :] = [v["coherence"] for v in mwcs.values()]
        error_2D[i_date, :] = [v["error"] for v in mwcs.values()]

    # np.arange(-115, 116)
    # fig, ax = plt.subplots(figsize=[8, 12])
    # cmap = ax.pcolor(range(-115, 116), mwcs_shifts.keys(), coherence_2D, vmax=1, vmin=min_coh)
    # ax.set_xlabel("lag (s)")
    # ax.set_title(station_pair_title)
    # cb = fig.colorbar(cmap)
    # cb.set_label("coherence")
    # fig.savefig(f"figures//coherence_{station_pair}.png", bbox_inches="tight")
    #
    # fig, ax = plt.subplots(figsize=[8, 12])
    # cmap = ax.pcolor(range(-115, 116), mwcs_shifts.keys(), error_2D, vmax=max_err, vmin=0, cmap="inferno_r")
    # ax.set_xlabel("lag (s)")
    # ax.set_title(station_pair_title)
    # cb = fig.colorbar(cmap)
    # cb.set_label("error (s)")
    # fig.savefig(f"figures//error_{station_pair}.png", bbox_inches="tight")
    # plt.close("all")
    #

    coda_coherence_2D = np.array(
        [list(ar)[:95] + list(ar)[-95:] for ar in coherence_2D]
    )

    fig_hist, ax_hist = plt.subplots()
    ax_hist.hist(np.ndarray.flatten(coherence_2D), bins=np.arange(0, 1.001, 0.01))
    ax_hist.hist(np.ndarray.flatten(coda_coherence_2D), bins=np.arange(0, 1.001, 0.01))

fig_hist.savefig(f"figures//{station_pair}_coherence_histogram.png")
