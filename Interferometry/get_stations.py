import numpy as np
from obspy import Stream, UTCDateTime, read_inventory
from obspy.clients.fdsn import Client
import os
import pyproj as pr

# from NocMeta.Meta import NOC_META
from read_inputs import read_stations

# client = Client("NCEDC")
# t_start = UTCDateTime("2004-07-01 00:00:00")
# t_end = UTCDateTime("2005-01-01 00:00:00")
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init="epsg:3401")


f = open("stations_DSA_unlocated_7.csv")
head = f.readline()
lines = f.readlines()
f.close()

g = open("stations_DSA_selected_.csv", "w")
g.write(head)

# inv = client.get_stations(channel="DP3", network="BP")
# inv.select(station=station, channel="DP3")[0][0]
inv = read_inventory("Station_xml//DSA_Full.xml")


for line in lines:
    lspl = line.split(",")
    station = lspl[2]
    inv_station = inv.select(station=station)
    latitude = inv_station[0][0].latitude
    longitude = inv_station[0][0].longitude
    elevation = inv_station[0][0].elevation
    east, north = pr.transform(latlon_proj, out_proj, longitude, latitude)
    g.write(",".join([*lspl[:3], f"{east:.1f},{north:.1f},{elevation:.1f}", *lspl[6:]]))
g.close()

latitude = 54.2904
longitude = -116.5843

east, north = pr.transform(latlon_proj, out_proj, longitude, latitude)
