import glob
from obspy import read
import os

seeds_23 = glob.glob(f"*{os.sep}*{os.sep}*{os.sep}*T23*")
for seed in seeds_23:
    year, month, day = seed.split(".DP3.")[1].split("T23")[0].split("-")
    destination = os.sep.join([year, month, day, os.path.basename(seed)])
    os.rename(seed, destination)
    # os.rename()


seeds = glob.glob(f"*{os.sep}*{os.sep}*{os.sep}*.mseed")
for seed in seeds:
    os.rename(seed, seed.replace(".DP3.", ".DPZ."))
