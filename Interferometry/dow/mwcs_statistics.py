from datetime import datetime, timedelta
import glob
import os
from random import shuffle

from matplotlib.collections import LineCollection
from matplotlib.dates import DateFormatter, date2num, MonthLocator, DayLocator
import matplotlib.pyplot as plt
import numpy as np

from read_inputs import read_mwcs, read_stations

min_coh, max_err = 0.65, 0.1
date_format = DateFormatter("%b %y")
stations = read_stations(r"dow/stations.csv")


def distance_from_pair(pair):
    first_pair = "_".join(pair.split("_")[:2])
    second_pair = "_".join(pair.split("_")[2:])
    return np.sqrt(
        sum(
            [
                (stations[first_pair][c] - stations[second_pair][c]) ** 2
                for c in ["e", "n", "z"]
            ]
        )
    )


def txt_file_to_datetime(txt_file_path):
    return datetime.strptime(os.path.basename(txt_file_path), "%Y-%m-%d %H-%M-%S.txt")


def n_hours_since_starttime(datetime_object, s_time):
    return int((datetime_object - starttime).total_seconds() / 3600) + 1


n_reject, distance = [], []
n_reject, distance = [], []

dir = glob.glob(rf"dow\MWCS\01\008_HOURS\ZZ\DC*")[0]
for dir in glob.glob(rf"dow\MWCS\01\008_HOURS\ZZ\DC*"):
    distance.append(distance_from_pair(dir.split(os.sep)[-1]))
    reject = {}
    txt_files = np.sort(glob.glob(f"{dir}\\*.txt"))
    starttime = txt_file_to_datetime(txt_files[0])
    endtime = txt_file_to_datetime(txt_files[-1])
    mwcs_shifts = {}
    for txt in txt_files:
        date = txt_file_to_datetime(txt)
        mwcs_shifts[date] = read_mwcs(txt)
    error, coherence, delay, lags = [], [], [], []
    for i_row, lag in enumerate(list(mwcs_shifts.values())[0].keys()):
        if abs(lag) > 19:
            coherence.append(
                np.array([v[lag]["coherence"] for v in mwcs_shifts.values()])
            )
            error.append(np.array([v[lag]["error"] for v in mwcs_shifts.values()]))
            delay.append(np.array([v[lag]["delay"] for v in mwcs_shifts.values()]))
            lags.append(lag * np.ones(len(coherence[-1])))
            dates = np.array([k for k in mwcs_shifts.keys()])
    error = np.ndarray.flatten(np.array(error))
    coherence = np.ndarray.flatten(np.array(coherence))
    i_count = 0
    for err, coh in zip(error, coherence):
        if err > max_err and coh < min_coh:
            i_count += 1
    n_measurements = len(error)
    lags = np.ndarray.flatten(np.array(lags))
    i_sort = shuffle(list(range(len(error))))
    # sc = ax[i_days].scatter(
    #     error[i_sort], coherence[i_sort], c=abs(lags[i_sort]), vmin=20, vmax=120, cmap="inferno", marker="."
    # )
    reject = i_count
    # n_reject.append(reject)
    # print(dir)
    # #     ax[i_days].set_xlabel("error (s)")
    # #     ax[i_days].set_title(f"{days} days")
    # #     ax[i_days].set_facecolor("0.9")
    # #     ax[i_days].text(0.9, 0.1, f"{i_count}/{n_measurements} rejected", ha="right", transform=ax[i_days].transAxes)
    # # ax[0].set_ylabel("coherence")
    # # cb = fig.colorbar(sc, ax=)
    # # cb.set_label("lag (s)")
    # # fig.savefig(f"figures{os.sep}dtt_metrics_{dir.split(os.sep)[-1]}.png")
    # # plt.close(fig)
    #
    # # fig, ax = plt.subplots()
    # # ax.set_facecolor("0.9")
    # # ax.plot(distance, [v["001"] for v in n_reject], "o", color="forestgreen", markeredgecolor="0.1")
    # # ax.plot(distance, [v["005"] for v in n_reject], "o", color="firebrick", markeredgecolor="0.1")
    # # ax.plot(distance, [v["010"] for v in n_reject], "o", color="royalblue", markeredgecolor="0.1")
    # #
    # #
    # # fig, ax = plt.subplots()
    # # ax.set_facecolor("0.9")
    # # for date in mwcs_shifts.keys():
    # #     ax.plot([k for k in mwcs_shifts[date].keys()][100:130], [v["delay"] for v in mwcs_shifts[date].values()][100:130])

    station_pair = dir.split(os.sep)[-1]
    station_pair_title = station_pair.replace("_", "").replace("BP", "")
    n_hours = n_hours_since_starttime(endtime, starttime) + 1
    coherence_2D = np.zeros([n_hours, len(mwcs_shifts[date])])
    error_2D = np.zeros([n_hours, len(mwcs_shifts[date])])
    delay_2D = np.zeros([n_hours, len(mwcs_shifts[date])])
    for date, mwcs in mwcs_shifts.items():
        i_hour = n_hours_since_starttime(date, starttime)
        coherence_2D[i_hour, :] = [v["coherence"] for v in mwcs.values()]
        error_2D[i_hour, :] = [v["error"] for v in mwcs.values()]
        delay_2D[i_hour, :] = [v["delay"] for v in mwcs.values()]

    masked_delay = np.ma.masked_outside(delay_2D, -0.1, 0.1)
    time_axis = [starttime + timedelta(seconds=i * 3600) for i in range(n_hours)]
    fig, ax = plt.subplots(figsize=[8, 12])
    cmap = ax.pcolor(
        range(-115, 116),
        time_axis,
        coherence_2D,
        vmax=1,
        vmin=min_coh,
        cmap="viridis",
    )
    ax.set_xlabel("lag (s)")
    ax.set_title(station_pair_title)
    cb = fig.colorbar(cmap)
    cb.set_label("coherence")
    fig.savefig(f"dow//figures//coherence_{station_pair}.png", bbox_inches="tight")

    fig, ax = plt.subplots(figsize=[8, 12])
    cmap = ax.pcolor(
        range(-115, 116),
        time_axis,
        error_2D,
        vmax=max_err,
        vmin=0,
        cmap="inferno_r",
    )
    ax.set_xlabel("lag (s)")
    ax.set_title(station_pair_title)
    cb = fig.colorbar(cmap)
    cb.set_label("error (s)")
    fig.savefig(f"dow//figures//error_{station_pair}.png", bbox_inches="tight")

    fig, ax = plt.subplots(figsize=[8, 12])
    ax.set_facecolor("0.8")
    cmap = ax.pcolor(
        range(-115, 116),
        list(mwcs_shifts.keys()),
        masked_delay,
        vmax=0.1,
        vmin=-0.1,
        cmap="seismic",
    )
    ax.set_xlabel("lag (s)")
    ax.set_title(station_pair_title)
    cb = fig.colorbar(cmap)
    cb.set_label("valid delay (s)")
    fig.savefig(f"dow//figures//delay_{station_pair}.png", bbox_inches="tight")
    plt.close("all")
