import os

import matplotlib.pyplot as plt
import numpy as np

from structural_monitoring.google_map_image_helper import (
    get_corners,
    image_pixel_xy_to_lat_lon,
)


width_height = 400, 500
zoom_level = 15
center_latlon = (-13.0149, -38.7618)

corners = get_corners(center_latlon, zoom_level, width_height)
station_latlons = {
    "SMA02": (-13.020022, -38.760444),
    "SMA01": (-13.013851, -38.757883),
    "SMA05": (-13.009224, -38.757644),
    "SMA04": (-13.010858, -38.764039),
    "SMA03": (-13.019597, -38.767276),
    "center": (-13.0149, -38.7618),
    **corners,
}
station_pixel_xy = [
    [72, 363],
    [233, 371],
    [149, 155],
    [291, 226],
    [298, 114],
    [200, 250],
    [0, 0],
    [0, 500],
    [400, 500],
    [400, 0],
]

img_path = r"C:\Users\adambaig\Project\Interferometry\dow\with_center.png"


os.path.isfile(img_path)
fig, ax = plt.subplots(figsize=[8, 10])
# for sta_id, latlon in station_latlons.items():

for pixel in station_pixel_xy:
    lat, lon = image_pixel_xy_to_lat_lon(pixel, center_latlon, zoom_level, width_height)
    ax.plot(lon, lat, "o", color="gold", markeredgecolor="k")


min_latitude = corners.get("SW")[0]
max_latitude = corners.get("NW")[0]
min_longitude = corners.get("SW")[1]
max_longitude = corners.get("NE")[1]
img = plt.imread(img_path)
ax.imshow(img, extent=[min_longitude, max_longitude, min_latitude, max_latitude])
ax.axis("off")
