import glob
import os

import matplotlib.pyplot as plt
import numpy as np
from obspy import read


stations = {}
with open(r"dow/stations.csv") as f:
    head = f.readline()
    lines = f.readlines()
    for line in lines:
        split_line = line.split(",")
        stations[".".join(split_line[1:3])] = {
            "east": float(split_line[3]),
            "north": float(split_line[4]),
            "elevation": float(split_line[5]),
            "enu": np.array([float(s) for s in split_line[3:6]]),
        }


def calc_distance(station1, station2):
    return np.linalg.norm(stations[station1]["enu"] - stations[station2]["enu"])


scale = 50

fig, ax = plt.subplots(figsize=[8, 12])
for pair_mseed in glob.glob(r"dow\STACKS\01\REF\ZZ\*MSEED"):
    pair_stream = read(pair_mseed)
    pair_data = pair_stream.copy().filter("bandpass", freqmin=2, freqmax=50)[0].data
    times = pair_stream[0].times()
    split_net_sta = os.path.basename(pair_mseed).split(".")[0].split("_")
    station1 = ".".join(split_net_sta[:2])
    station2 = ".".join(split_net_sta[2:])
    distance = calc_distance(station1, station2)
    ax.plot(times - 120, distance + scale * pair_data / abs(max(pair_data)))
    ax.text(-19, distance, f"{station1} to {station2}")
ax.set_xlim(-20, 20)
ax.set_xlabel("time (s)")
ax.set_ylabel("distance (m)")
ylim = ax.get_ylim()
ax.plot([0, 0], ylim, "0.5")
ax.set_ylim(*ylim)
fig.savefig("dow//ref_check.png")
