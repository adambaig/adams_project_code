import glob
from obspy import read
import os

seeds = glob.glob(f"*{os.sep}*{os.sep}*{os.sep}*.mseed")
for seed in seeds:
    st = read(seed)
    st[0].stats.channel = "DPZ"
    st.write(seed)
