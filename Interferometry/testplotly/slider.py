import plotly.graph_objects as go
import pandas as pd
import plotly.express as px
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Output, Input, State
import dash
import numpy as np


input = "/Users/bwitten/NMX/interferometry/testplotly/tomo_lite_results.csv"
stations = "/Users/bwitten/NMX/interferometry/testplotly/stations.csv"

df = pd.read_csv(input, index_col=False)
df = df.interpolate(method="linear", axis=0).ffill().bfill()
df_stations = pd.read_csv(stations, index_col=False)
df_stations = df_stations.drop_duplicates(subset=["station code"])
df_stations = df_stations.sort_values("station code")

stations = np.sort(list(df.columns)[1:])


station_markSize_standard = 1
station_size = []
station_color = []
colors = px.colors.qualitative.Dark24
ncolor = len(colors)
for jj, station in enumerate(stations):
    station_size.append(station_markSize_standard)
    station_color.append(colors[jj % ncolor])
df_stations["size"] = station_size
df_stations["color"] = station_color
station_code_update = []
for ii in df_stations["station code"]:
    station_code_update.append("CB_" + ii)
df_stations["station code"] = station_code_update

app = dash.Dash(__name__)

app.layout = html.Div(
    [
        dcc.Store(id="memory"),
        html.Div(
            [
                dcc.Graph(
                    id="stations-plot",
                    # hoverdata={'points':[{'customdata':stations[0]}]}
                ),
            ],
            style={"width": "20%", "display": "inline-block", "padding": "0 20"},
        ),
        html.Div(
            [
                dcc.Graph(id="velocity-graph"),
            ],
            style={"display": "inline-block", "width": "70%"},
        ),
    ]
)


@app.callback(
    [
        Output("stations-plot", "figure"),
        Output("velocity-graph", "figure"),
    ],
    [Input("velocity-graph", "clickData"), Input("stations-plot", "clickData")],
)
def update_graph(clickData_Vel, clickData_Station):
    ctx = dash.callback_context
    change = str(ctx.triggered[0]["prop_id"])
    vel_width_standard = 2
    vel_markSize_standard = 8
    station_markSize_standard = 2
    updateStation = ""

    stations = np.sort(list(df.columns)[1:])

    if change == "stations-plot.clickData":
        updateStation = ctx.triggered[0]["value"]["points"][0]["hovertext"]

    if change == "velocity-graph.clickData":
        updateStation = stations[ctx.triggered[0]["value"]["points"][0]["curveNumber"]]

    station_size = []
    for station in stations:
        if str(station) == updateStation:
            station_size.append(station_markSize_standard * 2)
        else:
            station_size.append(station_markSize_standard)
    df_stations["size"] = station_size

    fig = px.scatter(
        df_stations,
        x="latitude",
        y="longitude",
        hover_name="station code",
        color="station code",
        color_discrete_sequence=df_stations["color"].values.tolist(),
        size="size",
    )

    fig.update_layout(showlegend=False)
    fig1 = go.Figure()

    if updateStation != "":
        stations = np.append(
            stations[np.where(stations != updateStation)], updateStation
        )

    for jj, station in enumerate(stations):
        if str(station) == updateStation:
            vel_width = vel_width_standard * 2
            vel_markSize = vel_markSize_standard * 2
        else:
            vel_width = vel_width_standard
            vel_markSize = vel_markSize_standard

        fig1.add_trace(
            go.Scatter(
                x=df["date"],
                y=df[station],
                name=str(station),
                mode="lines+markers",
                line={
                    "width": vel_width,
                    "color": df_stations.loc[df_stations["station code"] == station][
                        "color"
                    ].values[0],
                },
                marker={"size": vel_markSize},
            )
        )

    fig1.update_traces(hoverinfo="name+x+y", showlegend=True)

    fig1.update_layout(
        title="Interferometry",
        xaxis_title="Date",
        yaxis_title="Velocity variation",
        legend_title="Station",
        height=1000,
        xaxis=dict(
            rangeselector=dict(
                buttons=list(
                    [
                        dict(count=1, label="1hr", step="hour", stepmode="backward"),
                        dict(count=3, label="3hr", step="hour", stepmode="backward"),
                        dict(count=1, label="1d", step="day", stepmode="backward"),
                        dict(count=10, label="10d", step="day", stepmode="backward"),
                        dict(step="all"),
                    ]
                )
            ),
            rangeslider=dict(visible=True),
            type="date",
        ),
    )

    return [fig, fig1]


if __name__ == "__main__":
    app.run_server(debug=True)
