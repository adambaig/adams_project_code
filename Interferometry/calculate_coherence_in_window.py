import glob
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap, ScalarMappable
from matplotlib.colors import Normalize
import numpy as np
from obspy import read
import os

from read_inputs import read_stations, read_mwcs

mwcs_dirs = glob.glob("MWCS\\01\\001_DAYS\\ZZ\\*")

coherence = {}
for dir in mwcs_dirs:
    pair = dir.split(os.sep)[-1]
    txt_files = glob.glob(f"{dir}{os.sep}" "*.txt")
    coherence[pair] = {}
    for txt in txt_files:
        date = txt.split(os.sep)[-1].split(".")[0]
        coherence[pair][date] = read_mwcs(txt)


lags = np.arange(-110, 111, 10.0)
coh = np.zeros([len(lags), 31])
for pair in coherence:
    for i_lag, lag in enumerate(lags):
        coh[i_lag, :] = np.array([v[lag]["error"] for v in coherence[pair].values()])
    fig, ax = plt.subplots()
    ax.pcolor(range(1, 32), lags, coh, vmin=0, vmax=0.03)
