from datetime import datetime
import functools
from glob import glob
import os
import sys
import numpy as np
from read_inputs import read_dtt


from logbook import Logger, StreamHandler

StreamHandler(sys.stdout).push_application()
log = Logger("tomography-lite-logger")


tt_files = glob("DTT_finer\\01\\ZZ\\*\\*.txt")
# tt_files = glob("test_tt_file.txt")
tts = {}
for tt_file in tt_files:
    f = open(tt_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    dt = datetime.strptime(lines[0].split(",")[0], "%Y-%m-%d-%H-%M-%S")
    tts[dt] = {}
    for line in lines:
        lspl = line.split(",")
        tts[dt][lspl[1]] = {
            "slope": float(lspl[2]),
            "slope error": float(lspl[3]),
            "y-int": float(lspl[4]),
            "y-int error": float(lspl[5]),
            "slope forced through origin": float(lspl[6]),
            "slope forced through origin error": float(lspl[7]),
        }

station_pairs = [dtt_file.keys() for dtt_file in tts.values()]
all_unique_station_pairs = np.unique(
    functools.reduce(lambda a, b: list(a) + list(b), station_pairs)
)


all_unique_stations = (
    "DS_DSA02,DS_DSA06,DS_DSA07,DS_DSA12,DS_DSA19,DS_DSA21,DS_DSA25".split(",")
)
filterid = 1
mov_stack = 8
components = "ZZ"

temporary_output_file = "tomo_lite_results_%02i_%03i_%s_temp.csv" % (
    filterid,
    mov_stack,
    components,
)
if os.path.isfile(temporary_output_file):
    log.error(f"Temporary output file {temporary_output_file} exists.   Halting")

dv = {}
out_string = f"date,{','.join(all_unique_stations)}\n"
for dt in sorted(tts.keys()):
    first_pair = ["_".join(k.split("_")[:2]) for k in tts[dt].keys() if k != "ALL"]
    second_pair = ["_".join(k.split("_")[2:]) for k in tts[dt].keys() if k != "ALL"]
    unique_stations_this_time_period = np.array(
        sorted(np.unique(np.hstack([first_pair, second_pair])))
    )
    n_sta = len(unique_stations_this_time_period)
    if n_sta > 0:
        A_matrix = np.zeros([len(first_pair) + 1, n_sta])
        dv_v = np.zeros(len(first_pair) + 1)
        dv[dt] = {}
        for i_row, (station_1, station_2) in enumerate(zip(first_pair, second_pair)):
            j_1 = np.where(unique_stations_this_time_period == station_1)[0]
            j_2 = np.where(unique_stations_this_time_period == station_2)[0]
            A_matrix[i_row, j_1] = 1
            A_matrix[i_row, j_2] = 1
            dv_v[i_row] = -tts[dt][f"{station_1}_{station_2}"][
                "slope forced through origin"
            ]

        if "ALL" in tts[dt]:
            A_matrix[-1, :] = np.ones(n_sta) / n_sta
            dv_v[-1] = -tts[dt]["ALL"]["slope forced through origin"]
            dv_indexed, residuals, rank, singular_values = np.linalg.lstsq(
                A_matrix, dv_v, rcond=None
            )
        else:
            log.warning(
                f'Did not find an "ALL" row in for {datetime.strftime(dt, "%Y-%m-%d %H:%M:%S")}.  Inverting without it.'
            )
            dv_indexed, residuals, rank, singular_values = np.linalg.lstsq(
                A_matrix[:-1, :], dv_v[:-1], rcond=None
            )

        if rank < len(unique_stations_this_time_period):
            log.error(f"ill-conditioned solution for {dt}")

        dv_all_unique_stations = np.zeros(len(all_unique_stations))
        j_station_this_time_period = 0
        out_string += datetime.strftime(dt, "%Y-%m-%d %H:%M:%S")
        for i_station, station in enumerate(all_unique_stations):
            if station in unique_stations_this_time_period:
                dv_all_unique_stations[i_station] = dv_indexed[
                    j_station_this_time_period
                ]
                out_string += f",{dv_indexed[j_station_this_time_period]:.6e}"
                j_station_this_time_period += 1
            else:
                dv_all_unique_stations[i_station] = None
                out_string += ","
        out_string += "\n"

print(out_string)

output_file = temporary_output_file.replace("_temp", "")
os.replace(temporary_output_file, output_file)
