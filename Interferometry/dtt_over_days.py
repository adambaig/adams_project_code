import glob

import matplotlib.pyplot as plt

from read_inputs import read_dtt

dir = r"Parkfield\DTT\02\001_DAYS\ZZ"

txt_files = glob.glob(f"{dir}\\*.txt")
dtt = read_dtt(txt_files[1])
