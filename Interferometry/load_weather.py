from datetime import datetime
import glob
import json
import os

import matplotlib.pyplot as plt
import numpy as np
import pytz
from read_inputs import read_dtt

UTC = pytz.utc


with open(r"dow\salvadorBahia2021-01-01to2022-04-19.json") as f:
    weather = json.load(f)

tt_files = glob.glob(os.path.join("dow", "DTT", "01", "008_HOURS", "ZZ", "*.txt"))

tt_all = {}
for file in tt_files:
    tt_data = read_dtt(file, hourly=True)
    time_index = datetime.strptime(os.path.basename(file), "%Y-%m-%d %H-%M-%S.txt")
    tt_all[UTC.localize(time_index)] = tt_data

read_dtt(file, hourly=True)

sought_key = "ALL"

dvv_all = {
    k: (-v[sought_key]["slope forced through origin"]) if sought_key in v else 0
    for k, v in tt_all.items()
}
temp_max = [v["tempmax"] for v in weather["days"]]
temp_min = [v["tempmin"] for v in weather["days"]]
pressure = [v["pressure"] for v in weather["days"]]
precip = [v["precip"] for v in weather["days"]]


smooth_dvv = (np.convolve(list(dvv_all.values()), np.ones(24), mode="same") / 24)[::24]

timezone = pytz.timezone(weather["timezone"])

fig, ax = plt.subplots(3, figsize=[12, 8], sharex=True)

days = [
    timezone.localize(datetime.strptime(day["datetime"], "%Y-%m-%d"))
    for day in weather["days"]
]


ax[0].plot(days, temp_max, "firebrick")
ax[0].plot(days, temp_min, "steelblue")
ax[1].plot(days, pressure, "darkblue")
ax[2].plot(
    np.array((dvv_day := list(dvv_all.keys())))[::24], smooth_dvv, color="darkgreen"
)
# ax[2].plot(dvv_all.keys(), dvv_all.values(), color='lightgreen', zorder=-2)
ax[2].set_xlim(dvv_day[0], dvv_day[-1])


ax[0].set_ylabel("Temperature range (C)")
ax[1].set_ylabel("Pressure (kPa)")
ax[2].set_ylabel("smooth dv/v (all)")
ax[0].set_title("Weather at Salvador Brazil compared to Interferometric Changes")
fig.savefig("weather_comparison.png")
