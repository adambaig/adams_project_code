from datetime import datetime, timedelta

from plotting import plot_snippet

base_date = datetime(2004, 9, 2)

dates = [base_date + timedelta(days=x) for x in range(10)]


station_pair = "BP_JCNB_BP_SMNB"
filter = "02"

for date in dates:
    date_str = datetime.strftime(date, "%Y-%m-%d")
    fig = plot_snippet(station_pair, date, -20, 10, filter="02")
    fig.savefig(f"figures//{station_pair}_{date_str}.png")
