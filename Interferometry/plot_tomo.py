from datetime import datetime
from ipywidgets import interact
import ipywidgets as widgets
import matplotlib.pyplot as plt
from obspy import read_inventory
import pyproj as pr

from NocMeta.Meta import NOC_META
from generalPlots import gray_background_with_grid

athena_code = "CBMM"
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META[athena_code]['epsg']}")
inv = read_inventory("Station_xml//CBMM_Full.xml")

f = open("tomo_lite_results.csv")
head = f.readline()
lines = f.readlines()
f.close()

dv = {}
station_codes = [h.strip() for h in head.split(",")[1:]]
for line in lines:
    lspl = line.split(",")
    date = lspl[0]
    dv[date] = {}
    for i_station, entry in enumerate(lspl[1:]):
        if entry.strip() != "":
            dv[date][station_codes[i_station]] = float(entry)


stations = {}
for network in inv:
    for station in network:
        east, north = pr.transform(
            latlon_proj, out_proj, station.longitude, station.latitude
        )
        stations[network.code + "_" + station.code] = {
            "e": east,
            "n": north,
            "z": station.elevation,
        }


xmax = max([v["e"] for v in stations.values()])
xmin = min([v["e"] for v in stations.values()])
ymax = max([v["n"] for v in stations.values()])
ymin = min([v["n"] for v in stations.values()])

xrange = xmax - xmin
yrange = ymax - ymin


scale = 400

for date in dv.keys():
    fig, ax = plt.subplots(figsize=[10, 10])
    ax.set_aspect("equal")
    scatter = ax.scatter(
        [stations[k]["e"] for k in dv[date].keys()],
        [stations[k]["n"] for k in dv[date].keys()],
        c=list(dv[date].values()),
        vmax=3e-3,
        vmin=-3e-3,
        cmap="seismic_r",
        edgecolor="0.1",
        s=scale,
    )
    ax.set_xlim([xmin - 0.05 * xrange, xmax + 0.05 * xrange])
    ax.set_ylim([ymin - 0.05 * yrange, ymax + 0.05 * yrange])
    dt = datetime.strptime(date, "%Y-%m-%d")
    title = datetime.strftime(dt, "%b %d, %Y")
    ax.set_title(title)
    ax = gray_background_with_grid(ax)
    cb = fig.colorbar(scatter)
    cb.set_label("relative velocity variation from ZZ")
    fig.savefig(
        datetime.strftime(dt, "tomo_lite_figures//ZZ_%d%m%y.png"), bbox_inches="tight"
    )
    plt.close(fig)
