from datetime import datetime
import glob
import os

from matplotlib.collections import LineCollection
from matplotlib.dates import DateFormatter, date2num, MonthLocator, DayLocator
import matplotlib.pyplot as plt
import numpy as np

from read_inputs import read_mwcs

date_format = DateFormatter("%b %y")
fig, ax = plt.subplots(ncols=3, nrows=7, figsize=[16, 12], sharex=True)
for i_days, days in enumerate(["001", "005", "010"]):
    dir = rf"MWCS\02\{days}_DAYS\ZZ\BP_GHIB_BP_VCAB"
    txt_files = glob.glob(f"{dir}\\*.txt")
    mwcs_shifts = {}
    for txt in txt_files:
        date = datetime.strptime(os.path.basename(txt), "%Y-%m-%d.txt")
        mwcs_shifts[date] = read_mwcs(txt)
    for i_row, lag in enumerate(range(-30, 31, 10)):
        coherence = np.array([v[lag]["coherence"] for v in mwcs_shifts.values()])
        error = np.array([v[lag]["error"] for v in mwcs_shifts.values()])
        delay = np.array([v[lag]["delay"] for v in mwcs_shifts.values()])
        dates = np.array([k for k in mwcs_shifts.keys()])
        norm = plt.Normalize(coherence.min(), coherence.max())
        points = np.array([date2num(dates), delay]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        lc = LineCollection(segments, cmap="viridis", norm=norm)
        lc.set_array(coherence)
        line = ax[i_row, i_days].add_collection(lc)
        ax[i_row, i_days].set_xlim([date2num(dates[0]), date2num(dates[-1])])
        ax[i_row, i_days].set_ylim([delay.min(), delay.max()])
        ax[i_row, i_days].xaxis.set_major_formatter(date_format)
        ax[i_row, i_days].set_facecolor("0.9")

ax[0, 0].set_title("001 days")
ax[0, 1].set_title("005 days")
ax[0, 2].set_title("010 days")
ax[0, 0].set_ylabel("lag +30")
ax[1, 0].set_ylabel("lag +20")
ax[2, 0].set_ylabel("lag +10")
ax[3, 0].set_ylabel("zero lag")
ax[4, 0].set_ylabel("lag $-$10")
ax[5, 0].set_ylabel("lag $-$20")
ax[6, 0].set_ylabel("lag $-$30")
