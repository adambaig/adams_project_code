import numpy as np

from obspy import Stream, UTCDateTime
from obspy.clients.fdsn import Client
import os

client = Client("NCEDC")
t_start = UTCDateTime("2004-07-01 00:00:00")
t_end = UTCDateTime("2005-01-01 00:00:00")


client.get_stations(channel="DP3", network="BP")


def createDir(aDir):
    if not os.path.isdir(aDir):
        os.makedirs(aDir)


def outname(tr):
    st = tr.stats
    return f"{st.network}.{st.station}.{st.location}.{st.channel}.{st.starttime}.mseed".replace(
        ":", "-"
    )


for i_h in np.arange((t_end - t_start) / 3600.0 - 1):
    t1 = t_start + 3600 * i_h
    t2 = t_start + 3600 * (i_h + 1)
    dir = t1.strftime(f"%Y{os.sep}%m{os.sep}%d{os.sep}")
    createDir(dir)
    st = client.get_waveforms("BP", "***B", "**", "DP3", t1, t2)
    for tr in st:
        st_out = Stream()
        st_out += tr
        st_out.write(f"{dir}{outname(tr)}")
