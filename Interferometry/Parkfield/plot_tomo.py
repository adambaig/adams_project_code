from datetime import datetime
from ipywidgets import interact
import ipywidgets as widgets
import matplotlib.pyplot as plt
from matplotlib import cm
from obspy import read_inventory
import pyproj as pr

from generalPlots import gray_background_with_grid
from read_inputs import read_stations, read_dtt

stations = read_stations("Parkfield//stations_parkfield.csv")
seismic_r = cm.get_cmap("seismic_r")
f = open("Parkfield//tomo_lite_results.csv")
head = f.readline()
lines = f.readlines()
f.close()

dv = {}
station_codes = [h.strip() for h in head.split(",")[1:]]
for line in lines:
    lspl = line.split(",")
    date = lspl[0]
    dv[date] = {}
    for i_station, entry in enumerate(lspl[1:]):
        if entry.strip() != "":
            dv[date][station_codes[i_station]] = float(entry)


xmax = max([v["e"] for v in stations.values()])
xmin = min([v["e"] for v in stations.values()])
ymax = max([v["n"] for v in stations.values()])
ymin = min([v["n"] for v in stations.values()])

xrange = xmax - xmin
yrange = ymax - ymin
dtt_dir = "Parkfield//DTT//02//001_DAYS//ZZ//"

scale = 400
vmax = 1e-3
vmin = -vmax
for date in list(dv.keys()):
    dtt = read_dtt(f"{dtt_dir}{date}.txt")
    fig, ax = plt.subplots(figsize=[10, 10])
    ax.set_aspect("equal")

    for pair, result in dtt.items():
        if pair != "ALL":
            station1 = "_".join(pair.split("_")[:2])
            station2 = "_".join(pair.split("_")[2:])
            ax.plot(
                [stations[s]["e"] for s in [station1, station2]],
                [stations[s]["n"] for s in [station1, station2]],
                color=seismic_r(
                    (-result["slope forced through origin"] + vmax) / 2 / vmax
                ),
            )
    scatter = ax.scatter(
        [stations[k]["e"] for k in dv[date].keys()],
        [stations[k]["n"] for k in dv[date].keys()],
        c=list(dv[date].values()),
        vmax=vmax,
        vmin=vmin,
        cmap="seismic_r",
        edgecolor="0.1",
        s=scale,
        zorder=4,
    )

    ax.set_xlim([xmin - 0.05 * xrange, xmax + 0.05 * xrange])
    ax.set_ylim([ymin - 0.05 * yrange, ymax + 0.05 * yrange])
    dt = datetime.strptime(date, "%Y-%m-%d")
    title = datetime.strftime(dt, "%b %d, %Y")
    ax.set_title(title)
    ax = gray_background_with_grid(ax, grid_spacing=2000)
    cb = fig.colorbar(scatter)
    cb.set_label("relative velocity variation from ZZ")
    fig.savefig(
        datetime.strftime(dt, "Parkfield//tomo_plots//ZZ_%Y%m%d.png"),
        bbox_inches="tight",
    )
    plt.close(fig)
