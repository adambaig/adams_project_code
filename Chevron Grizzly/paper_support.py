import matplotlib

matplotlib.use("Qt5agg")
import matplotlib.pyplot as plt
import numpy as np
from obspy.signal import filter
from obspy.signal.invsim import corn_freq_2_paz
from obspy.core.trace import Stats
from obspy import Stream, Trace, UTCDateTime
from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.raytrace import isotropic_ray_trace

from read_inputs import get_velocity_model

source = {
    "e": 0,
    "n": 0,
    "z": -2700.0,
    "moment_tensor": np.eye(3),
    "moment_magnitude": 1,
}

paz_10Hz = corn_freq_2_paz(10.0, damp=np.sqrt(0.5))
receiver = {
    "e": 500,
    "n": 200,
    "z": 900,
}


def createTraceStats(network, n1, o1, d1, component):
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


def create_obspy_trace(waveform, component):
    channel_obj = {
        "component": component.upper(),
        "channel": "CPZ",
        "station": "dum",
        "network": "SY",
        "location": "00",
    }
    xStats = createTraceStats("SY", len(time_series), 0, dt, channel_obj)
    return Trace(data=waveform, header=xStats)


Q = {"P": 1000000, "S": 1000000}
dt = 1e-4
time_series = np.arange(0, 1.1, dt)
velocity_model = get_velocity_model()
p_raypath = isotropic_ray_trace(source, receiver, velocity_model, "P")
s_raypath = isotropic_ray_trace(source, receiver, velocity_model, "S")
fig, ax = plt.subplots(2, 1, figsize=[10, 4], sharex=True)
for stress_drop in [1e4, 1e5, 1e6, 1e7]:
    source["stress_drop"] = stress_drop
    waveform = get_response(p_raypath, s_raypath, source, receiver, Q, time_series, 0)

    st = Stream()
    tr = create_obspy_trace(waveform["z"], "Z")
    st += tr
    st.simulate(paz_simulate=paz_10Hz)
    ax[0].plot(time_series, 10e6 * st[0].data)

ax[0].set_xlim([0.9, 1.1])
ax[0].legend(["10 kPa", "0.1 MPa", "1 MPa", "10 Mpa"])


Q = {"P": 70, "S": 50}
for stress_drop in [1e4, 1e5, 1e6, 1e7]:
    source["stress_drop"] = stress_drop
    waveform = get_response(p_raypath, s_raypath, source, receiver, Q, time_series, 0)
    st = Stream()
    tr = create_obspy_trace(waveform["z"], "Z")
    st += tr
    st.simulate(paz_simulate=paz_10Hz)
    ax[1].plot(time_series, 10e6 * st[0].data)


ax[1].set_xlim([0.9, 1.1])
ax[1].set_xlabel("time (s)")
ax[1].set_ylabel("amplitude ($\mu$m/s)")
ax[0].set_ylabel("amplitude ($\mu$m/s)")

plt.show()
