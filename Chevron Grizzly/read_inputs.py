# -*- coding: utf-8 -*-
"""
Created on Wed Jan 30 16:13:55 2019

@author: adambaig
"""

import numpy as np
import glob
from datetime import datetime
from obspy import UTCDateTime
from sms_moment_tensor.MT_math import mt_vector_to_matrix


def read_treatment():
    treatment_dir = (
        "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\"
        + "Chevron 02-04 Treatment Data\\"
    )
    treatment_data = {}
    for well in ["A", "B", "C", "D"]:
        files = glob.glob(treatment_dir + "Well " + well + "\\*.csv")
        for filename in files:
            f = open(filename)
            f.readline()
            f.readline()
            lines = f.readlines()
            f.close()
            stage = filename.split(".csv")[0].split("Interval")[1][1:]
            for line in lines:
                lspl = line.split(",")
                dt = datetime.strptime(lspl[0] + lspl[1], "%d-%b-%y%H:%M:%S")
                timestamp = datetime.strftime(dt, "%Y%m%d%H%M%S.000000")
                treatment_data[timestamp] = {}
                treatment_data[timestamp]["well"] = well
                treatment_data[timestamp]["stage"] = stage
                treatment_data[timestamp]["pressure"] = float(lspl[2])
                treatment_data[timestamp]["slurry_rate"] = float(lspl[8])
                treatment_data[timestamp]["proppant_conc"] = float(lspl[10])
    return treatment_data


filename = "flipped_frac_events.csv"


def read_events(filename="Chevron_withXC_utm_MTs.csv"):
    event_dir = "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\"
    event_files = glob.glob(event_dir + filename)
    events = {}
    filename = event_files[0]
    for filename in event_files:
        f = open(filename)
        head = f.readline()
        lines = f.readlines()
        f.close()
        line = lines[23]
        for line in lines:
            lspl = line.split(",")
            well = lspl[12]
            stagename = lspl[13].split("\n")[0]
            if stagename in ["121", "41", "42"]:
                stage = stagename[:-1] + "-Part" + stagename[-1]
            else:
                stage = stagename
            timestamp = UTCDateTime.strftime(UTCDateTime(lspl[1]), "%Y%m%d%H%M%S.%f")
            events[timestamp] = {}
            events[timestamp]["id"] = lspl[0]
            events[timestamp]["easting"] = float(lspl[2]) - 484550.0
            events[timestamp]["northing"] = float(lspl[3]) - 6020267.03
            events[timestamp]["depth"] = float(lspl[4]) - 910.0
            events[timestamp]["magnitude"] = float(lspl[5])
            events[timestamp]["stack_amp"] = float(lspl[6])
            events[timestamp]["filt_amp"] = float(lspl[7])
            events[timestamp]["xc_value"] = float(lspl[8])
            events[timestamp]["master"] = int(lspl[9])
            events[timestamp]["isShallow"] = lspl[10]
            events[timestamp]["cluster"] = int(lspl[11])
            events[timestamp]["well"] = well
            events[timestamp]["stage"] = stage
            if lspl[14] != "":
                mt_vector = [float(s) for s in lspl[15:21]]
                dc_vector = [float(s) for s in lspl[21:27]]
                if filename == "flipped_frac_events.csv":
                    events[timestamp]["general MT"] = -mt_vector_to_matrix(mt_vector)
                    events[timestamp]["dc MT"] = -mt_vector_to_matrix(dc_vector)
                else:
                    events[timestamp]["general MT"] = mt_vector_to_matrix(mt_vector)
                    events[timestamp]["dc MT"] = mt_vector_to_matrix(dc_vector)
                events[timestamp]["n picks"] = int(lspl[14])
                events[timestamp]["general condition number"] = float(lspl[27])
                events[timestamp]["dc condition number"] = float(lspl[28])
                events[timestamp]["general r"] = float(lspl[29])
                events[timestamp]["dc r"] = float(lspl[30])
                events[timestamp]["mt confidence"] = float(lspl[31])
    return events


def get_velocity_model():
    return [
        {"rho": 2180.0, "vp": 2446.0, "vs": 1069.0},
        {"rho": 2324.0, "vp": 3163.0, "vs": 1513.0, "top": 460.0},
        {"rho": 2421.0, "vp": 3726.0, "vs": 1862.0, "top": -20.0},
        {"rho": 2451.0, "vp": 3911.0, "vs": 2046.0, "top": -220.0},
        {"rho": 2495.0, "vp": 4201.0, "vs": 2301.0, "top": -1140.0},
        {"rho": 2734.0, "vp": 6052.0, "vs": 3303.0, "top": -1480.0},
        {"rho": 2686.0, "vp": 5637.0, "vs": 3027.0, "top": -1720.0},
        {"rho": 2749.0, "vp": 6186.0, "vs": 3275.0, "top": -1920.0},
        {"rho": 2494.0, "vp": 4190.0, "vs": 2288.0, "top": -2320.0},
        {"rho": 2614.0, "vp": 5063.0, "vs": 2687.0, "top": -2560.0},
    ]


def read_perfs():
    base_dir = (
        "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\" + "Chevron Image Magnitude\\"
    )
    f = open(base_dir + "Chevron0204_Stages.csv")
    lines = f.readlines()
    f.close()
    perfs = {"A": {}, "B": {}, "D": {}}
    for line in lines:
        lspl = line.split(",")
        well, stage = lspl[:2]
        if stage in ["121", "41", "42"]:
            stage = stage[:-1] + "-Part" + stage[-1]
        e, n, d = [float(s) for s in lspl[4:7]]
        perfs[well][stage] = {}
        perfs[well][stage] = {"north": n, "east": e, "depth": d}
    return perfs


def read_wells(local_coords=False):
    base_dir = "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\setup\\"
    wells = {}
    for well in ["A", "B", "C", "D"]:
        wells[well] = {}
        f = open(base_dir + "Well_" + well + ".csv")
        head = f.readline()
        unit = f.readline()
        lines = f.readlines()
        f.close()
        nwell = len(lines)
        easting, northing, tvdss, md = (
            np.zeros(nwell),
            np.zeros(nwell),
            np.zeros(nwell),
            np.zeros(nwell),
        )
        ii = -1
        for line in lines:
            ii += 1
            lspl = line.split(",")
            md[ii] = float(lspl[1])
            easting[ii], northing[ii] = [float(s) for s in lspl[9:11]]
            tvdss[ii] = float(lspl[5])
        if local_coords:
            easting = easting - 484550.0
            northing = northing - 6020267.03
        f = open(base_dir + "Perfs_" + well + ".csv")
        head = f.readline()
        lines = f.readlines()
        f.close()
        ii = -1
        for line in lines:
            ii += 1
            lspl = line.split(",")
            stage = "Stage " + str(int(round(float(lspl[0]))))
            if not (stage in wells[well].keys()):
                wells[well][stage] = {}
                jj = 0
            jj += 1
            top, bottom = [float(s) for s in lspl[1:3]]
            if well == "C":
                cluster = "Port " + str(jj)
            else:
                cluster = "Perf " + str(jj)
            wells[well][stage][cluster] = {}
            wells[well][stage][cluster]["top_east"] = np.interp(top, md, easting)
            wells[well][stage][cluster]["top_north"] = np.interp(top, md, northing)
            wells[well][stage][cluster]["top_tvdss"] = np.interp(top, md, tvdss)
            wells[well][stage][cluster]["bottom_east"] = np.interp(bottom, md, easting)
            wells[well][stage][cluster]["bottom_north"] = np.interp(
                bottom, md, northing
            )
            wells[well][stage][cluster]["bottom_tvdss"] = np.interp(bottom, md, tvdss)
        wells[well]["easting"] = easting
        wells[well]["northing"] = northing
        wells[well]["tvdss"] = tvdss
    return wells
