# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 10:34:27 2019

@author: adambaig
"""

import numpy as np
import matplotlib.pyplot as plt
from read_inputs import read_treatment, read_events, read_wells
from datetime import datetime
from obspy import UTCDateTime

if not(globals().has_key('treatment_data')):  
    # this take a long time, so only read in if it's not a global
    treatment_data = read_treatment()
events = read_events()
event_wells = events.keys()
wells = read_wells()
for well in event_wells:
    for stage in events[well].keys():
        # shift time to relative accounting for mountain time to utc (7 hours)
        t0 = treatment_data[well][stage]['serial time'][0]
        tlast = treatment_data[well][stage]['serial time'][-1] - t0
        if (well=='B' and stage=='10'):
            tlast = UTCDateTime(2016,11,22,17,00,000001).timestamp - t0
        if stage in ['12-Part1','4-Part1','4-Part2']:
            stage_name = 'Stage '+stage.split('-')[0]
        else: 
            stage_name = 'Stage '+stage
        north0 = 0.5*(np.average([v['bottom_north'] for (k,v) in wells[well][stage_name].items() ])
                + np.average([v['top_north'] for (k,v) in wells[well][stage_name].items() ]))
        east0 = 0.5*(np.average([v['bottom_east'] for (k,v) in wells['A'][stage_name].items() ])
                + np.average([v['top_east'] for (k,v) in wells[well][stage_name].items() ]))
        tvdss0 = 0.5*(np.average([v['bottom_tvdss'] for (k,v) in wells[well][stage_name].items() ])
                + np.average([v['top_tvdss'] for (k,v) in wells[well][stage_name].items() ]))
        fig = plt.figure()
        fig.tight_layout()
        a1 = fig.add_axes([0.2,0.52,0.6,0.33])
        a1.set_ylabel('northing relative to\ncenter of perfs (m)')
        a1.set_title('Well '+well+': Stage '+stage)
        isort = np.argsort(events[well][stage]['magnitude']) # plot high mag events on top
        scatterplot = a1.scatter(events[well][stage]['utc_time'][isort]-t0 - 7*3600,
                    events[well][stage]['y'][isort]-north0,
                    c=events[well][stage]['magnitude'][isort],
                    vmax = 0,vmin=-1,cmap = 'cubehelix',edgecolor='0.2',zorder=10)
        xticks = np.arange(3600 -t0%3600,tlast,3600)
        a1.set_xticks(xticks)
        for cluster in wells[well][stage_name].keys():
            n_top = wells[well][stage_name][cluster]['top_north']-north0
            b_top = wells[well][stage_name][cluster]['bottom_north']-north0
            a1.plot([0,t0],[n_top,n_top],'0.7',zorder=-1)
            a1.plot([0,t0],[b_top,b_top],'0.7',zorder=-1)    
        a1.set_xlim([0,tlast])
        a1.set_xticklabels([])
        a2 = fig.add_axes([0.2,0.15,0.6,0.33])
        a2.plot(treatment_data[well][stage]['serial time'] - t0,
                treatment_data[well][stage]['pressure']/120.,'forestgreen')
        a2.set_ylabel('pressure (MPa)', color='forestgreen')
        a2.set_ylim([0,1])
        a2.set_xlim([0,tlast])
        a2.set_xticks(xticks)
        a2.hist(events[well][stage]['utc_time'][isort]-t0 - 7*3600,
                bins=np.arange(0,tlast,300), zorder = -4,
                weights=np.ones(len(events[well][stage]['x']))/20)
        xticklabels = []
        for tick in xticks:
            xticklabels.append(datetime.strftime(UTCDateTime(
                    t0+tick)._get_datetime(), '%b %d\n%H%M'))
        a2.set_xticklabels(xticklabels)
        a2.set_yticks(np.arange(0,7.)/6)
        a2.set_xlabel('Mountain Standard Time')
        a2.set_yticklabels(['0','20','40','60','80','100','120'])
        a3 = a2.twinx()
        a3.plot(treatment_data[well][stage]['serial time'] - t0,
                treatment_data[well][stage]['slurry_rate']/15.,'firebrick')
        a3.set_ylabel('slurry rate (m${^3}$/min)', color='firebrick')
        a3.set_ylim([0,1])
        a3.set_yticks(np.arange(0,7.)/6)
        a3.set_yticklabels(['0','25','50','75','100','125','150'])        
        a2.plot(treatment_data[well][stage]['serial time'] - t0,
                treatment_data[well][stage]['proppant_conc']/500., 'darkgoldenrod')
        a4 = fig.add_axes([0.9,0.15,0,0.33])
        a4.yaxis.set_ticks_position('right')
        a4.yaxis.set_label_position('right')
        a4.set_yticks(np.arange(0,6.)/5)
        a4.set_yticklabels(['0','100','200','300','400','500'])
        a4.set_ylabel('proppant conc. (kg/m$^{3}$)', color='darkgoldenrod')
        a4.set_xticks([])

        a5 = fig.add_axes([0.85,0.52,0.03,0.33])
        cb = fig.colorbar(scatterplot,cax=a5,extend='both')
        cb.set_label('magnitude')
        a7 = fig.add_axes([0.1,0.15,0,0.33])
        a7.set_ylabel('event rate', color='royalblue')
        a7.set_xticks([])
        a7.set_yticks(np.arange(5.)/4.)
        a7.set_yticklabels(['0','5','10','15','20'])
        fig.savefig('RT_plots//along_well_'+well+'_stage_'+stage+'.png')

        plt.close('all')