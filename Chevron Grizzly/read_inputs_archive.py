# -*- coding: utf-8 -*-
"""
Created on Wed Jan 30 16:13:55 2019

@author: adambaig
"""

import numpy as np
import glob
from datetime import datetime
from obspy import UTCDateTime


def read_treatment():
    treatment_dir = (
        "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\"
        + "Chevron 02-04 Treatment Data\\"
    )
    treatment_data = {}
    for well in ["A", "B", "C", "D"]:
        files = glob.glob(treatment_dir + "Well " + well + "\\*.csv")
        treatment_data[well] = {}
        for filename in files:
            f = open(filename)
            head = f.readline()
            units = f.readline()
            lines = f.readlines()
            f.close()
            stage = filename.split(".csv")[0].split("Interval")[1][1:]
            treatment_data[well][stage] = {}
            serial_time, pressure, proppant_conc, slurry_rate = [], [], [], []
            for line in lines:
                lspl = line.split(",")
                dt_mtz = datetime.strptime(lspl[0] + lspl[1], "%d-%b-%y%H:%M:%S")
                pressure.append(float(lspl[2]))
                slurry_rate.append(float(lspl[8]))
                proppant_conc.append(float(lspl[10]))
                serial_time.append(UTCDateTime(dt_mtz).timestamp)  # adjust to UTC
            treatment_data[well][stage]["serial time"] = np.array(serial_time)
            treatment_data[well][stage]["pressure"] = np.array(pressure)
            treatment_data[well][stage]["slurry_rate"] = np.array(slurry_rate)
            treatment_data[well][stage]["proppant_conc"] = np.array(proppant_conc)
    return treatment_data


def read_events():
    event_dir = "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\"
    event_files = glob.glob(event_dir + "Chevron_withXC_utm.csv")
    events = {}
    for filename in event_files:
        f = open(filename)
        head = f.readline()
        lines = f.readlines()
        f.close()
        for line in lines:
            lspl = line.split(",")
            well = lspl[12]
            stage = lspl[13].split("\n")[0]
            if stage in ["121", "41", "42"]:
                stage = stage[:-1] + "-Part" + stage[-1]
            if not (well in events.keys()):
                events[well] = {}
            if not (stage in events[well].keys()):
                if vars().has_key("old_stage"):
                    events[old_well][old_stage]["id"] = np.array(event_id)
                    events[old_well][old_stage]["utc_time"] = np.array(utc_time)
                    events[old_well][old_stage]["x"] = np.array(event_x)
                    events[old_well][old_stage]["y"] = np.array(event_y)
                    events[old_well][old_stage]["z"] = np.array(event_z)
                    events[old_well][old_stage]["magnitude"] = np.array(magnitude)
                    events[old_well][old_stage]["stack_amp"] = np.array(stack_amp)
                    events[old_well][old_stage]["filt_amp"] = np.array(filt_amp)
                    events[old_well][old_stage]["xc_value"] = np.array(xc_value)
                    events[old_well][old_stage]["master"] = np.array(master)
                    events[old_well][old_stage]["isShallow"] = np.array(isShallow)
                    events[old_well][old_stage]["cluster"] = np.array(cluster)
                    del old_stage
                events[well][stage] = {}
                event_id, utc_time, event_x, event_y = [], [], [], []
                event_z, magnitude, stack_amp, filt_amp = [], [], [], []
                xc_value, master, isShallow, cluster = [], [], [], []
            event_id.append(lspl[0])
            utc_time.append(
                UTCDateTime(
                    datetime.strptime(lspl[1] + "000", "%Y-%m-%d %H:%M:%S.%f")
                ).timestamp
            )
            event_x.append(float(lspl[2]))  # ref x = 484550, y = 6020267.03, z = 910
            event_y.append(float(lspl[3]))
            event_z.append(float(lspl[4]))
            magnitude.append(float(lspl[5]))
            stack_amp.append(float(lspl[6]))
            filt_amp.append(float(lspl[7]))
            xc_value.append(float(lspl[8]))
            master.append(int(lspl[9]))
            isShallow.append(lspl[10])
            cluster.append(int(lspl[11]))
            old_well = well
            old_stage = stage
    events[old_well][old_stage]["id"] = np.array(event_id)
    events[old_well][old_stage]["utc_time"] = np.array(utc_time)
    events[old_well][old_stage]["x"] = np.array(event_x)
    events[old_well][old_stage]["y"] = np.array(event_y)
    events[old_well][old_stage]["z"] = np.array(event_z)
    events[old_well][old_stage]["magnitude"] = np.array(magnitude)
    events[old_well][old_stage]["stack_amp"] = np.array(stack_amp)
    events[old_well][old_stage]["filt_amp"] = np.array(filt_amp)
    events[old_well][old_stage]["xc_value"] = np.array(xc_value)
    events[old_well][old_stage]["master"] = np.array(master)
    events[old_well][old_stage]["isShallow"] = np.array(isShallow)
    events[old_well][old_stage]["cluster"] = np.array(cluster)

    return events


def read_perfs():
    base_dir = (
        "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\Chevron Image Magnitude\\"
    )
    f = open(base_dir + "Chevron0204_Stages.csv")
    lines = f.readlines()
    f.close()
    perfs = {"A": {}, "B": {}, "D": {}}
    for line in lines:
        lspl = line.split(",")
        well, stage = lspl[:2]
        if stage in ["121", "41", "42"]:
            stage = stage[:-1] + "-Part" + stage[-1]
        e, n, d = [float(s) for s in lspl[4:7]]
        perfs[well][stage] = {}
        perfs[well][stage] = {"north": n, "east": e, "depth": d}
    return perfs


def read_wells():
    base_dir = "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\setup\\"
    wells = {}
    for well in ["A", "B", "C", "D"]:
        wells[well] = {}
        f = open(base_dir + "Well_" + well + ".csv")
        head = f.readline()
        unit = f.readline()
        lines = f.readlines()
        f.close()
        nwell = len(lines)
        easting, northing, tvdss, md = (
            np.zeros(nwell),
            np.zeros(nwell),
            np.zeros(nwell),
            np.zeros(nwell),
        )
        ii = -1
        for line in lines:
            ii += 1
            lspl = line.split(",")
            md[ii] = float(lspl[1])
            easting[ii], northing[ii] = [float(s) for s in lspl[9:11]]
            tvdss[ii] = float(lspl[5])
        f = open(base_dir + "Perfs_" + well + ".csv")
        head = f.readline()
        lines = f.readlines()
        f.close()
        ii = -1
        for line in lines:
            ii += 1
            lspl = line.split(",")
            stage = "Stage " + str(int(round(float(lspl[0]))))
            if not (stage in wells[well].keys()):
                wells[well][stage] = {}
                jj = 0
            jj += 1
            top, bottom = [float(s) for s in lspl[1:3]]
            if well == "C":
                cluster = "Port " + str(jj)
            else:
                cluster = "Perf " + str(jj)
            wells[well][stage][cluster] = {}
            wells[well][stage][cluster]["top_east"] = np.interp(top, md, easting)
            wells[well][stage][cluster]["top_north"] = np.interp(top, md, northing)
            wells[well][stage][cluster]["top_tvdss"] = np.interp(top, md, tvdss)
            wells[well][stage][cluster]["bottom_east"] = np.interp(bottom, md, easting)
            wells[well][stage][cluster]["bottom_north"] = np.interp(
                bottom, md, northing
            )
            wells[well][stage][cluster]["bottom_tvdss"] = np.interp(bottom, md, tvdss)
        wells[well]["easting"] = easting
        wells[well]["northing"] = northing
        wells[well]["tvdss"] = tvdss
    return wells
