# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 10:34:27 2019

@author: adambaig
"""

import numpy as np
import matplotlib.pyplot as plt
from read_inputs import read_treatment, read_events
from datetime import datetime
from obspy import UTCDateTime

# approximate relative eastings of the injection wells
well_rel_easting= {
        'A': 307.5,
        'B': 41.,
        'C': -225.5,
        'D': -492.  
        }

def calib_mag(amp):
    slope = 0.9748814213323481
    yint = -1.4460873670177525
    return slope*np.log10(amp) + yint


if not(globals().has_key('treatment_data')):  
    # this take a long time, so only read in if it's not a global
    treatment_data = read_treatment()
events = read_events()
wells = ['A','B','D']
for well in wells:
    well_events = { k:v for (k,v) in events.items() if v['well'] == well }
    for stage in np.unique(np.array([ v['stage'] for (k,v) in well_events.items() ])):
        # shift time to relative accounting for mountain time to utc (7 hours)
        frac_events = { k:v for (k,v) in well_events.items() if 
                       ((v['stage']==stage)&(v['cluster']==0)) }
        stage_treatment = { k:v for (k,v) in treatment_data.items() if 
                                            ((v['stage']==stage)&(v['well']==well)) }
        treatment_time_unsorted = np.array([ UTCDateTime(k) for (k,v) in stage_treatment.items()])
        i_time_sort = np.argsort(treatment_time_unsorted)
        treatment_time = treatment_time_unsorted[i_time_sort]
        pressure      = np.array([v['pressure'] for (k,v) in stage_treatment.items()])[i_time_sort]
        slurry_rate   = np.array([v['slurry_rate'] for (k,v) in stage_treatment.items()])[i_time_sort]
        proppant_conc = np.array([v['proppant_conc'] for (k,v) in stage_treatment.items()])[i_time_sort]
        t0,tlast = treatment_time[0],treatment_time[-1]
        if (well=='B' and stage=='10'):
            tlast = UTCDateTime(2016,11,22,17,00,000001)
        fig = plt.figure()
        fig.tight_layout()
        a1 = fig.add_axes([0.2,0.52,0.6,0.33])
        a1.set_ylabel('easting relative to\ntreatment well (m)')
        a1.set_title('Well '+well+': Stage '+stage)
        magnitude = np.array([calib_mag(v['stack_amp']) for (k,v) in frac_events.items() ])
        easting = np.array([v['easting'] for (k,v) in frac_events.items() ])
        event_time = np.array([UTCDateTime(k) for (k,v) in frac_events.items() ]) - 7*3600 - t0
        isort = np.argsort(magnitude) # plot high mag events on top
        scatterplot = a1.scatter(event_time[isort],easting[isort]-well_rel_easting[well],
                    c=magnitude[isort],vmax = 0,vmin=-1.8,cmap = 'cubehelix',edgecolor='0.2',zorder=10)
        a1.plot([0,tlast-t0],[0,0],'k',lw=6,zorder=-2)
        a1.plot([0,tlast-t0],[0,0],'0.5',lw=4,zorder=-1)     
        y1,y2 = a1.get_ylim()
        for well_to_plot in ['A','B','C','D']:            
            a1.plot([0,tlast-t0],[well_rel_easting[well_to_plot]-well_rel_easting[well],
                     well_rel_easting[well_to_plot]-well_rel_easting[well]],'0.5',lw=3,zorder=-6)
   
        a1.set_xlim([0,tlast-t0])
        a1.set_ylim([y1,y2])
        xticks = np.arange(3600 - t0.timestamp%3600,tlast-t0,3600)
        a1.set_xticks(xticks)
        a1.set_xticklabels([])
        a2 = fig.add_axes([0.2,0.15,0.6,0.33])
        a2.plot(treatment_time - t0,pressure/120.,'forestgreen')
        a2.set_ylabel('pressure (MPa)', color='forestgreen')
        a2.set_ylim([0,1])
        a2.set_xlim([0,tlast-t0])
        a2.set_xticks(xticks)
        a2.hist(sorted(event_time),bins=np.arange(0,tlast-t0,300), zorder = -4,
                weights=np.ones(len(event_time))/20.)
        xticklabels = []
        for tick in xticks:
            xticklabels.append(datetime.strftime(UTCDateTime(
                    t0+tick)._get_datetime(), '%b %d\n%H%M'))
        a2.set_xticklabels(xticklabels)
        a2.set_yticks(np.arange(0,7.)/6)
        a2.set_xlabel('Mountain Standard Time')
        a2.set_yticklabels(['0','20','40','60','80','100','120'])
        a3 = a2.twinx()
        a3.plot(treatment_time - t0,slurry_rate/15.,'firebrick')
        a3.set_ylabel('slurry rate (m${^3}$/min)', color='firebrick')
        a3.set_ylim([0,1])
        a3.set_yticks(np.arange(0,7.)/6)
        a3.set_yticklabels(['0','25','50','75','100','125','150'])        
        a2.plot(treatment_time - t0,proppant_conc/500., 'darkgoldenrod')
        a4 = fig.add_axes([0.9,0.15,0,0.33])
        a4.yaxis.set_ticks_position('right')
        a4.yaxis.set_label_position('right')
        a4.set_yticks(np.arange(0,6.)/5)
        a4.set_yticklabels(['0','100','200','300','400','500'])
        a4.set_ylabel('proppant conc. (kg/m$^{3}$)', color='darkgoldenrod')
        a4.set_xticks([])

        a5 = fig.add_axes([0.85,0.52,0.03,0.33])
        cb = fig.colorbar(scatterplot,cax=a5,extend='both')
        cb.set_label('magnitude')
        a7 = fig.add_axes([0.1,0.15,0,0.33])
        a7.set_ylabel('event rate', color='royalblue')
        a7.set_xticks([])
        a7.set_yticks(np.arange(5.)/4.)
        a7.set_yticklabels(['0','5','10','15','20'])
        fig.savefig('RT_plots//well_'+well+'_stage_'+stage+'.png')

        plt.close('all')