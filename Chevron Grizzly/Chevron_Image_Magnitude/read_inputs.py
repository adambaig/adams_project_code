import numpy as np
import glob
from datetime import datetime
from obspy import UTCDateTime
import utm

east0 = 484553.56337197789
north0 = 6021642.0992601141


def read_treatment():
    treatment_dir = (
        "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\"
        + "Chevron 02-04 Treatment Data\\"
    )
    treatment_data = {}
    for well in ["A", "B", "C", "D"]:
        files = glob.glob(treatment_dir + "Well " + well + "\\*.csv")
        for filename in files:
            f = open(filename)
            f.readline()
            f.readline()
            lines = f.readlines()
            f.close()
            stage = filename.split(".csv")[0].split("Interval")[1][1:]
            for line in lines:
                lspl = line.split(",")
                dt = datetime.strptime(lspl[0] + lspl[1], "%d-%b-%y%H:%M:%S")
                timestamp = datetime.strftime(dt, "%Y%m%d%H%M%S.000000")
                treatment_data[timestamp] = {}
                treatment_data[timestamp]["well"] = well
                treatment_data[timestamp]["stage"] = stage
                treatment_data[timestamp]["pressure"] = float(lspl[2])
                treatment_data[timestamp]["slurry_rate"] = float(lspl[8])
                treatment_data[timestamp]["proppant_conc"] = float(lspl[10])
    return treatment_data


def read_events():
    event_dir = "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\"
    event_files = glob.glob(event_dir + "Chevron_withXC_utm_feb11.csv")
    events = {}
    for filename in event_files:
        f = open(filename)
        f.readline()
        lines = f.readlines()
        f.close()
        for line in lines:
            lspl = line.split(",")
            well = lspl[12]
            stagename = lspl[13].split("\n")[0]
            if stagename in ["121", "41", "42"]:
                stage = stagename[:-1] + "-Part" + stagename[-1]
            else:
                stage = stagename
            timestamp = UTCDateTime.strftime(UTCDateTime(lspl[1]), "%Y%m%d%H%M%S.%f")
            events[timestamp] = {}
            events[timestamp]["id"] = lspl[0]
            events[timestamp]["easting"] = float(lspl[2]) - east0
            events[timestamp]["northing"] = float(lspl[3]) - north0
            events[timestamp]["depth"] = float(lspl[4]) - 910.0
            events[timestamp]["magnitude"] = float(lspl[5])
            events[timestamp]["stack_amp"] = float(lspl[6])
            events[timestamp]["filt_amp"] = float(lspl[7])
            events[timestamp]["xc_value"] = float(lspl[8])
            events[timestamp]["master"] = int(lspl[9])
            events[timestamp]["isShallow"] = lspl[10]
            events[timestamp]["cluster"] = int(lspl[11])
            events[timestamp]["well"] = well
            events[timestamp]["stage"] = stage
    return events


def read_perfs():
    base_dir = (
        "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\Chevron_Image_Magnitude\\"
    )
    f = open(base_dir + "Chevron0204_Stages.csv")
    lines = f.readlines()
    f.close()
    perfs = {"A": {}, "B": {}, "D": {}}
    for line in lines:
        lspl = line.split(",")
        well, stage = lspl[:2]
        if stage in ["121", "41", "42"]:
            stage = stage[:-1] + "-Part" + stage[-1]
        e, n, d = [float(s) for s in lspl[4:7]]
        perfs[well][stage] = {}
        perfs[well][stage] = {"north": n, "east": e, "depth": d}
    return perfs


def read_broadband_stations():
    base_dir = "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\Chevron_Image_Magnitude\\PPV_MW\\"
    f = open(base_dir + "stations_9_utm_coords.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        station_name = lspl[0]
        stations[station_name] = {}
        stations[station_name]["lat"] = float(lspl[1])
        stations[station_name]["lon"] = float(lspl[2])
        stations[station_name]["elev"] = float(lspl[5])
        stations[station_name]["easting"] = float(lspl[3]) - east0
        stations[station_name]["northing"] = float(lspl[4]) - north0
    return stations


def read_wells():
    base_dir = "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\setup\\"
    wells = {}
    for well in ["A", "B", "C", "D"]:
        wells[well] = {}
        f = open(base_dir + "Well_" + well + ".csv")
        head = f.readline()
        unit = f.readline()
        lines = f.readlines()
        f.close()
        nwell = len(lines)
        easting, northing = np.zeros(nwell), np.zeros(nwell)
        tvdss, md = np.zeros(nwell), np.zeros(nwell)
        ii = -1
        for line in lines:
            ii += 1
            lspl = line.split(",")
            md[ii] = float(lspl[1])
            easting[ii] = float(lspl[9]) - east0
            northing[ii] = float(lspl[10]) - north0
            tvdss[ii] = float(lspl[5])
        f = open(base_dir + "Perfs_" + well + ".csv")
        head = f.readline()
        lines = f.readlines()
        f.close()
        ii = -1
        for line in lines:
            ii += 1
            lspl = line.split(",")
            stage = "Stage " + str(int(round(float(lspl[0]))))
            if not (stage in wells[well].keys()):
                wells[well][stage] = {}
                jj = 0
            jj += 1
            top, bottom = [float(s) for s in lspl[1:3]]
            if well == "C":
                cluster = "Port " + str(jj)
            else:
                cluster = "Perf " + str(jj)
            wells[well][stage][cluster] = {}
            wells[well][stage][cluster]["top_east"] = np.interp(top, md, easting)
            wells[well][stage][cluster]["top_north"] = np.interp(top, md, northing)
            wells[well][stage][cluster]["top_tvdss"] = np.interp(top, md, tvdss)
            wells[well][stage][cluster]["bottom_east"] = np.interp(bottom, md, easting)
            wells[well][stage][cluster]["bottom_north"] = np.interp(
                bottom, md, northing
            )
            wells[well][stage][cluster]["bottom_tvdss"] = np.interp(bottom, md, tvdss)
        wells[well]["easting"] = easting
        wells[well]["northing"] = northing
        wells[well]["tvdss"] = tvdss
    return wells


def read_PFI_stations():
    base_dir = "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\Chevron_Image_Magnitude\\PPV_MW\\"
    f = open(base_dir + "Chev0204_BuriedArray_StationMeta.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        station_name = lspl[0]
        stations[station_name] = {}
        stations[station_name]["lat"] = float(lspl[1])
        stations[station_name]["lon"] = float(lspl[2])
        stations[station_name]["elev"] = float(lspl[3])
        stations[station_name]["network"] = lspl[5]
        stations[station_name]["location"] = lspl[6]
        stations[station_name]["sample rate"] = float(lspl[7])
        east, north, d1, d2 = utm.from_latlon(float(lspl[1]), float(lspl[2]))
        stations[station_name]["easting"] = east - east0
        stations[station_name]["northing"] = north - north0
    return stations
