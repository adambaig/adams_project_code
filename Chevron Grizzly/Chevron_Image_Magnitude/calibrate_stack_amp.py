import matplotlib

matplotlib.use("Qt5agg")
import numpy as np
import matplotlib.pyplot as plt
from obspy import UTCDateTime
from datetime import datetime
import utm
from scipy.odr import Model, Data, ODR

matplotlib.rcParams["font.size"] = 16

### MWPE coeffs: a = 1, b=0, c=3.604, d = 1.090

fit_type = "simple"
stationdir = (
    "O:\\\\O&G\\Projects\\SO53222 (Chevron Pad 02-04 PFI)\\BuriedArray_Working\\"
)
figdir = "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\Chevron Image Magnitude\\PPV_MW\\figures\\"
f = open(stationdir + "Chev0204_BuriedArray_StationMeta.csv")
f.readline()
lines = f.readlines()
f.close()
stations = {}
for line in lines:
    lspl = line.split(",")
    station_name = "CV.1" + lspl[0] + ".04"
    stations[station_name] = {}
    stations[station_name]["lat"] = float(lspl[1])
    stations[station_name]["lon"] = float(lspl[2])
    stations[station_name]["elev"] = float(lspl[3])
    (
        stations[station_name]["east"],
        stations[station_name]["north"],
        d1,
        d2,
    ) = utm.from_latlon(stations[station_name]["lat"], stations[station_name]["lon"])
plt.close("all")

dr1 = "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\Chevron Image Magnitude\\"
dr2 = "O:\\O&G\\Projects\\SO53222 (Chevron Pad 02-04 PFI)\\Imaging\\Events\\"
pgv_dir = "O:\\O&G\\Projects\\SO53222 (Chevron Pad 02-04 PFI)\\BuriedArray_Working\\06_MagnitudeAnalysis\\PGVs\\"


def sqa(x):
    return np.squeeze(np.array(x))


f = open(dr1 + "PPV_Mw\\PPV_Mw_by_station.csv")
ppv_head = f.readline()
ppv_lines = f.readlines()
f.close()
npv = len(ppv_lines)

f = open(dr1 + "PPV_MW\\Mw_from_Specfit.csv")
spec_head = f.readline()
spec_lines = f.readlines()
f.close()
nspec = len(spec_lines)

f = open(dr2 + "Original_catalog.csv")
orig_lines = f.readlines()
f.close()
norig = len(orig_lines)

f = open(dr2 + "ImagingCatalog-utm-stackAmp.csv")
stack_head = f.readline()
stack_lines = f.readlines()
f.close()
nstack = len(stack_lines)

ppv_event, station, pgv = np.zeros(npv), np.zeros(npv), np.zeros(npv)
ii = -1
for line in ppv_lines:
    ii += 1
    lspl = line.split(",")
    ppv_event[ii] = lspl[0]
    station[ii] = lspl[2]
    pgv[ii] = float(lspl[4])

mw_event, mw_spec, mw_time = np.zeros(nspec), np.zeros(nspec), np.zeros(nspec)
str_drop, crn_freq, kappa = np.zeros(nspec), np.zeros(nspec), np.zeros(nspec)
ii = -1
for line in spec_lines:
    ii += 1
    lspl = line.split(",")
    mw_event[ii] = lspl[0]
    mw_spec[ii] = float(lspl[1])
    mw_time[ii] = UTCDateTime(lspl[6])
    str_drop[ii] = float(lspl[5])
    crn_freq[ii] = float(lspl[4])
    kappa[ii] = float(lspl[3])

stack_amp = np.zeros(len(stack_lines))
filt_amp = np.zeros(len(stack_lines))
stack_id = np.zeros(len(stack_lines))
stack_serial = np.zeros(len(stack_lines))

orig_id = np.zeros(norig)
orig_mw = np.zeros(norig)
orig_lt = np.zeros(norig)
orig_ln = np.zeros(norig)
orig_dp = np.zeros(norig)
orig_serial_time = np.zeros(norig)
ii = -1
for oline in orig_lines:
    ii += 1
    lspl = oline.split(",")
    orig_id[ii] = lspl[0]
    orig_ln[ii], orig_lt[ii], orig_dp[ii], orig_mw[ii], orig_serial_time[ii] = [
        float(s) for s in lspl[1:6]
    ]

ii = -1
for sline in stack_lines:
    ii += 1
    lspl = sline.split(",")
    if len(lspl) > 1:
        stack_id[ii] = int(lspl[0])
        stack_amp[ii] = float(lspl[6])
        filt_amp[ii] = float(lspl[5])
        tm, us = lspl[1].split(".")
        stack_time = datetime.strptime(tm, "%Y-%m-%d %H:%M:%S")
        delta = stack_time - datetime(1970, 1, 1)
        stack_serial[ii] = 86400 * delta.days + delta.seconds + float("." + us)

# assemble original catalog
events = {}
ii = -1
jj = 0
for event_id in orig_id:
    ii += 1
    timestamp = UTCDateTime.strftime(
        UTCDateTime(orig_serial_time[ii]), "%Y%m%d%H%M%S.%f"
    )
    events[timestamp] = {}
    events[timestamp]["lat"] = orig_lt[ii]
    events[timestamp]["lon"] = orig_ln[ii]
    events[timestamp]["east"], events[timestamp]["north"], d1, d2 = utm.from_latlon(
        orig_lt[ii], orig_ln[ii]
    )
    events[timestamp]["depth"] = orig_dp[ii] * 1000.0 - 910.0
    i_mw = np.where(abs(mw_time - orig_serial_time[ii]) < 1)[0]
    events[timestamp]["mw_calc"] = "SPEC" if len(i_mw) > 0 else "PPV"
    events[timestamp]["mw"] = mw_spec[i_mw] if len(i_mw) > 0 else orig_mw[ii]
    if len(i_mw > 0):
        events[timestamp]["stress drop"] = str_drop[i_mw]
        events[timestamp]["kappa"] = kappa[i_mw]
        events[timestamp]["corner freq"] = crn_freq[i_mw]
    events[timestamp]["PGV"] = {}
    round_id = str(int(round(event_id)))
    filename = pgv_dir + (10 - len(round_id)) * "0" + round_id + "_PGVs.csv"
    try:
        g = open(filename)
        pgv_lines = g.readlines()
        g.close()
        for line in pgv_lines:
            lspl = line.split(",")
            if float(lspl[4]) > 1e-4:
                station = lspl[1]
                events[timestamp]["PGV"][station] = (
                    float(lspl[4]) * 1e-8
                )  # 1e-8 is the apparent calibration coeff
    except:
        jj += 1
mw_comp, pgv_comp, d_hyp_comp, azimuth_comp = [], [], [], []
for event in {k: v for (k, v) in events.items() if (v["mw_calc"] == "SPEC")}:
    for station in events[event]["PGV"]:
        mw_comp.append(events[event]["mw"])
        pgv_comp.append(events[event]["PGV"][station])
        distance = np.sqrt(
            (events[event]["east"] - stations[station]["east"]) ** 2
            + (events[event]["north"] - stations[station]["north"]) ** 2
            + (events[event]["depth"] + stations[station]["elev"]) ** 2
        )
        d_hyp_comp.append(distance)
        azimuth_comp.append(
            np.arctan2(
                events[event]["east"] - stations[station]["east"],
                events[event]["north"] - stations[station]["north"],
            )
        )
mw_comp = sqa(mw_comp)
pgv_comp = sqa(pgv_comp)
d_hyp_comp = sqa(d_hyp_comp)


def fit_data(ppvs, distances, mags, fit_type):
    n_rows = len(ppvs)
    if fit_type == "simple":
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags])).T
        rhs = np.matrix(np.log10(ppvs) - np.log10(distances))
    elif fit_type == "geom_only":
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags, np.log10(distances)])).T
        rhs = np.matrix(np.log10(ppvs))
    elif fit_type == "include_anel":
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags, distances])).T
        rhs = np.matrix(np.log10(ppvs) - np.log10(distances))
    elif fit_type == "geom_and_anel":
        Amatrix = np.matrix(
            np.vstack([np.ones(n_rows), mags, np.log10(distances), distances])
        ).T
        rhs = np.matrix(np.log10(ppvs))
    return sqa(np.linalg.inv(Amatrix.T * Amatrix) * Amatrix.T * np.matrix(rhs).T)


def predict_mag(ppv, distance, fit, fit_type):
    if fit_type == "simple":
        predicted_mag = (np.log10(ppv) - np.log10(distance) - fit[0]) / fit[1]
    elif fit_type == "geom_only":
        predicted_mag = (np.log10(ppv) - fit[2] * np.log10(distance) - fit[0]) / fit[1]
    elif fit_type == "include_anel":
        predicted_mag = (
            np.log10(ppv) + np.log10(distance) - fit[2] * distance - fit[0]
        ) / fit[1]
    elif fit_type == "geom_and_anel":
        predicted_mag = (
            (fit[0] * np.log10(ppv) + fit[1] * np.log10(distance))
            + fit[2] * distance
            + fit[3]
        )
    return predicted_mag


def linear_fit(B, x):
    return B[0] * x + B[1]


moment = 10 ** (1.5 * mw_spec + 9)
nrg_unscaled = moment * moment * crn_freq * crn_freq * crn_freq

linear_model = Model(linear_fit)
odr_fit_data = Data(np.log10(moment), np.log10(nrg_unscaled))
linear_odr = ODR(odr_fit_data, linear_model, beta0=[0, 0])
linear_output = linear_odr.run()
slope, yint = linear_output.beta
f0, a0 = plt.subplots()
ev_x = np.array([v["east"] for (k, v) in events.items()])
ev_y = np.array([v["north"] for (k, v) in events.items()])
st_x = np.array([v["east"] for (k, v) in stations.items()])
st_y = np.array([v["north"] for (k, v) in stations.items()])
a0.set_aspect("equal")
a0.plot(ev_x, ev_y, "k.")
a0.plot(st_x, st_y, "kv")
a0.plot(ev_x, ev_y, ".", color="0.5", zorder=-2)
a0.set_xlabel("easting (m)")
a0.set_ylabel("northing (m)")
# match imaged events
is_stack_matched = np.zeros(len(stack_lines))
for event in events:
    imatch = np.where(abs(stack_serial - UTCDateTime(event).timestamp) < 1)[0]
    if len(imatch) > 0:
        if len(imatch) > 1:
            isort = np.argsort(abs(stack_serial[imatch] - UTCDateTime(event).timestamp))
            imatch = imatch[isort[0]]
        events[event]["stack amplitude"] = float(stack_amp[imatch])
        events[event]["filt amplitude"] = float(filt_amp[imatch])
        is_stack_matched[imatch] = 1
mw_residual, mw_pgv, mw_comp, dist_comp, station_by_resid = [], [], [], [], []
stack_amp, mw_match_spec, match_stress_drop, match_corner_freq = [], [], [], []
match_energy_index, match_east, match_north = [], [], []
for event in {k: v for (k, v) in events.items() if (v["mw_calc"] == "SPEC")}:
    if "stack amplitude" in events[event].keys():
        stack_amp.append(events[event]["stack amplitude"])
        mw_match_spec.append(events[event]["mw"])
        match_stress_drop.append(events[event]["stress drop"])
        match_corner_freq.append(events[event]["corner freq"])
        event_moment = 10 ** (1.5 * events[event]["mw"] + 9)
        unscaled_energy = event_moment**2 * match_corner_freq[-1] ** 3
        match_energy_index.append(
            unscaled_energy / (10 ** (slope * np.log10(event_moment) + yint))
        )
        events[event]["energy index"] = match_energy_index[-1]
        match_east.append(events[event]["east"])
        match_north.append(events[event]["north"])
    for station in events[event]["PGV"]:
        distance = np.sqrt(
            (events[event]["east"] - stations[station]["east"]) ** 2
            + (events[event]["north"] - stations[station]["north"]) ** 2
            + (events[event]["depth"] + stations[station]["elev"]) ** 2
        )
        mw_comp.append(events[event]["mw"])
        dist_comp.append(distance)
        station_by_resid.append(station)
        a0.plot(events[event]["east"], events[event]["north"], "ro")
        a0.plot(
            [events[event]["east"], stations[station]["east"]],
            [events[event]["north"], stations[station]["north"]],
            zorder=-1,
            lw=0.5,
            color="0.3",
        )
f0.tight_layout()
f0.savefig(figdir + "event_geometry_69_stations.png")

mw_comp = sqa(mw_comp)
fit = fit_data(pgv_comp, d_hyp_comp, mw_comp, fit_type)

for event in {k: v for (k, v) in events.items() if (v["mw_calc"] == "SPEC")}:
    for station in events[event]["PGV"]:
        mw_pgv.append(
            predict_mag(events[event]["PGV"][station], distance, fit, fit_type)
        )
        mw_residual.append(events[event]["mw"] - mw_pgv[-1])
mw_residual = sqa(mw_residual)

stack_amp_nospec, mw_match_nospec = [], []
for event in {k: v for (k, v) in events.items()}:
    if "stack amplitude" in events[event].keys():
        stack_amp_nospec.append(events[event]["stack amplitude"])
        predicted_mags = []
        for station in events[event]["PGV"]:
            distance = np.sqrt(
                (events[event]["east"] - stations[station]["east"]) ** 2
                + (events[event]["north"] - stations[station]["north"]) ** 2
                + (events[event]["depth"] + stations[station]["elev"]) ** 2
            )
            predicted_mags.append(
                predict_mag(events[event]["PGV"][station], distance, fit, fit_type)
            )
        mw_match_nospec.append(np.median(predicted_mags))

stack_amp_nospec = sqa(stack_amp_nospec)
mw_match_nospec = sqa(mw_match_nospec)
ilarge = np.where(stack_amp_nospec > 20)[0]
isgood = list(set(np.where(np.isnan(mw_match_nospec) == False)[0]) & set(ilarge))
odr_fit_stack = Data(np.log10(stack_amp_nospec[isgood]), mw_match_nospec[isgood])

linear_odr = ODR(odr_fit_stack, linear_model, beta0=[1, 1])
linear_output = linear_odr.run()
stack_mag_slope, stack_mag_yint = linear_output.beta

fig, ax = plt.subplots()
ax.semilogx(stack_amp_nospec, mw_match_nospec, ".")
ax.semilogx(
    [0.1, 1e4],
    [-stack_mag_slope + stack_mag_yint, 4 * stack_mag_slope + stack_mag_yint],
)
ax.set_xlabel("image amplitude")
ax.set_ylabel("magnitude from ppv")
fig.tight_layout()
f1, a1 = plt.subplots()
all_pgv, all_stack = [], []
for event in {k: v for (k, v) in events.items()}:
    if "stack amplitude" in events[event].keys():
        for station in events[event]["PGV"]:
            all_pgv.append(events[event]["PGV"][station])
            all_stack.append(events[event]["stack amplitude"])

a1.loglog(all_stack, all_pgv, "ko", alpha=0.005)
a1.set_ylabel("peak ground velocity (m/s)")
a1.set_xlabel("image amplitude")
a1.set_xlim([0.1, 1e4])
f1.tight_layout()

plt.show()
