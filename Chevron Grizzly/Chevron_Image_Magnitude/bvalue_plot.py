# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 16:16:38 2019

@author: adambaig
"""

import numpy as np
import matplotlib.pyplot as plt

def plotFMD():
    datafile = r'O:\O&G\Projects\SO53222 (Chevron Pad 02-04 PFI)\Imaging\Events\image_catalog_with_matched_or_xc_magnitudes.csv'
    magCol = 7

    histBins = [-2.0,3.0,0.1]

    regressionSplit = [0.4]

    bins = np.arange(histBins[0],histBins[1],histBins[2])
    data= np.genfromtxt(datafile,delimiter=',',dtype=float,usecols=magCol)
    print 'Min:',np.min(data),'Max:',np.max(data),'Count:',len(data)
    mwHist, binEdge = np.histogram(data, bins=bins)

    # plot mag bins
    ind = np.where(mwHist==np.max(mwHist))[0][0]+1
    Mc = binEdge[ind] - (histBins[2] / 2)
    Mc_count = mwHist[ind]
    plt.figure()
    plt.hist(data, bins=bins, color='lightsteelblue', edgecolor='black', alpha=1)

    # reverse cumulative sum
    countRange = np.where(mwHist > 0)[0]
    cumMw = np.cumsum(mwHist)
    negcumMw = len(data) - cumMw
    plt.plot(binEdge[countRange] + (histBins[2] / 2), negcumMw[countRange], 'o',color="r", alpha=0.8)

    if len(regressionSplit)==0:
        indRegress = np.where((negcumMw>=5) & (binEdge[:-1]>=Mc))[0]

        A = np.vstack([binEdge[indRegress], np.ones(len(binEdge[indRegress]))]).T
        logregressMw = np.log10(negcumMw[indRegress])
        m, c = np.linalg.lstsq(A, logregressMw)[0]
        print 'b = ', m * -1
        print 'a = ', c
        y1 = 10 ** ((np.min(binEdge[indRegress]) - (histBins[2]/2)) * m + c)
        y2 = 10 ** ((np.max(binEdge[indRegress]) + (histBins[2]/2)) * m + c)
        plt.plot([np.min(binEdge[indRegress]) - (histBins[2]/2),np.max(binEdge[indRegress]) + (histBins[2]/2)], [y1, y2], 'k-')
    else:
        regressionSplit.append(15.0)
        for i, split in enumerate(regressionSplit):
            if i==0:
                indRegress = np.where((negcumMw >= 5) & (binEdge[:-1] >= Mc) & (binEdge[:-1]<split))[0]
            else:
                indRegress = np.where((negcumMw >= 5) & (binEdge[:-1] >= regressionSplit[i-1]) & (binEdge[:-1] <= split))[0]

            A = np.vstack([binEdge[indRegress], np.ones(len(binEdge[indRegress]))]).T
            logregressMw = np.log10(negcumMw[indRegress])
            m, c = np.linalg.lstsq(A, logregressMw)[0]
            print 'b'+str(i+1)+' = ', m * -1
            print 'a'+str(i+1)+' = ', c
            y1 = 10 ** ((np.min(binEdge[indRegress]) - (histBins[2] / 2)) * m + c)
            y2 = 10 ** ((np.max(binEdge[indRegress]) + (histBins[2] / 2)) * m + c)
            plt.plot([np.min(binEdge[indRegress]) - (histBins[2] / 2), np.max(binEdge[indRegress]) + (histBins[2] / 2)],
                     [y1, y2], 'k-')

    plt.gca().set_yscale('log')
    plt.xlim([np.min(binEdge[countRange])-histBins[2],np.max(binEdge[countRange])+histBins[2]])
    plt.xlabel('Mw')
    plt.ylabel('N(>=M)')
    plt.draw()
    print 'Mc = ', Mc
    plt.show()

def plotFMD_from_dict(dict_with_mag,regressionSplit=[],manual_fit=False):
    histBins = [-2.0,3.0,0.1]
    bins = np.arange(histBins[0],histBins[1],histBins[2])
    data= np.array([v['mw'] for (k,v) in dict_with_mag.items()])
    igood = np.where(data>-7)[0]
    print 'Min:',np.min(data[igood]),'Max:',np.max(data[igood]),'Count:',len(data[igood])
    mwHist, binEdge = np.histogram(data[igood], bins=bins)

    # plot mag bins
    ind = np.where(mwHist==np.max(mwHist))[0][0]+1
    Mc = binEdge[ind] - (histBins[2] / 2)
    fig,ax = plt.subplots() 
    ax.hist(data[igood], bins=bins, color='lightsteelblue', edgecolor='black', alpha=1)

    # reverse cumulative sum
    countRange = np.where(mwHist > 0)[0]
    cumMw = np.cumsum(mwHist)
    negcumMw = len(data[igood]) - np.hstack([0,cumMw])[:-1]
    ax.plot(binEdge[countRange] + (histBins[2] / 2), negcumMw[countRange], 'o',color="r", markeredgecolor='k', alpha=1)
    ax.set_yscale('log')
    
    ax.set_xlim([np.min(binEdge[countRange])-histBins[2],np.max(binEdge[countRange])+histBins[2]])
    
    ax.set_xlabel('M')
    ax.set_ylabel('N(>=M)')
    points = []
    if manual_fit:
        regressionSplit=[0]
        while (len(points)<2):
            points = plt.ginput(-1,show_clicks=True)
        
    if (len(regressionSplit)==0 or len(points)==2):
        if manual_fit:
            x_click = [points[0][0],points[1][0]] 
            indRegress = np.where((bins<=x_click[1]) & 
                                  (bins>=x_click[0]))[0]
        else:
            indRegress = np.where((negcumMw>=5) & (binEdge[:-1]>=Mc))[0]
        A = np.vstack([binEdge[indRegress], np.ones(len(binEdge[indRegress]))]).T
        logregressMw = np.log10(negcumMw[indRegress])
        m, c = np.linalg.lstsq(A, logregressMw)[0]
        print ('b = %.2f' % (m * -1))
        print ('a = %.2f' % c)
        y1 = 10 ** ((np.min(binEdge[indRegress]) - (histBins[2]/2)) * m + c)
        y2 = 10 ** ((np.max(binEdge[indRegress]) + (histBins[2]/2)) * m + c)
        ax.plot([np.min(binEdge[indRegress]) - (histBins[2]/2),np.max(binEdge[indRegress]) + (histBins[2]/2)], [y1, y2], 'k-')
        
    else:
        regressionSplit.append(15.0)
        
        for i, split in enumerate(regressionSplit):
            if i==0:
                indRegress = np.where((negcumMw >= 5) & (binEdge[:-1] >= Mc) & (binEdge[:-1]<split))[0]
            else:
                indRegress = np.where((negcumMw >= 5) & (binEdge[:-1] >= regressionSplit[i-1]) & (binEdge[:-1] <= split))[0]

            A = np.vstack([binEdge[indRegress], np.ones(len(binEdge[indRegress]))]).T
            logregressMw = np.log10(negcumMw[indRegress])
            m, c = np.linalg.lstsq(A, logregressMw)[0]
            print 'b'+str(i+1)+' = ', m * -1
            print 'a'+str(i+1)+' = ', c
            y1 = 10 ** ((np.min(binEdge[indRegress]) - (histBins[2] / 2)) * m + c)
            y2 = 10 ** ((np.max(binEdge[indRegress]) + (histBins[2] / 2)) * m + c)
            ax.plot([np.min(binEdge[indRegress]) - (histBins[2] / 2), np.max(binEdge[indRegress]) + (histBins[2] / 2)],
                     [y1, y2], 'k-')


    print 'Mc = ', Mc
    return(fig)