# -*- coding: utf-8 -*-
"""
Created on Mon Jan 28 15:45:21 2019

@author: adambaig
"""
from obspy import read, UTCDateTime, Stream
import glob
import sys

sys.path.append("..")
import numpy as np
import matplotlib.pyplot as plt
from pylab import where
from datetime import datetime
import matplotlib.patches as mpatches
from read_inputs import read_events

dr = "O:\\O&G\\Projects\\SO53222 (Chevron Pad 02-04 PFI)\\Imaging\\Events\\MSEED_Full\\"
subdivision = 60  # one minute chunks

color_by_well = {"A": "firebrick", "B": "forestgreen", "D": "royalblue"}

f = open("Chevron0204_Stages.csv")
lines = f.readlines()
f.close()

events = read_events()
n_events = {}
for well in ["A", "B", "D"]:
    n_events[well] = {}
    for stage in np.unique(
        np.array([v["stage"] for (k, v) in events.items() if (v["well"] == well)])
    ):
        stage_events = {
            k: v
            for (k, v) in events.items()
            if ((v["well"] == well) and (v["stage"] == stage) and (v["cluster"] == 0))
        }
        n_events[well][stage] = len(stage_events)

stage_start, stage_end, well, stage = [], [], [], []
nstages = len(lines)
for line in lines:
    lspl = line.split(",")
    stage_start.append(
        UTCDateTime(datetime.strptime(lspl[2], "%d/%m/%Y %H:%M:%S")).timestamp
    )
    stage_end.append(
        UTCDateTime(datetime.strptime(lspl[3], "%d/%m/%Y %H:%M:%S")).timestamp
    )
    well.append(lspl[0])
    stage.append(lspl[1])


seeds = sorted(glob.glob(dr + "*.mseed"))
st = read(seeds[-1])
end_of_all = st[0].stats.endtime.timestamp
ii = -1
in_stage = []
for seed in seeds:
    ii += 1
    st = read(seed)
    start, end = st[0].stats.starttime.timestamp, st[0].stats.endtime.timestamp
    trAmp = st.select(station="amp", channel="__Z")[0].data  # filtered amplitude
    rawAmp = st.select(station="amp", channel="amp")[0].data  # stacked amplitude

    # further initialization here... ugly, I know

    if ii == 0:
        t0 = start - 60
        iend_of_all = np.floor((end_of_all - t0) / subdivision)
        noise_floor = np.zeros(int(iend_of_all + 1))
        sample_rate = st[0].stats.sampling_rate
        i_stage_start = []
        i_stage_end = []
        for istage in range(nstages):
            i_stage_start.append((stage_start[istage] - t0) / subdivision)
            i_stage_end.append((stage_end[istage] - t0) / subdivision)
    istart = int(np.floor((start - t0) / subdivision))
    iend = int(np.floor((end - t0) / subdivision))
    for interval in range(istart, iend - 1):
        isamp0 = int(subdivision * sample_rate * (interval - istart))
        isamp1 = int(subdivision * sample_rate * (interval - istart + 1))
        noise_floor[interval] = np.percentile(rawAmp[isamp0:isamp1], 10)
        found_stage = False
        for istage in range(nstages):
            if (interval > i_stage_start[istage]) & (interval < i_stage_end[istage]):
                found_stage = True
                in_stage.append(well[istage])
        if not (found_stage):
            in_stage.append("no")
    noise_floor[iend] = np.percentile(rawAmp[isamp1:], 10)

icheck = where(noise_floor > 1e-7)[0]

f1 = plt.figure()
a1 = f1.add_axes([0.1, 0.3, 0.8, 0.6])
a1.plot(icheck, noise_floor[icheck] * 1e6 / 78.74 / 64.0, ".", zorder=10)
for istage in range(nstages):
    start = (stage_start[istage] - t0) / float(subdivision)
    width = (min(stage_end[istage], end_of_all) - stage_start[istage]) / float(
        subdivision
    )
    patch = mpatches.Rectangle(
        [start, 0], width, 200, color=color_by_well[well[istage]], alpha=0.3
    )
    if stage[istage] in ["41", "42", "121"]:
        stagename = stage[istage][:-1] + "-Part" + stage[istage][-1]
    else:
        stagename = stage[istage]
    a1.text(
        start + width / 2,
        2,
        well[istage] + "-" + stagename,
        ha="center",
        va="bottom",
        rotation=90,
        zorder=25,
        color="w",
    )
    a1.add_patch(patch)
    patch2 = mpatches.Rectangle(
        [start, 0],
        width,
        n_events[well[istage]][stagename],
        color="k",
        alpha=0.3,
        zorder=20,
    )
    a1.add_patch(patch2)
a1.set_xticks(range(420, 5761, 720))
a1.set_xticklabels(
    [
        "Nov 19\n0000",
        "Nov 19\n1200",
        "Nov 20\n0000",
        "Nov 20\n1200",
        "Nov 21\n0000",
        "Nov 21\n1200",
        "Nov 22\n0000",
        "Nov 22\n1200",
    ],
    rotation=90,
)
a1.set_ylabel("stacked noise floor (nm/s)")
a1.set_xlabel("Mountain Standard Time")
a1.set_xlim([0, 5760])
a2 = a1.twinx()
a2.set_ylim([0, 200])
a2.set_ylabel("frac events per stage")
y1, y2 = a1.get_ylim()
a1.set_ylim([0, 200])
f1.savefig("noise_floor_with_time.png", transparent=True)

f2, a2 = plt.subplots()
a2.hist(noise_floor[icheck], bins=np.arange(0, 1, 0.01))
a2.set_xlabel("noise floor of raw stack signal")
a2.set_ylabel("counts")
f2.savefig("noise_histogram.png")

in_stage = np.array(in_stage)

f3, a3 = plt.subplots(2, 2)
i_Awell = list(set(where(in_stage == "A")[0]) & set(icheck))
i_Bwell = list(set(where(in_stage == "B")[0]) & set(icheck))
i_Dwell = list(set(where(in_stage == "D")[0]) & set(icheck))
i_no_well = list(set(where(in_stage == "no")[0]) & set(icheck))

a3[0, 0].hist(noise_floor[i_Awell], bins=np.arange(0, 1, 0.01), color="firebrick")
a3[0, 0].set_xlabel("noise floor of raw stack signal")
a3[0, 0].set_ylabel("counts")
a3[0, 0].set_title("A well pumping")

a3[1, 0].hist(noise_floor[i_Bwell], bins=np.arange(0, 1, 0.01), color="forestgreen")
a3[1, 0].set_xlabel("noise floor of raw stack signal")
a3[1, 0].set_ylabel("counts")
a3[1, 0].set_title("B well pumping")

a3[0, 1].hist(noise_floor[i_Dwell], bins=np.arange(0, 1, 0.01), color="royalblue")
a3[0, 1].set_xlabel("noise floor of raw stack signal")
a3[0, 1].set_ylabel("counts")
a3[0, 1].set_title("D well pumping")

a3[1, 1].hist(noise_floor[i_no_well], bins=np.arange(0, 1, 0.01), color="0.11")
a3[1, 1].set_xlabel("noise floor of raw stack signal")
a3[1, 1].set_ylabel("counts")
a3[1, 1].set_title("outside stages")

f3.savefig("noise_histogram_by_well.png")

# make a legend

f4, a4 = plt.subplots()
a4.set_aspect("equal")
patch1 = mpatches.Rectangle([1, 40], 8, 8, color="firebrick", alpha=0.3)
patch2 = mpatches.Rectangle([1, 30], 8, 8, color="forestgreen", alpha=0.3)
patch3 = mpatches.Rectangle([1, 20], 8, 8, color="royalblue", alpha=0.3)
patch4 = mpatches.Rectangle([1, 10], 8, 8, color="k", alpha=0.3)
a4.add_patch(patch1)
a4.add_patch(patch2)
a4.add_patch(patch3)
a4.add_patch(patch4)
a4.plot([5], [54], ".")
a4.text(12, 51, "noise sample")
a4.text(12, 41, "A well stage")
a4.text(12, 31, "B well stage")
a4.text(12, 21, "D well stage")
a4.text(12, 11, "event rate histogram")
a4.set_xlim([-100, 100])
a4.set_ylim([-100, 100])
