# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 13:23:20 2019

@author: adambaig
"""

import matplotlib

matplotlib.use("Qt5agg")
import numpy as np
import matplotlib.pyplot as plt
from obspy import UTCDateTime
from datetime import datetime
import utm

fit_type = "geom_and_anel"

### MwPE coeffs: a = 1, b=0, c=3.604, d = 1.090 from file

# read in station coordinate file
east0 = 484553.56337197789
north0 = 6021642.0992601141
base_dir = (
    "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\Chevron Image Magnitude\\PPV_MW\\"
)
f = open(base_dir + "Chevron_PFI_stations.csv")
lines = f.readlines()
f.close()


def sqa(x):
    return np.squeeze(np.array(x))


# read in spectral fit data with Mw's
f = open(base_dir + "Mw_from_Specfit.csv")
spec_head = f.readline()
spec_lines = f.readlines()
f.close()
nspec = len(spec_lines)

stations = {}
for line in lines:
    lspl = line.split(",")
    station_name = lspl[0]
    stations[station_name] = {}
    stations[station_name]["lat"] = float(lspl[1])
    stations[station_name]["lon"] = float(lspl[2])
    stations[station_name]["elev"] = float(lspl[3])
    east, north, d1, d2 = utm.from_latlon(
        stations[station_name]["lat"], stations[station_name]["lon"]
    )
    stations[station_name]["east"], stations[station_name]["north"] = (
        east - east0,
        north - north0,
    )
mw_spec = np.zeros(nspec)
mw_event = []
ii = -1
for line in spec_lines:
    ii += 1
    lspl = line.split(",")
    mw_event.append(lspl[0])
    mw_spec[ii] = float(lspl[-2])

# read in PPV measurements
f = open(base_dir + "PPV_Mw_9station.csv")
f.readline()
pgv_lines = f.readlines()
f.close()
ppv = {}
for line in pgv_lines:
    lspl = line.split(",")
    ppv_id = lspl[0]
    if not (ppv_id in ppv.keys()):
        ppv[ppv_id] = {}
    ppv[ppv_id][lspl[2]] = float(lspl[4])

# read in events
f = open(base_dir + "catalog_9_station.csv")
all_head = f.readline()
all_lines = f.readlines()
f.close()
nspec = len(spec_lines)
ii = -1
events = {}
for line in all_lines:
    ii += 1
    lspl = line.split(",")
    event_id = lspl[0]
    events[event_id] = {}
    events[event_id]["lat"] = float(lspl[2])
    events[event_id]["lon"] = float(lspl[3])
    events[event_id]["depth"] = float(lspl[4]) - 910.0
    east, north, d1, d2 = utm.from_latlon(
        events[event_id]["lat"], events[event_id]["lon"]
    )
    events[event_id]["east"], events[event_id]["north"] = east - east0, north - north0
    events[event_id]["pgv"] = ppv[event_id]
    events[event_id]["mw_calc"] = "SPEC" if event_id in mw_event else "PPV"
    events[event_id]["mw"] = float(lspl[5])

f0, a0 = plt.subplots()
ev_x = np.array([v["east"] for (k, v) in events.items()])
ev_y = np.array([v["north"] for (k, v) in events.items()])
st_x = np.array([v["east"] for (k, v) in stations.items()])
st_y = np.array([v["north"] for (k, v) in stations.items()])
a0.set_aspect("equal")
a0.plot(st_x, st_y, "kv")
# a0.plot(ev_x,ev_y,'.')
mw, pgv_spec, d_hyp, azimuth = [], [], [], []
for event in {k: v for (k, v) in events.items() if (v["mw_calc"] == "SPEC")}:
    for station in events[event]["pgv"]:
        mw.append(events[event]["mw"])
        pgv_spec.append(events[event]["pgv"][station])
        distance = np.sqrt(
            (events[event]["east"] - stations[station]["east"]) ** 2
            + (events[event]["north"] - stations[station]["north"]) ** 2
            + (events[event]["depth"] + stations[station]["elev"]) ** 2
        )
        azimuth.append(
            np.arctan2(
                events[event]["east"] - stations[station]["east"],
                events[event]["north"] - stations[station]["north"],
            )
        )
        d_hyp.append(distance)
        a0.plot(
            events[event]["east"],
            events[event]["north"],
            "o",
            color="firebrick",
            markeredgecolor="k",
            zorder=1,
        )
        a0.plot(
            [events[event]["east"], stations[station]["east"]],
            [events[event]["north"], stations[station]["north"]],
            zorder=-1,
            lw=0.5,
            color="0.3",
        )

plt.show()


def fit_data(ppvs, distances, mags, fit_type):
    n_rows = len(ppvs)
    if fit_type == "simple":
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags]))
        rhs = np.matrix(np.log10(ppvs) - np.log10(distances))
    elif fit_type == "geom_only":
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags, np.log10(distances)]))
        rhs = np.matrix(np.log10(ppvs))
    elif fit_type == "include_anel":
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags, distances]))
        rhs = np.matrix(np.log10(ppvs) - np.log10(distances))
    elif fit_type == "geom_and_anel":
        Amatrix = np.matrix(
            np.vstack([np.ones(n_rows), mags, np.log10(distances), distances])
        )
        rhs = np.matrix(np.log10(ppvs))
        return sqa(np.linalg.inv(Amatrix * Amatrix.T) * Amatrix * np.matrix(rhs).T)


def predict_mag(ppv, distance, fit_coeffs, fit_type):
    if fit_type == "simple":
        predicted_mag = (np.log10(ppv) + np.log10(distance) - fit[0]) / fit[1]
    elif fit_type == "geom_only":
        predicted_mag = (np.log10(ppv) - fit[2] * np.log10(distance) - fit[0]) / fit[1]
    elif fit_type == "include_anel":
        predicted_mag = (
            np.log10(ppv) + np.log10(distance) - fit[2] * distance - fit[0]
        ) / fit[1]
    elif fit_type == "geom_and_anel":
        predicted_mag = (
            (fit[0] * np.log10(ppv) + fit[1] * np.log10(distance))
            + fit[2] * distance
            + fit[3]
        )
    return predicted_mag


fit = fit_data(pgv_spec, d_hyp, mw, fit_type)

mw_residual, mw_pgv = [], []
for event in {k: v for (k, v) in events.items() if (v["mw_calc"] == "SPEC")}:
    for station in events[event]["pgv"]:
        distance = np.sqrt(
            (events[event]["east"] - stations[station]["east"]) ** 2
            + (events[event]["north"] - stations[station]["north"]) ** 2
            + (events[event]["depth"] + stations[station]["elev"]) ** 2
        )
        mw_pgv.append(
            predict_mag(events[event]["pgv"][station], distance, fit, fit_type)
        )
        mw_residual.append(events[event]["mw"] - mw_pgv[-1])

mw_residual = np.array(mw_residual)
f1, a1 = plt.subplots()
a1.set_aspect("equal")
a1.plot([0, 3.5], [0, 3.5], "k")
a1.plot(mw, mw_pgv, "r.")
a1.set_xlabel("M${_W}$ from spectral fitting")
a1.set_ylabel("M${_W}$ from peak velocity measurements")
a1.set_ylim([0, 3.5])
a1.set_xlim([0, 3.5])

f2 = plt.figure()
a2 = f2.add_subplot(111, projection="polar")
c = a2.scatter(
    np.pi - np.array(azimuth),
    abs(mw_residual),
    c=mw_residual,
    vmax=0.5,
    vmin=-0.5,
    cmap="bwr",
    edgecolor="k",
)
a2.set_thetagrids(np.arange(0, 361, 45), ["E", "NE", "N", "NW", "W", "SW", "S", "SE"])
a2.set_rlim([0, 1])
a2.set_rgrids(np.arange(0, 0.81, 0.2), ["", "0.4", "", "0.8"])
plt.show()
