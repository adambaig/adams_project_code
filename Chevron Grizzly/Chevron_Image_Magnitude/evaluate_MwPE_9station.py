import matplotlib

matplotlib.use("Qt5agg")
import numpy as np
import matplotlib.pyplot as plt
from obspy import UTCDateTime
from datetime import datetime
import utm
from scipy.odr import Model, Data, ODR
from read_inputs import read_wells, read_events

east0 = 484553.56337197789
north0 = 6021642.0992601141

fit_type = "simple"
frac_events = read_events()
### MwPE coeffs: a = 1, b=0, c=3.604, d = 1.090 from file
matplotlib.rcParams["font.size"] = 16
# read in station coordinate file
base_dir = (
    "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\Chevron Image Magnitude\\PPV_MW\\"
)
figdir = "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\Chevron Image Magnitude\\PPV_MW\\figures\\"

f = open(base_dir + "Chevron_PFI_stations.csv")
lines = f.readlines()
f.close()


def sqa(x):
    return np.squeeze(np.array(x))


# read in spectral fit data with Mw's
f = open(base_dir + "Mw_from_Specfit.csv")
spec_head = f.readline()
spec_lines = f.readlines()
f.close()
nspec = len(spec_lines)

stations = {}
for line in lines:
    lspl = line.split(",")
    station_name = lspl[0]
    stations[station_name] = {}
    stations[station_name]["lat"] = float(lspl[1])
    stations[station_name]["lon"] = float(lspl[2])
    stations[station_name]["elev"] = float(lspl[3])
    east, north, d1, d2 = utm.from_latlon(
        stations[station_name]["lat"], stations[station_name]["lon"]
    )
    stations[station_name]["east"] = east - east0
    stations[station_name]["north"] = north - north0
mw_event = []
mw_spec = np.zeros(nspec)
mw_time, str_drop, crn_freq, kappa = (
    np.zeros(nspec),
    np.zeros(nspec),
    np.zeros(nspec),
    np.zeros(nspec),
)
ii = -1
for line in spec_lines:
    ii += 1
    lspl = line.split(",")
    mw_event.append(lspl[0])
    mw_spec[ii] = float(lspl[1])
    mw_time[ii] = UTCDateTime(lspl[6])
    str_drop[ii] = float(lspl[5])
    crn_freq[ii] = float(lspl[4])
    kappa[ii] = float(lspl[3])

crn_freq = sqa(crn_freq)
str_drop = sqa(str_drop)
kappa = sqa(kappa)
# read in PPV measurements
f = open(base_dir + "PPV_Mw_9station.csv")
f.readline()
pgv_lines = f.readlines()
f.close()
ppv = {}
for line in pgv_lines:
    lspl = line.split(",")
    ppv_id = lspl[0]
    if not (ppv_id in ppv.keys()):
        ppv[ppv_id] = {}
    ppv[ppv_id][lspl[2]] = float(lspl[4])

# read in events
f = open(base_dir + "catalog_9_station.csv")
all_head = f.readline()
all_lines = f.readlines()
f.close()
nspec = len(spec_lines)
ii = -1
events = {}
for line in all_lines:
    ii += 1
    lspl = line.split(",")
    event_id = lspl[0]
    events[event_id] = {}
    events[event_id]["lat"] = float(lspl[2])
    events[event_id]["lon"] = float(lspl[3])
    events[event_id]["depth"] = 1000 * float(lspl[4]) - 910.0
    east, north, d1, d2 = utm.from_latlon(
        events[event_id]["lat"], events[event_id]["lon"]
    )
    events[event_id]["east"] = east - east0
    events[event_id]["north"] = north - north0
    events[event_id]["pgv"] = ppv[event_id]
    events[event_id]["mw_calc"] = "SPEC" if event_id in mw_event else "PPV"
    events[event_id]["mw"] = float(lspl[5])
    i_mw = np.where(np.array(mw_event) == event_id)[0]
    if len(i_mw) > 0:
        events[event_id]["stress drop"] = str_drop[i_mw]
        events[event_id]["kappa"] = kappa[i_mw]
        events[event_id]["corner freq"] = crn_freq[i_mw]


def linear_fit(B, x):
    return B[0] * x + B[1]


moment = 10 ** (1.5 * mw_spec + 9)
nrg_unscaled = moment * moment * crn_freq * crn_freq * crn_freq

linear_model = Model(linear_fit)
odr_fit_data = Data(np.log10(moment), np.log10(nrg_unscaled))
linear_odr = ODR(odr_fit_data, linear_model, beta0=[0, 0])
linear_output = linear_odr.run()
slope, yint = linear_output.beta

f0, a0 = plt.subplots()
ev_x = np.array([v["east"] for (k, v) in events.items()])
ev_y = np.array([v["north"] for (k, v) in events.items()])
st_x = np.array([v["east"] for (k, v) in stations.items()])
st_y = np.array([v["north"] for (k, v) in stations.items()])
a0.set_aspect("equal")
# a0.plot(ev_x,ev_y,'k.')
a0.plot(st_x, st_y, "kv")
# a0.plot(ev_x,ev_y,'.')
a0.set_xlabel("relative easting (m)")
a0.set_ylabel("relative northing (m)")
mw, pgv_spec, d_hyp, azimuth = [], [], [], []
for event in {k: v for (k, v) in events.items() if (v["mw_calc"] == "SPEC")}:
    match_corner_freq = events[event]["corner freq"]
    event_moment = 10 ** (1.5 * events[event]["mw"] + 9)
    unscaled_energy = event_moment**2 * match_corner_freq[-1] ** 3
    events[event]["energy index"] = unscaled_energy / (
        10 ** (slope * np.log10(event_moment) + yint)
    )
    a0.plot(
        events[event]["east"],
        events[event]["north"],
        "o",
        color="firebrick",
        markeredgecolor="k",
    )
    for station in events[event]["pgv"]:
        mw.append(events[event]["mw"])
        pgv_spec.append(events[event]["pgv"][station])
        distance = np.sqrt(
            (events[event]["east"] - stations[station]["east"]) ** 2
            + (events[event]["north"] - stations[station]["north"]) ** 2
            + (events[event]["depth"] + stations[station]["elev"]) ** 2
        )
        azimuth.append(
            np.arctan2(
                events[event]["east"] - stations[station]["east"],
                events[event]["north"] - stations[station]["north"],
            )
        )
        d_hyp.append(distance)
        a0.plot(
            [events[event]["east"], stations[station]["east"]],
            [events[event]["north"], stations[station]["north"]],
            zorder=-1,
            lw=0.5,
            color="0.3",
        )
f0.tight_layout()
f0.savefig(figdir + "event+geometry_9_stations.png")


def fit_data(ppvs, distances, mags, fit_type):
    n_rows = len(ppvs)
    if fit_type == "simple":
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags])).T
        rhs = np.matrix(np.log10(ppvs) - np.log10(distances))
    elif fit_type == "geom_only":
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags, np.log10(distances)])).T
        rhs = np.matrix(np.log10(ppvs))
    elif fit_type == "include_anel":
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags, distances])).T
        rhs = np.matrix(np.log10(ppvs) - np.log10(distances))
    elif fit_type == "geom_and_anel":
        Amatrix = np.matrix(
            np.vstack([np.ones(n_rows), mags, np.log10(distances), distances])
        ).T
        rhs = np.matrix(np.log10(ppvs))
    return sqa(np.linalg.inv(Amatrix.T * Amatrix) * Amatrix.T * np.matrix(rhs).T)


def predict_mag(ppv, distance, fit, fit_type):
    if fit_type == "simple":
        predicted_mag = (np.log10(ppv) - np.log10(distance) - fit[0]) / fit[1]

    elif fit_type == "geom_only":
        predicted_mag = (np.log10(ppv) - fit[2] * np.log10(distance) - fit[0]) / fit[1]
    elif fit_type == "include_anel":
        predicted_mag = (
            np.log10(ppv) + np.log10(distance) - fit[2] * distance - fit[0]
        ) / fit[1]
    elif fit_type == "geom_and_anel":
        predicted_mag = (
            (fit[0] * np.log10(ppv) + fit[1] * np.log10(distance))
            + fit[2] * distance
            + fit[3]
        )
    return predicted_mag


fit = fit_data(pgv_spec, d_hyp, mw, fit_type)

mw_residual, mw_pgv, station_by_resid = [], [], []
for event in {k: v for (k, v) in events.items() if (v["mw_calc"] == "SPEC")}:
    for station in events[event]["pgv"]:
        distance = np.sqrt(
            (events[event]["east"] - stations[station]["east"]) ** 2
            + (events[event]["north"] - stations[station]["north"]) ** 2
            + (events[event]["depth"] + stations[station]["elev"]) ** 2
        )
        mw_pgv.append(
            predict_mag(events[event]["pgv"][station], distance, fit, fit_type)
        )
        mw_residual.append(events[event]["mw"] - mw_pgv[-1])
        station_by_resid.append(station)

mw_residual = np.array(mw_residual)
d_hyp = np.array(d_hyp)
mw = np.array(mw)
f1, a1 = plt.subplots()
a1.set_aspect("equal")
a1.plot([-0.5, 3.5], [-0.5, 3.5], "k")
a1.plot(mw, mw_pgv, "r.")
a1.set_xlabel("M${_W}$ from spectral fitting")
a1.set_ylabel("M${_W}$ from peak velocity measurements")
a1.set_ylim([-0.5, 3.5])
a1.set_xlim([-0.5, 3.5])
f1.tight_layout()
f1.savefig(figdir + "mag_comparison_9_station.png")

f2 = plt.figure()
a2 = f2.add_subplot(111, projection="polar")
c2 = a2.scatter(
    np.pi - np.array(azimuth),
    abs(mw_residual),
    c=mw_residual,
    vmax=0.8,
    vmin=-0.8,
    cmap="bwr",
    edgecolor="k",
)
a2.set_thetagrids(np.arange(0, 361, 45), ["E", "NE", "N", "NW", "W", "SW", "S", "SE"])
a2.set_rlim([0, 1])
a2.set_rgrids(np.arange(0, 0.81, 0.2), ["", "", "", ""])
cb2 = f2.colorbar(c2, ax=a2)
cb2.set_label("M residual (obs - pre)")
f2.tight_layout()
f2.savefig(figdir + "residual_rose_plot_9_stations.png")


f3, a3 = plt.subplots()
isort = np.argsort(mw)

c3 = a3.scatter(d_hyp[isort], mw_residual[isort], c=mw[isort], edgecolor="0.5")
a3.set_xlabel("distance (m)")
a3.set_ylabel("residual mag (obs - pre)")
cb3 = f3.colorbar(c3)
cb3.set_label("M$_W$ from spectra")
f3.tight_layout()
f3.savefig(figdir + "residual_distance_plot_9_stations.png")


spec_events = {k: v for (k, v) in events.items() if "stress drop" in v}
spec_ev_x = np.array([v["east"] for (k, v) in spec_events.items()])
spec_ev_y = np.array([v["north"] for (k, v) in spec_events.items()])
spec_ev_ei = np.array([v["energy index"] for (k, v) in spec_events.items()])

fig, ax = plt.subplots()

ax.set_aspect("equal")

wells = read_wells()
for well in wells:
    ax.plot(wells[well]["easting"], wells[well]["northing"], "k", lw=3, zorder=-10)
    ax.plot(wells[well]["easting"], wells[well]["northing"], "0.5", lw=1, zorder=-9)

scatter = ax.scatter(
    spec_ev_x,
    spec_ev_y,
    c=np.log10(sqa(spec_ev_ei)),
    edgecolor="0.5",
    vmin=-1,
    vmax=1,
    cmap="bwr",
)
cb = fig.colorbar(scatter)
cb.set_label("log$_{10}$(Energy Index)")
ax.set_xlabel("easting (m)")
ax.set_ylabel("northing (m)")
plt.show()
