# -*- coding: utf-8 -*-
"""
Created on Wed Feb  6 11:06:24 2019

@author: adambaig
"""

import numpy as np
import matplotlib.pyplot as plt
from obspy import UTCDateTime
from read_inputs import read_wells

base_dir = "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\"

f = open(base_dir + "Chevron_withXC_utm.csv")
head = f.readline()
lines = f.readlines()
f.close()


def calib_mag(amp):
    slope = 0.9748814213323481
    yint = -1.4460873670177525
    return slope * np.log10(amp) + yint


events = {}

g = open(base_dir + "Chevron_withXC_utm_updated_M.csv", "w")
g.write(head)
for line in lines:
    lspl = line.split(",")
    timestamp = UTCDateTime.strftime(UTCDateTime(lspl[1]), "%Y%m%d%H%M%S.%f")
    events[timestamp] = {}
    events[timestamp]["id"] = lspl[0]
    events[timestamp]["easting"] = float(lspl[2])
    events[timestamp]["northing"] = float(lspl[3])
    events[timestamp]["depth"] = float(lspl[4])

    # events[timestamp]['mw'] = float(lspl[5])
    events[timestamp]["stack amp"] = float(lspl[6])
    events[timestamp]["mw"] = calib_mag(events[timestamp]["stack amp"])
    events[timestamp]["filt amp"] = float(lspl[7])
    events[timestamp]["xcvalue"] = float(lspl[8])
    events[timestamp]["master"] = int(lspl[9])
    events[timestamp]["isShallow"] = lspl[10]
    events[timestamp]["cluster"] = int(lspl[11])
    events[timestamp]["Well"] = lspl[12]
    events[timestamp]["Stage"] = int(lspl[13])
    for ii in range(14):
        if ii == 5:
            g.write(str(calib_mag(events[timestamp]["stack amp"])))
        else:
            g.write(lspl[ii])
        if ii != 13:
            g.write(",")

g.close()


fault_events = {k: v for (k, v) in events.items() if (v["cluster"] == 1)}
frac_events = {k: v for (k, v) in events.items() if (v["cluster"] == 0)}
off_pad = {k: v for (k, v) in fault_events.items() if (v["easting"] > 485050)}
on_pad = {k: v for (k, v) in fault_events.items() if (v["easting"] <= 485050)}

onpad_north = np.array([v["northing"] for (k, v) in on_pad.items()])
onpad_east = np.array([v["easting"] for (k, v) in on_pad.items()])
offpad_north = np.array([v["northing"] for (k, v) in off_pad.items()])
offpad_east = np.array([v["easting"] for (k, v) in off_pad.items()])

fig, ax = plt.subplots()

ax.plot(onpad_east, onpad_north, ".", color="firebrick")
ax.plot(offpad_east, offpad_north, ".", color="royalblue")
