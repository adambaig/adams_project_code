# -*- coding: utf-8 -*-
"""
Created on Mon Jan 21 11:57:23 2019

@author: adambaig
"""

import numpy as np
from pylab import where
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from scipy.odr import Model, Data, ODR

dr = "O:\\\\O&G\\Projects\\SO53222 (Chevron Pad 02-04 PFI)\\Imaging\\Events\\"
time_tolerance = 0.5
fit = "quad"
dr2 = "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\Chevron Image Magnitude\\"


def sqa(x):
    return np.squeeze(np.array(x))


f = open(dr + "Original_catalog.csv")
orig_lines = f.readlines()
f.close()

f = open(dr + "ImagingCatalog-utm-stackAmp.csv")
stack_head = f.readline()
stack_lines = f.readlines()
f.close()

f = open(dr2 + "xcCatalog.csv")
xc_head = f.readline()
xc_lines = f.readlines()
f.close()

plt.close("all")
orig_mag = []
orig_id = []
orig_serial = []
stack_amp = np.zeros(len(stack_lines) + len(xc_lines))
filt_amp = np.zeros(len(stack_lines) + len(xc_lines))
stack_id = np.zeros(len(stack_lines) + len(xc_lines))
stack_serial = np.zeros(len(stack_lines) + len(xc_lines))

ii = -1
for oline in orig_lines[:-1]:
    ii += 1
    lspl = oline.split(",")
    if len(lspl) > 1:
        orig_id_test = int(lspl[0])
        if orig_id_test not in orig_id:
            orig_id.append(orig_id_test)
            orig_mag.append(float(lspl[4]))
            orig_serial.append(float(lspl[5][:-1]))

orig_mag = np.array(orig_mag)
orig_id = np.array(orig_id)
orig_serial = np.array(orig_serial)

ii = -1
for sline in stack_lines:
    ii += 1
    lspl = sline.split(",")
    if len(lspl) > 1:
        stack_id[ii] = int(lspl[0])
        stack_amp[ii] = float(lspl[6])
        filt_amp[ii] = float(lspl[5])
        tm, us = lspl[1].split(".")
        stack_time = datetime.strptime(tm, "%Y-%m-%d %H:%M:%S")
        delta = stack_time - datetime(1970, 1, 1)
        stack_serial[ii] = 86400 * delta.days + delta.seconds + float("." + us)

for sline in xc_lines:
    ii += 1
    lspl = sline.split(",")
    if len(lspl) > 1:
        stack_id[ii] = int(lspl[0])
        stack_amp[ii] = float(lspl[6])
        filt_amp[ii] = float(lspl[5])
        stack_time = datetime.strptime(lspl[1], "%Y%m%d.%H%M%S.%f")
        delta = stack_time - datetime(1970, 1, 1)
        stack_serial[ii] = 86400 * delta.days + delta.seconds + float("." + us)

ii = -1
match_filt_amp, match_amp, match_mag = [], [], []
matched, matched_mag = np.zeros(len(stack_amp)), np.zeros(len(stack_amp))
for orig_time in orig_serial:
    ii += 1
    iMatch = where(np.abs(stack_serial - orig_time) < time_tolerance)[0]
    if len(iMatch == 1):
        match_amp.append(stack_amp[iMatch][0])
        match_mag.append(orig_mag[ii])
        match_filt_amp.append(filt_amp[iMatch][0])
        matched[iMatch] = 1
        matched_mag[iMatch] = match_mag[-1]

match_amp = sqa(match_amp)
match_filt_amp = sqa(match_filt_amp)
match_mag = sqa(match_mag)
igood = where(match_mag > -8)[0]
ilow = list(set(where(match_mag < -0.3)[0]) & set(where(match_amp > 1)[0]))

"""
fitting, might have mixed up dependant and inpedendant variables... oops!
A = np.matrix(np.vstack([match_mag[igood]**2,match_mag[igood],np.ones(len(igood))]))
b = np.matrix(np.log10(match_amp[igood]))
quad,slope,yint = np.linalg.inv(A*A.T)*A*b.T
A2 = np.matrix(np.vstack([match_mag[list(set(igood)&set(ilow))],np.ones(len(list(set(igood)&set(ilow))))]))
b2 = np.matrix(np.log10(match_amp[list(set(igood)&set(ilow))]))
slope2,yint2  = np.linalg.inv(A2*A2.T)*A2*b2.T

"""

# fitting with  ODR
# x --> image stack amplitude
# y --> magnitude


def linear_fit(B, x):
    return B[0] * x + B[1]


def quadratic_fit(B, x):
    return B[0] * x * x + B[1] * x + B[2]


linear_model = Model(linear_fit)
quadratic_model = Model(quadratic_fit)
fit_data = Data(np.log10(match_amp[igood]), match_mag[igood])
fit_data_low_mag = Data(
    np.log10(match_amp[list(set(igood) & set(ilow))]),
    match_mag[list(set(igood) & set(ilow))],
)
quadratic_odr = ODR(fit_data, quadratic_model, beta0=[0, 0, 0])
linear_odr = ODR(fit_data_low_mag, linear_model, beta0=[0, 0])
quadratic_output = quadratic_odr.run()
linear_output = linear_odr.run()

quad, qfit_slope, qfit_yint = quadratic_output.beta
lfit_slope, lfit_yint = linear_output.beta


def predict_mag(img_amp, model):
    log_amp = np.log10(img_amp)
    if model == "quad":
        predicted = quad * log_amp * log_amp + qfit_slope * log_amp + qfit_yint
        return predicted
    if model == "linear":
        predicted = lfit_slope * log_amp + lfit_yint
        return sqa(predicted)


xRange_quad = np.arange(-1, 3.01, 0.1)
yValue_quad = np.zeros(len(xRange_quad))
xRange_lin = np.arange(-1, 1.01, 0.1)
yValue_lin = np.zeros(len(xRange_lin))

ii = -1
for x in xRange_quad:
    ii += 1
    yValue_quad[ii] = predict_mag(10**x, "quad")

ii = -1
for x in xRange_lin:
    ii += 1
    yValue_lin[ii] = predict_mag(10**x, "linear")


# plot scatter plot of magnitude vs image amplitude with fits

f1, a1 = plt.subplots()
scatterplot = a1.scatter(
    match_amp[igood],
    match_mag[igood],
    c=np.log10(match_filt_amp[igood]),
    cmap="plasma",
    edgecolors="0.5",
    vmin=-1,
    vmax=1,
)
colourbar = f1.colorbar(scatterplot)
colourbar.set_label("${\log_{10}}$(filtered amplitude)")
a1.plot(10**xRange_quad, yValue_quad, "forestgreen")
a1.plot(10**xRange_lin, yValue_lin, "firebrick")
a1.set_xscale("log")
a1.set_xlabel("image stack amplitude")
a1.set_ylabel("magnitude")
f1.savefig("chevron_imaging_magnitude_extension.png")

manual_mag = []
expand_mag = []
residual_mag = []
corresponding_amp = []
ii = -1
for line in stack_lines:
    ii += 1
    if matched[ii] and matched_mag[ii] > -8:
        manual_mag.append(matched_mag[ii])
        expand_mag.append(matched_mag[ii])
        corresponding_amp.append(stack_amp[ii])
        residual_mag.append(matched_mag[ii] - predict_mag(stack_amp[ii], fit))
    else:
        # choose lower root
        expand_mag.append(predict_mag(stack_amp[ii], fit))

for line in xc_lines:
    ii += 1
    if matched[ii] and matched_mag[ii] > -8:
        manual_mag.append(matched_mag[ii])
        expand_mag.append(matched_mag[ii])
        corresponding_amp.append(stack_amp[ii])
        residual_mag.append(matched_mag[ii] - predict_mag(stack_amp[ii], fit))
    else:
        # choose lower root
        expand_mag.append(predict_mag(stack_amp[ii], fit))

# match the magntitudes with amplitudes and determine
# the magnitude residuals in different magnitude bins

manual_mag = sqa(manual_mag)
expand_mag = sqa(expand_mag)
residual_mag = sqa(residual_mag)
mag_bins = np.arange(-1.5, 2, 0.1)
n_manual = np.zeros(len(mag_bins))
n_extrap = np.zeros(len(mag_bins))
lowbar_mag, highbar_mag, lowbar_amp, highbar_amp, median_amp, median_mag = (
    [],
    [],
    [],
    [],
    [],
    [],
)
ii = -1
amp_data, match_filt_data, residual_mag_in_bin = [], [], []
for bin in mag_bins[:-1]:
    ii += 1
    ibin = where((match_mag > bin) * (match_mag < mag_bins[ii + 1]))[0]
    ibin2 = where((manual_mag > bin) * (manual_mag < mag_bins[ii + 1]))[0]
    amp_data.append(match_amp[ibin])
    match_filt_data.append(match_filt_amp[ibin])
    residual_mag_in_bin.append(residual_mag[ibin2])
    if len(residual_mag_in_bin[-1]) > 1:
        lowbar_mag.append(np.percentile(residual_mag_in_bin[-1], 5))
        highbar_mag.append(np.percentile(residual_mag_in_bin[-1], 95))
        median_mag.append(np.median(residual_mag_in_bin[-1]))
    else:
        lowbar_mag.append(0)
        highbar_mag.append(0)
        median_mag.append(0)

""" 
I don't think this plot works anymore

fig,ax = plt.subplots()
boxes = ax.boxplot(amp_data,showfliers=False)
ax.set_yscale('log')
ax.set_xticks(np.arange(0.5,36,5))
ax.set_xticklabels(np.arange(-1.5,2.1,0.5))
x1,x2 = ax.get_xlim()
y1,y2 = ax.get_ylim()
ax.semilogy(10*xRange_quad+15.5,yValue_quad,color='forestgreen')
ax.semilogy(10*xRange_lin+15.5,yValue_lin,color='firebrick')
ax.set_xlim([x1,x2])
ax.set_ylim([y1,y2])
ax.set_ylabel('image stack amplitude')
ax.set_xlabel('magnitude')

"""

fig, ax = plt.subplots()
ax.boxplot(residual_mag_in_bin, showfliers=False, whis=[5, 95])
ax.set_xticks(np.arange(0.5, 36, 5))
ax.set_xticklabels(np.arange(-1.5, 2.1, 0.5))
ax.set_ylabel("magnitude residual (observed-predicted)")
ax.set_xlabel("magnitude")

log_imgstack_bins = np.arange(-1, 4, 0.1)
residual_mag_in_logimg, mag_in_logimg = [], []
nbin = []
ii = -1
for bin in log_imgstack_bins[:-1]:
    ii += 1
    ibin = where(
        (np.log10(corresponding_amp) > bin)
        * (np.log10(corresponding_amp) < log_imgstack_bins[ii + 1])
    )[0]
    nbin.append(len(ibin))
    mag_in_logimg.append(manual_mag[ibin])
    residual_mag_in_logimg.append(residual_mag[ibin])
    if len(residual_mag_in_logimg[-1]) > 1:
        lowbar_amp.append(np.percentile(residual_mag_in_logimg[-1], 5))
        highbar_amp.append(np.percentile(residual_mag_in_logimg[-1], 95))
        median_amp.append(np.median(residual_mag_in_logimg[-1]))
    else:
        lowbar_amp.append(0)
        highbar_amp.append(0)
        median_amp.append(0)

f2, a2 = plt.subplots()
a2.boxplot(residual_mag_in_logimg, showfliers=False, whis=[5, 95])
a2.set_xticks(np.arange(0.5, 50.5, 5))
a2.set_xticklabels(np.arange(-1, 4.2, 0.5))
a2.set_ylabel("magnitude residual (observed-predicted)")
a2.set_xlabel("${\log_{10}}$(image stack amplitude)")

f3, a3 = plt.subplots()
a3.semilogx(10 ** (log_imgstack_bins[:-1] + 0.05), lowbar_amp)
a3.semilogx(10 ** (log_imgstack_bins[:-1] + 0.05), median_amp)
a3.semilogx(10 ** (log_imgstack_bins[:-1] + 0.05), highbar_amp)

a3.legend(["5th percentile", "median", "95th percentile"])

lowbar_amp = sqa(lowbar_amp)
highbar_amp = sqa(highbar_amp)
median_amp = sqa(median_amp)
f4, a4 = plt.subplots()
a4.semilogx(
    10 ** (log_imgstack_bins[:-1] + 0.05), (highbar_amp - lowbar_amp) / 2, "forestgreen"
)
a4.set_xlabel("image_amplitude")
a4.set_ylabel("2 ${\sigma}$ magnitude residual", color="forestgreen")

lowbar_mag = sqa(lowbar_mag)
highbar_mag = sqa(highbar_mag)
median_mag = sqa(median_mag)
f5, a5 = plt.subplots()
a5.plot(mag_bins[:-1], (highbar_mag - lowbar_mag) / 2, "firebrick")
a5.set_xlabel("magnitude")
a5.set_ylabel("2 ${\sigma}$ magnitude residual")

sigma_data = Data(
    log_imgstack_bins[11:20] + 0.05, 0.5 * (highbar_amp - lowbar_amp)[11:20]
)
sigma_odr = ODR(sigma_data, linear_model, beta0=[0, 0])
sigma_output = sigma_odr.run()
sigma_slope, sigma_yint = sigma_output.beta

f6, a6 = plt.subplots()
a6.semilogx(
    10 ** (log_imgstack_bins[:-1] + 0.05), (highbar_amp - lowbar_amp) / 2, "forestgreen"
)
a6.set_xlabel("image_amplitude")
a6.set_ylabel("2 ${\sigma}$ magnitude residual", color="forestgreen")
a6.semilogx(
    [0.1, 10], [-sigma_slope + sigma_yint, sigma_slope + sigma_yint], "firebrick", lw=2
)
a7 = a6.twinx()
a7.loglog(10 ** (log_imgstack_bins[:-1] + 0.05), nbin, ".", color="steelblue")
a7.set_ylabel("events in bin", color="steelblue")

log_imgstack_bins = np.arange(-1, 4, 0.1)
residual_mag_in_logimg = []
nmatch, nimg, nxc = [], [], []
ii = -1
ist = set(range(0, 11532))
ixc = set(range(11532, 15047))
imatch = set(where(matched)[0])
iumatch = set(where(matched == False)[0])
for bin in log_imgstack_bins[:-1]:
    ii += 1
    ibin = set(
        where(
            (np.log10(stack_amp) > bin)
            * (np.log10(stack_amp) < log_imgstack_bins[ii + 1])
        )[0]
    )
    nmatch.append(len(list(ibin & imatch)))
    nimg.append(len(list(ibin & iumatch & ist)))
    nxc.append(len(list(ibin & iumatch & ixc)))
"""
f8,a8 = plt.subplots()
p1 = a8.bar(log_imgstack_bins[:-1]+0.05,nmatch,0.08)    
p2 = a8.bar(log_imgstack_bins[:-1]+0.05,nimg,0.08,bottom=nmatch)
p3 = a8.bar(log_imgstack_bins[:-1]+0.05,nxc,0.08,bottom=nmatch+nimg)

a8.legend((p1,p2,p3),['Catalog Match','Imaged','Cross-Correlated'])
a8.set_xticks([-1,0,1,2,3,4])
a8.set_xticklabels(['0.1','1','10','100','1000','10${^4}$'])
a8.set_xlabel('stack image amplitude')
a8.set_ylabel('count')
"""

f9, a9 = plt.subplots()
a9.semilogx(stack_amp, expand_mag, "ko", alpha=0.1)
a9.set_ylabel("magnitude")
a9.set_xlabel("stack image amplitude")

f10, a10 = plt.subplots()
a10.boxplot(mag_in_logimg, showfliers=False, whis=[5, 95])
a10.set_xticks(np.arange(0.5, 50.5, 5))
a10.set_xticklabels(np.arange(-1, 4.2, 0.5))
a10.set_ylabel("magnitude")
a10.set_xlabel("${\log_{10}}$(image stack amplitude)")

f11, a11 = plt.subplots()
a11.semilogx(match_amp[ilow], match_mag[ilow], "o")
a11.semilogx(stack_amp[list(iumatch)], expand_mag[list(iumatch)], ".", alpha=1)
a11.legend(["moment magnitude", "extrapolated magnitude"])
a11.set_xlabel("image stack amplitude")
a11.set_ylabel("magnitude")
f11.savefig("chevron_imaging_magnitude_extension.png")
