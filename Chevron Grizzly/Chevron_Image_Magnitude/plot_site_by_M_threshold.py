import matplotlib

matplotlib.use("Qt5agg")
import numpy as np
from numpy import pi, cos, sin
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from read_inputs import read_wells, read_events, read_BB_stations, read_PFI_stations

from obspy import UTCDateTime

m_threshold = -2

minorLocator = MultipleLocator(100)


def sqa(x):
    return np.squeeze(np.array(x))


wells = read_wells()
events = read_events()
bb_stations = read_BB_stations()
pfi_stations = read_PFI_stations()
rot_ang = 42
R = np.matrix(
    [
        [cos(rot_ang * pi / 180), -sin(rot_ang * pi / 180)],
        [sin(rot_ang * pi / 180), cos(rot_ang * pi / 180)],
    ]
)
events
ev_east = np.array(
    [v["easting"] for (k, v) in events.items() if v["cluster"] in [1, 3]]
)
ev_north = np.array(
    [v["northing"] for (k, v) in events.items() if v["cluster"] in [1, 3]]
)
time = np.array(
    [UTCDateTime(k).timestamp for (k, v) in events.items() if v["cluster"] in [1, 3]]
)
days = (time - min(time)) / 86400
imag = np.where(
    np.array([v["magnitude"] for (k, v) in events.items() if v["cluster"] in [1, 3]])
    > m_threshold
)[0]


pfi_east = np.array([v["easting"] for (k, v) in pfi_stations.items()])
pfi_north = np.array([v["northing"] for (k, v) in pfi_stations.items()])
ev_h1, ev_h2 = [sqa(x) for x in R * np.matrix([ev_east, ev_north])]
# bb_h1, bb_h2 = [sqa(x) for x in R * np.matrix([bb_east, bb_north])]
pfi_h1, pfi_h2 = [sqa(x) for x in R * np.matrix([pfi_east, pfi_north])]
fig, ax = plt.subplots()
ax.set_aspect("equal")
for well in wells:
    if (well == "A") or (well == "C"):
        well_h1, well_h2 = [
            sqa(x)
            for x in R * np.matrix([wells[well]["easting"], wells[well]["northing"]])
        ]
    ax.plot(well_h1, well_h2, "firebrick")

ax.scatter(
    ev_h1[imag], ev_h2[imag], c=days[imag], cmap="gnuplot", vmin=0, vmax=4, s=0.4
)
# ax.plot(bb_h1, bb_h2, "v", color="steelblue", zorder=10, markeredgecolor="k")
ax.plot(pfi_h1, pfi_h2, "v", color="forestgreen", zorder=9, markeredgecolor="k")
ax.set_facecolor("0.9")
ax.xaxis.set_minor_locator(minorLocator)
ax.yaxis.set_minor_locator(minorLocator)
ax.grid(True, which="both")
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.tick_params(color="w")
ax.set_xlim([-1200, 200])
ax.set_ylim([-600, 1000])

plt.show()
