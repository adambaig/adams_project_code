# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 10:35:19 2019

@author: adambaig
"""

import numpy as np
from numpy import where
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from scipy.odr import Model, Data, ODR
from obspy import UTCDateTime

dr1 = "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\Chevron Image Magnitude\\"
dr2 = "O:\\O&G\\Projects\\SO53222 (Chevron Pad 02-04 PFI)\\Imaging\\Events\\"
pgv_dir = "O:\\O&G\\Projects\\SO53222 (Chevron Pad 02-04 PFI)\\BuriedArray_Working\\06_MagnitudeAnalysis\\PGVs\\"


def sqa(x):
    return np.squeeze(np.array(x))


f = open(dr1 + "PPV_Mw\\PPV_Mw_by_station.csv")
ppv_head = f.readline()
ppv_lines = f.readlines()
f.close()
npv = len(ppv_lines)

f = open(dr1 + "PPV_MW\\Mw_from_Specfit.csv")
spec_head = f.readline()
spec_lines = f.readlines()
f.close()
nspec = len(spec_lines)

f = open(dr2 + "Original_catalog.csv")
orig_lines = f.readlines()
f.close()
norig = len(orig_lines)

f = open(dr2 + "ImagingCatalog-utm-stackAmp.csv")
stack_head = f.readline()
stack_lines = f.readlines()
f.close()
nstack = len(stack_lines)

ppv_event, station, pgv = np.zeros(npv), np.zeros(npv), np.zeros(npv)
ii = -1
for line in ppv_lines:
    ii += 1
    lspl = line.split(",")
    ppv_event[ii] = lspl[0]
    station[ii] = lspl[2]
    pgv[ii] = float(lspl[4])

mw_event, mw = np.zeros(nspec), np.zeros(nspec)
ii = -1
for line in spec_lines:
    ii += 1
    lspl = line.split(",")
    mw_event[ii] = lspl[0]
    mw[ii] = float(lspl[1])

stack_amp = np.zeros(len(stack_lines))
filt_amp = np.zeros(len(stack_lines))
stack_id = np.zeros(len(stack_lines))
stack_serial = np.zeros(len(stack_lines))

orig_id = np.zeros(norig)
orig_mw = np.zeros(norig)
orig_lt = np.zeros(norig)
orig_ln = np.zeros(norig)
orig_dp = np.zeros(norig)
orig_serial_time = np.zeros(norig)
ii = -1
for oline in orig_lines:
    ii += 1
    lspl = oline.split(",")
    orig_id[ii] = lspl[0]
    orig_ln[ii], orig_lt[ii], orig_dp[ii], orig_mw[ii], orig_serial_time[ii] = [
        float(s) for s in lspl[1:6]
    ]

ii = -1
for sline in stack_lines:
    ii += 1
    lspl = sline.split(",")
    if len(lspl) > 1:
        stack_id[ii] = int(lspl[0])
        stack_amp[ii] = float(lspl[6])
        filt_amp[ii] = float(lspl[5])
        tm, us = lspl[1].split(".")
        stack_time = datetime.strptime(tm, "%Y-%m-%d %H:%M:%S")
        delta = stack_time - datetime(1970, 1, 1)
        stack_serial[ii] = 86400 * delta.days + delta.seconds + float("." + us)

# assemble original catalog
events = {}
ii = -1
jj = 0
for event_id in orig_id:
    ii += 1
    timestamp = UTCDateTime.strftime(
        UTCDateTime(orig_serial_time[ii]), "%Y%m%d%H%M%S.%f"
    )
    events[timestamp] = {}
    events[timestamp]["mw"] = orig_mw[ii]
    events[timestamp]["lat"] = orig_lt[ii]
    events[timestamp]["lon"] = orig_ln[ii]
    events[timestamp]["depth"] = orig_dp[ii]
    events[timestamp]["mw_calc"] = "SPEC" if event_id in mw_event else "PPV"
    events[timestamp]["PGV"] = {}
    round_id = str(int(round(event_id)))
    filename = pgv_dir + (10 - len(round_id)) * "0" + round_id + "_PGVs.csv"
    try:
        g = open(filename)
        pgv_lines = g.readlines()
        g.close()
        for line in pgv_lines:
            lspl = line.split(",")
            station = lspl[1]
            events[timestamp]["PGV"][station] = float(lspl[4])
    except:
        jj += 1
        print(filename)

# match imaged events
is_stack_matched = np.zeros(len(stack_lines))
for event in events:
    imatch = where(abs(stack_serial - UTCDateTime(event).timestamp) < 1)[0]
    if len(imatch) > 0:
        if len(imatch) > 1:
            isort = np.argsort(abs(stack_serial[imatch] - UTCDateTime(event).timestamp))
            imatch = imatch[isort[0]]
        events[event]["stack amplitude"] = float(stack_amp[imatch])
        events[event]["filt amplitude"] = float(filt_amp[imatch])
        is_stack_matched[imatch] = 1

matched_events = {k: v for (k, v) in events.items() if "stack amplitude" in v}
unmatched_events = {k: v for (k, v) in events.items() if not ("stack amplitude" in v)}
matched_amp = np.array([v["stack amplitude"] for (k, v) in matched_events.items()])
matched_mag = np.array([v["mw"] for (k, v) in matched_events.items()])
matched_pgv = [v["PGV"] for (k, v) in matched_events.items()]


def max_check(x):
    if len(x) > 0:
        return max(x)
    else:
        return 0


max_pgv = [max_check(s.values()) for s in matched_pgv]

# fitting with  ODR
# x --> image stack amplitude
# y --> magnitude


def linear_fit(B, x):
    return B[0] * x + B[1]


def quadratic_fit(B, x):
    return B[0] * x * x + B[1] * x + B[2]


igood = where(np.array(matched_mag) > -7)[0]
ilow = where(np.array(matched_mag) < -0.3)[0]
linear_model = Model(linear_fit)
quadratic_model = Model(quadratic_fit)
fit_data = Data(np.log10(matched_amp[igood]), matched_mag[igood])
fit_data_low_mag = Data(
    np.log10(matched_amp[list(set(igood) & set(ilow))]),
    matched_mag[list(set(igood) & set(ilow))],
)
quadratic_odr = ODR(fit_data, quadratic_model, beta0=[0, 0, 0])
linear_odr = ODR(fit_data_low_mag, linear_model, beta0=[0, 0])
quadratic_output = quadratic_odr.run()
linear_output = linear_odr.run()

quad, qfit_slope, qfit_yint = quadratic_output.beta
lfit_slope, lfit_yint = linear_output.beta


def predict_mag(img_amp, model):
    log_amp = np.log10(img_amp)
    if model == "quad":
        predicted = quad * log_amp * log_amp + qfit_slope * log_amp + qfit_yint
        return predicted
    if model == "linear":
        predicted = lfit_slope * log_amp + lfit_yint
        return sqa(predicted)


unmatched_stack = {}
for i_stack in where(is_stack_matched == 0)[0]:
    timestamp = UTCDateTime.strftime(
        UTCDateTime(stack_serial[i_stack]), "%Y%m%d%H%M%S.%f"
    )
    unmatched_stack[timestamp] = {}
    unmatched_stack[timestamp]["stack amplitude"] = float(stack_amp[i_stack])
    unmatched_stack[timestamp]["filt amplitude"] = float(stack_amp[i_stack])
    unmatched_stack[timestamp]["extrapolated mag"] = predict_mag(
        float(stack_amp[i_stack]), "quad"
    )

unmatched_amp = np.array([v["stack amplitude"] for (k, v) in unmatched_stack.items()])
unmatched_mag = np.array([v["extrapolated mag"] for (k, v) in unmatched_stack.items()])
