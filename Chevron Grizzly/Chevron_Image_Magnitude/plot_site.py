import matplotlib

matplotlib.use("Qt5agg")
import numpy as np
from numpy import pi, cos, sin
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from Chevron_Image_Magnitude.read_inputs import (
    read_wells,
    read_events,
    read_broadband_stations,
    read_PFI_stations,
)

minorLocator1 = MultipleLocator(500)
minorLocator2 = MultipleLocator(200)
minorLocator3 = MultipleLocator(200)


def sqa(x):
    return np.squeeze(np.array(x))


wells = read_wells()
events = read_events()
bb_stations = read_broadband_stations()
pfi_stations = read_PFI_stations()
rot_ang = 222
R = np.matrix(
    [
        [cos(rot_ang * pi / 180), -sin(rot_ang * pi / 180)],
        [sin(rot_ang * pi / 180), cos(rot_ang * pi / 180)],
    ]
)
ev0_east = np.array([v["easting"] for (k, v) in events.items() if v["cluster"] == 0])
ev0_north = np.array([v["northing"] for (k, v) in events.items() if v["cluster"] == 0])
ev1_east = np.array([v["easting"] for (k, v) in events.items() if v["cluster"] == 1])
ev1_north = np.array([v["northing"] for (k, v) in events.items() if v["cluster"] == 1])
ev2_east = np.array([v["easting"] for (k, v) in events.items() if v["cluster"] == 2])
ev2_north = np.array([v["northing"] for (k, v) in events.items() if v["cluster"] == 2])
bb_east = np.array([v["easting"] for (k, v) in bb_stations.items()])
bb_north = np.array([v["northing"] for (k, v) in bb_stations.items()])
pfi_east = np.array([v["easting"] for (k, v) in pfi_stations.items()])
pfi_north = np.array([v["northing"] for (k, v) in pfi_stations.items()])
ev1_h1, ev1_h2 = [sqa(x) for x in R * np.matrix([ev1_east, ev1_north])]
ev0_h1, ev0_h2 = [sqa(x) for x in R * np.matrix([ev0_east, ev0_north])]
bb_h1, bb_h2 = [sqa(x) for x in R * np.matrix([bb_east, bb_north])]
pfi_h1, pfi_h2 = [sqa(x) for x in R * np.matrix([pfi_east, pfi_north])]
fig, ax = plt.subplots()
ax.set_aspect("equal")
for well in wells:
    if (well == "A") or (well == "C") or (well == "B") or (well == "D"):
        well_h1, well_h2 = [
            sqa(x)
            for x in R * np.matrix([wells[well]["easting"], wells[well]["northing"]])
        ]
    ax.plot(well_h1, well_h2, "firebrick")
ax.plot(ev0_h1, ev0_h2, "k.", ms=0.4)
ax.plot(ev1_h1, ev1_h2, "k.", ms=0.4)
ax.plot(bb_h1, bb_h2, "v", color="steelblue", zorder=10, markeredgecolor="k")
ax.plot(pfi_h1, pfi_h2, "v", color="orange", zorder=9, markeredgecolor="k")
ax.set_facecolor("0.9")
ax.xaxis.set_minor_locator(minorLocator1)
ax.yaxis.set_minor_locator(minorLocator1)
ax.grid(True, which="both")
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.tick_params(color="w")


unique_stages = np.unique(
    np.array([v["well"] + v["stage"].split("-")[0] for k, v in events.items()])
)
color = {"A": "forestgreen", "B": "mediumorchid"}
symbol = {"A": "s", "B": "o"}
f1, a1 = plt.subplots()
a1.set_aspect("equal")
for well in ["A", "B"]:
    easts = [
        v["easting"]
        for k, v in events.items()
        if (v["well"] == well and v["cluster"] == 0)
    ]
    norths = [
        v["northing"]
        for k, v in events.items()
        if (v["well"] == well and v["cluster"] == 0)
    ]
    mags = [
        np.log10(v["stack_amp"]) * 0.98 - 1.4
        for k, v in events.items()
        if (v["well"] == well and v["cluster"] == 0)
    ]
    h1, h2 = [sqa(x) for x in R * np.matrix([easts, norths])]
    scatter = a1.scatter(
        h1,
        h2,
        marker=symbol[well],
        c=mags,
        cmap="viridis",
        vmin=-2,
        vmax=-1,
        zorder=4,
        edgecolor="k",
    )
x1, x2 = a1.get_xlim()
y1, y2 = a1.get_ylim()
for well in wells:
    well_h1, well_h2 = [
        sqa(x) for x in R * np.matrix([wells[well]["easting"], wells[well]["northing"]])
    ]
    a1.plot(well_h1, well_h2, "firebrick", zorder=-1)
    if well == "A" or well == "B":
        for stage in unique_stages[np.array([s[0] == well for s in unique_stages])]:
            stage_name = "Stage " + stage[1:]
            perfs = list(wells["A"]["Stage " + stage[1:]].keys())
            perfs.sort()
            first_perf = perfs[0]
            last_perf = perfs[-1]
            east_perf_1 = wells[well][stage_name][first_perf]["bottom_east"]
            north_perf_1 = wells[well][stage_name][first_perf]["bottom_north"]
            east_perf_last = wells[well][stage_name][last_perf]["top_east"]
            north_perf_last = wells[well][stage_name][last_perf]["bottom_north"]
            h1_first_perf, h2_first_perf = [
                sqa(x) for x in R * np.matrix([east_perf_1, north_perf_1]).T
            ]
            h1_last_perf, h2_last_perf = [
                sqa(x) for x in R * np.matrix([east_perf_last, north_perf_last]).T
            ]
            a1.plot(
                [h1_first_perf, h1_last_perf],
                [h2_first_perf, h2_last_perf],
                "k",
                lw=4,
                zorder=2,
            )
            a1.plot(
                [h1_first_perf, h1_last_perf],
                [h2_first_perf, h2_last_perf],
                color[well],
                lw=2,
                zorder=3,
            )
a1.set_facecolor("0.95")
a1.xaxis.set_minor_locator(minorLocator2)
a1.yaxis.set_minor_locator(minorLocator2)
a1.set_xlim([x1, x2])
a1.set_ylim([y1, y2])
a1.grid(True, which="both")
a1.set_xticklabels([])
a1.set_yticklabels([])
a1.tick_params(color="w")

cb = f1.colorbar(scatter, extend="max")
cb.set_label("calibrated M$_{W}$")

plt.show()
