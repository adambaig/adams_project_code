# -*- coding: utf-8 -*-
"""
Created on Wed Feb  6 15:35:29 2019

@author: adambaig
"""

import numpy as np
import matplotlib as plt
from obspy import UTCDateTime
from bvalue_plot import plotFMD_from_dict

base_dir = "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\history\\"

f = open(base_dir + "Initial_9_station_processing.csv")
f.readline()
lines = f.readlines()
f.close()

initial_events = {}
for line in lines:
    lspl = line.split(",")
    timestamp = UTCDateTime.strftime(UTCDateTime(lspl[5]), "%Y%m%d%H%M%S.%f")
    initial_events[timestamp] = {}
    initial_events[timestamp]["id"] = lspl[0]
    initial_events[timestamp]["longitude"] = float(lspl[1])
    initial_events[timestamp]["latitude"] = float(lspl[2])
    initial_events[timestamp]["depth"] = float(lspl[3])
    initial_events[timestamp]["mw"] = float(lspl[4])

f = open(base_dir + "NLLoc_WithMag_BA69lonlat_timestamp_wFWB_recalcMag.csv")
f.readline()
lines = f.readlines()
f.close()

buried_array_events = {}
for line in lines:
    lspl = line.split(",")
    timestamp = UTCDateTime.strftime(UTCDateTime(float(lspl[5])), "%Y%m%d%H%M%S.%f")
    buried_array_events[timestamp] = {}
    buried_array_events[timestamp]["id"] = lspl[0]
    buried_array_events[timestamp]["longitude"] = float(lspl[1])
    buried_array_events[timestamp]["latitude"] = float(lspl[2])
    buried_array_events[timestamp]["depth"] = float(lspl[3])
    buried_array_events[timestamp]["mw"] = float(lspl[4])

initial_plot = plotFMD_from_dict(initial_events, regressionSplit=[0.4])
buried_array_plot = plotFMD_from_dict(buried_array_events, regressionSplit=[0.4])
