# -*- coding: utf-8 -*-
"""
Created on Mon Jan 21 11:57:23 2019

@author: adambaig
"""

import numpy as np
from pylab import where
from datetime import datetime, timedelta
import matplotlib.pyplot as plt


dr = "O:\\\\O&G\\Projects\\SO53222 (Chevron Pad 02-04 PFI)\\Imaging\\Events\\"
time_tolerance = 0.5

dr2 = "C:\\\\Users\\adambaig\\Project\\"


def sqa(x):
    return np.squeeze(np.array(x))


f = open(dr + "Original_catalog.csv")
orig_lines = f.readlines()
f.close()

f = open(dr + "ImagingCatalog-utm-stackAmp.csv")
stack_head = f.readline()
stack_lines = f.readlines()
f.close()

f = open(dr2 + "xcCatalog.csv")
xc_head = f.readline()
xc_lines = f.readlines()
f.close()


orig_mag = []
orig_id = []
orig_serial = []
stack_amp = np.zeros(len(stack_lines) + len(xc_lines))
filt_amp = np.zeros(len(stack_lines) + len(xc_lines))
stack_id = np.zeros(len(stack_lines) + len(xc_lines))
stack_serial = np.zeros(len(stack_lines) + len(xc_lines))

ii = -1
for oline in orig_lines[:-1]:
    ii += 1
    lspl = oline.split(",")
    if len(lspl) > 1:
        orig_id_test = int(lspl[0])
        if orig_id_test not in orig_id:
            orig_id.append(orig_id_test)
            orig_mag.append(float(lspl[4]))
            orig_serial.append(float(lspl[5][:-1]))

orig_mag = np.array(orig_mag)
orig_id = np.array(orig_id)
orig_serial = np.array(orig_serial)

ii = -1
for sline in stack_lines:
    ii += 1
    lspl = sline.split(",")
    if len(lspl) > 1:
        stack_id[ii] = int(lspl[0])
        stack_amp[ii] = float(lspl[6])
        filt_amp[ii] = float(lspl[5])
        tm, us = lspl[1].split(".")
        stack_time = datetime.strptime(tm, "%Y-%m-%d %H:%M:%S")
        delta = stack_time - datetime(1970, 1, 1)
        stack_serial[ii] = 86400 * delta.days + delta.seconds + float("." + us)

for sline in xc_lines:
    ii += 1
    lspl = sline.split(",")
    if len(lspl) > 1:
        stack_id[ii] = int(lspl[0])
        stack_amp[ii] = float(lspl[6])
        filt_amp[ii] = float(lspl[5])
        stack_time = datetime.strptime(lspl[1], "%Y%m%d.%H%M%S.%f")
        delta = stack_time - datetime(1970, 1, 1)
        stack_serial[ii] = 86400 * delta.days + delta.seconds + float("." + us)

"""

I need to get this to work perhaps a little better

nMatch = len(matched_events)
match_amp,match_mag = np.zeros(nMatch), np.zeros(nMatch)
match_serial_o,match_serial_s = np.zeros(nMatch), np.zeros(nMatch)
ii = -1
for matched_event in matched_events:
    ii += 1
    o_id = where(orig_id==matched_event[0])[0]
    s_id = where(stack_id==matched_event[1])[0]
    if len(o_id)>0:
        if len(s_id)>0:
            match_amp[ii] = stack_amp[s_id[0]]
            match_mag[ii] = orig_mag[o_id[0]]
            match_serial_o[ii] = orig_serial[o_id[0]]
            match_serial_s[ii] = stack_serial[s_id[0]]
"""
ii = -1
match_filt_amp, match_amp, match_mag = [], [], []
matched, matched_mag = np.zeros(len(stack_amp)), np.zeros(len(stack_amp))
for orig_time in orig_serial:
    ii += 1
    iMatch = where(np.abs(stack_serial - orig_time) < time_tolerance)[0]
    if len(iMatch == 1):
        match_amp.append(stack_amp[iMatch][0])
        match_mag.append(orig_mag[ii])
        match_filt_amp.append(filt_amp[iMatch][0])
        matched[iMatch] = 1
        matched_mag[iMatch] = match_mag[-1]

match_amp = sqa(match_amp)
match_filt_amp = sqa(match_filt_amp)
match_mag = sqa(match_mag)
matched

igood = where(match_mag > -8)[0]
ilow = where(match_mag < -0.3)[0]


fig, ax = plt.subplots(1)

# ax.semilogy(match_mag[igood],match_amp[igood],'.',color='#1e499e',alpha=0.1)
scatterplot = ax.scatter(
    match_mag[igood],
    match_amp[igood],
    c=np.log10(match_filt_amp[igood]),
    cmap="plasma",
    edgecolors="0.5",
    vmin=-1,
    vmax=1,
)
ax.set_yscale("log")
ax.set_ylabel("image stack amplitude")
ax.set_xlabel("magnitude")

colourbar = fig.colorbar(scatterplot)
colourbar.set_label("${\log_{10}}$(filtered amplitude)")

# fit a quadratic

A = np.matrix(np.vstack([match_mag[igood] ** 2, match_mag[igood], np.ones(len(igood))]))
b = np.matrix(np.log10(match_amp[igood]))

quad, slope, yint = np.linalg.inv(A * A.T) * A * b.T


xRange = np.arange(-1.5, 1, 0.01)
yValue = np.zeros(len(xRange))
ii = -1
for x in xRange:
    ii += 1
    yValue[ii] = 10 ** sqa(quad * x * x + slope * x + yint)

ax.semilogy(xRange, yValue, color="forestgreen")

# fit a line to the lower magnitudes
"""
A2 = np.matrix(np.vstack([match_mag[list(set(igood)&set(ilow))],np.ones(len(list(set(igood)&set(ilow))))]))
b2 = np.matrix(np.log10(match_amp[list(set(igood)&set(ilow))]))
slope2,yint2  = np.linalg.inv(A2*A2.T)*A2*b2.T
xRange = np.arange(-2,-0.3,0.01)
yValue = np.zeros(len(xRange))
ii = -1
for x in xRange:
    ii += 1
    yValue[ii] = 10**sqa(slope2*x+yint2)

ax.semilogy(xRange,yValue,'#9c331e')
"""
fig.savefig("chevron_imaging_magnitude_extension.png")

"""

write a catalog with matched and extrapolated events
"""
g = open(dr + "image_catalog_with_matched_or_xc_magnitudes.csv", "w")
g.write(stack_head.replace("\n", ",magntiude,matched\n"))
ii = -1
for line in stack_lines:
    ii += 1
    g.write(line[:-1])
    if matched[ii] and matched_mag[ii] > -8:
        g.write(", %.3f,Yes\n" % matched_mag[ii])
    else:
        # choose lower root
        extrapolated_mag = (
            (
                -slope
                + np.sqrt(slope * slope - 4 * quad * (yint - np.log10(stack_amp[ii])))
            )
            / 2
            / quad
        )
        # extrapolated_mag = (np.log10(stack_amp[ii]) - yint2)/slope2
        g.write(", %.3f,No\n" % extrapolated_mag)
for line in xc_lines:
    ii += 1
    g.write(line[:-1])
    if matched[ii] and matched_mag[ii] > -8:
        g.write(", %.3f,Yes\n" % matched_mag[ii])
    else:
        # choose lower root
        extrapolated_mag = (
            (
                -slope
                + np.sqrt(slope * slope - 4 * quad * (yint - np.log10(stack_amp[ii])))
            )
            / 2
            / quad
        )
        # extrapolated_mag = (np.log10(stack_amp[ii]) - yint2)/slope2
        g.write(", %.3f,No\n" % extrapolated_mag)


g.close()


manual_mag = []
expand_mag = []
residual_mag = []
ii = -1
for line in stack_lines:
    ii += 1
    if matched[ii] and matched_mag[ii] > -8:
        manual_mag.append(matched_mag[ii])
        expand_mag.append(matched_mag[ii])
        residual_mag.append(
            (
                -slope
                + np.sqrt(slope * slope - 4 * quad * (yint - np.log10(stack_amp[ii])))
            )
            / 2
            / quad
            - matched_mag[ii]
        )
    else:
        # choose lower root
        expand_mag.append(
            (
                -slope
                + np.sqrt(slope * slope - 4 * quad * (yint - np.log10(stack_amp[ii])))
            )
            / 2
            / quad
        )

manual_mag = sqa(manual_mag)
expand_mag = sqa(expand_mag)
residual_mag = sqa(residual_mag)
mag_bins = np.arange(-1.3, 2, 0.05)
n_manual = np.zeros(len(mag_bins))
n_extrap = np.zeros(len(mag_bins))

ii = -1
for bin in mag_bins:
    ii += 1
    n_manual[ii] = len(where(manual_mag > bin)[0])
    n_extrap[ii] = len(where(expand_mag > bin)[0])

fig2, ax2 = plt.subplots(1)

ax2.semilogy(mag_bins, n_manual, "ro")
ax2.semilogy(mag_bins, n_extrap, "go")
ax2.set_xlabel("magnitude")
ax2.set_ylabel("number of events above magnitude")


fig3, ax3 = plt.subplots(1)
sc2 = ax3.scatter(
    match_mag[igood],
    match_filt_amp[igood],
    c=np.log10(match_amp[igood]),
    cmap="viridis",
    edgecolors="0.5",
    vmin=-0.5,
    vmax=1.5,
)
ax3.set_yscale("log")
ax3.set_ylim([0.01, 10000])
ax3.set_xlabel("magnitude")
ax3.set_ylabel("filtered amplitude")
colourbar2 = fig3.colorbar(sc2, extend="both")
colourbar2.set_label("${\log_{10}}$(image stack amplitude)")
