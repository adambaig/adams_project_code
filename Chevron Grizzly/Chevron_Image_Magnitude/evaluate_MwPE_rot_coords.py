# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 09:49:47 2019

@author: adambaig
"""
import matplotlib

matplotlib.use("Qt5agg")
import numpy as np
import matplotlib.pyplot as plt
from obspy import UTCDateTime
from datetime import datetime
import utm
from scipy.odr import Model, Data, ODR

### MWPE coeffs: a = 1, b=0, c=3.604, d = 1.090

from matplotlib.ticker import MultipleLocator

fit_type = "simple"

east0 = 484553.56337197789
north0 = 6021642.0992601141
rot_ang = 222
R = np.matrix(
    [
        [np.cos(rot_ang * np.pi / 180), -np.sin(rot_ang * np.pi / 180)],
        [np.sin(rot_ang * np.pi / 180), np.cos(rot_ang * np.pi / 180)],
    ]
)

stationdir = (
    "O:\\\\O&G\\Projects\\SO53222 (Chevron Pad 02-04 PFI)\\BuriedArray_Working\\"
)
figdir = (
    "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\"
    + "Chevron Image Magnitude\\PPV_MW\\figures\\"
)


minorLocator = MultipleLocator(500)

f = open(stationdir + "Chev0204_BuriedArray_StationMeta.csv")
f.readline()
lines = f.readlines()
f.close()
stations = {}
for line in lines:
    lspl = line.split(",")
    station_name = "CV.1" + lspl[0] + ".04"
    stations[station_name] = {}
    stations[station_name]["lat"] = float(lspl[1])
    stations[station_name]["lon"] = float(lspl[2])
    stations[station_name]["elev"] = float(lspl[3])
    east, north, d1, d2 = utm.from_latlon(
        stations[station_name]["lat"], stations[station_name]["lon"]
    )
    stations[station_name]["east"] = east - east0
    stations[station_name]["north"] = north - north0


plt.close("all")
matplotlib.rcParams["font.size"] = 16
dr1 = "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\Chevron Image Magnitude\\"
dr2 = "O:\\O&G\\Projects\\SO53222 (Chevron Pad 02-04 PFI)\\Imaging\\Events\\"
pgv_dir = (
    "O:\\O&G\\Projects\\SO53222 (Chevron Pad 02-04 PFI)\\"
    + "BuriedArray_Working\\06_MagnitudeAnalysis\\PGVs\\"
)


def sqa(x):
    return np.squeeze(np.array(x))


f = open(dr1 + "PPV_Mw\\PPV_Mw_by_station.csv")
ppv_head = f.readline()
ppv_lines = f.readlines()
f.close()
npv = len(ppv_lines)

f = open(dr1 + "PPV_MW\\Mw_from_Specfit.csv")
spec_head = f.readline()
spec_lines = f.readlines()
f.close()
nspec = len(spec_lines)

f = open(dr2 + "Original_catalog.csv")
orig_lines = f.readlines()
f.close()
norig = len(orig_lines)

f = open(dr2 + "ImagingCatalog-utm-stackAmp.csv")
stack_head = f.readline()
stack_lines = f.readlines()
f.close()
nstack = len(stack_lines)

ppv_event, station, pgv = np.zeros(npv), np.zeros(npv), np.zeros(npv)
ii = -1
for line in ppv_lines:
    ii += 1
    lspl = line.split(",")
    ppv_event[ii] = lspl[0]
    station[ii] = lspl[2]
    pgv[ii] = float(lspl[4])

mw_event, mw_spec, mw_time = np.zeros(nspec), np.zeros(nspec), np.zeros(nspec)
str_drop, crn_freq, kappa = np.zeros(nspec), np.zeros(nspec), np.zeros(nspec)
ii = -1
for line in spec_lines:
    ii += 1
    lspl = line.split(",")
    mw_event[ii] = lspl[0]
    mw_spec[ii] = float(lspl[1])
    mw_time[ii] = UTCDateTime(lspl[6])
    str_drop[ii] = float(lspl[5])
    crn_freq[ii] = float(lspl[4])
    kappa[ii] = float(lspl[3])

stack_amp = np.zeros(len(stack_lines))
filt_amp = np.zeros(len(stack_lines))
stack_id = np.zeros(len(stack_lines))
stack_serial = np.zeros(len(stack_lines))

orig_id = np.zeros(norig)
orig_mw = np.zeros(norig)
orig_lt = np.zeros(norig)
orig_ln = np.zeros(norig)
orig_dp = np.zeros(norig)
orig_serial_time = np.zeros(norig)
ii = -1
for oline in orig_lines:
    ii += 1
    lspl = oline.split(",")
    orig_id[ii] = lspl[0]
    orig_ln[ii], orig_lt[ii], orig_dp[ii], orig_mw[ii], orig_serial_time[ii] = [
        float(s) for s in lspl[1:6]
    ]

ii = -1
for sline in stack_lines:
    ii += 1
    lspl = sline.split(",")
    if len(lspl) > 1:
        stack_id[ii] = int(lspl[0])
        stack_amp[ii] = float(lspl[6])
        filt_amp[ii] = float(lspl[5])
        tm, us = lspl[1].split(".")
        stack_time = datetime.strptime(tm, "%Y-%m-%d %H:%M:%S")
        delta = stack_time - datetime(1970, 1, 1)
        stack_serial[ii] = 86400 * delta.days + delta.seconds + float("." + us)

# assemble original catalog
events = {}
ii = -1
jj = 0
for event_id in orig_id:
    ii += 1
    timestamp = UTCDateTime.strftime(
        UTCDateTime(orig_serial_time[ii]), "%Y%m%d%H%M%S.%f"
    )
    events[timestamp] = {}
    events[timestamp]["lat"] = orig_lt[ii]
    events[timestamp]["lon"] = orig_ln[ii]
    east, north, d1, d2 = utm.from_latlon(orig_lt[ii], orig_ln[ii])
    events[timestamp]["east"] = east - east0
    events[timestamp]["north"] = north - north0
    events[timestamp]["depth"] = orig_dp[ii] * 1000.0 - 910.0
    i_mw = np.where(abs(mw_time - orig_serial_time[ii]) < 1)[0]
    events[timestamp]["mw_calc"] = "SPEC" if len(i_mw) > 0 else "PPV"
    events[timestamp]["mw"] = mw_spec[i_mw] if len(i_mw) > 0 else orig_mw[ii]
    if len(i_mw > 0):
        events[timestamp]["stress drop"] = str_drop[i_mw]
        events[timestamp]["kappa"] = kappa[i_mw]
        events[timestamp]["corner freq"] = crn_freq[i_mw]
    events[timestamp]["PGV"] = {}
    round_id = str(int(round(event_id)))
    filename = pgv_dir + (10 - len(round_id)) * "0" + round_id + "_PGVs.csv"
    try:
        g = open(filename)
        pgv_lines = g.readlines()
        g.close()
        for line in pgv_lines:
            lspl = line.split(",")
            if float(lspl[4]) > 1e-4:
                station = lspl[1]
                events[timestamp]["PGV"][station] = (
                    float(lspl[4]) * 1e-8
                )  # 1e-8 is the apparent calibration coeff
    except:
        jj += 1


mw_comp, pgv_comp, d_hyp_comp, azimuth_comp = [], [], [], []
for event in {k: v for (k, v) in events.items() if (v["mw_calc"] == "SPEC")}:
    for station in events[event]["PGV"]:
        mw_comp.append(events[event]["mw"])
        pgv_comp.append(events[event]["PGV"][station])
        distance = np.sqrt(
            (events[event]["east"] - stations[station]["east"]) ** 2
            + (events[event]["north"] - stations[station]["north"]) ** 2
            + (events[event]["depth"] + stations[station]["elev"]) ** 2
        )
        d_hyp_comp.append(distance)
        azimuth_comp.append(
            np.arctan2(
                events[event]["east"] - stations[station]["east"],
                events[event]["north"] - stations[station]["north"],
            )
        )

mw_comp = sqa(mw_comp)
pgv_comp = sqa(pgv_comp)
d_hyp_comp = sqa(d_hyp_comp)


def fit_data(ppvs, distances, mags, fit_type):
    n_rows = len(ppvs)
    if fit_type == "simple":
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags])).T
        rhs = np.matrix(np.log10(ppvs) - np.log10(distances))
    elif fit_type == "geom_only":
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags, np.log10(distances)])).T
        rhs = np.matrix(np.log10(ppvs))
    elif fit_type == "include_anel":
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags, distances])).T
        rhs = np.matrix(np.log10(ppvs) - np.log10(distances))
    elif fit_type == "geom_and_anel":
        Amatrix = np.matrix(
            np.vstack([np.ones(n_rows), mags, np.log10(distances), distances])
        ).T
        rhs = np.matrix(np.log10(ppvs))
    return sqa(np.linalg.inv(Amatrix.T * Amatrix) * Amatrix.T * np.matrix(rhs).T)


def predict_mag(ppv, distance, fit, fit_type):
    if fit_type == "simple":
        predicted_mag = (np.log10(ppv) - np.log10(distance) - fit[0]) / fit[1]
    elif fit_type == "geom_only":
        predicted_mag = (np.log10(ppv) - fit[2] * np.log10(distance) - fit[0]) / fit[1]
    elif fit_type == "include_anel":
        predicted_mag = (
            np.log10(ppv) + np.log10(distance) - fit[2] * distance - fit[0]
        ) / fit[1]
    elif fit_type == "geom_and_anel":
        predicted_mag = (
            (fit[0] * np.log10(ppv) + fit[1] * np.log10(distance))
            + fit[2] * distance
            + fit[3]
        )
    return predicted_mag


def linear_fit(B, x):
    return B[0] * x + B[1]


moment = 10 ** (1.5 * mw_spec + 9)
nrg_unscaled = moment * moment * crn_freq * crn_freq * crn_freq

linear_model = Model(linear_fit)
odr_fit_data = Data(np.log10(moment), np.log10(nrg_unscaled))
linear_odr = ODR(odr_fit_data, linear_model, beta0=[0, 0])
linear_output = linear_odr.run()
slope, yint = linear_output.beta

f0, a0 = plt.subplots()
ev_x = np.array([v["east"] for (k, v) in events.items()])
ev_y = np.array([v["north"] for (k, v) in events.items()])
st_x = np.array([v["east"] for (k, v) in stations.items()])
st_y = np.array([v["north"] for (k, v) in stations.items()])


a0.set_aspect("equal")
st_h1, st_h2 = [sqa(x) for x in R * np.matrix([st_x, st_y])]
a0.plot(st_h1, st_h2, "v", color="orange", markeredgecolor="k")
a0.set_xlabel("relative easting (m)")
a0.set_ylabel("relative northing (m)")

# match imaged events
is_stack_matched = np.zeros(len(stack_lines))
for event in events:
    imatch = np.where(abs(stack_serial - UTCDateTime(event).timestamp) < 1)[0]
    if len(imatch) > 0:
        if len(imatch) > 1:
            isort = np.argsort(abs(stack_serial[imatch] - UTCDateTime(event).timestamp))
            imatch = imatch[isort[0]]
        events[event]["stack amplitude"] = float(stack_amp[imatch])
        events[event]["filt amplitude"] = float(filt_amp[imatch])
        is_stack_matched[imatch] = 1


mw_residual, mw_pgv, mw_comp, dist_comp, station_by_resid = [], [], [], [], []
stack_amp2, mw_match_spec, match_stress_drop, match_corner_freq = [], [], [], []
match_energy_index, match_east, match_north = [], [], []
for event in {k: v for (k, v) in events.items() if (v["mw_calc"] == "SPEC")}:
    if "stack amplitude" in events[event].keys():
        stack_amp2.append(events[event]["stack amplitude"])
        mw_match_spec.append(events[event]["mw"])
        match_stress_drop.append(events[event]["stress drop"])
        match_corner_freq.append(events[event]["corner freq"])
        event_moment = 10 ** (1.5 * events[event]["mw"] + 9)
        unscaled_energy = event_moment**2 * match_corner_freq[-1] ** 3
        match_energy_index.append(
            unscaled_energy / (10 ** (slope * np.log10(event_moment) + yint))
        )
        events[event]["energy index"] = match_energy_index[-1]
        match_east.append(events[event]["east"])
        match_north.append(events[event]["north"])
    for station in events[event]["PGV"]:
        distance = np.sqrt(
            (events[event]["east"] - stations[station]["east"]) ** 2
            + (events[event]["north"] - stations[station]["north"]) ** 2
            + (events[event]["depth"] + stations[station]["elev"]) ** 2
        )
        mw_comp.append(events[event]["mw"])
        dist_comp.append(distance)
        station_by_resid.append(station)
        ev_h1, ev_h2 = [
            sqa(x)
            for x in R * np.matrix([events[event]["east"], events[event]["north"]]).T
        ]
        st_h1, st_h2 = [
            sqa(x)
            for x in R
            * np.matrix([stations[station]["east"], stations[station]["north"]]).T
        ]
        a0.plot(ev_h1, ev_h2, "o", color="firebrick", markeredgecolor="k")
        a0.plot([ev_h1, st_h1], [ev_h2, st_h2], zorder=-1, lw=0.5, color="0.3")
a0.set_xlabel("")
a0.set_ylabel("")
a0.xaxis.set_minor_locator(minorLocator)
a0.yaxis.set_minor_locator(minorLocator)
a0.set_xticklabels([])
a0.set_yticklabels([])
a0.grid(True, which="both")
f0.tight_layout()
f0.savefig(figdir + "rotated_event_geometry_69_stations.png")

mw_comp = sqa(mw_comp)
fit = fit_data(pgv_comp, d_hyp_comp, mw_comp, fit_type)
for event in {k: v for (k, v) in events.items() if (v["mw_calc"] == "SPEC")}:
    for station in events[event]["PGV"]:
        distance = np.sqrt(
            (events[event]["east"] - stations[station]["east"]) ** 2
            + (events[event]["north"] - stations[station]["north"]) ** 2
            + (events[event]["depth"] + stations[station]["elev"]) ** 2
        )
        mw_pgv.append(
            predict_mag(events[event]["PGV"][station], distance, fit, fit_type)
        )
        mw_residual.append(events[event]["mw"] - mw_pgv[-1])
mw_residual = sqa(mw_residual)

stack_amp_nospec, mw_match_nospec, pgv_array, mw_array = [], [], [], []
ppv_dataset = {}
for event in {k: v for (k, v) in events.items()}:
    if "stack amplitude" in events[event].keys():
        stack_amp_nospec.append(events[event]["stack amplitude"])
        predicted_mags = []
        for station in events[event]["PGV"]:
            distance = np.sqrt(
                (events[event]["east"] - stations[station]["east"]) ** 2
                + (events[event]["north"] - stations[station]["north"]) ** 2
                + (events[event]["depth"] + stations[station]["elev"]) ** 2
            )
            predicted_mags.append(
                predict_mag(events[event]["PGV"][station], distance, fit, fit_type)
            )
            pgv_array.append(events[event]["PGV"][station])
        mw_match_nospec.append(np.median(predicted_mags))
        for station in events[event]["PGV"]:
            mw_array.append(mw_match_nospec[-1])
        ppv_dataset[event] = {}
        ppv_dataset[event]["mw"] = mw_match_nospec[-1]


av_residual, st_residual, station_x, station_y = [], [], [], []
for station in np.unique(station_by_resid):
    ista = np.where(np.array(station_by_resid) == station)[0]
    av_residual.append(np.average(mw_residual[ista]))
    st_residual.append(np.std(mw_residual[ista]))
    station_x.append(stations[station]["east"])
    station_y.append(stations[station]["north"])

f1, a1 = plt.subplots()
a1.set_aspect("equal")
a1.plot([-0.5, 3.5], [-0.5, 3.5], "k")
a1.plot(mw_comp, mw_pgv, "r.")
a1.set_xlabel("M${_W}$ from spectral fitting")
a1.set_ylabel("M${_W}$ from peak velocity measurements")
a1.set_ylim([-0.5, 3.5])
a1.set_xlim([-0.5, 3.5])
f1.tight_layout()
f1.savefig(figdir + "mag_comparison_69_station.png")
f2 = plt.figure()
a2 = f2.add_subplot(111, projection="polar")
c2 = a2.scatter(
    np.pi - np.array(azimuth_comp),
    abs(mw_residual),
    c=mw_residual,
    vmax=0.5,
    vmin=-0.5,
    cmap="bwr",
    edgecolor="k",
)
a2.set_thetagrids(np.arange(0, 361, 45), ["E", "NE", "N", "NW", "W", "SW", "S", "SE"])
a2.set_rlim([0, 1])
a2.set_rgrids(np.arange(0, 0.81, 0.2), ["", "0.4", "", "0.8"])
cb2 = f2.colorbar(c2)
cb2.set_label("residual mag (obs - pre)")
f2.tight_layout()
f2.savefig(figdir + "residual_rose_plot_69_stations.png")

f3, a3 = plt.subplots()
isort = np.argsort(mw_comp)
c3 = a3.scatter(
    d_hyp_comp[isort], mw_residual[isort], c=mw_comp[isort], edgecolor="0.5"
)
a3.set_xlabel("distance (m)")
a3.set_ylabel("residual mag (obs - pre)")
cb3 = f3.colorbar(c3)
cb3.set_label("M$_W$ from spectra")
f3.tight_layout()
f3.savefig(figdir + "residual_distance_plot_69_stations.png")

f4, a4 = plt.subplots()
a4.set_aspect("equal")
c4 = a4.scatter(
    station_x,
    station_y,
    c=np.array(av_residual),
    vmax=0.2,
    vmin=-0.2,
    cmap="bwr",
    marker="v",
    edgecolor="k",
)
a4.set_xlabel("easting (m)")
a4.set_ylabel("northing (m)")
cb4 = f4.colorbar(c4)
cb4.set_label("average M residual")
f4.tight_layout()
f4.savefig(figdir + "average_station_residual_69_stations.png")

f5, a5 = plt.subplots()
a5.set_aspect("equal")
c5 = a5.scatter(
    station_x,
    station_y,
    c=np.array(st_residual),
    vmin=0.2,
    vmax=0.5,
    cmap="magma_r",
    marker="v",
    edgecolor="k",
)
a5.set_xlabel("easting (m)")
a5.set_ylabel("northing (m)")
cb5 = f5.colorbar(c5)
cb5.set_label("st. dev. M residual")
f5.tight_layout()
f5.savefig(figdir + "stDev_station_residual_69_stations.png")

f6, a6 = plt.subplots()
scatter = a6.scatter(
    stack_amp2,
    mw_match_spec,
    c=np.log10(match_energy_index),
    edgecolor="0.5",
    vmin=-1,
    vmax=1,
    cmap="bwr",
)
a6.plot(stack_amp_nospec, mw_match_nospec, ".", color="0.3", zorder=-1)
cb6 = f6.colorbar(scatter)
cb6.set_label("log$_{10}$(Energy Index)")
a6.set_xlabel("image amplitude")
a6.set_ylabel("magnitude")
a6.set_xscale("log")
plt.show()
