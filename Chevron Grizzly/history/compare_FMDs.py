# -*- coding: utf-8 -*-
"""
Created on Fri Feb 15 14:24:52 2019

@author: adambaig
"""

import numpy as np
import matplotlib.pyplot as plt
from obspy import UTCDateTime

f = open("Initial_9_station_processing.csv")
line = f.readline()
lines9 = f.readlines()
f.close()

f = open("NLLoc_WithMag_BA69lonlat_timestamp_wFWB_recalcMag.csv")
line = f.readline()
linesFWB = f.readlines()
f.close()

f = open("Chevron_withXC_utm_updated_M.csv")
line = f.readline()
linesXC = f.readlines()
f.close()

mw_9 = []
for line in lines9:
    lspl = line.split(",")
    mw_9.append(float(lspl[4]))

t0 = UTCDateTime(2016, 11, 18)
mw_FWB, ts_FWB = [], []
for line in linesFWB:
    lspl = line.split(",")
    ts_FWB.append(UTCDateTime(float(lspl[5])) - t0)

mw_XC, ts_XC = [], []
for line in linesXC:
    lspl = line.split(",")
    mw_XC.append(float(lspl[5]))
    ts_XC.append(UTCDateTime(lspl[1]) - t0)
    i_match = np.where(abs(np.array(ts_FWB) - ts_XC[-1]) < 1)[0]
    if len(i_match) > 0:
        mw_FWB.append(float(lspl[5]))

bins = np.arange(-2, 2.5, 0.1)

fig, ax = plt.subplots()
h_9, bins_9, p_9 = ax.hist(
    mw_9, bins, facecolor="forestgreen", edgecolor="k", width=0.08, zorder=2, alpha=0.6
)
h_FWB, bins_FWB, p_FWB = ax.hist(
    mw_FWB, bins, facecolor="royalblue", edgecolor="k", width=0.08, zorder=1, alpha=0.6
)
h_XC, bins_XC, p_XC = ax.hist(
    mw_XC, bins, facecolor="goldenrod", edgecolor="k", width=0.08, zorder=0, alpha=0.6
)
ax.set_yscale("log")
ax.legend(
    [p_9[0], p_FWB[0], p_XC[0]],
    ["original 9 station dataset", "FWB dataset", "Imaged dataset"],
)
ax.set_xlabel("magnitude")
ax.set_ylabel("number of events")
