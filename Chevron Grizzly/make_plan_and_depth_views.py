fig_events, (a_plan, a_depth) = plt.subplots(1, 2, figsize=[16, 8])
a_plan.plot(
    [v["easting"] for v in frac_events.values()],
    [v["northing"] for v in frac_events.values()],
    "o",
    color="firebrick",
    markeredgecolor="0.2",
    markeredgewidth=0.25,
)
a_plan.plot(
    [v["easting"] for v in fault_events.values()],
    [v["northing"] for v in fault_events.values()],
    "o",
    color="firebrick",
    markeredgecolor="0.2",
    markeredgewidth=0.25,
)
