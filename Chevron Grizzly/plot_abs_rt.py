import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from read_inputs import read_treatment, read_events, read_wells
from datetime import datetime
from obspy import UTCDateTime
from sms_moment_tensor.MT_math import decompose_MT

if not ("treatment_data" in globals()):
    treatment_data = read_treatment()
events = read_events(filename="flipped_frac_events.csv")
event_wells = np.unique([v["well"] for k, v in events.items()])
event_wells = ["A", "B"]
wells = read_wells()
frac_angle = 90 * np.pi / 180.0

custom_cmap = mcolors.ListedColormap(
    np.vstack(
        [
            mcolors.to_rgba("firebrick"),
            mcolors.to_rgba("darkgoldenrod"),
            mcolors.to_rgba("royalblue"),
        ]
    )
)


def sqa(x):
    return np.squeeze(np.array(x))


R = np.matrix(
    [
        [np.cos(frac_angle), np.sin(frac_angle)],
        [-np.sin(frac_angle), np.cos(frac_angle)],
    ]
)


def array_from_dict(dictionary, value):
    return np.array([v[value] for k, v in dictionary.items()])


def array_from_utc_keys(dictionary):
    return np.array([UTCDateTime(t).timestamp for t in [k for k in dictionary.keys()]])


east_ref = 484550.0
north_ref = 6020267.03

for event in events:
    decomp = decompose_MT(events[event]["dc MT"])
    if decomp["p_plunge"] > 45:
        if np.tan(np.pi * decomp["b_trend"] / 180) > 0:
            events[event]["mt cluster"] = 1
        else:
            events[event]["mt cluster"] = 2
    else:
        events[event]["mt cluster"] = 0

event_wells
well = "B"
stage = "3"
# for well in event_wells:
for well in ["B"]:
    well_events = {k: v for k, v in events.items() if v["well"] == well}
    #    for stage in np.unique([v["stage"] for k, v in well_events.items()]):
    # shift time to relative accounting for mountain time to utc (7 hours)
    for stage in ["3"]:
        times = [
            UTCDateTime(t).timestamp
            for t in [
                k
                for k, v in treatment_data.items()
                if v["well"] == well and v["stage"] == stage
            ]
        ]
        time_sort = np.argsort(times)
        eng_times = np.array(times)[time_sort]
        pressure = np.array(
            [
                v["pressure"]
                for k, v in treatment_data.items()
                if v["well"] == well and v["stage"] == stage
            ]
        )[time_sort]
        slurry_rate = np.array(
            [
                v["slurry_rate"]
                for k, v in treatment_data.items()
                if v["well"] == well and v["stage"] == stage
            ]
        )[time_sort]

        proppant_conc = np.array(
            [
                v["proppant_conc"]
                for k, v in treatment_data.items()
                if v["well"] == well and v["stage"] == stage
            ]
        )[time_sort]
        t0 = eng_times[0]
        tlast = eng_times[-1] - t0
        if well == "B" and stage == "10":
            tlast = UTCDateTime(2016, 11, 22, 17, 00).timestamp - t0
        if stage in ["12-Part1", "4-Part1", "4-Part2"]:
            stage_name = "Stage " + stage.split("-")[0]
        else:
            stage_name = "Stage " + stage
        stage_events = {k: v for k, v in well_events.items() if v["stage"] == stage}
        north0 = 0.5 * (
            np.average(
                [v["bottom_north"] for (k, v) in wells[well][stage_name].items()]
            )
            + np.average([v["top_north"] for (k, v) in wells[well][stage_name].items()])
        )
        east0 = 0.5 * (
            np.average([v["bottom_east"] for (k, v) in wells[well][stage_name].items()])
            + np.average([v["top_east"] for (k, v) in wells[well][stage_name].items()])
        )
        tvdss0 = 0.5 * (
            np.average(
                [v["bottom_tvdss"] for (k, v) in wells[well][stage_name].items()]
            )
            + np.average([v["top_tvdss"] for (k, v) in wells[well][stage_name].items()])
        )
        h1, h2 = [
            sqa(a)
            for a in R
            * np.matrix(
                np.vstack(
                    [
                        array_from_dict(stage_events, "easting") + east_ref - east0,
                        array_from_dict(stage_events, "northing") + north_ref - north0,
                    ]
                )
            )
        ]
        rel_depth = array_from_dict(stage_events, "depth") - tvdss0
        # h1 = array_from_dict(stage_events, "easting") + east_ref - east0
        # h2 = array_from_dict(stage_events, "northing") + north_ref - north0
        fig = plt.figure(figsize=[8, 6])
        fig.tight_layout()
        a1 = fig.add_axes([0.2, 0.52, 0.6, 0.33])
        a1.set_ylabel("distance\nperp. to well (m)")
        a1.set_title("Well " + well + ": Stage " + stage)
        isort = np.argsort(
            array_from_dict(stage_events, "magnitude")
        )  # plot high mag events on top
        scatterplot = a1.scatter(
            array_from_utc_keys(stage_events)[isort] - t0 - 7 * 3600,
            abs(h2[isort]),
            c=array_from_dict(stage_events, "mt cluster")[isort],
            vmax=2,
            vmin=0,
            cmap=custom_cmap,
            edgecolor="0.2",
            zorder=10,
        )

        x1, x2 = a1.get_xlim()
        y1, y2 = a1.get_ylim()
        t_init = times[np.where(slurry_rate > 0)[0][0]]

        event_times = array_from_utc_keys(stage_events) - t_init - 7 * 3600
        diffusivities = h1 * h1 / event_times / 4 / np.pi
        vrt_diffusivities = rel_depth * rel_depth / event_times / 4 / np.pi
        diff = np.percentile(diffusivities, 95)
        vrt_diff = np.percentile(vrt_diffusivities, 95)
        envelope_times = (eng_times - t_init)[np.where(eng_times >= t_init)]
        a1.plot(
            envelope_times + t_init - t0, np.sqrt(4 * np.pi * diff * envelope_times)
        )
        a1.set_xlim([x1, x2])
        a1.set_ylim([y1, y2])
        UTCDateTime(eng_times[944])
        xticks = np.arange(3600 - t0 % 3600, tlast, 3600)

        a1.set_xticks(xticks)
        a1.set_xlim([0, tlast])
        a1.set_xticklabels([])
        a1.set_facecolor("0.95")
        a2 = fig.add_axes([0.2, 0.15, 0.6, 0.33])
        a2.plot(eng_times - t0, pressure / 120.0, "forestgreen")
        a2.set_ylabel("pressure (MPa)", color="forestgreen")
        a2.set_ylim([0, 1])
        a2.set_xlim([0, tlast])
        a2.set_xticks(xticks)
        a2.hist(
            array_from_utc_keys(stage_events)[isort] - t0 - 7 * 3600,
            bins=np.arange(0, tlast, 300),
            zorder=-4,
            weights=np.ones(len(stage_events)) / 20,
        )
        xticklabels = []
        for tick in xticks:
            xticklabels.append(
                datetime.strftime(UTCDateTime(t0 + tick)._get_datetime(), "%b %d\n%H%M")
            )
        a2.set_xticklabels(xticklabels)
        a2.set_facecolor("0.95")
        a2.set_yticks(np.arange(0, 7.0) / 6)
        a2.set_xlabel("Mountain Standard Time")
        a2.set_yticklabels(["0", "20", "40", "60", "80", "100", "120"])
        a3 = a2.twinx()
        a3.plot(eng_times - t0, slurry_rate / 15.0, "firebrick")
        a3.set_ylabel("slurry rate (m${^3}$/min)", color="firebrick")
        a3.set_ylim([0, 1])
        a3.set_yticks(np.arange(0, 7.0) / 6)
        a3.set_yticklabels(["0", "25", "50", "75", "100", "125", "150"])
        a2.plot(eng_times - t0, proppant_conc / 500.0, "darkgoldenrod")
        a4 = fig.add_axes([0.9, 0.15, 0, 0.33])
        a4.yaxis.set_ticks_position("right")
        a4.yaxis.set_label_position("right")
        a4.set_yticks(np.arange(0, 6.0) / 5)
        a4.set_yticklabels(["0", "100", "200", "300", "400", "500"])
        a4.set_ylabel("proppant conc. (kg/m$^{3}$)", color="darkgoldenrod")
        a4.set_xticks([])

        a5 = fig.add_axes([0.85, 0.52, 0.03, 0.33])
        cb = fig.colorbar(scatterplot, cax=a5)

        cb.set_label("cluster")
        cb.set_ticks([0.3333, 1, 1.6667])
        cb.set_ticklabels(["SS", "N1", "N2"])
        cb.ax.invert_yaxis()
        a7 = fig.add_axes([0.1, 0.15, 0, 0.33])
        a7.set_ylabel("event rate", color="royalblue")
        a7.set_xticks([])
        a7.set_yticks(np.arange(5.0) / 4.0)
        a7.set_yticklabels(["0", "5", "10", "15", "20"])

plt.hist(h1 * h1 / event_times / 4 / np.pi)


plt.plot(h1 * h1, event_times, ".")

plt.show()
