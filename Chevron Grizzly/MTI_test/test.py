import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np

# import image_stream_1D
# import image_stream_1D_func
import time, os, sys

# import rsf.api as rsf
# import m8r as sf


from sms_moment_tensor.MT_math import sdr_to_mt, conjugate_sdr
from obspy.imaging.mopad_wrapper import beach
from sms_ray_modelling.raytrace import isotropic_ray_trace
import glob
import mplstereonet as mpls
from matplotlib import cm

bwr = cm.get_cmap("bwr")


vel = get_velocity_model()
events = read_events()
id = 10450
event = events[id]
stationsPFI = r"C:\Users\adambaig\Project\Chevron Grizzly\MTI_test\vertical_channel_coordinates04.csv"
f = open(stationsPFI)
lines = f.readlines()
f.close()
rec = []
for line in lines:
    easting, northing, tvdss = [float(s) for s in line.split(",")]
    rec.append({"e": easting, "n": northing, "z": -1 * tvdss})

mt_dc = sdr_to_mt(event["strike"], event["dip"], event["rake"])
mt_dc_vec = [
    mt_dc[0, 0],
    mt_dc[1, 1],
    mt_dc[2, 2],
    mt_dc[0, 1],
    mt_dc[0, 2],
    mt_dc[1, 2],
] / np.amax(abs(mt_dc))

beachball_me_dc = beach(
    mt_dc_vec,
    xy=(1, 1),
    zorder=2,
    width=1,
    mopad_basis="XYZ",
    linewidth=0.5,
    facecolor="r",
)

fig3, ax4 = plt.subplots()
ax4.set_aspect("equal")
src = {"e": event["easting"], "n": event["northing"], "z": event["elevation"]}
f1, a1 = mpls.subplots()
# ax4.scatter(ox, oy, c="r", alpha=1, s=10)
beachball_me_dc = beach(
    mt_dc_vec,
    xy=(1, 1),
    zorder=2,
    width=1,
    mopad_basis="XYZ",
    linewidth=0.5,
    facecolor="r",
)
ax4.add_collection(beachball_me_dc)
ax4.set_xlim([0, 2])
ax4.set_ylim([0, 2])

fig5, ax5 = plt.subplots()
ax5.set_aspect("equal")
src = {"e": event["easting"], "n": event["northing"], "z": event["elevation"]}

beachball_me_dc = beach(
    (event["strike"], event["dip"], event["rake"]),
    xy=(1, 1),
    zorder=2,
    width=1,
    linewidth=0.5,
    facecolor="r",
)
ax5.add_collection(beachball_me_dc)
ax5.set_xlim([0, 2])
ax5.set_ylim([0, 2])


conjugate = conjugate_sdr(event["strike"], event["dip"], event["rake"])

for jj, rec1 in enumerate(rec):
    p_raypath = isotropic_ray_trace(src, rec1, vel, "p")
    plunge = (
        180
        * np.arccos(
            np.sqrt(
                p_raypath["hrz_slowness"]["e"] ** 2
                + p_raypath["hrz_slowness"]["n"] ** 2
            )
            * p_raypath["velocity_model_chunk"][0]["v"]
        )
        / np.pi
    )

    trend = (
        180
        * np.arctan2(p_raypath["hrz_slowness"]["e"], p_raypath["hrz_slowness"]["n"])
        / np.pi
    )
    print(plunge, trend)
    a1.line(plunge, trend, "o", markeredgecolor="k", color=bwr(0.5))
a1.plane(event["strike"], event["dip"], "0.4")
a1.plane(conjugate[0], conjugate[1], "0.4")
plt.title(str(id) + " " + str(round(event["Mw"], 2)))
# plt.savefig(stplotdir + "/%s.jpg" % (id))
# plt.close()
plt.show()


def get_velocity_model():
    return [
        {"rho": 2180.0, "vp": 2446.0, "vs": 1069.0},
        {"rho": 2324.0, "vp": 3163.0, "vs": 1513.0, "top": 460.0},
        {"rho": 2421.0, "vp": 3726.0, "vs": 1862.0, "top": -20.0},
        {"rho": 2451.0, "vp": 3911.0, "vs": 2046.0, "top": -220.0},
        {"rho": 2495.0, "vp": 4201.0, "vs": 2301.0, "top": -1140.0},
        {"rho": 2734.0, "vp": 6052.0, "vs": 3303.0, "top": -1480.0},
        {"rho": 2686.0, "vp": 5637.0, "vs": 3027.0, "top": -1720.0},
        {"rho": 2749.0, "vp": 6186.0, "vs": 3275.0, "top": -1920.0},
        {"rho": 2494.0, "vp": 5190.0, "vs": 2288.0, "top": -2320.0},
        {"rho": 2614.0, "vp": 5063.0, "vs": 2687.0, "top": -2560.0},
    ]


def read_events():
    f = open("Chevron_pfi_mts_20-60Hz.csv")
    line = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        id = int(lspl[0])
        events[id] = {
            "Mw": float(lspl[1]),
            "stack amp": float(lspl[2]),
            "easting": float(lspl[3]),
            "northing": float(lspl[4]),
            "elevation": -float(lspl[5]),
            "strike": float(lspl[6]),
            "dip": float(lspl[7]),
            "rake": float(lspl[8]),
            "polarity": float(lspl[9]),
        }
    return events
