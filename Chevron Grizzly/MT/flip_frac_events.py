import matplotlib

matplotlib.use("Qt5agg")

from datetime import datetime
import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
from obspy.imaging.mopad_wrapper import beach
import sys
from sklearn.cluster import AgglomerativeClustering
from sms_moment_tensor.MT_math import (
    decompose_MT,
    mt_to_sdr,
    mt_matrix_to_vector,
    general_to_DC,
)

sys.path.append("..")
from read_inputs import read_events, read_wells

PI = np.pi

events = read_events()
for event in {k: v for k, v in events.items() if "dc MT" in v}:
    strike, dip, rake = mt_to_sdr(events[event]["dc MT"], conjugate=False)
    decomp = decompose_MT(events[event]["dc MT"])
    events[event]["p_trend"] = decomp["p_trend"]
    events[event]["b_trend"] = decomp["b_trend"]
    events[event]["t_trend"] = decomp["t_trend"]
    events[event]["p_plunge"] = decomp["p_plunge"]
    events[event]["b_plunge"] = decomp["b_plunge"]
    events[event]["t_plunge"] = decomp["t_plunge"]
    events[event]["strike"] = strike
    events[event]["dip"] = dip
    events[event]["rake"] = rake


#
# strain_fig, (p_ax, b_ax, t_ax) = mpls.subplots(1, 3, figsize=[16, 6])
mt_events = {k: v for k, v in events.items() if "dc MT" in v}
fault_events = {
    k: v
    for (k, v) in mt_events.items()
    if (v["cluster"] == 1 and v["mt confidence"] > 0.9)
}

frac_events = {
    k: v
    for (k, v) in mt_events.items()
    if (v["cluster"] == 0 and v["mt confidence"] > 0.9)
}

avg_frac_mt = np.zeros([3, 3])
for id_event, event in frac_events.items():
    avg_frac_mt += event["dc MT"] / np.linalg.norm(event["dc MT"])

avg_frac_mt /= len(frac_events)

for id_event, event in frac_events.items():
    if np.tensordot(avg_frac_mt, event["dc MT"]) < 0:
        event["flip dc"] = event["dc MT"]
        event["flip gen"] = event["general MT"]
    else:
        event["flip dc"] = -event["dc MT"]
        event["flip gen"] = -event["general MT"]

for event in {k: v for k, v in frac_events.items()}:
    strike, dip, rake = mt_to_sdr(events[event]["dc MT"], conjugate=False)
    decomp = decompose_MT(events[event]["flip dc"])
    frac_events[event]["p_trend"] = decomp["p_trend"]
    frac_events[event]["b_trend"] = decomp["b_trend"]
    frac_events[event]["t_trend"] = decomp["t_trend"]
    frac_events[event]["p_plunge"] = decomp["p_plunge"]
    frac_events[event]["b_plunge"] = decomp["b_plunge"]
    frac_events[event]["t_plunge"] = decomp["t_plunge"]
    frac_events[event]["strike"] = strike
    frac_events[event]["dip"] = dip
    frac_events[event]["rake"] = rake


f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
p_plunge = np.array([v["p_plunge"] for k, v in frac_events.items()])
p_trend = np.array([v["p_trend"] for k, v in frac_events.items()])
b_plunge = np.array([v["b_plunge"] for k, v in frac_events.items()])
b_trend = np.array([v["b_trend"] for k, v in frac_events.items()])
t_plunge = np.array([v["t_plunge"] for k, v in frac_events.items()])
t_trend = np.array([v["t_trend"] for k, v in frac_events.items()])
cax_p = a3[0].density_contourf(
    p_plunge, p_trend, measurement="lines", alpha=0.5, cmap="Greens"
)
cax_b = a3[1].density_contourf(
    b_plunge, b_trend, measurement="lines", alpha=0.5, cmap="Greens"
)
cax_t = a3[2].density_contourf(
    t_plunge, t_trend, measurement="lines", alpha=0.5, cmap="Greens"
)
a3[0].line(p_plunge, p_trend, "k.", alpha=0.1)
a3[1].line(b_plunge, b_trend, "k.", alpha=0.1)
a3[2].line(t_plunge, t_trend, "k.", alpha=0.1)
a3[0].set_title("P axes\n\n")
a3[1].set_title("B axes\n\n")
a3[2].set_title("T axes\n\n")
a3[0].grid()
a3[1].grid()
a3[2].grid()

#
# n_mt = len(frac_events)
# dot_product_pairs = np.zeros([n_mt, n_mt])
# for i1, (id1, mt1) in enumerate(frac_events.items()):
#     for i2, (id2, mt2) in enumerate(frac_events.items()):
#         dc1_norm = mt1["flip dc"] / np.linalg.norm(mt1["flip dc"])
#         dc2_norm = mt2["flip dc"] / np.linalg.norm(mt2["flip dc"])
#         dot_product_pairs[i1, i2] = np.tensordot(dc1_norm, dc2_norm)
# distance_mat = abs(1 - dot_product_pairs)

n_clusters = 3
for event_id, mt in frac_events.items():
    decomp = decompose_MT(mt["flip dc"])
    if decomp["p_plunge"] < 45:
        if np.tan(np.pi * decomp["b_trend"] / 180) > 0:
            frac_events[event_id]["label"] = 1
        else:
            frac_events[event_id]["label"] = 2
    else:
        frac_events[event_id]["label"] = 0


# clustering = AgglomerativeClustering(
#     n_clusters=n_clusters, affinity="precomputed", linkage="complete"
# ).fit(distance_mat)
# labels = clustering.labels_
# for i_mt, (id, mt) in enumerate(frac_events.items()):
#     frac_events[id]["label"] = labels[i_mt]

colors = [
    "firebrick",
    "darkgoldenrod",
    "royalblue",
    "indigo",
    "bisque",
    "orange",
]
wells = read_wells()
f1, (a1, a2) = plt.subplots(1, 2)
a1.set_aspect("equal")
a1.set_facecolor("0.8")
for wellname, well in wells.items():
    a1.plot(well["easting"], well["northing"], "0.8", zorder=3, lw=4)
    a1.plot(well["easting"], well["northing"], "0.3", zorder=3, lw=2)
patches = []
event
for i_cluster in range(n_clusters):
    mt_cluster = {
        k: v
        for k, v in frac_events.items()
        if v["label"] == i_cluster and v["well"] != "D"
    }
    mt_avg = sum(
        [v["flip dc"] / np.linalg.norm(v["flip dc"]) for k, v in mt_cluster.items()]
    )
    p_trend, p_plunge, b_trend, b_plunge, t_trend, t_plunge = (
        [],
        [],
        [],
        [],
        [],
        [],
    )
    print("cluster " + str(i_cluster) + ": " + str(len(mt_cluster)))
    for id, mt in mt_cluster.items():
        decomp = decompose_MT(frac_events[id]["flip dc"])
        p_trend.append(decomp["p_trend"])
        b_trend.append(decomp["b_trend"])
        t_trend.append(decomp["t_trend"])
        p_plunge.append(decomp["p_plunge"])
        b_plunge.append(decomp["b_plunge"])
        t_plunge.append(decomp["t_plunge"])

    f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = a3[0].density_contourf(
        p_plunge, p_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    cax_b = a3[1].density_contourf(
        b_plunge, b_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    cax_t = a3[2].density_contourf(
        t_plunge, t_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    a3[0].set_title("P axes\n\n")
    a3[1].set_title("B axes\n\n")
    a3[2].set_title("T axes\n\n")
    a3[0].grid()
    a3[1].grid()
    a3[2].grid()
    a3[0].set_ylabel("cluster %i\n\n" % i_cluster)
    east, north = np.array(
        [(v["easting"], v["northing"]) for k, v in mt_cluster.items()]
    ).T
    (patch,) = a1.plot(
        east + 484550.0,
        north + 6020267.03,
        "o",
        color=colors[i_cluster],
        zorder=5,
        linewidth=0.25,
        markeredgecolor="k",
    )
    patches.append(patch)
    avg_mt = sum([v["flip dc"] for k, v in mt_cluster.items()])
    avg_mt /= np.linalg.norm(avg_mt)
    avg_dc = general_to_DC(avg_mt)

    f_bb, a_bb = plt.subplots()
    beachball = beach(
        mt_matrix_to_vector(avg_dc),
        xy=(0.5, 0.5),
        width=0.96,
        facecolor=colors[i_cluster],
        mopad_basis="XYZ",
        linewidth=0.25,
    )
    a_bb.set_aspect("equal")
    a_bb.add_collection(beachball)
    a_bb.set_axis_off()


avg_fault = sum([v["dc MT"] for k, v in fault_events.items()])
avg_fault /= np.linalg.norm(avg_fault)
avg_dc = general_to_DC(avg_fault)

a2.legend(
    patches,
    [("Cluster %i" % s) for s in range(1, n_clusters + 1)],
    numpoints=1,
)
a1.set_xlabel("easting (m)")
a1.set_ylabel("northing (m)")
a1.set_xlim([483000, 486000])
a1.set_ylim([6021000, 6023500])
a1.grid(which="both")

rakes = []
for event_id, event in frac_events.items():
    sdrs = mt_to_sdr(event["flip dc"])
    rakes.append(sdrs[2])
    rakes.append(sdrs[5])

f5 = plt.figure()
a5 = f5.add_subplot(111, projection="polar")
rakes = np.mod(np.array(rakes), 360)
bin_number = 31
bins = np.linspace(0, 2 * np.pi, bin_number)
n_rake, _, _ = plt.hist(rakes * np.pi / 180.0, bins, visible=False)
width = 2 * np.pi / bin_number
bars = a5.bar(bins[:-1], n_rake, width=width, facecolor="royalblue")
lines, labels = plt.thetagrids(
    [0, 90, 180, 270],
    [
        "            Left-lateral\n            strike-slip",
        "Thrust",
        "Right-lateral            \nstrike-slip           ",
        "Normal",
    ],
)
a5.set_title("Rakes of both fault planes\n\n")
plt.show()

if False:
    f = open("flipped_frac_events.csv", "w")
    f.write("id, timestamp, easting, northing, depth, magnitude, stack amp,")
    f.write("filt amp, xc value, master, isShallow, cluster, well, stage,")
    f.write("n picks,mt11, mt22, mt33, mt12, mt13, mt23, ")
    f.write("dc11, dc22, dc33, dc12, dc13, dc23, ")
    f.write("gen condition number, dev condition number,")
    f.write("gen r,dc r, mt confidence\n")
    for event_id, event in frac_events.items():
        f.write(event["id"] + "," + event_id + ",")
        f.write(
            "%.2f, %.2f, %.2f,"
            % (
                event["easting"] + 484550.0,
                event["northing"] + 6020267.03,
                event["depth"] + 910,
            )
        )
        f.write(
            "%.5f, %.8e, %.8e, %.5f,%i,"
            % (
                event["magnitude"],
                event["stack_amp"],
                event["filt_amp"],
                event["xc_value"],
                event["master"],
            )
        )
        f.write(event["isShallow"])
        f.write("," + str(event["cluster"]) + ",")
        f.write(event["well"] + "," + event["stage"] + ",%i," % event["n picks"])
        mt_out = mt_matrix_to_vector(event["flip dc"])
        gn_out = mt_matrix_to_vector(event["flip gen"])
        f.write(
            6
            * "%.8e,"
            % (
                gn_out[0],
                gn_out[1],
                gn_out[2],
                gn_out[3],
                gn_out[4],
                gn_out[5],
            )
        )
        f.write(
            6
            * "%.8e,"
            % (
                mt_out[0],
                mt_out[1],
                mt_out[2],
                mt_out[3],
                mt_out[4],
                mt_out[5],
            )
        )

        f.write(
            "%.2f,%.2f,%.4f,%.4f,%.4f\n"
            % (
                event["general condition number"],
                event["dc condition number"],
                event["dc r"],
                event["general r"],
                event["mt confidence"],
            )
        )
    f.close()
