import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
from numpy import sin, cos, arccos, arcsin, arctan, arctan2
import matplotlib.pyplot as plt
import mplstereonet as mpls

from stress_inversions import (
    Michael_stress_inversion,
    stereonet_plot_stress_tensor,
)


n_iterations = 1000
friction_angle = 0.6
PI = np.pi
f = open(r"Catalogs\events_Sep_11_2019.csv")
head = f.readline()
lines = f.readlines()
f.close()
line = lines[0]
head.split(",")


def point_down(vec):
    if vec[2] < 0:
        return -vec
    else:
        return vec


def sqa(x):
    return np.squeeze(np.array(x))


def unit_vector_2_trend_plunge(u):
    if u[2] < 0:
        u = -u
    trend = arctan2(u[1], u[0]) * 180.0 / PI
    plunge = arctan(u[2] / np.sqrt(u[0] * u[0] + u[1] * u[1])) * 180.0 / PI
    return [trend, plunge]


def decompose_stress(stress_tensor):
    eigvals, eigvecs = np.linalg.eig(stress_tensor)
    iSort = np.argsort(eigvals)
    s3_axis = eigvecs[:, iSort[0]]
    s2_axis = eigvecs[:, iSort[1]]
    s1_axis = eigvecs[:, iSort[2]]
    R = (eigvals[iSort[2]] - eigvals[iSort[1]]) / (
        eigvals[iSort[2]] - eigvals[iSort[0]]
    )
    return (R, np.matrix(np.hstack([s1_axis, s2_axis, s3_axis])))


event_lat, event_lon, depth, strike, dip, rake = [], [], [], [], [], []
for line in lines:
    lspl = line.split(",")
    if lspl[-2] != "":
        event_lat.append(float(lspl[6]))
        event_lon.append(float(lspl[7]))
        depth.append(float(lspl[12]))
        dip.append(float(lspl[-4]))
        strike.append(float(lspl[-3]))
        rake.append(float(lspl[-2]))

strike = np.array(strike)
dip = np.array(dip)
rake = np.array(rake)
sqrt2 = np.sqrt(2)
normal1, slip1, normal2, slip2, paxis, taxis, baxis = (
    np.zeros([3, len(strike)]),
    np.zeros([3, len(strike)]),
    np.zeros([3, len(strike)]),
    np.zeros([3, len(strike)]),
    np.zeros([3, len(strike)]),
    np.zeros([3, len(strike)]),
    np.zeros([3, len(strike)]),
)
p_trend, p_plunge, b_trend, b_plunge, t_trend, t_plunge, strike2, dip2, rake2 = (
    np.zeros(len(strike)),
    np.zeros(len(strike)),
    np.zeros(len(strike)),
    np.zeros(len(strike)),
    np.zeros(len(strike)),
    np.zeros(len(strike)),
    np.zeros(len(strike)),
    np.zeros(len(strike)),
    np.zeros(len(strike)),
)


for ii in range(len(strike)):
    st = strike[ii] * PI / 180.0
    dp = dip[ii] * PI / 180.0
    rk = rake[ii] * PI / 180.0
    normal1[:, ii] = [-sin(st) * sin(dp), cos(st) * sin(dp), -cos(dp)]
    slip1[:, ii] = [
        cos(st) * cos(rk) + sin(st) * cos(dp) * sin(rk),
        sin(st) * cos(rk) - cos(st) * cos(dp) * sin(rk),
        -sin(dp) * sin(rk),
    ]
    normal2[:, ii] = slip1[:, ii]
    slip2[:, ii] = normal1[:, ii]
    if normal2[2, ii] > 0:
        normal2[:, ii] *= -1
        slip2[:, ii] *= -1
    if normal2[2, ii] == -1:
        dip2[ii] = 0
        strike2[ii] = 0
        rake2[ii] = arctan2(-slip2[1, ii] / slip2[0, ii])
    else:
        dip2[ii] = arccos(-normal2[2, ii])
        strike2[ii] = arctan2(-normal2[0, ii], normal2[1, ii])
        rake2[ii] = arctan2(
            -slip2[2, ii] / sin(dip2[ii]),
            slip2[0, ii] * cos(strike2[ii]) + slip2[1, ii] * strike2[ii],
        )
    paxis[:, ii] = point_down(normal1[:, ii] - slip1[:, ii]) / sqrt2
    taxis[:, ii] = point_down(normal1[:, ii] + slip1[:, ii]) / sqrt2
    baxis[:, ii] = point_down(np.cross(normal1[:, ii], slip1[:, ii]))
    p_trend[ii] = arctan2(paxis[1, ii], paxis[0, ii])
    b_trend[ii] = arctan2(baxis[1, ii], baxis[0, ii])
    t_trend[ii] = arctan2(taxis[1, ii], taxis[0, ii])
    p_plunge[ii] = arcsin(paxis[2, ii])
    b_plunge[ii] = arcsin(baxis[2, ii])
    t_plunge[ii] = arcsin(taxis[2, ii])

p_trend *= 180.0 / PI
b_trend *= 180.0 / PI
t_trend *= 180.0 / PI
p_plunge *= 180.0 / PI
b_plunge *= 180.0 / PI
t_plunge *= 180.0 / PI
strike2 *= 180.0 / PI
dip2 *= 180.0 / PI
rake2 *= 180.0 / PI


g = open("strain.csv", "w")
g.write("P trend,P plunge,B trend,B plunge,T trend,T plunge\n")
for ii in range(len(strike)):
    g.write(
        "%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n"
        % (
            np.mod(p_trend[ii], 360),
            p_plunge[ii],
            np.mod(b_trend[ii], 360),
            b_plunge[ii],
            np.mod(t_trend[ii], 360),
            t_plunge[ii],
        )
    )
g.close()


fig, ax = mpls.subplots()
ax.grid()
pp = ax.line(p_plunge, p_trend, "o", color="firebrick", markeredgecolor="k")
pt = ax.line(t_plunge, t_trend, "o", color="royalblue", markeredgecolor="k")
pb = ax.line(b_plunge, b_trend, "o", color="forestgreen", markeredgecolor="k")

f3, a3 = mpls.subplots()
a3.grid()

stress_tensor = np.matrix(np.zeros([3, 3]))

f5, a5 = mpls.subplots()
a5.grid()
R_iteration = []
for i_iteration in range(n_iterations):
    selected_strike, selected_dip, selected_rake = (
        np.zeros(len(strike)),
        np.zeros(len(strike)),
        np.zeros(len(strike)),
    )
    unselected_strike, unselected_dip, unselected_rake = (
        np.zeros(len(strike)),
        np.zeros(len(strike)),
        np.zeros(len(strike)),
    )
    for ii in range(len(strike)):
        test = np.random.rand()
        if test < 0.5:
            selected_strike[ii] = strike[ii]
            selected_dip[ii] = dip[ii]
            selected_rake[ii] = rake[ii]
        else:
            selected_strike[ii] = strike2[ii]
            selected_dip[ii] = dip2[ii]
            selected_rake[ii] = rake2[ii]
    iteration_stress_tensor = Michael_stress_inversion(
        selected_strike, selected_dip, selected_rake
    )
    eigvals, eigvecs = np.linalg.eig(iteration_stress_tensor)
    iSort = np.argsort(eigvals)
    s3_axis = eigvecs[:, iSort[0]]
    s2_axis = eigvecs[:, iSort[1]]
    s1_axis = eigvecs[:, iSort[2]]
    R_iteration.append(
        (eigvals[iSort[2]] - eigvals[iSort[1]])
        / (eigvals[iSort[2]] - eigvals[iSort[0]])
    )
    stress_axes = np.matrix(np.hstack([s1_axis, s2_axis, s3_axis]))
    s1_trend, s1_plunge = unit_vector_2_trend_plunge(sqa(s1_axis))
    s2_trend, s2_plunge = unit_vector_2_trend_plunge(sqa(s2_axis))
    s3_trend, s3_plunge = unit_vector_2_trend_plunge(sqa(s3_axis))
    a3.line(s1_plunge, s1_trend, ".", ms=1, zorder=-1, color="firebrick")
    a3.line(s2_plunge, s2_trend, ".", ms=1, zorder=-1, color="forestgreen")
    a3.line(s3_plunge, s3_trend, ".", ms=1, zorder=-1, color="royalblue")
    stress_tensor += iteration_stress_tensor

R, stress_axes = decompose_stress(stress_tensor)


s1_axis = stress_axes[:, 0]
s2_axis = stress_axes[:, 1]
s3_axis = stress_axes[:, 2]

tau, sigma = np.zeros([2, len(strike)]), np.zeros([2, len(strike)])
instability = np.zeros([2, len(strike)])
for ii in range(len(strike)):
    for jj, n in enumerate([normal1[:, ii], normal2[:, ii]]):
        n1, n2, n3 = sqa(n * stress_axes)
        sign_tau = np.sign(n3)
        sigma[jj, ii] = n1 * n1 + (1.0 - 2.0 * R) * n2 * n2 - n3 * n3
        tau[jj, ii] = sign_tau * np.sqrt(
            1.0 + 4.0 * R * (R - 1.0) * n2 * n2 - sigma[jj, ii] * sigma[jj, ii]
        )
        instability[jj, ii] = (
            abs(tau[jj, ii]) - friction_angle * (sigma[jj, ii] - 1.0)
        ) / (friction_angle + np.sqrt(1.0 + friction_angle * friction_angle))

selected_sigma, selected_tau = np.zeros(len(strike)), np.zeros(len(strike))
unselected_sigma, unselected_tau = np.zeros(len(strike)), np.zeros(len(strike))
for ii in range(len(strike)):
    if instability[0, ii] > instability[1, ii]:
        selected_strike[ii] = strike[ii]
        selected_dip[ii] = dip[ii]
        selected_rake[ii] = rake[ii]
        selected_sigma[ii] = sigma[0, ii]
        selected_tau[ii] = tau[0, ii]
        unselected_strike[ii] = strike2[ii]
        unselected_dip[ii] = dip2[ii]
        unselected_rake[ii] = rake2[ii]
        unselected_sigma[ii] = sigma[1, ii]
        unselected_tau[ii] = tau[1, ii]
    else:
        selected_strike[ii] = strike2[ii]
        selected_dip[ii] = dip2[ii]
        selected_rake[ii] = rake2[ii]
        selected_sigma[ii] = sigma[1, ii]
        selected_tau[ii] = tau[1, ii]
        unselected_strike[ii] = strike[ii]
        unselected_dip[ii] = dip[ii]
        unselected_rake[ii] = rake[ii]
        unselected_sigma[ii] = sigma[0, ii]
        unselected_tau[ii] = tau[0, ii]


a3.plane(selected_strike, selected_dip, "0.3", zorder=-2)


f4, a4 = plt.subplots()
a4.set_aspect("equal")
circle_x, circle_y = [], []
for i_rad in np.arange(0, 2 * PI + PI / 100, PI / 100):
    circle_x.append(np.sin(i_rad))
    circle_y.append(np.cos(i_rad))
circle_x = np.array(circle_x)
circle_y = np.array(circle_y)

a4.plot(circle_x, circle_y, "0.3")
a4.plot(R * circle_x + 1 - R, R * circle_y, "0.3")
a4.plot((1 - R) * circle_x - R, (1 - R) * circle_y, "0.3")
a4.plot(selected_sigma, selected_tau, "o", color="firebrick", markeredgecolor="k")
a4.plot(unselected_sigma, unselected_tau, ".", color="olive")
a4.arrow(-1.1, 0, 2.2, 0, head_width=0.06, head_length=0.1, fc="k", ec="k")
a4.arrow(-1.1, 0, 0, 1.1, head_width=0.06, head_length=0.1, fc="k", ec="k")
a4.arrow(-1.1, 0, 0, -1.1, head_width=0.06, head_length=0.1, fc="k", ec="k")
a4.text(-1.1, 1.22, "$\\tau$", ha="center")
a4.text(-1.1, -1.21, "$-\\tau$", va="top", ha="center")
a4.text(1.21, 0.0, "${\\sigma}$", va="center")
a4.text(-0.98, -0.1, "$\\sigma_3$")
a4.text(-0.98, -0.1, "$\\sigma_3$")
a4.text(1 - 2 * R + 0.03, -0.1, "$\\sigma_2$")
a4.text(1.02, -0.1, "$\\sigma_1$")
a4.axis("off")
a4.set_xlim(-1.3, 1.3)
a4.set_ylim(-1.3, 1.3)

f6, a6 = plt.subplots()
a6.hist(R_iteration, bins=np.arange(0, 1.01, 0.02))
y1, y2 = a6.get_ylim()
a6.plot([R, R], [0, y2], "firebrick", lw=3)
a6.set_xlabel("Stress Ratio, R")
a6.set_ylabel("count")

for ii in range(len(strike)):
    p1 = a5.pole(
        selected_strike[ii],
        selected_dip[ii],
        "o",
        color="seagreen",
        markeredgecolor="k",
    )

s1_trend, s1_plunge = unit_vector_2_trend_plunge(sqa(s1_axis))
s2_trend, s2_plunge = unit_vector_2_trend_plunge(sqa(s2_axis))
s3_trend, s3_plunge = unit_vector_2_trend_plunge(sqa(s3_axis))

ps1 = a3.line(s1_plunge, s1_trend, "^", ms=10, color="firebrick", markeredgecolor="k")
ps2 = a3.line(s2_plunge, s2_trend, "^", ms=10, color="forestgreen", markeredgecolor="k")
ps3 = a3.line(s3_plunge, s3_trend, "^", ms=10, color="royalblue", markeredgecolor="k")
fleg, aleg = plt.subplots()
aleg.legend([ps1[0], ps2[0], ps3[0]], ["$\\sigma_1$", "$\\sigma_2$", "$\\sigma_3$"])
aleg.axis("off")

plt.show()
