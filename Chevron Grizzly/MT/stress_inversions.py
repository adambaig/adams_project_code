import matplotlib

matplotlib.use("Qt5agg")

from datetime import datetime
import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
import sys

from sms_moment_tensor.MT_math import (
    mt_to_sdr,
    decompose_MT,
    unit_vector_to_trend_plunge,
    trend_plunge_to_unit_vector,
    strike_dip_to_normal,
)
from sms_moment_tensor.stress_inversions import (
    Michael_stress_inversion,
    iterate_Micheal_stress_inversion,
    decompose_stress,
)

sys.path.append("..")
from read_inputs import read_events

PI = np.pi

events = read_events()
for event in {k: v for k, v in events.items() if "dc MT" in v}:
    strike, dip, rake = mt_to_sdr(events[event]["dc MT"], conjugate=False)
    decomp = decompose_MT(events[event]["dc MT"])
    events[event]["p_trend"] = decomp["p_trend"]
    events[event]["b_trend"] = decomp["b_trend"]
    events[event]["t_trend"] = decomp["t_trend"]
    events[event]["p_plunge"] = decomp["p_plunge"]
    events[event]["b_plunge"] = decomp["b_plunge"]
    events[event]["t_plunge"] = decomp["t_plunge"]
    events[event]["strike"] = strike
    events[event]["dip"] = dip
    events[event]["rake"] = rake
#
# strain_fig, (p_ax, b_ax, t_ax) = mpls.subplots(1, 3, figsize=[16, 6])
mt_events = {k: v for k, v in events.items() if "dc MT" in v}
fault_events = {
    k: v
    for (k, v) in mt_events.items()
    if (v["cluster"] == 1 and v["mt confidence"] > 0.9)
}

frac_events = {
    k: v
    for (k, v) in mt_events.items()
    if (v["cluster"] == 0 and v["mt confidence"] > 0.9)
}


frac_events
well_A_events = {k: v for (k, v) in frac_events.items() if (v["well"] == "A")}
well_B_events = {k: v for (k, v) in frac_events.items() if (v["well"] == "B")}
well_D_events = {k: v for (k, v) in frac_events.items() if (v["well"] == "D")}
reference = trend_plunge_to_unit_vector(120, 0)
for timestamp, event in fault_events.items():
    strike1, dip1, rake1, strike2, dip2, rake2 = mt_to_sdr(event["dc MT"])
    normal1 = strike_dip_to_normal(strike1, dip1)
    normal2 = strike_dip_to_normal(strike2, dip2)
    dot1 = normal1 @ reference
    dot2 = normal2 @ reference
    if abs(dot1) > abs(dot2):
        events[timestamp]["real_strike"] = strike1
        events[timestamp]["real_dip"] = dip1
        events[timestamp]["real_rake"] = rake1
        fault_events[timestamp]["real_strike"] = strike1
        fault_events[timestamp]["real_dip"] = dip1
        fault_events[timestamp]["real_rake"] = rake1
    else:
        events[timestamp]["real_strike"] = strike2
        events[timestamp]["real_dip"] = dip2
        events[timestamp]["real_rake"] = rake2
        fault_events[timestamp]["real_strike"] = strike2
        fault_events[timestamp]["real_dip"] = dip2
        fault_events[timestamp]["real_rake"] = rake2

stress_tensor = {
    "fault": np.array(
        Michael_stress_inversion(
            np.array([v["real_strike"] for k, v in fault_events.items()]),
            np.array([v["real_dip"] for k, v in fault_events.items()]),
            np.array([v["real_rake"] for k, v in fault_events.items()]),
        )
    ),
    "well_A": iterate_Micheal_stress_inversion(well_A_events, 100),
    "well_B": iterate_Micheal_stress_inversion(well_B_events, 100),
    "well_D": iterate_Micheal_stress_inversion(well_D_events, 100),
}
R, stress_axes = {}, {}
for dataset in ["fault", "well_A", "well_B", "well_D"]:
    R[dataset], stress_axes[dataset] = decompose_stress(stress_tensor[dataset])

color = {
    "fault": "firebrick",
    "well_A": "royalblue",
    "well_B": "royalblue",
    "well_D": "royalblue",
}
symbol = {"fault": "o", "well_A": "^", "well_B": "d", "well_D": "s"}


def resolve_shear_and_normal_stress(stress_tensor, normal, sign_shear=False):
    # if normal.ndim == 1:
    #     n_planes = 1
    # else:
    #     n_planes, _ = normal.shape
    # # for normal in normals:
    # for i_plane in n_planes:
    R, stress_axes = decompose_stress(stress_tensor)
    n1 = normal @ stress_axes["s1"]
    n2 = normal @ stress_axes["s2"]
    n3 = normal @ stress_axes["s3"]
    sign_tau = 1
    if sign_shear:
        sign_tau = np.sign(n3)
    sigma = n1 * n1 + (1.0 - 2.0 * R) * n2 * n2 - n3 * n3
    tau = sign_tau * np.sqrt(1.0 + 4.0 * R * (R - 1.0) * n2 * n2 - sigma * sigma)
    return sigma, tau


def most_unstable_sdr(mt, stress, friction_angle=0.6):
    strike1, dip1, rake1, strike2, dip2, rake2 = mt_to_sdr(mt)
    normal1 = strike_dip_to_normal(strike1, dip1)
    normal2 = strike_dip_to_normal(strike2, dip2)
    instability = np.zeros(2)
    for jj, normal in enumerate([normal1, normal2]):
        sigma, tau = resolve_shear_and_normal_stress(stress, normal)
        instability[jj] = (abs(tau) - friction_angle * (sigma - 1.0)) / (
            friction_angle + np.sqrt(1.0 + friction_angle * friction_angle)
        )
    if instability[0] > instability[1]:
        return (strike1, dip1, rake1)
    else:
        return (strike2, dip2, rake2)


patches = []
stress_figs, stress_ax = mpls.subplots(1, 3, figsize=[9, 2])
for dataset in ["fault", "well_A", "well_B"]:
    for i_ax in range(3):
        ax_str = "s" + str(i_ax + 1)
        trend, plunge = unit_vector_to_trend_plunge(stress_axes[dataset][ax_str])
        patch = stress_ax[i_ax].line(
            plunge,
            trend,
            symbol[dataset],
            c=color[dataset],
            markeredgecolor="0.2",
        )
    patches.append(patch[0])

fig_legend, ax_legend = plt.subplots()
ax_legend.legend(patches, ["fault", "well A", "well B"])


datasets = {
    "well_A": well_A_events,
    "well_B": well_B_events,
    "well_D": well_D_events,
    "fault": fault_events,
}

mt = event["dc MT"]
stress = stress_tensor[dataset]

for dataset, selected_events in datasets.items():
    print(dataset + ": " + str(len(selected_events)))
    for timestamp, event in selected_events.items():
        if dataset != "fault":
            strike, dip, rake = most_unstable_sdr(
                event["dc MT"], stress_tensor[dataset]
            )
            events[timestamp]["real_strike"] = strike
            events[timestamp]["real_dip"] = dip
            events[timestamp]["real_rake"] = rake
            selected_events[timestamp]["real_strike"] = strike
            selected_events[timestamp]["real_dip"] = dip
            selected_events[timestamp]["real_rake"] = rake

fault_plane_figs, fp_ax = mpls.subplots(1, 3, figsize=[16, 6])
fig_rake = [plt.figure(), plt.figure(), plt.figure()]
rake_bin_number = 37
rake_bins = np.linspace(0, 2 * PI, rake_bin_number)
rake_bar_width = 2 * PI / rake_bin_number
fig_mc, mc_ax = plt.subplots(3, figsize=[8, 9])


circle_x, circle_y = [], []
for i_rad in np.arange(0, PI + PI / 100, PI / 100):
    circle_x.append(np.cos(i_rad))
    circle_y.append(np.sin(i_rad))
circle_x = np.array(circle_x)
circle_y = np.array(circle_y)
alpha = {"fault": 0.01, "well_A": 0.3, "well_B": 0.3}
for i_ax, dataset in enumerate(["fault", "well_A", "well_B"]):
    rake_ax = fig_rake[i_ax].add_subplot(111, projection="polar")

    strikes = np.array([v["real_strike"] for k, v in datasets[dataset].items()])
    dips = np.array([v["real_dip"] for k, v in datasets[dataset].items()])
    fp_ax[i_ax].density_contourf(strikes, dips, measurement="poles", cmap="Reds")
    fp_ax[i_ax].set_title(dataset.replace("_", " ") + "\n\n")
    rakes = np.mod(
        np.array([v["real_rake"] for k, v in datasets[dataset].items()]), 360
    )
    n_rake, _, _ = plt.hist(rakes * PI / 180.0, rake_bins, visible=False)
    rake_ax.bar(rake_bins[:-1], n_rake, width=rake_bar_width, facecolor="royalblue")
    rake_ax.set_title(dataset.replace("_", " ") + "\n\n")
    n_stress, s_stress = (np.zeros(len(strikes)), np.zeros(len(strikes)))
    for i_plane, (strike, dip) in enumerate(zip(strikes, dips)):
        normal = strike_dip_to_normal(strike, dip)
        n_stress[i_plane], s_stress[i_plane] = resolve_shear_and_normal_stress(
            stress_tensor[dataset], normal
        )

    mc_ax[i_ax].set_aspect("equal")
    mc_ax[i_ax].plot(circle_x, circle_y, "0.3", zorder=10)
    mc_ax[i_ax].plot(
        R[dataset] * circle_x + 1 - R[dataset],
        R[dataset] * circle_y,
        "0.3",
        zorder=10,
    )
    mc_ax[i_ax].plot(
        (1 - R[dataset]) * circle_x - R[dataset],
        (1 - R[dataset]) * circle_y,
        "0.3",
        zorder=10,
    )
    mc_ax[i_ax].plot(
        n_stress,
        s_stress,
        ".",
        color="seagreen",
        alpha=alpha[dataset],
        zorder=8,
    )
    mc_ax[i_ax].arrow(-1.1, 0, 2.2, 0, head_width=0.06, head_length=0.1, fc="k", ec="k")
    mc_ax[i_ax].arrow(-1.1, 0, 0, 1.1, head_width=0.06, head_length=0.1, fc="k", ec="k")
    mc_ax[i_ax].text(-1.1, 1.22, "$\\tau$", ha="center")
    mc_ax[i_ax].text(1.21, 0.0, "${\\sigma}$", va="center")
    mc_ax[i_ax].text(-0.98, -0.1, "$\\sigma_3$")
    mc_ax[i_ax].text(-0.98, -0.1, "$\\sigma_3$")
    mc_ax[i_ax].text(1 - 2 * R[dataset] + 0.03, -0.1, "$\\sigma_2$")
    mc_ax[i_ax].text(1.02, -0.1, "$\\sigma_1$")
    mc_ax[i_ax].axis("off")
    mc_ax[i_ax].set_xlim(-1.3, 1.3)
    mc_ax[i_ax].set_ylim(-0.15, 1.3)

#
# f = open("MT_catalog_for_transform.csv", "w")
# f.write(
#     "id, timestamp, easting (m), northing (m), TVD (m),event type,"
#     + " moment magnitude, well, stage, strike (deg), dip (deg), rake (deg),"
#     + "r_sq, condition number, probability\n"
# )
# for timestamp, event in {
#     k: v for k, v in events.items() if "real_rake" in v
# }.items():
#     f.write(
#         event["id"]
#         + ","
#         + datetime.strftime(
#             datetime.strptime(timestamp, "%Y%m%d%H%M%S.%f"),
#             "%Y-%m-%d %H:%M:%S.%f",
#         )[:-3]
#         + ","
#     )
#     f.write(
#         "%.2f, %.2f, %.2f,"
#         % (
#             event["easting"] + 484550.0,
#             event["northing"] + 6020267.03,
#             event["depth"] + 910.0,
#         )
#     )
#     f.write(["Frac", "Fault"][event["cluster"]])
#     f.write(",%.2f," % event["magnitude"])
#     f.write(event["well"] + "," + event["stage"])
#     f.write(
#         ",%.1f,%.1f,%.1f"
#         % (event["real_strike"] % 360, event["real_dip"], event["real_rake"])
#     )
#     f.write(
#         ",%.3f,%.1f,%.3f\n"
#         % (event["dc r"], event["dc condition number"], event["mt confidence"])
#     )
# f.close()
