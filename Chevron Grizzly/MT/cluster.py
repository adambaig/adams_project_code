import matplotlib

matplotlib.use("Qt5agg")

from datetime import datetime
import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
from obspy.imaging.mopad_wrapper import beach
import sys
from sklearn.cluster import AgglomerativeClustering
from sms_moment_tensor.MT_math import (
    decompose_MT,
    mt_to_sdr,
    mt_matrix_to_vector,
    general_to_DC,
)

sys.path.append("..")
from read_inputs import read_events, read_wells

PI = np.pi

events = read_events()
for event in {k: v for k, v in events.items() if "dc MT" in v}:
    strike, dip, rake = mt_to_sdr(events[event]["dc MT"], conjugate=False)
    decomp = decompose_MT(events[event]["dc MT"])
    events[event]["p_trend"] = decomp["p_trend"]
    events[event]["b_trend"] = decomp["b_trend"]
    events[event]["t_trend"] = decomp["t_trend"]
    events[event]["p_plunge"] = decomp["p_plunge"]
    events[event]["b_plunge"] = decomp["b_plunge"]
    events[event]["t_plunge"] = decomp["t_plunge"]
    events[event]["strike"] = strike
    events[event]["dip"] = dip
    events[event]["rake"] = rake
#
# strain_fig, (p_ax, b_ax, t_ax) = mpls.subplots(1, 3, figsize=[16, 6])
mt_events = {k: v for k, v in events.items() if "dc MT" in v}
fault_events = {
    k: v
    for (k, v) in mt_events.items()
    if (v["cluster"] == 1 and v["mt confidence"] > 0.9)
}

frac_events = {
    k: v
    for (k, v) in mt_events.items()
    if (v["cluster"] == 0 and v["mt confidence"] > 0.9)
}

n_mt = len(frac_events)
dot_product_pairs = np.zeros([n_mt, n_mt])
for i1, (id1, mt1) in enumerate(frac_events.items()):
    for i2, (id2, mt2) in enumerate(frac_events.items()):
        dc1_norm = mt1["dc MT"] / np.linalg.norm(mt1["dc MT"])
        dc2_norm = mt2["dc MT"] / np.linalg.norm(mt2["dc MT"])
        dot_product_pairs[i1, i2] = np.tensordot(dc1_norm, dc2_norm)

distance_mat = abs(1 - (dot_product_pairs))


n_clusters = 5
clustering = AgglomerativeClustering(
    n_clusters=n_clusters, affinity="precomputed", linkage="complete"
).fit(distance_mat)
labels = clustering.labels_
for i_mt, (id, mt) in enumerate(frac_events.items()):
    frac_events[id]["label"] = labels[i_mt]

colors = ["firebrick", "forestgreen", "royalblue", "darkgoldenrod", "indigo"]
wells = read_wells()
f1, (a1, a2) = plt.subplots(1, 2)
a1.set_aspect("equal")
for wellname, well in wells.items():
    a1.plot(well["easting"], well["northing"], "0.8", zorder=3, lw=4)
    a1.plot(well["easting"], well["northing"], "0.3", zorder=3, lw=2)
patches = []
for i_cluster in range(n_clusters):
    mt_cluster = {k: v for k, v in frac_events.items() if v["label"] == i_cluster}
    mt_avg = sum(
        [v["dc MT"] / np.linalg.norm(v["dc MT"]) for k, v in mt_cluster.items()]
    )
    p_trend, p_plunge, b_trend, b_plunge, t_trend, t_plunge = (
        [],
        [],
        [],
        [],
        [],
        [],
    )
    print("cluster " + str(i_cluster) + ": " + str(len(mt_cluster)))
    for id, mt in mt_cluster.items():
        decomp = decompose_MT(frac_events[id]["dc MT"])
        p_trend.append(decomp["p_trend"])
        b_trend.append(decomp["b_trend"])
        t_trend.append(decomp["t_trend"])
        p_plunge.append(decomp["p_plunge"])
        b_plunge.append(decomp["b_plunge"])
        t_plunge.append(decomp["t_plunge"])
    f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = a3[0].density_contourf(
        p_plunge, p_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    cax_b = a3[1].density_contourf(
        b_plunge, b_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    cax_t = a3[2].density_contourf(
        t_plunge, t_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    # a3[0].line(p_plunge, p_trend, "k.")
    # a3[1].line(b_plunge, b_trend, "k.")
    # a3[2].line(t_plunge, t_trend, "k.")
    a3[0].set_title("P axes\n\n")
    a3[1].set_title("B axes\n\n")
    a3[2].set_title("T axes\n\n")
    a3[0].grid()
    a3[1].grid()
    a3[2].grid()
    a3[0].set_ylabel("cluster %i\n\n" % i_cluster)
    east, north = np.array(
        [(v["easting"], v["northing"]) for k, v in mt_cluster.items()]
    ).T
    (patch,) = a1.plot(east + 484550.0, north + 6020267.03, ".", zorder=5)
    patches.append(patch)
    avg_mt = sum([v["dc MT"] for k, v in mt_cluster.items()])
    avg_mt /= np.linalg.norm(avg_mt)
    avg_dc = general_to_DC(avg_mt)

    f_bb, a_bb = plt.subplots()
    beachball = beach(
        mt_matrix_to_vector(avg_dc),
        xy=(0.5, 0.5),
        width=0.48,
        facecolor=colors[i_cluster],
        mopad_basis="XYZ",
        linewidth=0.25,
    )
    a_bb.set_aspect("equal")
    a_bb.add_collection(beachball)
    a_bb.set_axis_off()
    mmax = max(v["magnitude"] for k, v in mt_cluster.items())
    mt_large = {k: v for k, v in mt_cluster.items() if v["magnitude"] > (mmax - 0.001)}
    for i_mt, event in mt_large.items():
        print(i_mt + "," + event["id"])
        print(str(event["dc MT"] / np.linalg.norm(event["dc MT"])))
        print(str(event["easting"]) + " " + str(event["northing"]))

    fig_mhist, ax_mhist = plt.subplots()
    ax_mhist.hist(
        np.array([v["magnitude"] for k, v in mt_cluster.items()]),
        np.linspace(-2, 0, 21),
    )
    ax_mhist.set_yscale("log")
a2.legend(patches, [("Cluster %i" % s) for s in range(n_clusters)], numpoints=1)


patches = []
for i_cluster in range(n_clusters):
    mt_cluster = {k: v for k, v in frac_events.items() if v["label"] == i_cluster}
