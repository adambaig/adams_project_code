import matplotlib

matplotlib.use("Qt5agg")

from datetime import datetime
import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
import sys
from obspy import UTCDateTime

from sms_moment_tensor.MT_math import (
    mt_to_sdr,
    decompose_MT,
    unit_vector_to_trend_plunge,
    trend_plunge_to_unit_vector,
    strike_dip_to_normal,
)
from sms_moment_tensor.stress_inversions import (
    Michael_stress_inversion,
    iterate_Micheal_stress_inversion,
    decompose_stress,
    resolve_shear_and_normal_stress,
    most_unstable_sdr,
)

sys.path.append("..")
from read_inputs import read_events

PI = np.pi

events = read_events(filename="flipped_frac_events.csv")
for event in {k: v for k, v in events.items() if "dc MT" in v}:
    strike, dip, rake = mt_to_sdr(events[event]["dc MT"], conjugate=False)
    decomp = decompose_MT(events[event]["dc MT"])
    events[event]["p_trend"] = decomp["p_trend"]
    events[event]["b_trend"] = decomp["b_trend"]
    events[event]["t_trend"] = decomp["t_trend"]
    events[event]["p_plunge"] = decomp["p_plunge"]
    events[event]["b_plunge"] = decomp["b_plunge"]
    events[event]["t_plunge"] = decomp["t_plunge"]
    events[event]["strike"] = strike
    events[event]["dip"] = dip
    events[event]["rake"] = rake
#
# strain_fig, (p_ax, b_ax, t_ax) = mpls.subplots(1, 3, figsize=[16, 6])
mt_events = {k: v for k, v in events.items() if "dc MT" in v}


well_B_events = {k: v for (k, v) in mt_events.items() if (v["well"] == "B")}

stages = np.unique([v["stage"] for k, v in well_B_events.items()])
first_event_time = []

for stage in stages:
    stage_events_times = np.array(
        [k for (k, v) in well_B_events.items() if v["stage"] == stage]
    )
    stage_events_times.sort()
    first_event_time.append(UTCDateTime(stage_events_times[0]).timestamp)
i_time_sort = np.argsort(first_event_time)

stages_sort = stages[i_time_sort]
stress_tensor, stress_iterations = {}, {}
for stage in stages_sort:
    stage_events = {k: v for (k, v) in well_B_events.items() if v["stage"] == stage}
    stress, iterations = iterate_Micheal_stress_inversion(
        stage_events, 1000, output_iterations=True
    )
    label = "well_B_stage_" + stage
    stress_tensor[label] = stress
    stress_iterations[label] = iterations

R, stress_axes = {}, {}
for stage in stages_sort:
    dataset = "well_B_stage_" + stage
    R[dataset], stress_axes[dataset] = decompose_stress(stress_tensor[dataset])

multi_ax_color = ["firebrick", "forestgreen", "royalblue"]

patches = []
stress_fig, stress_ax_uncertainty = mpls.subplots(3, 3, figsize=[16, 12])
r_fig, r_ax = plt.subplots(3, 3, figsize=[16, 12])
for i_dataset, stage in enumerate(stages_sort):
    dataset = "well_B_stage_" + stage
    i_row = i_dataset // 3
    i_col = i_dataset % 3
    for i_ax in range(3):
        ax_str = "s" + str(i_ax + 1)
        trend, plunge = unit_vector_to_trend_plunge(stress_axes[dataset][ax_str])
        stress_ax_uncertainty[i_row, i_col].line(
            plunge,
            trend,
            "o",
            c=multi_ax_color[i_ax],
            markeredgecolor="0.2",
            zorder=4,
        )
    r_distribution = np.zeros(len(stress_iterations[dataset]))
    for i_iteration, stress_iteration in enumerate(stress_iterations[dataset]):
        r_distribution[i_iteration], axes_iteration = decompose_stress(stress_iteration)

        for i_ax in range(3):
            ax_str = "s" + str(i_ax + 1)
            trend, plunge = unit_vector_to_trend_plunge(axes_iteration[ax_str])
            stress_ax_uncertainty[i_row, i_col].line(
                plunge,
                trend,
                "o",
                c=multi_ax_color[i_ax],
                alpha=0.01,
                zorder=3,
            )
        stress_ax_uncertainty[i_row, i_col].set_title(dataset + "\n\n")
    r_ax[i_row, i_col].hist(r_distribution, bins=np.linspace(0, 1, 101))
    y1, y2 = r_ax[i_row, i_col].get_ylim()
    r_ax[i_row, i_col].plot([R[dataset], R[dataset]], [0, y2])
    r_ax[i_row, i_col].set_ylim([0, y2])
    r_ax[i_row, i_col].set_xlim([0, 1])
    r_ax[i_row, i_col].set_title(dataset)
    patches.append(patch[0])

circle_x, circle_y = [], []
for i_rad in np.arange(0, PI + PI / 100, PI / 100):
    circle_x.append(np.cos(i_rad))
    circle_y.append(np.sin(i_rad))
circle_x = np.array(circle_x)
circle_y = np.array(circle_y)

fig_mc, mc_ax = plt.subplots(3, 3, figsize=[16, 9])
for i_dataset, stage in enumerate(stages_sort):
    dataset = "well_B_stage_" + stage
    i_row = i_dataset // 3
    i_col = i_dataset % 3
    n_stress, s_stress = (np.zeros(len(strikes)), np.zeros(len(strikes)))
    for i_plane, (strike, dip) in enumerate(zip(strikes, dips)):
        normal = strike_dip_to_normal(strike, dip)
        n_stress[i_plane], s_stress[i_plane] = resolve_shear_and_normal_stress(
            stress_tensor[dataset], normal
        )

    mc_ax[i_row, i_col].set_aspect("equal")
    mc_ax[i_row, i_col].plot(circle_x, circle_y, "0.3", zorder=10)
    mc_ax[i_row, i_col].plot(
        R[dataset] * circle_x + 1 - R[dataset],
        R[dataset] * circle_y,
        "0.3",
        zorder=10,
    )
    mc_ax[i_row, i_col].plot(
        (1 - R[dataset]) * circle_x - R[dataset],
        (1 - R[dataset]) * circle_y,
        "0.3",
        zorder=10,
    )
    mc_ax[i_row, i_col].plot(
        n_stress, s_stress, ".", color="seagreen", alpha=0.6, zorder=8
    )
    mc_ax[i_row, i_col].arrow(
        -1.1, 0, 2.2, 0, head_width=0.06, head_length=0.1, fc="k", ec="k"
    )
    mc_ax[i_row, i_col].arrow(
        -1.1, 0, 0, 1.1, head_width=0.06, head_length=0.1, fc="k", ec="k"
    )
    mc_ax[i_row, i_col].text(-1.1, 1.22, "$\\tau$", ha="center")
    mc_ax[i_row, i_col].text(1.21, 0.0, "${\\sigma}$", va="center")
    mc_ax[i_row, i_col].text(-0.98, -0.1, "$\\sigma_3$")
    mc_ax[i_row, i_col].text(-0.98, -0.1, "$\\sigma_3$")
    mc_ax[i_row, i_col].text(1 - 2 * R[dataset] + 0.03, -0.1, "$\\sigma_2$")
    mc_ax[i_row, i_col].text(1.02, -0.1, "$\\sigma_1$")
    mc_ax[i_row, i_col].axis("off")
    mc_ax[i_row, i_col].set_xlim(-1.3, 1.3)
    mc_ax[i_row, i_col].set_ylim(-0.15, 1.3)
"""

for dataset, selected_events in datasets.items():
    print(dataset + ": " + str(len(selected_events)))
    for timestamp, event in selected_events.items():
        strike, dip, rake = most_unstable_sdr(
            event["dc MT"], stress_tensor[dataset]
        )
        events[timestamp]["real_strike"] = strike
        events[timestamp]["real_dip"] = dip
        events[timestamp]["real_rake"] = rake
        selected_events[timestamp]["real_strike"] = strike
        selected_events[timestamp]["real_dip"] = dip
        selected_events[timestamp]["real_rake"] = rake

fault_plane_figs, fp_ax = mpls.subplots(1, 3, figsize=[16, 6])
fig_rake = [plt.figure(), plt.figure(), plt.figure()]
rake_bin_number = 37
rake_bins = np.linspace(0, 2 * PI, rake_bin_number)
rake_bar_width = 2 * PI / rake_bin_number
fig_mc, mc_ax = plt.subplots(3, figsize=[8, 9])



alpha = {"well_A": 0.3, "well_B": 0.3, "well_D": 1}
for i_ax, dataset in enumerate(["well_A", "well_B", "well_D"]):
    rake_ax = fig_rake[i_ax].add_subplot(111, projection="polar")

    strikes = np.array(
        [v["real_strike"] for k, v in datasets[dataset].items()]
    )
    dips = np.array([v["real_dip"] for k, v in datasets[dataset].items()])
    fp_ax[i_ax].density_contourf(
        strikes, dips, measurement="poles", cmap="Reds"
    )
    fp_ax[i_ax].set_title(dataset.replace("_", " ") + "\n\n")
    rakes = np.mod(
        np.array([v["real_rake"] for k, v in datasets[dataset].items()]), 360
    )
    n_rake, _, _ = plt.hist(rakes * PI / 180.0, rake_bins, visible=False)
    rake_ax.bar(
        rake_bins[:-1], n_rake, width=rake_bar_width, facecolor="royalblue"
    )
    rake_ax.set_title(dataset.replace("_", " ") + "\n\n")
    n_stress, s_stress = (np.zeros(len(strikes)), np.zeros(len(strikes)))
    for i_plane, (strike, dip) in enumerate(zip(strikes, dips)):
        normal = strike_dip_to_normal(strike, dip)
        n_stress[i_plane], s_stress[i_plane] = resolve_shear_and_normal_stress(
            stress_tensor[dataset], normal
        )

    mc_ax[i_ax].set_aspect("equal")
    mc_ax[i_ax].plot(circle_x, circle_y, "0.3", zorder=10)
    mc_ax[i_ax].plot(
        R[dataset] * circle_x + 1 - R[dataset],
        R[dataset] * circle_y,
        "0.3",
        zorder=10,
    )
    mc_ax[i_ax].plot(
        (1 - R[dataset]) * circle_x - R[dataset],
        (1 - R[dataset]) * circle_y,
        "0.3",
        zorder=10,
    )
    mc_ax[i_ax].plot(
        n_stress,
        s_stress,
        ".",
        color="seagreen",
        alpha=alpha[dataset],
        zorder=8,
    )
    mc_ax[i_ax].arrow(
        -1.1, 0, 2.2, 0, head_width=0.06, head_length=0.1, fc="k", ec="k"
    )
    mc_ax[i_ax].arrow(
        -1.1, 0, 0, 1.1, head_width=0.06, head_length=0.1, fc="k", ec="k"
    )
    mc_ax[i_ax].text(-1.1, 1.22, "$\\tau$", ha="center")
    mc_ax[i_ax].text(1.21, 0.0, "${\\sigma}$", va="center")
    mc_ax[i_ax].text(-0.98, -0.1, "$\\sigma_3$")
    mc_ax[i_ax].text(-0.98, -0.1, "$\\sigma_3$")
    mc_ax[i_ax].text(1 - 2 * R[dataset] + 0.03, -0.1, "$\\sigma_2$")
    mc_ax[i_ax].text(1.02, -0.1, "$\\sigma_1$")
    mc_ax[i_ax].axis("off")
    mc_ax[i_ax].set_xlim(-1.3, 1.3)
    mc_ax[i_ax].set_ylim(-0.15, 1.3)
"""


np.log10(2) / 2
10 ** (0.15)
