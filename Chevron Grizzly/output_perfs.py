# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 08:59:52 2019

@author: adambaig
"""

from read_inputs import read_wells

wells = read_wells()

basedir = "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\"
f = open("output_perfs.csv", "w")
f.write(
    "well,stage,Cluster,top northing (m),bottom northing (m),"
    + "top easting (m),bottom easting (m), top tvdss (m), bottom tvdss(m)\n"
)
for well in wells.keys():
    for stage in wells[well].keys():
        if stage[:5] == "Stage":
            for cluster in sorted(wells[well][stage].keys()):
                f.write(well + c + stage + c + cluster + c)
                f.write(
                    "%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n"
                    % (
                        wells[well][stage][cluster]["top_north"],
                        wells[well][stage][cluster]["bottom_north"],
                        wells[well][stage][cluster]["top_east"],
                        wells[well][stage][cluster]["bottom_east"],
                        wells[well][stage][cluster]["top_tvdss"],
                        wells[well][stage][cluster]["bottom_tvdss"],
                    )
                )
f.close()
