import matplotlib

matplotlib.use("Qt5agg")

from datetime import datetime
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import mplstereonet as mpls
import numpy as np
import sys
from obspy import UTCDateTime
import scipy.stats as st

from sms_moment_tensor.MT_math import decompose_MT, mt_to_sdr

sys.path.append("..")
from read_inputs import read_events, read_wells

PI = np.pi


def r_nearest_neighbours(grid_point, events, radius):
    radius_sq = radius * radius
    ref_east, ref_north = grid_point["east"], grid_point["north"]
    cull_events = {
        k: v
        for k, v in events.items()
        if (
            (v["easting"] - ref_east) ** 2 + (v["northing"] - ref_north) ** 2
            < radius_sq
        )
    }
    return cull_events


def r3_nearest_neighbours(grid_point, events, radius):
    radius_sq = radius * radius
    ref_east, ref_north, ref_depth = (
        grid_point["east"],
        grid_point["north"],
        grid_point["depth"],
    )
    cull_events = {
        k: v
        for k, v in events.items()
        if (
            (v["easting"] - ref_east) ** 2
            + (v["northing"] - ref_north) ** 2
            + (v["depth"] - ref_depth) ** 2
            < radius_sq
        )
    }
    return cull_events


def scaled_mt(event):
    moment = 10 ** (1.5 * event["magnitude"] + 9)
    return event["dc MT"] * moment / np.linalg.norm(event["dc MT"])


events = {
    k: v
    for k, v in read_events(filename="flipped_frac_events.csv").items()
    if v["magnitude"] > -1.55
}
wells = read_wells(local_coords=True)
for event in {k: v for k, v in events.items() if "dc MT" in v}:
    strike, dip, rake = mt_to_sdr(events[event]["dc MT"], conjugate=False)
    decomp = decompose_MT(events[event]["dc MT"])
    events[event]["p_trend"] = decomp["p_trend"]
    events[event]["b_trend"] = decomp["b_trend"]
    events[event]["t_trend"] = decomp["t_trend"]
    events[event]["p_plunge"] = decomp["p_plunge"]
    events[event]["b_plunge"] = decomp["b_plunge"]
    events[event]["t_plunge"] = decomp["t_plunge"]
    events[event]["strike"] = strike
    events[event]["dip"] = dip
    events[event]["rake"] = rake


easting, northing = np.array(
    [(v["easting"], v["northing"]) for k, v in events.items()]
).T
grid_spacing = 40
reservoir_thickness = 250
min_events = 5
east_grid = np.arange(min(easting), max(easting), grid_spacing)
north_grid = np.arange(min(northing), max(northing), grid_spacing)


def plot_perfs(axis, well, highlight_stage=None):
    stages = [k for k in well.keys() if "Stage" in k]
    for stage in stages:
        norths = [(v["top_north"], v["bottom_north"]) for v in well[stage].values()]
        easts = [(v["top_east"], v["bottom_east"]) for v in well[stage].values()]
        if stage == highlight_stage:
            for e, n in zip(easts, norths):
                axis.plot(e, n, "r", lw=9, zorder=6)
                axis.plot(e, n, "k", lw=10, zorder=5)
        else:
            for e, n in zip(easts, norths):
                axis.plot(e, n, "w", lw=7, zorder=6)
                axis.plot(e, n, "k", lw=8, zorder=5)
    return axis


grid_points = []
strain_mag = np.zeros([len(east_grid), len(north_grid)])
for i_east, east in enumerate(east_grid):
    for i_north, north in enumerate(north_grid):
        grid_mts = r_nearest_neighbours(
            {"east": east, "north": north}, events, 2 * grid_spacing
        )
        volume = 4 * PI * grid_spacing * grid_spacing * reservoir_thickness
        if len(grid_mts) >= min_events:
            strain = (
                sum([scaled_mt(v) for k, v in grid_mts.items()]) / volume / 2.0 / 1.5e9
            )
            grid_points.append({"east": east, "north": north, "strain": strain})
            strain_mag[i_east, i_north] = np.linalg.norm(strain)
        else:
            grid_points.append({"east": east, "north": north, "strain": 0})

l2, l5 = np.log10(2), np.log10(5)
levels = [-9, -9 + l2, -9 + l5, -8, -8 + l2, -8 + l5, -7]
fig, ax = plt.subplots()
ax.set_aspect("equal")
ax.set_facecolor("0.95")
wells = read_wells(local_coords=True)
contour_plot = ax.contourf(
    east_grid,
    north_grid,
    np.log10(strain_mag).T,
    zorder=16,
    alpha=0.5,
    vmin=-9,
    vmax=-7,
    levels=levels,
    extend="max",
    cmap="inferno_r",
)

ax.plot(
    [v["easting"] for v in events.values()],
    [v["northing"] for v in events.values()],
    "k.",
    zorder=10,
)
x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()
for well in wells.values():
    ax.plot(well["easting"], well["northing"], "0.2", lw=3)
    ax.plot(well["easting"], well["northing"], "0.8", lw=2)
    plot_perfs(ax, well)
minor_ticks = np.arange(-5000, 5000, 200)
major_ticks = np.arange(-5000, 5000, 200)
ax.set_xticks(minor_ticks, minor=True)
ax.set_yticks(minor_ticks, minor=True)
ax.set_xticks(major_ticks, minor=False)
ax.set_yticks(major_ticks, minor=False)
ax.grid(which="both")
ax.grid(which="minor", alpha=1)
ax.set_ylim([y1, y2])
ax.set_xlim([x1, x2])
ax.set_yticklabels([])
ax.set_xticklabels([])
ax.tick_params(which="both", color="w")


f_cb, a_cb = plt.subplots()
cb = f.colorbar(contour_plot, ax=a_cb)
cb.set_ticklabels(
    [
        "$1\cdot10^{-9}$",
        "$2\cdot10^{-9}$",
        "$5\cdot10^{-9}$",
        "$1\cdot10^{-8}$",
        "$2\cdot10^{-8}$",
        "$5\cdot10^{-8}$",
        "$1\cdot10^{-7}$",
    ]
)
cb.set_label("strain")
# def area(vs):
#     a = 0
#     x0, y0 = vs[0]
#     for [x1, y1] in vs[1:]:
#         dx = x1 - x0
#         dy = y1 - y0
#         a += 0.5 * (y0 * dx - x0 * dy)
#         x0 = x1
#         y0 = y1
#     return a
#
#
#
#
#
# f, a = plt.subplots()
# a.set_aspect('equal')
# for j in range(len(contour_plot.collections)):
#     n_collections = len(contour_plot.collections[j].get_paths())
#     for i in range(n_collections):
#         x_path = contour_plot.collections[j].get_paths()[i].vertices[:,0]
#         y_path = contour_plot.collections[j].get_paths()[i].vertices[:,1]
#         a.plot(x_path,y_path)
#
#
# contour_plot.collections[1].get_paths()[i].vertices[:,0]

plt.show()
