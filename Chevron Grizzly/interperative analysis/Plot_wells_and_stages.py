import matplotlib

matplotlib.use("Qt5agg")

from datetime import datetime
import matplotlib.pyplot as plt
from matplotlib import cm

import mplstereonet as mpls
import numpy as np
import sys
from obspy import UTCDateTime
import scipy.stats as st

from sms_moment_tensor.MT_math import mt_to_sdr, decompose_MT

sys.path.append("..")
from read_inputs import read_events, read_wells

PI = np.pi

events = read_events(filename="flipped_frac_events.csv")
wells = read_wells(local_coords=True)
for event in {k: v for k, v in events.items() if "dc MT" in v}:
    strike, dip, rake = mt_to_sdr(events[event]["dc MT"], conjugate=False)
    decomp = decompose_MT(events[event]["dc MT"])
    events[event]["p_trend"] = decomp["p_trend"]
    events[event]["b_trend"] = decomp["b_trend"]
    events[event]["t_trend"] = decomp["t_trend"]
    events[event]["p_plunge"] = decomp["p_plunge"]
    events[event]["b_plunge"] = decomp["b_plunge"]
    events[event]["t_plunge"] = decomp["t_plunge"]
    events[event]["strike"] = strike
    events[event]["dip"] = dip
    events[event]["rake"] = rake
#
# strain_fig, (p_ax, b_ax, t_ax) = mpls.subplots(1, 3, figsize=[16, 6])
mt_events = {k: v for k, v in events.items() if "dc MT" in v}


well_B_events = {k: v for (k, v) in mt_events.items() if (v["well"] == "B")}

stages = np.unique([v["stage"] for k, v in well_B_events.items()])
first_event_time = []

for stage in stages:
    stage_events_times = np.array(
        [k for (k, v) in well_B_events.items() if v["stage"] == stage]
    )
    stage_events_times.sort()
    first_event_time.append(UTCDateTime(stage_events_times[0]).timestamp)
i_time_sort = np.argsort(first_event_time)


def plot_perfs(axis, well, highlight_stage=None):
    stages = [k for k in well.keys() if "Stage" in k]
    for stage in stages:
        norths = [(v["top_north"], v["bottom_north"]) for v in well[stage].values()]
        easts = [(v["top_east"], v["bottom_east"]) for v in well[stage].values()]
        if stage == highlight_stage:
            for e, n in zip(easts, norths):
                axis.plot(e, n, "r", lw=9, zorder=6)
                axis.plot(e, n, "k", lw=10, zorder=5)
        else:
            for e, n in zip(easts, norths):
                axis.plot(e, n, "w", lw=7, zorder=6)
                axis.plot(e, n, "k", lw=8, zorder=5)
    return axis


avg_depth = {}
for frac_well in ["A", "B"]:

    perf_depths = []
    for stage in stages[i_time_sort]:
        stage_name = "Stage " + stage.split("-")[0]
        perf_avg_north = np.average(
            [
                0.5 * (v["top_north"] + v["bottom_north"])
                for v in wells[frac_well][stage_name].values()
            ]
        )
        perf_avg_east = np.average(
            [
                0.5 * (v["top_east"] + v["bottom_east"])
                for v in wells[frac_well][stage_name].values()
            ]
        )
        perf_depths.append(
            [
                0.5 * (v["top_tvdss"] + v["bottom_tvdss"])
                for v in wells[frac_well][stage_name].values()
            ]
        )
        for event_id in {
            k: v
            for k, v in events.items()
            if v["stage"] == stage and v["well"] == frac_well
        }.keys():
            events[event_id]["rel_north"] = (
                events[event_id]["northing"] - perf_avg_north
            )
            events[event_id]["rel_east"] = events[event_id]["easting"] - perf_avg_east
    avg_depth[frac_well] = np.average(perf_depths)

A_events = {
    k: v for k, v in events.items() if v["well"] == "A" and "rel_east" in v.keys()
}

B_events = {
    k: v for k, v in events.items() if v["well"] == "B" and "rel_east" in v.keys()
}


fig, (ax2, ax1) = plt.subplots(1, 2, sharey=True, figsize=[13, 10])
for ax in (ax1, ax2):
    ax.set_aspect("equal")
    ax.set_facecolor("0.95")
easting_A = [v["easting"] for v in A_events.values()]
northing_A = [v["northing"] for v in A_events.values()]
easting_B = [v["easting"] for v in B_events.values()]
northing_B = [v["northing"] for v in B_events.values()]
stage_num_A = [int(v["stage"].split("-")[0]) for v in A_events.values()]
stage_num_B = [int(v["stage"].split("-")[0]) for v in B_events.values()]
scatter = ax1.scatter(
    easting_A,
    northing_A,
    marker="o",
    c=stage_num_A,
    zorder=17,
    cmap="tab10",
    vmin=2.5,
    vmax=12.5,
    edgecolor="k",
)
ax2.scatter(
    easting_B,
    northing_B,
    marker="o",
    c=stage_num_B,
    zorder=17,
    cmap="tab10",
    vmin=2.5,
    vmax=12.5,
    edgecolor="k",
)

x1_A, x2_A = ax1.get_xlim()
x1_B, x2_B = ax2.get_xlim()
y1_A, y2_A = ax1.get_ylim()
y1_B, y2_B = ax2.get_ylim()

for well in wells:
    for ax in (ax1, ax2):
        ax.plot(wells[well]["easting"], wells[well]["northing"], "0.2", lw=4, zorder=3)
        ax.plot(
            wells[well]["easting"], wells[well]["northing"], "0.8", lw=2.5, zorder=4
        )
# perfs on A well

tab10 = cm.get_cmap("tab10")

for stage in range(3, 13):
    color = tab10((stage - 2.5) / 10)
    stage_name = "Stage " + str(stage)
    north1 = wells["A"][stage_name]["Perf 1"]["top_north"]
    north2 = wells["A"][stage_name]["Perf 4"]["bottom_north"]
    east1 = wells["A"][stage_name]["Perf 1"]["top_east"]
    east2 = wells["A"][stage_name]["Perf 4"]["bottom_east"]
    ax1.plot([east1, east2], [north1, north2], color=color, lw=8, zorder=10)
    ax1.plot([east1, east2], [north1, north2], color="k", lw=9.5, zorder=9)

for stage in range(3, 11):
    color = tab10((stage - 2.5) / 10)
    stage_name = "Stage " + str(stage)
    north1 = wells["B"][stage_name]["Perf 1"]["top_north"]
    north2 = wells["B"][stage_name]["Perf 4"]["bottom_north"]
    east1 = wells["B"][stage_name]["Perf 1"]["top_east"]
    east2 = wells["B"][stage_name]["Perf 4"]["bottom_east"]
    ax2.plot([east1, east2], [north1, north2], color=color, lw=8, zorder=10)
    ax2.plot([east1, east2], [north1, north2], color="k", lw=9.5, zorder=9)

minor_ticks = np.arange(-5000, 5000, 100)
major_ticks = np.arange(-5000, 5000, 100)
for ax in (ax1, ax2):
    ax.set_xticks(minor_ticks, minor=True)
    ax.set_yticks(minor_ticks, minor=True)
    ax.set_xticks(major_ticks, minor=False)
    ax.set_yticks(major_ticks, minor=False)
    ax.grid(which="both")
    ax.grid(which="minor", alpha=1)
    ax.set_aspect("equal")
    ax.set_yticklabels([])
    ax.set_xticklabels([])
    ax.tick_params(which="both", color="w")

ax1.set_xlim(x1_A, x2_A)
ax2.set_xlim(x1_B, x2_B)
ax1.set_ylim(y1_A, y2_A)
ax2.set_ylim(y1_A, y2_A)
# ax1.set_xlabel("easting (m)")
# ax2.set_xlabel("easting (m)")
# ax2.set_ylabel("northing (m)")
plt.show()
