import matplotlib

matplotlib.use("Qt5agg")

from datetime import datetime
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from matplotlib import cm
import matplotlib.colors as mcolors
import mplstereonet as mpls
import numpy as np
import sys
from obspy import UTCDateTime

from sms_moment_tensor.MT_math import (
    mt_to_sdr,
    decompose_MT,
    unit_vector_to_trend_plunge,
    trend_plunge_to_unit_vector,
    strike_dip_to_normal,
)
from sms_moment_tensor.stress_inversions import (
    Michael_stress_inversion,
    iterate_Micheal_stress_inversion,
    decompose_stress,
    resolve_shear_and_normal_stress,
    most_unstable_sdr,
)

sys.path.append("..")
from read_inputs import read_events, read_wells

PI = np.pi
tab10 = cm.get_cmap("tab10")

custom_cmap = mcolors.ListedColormap(
    np.vstack(
        [
            mcolors.to_rgba("firebrick"),
            mcolors.to_rgba("darkgoldenrod"),
            mcolors.to_rgba("royalblue"),
        ]
    )
)


events = {
    k: v
    for k, v in read_events(filename="flipped_frac_events.csv").items()
    if v["magnitude"] > -1.55
}
wells = read_wells(local_coords=True)
for event in {k: v for k, v in events.items() if "dc MT" in v}:
    strike, dip, rake = mt_to_sdr(events[event]["dc MT"], conjugate=False)
    decomp = decompose_MT(events[event]["dc MT"])
    events[event]["p_trend"] = decomp["p_trend"]
    events[event]["b_trend"] = decomp["b_trend"]
    events[event]["t_trend"] = decomp["t_trend"]
    events[event]["p_plunge"] = decomp["p_plunge"]
    events[event]["b_plunge"] = decomp["b_plunge"]
    events[event]["t_plunge"] = decomp["t_plunge"]
    events[event]["strike"] = strike
    events[event]["dip"] = dip
    events[event]["rake"] = rake


for event in events:
    decomp = decompose_MT(events[event]["dc MT"])
    if decomp["p_plunge"] > 45:
        if np.tan(np.pi * decomp["b_trend"] / 180) > 0:
            events[event]["mt cluster"] = 1
        else:
            events[event]["mt cluster"] = 2
    else:
        events[event]["mt cluster"] = 0

well = "B"
stage = "3"

# strain_fig, (p_ax, b_ax, t_ax) = mpls.subplots(1, 3, figsize=[16, 6])
mt_events = {k: v for k, v in events.items() if "dc MT" in v}
well_B_events = {k: v for (k, v) in mt_events.items() if (v["well"] == "B")}
stages = np.unique([v["stage"] for k, v in well_B_events.items()])
first_event_time = []


def array_from_dict(dictionary, value):
    return np.array([v[value] for k, v in dictionary.items()])


def plot_perfs(axis, well, highlight_stage=None, color="r"):
    stages = [k for k in well.keys() if "Stage" in k]
    for stage in stages:
        norths = [(v["top_north"], v["bottom_north"]) for v in well[stage].values()]
        easts = [(v["top_east"], v["bottom_east"]) for v in well[stage].values()]
        if stage == highlight_stage:
            for e, n in zip(easts, norths):
                axis.plot(e, n, color, lw=9, zorder=6)
                axis.plot(e, n, "k", lw=10, zorder=5)
        else:
            for e, n in zip(easts, norths):
                axis.plot(e, n, "w", lw=7, zorder=6)
                axis.plot(e, n, "k", lw=8, zorder=5)
    return axis


n_std_dev = 2
zoom_factor = 0.2
frac_dimensions = {}
stage_events = {
    k: v for k, v in events.items() if v["stage"] == stage and v["well"] == "B"
}
eastings = np.array([v["easting"] for v in stage_events.values()])
northings = np.array([v["northing"] for v in stage_events.values()])
east_center, north_center = np.average(eastings), np.average(northings)
xy = np.vstack([eastings - east_center, northings - north_center])
covariance = np.cov(eastings, northings)
eig_vals, eig_vecs = np.linalg.eig(covariance)
i_sort = np.argsort(eig_vals)
ellipse_minor_axis, ellipse_major_axis = eig_vecs[:, i_sort]
minor_length, major_length = np.sqrt(eig_vals[i_sort]) * n_std_dev
azimuth = np.arctan(ellipse_major_axis[0] / ellipse_major_axis[1]) * 180 / np.pi
color = matplotlib.colors.to_hex(tab10((int(stage.split("-")[0]) - 2.5) / 10))

fig, ax = plt.subplots()
ax.set_aspect("equal")
ax.set_facecolor("0.95")
ax.scatter(
    [v["easting"] for v in stage_events.values()],
    [v["northing"] for v in stage_events.values()],
    c=array_from_dict(stage_events, "mt cluster"),
    marker="o",
    s=[(v["magnitude"] + 1.6) * 100 for v in stage_events.values()],
    zorder=3,
    edgecolor="k",
    cmap=custom_cmap,
)
x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()
yrange = y2 - y1
xrange = x2 - x1
x1_new = x1 - zoom_factor * (x2 - x1)
y1_new = y1 - zoom_factor * (y2 - y1)
x2_new = x2 + zoom_factor * (y2 - y1)
y2_new = y2 + zoom_factor * (y2 - y1)
for well_id, well in wells.items():
    ax.plot(well["easting"], well["northing"], "0.2", lw=3)
    ax.plot(well["easting"], well["northing"], "0.8", lw=2)
    if well_id == "B":
        plot_perfs(
            ax, well, highlight_stage="Stage " + stage.split("-")[0], color=color
        )
    else:
        plot_perfs(ax, well)
minor_ticks = np.arange(-5000, 5000, 50)
major_ticks = np.arange(-5000, 5000, 50)
ax.set_xticks(minor_ticks, minor=True)
ax.set_yticks(minor_ticks, minor=True)
ax.set_xticks(major_ticks, minor=False)
ax.set_yticks(major_ticks, minor=False)
ax.set_title("Well B: Stage " + stage)
ax.grid(which="both")
ax.grid(which="minor", alpha=1)
ax.set_ylim([y1_new, y2_new])
ax.set_xlim([x1_new, x2_new])
ax.set_aspect("equal")
ax.set_yticklabels([])
ax.set_xticklabels([])
ax.tick_params(which="both", color="w")
plt.show()
