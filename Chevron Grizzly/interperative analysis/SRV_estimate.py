import matplotlib

matplotlib.use("Qt5agg")

from datetime import datetime
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import mplstereonet as mpls
import numpy as np
import sys
from obspy import UTCDateTime
import scipy.stats as st
from scipy.ndimage import zoom
from skimage import measure
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from sms_moment_tensor.MT_math import decompose_MT, mt_to_sdr

sys.path.append("..")
from read_inputs import read_events, read_wells

PI = np.pi


def r_nearest_neighbours(grid_point, events, radius):
    radius_sq = radius * radius
    ref_east, ref_north = grid_point["east"], grid_point["north"]
    cull_events = {
        k: v
        for k, v in events.items()
        if (
            (v["easting"] - ref_east) ** 2 + (v["northing"] - ref_north) ** 2
            < radius_sq
        )
    }
    return cull_events


def r3_nearest_neighbours(grid_point, events, radius):
    radius_sq = radius * radius
    ref_east, ref_north, ref_depth = (
        grid_point["east"],
        grid_point["north"],
        grid_point["depth"],
    )
    cull_events = {
        k: v
        for k, v in events.items()
        if (
            (v["easting"] - ref_east) ** 2
            + (v["northing"] - ref_north) ** 2
            + (v["depth"] - ref_depth) ** 2
            < radius_sq
        )
    }
    return cull_events


def scaled_mt(event):
    moment = 10 ** (1.5 * event["magnitude"] + 9)
    return event["dc MT"] * moment / np.linalg.norm(event["dc MT"])


complete_events = {
    k: v
    for k, v in read_events(filename="flipped_frac_events.csv").items()
    if v["magnitude"] > -1.55
}
wells = read_wells(local_coords=True)
for event in {k: v for k, v in complete_events.items() if "dc MT" in v}:
    strike, dip, rake = mt_to_sdr(complete_events[event]["dc MT"], conjugate=False)
    decomp = decompose_MT(complete_events[event]["dc MT"])
    complete_events[event]["p_trend"] = decomp["p_trend"]
    complete_events[event]["b_trend"] = decomp["b_trend"]
    complete_events[event]["t_trend"] = decomp["t_trend"]
    complete_events[event]["p_plunge"] = decomp["p_plunge"]
    complete_events[event]["b_plunge"] = decomp["b_plunge"]
    complete_events[event]["t_plunge"] = decomp["t_plunge"]
    complete_events[event]["strike"] = strike
    complete_events[event]["dip"] = dip
    complete_events[event]["rake"] = rake


easting, northing, depth = np.array(
    [(v["easting"], v["northing"], v["depth"]) for k, v in complete_events.items()]
).T
grid_spacing = 40
reservoir_thickness = 250
min_events = 5
east_grid = np.arange(
    min(easting) - 2 * grid_spacing, max(easting) + 2 * grid_spacing, grid_spacing
)
north_grid = np.arange(
    min(northing) - 2 * grid_spacing, max(northing) + 2 * grid_spacing, grid_spacing
)
depth_grid = np.arange(
    min(depth) - 2 * grid_spacing, max(depth) + 2 * grid_spacing, grid_spacing
)


def plot_perfs(axis, well, highlight_stage=None):
    stages = [k for k in well.keys() if "Stage" in k]
    for stage in stages:
        norths = [(v["top_north"], v["bottom_north"]) for v in well[stage].values()]
        easts = [(v["top_east"], v["bottom_east"]) for v in well[stage].values()]
        if stage == highlight_stage:
            for e, n in zip(easts, norths):
                axis.plot(e, n, "r", lw=9, zorder=6)
                axis.plot(e, n, "k", lw=10, zorder=5)
        else:
            for e, n in zip(easts, norths):
                axis.plot(e, n, "w", lw=7, zorder=6)
                axis.plot(e, n, "k", lw=8, zorder=5)
    return axis


grid_points = []
f = open("strain_out.csv", "w")
f.write("grid east, grid north, grid depth, strain\n")
strain_mag = np.zeros([len(east_grid), len(north_grid), len(depth_grid)])
for i_east, east in enumerate(east_grid):
    for i_north, north in enumerate(north_grid):
        for i_depth, depth in enumerate(depth_grid):
            grid_mts = r3_nearest_neighbours(
                {"east": east, "north": north, "depth": depth},
                complete_events,
                2 * grid_spacing,
            )
            volume = 4 * PI * grid_spacing * grid_spacing * grid_spacing / 3
            if len(grid_mts) >= min_events:
                strain = (
                    sum([scaled_mt(v) for k, v in grid_mts.items()])
                    / volume
                    / 2.0
                    / 1.5e9
                )
                grid_points.append(
                    {"east": east, "north": north, "depth": depth, "strain": strain}
                )
                strain_mag[i_east, i_north, i_depth] = np.linalg.norm(strain)
            else:
                grid_points.append(
                    {"east": east, "north": north, "depth": depth, "strain": 0}
                )
                strain_mag[i_east, i_north, i_depth] = 0
            f.write(
                "%.0f,%.0f,%.0f,%.3e\n"
                % (east, north, depth, np.linalg.norm(strain_mag))
            )

f.close()

strains = np.squeeze(
    strain_mag.reshape([(i_east + 1) * (i_north + 1) * (i_depth + 1), 1])
)


plt.hist(
    np.log10(np.array(strains[np.where(strains > 0)[0]])), bins=np.arange(-10, -6, 0.1)
)

print(
    "volume is %.3e m3"
    % len(np.array(strains[np.where(strains > 10**-8)[0]]))
    * grid_spacing
    * grid_spacing
    * grid_spacing
)

strain_mag_interp = zoom(strain_mag, 4)

ne4, nn4, nd4 = strain_mag_interp.shape


l2, l5 = np.log10(2), np.log10(5)
levels = [-8, -8 + l2, -8 + l5, -7, -7 + l2, -7 + l5, -6]
fig, ax = plt.subplots()
ax.set_aspect("equal")
ax.set_facecolor("0.95")
wells = read_wells(local_coords=True)
east_grid4 = np.linspace(east_grid[0], east_grid[0] + ne4 * grid_spacing / 4, ne4)
north_grid4 = np.linspace(north_grid[0], north_grid[0] + nn4 * grid_spacing / 4, nn4)
depth_grid4 = np.linspace(depth_grid[0], depth_grid[0] + nd4 * grid_spacing / 4, nd4)
contour_plot = ax.contourf(
    east_grid4,
    north_grid4,
    np.log10(strain_mag_interp[:, :, 37]).T,
    zorder=16,
    alpha=0.5,
    vmin=-8,
    vmax=-6,
    levels=levels,
    extend="max",
    cmap="inferno_r",
)

slice_events = {
    k: v
    for k, v in complete_events.items()
    if v["depth"] > (depth_grid4[37] - 2 * grid_spacing)
    and v["depth"] < (depth_grid4[37] + 2 * grid_spacing)
}

ax.plot(
    [v["easting"] for v in slice_events.values()],
    [v["northing"] for v in slice_events.values()],
    "k.",
    zorder=10,
)
x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()
for well in wells.values():
    ax.plot(well["easting"], well["northing"], "0.2", lw=3)
    ax.plot(well["easting"], well["northing"], "0.8", lw=2)
    plot_perfs(ax, well)
minor_ticks = np.arange(-5000, 5000, 200)
major_ticks = np.arange(-5000, 5000, 200)
ax.set_xticks(minor_ticks, minor=True)
ax.set_yticks(minor_ticks, minor=True)
ax.set_xticks(major_ticks, minor=False)
ax.set_yticks(major_ticks, minor=False)
ax.grid(which="both")
ax.grid(which="minor", alpha=1)
ax.set_ylim([y1, y2])
ax.set_xlim([x1, x2])
ax.set_yticklabels([])
ax.set_xticklabels([])
ax.tick_params(which="both", color="w")

f_cb, a_cb = plt.subplots()
cb = f_cb.colorbar(contour_plot, ax=a_cb)
cb.set_ticklabels(
    [
        "$1\cdot10^{-8}$",
        "$2\cdot10^{-8}$",
        "$5\cdot10^{-8}$",
        "$1\cdot10^{-7}$",
        "$2\cdot10^{-7}$",
        "$5\cdot10^{-7}$",
        "$1\cdot10^{-6}$",
    ]
)
cb.set_label("strain")


vertexes, faces, normals, values = measure.marching_cubes_lewiner(
    strain_mag, 10**-8, spacing=(1.0, 1.0, 1.0)
)

np.amax(vertexes, axis=0)

f = open("isosurface.csv", "w")
f.write("easting (m),northing (m),depth (m)\n")
for vertex in vertexes:
    east_ordinal, north_ordinal, depth_ordinal = vertex
    east_interp = np.interp(east_ordinal, np.arange(ne4), east_grid4)
    north_interp = np.interp(north_ordinal, np.arange(nn4), north_grid4)
    depth_interp = np.interp(depth_ordinal, np.arange(nd4), depth_grid4)
    f.write("%.2f,%.2f,%.2f\n" % (east_interp, north_interp, depth_interp))

f.close()

f_iso = plt.figure()
a_iso = f_iso.add_subplot(111, projection="3d")
mesh = Poly3DCollection(vertexes[faces])
mesh.set_edgecolor("k")
a_iso.add_collection3d(mesh)

plt.show()
