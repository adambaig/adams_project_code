import matplotlib

matplotlib.use("Qt5agg")

from datetime import datetime
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import mplstereonet as mpls
import numpy as np
import sys
from obspy import UTCDateTime
import scipy.stats as st

from sms_moment_tensor.MT_math import mt_to_sdr, decompose_MT

sys.path.append("..")
from read_inputs import read_events, read_wells

PI = np.pi

events = read_events(filename="flipped_frac_events.csv")
wells = read_wells(local_coords=True)
for event in {k: v for k, v in events.items() if "dc MT" in v}:
    strike, dip, rake = mt_to_sdr(events[event]["dc MT"], conjugate=False)
    decomp = decompose_MT(events[event]["dc MT"])
    events[event]["p_trend"] = decomp["p_trend"]
    events[event]["b_trend"] = decomp["b_trend"]
    events[event]["t_trend"] = decomp["t_trend"]
    events[event]["p_plunge"] = decomp["p_plunge"]
    events[event]["b_plunge"] = decomp["b_plunge"]
    events[event]["t_plunge"] = decomp["t_plunge"]
    events[event]["strike"] = strike
    events[event]["dip"] = dip
    events[event]["rake"] = rake
#
# strain_fig, (p_ax, b_ax, t_ax) = mpls.subplots(1, 3, figsize=[16, 6])
mt_events = {k: v for k, v in events.items() if "dc MT" in v}


well_B_events = {k: v for (k, v) in mt_events.items() if (v["well"] == "B")}

stages = np.unique([v["stage"] for k, v in well_B_events.items()])
first_event_time = []

for stage in stages:
    stage_events_times = np.array(
        [k for (k, v) in well_B_events.items() if v["stage"] == stage]
    )
    stage_events_times.sort()
    first_event_time.append(UTCDateTime(stage_events_times[0]).timestamp)
i_time_sort = np.argsort(first_event_time)


def plot_perfs(axis, well, highlight_stage=None):
    stages = [k for k in well.keys() if "Stage" in k]
    for stage in stages:
        norths = [(v["top_north"], v["bottom_north"]) for v in well[stage].values()]
        easts = [(v["top_east"], v["bottom_east"]) for v in well[stage].values()]
        if stage == highlight_stage:
            for e, n in zip(easts, norths):
                axis.plot(e, n, "r", lw=9, zorder=6)
                axis.plot(e, n, "k", lw=10, zorder=5)
        else:
            for e, n in zip(easts, norths):
                axis.plot(e, n, "w", lw=7, zorder=6)
                axis.plot(e, n, "k", lw=8, zorder=5)
    return axis


avg_depth = {}
for frac_well in ["A", "B"]:

    perf_depths = []
    for stage in stages[i_time_sort]:
        stage_name = "Stage " + stage.split("-")[0]
        perf_avg_north = np.average(
            [
                0.5 * (v["top_north"] + v["bottom_north"])
                for v in wells[frac_well][stage_name].values()
            ]
        )
        perf_avg_east = np.average(
            [
                0.5 * (v["top_east"] + v["bottom_east"])
                for v in wells[frac_well][stage_name].values()
            ]
        )
        perf_depths.append(
            [
                0.5 * (v["top_tvdss"] + v["bottom_tvdss"])
                for v in wells[frac_well][stage_name].values()
            ]
        )
        for event_id in {
            k: v
            for k, v in events.items()
            if v["stage"] == stage and v["well"] == frac_well
        }.keys():
            events[event_id]["rel_north"] = (
                events[event_id]["northing"] - perf_avg_north
            )
            events[event_id]["rel_east"] = events[event_id]["easting"] - perf_avg_east
    avg_depth[frac_well] = np.average(perf_depths)

A_events = {
    k: v for k, v in events.items() if v["well"] == "A" and "rel_east" in v.keys()
}

B_events = {
    k: v for k, v in events.items() if v["well"] == "B" and "rel_east" in v.keys()
}


minor_ticks = np.arange(-500, 500, 50)
major_ticks = np.arange(-500, 500, 50)
fig, (ax2, ax1) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=[20, 10])
for ax in (ax1, ax2):
    ax.set_aspect("equal")
    ax.set_facecolor("0.95")
rel_east_A = [v["rel_east"] for v in A_events.values()]
rel_north_A = [v["rel_north"] for v in A_events.values()]
rel_east_B = [v["rel_east"] for v in B_events.values()]
rel_north_B = [v["rel_north"] for v in B_events.values()]
stage_num_A = [int(v["stage"].split("-")[0]) for v in A_events.values()]
stage_num_B = [int(v["stage"].split("-")[0]) for v in B_events.values()]
scatter = ax1.scatter(
    rel_east_A,
    rel_north_A,
    marker="o",
    c=stage_num_A,
    zorder=17,
    cmap="tab10",
    vmin=2.5,
    vmax=12.5,
    edgecolor="k",
)
ax2.scatter(
    rel_east_B,
    rel_north_B,
    marker="o",
    c=stage_num_B,
    zorder=17,
    cmap="tab10",
    vmin=2.5,
    vmax=12.5,
    edgecolor="k",
)

x_max = 300
xx, yy = np.mgrid[-x_max:x_max:100j, -x_max:x_max:100j]
positions = np.vstack([xx.ravel(), yy.ravel()])
values_A = np.vstack([rel_east_A, rel_north_A])
kernel_A = st.gaussian_kde(values_A)
contour_A = np.reshape(kernel_A(positions).T, xx.shape)
positions = np.vstack([xx.ravel(), yy.ravel()])
values_B = np.vstack([rel_east_B, rel_north_B])
kernel_B = st.gaussian_kde(values_B)
contour_B = np.reshape(kernel_B(positions).T, xx.shape)

for ax in (ax1, ax2):
    ax.plot([0, 0], [-1000, 1000], "0.2", lw=4, zorder=3)
    ax.plot([0, 0], [-1000, 1000], "0.8", lw=2.5, zorder=4)
    ax.plot([0, 0], [-30, 30], "orange", lw=8, zorder=6)
    ax.plot([0, 0], [-30, 30], "k", lw=9.5, zorder=5)

ax1.plot([-270, -270], [-1000, 1000], "0.2", lw=2.5, zorder=4)
ax2.plot([270, 270], [-1000, 1000], "0.2", lw=2.5, zorder=4)
ax2.plot([-270, -270], [-1000, 1000], "0.2", lw=2.5, zorder=4)
cf_set_A = ax1.contourf(xx, yy, contour_A, cmap="Greens", zorder=10, alpha=0.6)
cf_set_B = ax2.contourf(xx, yy, contour_B, cmap="Greens", zorder=10, alpha=0.6)

for ax in (ax1, ax2):
    ax.set_xticks(minor_ticks, minor=True)
    ax.set_yticks(minor_ticks, minor=True)
    ax.set_xticks(major_ticks, minor=False)
    ax.set_yticks(major_ticks, minor=False)
    ax.grid(which="both")
    ax.grid(which="minor", alpha=1)
    ax.set_aspect("equal")
    ax.set_yticklabels([])
    ax.set_xticklabels([])
    ax.tick_params(which="both", color="w")

ax1.set_xlim([-x_max, x_max])
ax1.set_ylim([-x_max, x_max])
ax1.set_title("Well A, Stages 3-12: Leading", fontsize=20)
ax2.set_title("Well B, Stages 3-10: Lagging", fontsize=20)
ax2.set_xlim([x1_B, x2_B])
f_cb, a_cb = plt.subplots()
cb = plt.colorbar(scatter, ax=a_cb)
cb.set_ticks(range(3, 13))
cb.set_label("Stage")


fig_d, (axd_B, axd_A) = plt.subplots(1, 2, sharey=True, figsize=[20, 10])
depth_bins = np.arange(-3000, -2000, 20)
elevations_A = [-v["depth"] for v in A_events.values()]
elevations_B = [-v["depth"] for v in B_events.values()]


axd_A.hist(elevations_A, bins=depth_bins, orientation="horizontal")
x1, x2 = axd_A.get_xlim()
axd_A.plot([0, 0.9 * x2], [-avg_depth["A"], -avg_depth["A"]], "orange", lw=3)
axd_B.hist(elevations_B, bins=depth_bins, orientation="horizontal")
x1, x2 = axd_B.get_xlim()
axd_B.plot([0, 0.9 * x2], [-avg_depth["B"], -avg_depth["B"]], "orange", lw=3)
plt.show()
