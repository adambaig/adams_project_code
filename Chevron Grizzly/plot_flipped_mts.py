import matplotlib

matplotlib.use("Qt5agg")

from datetime import datetime
import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
import sys
from sms_moment_tensor.MT_math import (
    decompose_MT,
    mt_to_sdr,
    mt_matrix_to_vector,
    general_to_DC,
)
from sms_moment_tensor.stress_inversions import (
    iterate_Micheal_stress_inversion,
)

sys.path.append("..")
from read_inputs import read_events, read_wells

PI = np.pi

events = read_events(filename="flipped_frac_events.csv")
