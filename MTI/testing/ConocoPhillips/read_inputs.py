import numpy as np

event_file = "C:\\Users\\adambaig\\Project\\MTI\\testing\\ConocoPhillips\\1_DataFetch\\EventCatalogFiltered.csv"


f = open(event_file)
head = f.readline()
lines = f.readlines()
f.close()
events = {}
for line in lines:
    lspl = line.split(",")
    event = lspl[0]
    events[event] = {
        "UTC time": lspl[1],
        "latitude": float(lspl[2]),
        "longitude": float(lspl[3]),
        "depth_km": float(lspl[4]),
        "magnitude": float(lspl[5]),
        "mag type": lspl[6].split()[0],
    }


def read_event(event):
    return events[event]


def read_events():
    return events
