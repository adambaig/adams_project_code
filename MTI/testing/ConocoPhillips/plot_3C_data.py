import matplotlib

matplotlib.use('Qt5agg')

import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory
import pyproj

from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.rotation import rotate_to_raypath

from read_inputs import read_event

seed_dir = "C:\\Users\\adambaig\\Project\\MTI\\testing\\ConocoPhillips\\2_WaveformProcessing\\mSeeds_processed\\"

seeds = glob.glob(seed_dir+"\\*.MSEED")
sta_xml = "C:\\Users\\adambaig\\Project\\MTI\\testing\\ConocoPhillips\\1_DataFetch\\Instrument_Response\\CP_ESA_Full.xml"

seed = seeds[0]
inv = read_inventory(sta_xml)
for seed in seeds:
    fig,ax = plt.subplots(1,3,figsize =[16,12])
    stream = read(seed)
    nslcs =  [trace.get_id().split('.') for trace in stream]
    net_stations = [(nslc[0]+'.'+nslc[1]) for nslc in nslcs]
    event = read_event(seed.split('\\')[-1].split('_')[0])
    event
    i_3c = -1
    for netstat in net_stations:
        st_3c = stream.select(network=netstat.split('.')[0], station=netstat.split('.')[1])
        channels = [s.get_id().split('.')[3][2] for s in st_3c]
        isThreeC = ("1" in channels and "2" in channels and "Z" in channels) or ("E" in channels and "N" in channels and "Z" in channels)
        if isThreeC:
            i_3c+=1
            st_3c.detrend()
            st_3c.filter.bandpass()
            st_3c.rotate("->ZNE", inventory= inv)
            waveforms = {"e": st_3c[2].data, "n": st_3c[1].data, "z": st_3c[0].data}
            station_inv = inv.select(network=netstat.split('.')[0], station=netstat.split('.')[1])
            sta_latitude = station_inv[0][0].latitude
            sta_longitude = station_inv[0][0].longitude
            p_raypath =
            rotated_waveforms = rotate_to_raypath(waveforms, p_raypath)
            time_axis = st_3c[0].times()
            ax[0].plot(time_axis,i_3c + rotated_waveforms["p"]/max(abs(rotated_waveforms["p"])))
            ax[1].plot(time_axis,i_3c + rotated_waveforms["v"]/max(abs(rotated_waveforms["v"])))
            ax[2].plot(time_axis,i_3c + rotated_waveforms["h"]/max(abs(rotated_waveforms["h"])))

plt.show()
