import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from obspy import read
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import decompose_MT
from sms_ray_modelling.raytrace import isotropic_ray_trace
from basic_operations.rotation import rotate_PVH
import sys

sys.path.append("..")
from read_inputs import read_stations, get_velocity_model


def sqa(x):
    return np.squeeze(np.array(x))


bwr = cm.get_cmap("bwr")
stations = {k: v for k, v in read_stations().items() if ".02." in k}
velocity_model = get_velocity_model()
Q = {"P": 80, "S": 60}
time_series = np.linspace(0, 8, 2001)

source = {
    "e": np.average([v["e"] for k, v in stations.items()]),
    "n": np.average([v["n"] for k, v in stations.items()]),
    "z": -2700,
}
fig, ax = plt.subplots(2, 3)
fig2, ax2 = plt.subplots(2, 3)
validation_matrix = np.zeros([6, 6])
for seedfile in glob.glob(r"basis seeds/m??_dc*.mseed"):
    st = read(seedfile)
    A_matrix = []
    amps = np.zeros(len(stations))
    for i_station, station in enumerate(stations):
        p_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "S")
        net_code, sta_code, loc_code, chn_code = station.split(".")
        three_c = st.select(station=sta_code)
        waveforms = {
            "n": three_c.select(channel="??N")[0].data,
            "e": three_c.select(channel="??E")[0].data,
            "z": three_c.select(channel="??Z")[0].data,
        }
        i_s_time = int(s_raypath["traveltime"] // time_series[1])
        i_p_time = int(p_raypath["traveltime"] // time_series[1])
        rot_waveforms, linearity = rotate_PVH(waveforms, [i_p_time - 5, i_p_time + 20])
        i_peak = np.argmax(abs(rot_waveforms["h"][i_s_time : i_s_time + 40])) + i_s_time
        amps[i_station] = back_project_amplitude(
            p_raypath, s_raypath, rot_waveforms["h"][i_peak], "SH", Q=80
        )
        A_matrix.append(inversion_matrix_row(s_raypath, "SH"))
    mt_gen, cn_gen, rsq_gen, mt_dc, cn_dev, rsq_dev = solve_moment_tensor(
        A_matrix, amps
    )

    max_amp = max(abs(amps))
    mt_component = seedfile.split("\\")[-1].split("_")[0]
    mt_1 = int(mt_component[1])
    mt_2 = int(mt_component[2])
    if mt_1 == mt_2:
        axis = ax[0, mt_1 - 1]
        axis2 = ax2[0, mt_1 - 1]
        i_row = mt_1 - 1
    else:
        axis = ax[1, mt_1 + mt_2 - 3]
        axis2 = ax2[1, mt_1 + mt_2 - 3]
        i_row = mt_1 + mt_2
    axis.set_aspect("equal")
    axis.scatter(
        [v["e"] for k, v in stations.items()],
        [v["n"] for k, v in stations.items()],
        c=amps,
        vmin=-max_amp,
        vmax=max_amp,
        cmap="bwr",
    )
    axis.set_xticklabels("")
    axis.set_yticklabels("")
    axis2.set_aspect("equal")
    axis2.scatter(
        [v["e"] for k, v in stations.items()],
        [v["n"] for k, v in stations.items()],
        c=sqa(A_matrix * np.matrix(mt_dc).T),
        vmin=-max_amp,
        vmax=max_amp,
        cmap="bwr",
    )
    axis2.set_xticklabels("")
    axis2.set_yticklabels("")
plt.show()
