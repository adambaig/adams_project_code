import numpy as np
import matplotlib.pyplot as plt
from obspy import Stream, Trace, UTCDateTime
from obspy.core.trace import Stats

from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)

from read_inputs import read_stations, get_velocity_model


def sqa(x):
    return np.squeeze(np.array(x))


def createTraceStats(network, n1, o1, d1, component):
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


stations = read_stations()
velocity_model = get_velocity_model()
source = {
    "e": np.average([v["e"] for k, v in stations.items()]),
    "n": np.average([v["n"] for k, v in stations.items()]),
    "z": -2700,
    "stress_drop": 1e6,
    "moment_magnitude": 0,
}
Q = {"P": 80, "S": 60}
time_series = np.linspace(0, 4, 2001)

waveforms = {}
sub_stations = {k: v for k, v in stations.items() if k.split(".")[2] == "02"}
comp_map = {"n": "N", "e": "E", "z": "Z"}
for Mij in ["m11", "m22", "m33", "m12", "m13", "m23"]:
    mt_1 = int(Mij[1]) - 1
    mt_2 = int(Mij[2]) - 1
    source["moment_tensor"] = np.matrix(np.zeros([3, 3]))
    st = Stream()
    seedtime = UTCDateTime.now()
    if mt_1 == mt_2:
        mt_ii = np.mod(mt_1 + 1, 3)
        mt_jj = np.mod(mt_1 + 2, 3)
        ## change back
        source["moment_tensor"][mt_ii, mt_ii] = 1
        source["moment_tensor"][mt_jj, mt_jj] = -1
    else:
        source["moment_tensor"][mt_1, mt_2] = 1
        source["moment_tensor"][mt_2, mt_1] = 1

    for i_station, station in enumerate(sub_stations):
        p_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "S")
        waveforms[station] = get_response(
            p_raypath, s_raypath, source, stations[station], Q, time_series, 0
        )
        net_code, sta_code, loc_code, chn_code = station.split(".")
        for comp in ["n", "e", "z"]:
            channel_obj = {
                "component": comp_map[comp],
                "channel": chn_code[:2] + comp_map[comp],
                "station": sta_code,
                "network": net_code,
                "location": loc_code,
            }
            xStats = createTraceStats(
                "CV", len(time_series), seedtime, time_series[1], channel_obj
            )
            tr = Trace(waveforms[station][comp], header=xStats)
            st += tr
    outfile = "basis seeds//" + Mij + "_dc_basis_waveforms.mseed"
    st.write(outfile)
