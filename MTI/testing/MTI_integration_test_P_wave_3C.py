import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import mt_matrix_to_vector, mt_vector_to_matrix
from sms_ray_modelling.raytrace import isotropic_ray_trace
from basic_operations.rotation import rotate_PVH
import sys

sys.path.append("..")
from read_inputs import read_stations, get_velocity_model


def sqa(x):
    return np.squeeze(np.array(x))


def mt_compare(mt1, mt2):
    mt1 = np.matrix(mt1)
    mt2 = np.matrix(mt2)
    norm_mt1 = mt1 / np.linalg.norm(mt1)
    norm_mt2 = mt2 / np.linalg.norm(mt2)
    return np.tensordot(norm_mt1, norm_mt2)


stations = {k: v for k, v in read_stations().items() if ".02." in k}
velocity_model = get_velocity_model()
Q = {"P": 80, "S": 60}
time_series = np.linspace(0, 4, 2001)
amps = np.zeros(len(stations))
source = {
    "e": np.average([v["e"] for k, v in stations.items()]),
    "n": np.average([v["n"] for k, v in stations.items()]),
    "z": -2700,
}
test_dir = r"C:\Users\adambaig\python_modules\sms_moment-tensor\test\synthetic_dataset"
fig, ax = plt.subplots(2, 3)
validation_matrix = np.zeros([6, 6])
for seedfile in glob.glob(test_dir + "//m??_basis*.mseed"):
    st = read(seedfile)
    A_matrix = []
    for i_station, station in enumerate(stations):
        p_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "S")
        net_code, sta_code, loc_code, chn_code = station.split(".")
        three_c = st.select(station=sta_code)
        waveforms = {
            "n": three_c.select(channel="??N")[0].data,
            "e": three_c.select(channel="??E")[0].data,
            "z": three_c.select(channel="??Z")[0].data,
        }
        i_p_time = int(p_raypath["traveltime"] // time_series[1])
        rot_waveforms, linearity = rotate_PVH(waveforms, [i_p_time - 5, i_p_time + 20])
        i_peak = np.argmax(abs(rot_waveforms["p"][i_p_time : i_p_time + 20])) + i_p_time

        amps[i_station] = back_project_amplitude(
            p_raypath, s_raypath, rot_waveforms["p"][i_peak], "P", Q=80
        )
        A_matrix.append(inversion_matrix_row(p_raypath, "P"))
    moment_tensor = solve_moment_tensor(A_matrix, amps)
    mt_gen = mt_matrix_to_vector(moment_tensor["general"])
    mt_component = seedfile.split("\\")[-1].split("_")[0]
    mt_1 = int(mt_component[1])
    mt_2 = int(mt_component[2])
    if mt_1 == mt_2:
        axis = ax[0, mt_1 - 1]
        i_row = mt_1 - 1
    else:
        axis = ax[1, mt_1 + mt_2 - 3]
        i_row = mt_1 + mt_2
    axis.set_aspect("equal")
    axis.plot(amps, sqa(A_matrix * np.matrix(mt_gen).T), "k.")
    x1, x2 = axis.get_xlim()
    y1, y2 = axis.get_ylim()
    xy_max = 1.05 * max([-x1, x2, -y1, y2])
    axis.plot([-xy_max, xy_max], [-xy_max, xy_max], "r--", zorder=-10)
    axis.text(-0.8 * xy_max, 0.8 * xy_max, ("M$_{%i%i}$" % (mt_1, mt_2)))
    axis.set_xlim([-xy_max, xy_max])
    axis.set_ylim([-xy_max, xy_max])
    axis.set_xticklabels("")
    axis.set_yticklabels("")
    validation_matrix[i_row, :] = mt_gen
    MT = mt_vector_to_matrix(mt_gen)
    MT = MT / np.linalg.norm(MT)
    MT_in = np.matrix(np.zeros([3, 3]))
    if mt_1 == mt_2:
        MT_in[mt_1 - 1, mt_1 - 1] = 1
    else:
        MT_in[mt_1 - 1, mt_2 - 1] = 1
        MT_in[mt_2 - 1, mt_1 - 1] = 1
    MT_in = MT_in / np.linalg.norm(MT_in)

    print(mt_compare(MT_in, moment_tensor["general"]))

fig.text(0.05, 0.5, "modelled", va="center", rotation=90)
fig.text(0.5, 0.05, "observed and back-projected", ha="center")

vmax = np.log10(max([np.amax(validation_matrix), -np.amin(validation_matrix)]))

ff = plt.figure()
aa = ff.add_axes([0.1, 0.1, 0.6, 0.8])
aa.set_aspect("equal")
ij_pos = np.where(validation_matrix > 0)
ij_neg = np.where(validation_matrix < 0)

pos_matrix = 1e-9 * np.ones([6, 6])
neg_matrix = np.zeros([6, 6])
for ii in range(6):
    for jj in range(6):
        if validation_matrix[ii, jj] > 0:
            pos_matrix[ii, jj] = validation_matrix[ii, jj]
        else:
            neg_matrix[ii, jj] = -validation_matrix[ii, jj]
aa.pcolor(np.log10(pos_matrix), cmap="hot_r", vmax=vmax, vmin=vmax - 4)
aa.pcolor(np.log10(neg_matrix), cmap="PuBuGn", vmax=vmax, vmin=vmax - 4)

scale = np.linspace(0, 1, 100)
aa.set_yticklabels("")
aa.set_xticklabels("")
aa.set_xticks(np.arange(0.5, 6), minor=True)
aa.set_yticks(np.arange(0.5, 6), minor=True)
aa.set_xticklabels(
    ["M$_{11}$", "M$_{22}$", "M$_{33}$", "M$_{12}$", "M$_{13}$", "M$_{23}$"],
    minor=True,
)
aa.set_yticklabels(
    ["M$_{11}$", "M$_{22}$", "M$_{33}$", "M$_{12}$", "M$_{13}$", "M$_{23}$"],
    minor=True,
)
aa.set_ylabel("input solution")
aa.set_xlabel("output solution")

aa.set_ylim([6, 0])

cb = ff.add_axes([0.72, 0.1, 0.08, 0.8])
cb.pcolor(
    [0, 1],
    scale,
    np.array(np.matrix(scale).T * np.matrix([1, 1])),
    cmap="hot_r",
)
cb.pcolor(
    [0, 1],
    -scale,
    np.array(np.matrix(scale).T * np.matrix([1, 1])),
    cmap="PuBuGn",
)

cb.yaxis.set_label_position("right")
cb.yaxis.tick_right()
cb.set_ylabel("amplitude")
cb.set_xticklabels("")
cb.set_yticks([-1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1.0])
cb.set_yticklabels(
    [
        "$\minus$100%",
        "$\minus$10%",
        "$\minus$1%",
        "$\minus$0.1%",
        "0.01%\n$\minus$0.01%",
        "0.1%",
        "1%",
        "10%",
        "100%",
    ]
)
ff.savefig("validation_matrix.png")
fig.savefig("regression_per_term.png")

plt.show()
