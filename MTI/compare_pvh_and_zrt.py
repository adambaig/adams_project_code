import matplotlib.pyplot as plt
import numpy as np

pvh, zrt = {}, {}
f = open(f"Rotation_PVH_Test//rotate_pvh_comparison_WTX.csv")
head = f.readline()
lines = f.readlines()
f.close()

for line in lines:
    lspl = line.split(",")
    pvh[lspl[0]] = {"original": float(lpsl[1]), "recalc": float(lspl[2])}

f = open(f"Rotation_PVH_Test//rotate_zrt_comparison_WTX.csv")
head = f.readline()
lines = f.readlines()
f.close()

for line in lines:
    lspl = line.split(",")
    zrt[lspl[0]] = {"original": float(lspl[1]), "recalc": float(lspl[2])}
