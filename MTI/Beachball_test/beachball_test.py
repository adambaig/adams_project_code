import matplotlib.pyplot as plt
from obspy.imaging.mopad_wrapper import beach
import mplstereonet as mpls
from sms_moment_tensor.MT_math import conjugate_sdr


strike, dip, rake = 322.5, 78.3, 83.2
strike_aux, dip_aux, rake_aux = conjugate_sdr(strike, dip, rake)

fig = plt.figure(figsize=[16, 5])
a1 = fig.add_subplot(1, 3, 1)
beachball = beach((strike, dip, rake))
a1.add_collection(beachball)
a1.set_xlim([-110, 110])
a1.set_ylim([-110, 110])
a1.set_aspect("equal")
a1.set_title("beachball from obspy")
a1.axis("off")

a2 = fig.add_subplot(1, 3, 3, projection="equal_area_stereonet")
a2.plane(strike, dip, "k")
a2.plane(strike_aux, dip_aux, "k")
a2.grid()
a2.set_title("equal area\n\n")

a3 = fig.add_subplot(1, 3, 2, projection="equal_angle_stereonet")
a3.plane(strike, dip, "k")
a3.plane(strike_aux, dip_aux, "k")
a3.set_title("equal angle\n\n")
a3.grid()


fig.savefig("comparison.png")
