import matplotlib.pyplot as plt
from obspy import read_inventory
import pyproj as pr

inv = read_inventory("OXY_SNY//OXY_SNY_Full.xml")
stations = {}

latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init="epsg:32614")
for network in inv:
    for station in network:
        easting, northing = pr.transform(
            latlon_proj, out_proj, station.longitude, station.latitude
        )
        stations[f"{network.code}.{station.code}"] = {
            "easting": easting,
            "northing": northing,
        }

f = open("OXY_SNY//Oxy_Snyder_Full_Catalog_With_Datetimes_20200107_large.csv")
head = f.readline()
lines = f.readlines()
f.close()

events = {}
for line in lines:
    lspl = line.split(",")
    long, lat = [float(s) for s in lspl[2:4]]
    easting, northing = pr.transform(latlon_proj, out_proj, long, lat)
    events[lspl[0]] = {"easting": easting, "northing": northing}


for event in events:
    fig, ax = plt.subplots()
    ax.set_aspect("equal")
    ax.plot(
        event["easting"],
        event["northing"],
        "o",
        color="firebrick",
        markeredgecolor="0.2",
    )
    ax.plot(
        [v["easting"] for v in stations.values()],
        [v["northing"] for v in stations.values()],
        "o",
        color="0.5",
        markeredgecolor="0.2",
    )
