import numpy as np
from sms_moment_tensor.MT_math import sdr_to_mt

Artemis_DC = [
    {"strike": 194.4, "dip": 49.1, "rake": -144.1},
    {"strike": 86.3, "dip": 49.9, "rake": -21.3},
    {"strike": 91.6, "dip": 43.5, "rake": -14.4},
    {"strike": 181.3, "dip": 76.8, "rake": 174.6},
    {"strike": 180.1, "dip": 70.1, "rake": -166.0},
]

RegMTI = [
    {"strike": 93.5, "dip": 68.0, "rake": -6.6},
    {"strike": 81.5, "dip": 60.3, "rake": -33.6},
    {"strike": 90.3, "dip": 59, "rake": -15},
    {"strike": 93.8, "dip": 87.9, "rake": 17},
    {"strike": 187.9, "dip": 81.4, "rake": -151.5},
]


for a_sol, s_sol in zip(Artemis_DC, RegMTI):
    a_mt = sdr_to_mt(a_sol["strike"], a_sol["dip"], a_sol["rake"])
    s_mt = sdr_to_mt(s_sol["strike"], s_sol["dip"], s_sol["rake"])
    print("%.3f" % np.tensordot(s_mt, a_mt))
    print("%.1f" % (np.arccos(np.tensordot(s_mt, a_mt)) * 180 / np.pi))
