import glob

import pickle


mixed_pickles = glob.glob("Pickles//WTX//*.pickle")

for pickle_file in mixed_pickles:
    pickle_out = pickle.load(open(pickle_file, "rb"))
    if any("WTX" in s for s in pickle_out["ampDict"]):
        pass
    if any("ISM" in s for s in pickle_out["ampDict"]):
        print(pickle_file)
