import gspread
from gspread.models import Cell
import numpy as np
from oauth2client.service_account import ServiceAccountCredentials
from sms_moment_tensor.MT_math import sdr_to_mt

scope = [
    "https://spreadsheets.google.com/feeds",
    "https://www.googleapis.com/auth/drive",
]
creds = ServiceAccountCredentials.from_json_keyfile_name("client-secret.json", scope)
client = gspread.authorize(creds)

sheet = client.open("OK SLU solutions").sheet1
events = sheet.get_all_records()
cells = []
for i_event, event in enumerate(events):
    if event["Artemis DC Strike"] != "":
        a_mt = sdr_to_mt(
            event["Artemis DC Strike"],
            event["Artemis DC Dip"],
            event["Artemis DC Rake"],
        )
        s_mt = sdr_to_mt(event["SLU Strike"], event["SLU Dip"], event["SLU Rake"])
        similarity = np.float64(np.tensordot(s_mt, a_mt))
        cells.append(Cell(row=i_event + 2, col=15, value=similarity))


sheet.update_cells(cells)
