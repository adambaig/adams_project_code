import matplotlib

matplotlib.use("Qt5Agg")

import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy.imaging.mopad_wrapper import beach
from obspy import read_inventory, UTCDateTime
import os
import pickle
import pyproj as pr
from six import iteritems
import time
import re
from subprocess import Popen, PIPE
from copy import deepcopy

from ExternalPrograms.FocMec.FocMecSupport import runFocMec
from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from General.BasicFunctions import listToArray2D
from logiklyst.PluginsNOC.Locate import (
    locatorChecks,
    formHinvInputs,
    runHypoInv,
)
from NocMeta.Meta import NOC_META
from sms_moment_tensor.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import (
    mt_matrix_to_vector,
    mt_to_sdr,
    clvd_iso_dc,
    average_MTs,
)

from sms_moment_tensor.plotting import (
    plot_regression,
    plot_beachballs,
    plot_beachball_from_sdr,
    plot_fault_planes_from_Focmec,
)
from sms_ray_modelling.raytrace import isotropic_ray_trace, takeoff_angle_Focmec


def createDir(aDir):
    if not os.path.isdir(aDir):
        os.makedirs(aDir)


athena_code = "WTX"

if not os.path.exists("Station_xmls//" + athena_code + "_Full.xml"):
    inv = formStaXml(outDir="Station_xmls//.", athenaCode=athena_code)
else:
    inv = read_inventory(r"Station_xmls/" + athena_code + "_Full.xml")


def formFocMecInputFile(
    mainDir, sourceTag, eveID, ampDict, pTakeoff, sTakeoff, aziDict
):
    inputPath = "%s/%s/FocMec/%s.inp" % (mainDir, sourceTag, eveID)
    createDir(os.path.dirname(inputPath))
    # The [positive (peak), negative (trough)] sense codes, relative to channel orientation
    senseLookup = {"RTP": ["C", "D"], "RTV": ["F", "B"], "RTH": ["L", "R"]}
    with open(inputPath, "w") as aFile:
        # Give header to file
        aFile.write("eventID:%s, runAtTime:%s\n" % (eveID, UTCDateTime(time.time())))
        # Loop through polarity pick observations
        for nslc in ampDict.keys():
            net, sta, loc, cha = nslc.split(".")
            nsl = ".".join([net, sta, ""])
            # Note that in ampDict positive is trough and negative is peak
            ampSign = ampDict[nslc]["edgeTypes"][0]
            if ampSign > 0:
                sense = senseLookup[cha][1]
            elif ampSign < 0:
                sense = senseLookup[cha][0]
            else:
                continue
            takeOff = pTakeoff[nsl] if cha[-1] == "P" else sTakeoff[nsl]
            staTag = sta if len(sta) <= 4 else sta[-4:]
            aFile.write(
                "{:4}{:8.2f}{:8.2f}{:1}\n".format(staTag, aziDict[nsl], takeOff, sense)
            )
    return inputPath


latlon_proj = pr.Proj("epsg:4326")
out_proj = pr.Proj("epsg:" + str(NOC_META[athena_code]["epsg"]))
pickles = glob.glob("Pickles//" + athena_code + "//106600.pickle")

stations = {}
for network in inv:
    net_code = network.code
    for station in network:
        net_stat = net_code + "." + station.code
        stations[net_stat] = {
            "latitude": station.latitude,
            "longitude": station.longitude,
            "elevation": station.elevation,
        }
        easting, northing = pr.transform(
            latlon_proj, out_proj, station.latitude, station.longitude
        )
        stations[net_stat]["e"] = easting
        stations[net_stat]["n"] = northing
        stations[net_stat]["z"] = station.elevation

for pickle_file in pickles:
    pickle_out = pickle.load(open(pickle_file, "rb"))
    event_id = os.path.basename(pickle_file).split(".")[0]
    mean_frequency = np.sqrt(pickle_out["bpFreqs"][0] * pickle_out["bpFreqs"][1])
    event_latitude = pickle_out["eveDict"]["event"]["origin"]["latitude"]
    event_longitude = pickle_out["eveDict"]["event"]["origin"]["longitude"]

    event_easting, event_northing = pr.transform(
        latlon_proj, out_proj, event_latitude, event_longitude
    )
    event_elevation = -1000.0 * pickle_out["eveDict"]["event"]["origin"]["depth"]
    event = {"e": event_easting, "n": event_northing, "z": event_elevation}
    velocity_model = pickle_out["velDict"]
    p_raypaths, s_raypaths = {}, {}
    for nsl in stations.keys():
        p_raypaths[nsl] = isotropic_ray_trace(event, stations[nsl], velocity_model, "P")
        s_raypaths[nsl] = isotropic_ray_trace(event, stations[nsl], velocity_model, "S")

    aziDict = {
        nsl + ".": (np.arctan2(x["e"], x["n"]) * 180 / np.pi)
        for nsl, x in {
            nsl: p_raypaths[nsl]["hrz_slowness"] for nsl in stations.keys()
        }.items()
    }

    p_takeoff = {
        key + ".": takeoff_angle_Focmec(ray_path)
        for key, ray_path in iteritems(p_raypaths)
    }
    s_takeoff = {
        key + ".": takeoff_angle_Focmec(ray_path)
        for key, ray_path in iteritems(s_raypaths)
    }

    focMecInPath = formFocMecInputFile(
        ".", athena_code, event_id, pickle_out["ampDict"], p_takeoff, s_takeoff, aziDict
    )
    sdrList, focMecOutPath = runFocMec(
        focMecInPath, maxPolErr=len(pickle_out["ampDict"])
    )
    staLocDict = {
        ".".join([net.code, sta.code, cha.location_code]): [sta.longitude, sta.latitude]
        for net in inv
        for sta in net
        for cha in sta
    }

    A_matrix = []
    right_hand_side = []
    for nslc, amp_data in pickle_out["ampDict"].items():
        net_stat = ".".join(nslc.split(".")[:2])
        phase = nslc[-1]
        s_raypath = isotropic_ray_trace(event, stations[net_stat], velocity_model, "S")
        raypath = {
            "P": isotropic_ray_trace(event, stations[net_stat], velocity_model, "P"),
            "V": s_raypath,
            "H": s_raypath,
        }

        A_matrix.append(inversion_matrix_row(raypath[phase], phase))
        right_hand_side.append(
            back_project_amplitude(
                raypath["P"],
                raypath["V"],
                amp_data["amps"][0],
                phase,
                frequency=mean_frequency,
            )
        )
    moment_tensor = solve_moment_tensor(A_matrix, right_hand_side)
    fig = plt.figure(figsize=[16, 9])
    NSLCs = list(pickle_out["ampDict"].keys())
    axDCRegression = fig.add_subplot(2, 3, 1)
    axDc = fig.add_subplot(2, 3, 4)
    axGeneralRegression = fig.add_subplot(2, 3, 2)
    axGen = fig.add_subplot(2, 3, 5)
    axFocMecAll = fig.add_subplot(2, 3, 3, projection="stereonet")
    axFocMecSelect = fig.add_subplot(2, 3, 6)
    strike1, dip1, rake1, strike2, dip2, rake2 = mt_to_sdr(moment_tensor["dc"])
    clvd, iso, dc = clvd_iso_dc(moment_tensor["general"])
    fig.text(0.10, 0.07, "plane 1\nstrike:\ndip\nrake:")
    fig.text(
        0.19,
        0.07,
        (
            "\n%.1f$^{\circ}$\n%.1f$^{\circ}$\n%.1f$^{\circ}$"
            % (strike1 % 360, dip1, rake1)
        ).replace("-", "$-$"),
        ha="right",
    )
    fig.text(0.22, 0.07, "plane 2\nstrike:\ndip\nrake:")
    fig.text(
        0.31,
        0.07,
        (
            "\n%.1f$^{\circ}$\n%.1f$^{\circ}$\n%.1f$^{\circ}$"
            % (strike2 % 360, dip2, rake2)
        ).replace("-", "$-$"),
        ha="right",
    )
    fig.text(0.48, 0.07, "source decomposition\nDC\nCLVD\nISO")
    fig.text(
        0.56,
        0.07,
        ("\n%.1f%%\n%.1f%%\n%.1f%%" % (dc, clvd, iso)).replace("-", "$-$"),
        ha="right",
    )
    fig.tight_layout()
    plot_regression(
        right_hand_side,
        moment_tensor["dc"],
        A_matrix,
        axDCRegression,
        fig=fig,
        nslc_tags=NSLCs,
    )
    plot_regression(
        right_hand_side,
        moment_tensor["general"],
        A_matrix,
        axGeneralRegression,
        fig=fig,
        nslc_tags=NSLCs,
    )
    if len(sdrList) > 0:
        plot_fault_planes_from_Focmec(focMecInPath, focMecOutPath, axFocMecAll, fig)

        fps_mt = average_MTs(sdrList)
        fps_strike1, fps_dip1, fps_rake1, fps_strike2, fps_dip2, fps_rake2 = mt_to_sdr(
            fps_mt
        )
        plot_beachball_from_sdr(
            (fps_strike1 + 360) % 360.0,
            fps_dip1,
            fps_rake1,
            axFocMecSelect,
            fig=fig,
        )
    plot_beachballs(moment_tensor, axDc, axGen)
    dc_similarity = (
        np.tensordot(moment_tensor["dc"], fps_mt)
        / np.linalg.norm(moment_tensor["dc"])
        / np.linalg.norm(fps_mt)
    )
    fig.text(0.75, 0.07, "plane 1\nstrike:\ndip\nrake:")
    fig.text(
        0.84,
        0.07,
        (
            "\n%.1f$^{\circ}$\n%.1f$^{\circ}$\n%.1f$^{\circ}$"
            % (fps_strike1 % 360, fps_dip1, fps_rake1)
        ).replace("-", "$-$"),
        ha="right",
    )
    fig.text(0.87, 0.07, "plane 2\nstrike:\ndip\nrake:")
    fig.text(
        0.96,
        0.07,
        (
            "\n%.1f$^{\circ}$\n%.1f$^{\circ}$\n%.1f$^{\circ}$"
            % (fps_strike2 % 360, fps_dip2, fps_rake2)
        ).replace("-", "$-$"),
        ha="right",
    )
    fig.text(0.75, 0.01, "similarity between DC MT and FPS: %.3f" % dc_similarity)
    fig.savefig(athena_code + "//plots//" + event_id + ".png")

pickle_out
np.linalg.norm(
    [stations["TX.PB19"]["e"] - event["e"], stations["TX.PB19"]["n"] - event["n"]]
)
plt.show()


pickle_out["eveDict"]
