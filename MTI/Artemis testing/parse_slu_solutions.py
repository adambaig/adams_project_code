import matplotlib

matplotlib.use("Qt5Agg")

import matplotlib.pyplot as plt

import gspread
from gspread.models import Cell
import numpy as np
from oauth2client.service_account import ServiceAccountCredentials
from selenium import webdriver
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon


driver_path = r"C:\Users\adambaig\Documents\chromedriver\chromedriver"

driver = webdriver.Chrome(executable_path=driver_path)


scope = [
    "https://spreadsheets.google.com/feeds",
    "https://www.googleapis.com/auth/drive",
]
creds = ServiceAccountCredentials.from_json_keyfile_name("client-secret.json", scope)
client = gspread.authorize(creds)

sheet = client.open("OK SLU solutions").worksheet("Sheet3")


f = open("OSA_buffer.csv")
head = f.readline()
lines = f.readlines()
f.close()

polygon = Polygon([(float(l.split(",")[0]), float(l.split(",")[1])) for l in lines])


url_base = "http://www.eas.slu.edu/eqc/eqc_mt/MECH.NA/"
events = sheet.get_all_records()
cells = []

fig, ax = plt.subplots()
for i_event, event in enumerate(events):
    timecode = event["Mechanism"].split()[0]
    point = Point(event["Longitude"], event["Latitude"])
    if polygon.contains(point):
        ndk = url_base + timecode + "/" + timecode + ".ndk"
        driver.get(ndk)
        strike, dip, rake = driver.page_source.split("</pre>")[0].split()[-3:]
        ax.plot(event["Longitude"], event["Latitude"], "ro")
    else:
        ax.plot(event["Longitude"], event["Latitude"], "k.")
plt.show()

#         cells.append(Cell(row=i_event + 2, col=9, value=strike))
#         cells.append(Cell(row=i_event + 2, col=10, value=dip))
#         cells.append(Cell(row=i_event + 2, col=11, value=rake))
#
#
# sheet.update_cells(cells)
