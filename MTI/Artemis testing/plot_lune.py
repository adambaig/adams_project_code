import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np

PI = np.pi
D2R = PI / 180


def hammer_projection(lat_deg, lon_deg):
    lat_rad, lon_rad = D2R * lat_deg, D2R * lon_deg
    x = (
        2
        * np.cos(lat_rad)
        * np.sin(lon_rad / 2)
        / np.sqrt(1 + np.cos(lat_rad) * np.cos(lon_rad / 2))
    )
    y = np.sin(lat_rad) / np.sqrt(1 + np.cos(lat_rad) * np.cos(lon_rad / 2))
    return x, y


fig, ax = plt.subplots(figsize=[6, 12])
ax.set_aspect("equal")

for lon in np.linspace(-30, 30, 3):
    graticule_line = []
    for lat in np.linspace(-90, 90, 181):
        graticule_line.append(hammer_projection(lat, lon))
    grat_x, grat_y = [g[0] for g in graticule_line], [g[1] for g in graticule_line]
    ax.plot(grat_x, grat_y, "k", zorder=0, lw=1)

for lon in np.linspace(-20, 20, 5):
    graticule_line = []
    for lat in np.linspace(-80, 80, 181):
        graticule_line.append(hammer_projection(lat, lon))
    grat_x, grat_y = [g[0] for g in graticule_line], [g[1] for g in graticule_line]
    ax.plot(grat_x, grat_y, "0.5", zorder=-1, lw=0.5)

for lat in np.linspace(-80, 80, 17):
    graticule_line = []
    for lon in np.linspace(-30, 30, 61):
        graticule_line.append(hammer_projection(lat, lon))
    grat_x, grat_y = [g[0] for g in graticule_line], [g[1] for g in graticule_line]
    ax.plot(grat_x, grat_y, "0.5", zorder=-1, lw=0.5)

grat_x, _ = hammer_projection(0, 30)
ax.plot([-grat_x, grat_x], [0, 0], "k", zorder=0)

grat_x, grat_y = hammer_projection(np.arcsin(np.sqrt(25 / 33)) / D2R, -30)
ax.plot([-grat_x, grat_x], [-grat_y, grat_y], "k", zorder=0)

ax.set_axis_off()

plt.show()
