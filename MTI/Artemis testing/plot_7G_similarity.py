import matplotlib.pyplot as plt
import numpy as np


f = open("7G//Artemis_before_after_comparison.csv")
head = f.readline()
lines = f.readlines()
f.close()

n_lines = len(lines)
similarity = np.zeros(n_lines)
for i_line, line in enumerate(lines):
    lspl = line.split(",")
    similarity[i_line] = float(lspl[9])

fig, ax = plt.subplots()
bins = np.arange(-0.40, 1.05, 0.05)
ax.hist(similarity, bins)
ax.set_xlabel("similarity")
ax.set_ylabel("count")
y1, y2 = ax.get_ylim()
ax.plot([0, 0], [y1, y2], "k", lw=1)
ax.set_ylim(y1, y2)
ax.set_xlim(-0.4, 1)
