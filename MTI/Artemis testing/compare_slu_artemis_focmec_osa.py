import numpy as np
from sms_moment_tensor.MT_math import similarity_from_sdr

# f = open("Artemis testing//WTX SLU solutions.csv")
f = open("Artemis testing//OK SLU solutions - 2018.csv")
head = f.readline()
lines = f.readlines()
f.close()

events = {}
for line in lines:
    lspl = line.split(",")
    events[lspl[7]] = {
        "date": lspl[0],
        "time": lspl[1],
        "latitude": float(lspl[2]),
        "longitude": float(lspl[3]),
        "depth (km)": float(lspl[4]),
        "magnitude": float(lspl[5]),
        "slu": {
            "strike": float(lspl[8]),
            "dip": float(lspl[9]),
            "rake": float(lspl[10]),
        },
        "artemis": {
            "strike": float(lspl[11]),
            "dip": float(lspl[12]),
            "rake": float(lspl[13]),
        },
        "focmec": {
            "strike": float(lspl[15]),
            "dip": float(lspl[16]),
            "rake": float(lspl[17]),
        },
    }

similarity = {"artemis": [], "focmec": []}
for event in events.values():
    similarity["artemis"].append(similarity_from_sdr(event["slu"], event["artemis"]))
    similarity["focmec"].append(similarity_from_sdr(event["slu"], event["focmec"]))
np.median(similarity["artemis"])
