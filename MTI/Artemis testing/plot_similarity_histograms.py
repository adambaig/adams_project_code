import matplotlib

matplotlib.use("Qt5Agg")

import matplotlib.pyplot as plt
import numpy as np

events = {}
# fig,ax = plt.subplots(1,4, figsize=[16,6] )
for i_network, network in enumerate(["CUA", "VE", "WTX"]):
    f = open(
        "C:\\Users\\adambaig\\Project\\MTI\\Artemis testing\\"
        + network
        + "\\Artemis_Focmec_comparison.csv"
    )
    head = f.readline()
    lines = f.readlines()
    f.close()

    events[network] = {}
    for line in lines:
        lspl = line.split(",")
        events[network][lspl[0]] = {
            "latitude": float(lspl[1]),
            "longitude": float(lspl[2]),
            "similarity": float(lspl[-1]),
        }

    h_bins = np.linspace(-1, 1, 20)

    # if network == "WTX":
    #     similarity_culb = [
    #         v["similarity"] for v in events.values() if v["longitude"] < -104
    #     ]
    #     similarity_reev = [
    #         v["similarity"] for v in events.values() if v["longitude"] > -104
    #     ]
    # ax[2].hist(similarity_culb, h_bins)
    # ax[3].hist(similarity_reev, h_bins)
    # else:
    # ax[i_network].hist([v["similarity"] for v in events.values()], h_bins)
# for ii in range(4):
#     ax[ii].set_xlabel('similarity')
# ax[0].set_title('Cuadrilla')
# ax[1].set_title('Vesta')
# ax[2].set_title('Culberson County')
# ax[3].set_title('Reeves County')
# plt.show()
len(events["VE"])
([c["similarity"] for c in events["WTX"].values()])
