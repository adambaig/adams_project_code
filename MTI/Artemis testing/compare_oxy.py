import numpy as np
from sms_moment_tensor.MT_math import sdr_to_mt

Artemis_DC = [
    {"strike": 99.4, "dip": 51.1, "rake": -34.9},
    {"strike": 125.8, "dip": 66.2, "rake": -4.2},
]

SLU = [
    {"strike": 195, "dip": 90, "rake": -155},
    {"strike": 202, "dip": 85, "rake": -155},
]

USGS = [
    {"strike": 195, "dip": 82, "rake": -160},
    {"strike": 205, "dip": 73, "rake": -153},
]


for a_sol, s_sol, u_sol in zip(Artemis_DC, SLU, USGS):
    a_mt = sdr_to_mt(a_sol["strike"], a_sol["dip"], a_sol["rake"])
    s_mt = sdr_to_mt(s_sol["strike"], s_sol["dip"], s_sol["rake"])
    u_mt = sdr_to_mt(u_sol["strike"], u_sol["dip"], u_sol["rake"])
    print("%.3f" % np.tensordot(u_mt, a_mt))
    print("%.1f" % (np.arccos(np.tensordot(u_mt, a_mt)) * 180 / np.pi))
