import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np

from sms_moment_tensor.MT_math import sdr_to_mt

f = open("SLU_solns_20561.csv")
head = f.readline()
lines = f.readlines()
f.close()


Artemis_soln = {"strike": 327.7, "dip": 41.4, "rake": 28.2}
Artemis_MT = sdr_to_mt(
    Artemis_soln["strike"], Artemis_soln["dip"], Artemis_soln["rake"]
)

SLU = {}
similarity = []
for line in lines:
    lspl = [float(s) for s in line.split(",")[1:]]
    SLU[lspl[0]] = {
        "strike": lspl[1],
        "dip": lspl[2],
        "rake": lspl[3],
        "mw": lspl[4],
        "fit": lspl[5],
    }

for id, event in SLU.items():
    SLU[id]["MT"] = sdr_to_mt(event["strike"], event["dip"], event["rake"])
    SLU[id]["similarity"] = np.tensordot(Artemis_MT, SLU[id]["MT"])

depths = list(SLU.keys())
fits, similarities = np.array([[v["fit"], v["similarity"]] for v in SLU.values()]).T
fig, ax = plt.subplots()
ax2 = ax.twiny()
p1 = ax.plot(fits, depths, "firebrick")
p2 = ax2.plot(similarities, depths, "royalblue")
ax.set_xlim([0, 1])
ax2.set_xlim([-1, 1])
ax.set_ylim([30, 0])
ax2.set_xlabel("similarity with Artemis")
ax.set_xlabel("SLU fit")
ax.set_ylabel("Depth")
ax.legend([p1[0], p2[0]], ["SLU fit", "similarity"], loc="lower right")
plt.show()
