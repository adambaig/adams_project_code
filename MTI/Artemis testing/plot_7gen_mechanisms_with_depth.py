import matplotlib

matplotlib.use("Qt5Agg")

from copy import deepcopy
import glob
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from obspy.imaging.mopad_wrapper import beach
from obspy import read_inventory, UTCDateTime
import os
import pickle
import pyproj as pr
import re
from six import iteritems
from subprocess import Popen, PIPE
import time


from ExternalPrograms.FocMec.FocMecSupport import runFocMec
from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from General.BasicFunctions import listToArray2D
from logiklyst.PluginsNOC.Locate import (
    locatorChecks,
    formHinvInputs,
    runHypoInv,
)
from NocMeta.Meta import NOC_META
from sms_moment_tensor.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import (
    mt_matrix_to_vector,
    mt_to_sdr,
    clvd_iso_dc,
    average_MTs,
)

from sms_moment_tensor.plotting import (
    plot_regression,
    plot_beachballs,
    plot_beachball_from_sdr,
    plot_fault_planes_from_Focmec,
)
from sms_ray_modelling.raytrace import isotropic_ray_trace, takeoff_angle_Focmec


def createDir(aDir):
    if not os.path.isdir(aDir):
        os.makedirs(aDir)


athena_code = "7G"

brg = cm.get_cmap("brg")
if not os.path.exists("Station_xmls//" + athena_code + "_Full.xml"):
    inv = formStaXml(outDir="Station_xmls//.", athenaCode=athena_code)
else:
    inv = read_inventory(r"Station_xmls/" + athena_code + "_Full.xml")


def formFocMecInputFile(
    mainDir, sourceTag, eveID, ampDict, pTakeoff, sTakeoff, aziDict
):
    inputPath = "%s/%s/FocMec/%s.inp" % (mainDir, sourceTag, eveID)
    createDir(os.path.dirname(inputPath))
    # The [positive (peak), negative (trough)] sense codes, relative to channel orientation
    senseLookup = {"RTP": ["C", "D"], "RTV": ["F", "B"], "RTH": ["L", "R"]}
    with open(inputPath, "w") as aFile:
        # Give header to file
        aFile.write("eventID:%s, runAtTime:%s\n" % (eveID, UTCDateTime(time.time())))
        # Loop through polarity pick observations
        for nslc in ampDict.keys():
            net, sta, loc, cha = nslc.split(".")
            nsl = ".".join([net, sta, ""])
            # Note that in ampDict positive is trough and negative is peak
            ampSign = ampDict[nslc]["edgeTypes"][0]
            if ampSign > 0:
                sense = senseLookup[cha][1]
            elif ampSign < 0:
                sense = senseLookup[cha][0]
            else:
                continue
            takeOff = pTakeoff[nsl] if cha[-1] == "P" else sTakeoff[nsl]
            staTag = sta if len(sta) <= 4 else sta[-4:]
            aFile.write(
                "{:4}{:8.2f}{:8.2f}{:1}\n".format(staTag, aziDict[nsl], takeOff, sense)
            )
    return inputPath


latlon_proj = pr.Proj("epsg:4326")
out_proj = pr.Proj("epsg:" + str(NOC_META[athena_code]["epsg"]))
pickles = glob.glob("Pickles//" + athena_code + "//*.pickle")

stations = {}
for network in inv:
    net_code = network.code
    for station in network:
        net_stat = net_code + "." + station.code
        stations[net_stat] = {
            "latitude": station.latitude,
            "longitude": station.longitude,
            "elevation": station.elevation,
        }
        easting, northing = pr.transform(
            latlon_proj, out_proj, station.latitude, station.longitude
        )
        stations[net_stat]["e"] = easting
        stations[net_stat]["n"] = northing
        stations[net_stat]["z"] = station.elevation


fig_vm = plt.figure()
ax_vm = fig_vm.add_axes([0.15, 0.1, 0.15, 0.8])
ax_fit = fig_vm.add_axes([0.3, 0.1, 0.6, 0.8])
pickle_out = pickle.load(open(pickles[0], "rb"))
velocity_model = pickle_out["velDict"]
median_elev = np.median([station["z"] for station in stations.values()])
top, vp, vs = [], [], []
top.append(median_elev)
vp.append(velocity_model[0]["vp"])
vs.append(velocity_model[0]["vs"])
vp.append(velocity_model[0]["vp"])
vs.append(velocity_model[0]["vs"])
for layer in velocity_model[1:]:
    for ii in range(2):
        vs.append(layer["vs"])
        vp.append(layer["vp"])
        top.append(layer["top"])
top.append(layer["top"] - 3000)
ax_vm.plot(vp, top, "royalblue")
ax_vm.plot(vs, top, "firebrick")
ax_vm.set_xlabel("velocity (m/s)")
ax_vm.set_ylabel("elevation rel to sea-level (m)")
x_range = ax_vm.get_xlim()
ax_vm.plot([0, x_range[1]], [median_elev, median_elev], "0.3")
ax_vm.set_xlim([0, x_range[1]])
y_range = ax_vm.get_ylim()

ax_fit.set_aspect("equal")

for scaled_top in 1 - (y_range[1] - top[::2]) / (y_range[1] - y_range[0]):
    ax_fit.plot([0, 1], [scaled_top, scaled_top], "0.3")
ax_fit.set_xlim([0, 1])
ax_fit.set_ylim([0, 1])
ax_fit.set_yticks([])
ax_fit.set_yticklabels([""])
ax_fit.set_xticks([])
ax_fit.set_xticklabels([""])
n_pickles = len(pickles) + 1

for i_pickle, pickle_file in enumerate(pickles):
    pickle_out = pickle.load(open(pickle_file, "rb"))
    event_id = os.path.basename(pickle_file).split(".")[0]
    mean_frequency = np.sqrt(pickle_out["bpFreqs"][0] * pickle_out["bpFreqs"][1])
    event_latitude = pickle_out["eveDict"]["event"]["origin"]["latitude"]
    event_longitude = pickle_out["eveDict"]["event"]["origin"]["longitude"]

    event_easting, event_northing = pr.transform(
        latlon_proj, out_proj, event_latitude, event_longitude
    )
    event_elevation = -1000.0 * pickle_out["eveDict"]["event"]["origin"]["depth"]
    event = {"e": event_easting, "n": event_northing, "z": event_elevation}
    # velocity_model = [{"vp": 4000, "vs": 2500, "rho": 2500}]
    p_raypaths, s_raypaths = {}, {}
    for nsl in stations.keys():
        p_raypaths[nsl] = isotropic_ray_trace(event, stations[nsl], velocity_model, "P")
        s_raypaths[nsl] = isotropic_ray_trace(event, stations[nsl], velocity_model, "S")

    aziDict = {
        nsl + ".": (np.arctan2(x["e"], x["n"]) * 180 / np.pi)
        for nsl, x in {
            nsl: p_raypaths[nsl]["hrz_slowness"] for nsl in stations.keys()
        }.items()
    }

    p_takeoff = {
        key + ".": takeoff_angle_Focmec(ray_path)
        for key, ray_path in iteritems(p_raypaths)
    }
    s_takeoff = {
        key + ".": takeoff_angle_Focmec(ray_path)
        for key, ray_path in iteritems(s_raypaths)
    }

    focMecInPath = formFocMecInputFile(
        ".", athena_code, event_id, pickle_out["ampDict"], p_takeoff, s_takeoff, aziDict
    )
    sdrList, focMecOutPath = runFocMec(
        focMecInPath, maxPolErr=len(pickle_out["ampDict"])
    )
    staLocDict = {
        ".".join([net.code, sta.code, cha.location_code]): [sta.longitude, sta.latitude]
        for net in inv
        for sta in net
        for cha in sta
    }

    A_matrix = []
    right_hand_side = []
    for nslc, amp_data in pickle_out["ampDict"].items():
        net_stat = ".".join(nslc.split(".")[:2])
        phase = nslc[-1]
        s_raypath = isotropic_ray_trace(event, stations[net_stat], velocity_model, "S")
        raypath = {
            "P": isotropic_ray_trace(event, stations[net_stat], velocity_model, "P"),
            "V": s_raypath,
            "H": s_raypath,
        }

        A_matrix.append(inversion_matrix_row(raypath[phase], phase))
        right_hand_side.append(
            back_project_amplitude(
                raypath["P"],
                raypath["V"],
                amp_data["amps"][0],
                phase,
                frequency=mean_frequency,
            )
        )
    moment_tensor = solve_moment_tensor(A_matrix, right_hand_side)

    beachball = beach(
        mt_matrix_to_vector(moment_tensor["dc"] / np.linalg.norm(moment_tensor["dc"])),
        xy=(
            (i_pickle + 1) / n_pickles,
            1 - (event_elevation - y_range[1]) / (y_range[0] - y_range[1]),
        ),
        zorder=2,
        width=0.05,
        mopad_basis="XYZ",
        linewidth=0.5,
        facecolor=brg(moment_tensor["r_dc"]),
    )
    ax_fit.add_collection(beachball)


#    cb.add_label("r for double-couple")
fig, ax = plt.subplots()
cb = fig.colorbar(cm.ScalarMappable(cmap="brg"), ax=ax)

plt.show()
