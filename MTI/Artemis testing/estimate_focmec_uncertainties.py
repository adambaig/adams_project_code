import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt
import numpy as np


from sms_moment_tensor.MT_math import (
    mt_matrix_to_vector,
    sdr_to_mt,
    pt_to_sdr,
    unit_vector_to_trend_plunge,
    decompose_MT,
    average_MTs,
    mt_to_sdr,
    conjugate_sdr,
    trend_plunge_to_unit_vector,
)


PI = np.pi
R2D = 180 / PI
wtx_focmec_files = glob.glob("WTX\\FocMec\\*.out")

for focmec_out_file in wtx_focmec_files:
    g = open(focmec_out_file)
    lines = g.readlines()
    g.close()

    reached_header = False
    fault_planes = []
    for i_line, line in enumerate(lines, 1):
        if "allowed  errors" in line:
            report_line = line
        if "Polarities/Errors" in line:
            report_line = line
        if "    Dip   Strike   Rake    Pol: P     SV    SH" in line:
            reached_header = True
            n_planes = len(lines) - i_line
            i_plane = -2
        if reached_header:
            i_plane += 1
            if i_plane > -1:
                dip, strike, rake = [float(s) for s in line.split()[:3]]
                aux_strike, aux_dip, aux_rake = conjugate_sdr(strike, dip, rake)
                fault_planes.append({"strike": strike, "dip": dip, "rake": rake})

    average_decomp = decompose_MT(average_MTs(fault_planes))
    p_axis_avg = trend_plunge_to_unit_vector(
        average_decomp["p_trend"], average_decomp["p_plunge"]
    )
    t_axis_avg = trend_plunge_to_unit_vector(
        average_decomp["t_trend"], average_decomp["t_plunge"]
    )
    p_deg_misfit, t_deg_misfit = [], []
    for fault_plane in fault_planes:
        decomp_fp = decompose_MT(
            sdr_to_mt(fault_plane["strike"], fault_plane["dip"], fault_plane["rake"])
        )
        p_axis = trend_plunge_to_unit_vector(
            decomp_fp["p_trend"], decomp_fp["p_plunge"]
        )
        t_axis = trend_plunge_to_unit_vector(
            decomp_fp["t_trend"], decomp_fp["t_plunge"]
        )
        p_deg_misfit.append(R2D * np.arccos(np.dot(p_axis_avg, p_axis)))
        t_deg_misfit.append(R2D * np.arccos(np.dot(t_axis_avg, t_axis)))

    if len(fault_planes) > 10:
        print(np.average(p_deg_misfit), np.average(t_deg_misfit))
