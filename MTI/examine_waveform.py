import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt

from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from basic_operations.rotation import rotate_to_raypath, rotate_PVH
from read_inputs import get_velocity_model


def sqa(x):
    return np.squeeze(np.array(x))


source = {
    "e": 0,
    "n": 0,
    "z": -1000,
    "stress_drop": 1e6,
    "moment_magnitude": 0,
    "moment_tensor": np.matrix([[1, 0, 0], [0, -1, 0], [0, 0, 0]]),
}
Q = {"P": 80, "S": 60}
velocity_model = get_velocity_model()
station = {"e": 400, "n": 400, "z": 0}

time_series = np.linspace(0, 2, 2001)

p_raypath = isotropic_ray_trace(source, station, velocity_model, "P")
s_raypath = isotropic_ray_trace(source, station, velocity_model, "S")
waveforms = get_response(p_raypath, s_raypath, source, station, Q, time_series, 1e-9)
i_p_time = int(p_raypath["traveltime"] // time_series[1])
window = [i_p_time - 5, i_p_time + 20]
wave_max = max(
    [
        max(abs(waveforms["e"])),
        max(abs(waveforms["n"])),
        max(abs(waveforms["z"])),
    ]
)

fig, ax = plt.subplots(3)
ax[0].plot(time_series, 1 + waveforms["e"] / wave_max)
ax[0].plot(time_series, 0 + waveforms["n"] / wave_max)
ax[0].plot(time_series, -1 + waveforms["z"] / wave_max)
rot_waveforms, linearity = rotate_PVH(waveforms, window)
ax[1].plot(time_series, 1 + rot_waveforms["p"] / wave_max)
ax[1].plot(time_series, 0 + rot_waveforms["v"] / wave_max)
ax[1].plot(time_series, -1 + rot_waveforms["h"] / wave_max)
rot_rp_waveforms = rotate_to_raypath(waveforms, p_raypath)
ax[2].plot(time_series, 1 + rot_rp_waveforms["p"] / wave_max)
ax[2].plot(time_series, 0 + rot_rp_waveforms["v"] / wave_max)
ax[2].plot(time_series, -1 + rot_rp_waveforms["h"] / wave_max)

plt.show()
