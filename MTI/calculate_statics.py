import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
from obspy import read, UTCDateTime, Trace
from obspy.imaging.mopad_wrapper import beach
from obspy.signal import filter
from obspy.signal.invsim import corn_freq_2_paz
from obspy.core.trace import Stats
from operator import itemgetter
from scipy.fftpack import fft, ifft, fftfreq

from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.interface_scattering import (
    P_stack_transmission,
    SV_stack_transmission,
    SH_stack_transmission,
)
from moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from read_inputs import get_velocity_model, read_stations
from read_chevron_0204_data import read_events, read_waveform, read_picks, read_wells

location = "04"
project = "Chevron"
dr = (
    "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\"
    + "Chevron Synthetics\\2019-04-03T18-28\\"
)
catalog = read_events()
stations = read_stations()
velocity_model = get_velocity_model()
wells = read_wells()
Q = {"P": 70, "S": 50}
explosion = {
    "moment_tensor": np.matrix([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]),
    "stress_drop": 3.0e5,
}
xcorr_time = np.arange(-2.998, 3, 0.002)
paz_10hz = corn_freq_2_paz(10.0, damp=np.sqrt(0.5))
snr_threshold = 1.5
n_stations = 69
bandpass_hf = 50
bandpass_lf = 20
statics = {}


def sqa(x):
    return np.squeeze(np.array(x))


def createTraceStats(network, n1, o1, d1, component):
    # make a stats file for a seed trace
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


big_events = {k: v for (k, v) in catalog.items() if v["magnitude"] > 1}

mid_events = {
    k: v for (k, v) in catalog.items() if (v["magnitude"] < 0.7 and v["magnitude"] > 0)
}


def sqa(x):
    return np.squeeze(np.array(x))


f = open("shift_per_event_M0to0.7_location" + location + ".csv", "w")

fig, ax = plt.subplots()

for ii, event in enumerate(mid_events):
    st = read_waveform(UTCDateTime(event))
    picks = read_picks(event)
    time_series = np.arange(st[0].stats.npts) * st[0].stats.delta
    freq = fftfreq(len(time_series) * 2 - 1, time_series[1])
    tsfreq = fftfreq(len(time_series), time_series[1])
    jj = -1
    corr_array = np.zeros([3001, n_stations])
    dat = np.zeros([1501, n_stations])
    dat_align = np.zeros([1501, n_stations])
    syn = np.zeros([1501, n_stations])
    amp = -1.0e11 * np.ones(n_stations)
    snr = np.ones(n_stations)
    easting, northing = np.zeros(n_stations), np.zeros(n_stations)
    kk = 0
    rhs, Amat = [], []
    if picks is None:
        print("picks needed for event " + event)
    else:
        p_raypath = [object() for _ in range(n_stations)]
        s_raypath = [object() for _ in range(n_stations)]
        xcorr_lags = np.zeros(n_stations)
        if ii == 0:
            f.write("event time")
            for trace in st:
                if trace.stats.location == location:
                    id = trace.get_id()
                    f.write("," + id)
                    statics[id] = -999.25 * np.ones(len(mid_events))
            f.write("\n")
        f.write(str(UTCDateTime(event)))
        for trace in st:
            if trace.stats.location == location:
                jj += 1
                id = trace.get_id()
                source = {
                    "x": mid_events[event]["easting"],
                    "y": mid_events[event]["northing"],
                    "z": mid_events[event]["depth"] - 910,
                }
                p_raypath[jj] = isotropic_ray_trace(
                    source, stations[id], velocity_model, "P"
                )
                s_raypath[jj] = isotropic_ray_trace(
                    source, stations[id], velocity_model, "S"
                )

                explosion["moment_magnitude"] = mid_events[event]["magnitude"]
                explosion["x"] = source["x"]
                explosion["y"] = source["y"]
                explosion["z"] = source["z"]
                explosive_response = get_response(
                    p_raypath[jj],
                    s_raypath[jj],
                    explosion,
                    stations[id],
                    Q,
                    time_series,
                    0,
                )
                normalized_response = explosive_response["d"] / np.sqrt(
                    np.correlate(explosive_response["d"], explosive_response["d"])
                )
                expl_channel_object = {
                    "component": "Z",
                    "channel": "CPZ",
                    "station": stations[id],
                    "network": "CV",
                    "location": "04",
                }
                expl_stats = createTraceStats(
                    "CV",
                    trace.stats.npts,
                    trace.stats.starttime,
                    trace.stats.delta,
                    expl_channel_object,
                )
                expl_trace = Trace(data=normalized_response, header=expl_stats)
                expl_trace.simulate(paz_simulate=paz_10hz)
                xcorr = np.correlate(trace.data, expl_trace.data, mode="full")
                if id in picks:
                    t_residual = (
                        picks[id]["P"]
                        - UTCDateTime(picks["t0"]).timestamp
                        - p_raypath[jj]["traveltime"]
                    )
                    pick_residual = picks[id]["P"] - UTCDateTime(picks["t0"]).timestamp
                    syn[:, jj] = np.real(
                        ifft(
                            fft(
                                filter.bandpass(
                                    expl_trace.data, bandpass_lf, bandpass_hf, 500
                                )
                            )
                            * np.exp(
                                2.0j
                                * np.pi
                                * (p_raypath[jj]["traveltime"] - 1)
                                * tsfreq
                            )
                        )
                    )
                    dat[:, jj] = np.real(
                        ifft(
                            fft(
                                filter.bandpass(
                                    trace.data, bandpass_lf, bandpass_hf, 500
                                )
                            )
                            * np.exp(
                                2.0j
                                * np.pi
                                * (picks[id]["P"] - UTCDateTime(event).timestamp - 0.85)
                                * tsfreq
                            )
                        )
                    )

                    xshift = np.real(
                        ifft(
                            fft(xcorr)
                            * np.exp(2.0j * np.pi * (0.15 + t_residual) * freq)
                        )
                    )
                    xcorr_lags[jj] = xcorr_time[
                        np.argmax(abs(xshift[1450:1550])) + 1450
                    ]
                    f.write(",%.4e" % xcorr_lags[jj])
                    statics[id][ii] = xcorr_lags[jj]
                    corr_array[:, jj] = xshift / max(abs(xcorr))
                    amp[jj] = corr_array[
                        np.argsort(abs(corr_array[1479:1520, jj]))[-1] + 1499, jj
                    ]
                    easting[jj] = stations[id]["x"]
                    northing[jj] = stations[id]["y"]
                    snr[jj] = abs(amp[jj]) / np.sqrt(np.var(xcorr[1000:1400]))
                    if np.isnan(snr[jj]):
                        snr[jj] = 0.0
                    if snr[jj] > snr_threshold:
                        kk += 1
                        rhs.append(
                            back_project_amplitude(
                                p_raypath[jj], s_raypath[jj], amp[jj], "P"
                            )
                        )
                        Amat.append(inversion_matrix_row(p_raypath[jj], "P"))
                    dat_align[:, jj] = np.real(
                        ifft(
                            fft(
                                filter.bandpass(
                                    trace.data, bandpass_lf, bandpass_hf, 500
                                )
                            )
                            * np.exp(
                                2.0j
                                * np.pi
                                * (
                                    picks[id]["P"]
                                    - UTCDateTime(event).timestamp
                                    - 0.85
                                    + xcorr_lags[jj]
                                )
                                * tsfreq
                            )
                        )
                    )
                else:
                    f.write(",-999.25")
                ax.plot(xcorr_lags, "k", alpha=0.2)
        f.write("\n")
f.close()


plt.show()
