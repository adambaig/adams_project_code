import matplotlib

matplotlib.use("Qt4agg")

import numpy as np
from numpy import sin, cos, arccos, arcsin, arctan, arctan2
import matplotlib.pyplot as plt
from moment_tensor_inversion import decompose_MT

f = open("M.gt.minus1\MT_output_location01.csv")
line = f.readline()
lines = f.readlines()
f.close()

PI = np.pi
sqrt2 = np.sqrt(2)


def normalize(matrix):
    return matrix / np.linalg.norm(matrix)


def pt_to_sdr(p_trend, p_plunge, t_trend, t_plunge, conjugate=False):
    # to do: make sure P and T are orthogonal
    # this assumes DC
    # input and output in degrees
    p_trend *= PI / 180.0
    t_trend *= PI / 180.0
    p_plunge *= PI / 180.0
    t_plunge *= PI / 180.0
    t_axis = np.array(
        [cos(t_plunge) * cos(t_trend), cos(t_plunge) * sin(t_trend), sin(t_plunge)]
    )
    p_axis = np.array(
        [cos(p_plunge) * cos(p_trend), cos(p_plunge) * sin(p_trend), sin(p_plunge)]
    )
    normal1 = -(t_axis + p_axis) / sqrt2
    slip1 = (p_axis - t_axis) / sqrt2
    if normal1[2] == -1:
        dip1 = 0
        strike1 = 0
        rake1 = arctan2(-slip1[1] / slip1[0])
    else:
        dip1 = arccos(-normal1[2])
        strike1 = arctan2(-normal1[0], normal1[1])
        rake1 = arctan2(
            -slip1[2] / sin(dip1), slip1[0] * cos(strike1) + slip1[1] * strike1
        )
    if conjugate:
        normal2 = slip1
        slip2 = normal1
        if normal2[2] > 0:
            normal2 *= -1
            slip2 *= -1
        if normal2[2] == -1:
            dip2 = 0
            strike2 = 0
            rake2 = arctan2(-slip2[1] / slip2[0])
        else:
            dip2 = arccos(-normal2[2])
            strike2 = arctan2(-normal2[0], normal2[1])
            rake2 = arctan2(
                -slip2[2] / sin(dip2), slip2[0] * cos(strike2) + slip2[1] * strike2
            )
        return (
            180.0 * strike1 / PI,
            180.0 * dip1 / PI,
            180.0 * rake1 / PI,
            180.0 * strike2 / PI,
            180.0 * dip2 / PI,
            180.0 * rake2 / PI,
        )
    else:
        return (180.0 * strike1 / PI, 180.0 * dip1 / PI, 180.0 * rake1 / PI)


rakes = []

for line in lines:
    lspl = line.split(",")
    dc = [float(_) for _ in lspl[9:15]]
    if np.linalg.norm(dc) > 0:
        MT = normalize(
            np.matrix(
                [
                    [dc[0], dc[1], dc[3]],
                    [dc[1], dc[2], dc[4]],
                    [dc[3], dc[4], dc[5]],
                ]
            )
        )
        params = decompose_MT(MT)
        strike1, dip1, rake1, strike2, dip2, rake2 = pt_to_sdr(
            params["p_trend"],
            params["p_plunge"],
            params["t_trend"],
            params["t_plunge"],
            conjugate=True,
        )
        rakes.append(rake1)
        rakes.append(rake2)

bin_number = 36
rakes = np.mod(np.array(rakes), 360) * PI / 180
bins = np.linspace(0, 2 * PI, bin_number + 1)
n, _, _ = plt.hist(rakes, bins, visible=False)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, polar=True)


width = 2 * PI / bin_number
bars = ax.bar(bins[:bin_number], n, width=width)
plt.show()
