import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import glob
from read_chevron_0204_data import read_events, read_wells
import random
from moment_tensor_inversion import decompose_MT

location = "01"
file_f = glob.glob("MT_output_filtered_location" + location + ".csv")
file_x = glob.glob("MT_output_xcorr_location" + location + ".csv")
dataset = ["filtered", "xcorr"]

wells = read_wells()
events = read_events()
d2r = np.pi / 180.0
rsq_cut = 0.15
n_picks_cut = 35
f = ["" for _ in range(2)]
lines = [[] for _ in range(2)]

for ifile, MT_file in enumerate([file_f, file_x]):
    f[ifile] = open(MT_file[0])
    head = f[ifile].readline()
    lines[ifile] = f[ifile].readlines()
    f[ifile].close()


def normalize(matrix):
    return matrix / np.linalg.norm(matrix)


for jj in range(len(lines[0])):
    event = lines[0][jj].split(",")[0]
    number_of_resolved_events = 0
    for ii in range(2):
        lspl = lines[ii][jj].split(",")
        if int(lspl[2]) > 0:
            number_of_resolved_events += 1
            gen = [float(_) for _ in lspl[3:9]]
            dc = [float(_) for _ in lspl[9:15]]
            events[event]["MT_GN_" + dataset[ii]] = normalize(
                np.matrix(
                    [
                        [gen[0], gen[3], gen[4]],
                        [gen[3], gen[1], gen[5]],
                        [gen[4], gen[5], gen[2]],
                    ]
                )
            )
            events[event]["MT_DC_" + dataset[ii]] = normalize(
                np.matrix(
                    [
                        [dc[0], dc[3], dc[4]],
                        [dc[3], dc[1], dc[5]],
                        [dc[4], dc[5], dc[2]],
                    ]
                )
            )
            events[event]["cn_GN_" + dataset[ii]] = float(lspl[-4])
            events[event]["cn_DV_" + dataset[ii]] = float(lspl[-3])
            events[event]["r2_GN_" + dataset[ii]] = float(lspl[-2])
            events[event]["r2_DC_" + dataset[ii]] = float(lspl[-1])
            events[event]["n_picks_" + dataset[ii]] = float(lspl[2])


filter_events = {k: v for k, v in events.items() if "n_picks_filtered" in v}
xcorr_events = {k: v for k, v in events.items() if "n_picks_xcorr" in v}
common_events = {
    k: v
    for k, v in events.items()
    if (("n_picks_filtered" in v) and ("n_picks_xcorr" in v))
}

r2_dc_filt = np.array([v["r2_DC_filtered"] for k, v in common_events.items()])
r2_gn_filt = np.array([v["r2_GN_filtered"] for k, v in common_events.items()])
r2_dc_xcor = np.array([v["r2_DC_xcorr"] for k, v in common_events.items()])
r2_gn_xcor = np.array([v["r2_GN_xcorr"] for k, v in common_events.items()])
n_picks_filt = np.array([v["n_picks_filtered"] for k, v in common_events.items()])
n_picks_xcor = np.array([v["n_picks_xcorr"] for k, v in common_events.items()])
mag = np.array([v["magnitude"] for k, v in common_events.items()])

fig, ax = plt.subplots(2, 2)

bins = np.arange(-1, 2.1, 0.25)
violin_filt_dc, violin_filt_gn, violin_xcor_dc, violin_xcor_gn = [], [], [], []
for jj in bins[:-1]:
    violin_filt_dc.append([])
    violin_filt_gn.append([])
    violin_xcor_dc.append([])
    violin_xcor_gn.append([])

for ii in range(len(mag)):
    for jj in range(len(bins) - 1):
        if (mag[ii] > bins[jj]) and (mag[ii] < bins[jj + 1]):
            if n_picks_filt[ii] > n_picks_cut:
                violin_filt_gn[jj].append(r2_gn_filt[ii])
            if n_picks_filt[ii] > n_picks_cut:
                violin_filt_dc[jj].append(r2_dc_filt[ii])
            if n_picks_xcor[ii] > n_picks_cut:
                violin_xcor_gn[jj].append(r2_gn_filt[ii])
            if n_picks_xcor[ii] > n_picks_cut:
                violin_xcor_dc[jj].append(r2_dc_filt[ii])
for ii, bin in enumerate(violin_filt_gn):
    if len(bin) == 0:
        violin_filt_gn[ii].append(-1)

for ii, bin in enumerate(violin_filt_dc):
    if len(bin) == 0:
        violin_filt_dc[ii].append(-1)

for ii, bin in enumerate(violin_xcor_gn):
    if len(bin) == 0:
        violin_xcor_gn[ii].append(-1)

for ii, bin in enumerate(violin_xcor_dc):
    if len(bin) == 0:
        violin_xcor_dc[ii].append(-1)

ax[0, 0].violinplot(violin_filt_gn)
ax[0, 1].violinplot(violin_filt_dc)
ax[1, 0].violinplot(violin_xcor_gn)
ax[1, 1].violinplot(violin_xcor_dc)
for ii in range(2):
    for jj in range(2):
        ax[ii, jj].set_ylim([-0.05, 1.05])
        ax[ii, jj].set_ylabel("R$^2$")
        ax[ii, jj].set_xlabel("magnitude bin")
        ax[ii, jj].set_xticks(np.arange(0.5, 13, 1))
        ax[ii, jj].set_xticklabels(np.arange(-1, 2.1, 0.25))


plt.show()
