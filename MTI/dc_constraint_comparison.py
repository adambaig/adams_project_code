import glob
import os
import pickle

import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory, UTCDateTime
import pyproj as pr
from scipy.optimize import minimize, NonlinearConstraint, LinearConstraint

from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import (
    mt_matrix_to_vector,
    sdr_to_mt,
    mt_vector_to_matrix,
    mt_to_sdr,
    clvd_iso_dc,
)
from sms_moment_tensor.plotting import (
    individual_bb_plot,
    plot_beachball_from_sdr,
    plot_regression,
)
from sms_ray_modelling.raytrace import isotropic_ray_trace

# inv = formStaXml(outDir='Station_xmls/', athenaCode='WTX')

inv = {}
for network_inv in glob.glob("Station_xmls/WTX_networks/*.xml"):
    network = os.path.basename(network_inv).split(".")[0]
    inv[network] = read_inventory(network_inv)

total_inv = (
    inv["4T"]
    + inv["BP"]
    + inv["CE"]
    + inv["GM"]
    + inv["NX"]
    + inv["SC"]
    + inv["TX"]
    + inv["US"]
)

pickles = glob.glob("DC_constraint//MTI pickles//*.pickle")
compare_dict = {}


def obj_function(m, rows, bp_amplitudes):
    sum_sqaures = 0
    for row, amplitude in zip(rows, bp_amplitudes):
        sum_sqaures += np.squeeze(np.array(row) @ np.array(m) - amplitude) ** 2
    return sum_sqaures


calculate_determinant = lambda m: np.linalg.det(mt_vector_to_matrix(m))
determinant_constraint = NonlinearConstraint(calculate_determinant, 0, 0)
trace_constraint = LinearConstraint([1, 1, 1, 0, 0, 0], 0, 0)

latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init="epsg:3081")
inst_xml_file = "Station_xmls//WTX_Full.xml"
# inv = read_inventory(inst_xml_file)
g = open("DC_constraint//comparison_WTX.csv", "w")
g.write(
    "event id,datetime,Ml,full m11,full m22,full m33,full m12,"
    "full m13,full m23,clvd,iso,dc,full cn,full r,dc_cur strike,dc_cur dip,"
    "dc_cur rake,dc_cur r,dc_prop strike,dc_prop dip,dc_prop rake,dc_prop r,"
    "method similarity,var reduction current,var reduction proposed,"
    "dc norm current,dc norm proposed\n"
)


for pickle_file in pickles:

    f = open(pickle_file, "rb")
    event = pickle.load(f)
    f.close()
    origin = event["eveDict"]["event"]["origin"]
    velocity_model = event["velDict"]
    velocity_model[0].pop("top")
    amplitudes = event["ampDict"]

    referenceFreq = np.exp(np.log(event["bpFreqs"][0]) + np.log(event["bpFreqs"][1]))
    # try:
    #     long_o, lat_o, elevation_o = (
    #         origin["momentTensors"]["longitude"],
    #         origin["momentTensors"]["latitude"],
    #         -1000 * origin["momentTensors"]["depth"],
    #     )
    # except:
    #     long_o, lat_o, elevation_o = (
    #         origin["momentTensors"][0]["longitude"],
    #         origin["momentTensors"][0]["latitude"],
    #         -1000 * origin["momentTensors"][0]["depth"],
    #     )
    long_o, lat_o, elevation_o = (
        origin["longitude"],
        origin["latitude"],
        -1000 * origin["depth"],
    )

    east_o, north_o = pr.transform(latlon_proj, out_proj, long_o, lat_o)
    source = {"e": east_o, "n": north_o, "z": elevation_o}
    back_proj_amp, forward_mti_amp, forward_fps_amp, row = {}, {}, {}, {}
    for station_phase, amp_vals in amplitudes.items():
        network, station, location, channel = station_phase.split(".")
        station_inv = total_inv.select(network=network, station=station)[0][0]
        east, north = pr.transform(
            latlon_proj, out_proj, station_inv.longitude, station_inv.latitude
        )
        elevation = station_inv.elevation
        receiver = {"e": east, "n": north, "z": elevation}
        p_raypath = isotropic_ray_trace(source, receiver, velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, receiver, velocity_model, "S")
        back_proj_amp[station_phase] = back_project_amplitude(
            p_raypath,
            s_raypath,
            amp_vals["amps"][0],
            channel[-1],
            frequency=referenceFreq,
        )
        row[station_phase] = inversion_matrix_row(
            p_raypath if channel[-1] == "P" else s_raypath, channel[-1]
        )

    mt = solve_moment_tensor(list(row.values()), list(back_proj_amp.values()))
    norm = np.linalg.norm(mt_matrix_to_vector(mt["dc"]))
    m = mt_matrix_to_vector(mt["general"])
    rows = list(row.values())
    bp_amplitudes = list(back_proj_amp.values())
    min_object = minimize(
        obj_function,
        mt_matrix_to_vector(mt["dc"]) / norm,
        args=(rows, bp_amplitudes / norm),
        method="SLSQP",
        constraints=(trace_constraint, determinant_constraint),
    )
    mt_dc_constrained = norm * min_object.x
    number_of_iterations = min_object.nit
    event_id = os.path.basename(pickle_file).split(".")[0]
    event_datetime = str(UTCDateTime(origin["time"]))
    local_magnitude = origin["magnitudes"][0]["value"]
    clvd_percent, iso_percent, dc_percent = clvd_iso_dc(mt["general"])
    sdr_current = mt_to_sdr(mt["dc"], conjugate=False)
    sdr_proposed = mt_to_sdr(mt_dc_constrained, conjugate=False)
    dc_current = {
        "strike": sdr_current[0],
        "dip": sdr_current[1],
        "rake": sdr_current[2],
    }
    dc_proposed = {
        "strike": sdr_proposed[0],
        "dip": sdr_proposed[1],
        "rake": sdr_proposed[2],
    }
    similarity = np.tensordot(sdr_to_mt(**dc_current), sdr_to_mt(**dc_proposed))
    dc_proposed_forward_amps = (np.array(list(row.values())) @ mt_dc_constrained).T[0]
    dc_current_forward_amps = (
        np.array(list(row.values())) @ mt_matrix_to_vector(mt["dc"])
    ).T[0]
    r_dc_proposed = np.corrcoef(list(back_proj_amp.values()), dc_proposed_forward_amps)[
        1, 0
    ]
    variance_reduction_proposed = 100.0 * (
        1 - np.var(bp_amplitudes - dc_proposed_forward_amps) / np.var(bp_amplitudes)
    )
    variance_reduction_current = 100.0 * (
        1 - np.var(bp_amplitudes - dc_current_forward_amps) / np.var(bp_amplitudes)
    )
    g.write(f"{event_id},{event_datetime},{local_magnitude},")
    [g.write(f"{m:.4e},") for m in mt_matrix_to_vector(mt["general"])]
    g.write(f"{clvd_percent:.2f},{iso_percent:2f},{dc_percent:2f},")
    g.write(f'{mt["condition_number_gen"]:2f}, {mt["r_gen"]:.4f},')
    [g.write(f"{sdr:2f},") for sdr in sdr_current]
    g.write(f'{mt["r_dc"]:4f},')
    [g.write(f"{sdr:2f},") for sdr in sdr_proposed]
    g.write(f"{r_dc_proposed:4f},{similarity:6f},")
    g.write(f"{variance_reduction_current:.1f},{variance_reduction_proposed:.1f},")
    g.write(f"{norm:.6e},{np.linalg.norm(mt_dc_constrained):.6e}\n")


g.close()
