import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import glob

files = glob.glob(
    "xc_and_filt_larger_subset/MT_output_xcorrfilt_location0[0-9].csv"
)
nfiles = len(files)
rsq_cut = 0.02
n_picks_cut = 7+
f = ["" for _ in range(nfiles)]
location = ["" for _ in range(nfiles)]
lines = [[] for _ in range(nfiles)]
for ifile, MT_file in enumerate(files):
    f[ifile] = open(MT_file)
    head = f[ifile].readline()
    location[ifile] = MT_file.split("location")[1].split(".csv")[0]
    lines[ifile] = f[ifile].readlines()


def normalize(matrix):
    return matrix / np.linalg.norm(matrix)


events = {}
gn_similarity, dc_similarity, mag, min_rsq_gn, min_rsq_dc, min_n_picks = (
    [],
    [],
    [],
    [],
    [],
    [],
)
for jj in range(len(lines[0])):
    event = lines[0][jj].split(",")[0]
    events[event] = {}
    n_good = 0
    for ii, loc in enumerate(location[:3]):
        lspl = lines[ii][jj].split(",")
        gen = [float(_) for _ in lspl[3:9]]
        dc = [float(_) for _ in lspl[9:15]]
        if int(lspl[2]) > 0:
            n_good += 1
            events[event]["MT_GN_" + loc] = normalize(
                np.matrix(
                    [
                        [gen[0], gen[1], gen[3]],
                        [gen[1], gen[2], gen[4]],
                        [gen[3], gen[4], gen[5]],
                    ]
                )
            )
            events[event]["MT_DC_" + loc] = normalize(
                np.matrix(
                    [
                        [dc[0], dc[1], dc[3]],
                        [dc[1], dc[2], dc[4]],
                        [dc[3], dc[4], dc[5]],
                    ]
                )
            )
            events[event]["cn_GN_" + loc] = float(lspl[-4])
            events[event]["cn_DV_" + loc] = float(lspl[-3])
            events[event]["r2_GN_" + loc] = float(lspl[-2])
            events[event]["r2_DC_" + loc] = float(lspl[-1])
            events[event]["n_picks_" + loc] = float(lspl[2])
    if n_good > 0:
        gn_similarity.append(
            np.linalg.norm(
                sum([v for k, v in events[event].items() if "MT_GN" in k])
                / n_good
            )
        )
        min_rsq_gn.append(
            min([v for k, v in events[event].items() if "r2_GN" in k])
        )
        dc_similarity.append(
            np.linalg.norm(
                sum([v for k, v in events[event].items() if "MT_DC" in k])
                / n_good
            )
        )
        min_rsq_dc.append(
            min([v for k, v in events[event].items() if "r2_DC" in k])
        )
        min_n_picks.append(
            min([v for k, v in events[event].items() if "n_picks" in k])
        )
        mag.append(float(lspl[1]))

mag = np.array(mag)
gn_similarity = np.array(gn_similarity)
dc_similarity = np.array(dc_similarity)


min_rsq_gn = np.array(min_rsq_gn)
min_rsq_dc = np.array(min_rsq_dc)
min_n_picks = np.array(min_n_picks)
fig, ax = plt.subplots(2)
i_good = np.where((min_rsq_gn > rsq_cut) & (min_n_picks > n_picks_cut))[0]
sc0 = ax[0].scatter(
    mag[i_good],
    gn_similarity[i_good],
    c=min_rsq_gn[i_good],
    cmap="viridis",
    edgecolor="0.7",
    vmin=0,
    vmax=1,
)
cb0 = fig.colorbar(sc0, ax=ax[0])
cb0.set_label("minimum R$^2$ value")
ax[0].set_ylabel("similarity")
ax[0].text(0.4, 0.05, "General Solution")
ax[0].set_ylim([0.0, 1.05])
i_good = np.where((min_rsq_dc > rsq_cut) & (min_n_picks > n_picks_cut))[0]
sc1 = ax[1].scatter(
    mag[i_good],
    dc_similarity[i_good],
    c=min_rsq_dc[i_good],
    cmap="inferno",
    edgecolor="0.7",
    vmin=0,
    vmax=1,
)
ax[1].set_ylabel("similarity")
ax[1].set_xlabel("magnitude")
ax[1].text(0.4, 0.05, "Double-Couple Forced")
ax[1].set_ylim([0.0, 1.05])
cb1 = fig.colorbar(sc1, ax=ax[1])
cb1.set_label("minimum R$^2$ value")
f2, a2 = plt.subplots(2)
bins = np.arange(-1, 2.1, 0.25)
violin_dc, violin_gn = [], []
for jj in bins[:-1]:
    violin_dc.append([])
    violin_gn.append([])
for ii in range(len(mag)):
    for jj in range(len(bins) - 1):
        if (mag[ii] > bins[jj]) and (mag[ii] < bins[jj + 1]):
            if (min_rsq_gn[ii] > rsq_cut) & (min_n_picks[ii] > n_picks_cut):
                violin_gn[jj].append(gn_similarity[ii])
            if (
                (min_rsq_dc[ii] > rsq_cut) & (min_n_picks[ii] > n_picks_cut)
            ) > rsq_cut:
                violin_dc[jj].append(dc_similarity[ii])

for ii, bin in enumerate(violin_gn):
    if len(bin) == 0:
        violin_gn[ii].append(-1)

for ii, bin in enumerate(violin_dc):
    if len(bin) == 0:
        violin_dc[ii].append(-1)

a2[0].violinplot(violin_gn)
a2[1].violinplot(violin_dc)
a2[0].set_xticks(np.arange(0.5, 13, 1))
a2[0].set_xticklabels([])
a2[1].set_xticks(np.arange(0.5, 13, 1))
a2[1].set_xticklabels(np.arange(-1, 2.1, 0.25))
a2[0].set_ylabel("GN similarity")
a2[1].set_ylabel("DC similarity")
y1, y2 = a2[0].get_ylim()
a2[0].set_ylim([0, y2])
y1, y2 = a2[1].get_ylim()
a2[1].set_ylim([0, y2])
rsq_dc = []
rsq_gn = []
for event in events:
    if "r2_DC_01" in events[event]:
        rsq_dc.append(events[event]["r2_DC_01"])
    if "r2_DC_02" in events[event]:
        rsq_dc.append(events[event]["r2_DC_02"])
    if "r2_DC_03" in events[event]:
        rsq_dc.append(events[event]["r2_DC_03"])
    if "r2_GN_01" in events[event]:
        rsq_gn.append(events[event]["r2_GN_01"])
    if "r2_GN_02" in events[event]:
        rsq_gn.append(events[event]["r2_GN_02"])
    if "r2_GN_03" in events[event]:
        rsq_gn.append(events[event]["r2_GN_03"])

f3, a3 = plt.subplots(nrows=1, ncols=2)
a3[0].hist(rsq_gn, np.arange(0, 1.01, 0.02))
a3[1].hist(rsq_dc, np.arange(0, 1.01, 0.02))
a3[0].set_title("General Solutions")
a3[0].set_xlabel("R")
a3[0].set_ylabel("count")
a3[1].set_title("DC Forced Solution")
a3[1].set_xlabel("R")
a3[1].set_ylabel("count")
plt.show()


plt.plot(dc_similarity, min_rsq_dc, ".")
