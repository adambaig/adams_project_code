from obspy import read, UTCDateTime
import glob
import numpy as np


def read_events():
    event_dir = "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\"
    event_files = glob.glob(event_dir + "Chevron_withXC_utm.csv")
    events = {}
    for filename in event_files:
        f = open(filename)
        f.readline()
        lines = f.readlines()
        f.close()
        for line in lines:
            lspl = line.split(",")
            well = lspl[12]
            stagename = lspl[13].split("\n")[0]
            if stagename in ["121", "41", "42"]:
                stage = stagename[:-1] + "-Part" + stagename[-1]
            else:
                stage = stagename
            timestamp = UTCDateTime.strftime(UTCDateTime(lspl[1]), "%Y%m%d%H%M%S.%f")
            events[timestamp] = {}
            events[timestamp]["id"] = lspl[0]
            events[timestamp]["easting"] = float(lspl[2])
            events[timestamp]["northing"] = float(lspl[3])
            events[timestamp]["depth"] = float(lspl[4])
            events[timestamp]["magnitude"] = float(lspl[5])
            events[timestamp]["stack_amp"] = float(lspl[6])
            events[timestamp]["filt_amp"] = float(lspl[7])
            events[timestamp]["xc_value"] = float(lspl[8])
            events[timestamp]["master"] = int(lspl[9])
            events[timestamp]["isShallow"] = lspl[10]
            events[timestamp]["cluster"] = int(lspl[11])
            events[timestamp]["well"] = well
            events[timestamp]["stage"] = stage
    return events


def read_waveform(UTC_t0, prepad=0.2, trace_length=3.0):
    seed_dir = (
        "O:\\O&G\\Projects\\SO53222 (Chevron Pad 02-04 PFI)"
        + "\\Imaging\\Chev_PFI_seeds"
    )
    t_start = UTC_t0 - prepad
    t_end = t_start + trace_length
    seedfiles = glob.glob(seed_dir + "\\*.mseed")
    seedfiles.sort()
    seedtimes = np.zeros(len(seedfiles))
    ii = -1
    for seedfile in seedfiles:
        ii += 1
        seedtimes[ii] = UTCDateTime(seedfile.split("\\")[-1].split(".")[0])
    i_trace_start = np.where(t_start.timestamp > seedtimes)[0][-1]
    i_trace_end = np.where(t_end.timestamp > seedtimes)[0][-1]
    st = read(seedfiles[i_trace_start])
    if i_trace_end > i_trace_start:
        for i_trace in range(i_trace_start + 1, i_trace_end + 1):
            st += read(seedfiles[i_trace])
        st.merge(method=1)
    st.trim(starttime=t_start, endtime=t_end)
    return st


pick_dir = (
    "O:\\O&G\\Projects\\SO53222 (Chevron Pad 02-04 PFI)\\Imaging\\02_ChevPFI_picking\\"
)

pfiles = glob.glob(pick_dir + "*.picks")

pickfile = {}
for pfile in pfiles:
    event_id, timestr = pfile.split("\\")[-1].split(".picks")[0].split("_")
    ymd, hms, mu_sec = timestr.split(".")
    pickfile[pfile.split("\\")[-1]] = {
        "serial_time": UTCDateTime(ymd + hms + "." + mu_sec),
        "event_id": event_id,
    }


def read_picks(event_time):
    time_tolerance = 0.4
    ifile = np.where(
        abs(
            np.array([v["serial_time"] for (k, v) in pickfile.items()])
            - UTCDateTime(event_time)
        )
        < time_tolerance
    )[0]
    if len(ifile > 0):
        f = open(pfiles[ifile[0]])
        lines = f.readlines()
        f.close()
        picks = {}
        picks["id"] = pickfile[pfiles[ifile[0]].split("\\")[-1]]["event_id"]
        picks["t0"] = pickfile[pfiles[ifile[0]].split("\\")[-1]]["serial_time"]
        for line in lines:
            lspl = line.split(",")
            station = lspl[0] + ".CPZ"
            picks[station] = {}

            if lspl[1] == "P":
                picks[station]["P"] = float(lspl[2])
            elif lspl[1] == "S":
                picks[station]["S"] = float(lspl[2])
        return picks


def read_wells():
    base_dir = "C:\\Users\\adambaig\\Project\\Chevron Grizzly\\setup\\"
    wells = {}
    for well in ["A", "B", "C", "D"]:
        wells[well] = {}
        f = open(base_dir + "Well_" + well + ".csv")
        head = f.readline()
        unit = f.readline()
        lines = f.readlines()
        f.close()
        nwell = len(lines)
        easting, northing, tvdss, md = (
            np.zeros(nwell),
            np.zeros(nwell),
            np.zeros(nwell),
            np.zeros(nwell),
        )
        ii = -1
        for line in lines:
            ii += 1
            lspl = line.split(",")
            md[ii] = float(lspl[1])
            easting[ii], northing[ii] = [float(s) for s in lspl[9:11]]
            tvdss[ii] = float(lspl[5])
        f = open(base_dir + "Perfs_" + well + ".csv")
        head = f.readline()
        lines = f.readlines()
        f.close()
        ii = -1
        for line in lines:
            ii += 1
            lspl = line.split(",")
            stage = "Stage " + str(int(round(float(lspl[0]))))
            if not (stage in wells[well].keys()):
                wells[well][stage] = {}
                jj = 0
            jj += 1
            top, bottom = [float(s) for s in lspl[1:3]]
            if well == "C":
                cluster = "Port " + str(jj)
            else:
                cluster = "Perf " + str(jj)
            wells[well][stage][cluster] = {}
            wells[well][stage][cluster]["top_east"] = np.interp(top, md, easting)
            wells[well][stage][cluster]["top_north"] = np.interp(top, md, northing)
            wells[well][stage][cluster]["top_tvdss"] = np.interp(top, md, tvdss)
            wells[well][stage][cluster]["bottom_east"] = np.interp(bottom, md, easting)
            wells[well][stage][cluster]["bottom_north"] = np.interp(
                bottom, md, northing
            )
            wells[well][stage][cluster]["bottom_tvdss"] = np.interp(bottom, md, tvdss)
        wells[well]["easting"] = easting
        wells[well]["northing"] = northing
        wells[well]["tvdss"] = tvdss
    return wells
