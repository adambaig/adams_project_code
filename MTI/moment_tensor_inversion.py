import numpy as np
import matplotlib.pyplot as plt
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.interface_scattering import P_stack_transmission


r2d = 180.0 / np.pi


def point_down(vec):
    if vec[2] < 0:
        return -vec
    else:
        return vec


def sqa(x):
    return np.squeeze(np.array(x))


def unit_vector_2_trend_plunge(u):
    if u[2] < 0:
        u = -u
    trend = arctan2(u[1], u[0]) * 180.0 / PI
    plunge = arctan(u[2] / np.sqrt(u[0] * u[0] + u[1] * u[1])) * 180.0 / PI
    return [trend, plunge]


def inversion_matrix_row(raypath, phase):
    horizontal_slowness_sq = (
        raypath["hrz_slowness"]["x"] * raypath["hrz_slowness"]["x"]
        + raypath["hrz_slowness"]["y"] * raypath["hrz_slowness"]["y"]
    )
    v_source = raypath["velocity_model_chunk"][0]["v"]
    sin_takeoff = np.sqrt(horizontal_slowness_sq) * v_source
    cos_takeoff = np.sqrt(1 - sin_takeoff * sin_takeoff)
    azimuth = np.arctan2(raypath["hrz_slowness"]["y"], raypath["hrz_slowness"]["x"])
    if phase == "P":
        return np.matrix(
            [
                sin_takeoff**2 * np.sin(azimuth) ** 2,
                sin_takeoff**2 * np.sin(2.0 * azimuth),
                sin_takeoff**2 * np.cos(azimuth) ** 2,
                2.0 * sin_takeoff * cos_takeoff * np.sin(azimuth),
                2.0 * sin_takeoff * cos_takeoff * np.cos(azimuth),
                np.cos(azimuth) ** 2,
            ]
        )


def back_project_amplitude(p_raypath, s_raypath, amp, phase):
    # assumes a vertical phone
    if phase == "P":
        horizontal_slowness_sq = (
            p_raypath["hrz_slowness"]["x"] * p_raypath["hrz_slowness"]["x"]
            + p_raypath["hrz_slowness"]["y"] * p_raypath["hrz_slowness"]["y"]
        )
        v_receiver = p_raypath["velocity_model_chunk"][-1]["v"]
        cos_incoming = np.sqrt(1.0 - horizontal_slowness_sq * v_receiver**2)
        velocity_and_rho = []
        for layer in zip(
            p_raypath["velocity_model_chunk"], s_raypath["velocity_model_chunk"]
        ):
            velocity_and_rho.append(
                {"rho": layer[0]["rho"], "vp": layer[0]["v"], "vs": layer[1]["v"]}
            )
        p_trans = P_stack_transmission(
            velocity_and_rho, np.sqrt(horizontal_slowness_sq)
        )
        return amp * p_raypath["geometrical_spreading"] / p_trans


def solve_moment_tensor(A_matrix, rhs, DC=True, qcplot=False):
    rhs = np.matrix(rhs)
    A_matrix = np.matrix(np.squeeze(A_matrix))
    m11, m12, m22, m13, m23, m33 = [
        sqa(x) for x in np.linalg.inv(A_matrix.T * A_matrix) * A_matrix.T * rhs.T
    ]
    cn_gen = np.linalg.cond(A_matrix)
    mt_gen = [m11, m22, m33, m12, m13, m23]
    rhs_comp = A_matrix * np.matrix(mt_gen).T
    rsq_gen = (np.corrcoef(sqa(rhs), sqa(rhs_comp))[0, 1]) ** 2
    if DC:
        # to estimate the deviatoric solution, the
        # system of equations is augmented with the zero trace
        # constraint.  The deviatoric solution is constructed from this
        # system.  However, because of the vagaries of least squares
        # inversions, the trace is not necessarily zero after this
        # procedure so it is explicitly enforced for dev_MT.   The only reason
        # I do things this way is to estimate the deviatoric condition number.
        dev_matrix = np.append(A_matrix, [[1, 0, 1, 0, 0, 1]], axis=0)
        rhs_dev = np.append(rhs, [[0]], axis=1)
        dv11, dv12, dv22, dv13, dv23, dv33 = [
            sqa(x)
            for x in np.linalg.inv(dev_matrix.T * dev_matrix) * dev_matrix.T * rhs_dev.T
        ]

        trace_over_3 = (dv11 + dv22 + dv33) / 3
        cn_dev = np.linalg.cond(dev_matrix)
        dev_MT = np.matrix(
            [
                [dv11 - trace_over_3, dv12, dv13],
                [dv12, dv22 - trace_over_3, dv23],
                [dv13, dv23, dv33 - trace_over_3],
            ]
        )
        eigvals, eigvecs = np.linalg.eig(dev_MT)
        # remove clvd from deviatoric solution a la Strelitz, 1989
        i_sort = np.argsort(eigvals)
        dc_eigs = np.zeros(3)
        dc_eigs[i_sort[0]] = eigvals[i_sort[0]] + eigvals[i_sort[1]] / 2.0
        dc_eigs[i_sort[2]] = eigvals[i_sort[2]] + eigvals[i_sort[1]] / 2.0
        dc_MT = eigvecs * np.diag(dc_eigs) * eigvecs.T
        mt_dc = [
            dc_MT[0, 0],
            dc_MT[1, 1],
            dc_MT[2, 2],
            dc_MT[0, 1],
            dc_MT[0, 2],
            dc_MT[1, 2],
        ]
        rhs_dc_comp = A_matrix * np.matrix(mt_dc).T
        rsq_dc = (np.corrcoef(sqa(rhs), sqa(rhs_dc_comp))[0, 1]) ** 2
        return (mt_gen, cn_gen, rsq_gen, mt_dc, cn_dev, rsq_dc)
    else:
        return (mt_gen, cn_gen, rsq_gen)


def decompose_MT(moment_tensor):
    eigenvalues, eigenvectors = np.linalg.eig(moment_tensor)
    i_sort = np.argsort(eigenvalues)
    p_axis = sqa(point_down(eigenvectors[:, i_sort[0]]))
    b_axis = sqa(point_down(eigenvectors[:, i_sort[1]]))
    t_axis = sqa(point_down(eigenvectors[:, i_sort[2]]))
    lambda_1, lambda_2, lambda_3 = eigenvalues[i_sort]
    p_trend = np.arctan2(p_axis[1], p_axis[0])
    b_trend = np.arctan2(b_axis[1], b_axis[0])
    t_trend = np.arctan2(t_axis[1], t_axis[0])
    p_plunge = np.arcsin(p_axis[2])
    b_plunge = np.arcsin(b_axis[2])
    t_plunge = np.arcsin(t_axis[2])
    parameters = {
        "p_trend": r2d * p_trend,
        "p_plunge": r2d * p_plunge,
        "b_trend": r2d * b_trend,
        "b_plunge": r2d * b_plunge,
        "t_trend": r2d * t_trend,
        "t_plunge": r2d * t_plunge,
        "lambda_1": lambda_1,
        "lambda_2": lambda_2,
        "lambda_3": lambda_3,
    }
    return parameters
