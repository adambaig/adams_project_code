import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import mplstereonet as mpls
from random import sample, random, gauss

from randomize_seismicity.moment_tensor import dc
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import decompose_MT
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response

from read_inputs import read_stations, get_velocity_model

greens = cm.get_cmap("Greens")
purples = cm.get_cmap("Purples")
bwr = cm.get_cmap("bwr")


def sqa(x):
    return np.squeeze(np.array(x))


n_subsample = 69
flip_rate = 0.0
n_iterations = 100
amp_std = 0.3
polarity_only = False
compare_general_solution = True

stations = read_stations()
velocity_model = get_velocity_model()
source = {
    "e": np.average([v["e"] for k, v in stations.items()]),
    "n": np.average([v["n"] for k, v in stations.items()]),
    "z": 2700,
    # "moment_tensor": np.matrix(
    #     [[-1, 0, 0], [0, 0, 0], [0, 0, 1]]
    # ),  # messed up
    "moment_tensor": np.matrix([[-1, 0, 0], [0.0, 0, 0.0], [0.0, 0.0, -1.0]]),
    "stress_drop": 1e6,
    "moment_magnitude": 0,
}
Q = {"P": 80, "S": 60}
time_series = np.linspace(0, 4, 2001)
fig = plt.figure()
ax = fig.add_axes([0.08, 0.1, 0.6, 0.8], projection="stereonet")

ax_cb = fig.add_axes([0.78, 0.2, 0.08, 0.7])

waveforms = {}
sub_stations = {k: v for k, v in stations.items() if k.split(".")[2] == "02"}
sub_sub_stations = list(sub_stations)  # [1::4]
amps, x, y = (
    np.zeros(len(sub_stations)),
    np.zeros(len(sub_stations)),
    np.zeros(len(sub_stations)),
)
for i_station, station in enumerate(sub_stations):
    p_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "P")
    s_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "S")
    waveforms[station] = get_response(
        p_raypath, s_raypath, source, stations[station], Q, time_series, 1e-9
    )
    trace = waveforms[station]["z"]
    cos_incoming = np.sqrt(
        1.0
        - (p_raypath["hrz_slowness"]["e"] ** 2 + p_raypath["hrz_slowness"]["n"] ** 2)
        * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
    )
    i_p_time = int(p_raypath["traveltime"] // time_series[1])
    i_peak = np.argmax(abs(trace[i_p_time : i_p_time + 20])) + i_p_time
    amps[i_station] = (
        back_project_amplitude(p_raypath, s_raypath, trace[i_peak], "P", Q=80)
        / cos_incoming
    )
    x[i_station] = stations[station]["e"]
    y[i_station] = stations[station]["n"]

amps = amps / max(abs(amps))

r_vector = np.zeros(n_iterations)
cond_number = np.zeros(n_iterations)
for iteration in range(n_iterations):
    # random_sample = sample(list(sub_sub_stations), n_subsample)
    A_matrix = []
    right_hand_side = []

    for i_station, station in enumerate(sub_stations):
        if abs(amps[i_station]) > 0.1:
            trace = waveforms[station]["z"]
            p_raypath = isotropic_ray_trace(
                source, stations[station], velocity_model, "P"
            )
            s_raypath = isotropic_ray_trace(
                source, stations[station], velocity_model, "S"
            )
            i_p_time = int(p_raypath["traveltime"] // time_series[1])
            i_peak = np.argmax(abs(trace[i_p_time : i_p_time + 20])) + i_p_time
            A_matrix.append(inversion_matrix_row(p_raypath, "P"))
            cos_incoming = np.sqrt(
                1.0
                - (
                    p_raypath["hrz_slowness"]["e"] ** 2
                    + p_raypath["hrz_slowness"]["n"] ** 2
                )
                * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
            )
            if polarity_only:
                measured_amp = (
                    np.sign(random() - flip_rate) * trace[i_peak]
                ) / cos_incoming
            else:
                measured_amp = (
                    gauss(1.0, amp_std)
                    * np.sign(random() - flip_rate)
                    * trace[i_peak]
                    / cos_incoming
                )
            right_hand_side.append(
                back_project_amplitude(p_raypath, s_raypath, measured_amp, "P", Q=80)
            )
    moment_tensor = solve_moment_tensor(A_matrix, right_hand_side)
    r_vector[iteration] = moment_tensor["r_dc"]
    cond_number[iteration] = moment_tensor["condition_number_dev"]
    decomp = decompose_MT(moment_tensor["dc"])
    ax.line(
        decomp["p_plunge"],
        decomp["p_trend"],
        ".",
        markeredgecolor="0.7",
        linewidth=0.25,
        color=purples(moment_tensor["r_dc"]),
    )
    ax.line(
        decomp["t_plunge"],
        decomp["t_trend"],
        ".",
        markeredgecolor="0.7",
        linewidth=0.25,
        color=greens(moment_tensor["r_dc"]),
    )

decomp_true = decompose_MT(source["moment_tensor"])
ax.line(
    decomp_true["p_plunge"],
    decomp_true["p_trend"],
    "o",
    color="mediumorchid",
    zorder=10,
    markeredgecolor="k",
)
ax.line(
    decomp_true["t_plunge"],
    decomp_true["t_trend"],
    "o",
    color="seagreen",
    zorder=10,
    markeredgecolor="k",
)

ax.grid()

scale = np.linspace(0, 1, 100)

ax_cb.pcolor(
    [0, 1],
    scale,
    np.array(np.matrix(scale).T * np.matrix([1, 1])),
    cmap=purples,
)
ax_cb.pcolor(
    [1, 2],
    scale,
    np.array(np.matrix(scale).T * np.matrix([1, 1])),
    cmap=greens,
)
ax_cb.yaxis.set_label_position("right")
ax_cb.yaxis.tick_right()
ax_cb.set_ylabel("R$^{2}$")
ax_cb.set_xticks([0.5, 1.5])
ax_cb.set_xticklabels(["P", "T"])
ax_cb.set_xlabel("axes")

f1, a1 = plt.subplots()
a1.set_aspect("equal")
a1.scatter(x, y, c=amps, marker="o", edgecolors="k", cmap=bwr, vmin=-1, vmax=1)
a1.plot(source["e"], source["n"], "r*", markeredgecolor="k")
a1.set_xlabel("Easting (m)")
a1.set_ylabel("Northing (m)")

if compare_general_solution:
    mt_comp = np.matrix(moment_tensor["general"]).T
else:
    mt_comp = np.matrix(moment_tensor["dc"]).T

f3, a3 = plt.subplots()
a3.set_aspect("equal")
a3.plot(right_hand_side, sqa(A_matrix * mt_comp), "k.")
x1, x2 = a3.get_xlim()
y1, y2 = a3.get_ylim()
xy_max = 1.05 * max([-x1, x2, -y1, y2])
a3.plot([-xy_max, xy_max], [-xy_max, xy_max], "r--", zorder=-10)
a3.set_xlim([-xy_max, xy_max])
a3.set_ylim([-xy_max, xy_max])
a3.set_xlabel("observed and back-projected")
a3.set_ylabel("modelled")

f4, a4 = plt.subplots()
a4.set_aspect("equal")
a4.scatter(
    x,
    y,
    c=sqa(A_matrix * mt_comp),
    marker="o",
    edgecolors="k",
    cmap=bwr,
    vmin=-0.01,
    vmax=0.01,
)
a4.plot(source["e"], source["n"], "r*", markeredgecolor="k")
a4.set_xlabel("Easting (m)")
a4.set_ylabel("Northing (m)")


plt.show()
