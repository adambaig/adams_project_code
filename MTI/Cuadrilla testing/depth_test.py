import matplotlib

matplotlib.use("Qt5agg")

from functools import reduce
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from obspy.imaging.mopad_wrapper import beach
from obspy import read_inventory
import pickle
from pyproj import Transformer

# from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from sms_moment_tensor.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import mt_matrix_to_vector, mt_to_sdr

from sms_moment_tensor.plotting import (
    plot_regression,
    plot_beachballs,
    plot_beachball_from_sdr,
    plot_fault_planes_from_Focmec,
)
from sms_ray_modelling.raytrace import isotropic_ray_trace


def multiply(x1, x2):
    return x1 * x2


transformer = Transformer.from_crs("epsg:4326", "epsg:27700")

test_pickle = "Pickles//39767.pickle"

f = open(test_pickle, "rb")
pickle_out = pickle.load(f)
f.close()


# inv = formStaXml(athenaCode="CUA", outDir=".")
inv = read_inventory("CUA_Full.xml")
stations = {}
for network in inv:
    net_code = network.code
    for station in network:
        net_stat = net_code + "." + station.code
        stations[net_stat] = {
            "latitude": station.latitude,
            "longitude": station.longitude,
            "elevation": station.elevation,
        }
        easting, northing = transformer.transform(station.latitude, station.longitude)
        stations[net_stat]["e"] = easting
        stations[net_stat]["n"] = northing
        stations[net_stat]["z"] = station.elevation

mean_frequency = np.sqrt(reduce(multiply, pickle_out["bpFreqs"]))
event_latitude = pickle_out["eveDict"]["event"]["origin"]["latitude"]
event_longitude = pickle_out["eveDict"]["event"]["origin"]["longitude"]
event_easting, event_northing = transformer.transform(event_latitude, event_longitude)
event_elevation = -1000.0 * pickle_out["eveDict"]["event"]["origin"]["depth"]
event = {"e": event_easting, "n": event_northing, "z": event_elevation}
velocity_model = pickle_out["velDict"]
A_matrix = []
right_hand_side = []
p_only_rhs = {}
for nslc, amp_data in pickle_out["ampDict"].items():
    net_stat = ".".join(nslc.split(".")[:2])
    phase = nslc[-1]
    s_raypath = isotropic_ray_trace(event, stations[net_stat], velocity_model, "S")
    raypath = {
        "P": isotropic_ray_trace(event, stations[net_stat], velocity_model, "P"),
        "V": s_raypath,
        "H": s_raypath,
    }

    A_matrix.append(inversion_matrix_row(raypath[phase], phase))
    right_hand_side.append(
        back_project_amplitude(
            raypath["P"],
            raypath["V"],
            amp_data["amps"][0],
            phase,
            frequency=mean_frequency,
        )
    )

    if phase == "P":
        p_only_rhs[net_stat] = right_hand_side[-1]
mt_object = solve_moment_tensor(A_matrix, right_hand_side)
mt_dc = mt_matrix_to_vector(mt_object["dc"]) / np.linalg.norm(mt_object["dc"])
mt_gen = mt_matrix_to_vector(mt_object["general"]) / np.linalg.norm(
    mt_object["general"]
)
mt_to_sdr(mt_dc)
pickle_out["eveDict"]["event"]["origin"]["mt"]
beachball_dc = beach(mt_dc, xy=(0.8, 1), mopad_basis="XYZ", width=1)
beachball_gen = beach(mt_gen, xy=(2.2, 1), mopad_basis="XYZ", width=1)
fig, ax = plt.subplots()
ax.set_aspect("equal")
ax.add_collection(beachball_dc)
ax.add_collection(beachball_gen)
ax.set_xlim([0, 3])
ax.set_ylim([0, 2])

norm_color = max(abs(np.array([v for k, v in p_only_rhs.items()]))) / 1000
f2, a2 = plt.subplots()
bwr = cm.get_cmap("bwr")
a2.set_aspect("equal")
a2.plot(event_easting, event_northing, "o", color="0.8", markeredgecolor="k")
for nslc, amp_data in pickle_out["ampDict"].items():
    net_stat = ".".join(nslc.split(".")[:2])
    a2.plot(
        stations[net_stat]["e"],
        stations[net_stat]["n"],
        "v",
        color=bwr(0.5 + p_only_rhs[net_stat] / 2 / norm_color),
        markeredgecolor="k",
    )
plt.show()

list(pickle_out["ampDict"].keys())
