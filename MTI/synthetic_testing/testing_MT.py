import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)

import mplstereonet as mpls


def sqa(x):
    return np.squeeze(np.array(x))


stationx = np.linspace(-2000, 2000, 11)
stationy = np.linspace(-2000, 2000, 11)
surface = -100

source = {
    "x": 10,
    "y": 10,
    "z": 1000,
    "stress_drop": 1e5,
    "moment_tensor": np.matrix([[0.0, 1.0, 0], [0, 0, 0], [0, 0, 0]]),
    "moment_magnitude": 0,
}
time_series = np.linspace(0, 2, 2001)
dt = time_series[1]
velocity_model = [{"vp": 5000, "vs": 3000, "rho": 2400}]
Q = {"P": 60, "S": 60}

p_input, v_input, h_input = [], [], []
theta, phi = [], []
f1, a1 = plt.subplots()
for x in stationx:
    for y in stationy:
        rec = {"x": x, "y": y, "z": surface}
        p_raypath = isotropic_ray_trace(source, rec, velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, rec, velocity_model, "S")
        response = get_response(
            p_raypath, s_raypath, source, rec, Q, time_series, 1e-10
        )
        p_raypath["traveltime"]
        pwin1 = int(p_raypath["traveltime"] / dt) - 5
        pwin2 = pwin1 + 100
        swin1 = int(s_raypath["traveltime"] / dt) - 5
        swin2 = swin1 + 150
        pwin_ned = np.matrix(
            [
                response["n"][pwin1:pwin2] - np.average(response["n"][pwin1:pwin2]),
                response["e"][pwin1:pwin2] - np.average(response["e"][pwin1:pwin2]),
                response["d"][pwin1:pwin2] - np.average(response["d"][pwin1:pwin2]),
            ]
        )
        swin_ned = np.matrix(
            [
                response["n"][swin1:swin2] - np.average(response["n"][swin1:swin2]),
                response["e"][swin1:swin2] - np.average(response["e"][swin1:swin2]),
                response["d"][swin1:swin2] - np.average(response["d"][swin1:swin2]),
            ]
        )

        eigvals, eigvecs = np.linalg.eig(pwin_ned * pwin_ned.T)
        i_sort = np.argsort(eigvals)
        p_dir = sqa(eigvecs[:, i_sort[-1]])
        src2rec = np.array(
            [rec["y"] - source["y"], rec["x"] - source["x"], -rec["z"] + source["z"]]
        )
        if (
            np.dot(p_dir, src2rec) < 0
        ):  # ensure that the p direction points from the source to the receiver
            p_dir *= -1

        # conventions for ray angles from Chapman 2004, eq 4.5.79-81
        # assuming NED

        incoming_theta = np.arccos(p_dir[2])
        incoming_phi = np.arctan2(p_dir[0], p_dir[1])
        sv_dir = np.array(
            [
                np.cos(incoming_theta) * np.cos(incoming_phi),
                np.cos(incoming_theta) * np.sin(incoming_phi),
                -np.sin(incoming_theta),
            ]
        )
        a1.quiver(x, y, np.cos(incoming_phi), np.sin(incoming_phi))
        sh_dir = np.array([-np.sin(incoming_phi), np.cos(incoming_phi), 0])
        ray_rotation_matrix = np.matrix(np.vstack([p_dir, sv_dir, sh_dir]))
        pwin_pvh = ray_rotation_matrix * pwin_ned
        swin_pvh = ray_rotation_matrix * swin_ned
        p_comp = sqa(pwin_pvh[0, :])
        v_comp = sqa(swin_pvh[1, :])
        h_comp = sqa(swin_pvh[2, :])

        p_amp = p_comp[np.argmax(abs(p_comp))]
        v_amp = v_comp[np.argmax(abs(v_comp))]
        h_amp = h_comp[np.argmax(abs(h_comp))]
        p_input.append(
            back_project_amplitude(p_raypath, s_raypath, p_amp, "P", frequency=2)
        )
        v_input.append(
            back_project_amplitude(p_raypath, s_raypath, v_amp, "SV", frequency=2)
        )
        h_input.append(
            back_project_amplitude(p_raypath, s_raypath, h_amp, "SH", frequency=2)
        )
        theta.append(incoming_theta * 180 / np.pi)
        phi.append(incoming_phi * 180 / np.pi)

p_abs = abs(max(p_input))
v_abs = abs(max(v_input))
h_abs = abs(max(h_input))

bwr = cm.get_cmap("bwr")

fig, ax = mpls.subplots(1, 3, figsize=[15, 5])
for ii in range(len(p_input)):
    ax[0].line(
        90 - theta[ii],
        phi[ii],
        "o",
        markeredgecolor="k",
        color=bwr(0.5 * (p_input[ii] + p_abs) / p_abs),
    )
    ax[1].line(
        90 - theta[ii],
        phi[ii],
        "o",
        markeredgecolor="k",
        color=bwr(0.5 * (v_input[ii] + v_abs) / v_abs),
    )
    ax[2].line(
        90 - theta[ii],
        phi[ii],
        "o",
        markeredgecolor="k",
        color=bwr(0.5 * (h_input[ii] + h_abs) / h_abs),
    )
theta
plt.show()
