import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import mplstereonet as mpls
from random import sample, random
from matplotlib import cm

from randomize_seismicity.moment_tensor import dc
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import decompose_MT
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response

from read_inputs import read_stations, get_velocity_model

bwr = cm.get_cmap("bwr")

n_subsample = 10
flip_rate = 0.0
n_iterations = 100

stations = read_stations()
velocity_model = get_velocity_model()
source = {
    "x": np.average([v["x"] for k, v in stations.items()]),
    "y": np.average([v["y"] for k, v in stations.items()]),
    "z": 2700,
    "moment_tensor": np.matrix([[0, 0, 0], [0, 0, 1], [0, 1, 0]]),
    "stress_drop": 1e6,
    "moment_magnitude": 0,
}
Q = {"P": 80, "S": 60}
time_series = np.linspace(0, 4, 2001)
fig, ax = mpls.subplots()
waveforms = {}

for station in stations:
    p_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "P")
    s_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "S")
    waveforms[station] = get_response(
        p_raypath, s_raypath, source, stations[station], Q, time_series, 1e-9
    )

amps = np.zeros(len(stations))
for i_station, station in enumerate(stations):
    trace = waveforms[station]["d"]
    p_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "P")
    s_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "S")
    i_p_time = int(p_raypath["traveltime"] // time_series[1])
    i_peak = np.argmax(abs(trace[i_p_time : i_p_time + 20])) + i_p_time
    cos_incoming = np.sqrt(
        1.0
        - (p_raypath["hrz_slowness"]["x"] ** 2 + p_raypath["hrz_slowness"]["y"] ** 2)
        * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
    )
    amps[i_station] = trace[i_peak] / cos_incoming
amps = amps / max(abs(amps))
decomp_true = decompose_MT(source["moment_tensor"])
ax.line(
    decomp_true["p_plunge"],
    decomp_true["p_trend"],
    "o",
    color="mediumorchid",
    zorder=10,
    markeredgecolor="k",
)
ax.line(
    decomp_true["t_plunge"],
    decomp_true["t_trend"],
    "o",
    color="seagreen",
    zorder=10,
    markeredgecolor="k",
)
ax.grid()
f1, a1 = plt.subplots()
a1.set_aspect("equal")
a1.scatter(
    [v["x"] for k, v in stations.items()],
    [v["y"] for k, v in stations.items()],
    c=amps,
    marker="o",
    edgecolors="k",
    cmap=bwr,
    vmin=-1,
    vmax=1,
)
a1.plot(source["x"], source["y"], "r*", markeredgecolor="k")
a1.set_xlabel("Easting (m)")
a1.set_ylabel("Northing (m)")
plt.show()
