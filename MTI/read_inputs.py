def read_catalog():

    dr = (
        "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\"
        + "Chevron Synthetics\\2019-04-03T18-28\\"
    )

    f = open(dr + "synthetic_catalog.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        dat, tim = lspl[:2]
        event = dat + "T" + tim + "Z"
        events[event] = {}
        events[event]["e"], events[event]["n"], events[event]["z"] = [
            float(s) for s in lspl[2:5]
        ]
        events[event]["magnitude"], events[event]["stress drop"] = [
            float(s) for s in lspl[5:7]
        ]
        events[event]["m11"], events[event]["m22"], events[event]["m33"] = [
            float(s) for s in lspl[7:10]
        ]
        events[event]["m12"], events[event]["m13"], events[event]["m23"] = [
            float(s) for s in lspl[10:13]
        ]
    #        events[event]['amplitude'] = float(lspl[13])
    #        events[event]['rel x'],events[event]['rel y'],events[event]['rel z'] = [
    #                float(s) for s in lspl[14:17]
    #        ]
    return events


def get_velocity_model():
    return [
        {"rho": 2180.0, "vp": 2446.0, "vs": 1069.0},
        {"rho": 2324.0, "vp": 3163.0, "vs": 1513.0, "top": 460.0},
        {"rho": 2421.0, "vp": 3726.0, "vs": 1862.0, "top": -20.0},
        {"rho": 2451.0, "vp": 3911.0, "vs": 2046.0, "top": -220.0},
        {"rho": 2495.0, "vp": 4201.0, "vs": 2301.0, "top": -1140.0},
        {"rho": 2734.0, "vp": 6052.0, "vs": 3303.0, "top": -1480.0},
        {"rho": 2686.0, "vp": 5637.0, "vs": 3027.0, "top": -1720.0},
        {"rho": 2749.0, "vp": 6186.0, "vs": 3275.0, "top": -1920.0},
        {"rho": 2494.0, "vp": 4190.0, "vs": 2288.0, "top": -2320.0},
        {"rho": 2614.0, "vp": 5063.0, "vs": 2687.0, "top": -2560.0},
    ]


def read_stations():
    dr = "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\" + "Chevron Synthetics\\"
    f = open(dr + "vertical_channel_coordinates.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        station_name = "CV." + lspl[0] + "." + lspl[2] + "." + lspl[1]
        stations[station_name] = {}
        stations[station_name]["e"] = float(lspl[3])
        stations[station_name]["n"] = float(lspl[4])
        stations[station_name]["z"] = -float(lspl[5])
    return stations


def project_dir(project):
    if project == "Chevron":
        return (
            "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\"
            + "Chevron Synthetics\\seeds_fs_conv\\"
        )
    elif project == "Conoco":
        return (
            "C:\\\\Users\\adambaig\\Project\\ConocoPhillips 5-27 Proposal\\"
            + "detectability modelling\\"
        )
    elif project == "Tec Petrol":
        return (
            "C:\\\\Users\\adambaig\\Project\\TEC Petrol\\Mar 22 modelling\\"
            + "2019-03-22T14-59\\"
        )
