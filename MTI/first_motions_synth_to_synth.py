import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
from obspy import read
from obspy.imaging.mopad_wrapper import beach
from obspy.signal import filter
from obspy.signal.invsim import corn_freq_2_paz
from operator import itemgetter

from .moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)
from ray_modelling.raytrace import isotropic_ray_trace
from ray_modelling.waveform_model import get_response
from ray_modelling.interface_scattering import (
    P_stack_transmission,
    SV_stack_transmission,
    SH_stack_transmission,
)
from read_inputs import read_catalog, project_dir, read_stations

project = "Chevron"
dr = (
    "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\"
    + "Chevron Synthetics\\2019-04-03T18-28\\"
)
catalog = read_catalog()
stations = read_stations()
velocity_model = [
    {"rho": 2180.0, "vp": 2446.0, "vs": 1069.0, "color": "bisque"},
    {
        "rho": 2324.0,
        "vp": 3163.0,
        "vs": 1513.0,
        "top": -460.0,
        "color": "lightsteelblue",
    },
    {"rho": 2421.0, "vp": 3726.0, "vs": 1862.0, "top": 20.0, "color": "tan"},
    {"rho": 2451.0, "vp": 3911.0, "vs": 2046.0, "top": 220.0, "color": "saddlebrown"},
    {
        "rho": 2495.0,
        "vp": 4201.0,
        "vs": 2301.0,
        "top": 1140.0,
        "color": "mediumseagreen",
    },
    {"rho": 2734.0, "vp": 6052.0, "vs": 3303.0, "top": 1480.0, "color": "indigo"},
    {"rho": 2686.0, "vp": 5637.0, "vs": 3027.0, "top": 1720.0, "color": "rosybrown"},
    {
        "rho": 2749.0,
        "vp": 6186.0,
        "vs": 3275.0,
        "top": 1920.0,
        "color": "darkslateblue",
    },
    {"rho": 2494.0, "vp": 4190.0, "vs": 2288.0, "top": 2320.0, "color": "lavender"},
    {"rho": 2614.0, "vp": 5063.0, "vs": 2687.0, "top": 2560.0, "color": "khaki"},
]
Q = {"P": 70, "S": 50}
explosion = {
    "moment_tensor": np.matrix([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]),
    "stress_drop": 3.0e5,
}
time_series = np.arange(0, 4, 0.002)
xcorr_time = np.arange(-3.998, 4, 0.002)
ii = 0
paz_10hz = corn_freq_2_paz(10.0, damp=np.sqrt(0.5))
snr_threshold = 10


def sqa(x):
    return np.squeeze(np.array(x))


gn_sim, dc_sim, mw, cn_gen, cn_dev = [], [], [], [], []
for event in catalog:
    ii += 1
    seedfile = dr + "synthetic" + event.replace(":", "-") + ".mseed"
    st = read(seedfile)
    jj = -1
    corr_array = np.zeros([3999, 69])
    snr = np.zeros(69)
    Amat = []
    rhs = []
    kk = 0
    for trace in st[::4]:
        jj += 1
        id = trace.get_id()
        pRaypath = isotropic_ray_trace(
            catalog[event], stations[id], velocity_model, "P"
        )
        sRaypath = isotropic_ray_trace(
            catalog[event], stations[id], velocity_model, "S"
        )

        explosion["moment_magnitude"] = catalog[event]["magnitude"]
        explosion["x"] = catalog[event]["x"]
        explosion["y"] = catalog[event]["y"]
        explosion["z"] = catalog[event]["z"]
        explosive_response = get_response(
            pRaypath, sRaypath, explosion, stations[id], Q, time_series, 0
        )
        normalized_response = explosive_response["d"] / np.sqrt(
            np.correlate(explosive_response["d"], explosive_response["d"])
        )
        xcorr = np.correlate(trace.data, normalized_response, mode="full")
        corr_array[:, jj] = xcorr
        index, dum = max(enumerate(abs(xcorr[1900:2100])), key=itemgetter(1))
        peak = xcorr[1900 + index]
        snr[jj] = abs(peak) / np.sqrt(np.var(xcorr[1000:1900]))
        if snr[jj] > snr_threshold:
            kk += 1
            hrz_P_slowness_sq = (
                pRaypath["hrz_slowness"]["x"] ** 2 + pRaypath["hrz_slowness"]["y"] ** 2
            )
            vp_receiver = pRaypath["velocity_model_chunk"][-1]["v"]
            vp_source = pRaypath["velocity_model_chunk"][0]["v"]
            velocity_and_rho = []
            for layer in zip(
                pRaypath["velocity_model_chunk"], sRaypath["velocity_model_chunk"]
            ):
                velocity_and_rho.append(
                    {"rho": layer[0]["rho"], "vp": layer[0]["v"], "vs": layer[1]["v"]}
                )
            cos_incoming_P = np.sqrt(1.0 - hrz_P_slowness_sq * vp_receiver**2)
            sin_takeoff_P = np.sqrt(hrz_P_slowness_sq) * vp_source
            cos_takeoff_P = np.sqrt(1.0 - hrz_P_slowness_sq * vp_source**2)
            azimuth = np.arctan2(
                stations[id]["y"] - catalog[event]["y"],
                stations[id]["x"] - catalog[event]["x"],
            )
            p_trans = P_stack_transmission(velocity_and_rho, np.sqrt(hrz_P_slowness_sq))
            rhs.append(
                peak * pRaypath["geometrical_spreading"] / cos_incoming_P / p_trans
            )
            Amat.append(
                [
                    sin_takeoff_P**2 * np.sin(azimuth) ** 2,
                    sin_takeoff_P**2 * np.sin(2.0 * azimuth),
                    sin_takeoff_P**2 * np.cos(azimuth) ** 2,
                    2.0 * sin_takeoff_P * cos_takeoff_P * np.sin(azimuth),
                    2.0 * sin_takeoff_P * cos_takeoff_P * np.cos(azimuth),
                    np.cos(azimuth) ** 2,
                ]
            )
    if kk > 6:
        rhs = np.matrix(rhs)
        Amat = np.matrix(Amat)
        Amat_dc = Amat[:, :5]
        m22, m12, m11, m23, m13, m33 = [
            sqa(x) for x in np.linalg.inv(Amat.T * Amat) * Amat.T * rhs.T
        ]
        cn_gen.append(np.linalg.cond(Amat.T * Amat))
        mt_in = [
            catalog[event]["m11"],
            catalog[event]["m22"],
            catalog[event]["m33"],
            catalog[event]["m12"],
            catalog[event]["m13"],
            catalog[event]["m23"],
        ]
        mt_out = [m11, m22, m33, m12, m13, m23]
        mtdev22, mtdev12, mtdev11, mtdev23, mtdev13 = [
            sqa(x) for x in np.linalg.inv(Amat_dc.T * Amat_dc) * Amat_dc.T * rhs.T
        ]
        mtdev33 = -mtdev11 - mtdev22
        devMT = np.matrix(
            [
                [mtdev11, mtdev12, mtdev13],
                [mtdev12, mtdev22, mtdev23],
                [mtdev13, mtdev23, mtdev33],
            ]
        )
        cn_dev.append(np.linalg.cond(Amat_dc.T * Amat_dc))
        eigvals, eigvecs = np.linalg.eig(devMT)
        # remove clvd from deviatoric solution a la Strelitz, 1989
        i_sort = np.argsort(eigvals)
        dc_eigs = np.zeros(3)
        dc_eigs[i_sort[0]] = eigvals[i_sort[0]] + eigvals[i_sort[1]] / 2.0
        dc_eigs[i_sort[2]] = eigvals[i_sort[2]] + eigvals[i_sort[1]] / 2.0
        dcMT = eigvecs * np.diag(dc_eigs) * eigvecs.T
        mt_dc = [dcMT[0, 0], dcMT[1, 1], dcMT[2, 2], dcMT[0, 1], dcMT[0, 2], dcMT[1, 2]]
        beachball_in = beach(
            mt_in, xy=(1, 5), zorder=2, width=1, mopad_basis="NED", linewidth=0.5
        )
        beachball_out = beach(
            mt_out, xy=(1, 3), zorder=2, width=1, mopad_basis="NED", linewidth=0.5
        )
        beachball_dc = beach(
            mt_dc, xy=(1, 1), zorder=2, width=1, mopad_basis="NED", linewidth=0.5
        )
        """
        fig = plt.figure()
        ax1 = fig.add_axes([0.1,0.4,0.6,0.45])
        cmax = np.amax(abs(corr_array))
        ax1.pcolor(range(1,70),xcorr_time[1800:2200],corr_array[1800:2200,:],
                    cmap="bwr",vmin=-cmax,vmax=cmax)
        ax1.set_title("M$_{W}$ = "+str(catalog[event]["magnitude"]).replace('-','$-$'))
        ax1.set_ylabel('lag time (s)')
        ax2 = fig.add_axes([0.1,0.1,0.6,0.2])
        ax2.set_ylabel('SNR (dB)')
        ax2.set_xlabel('station')
        ax2.plot(range(1,70),10*np.log10(snr))
        ax2.plot([1,69],[10*np.log10(snr_threshold),10*np.log10(snr_threshold)],'firebrick')
        ax2.set_ylim([0,50])
        ax2.set_xlim([1,69])
        ax2.text(2,42,
                ('%i stations above SNR %.1f dB' %(kk,10*np.log10(snr_threshold))))
        ax3 = fig.add_axes([0.75,0.25,0.2,0.5])
        ax3.set_aspect('equal')
        ax3.text(1,5.6,'input',ha='center')
        ax3.text(1,3.6,'gen.',ha='center')
        ax3.text(1,2.4,('CN = %.1f' % cn_gen[-1]),ha='center',va='top')
        ax3.text(1,1.6,'DC',ha='center')
        ax3.text(1,0.4,('CN = %.1f' % cn_dev[-1]),ha='center',va='top')
        ax3.set_xlim([0,2])
        ax3.set_ylim([0,6])
        ax3.add_collection(beachball_in)
        ax3.add_collection(beachball_out)
        ax3.add_collection(beachball_dc)
        ax3.axis('off')

        fig.savefig('synthetic_polarities//'+event.replace(':','-')+'.png')
        plt.close('all')
        """
        inputMT = np.matrix(
            [
                [catalog[event]["m11"], catalog[event]["m12"], catalog[event]["m13"]],
                [catalog[event]["m12"], catalog[event]["m22"], catalog[event]["m23"]],
                [catalog[event]["m13"], catalog[event]["m23"], catalog[event]["m33"]],
            ]
        )
        outMT = np.matrix([[m11, m12, m13], [m12, m22, m23], [m13, m23, m33]])
        gn_sim.append(
            np.tensordot(
                outMT / np.linalg.norm(outMT), inputMT / np.linalg.norm(inputMT)
            )
        )
        dc_sim.append(
            np.tensordot(dcMT / np.linalg.norm(dcMT), inputMT / np.linalg.norm(inputMT))
        )
        mw.append(catalog[event]["magnitude"])

# plt.show()

fig, ax = plt.subplots()
scatter = ax.scatter(mw, gn_sim, c=cn_gen, marker="v", edgecolor="0.5", vmin=1, vmax=40)
ax.scatter(mw, dc_sim, c=cn_dev, marker="o", edgecolor="0.5", vmin=1, vmax=40)
ax.set_xlabel("moment magnitude")
ax.set_ylabel("mechanism similarity")
cb = fig.colorbar(scatter)
cb.set_label("Condition Number")
ax.legend(["general solution", "forced DC"])
plt.show()
