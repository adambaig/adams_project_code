import numpy as np
from read_chevron_0204_data import read_events

f = open("MT_output_xcorrfilt_location01.csv")
head = f.readline()
lines = f.readlines()
f.close()

events = read_events()
line = lines[1]
events
g = open("MT_output_xcorrfilt_location01_fixed.csv", "w")
hsplit = head.split(",")


def calib_mag(amp):
    slope = 0.9748814213323481
    yint = -1.4460873670177525
    return slope * np.log10(amp) + yint


g.write(",".join(hsplit[:2]) + ",")
g.write(
    ",".join(
        [
            "easting",
            "northing",
            "depth",
            "isShallow",
            "stack_amp",
            "calibrated_Mw",
        ]
    )
)
g.write("," + ",".join(hsplit[2:]))
for line in lines:
    lspl = line.split(",")
    timestamp = lspl[0].split(".")[0][-14:] + "." + lspl[0].split(".")[1]
    id = lspl[0].split(".")[0][:-14]
    if id == "":
        id = events[timestamp]["id"]
    g.write(",".join([id, timestamp]))
    g.write(
        ",%.2f,%.2f,%.2f,"
        % (
            events[timestamp]["easting"],
            events[timestamp]["northing"],
            events[timestamp]["depth"],
        )
    )
    g.write(str(events[timestamp]["isShallow"]))
    g.write(
        ",%.7e, %.3f,"
        % (
            events[timestamp]["stack_amp"],
            calib_mag(events[timestamp]["stack_amp"]),
        )
    )
    g.write(",".join(lspl[1:]))

g.close()
