import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
from obspy import read, UTCDateTime, Trace
from obspy.imaging.mopad_wrapper import beach
from obspy.signal import filter
from obspy.signal.invsim import corn_freq_2_paz
from obspy.core.trace import Stats
from operator import itemgetter
from scipy.fftpack import fft, ifft, fftfreq
from scipy.signal import hilbert

from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.interface_scattering import (
    P_stack_transmission,
    SV_stack_transmission,
    SH_stack_transmission,
)
from moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from read_inputs import get_velocity_model, read_stations
from read_chevron_0204_data import read_events, read_waveform, read_picks, read_wells

matplotlib.rcParams.update({"font.size": 12})

dr = (
    "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\"
    + "Chevron Synthetics\\2019-04-03T18-28\\"
)
plot_out = False
lwin = 50
catalog = read_events()
stations = read_stations()
velocity_model = get_velocity_model()
wells = read_wells()
Q = {"P": 70, "S": 50}
explosion = {
    "moment_tensor": np.matrix([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]),
    "stress_drop": 3.0e5,
}
xcorr_time = np.arange(-2.998, 3, 0.002)
ii = 0
paz_10hz = corn_freq_2_paz(10.0, damp=np.sqrt(0.5))
snr_threshold = 1.5
n_stations = 69
bandpass_hf = 50.0
bandpass_lf = 20.0
lwin2 = lwin // 2


def sqa(x):
    return np.squeeze(np.array(x))


def createTraceStats(network, n1, o1, d1, component):
    # make a stats file for a seed trace
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


mid_events = {k: v for (k, v) in catalog.items() if (v["magnitude"] > -1)}

# mid_events = {k: v for (k, v) in catalog.items() if (k == "20161121041745.390000")}
xcorr_time = np.arange(-3, 3.001, 0.002)

nevents = 1000
i_event = 0
for location in ["01", "02", "03"]:
    i_event += 1
    statics = {}
    f = open("aggregate_statics" + location + ".csv")
    head = f.readline()
    for line in f.readlines():
        lspl = line.split(",")
        statics[lspl[0]] = float(lspl[1])
    f.close()
    f_f = open("MT_output_filtered_location" + location + ".csv", "w")
    f_f.write(
        "event time,magnitude,n picks,m11,m12,m13,m22,m23,m33,dc11,"
        + "dc12,dc13,dc22,dc23,dc33,cn_gen,cn_dv,rsq_gen,rsq_dc\n"
    )
    f_x = open("MT_output_xcorr_location" + location + ".csv", "w")
    f_x.write(
        "event time,magnitude,n picks,m11,m12,m13,m22,m23,m33,dc11,"
        + "dc12,dc13,dc22,dc23,dc33,cn_gen,cn_dv,rsq_gen,rsq_dc\n"
    )
    for event in mid_events:
        jj = -1
        st = read_waveform(UTCDateTime(event))
        picks = read_picks(event)
        if plot_out:
            fig0 = plt.figure(figsize=[16, 9])
            ax0 = fig0.add_axes([0.05, 0.1, 0.25, 0.8])
            ax1 = fig0.add_axes([0.35, 0.1, 0.25, 0.8])
            ax2 = fig0.add_axes([0.65, 0.1, 0.25, 0.8])
        dat = np.zeros([1501, n_stations])
        norm_dat = np.zeros([1501, n_stations])
        norm_f_dat = np.zeros([1501, n_stations])
        xcor_dat = np.zeros([1501, n_stations])
        filtered_dat = np.zeros([1501, n_stations])
        time_series = np.arange(st[0].stats.npts) * st[0].stats.delta
        tsfreq = fftfreq(len(time_series), time_series[1])
        winfreq = fftfreq(100, time_series[1])
        if picks is None:
            print("picks needed for event " + event)
        else:
            location_picks = {
                k: v for k, v in picks.items() if ("." + location + "." in k)
            }
            if len(location_picks) > 0:
                p_raypath = [object() for _ in range(n_stations)]
                s_raypath = [object() for _ in range(n_stations)]
                for trace in st:
                    if trace.stats.location == location:
                        expl_channel_object = {
                            "component": "Z",
                            "channel": "CPZ",
                            "station": "dummy",
                            "network": "CV",
                            "location": "04",
                        }
                        expl_stats = createTraceStats(
                            "CV",
                            trace.stats.npts,
                            trace.stats.starttime,
                            trace.stats.delta,
                            expl_channel_object,
                        )
                        jj += 1
                        id = trace.get_id()
                        source = {
                            "x": mid_events[event]["easting"],
                            "y": mid_events[event]["northing"],
                            "z": mid_events[event]["depth"] - 910,
                        }
                        p_raypath[jj] = isotropic_ray_trace(
                            source, stations[id], velocity_model, "P"
                        )
                        s_raypath[jj] = isotropic_ray_trace(
                            source, stations[id], velocity_model, "S"
                        )
                        explosion["moment_magnitude"] = catalog[event]["magnitude"]
                        explosion["x"] = source["x"]
                        explosion["y"] = source["y"]
                        explosion["z"] = source["z"]

                        explosive_response = np.real(
                            ifft(
                                fft(
                                    get_response(
                                        p_raypath[jj],
                                        s_raypath[jj],
                                        explosion,
                                        stations[id],
                                        Q,
                                        time_series,
                                        0,
                                    )["d"]
                                )
                                * np.exp(
                                    2.0j
                                    * np.pi
                                    * (p_raypath[jj]["traveltime"] - 1)
                                    * tsfreq
                                )
                            )
                        )

                        normalized_response = explosive_response / np.sqrt(
                            np.correlate(explosive_response, explosive_response)
                        )
                        expl_trace = Trace(data=normalized_response, header=expl_stats)
                        expl_trace.simulate(paz_simulate=paz_10hz)
                        predicted_onset = location_picks[id]["P"] + statics[id]
                        dat[:, jj] = np.real(
                            ifft(
                                fft(trace.data)
                                * np.exp(
                                    2.0j
                                    * np.pi
                                    * (
                                        predicted_onset
                                        - UTCDateTime(event).timestamp
                                        - 0.85
                                    )
                                    * tsfreq
                                )
                            )
                        )
                        disp_spec = np.array(
                            10e-8
                            * abs(fft(dat[500 : 500 + lwin, jj]))[:lwin2]
                            / winfreq[:lwin2]
                            / lwin
                            / 2
                            / np.pi
                        )
                        noise_spec = np.array(
                            10e-8
                            * abs(fft(dat[400 - lwin : 400, jj]))[:lwin2]
                            / winfreq[:lwin2]
                            / lwin
                            / 2
                            / np.pi
                        )
                        igood = np.where(disp_spec / noise_spec > snr_threshold)[0]
                        xcor_dat[:, jj] = np.correlate(
                            dat[:, jj], normalized_response, "same"
                        )
                        filtered_dat[:, jj] = filter.bandpass(
                            dat[:, jj], bandpass_lf, bandpass_hf, 500, zerophase=True
                        )
                        if plot_out:
                            ax0.plot(
                                time_series - 1,
                                jj + 1 + dat[:, jj] / max(abs(dat[:, jj])),
                                "0.2",
                            )
                            ax1.plot(
                                time_series - 1,
                                jj
                                + 1
                                + filtered_dat[:, jj] / max(abs(filtered_dat[:, jj])),
                                "0.2",
                                zorder=-1,
                            )
                            ax2.plot(
                                time_series - time_series[-1] / 2,
                                jj + 1 + xcor_dat[:, jj] / max(abs(xcor_dat[:, jj])),
                                "0.2",
                                zorder=-1,
                            )
                        norm_dat[:, jj] = xcor_dat[:, jj] / max(abs(xcor_dat[:, jj]))
                        norm_f_dat[:, jj] = filtered_dat[:, jj] / max(
                            abs(filtered_dat[:, jj])
                        )
            envelopes = hilbert(norm_dat, axis=0)
            envelopes *= np.conj(envelopes)
            i_x_max = np.argmax(np.sum(np.real(envelopes), axis=-1)[750:790]) + 750
            envelopes = hilbert(norm_f_dat, axis=0)
            envelopes *= np.conj(envelopes)
            i_f_max = np.argmax(np.sum(np.real(envelopes), axis=-1)[500:540]) + 500
            right_hand_side_filt, right_hand_side_xcor = [], []
            inversion_matrix_filt, inversion_matrix_xcor = [], []
            signal_filt, noise_filt, snr_filt, amp_filt = (
                np.zeros(n_stations),
                np.zeros(n_stations),
                np.zeros(n_stations),
                np.zeros(n_stations),
            )
            signal_xcor, noise_xcor, snr_xcor, amp_xcor = (
                np.zeros(n_stations),
                np.zeros(n_stations),
                np.zeros(n_stations),
                np.zeros(n_stations),
            )
            if plot_out:
                ax0.set_xlim([-0.4, 0.4])
                ax1.set_xlim([-0.4, 0.4])
                ax2.set_xlim([-0.4, 0.4])
                ax1.set_title(
                    event
                    + (" Mw = %.1f" % mid_events[event]["magnitude"]).replace(
                        "-", "$-$"
                    )
                )
                ax0.set_ylabel("raw data")
                ax1.set_ylabel("filtered data")
                ax2.set_ylabel("cross-correlated data")
                ax0.set_xlabel("frequency")
                ax1.set_xlabel("shifted time (s)")
                ax2.set_xlabel("shifted time (s)")
            for jj in range(n_stations):
                n_amp_win = int(500 / bandpass_hf / 2)
                i_trace_max = (
                    np.argmax(
                        abs(filtered_dat[i_f_max - n_amp_win : i_f_max + n_amp_win, jj])
                    )
                    + i_f_max
                    - n_amp_win
                )
                amp_filt[jj] = filtered_dat[i_trace_max, jj]
                signal_filt[jj] = np.std(
                    filtered_dat[i_f_max - n_amp_win : i_f_max + n_amp_win, jj]
                )
                noise_filt[jj] = np.std(filtered_dat[: 300 + 2 * n_amp_win, jj])
                snr_filt[jj] = signal_filt[jj] / noise_filt[jj]
                if plot_out:
                    ax1.text(-0.39, jj + 1.01, ("%.1f" % (snr_filt[jj])), fontsize=6)
                if snr_filt[jj] > snr_threshold:
                    if plot_out:
                        if amp_filt[jj] > 0:
                            ax1.plot(
                                time_series[i_trace_max] - 1,
                                jj + 1 + amp_filt[jj] / max(abs(filtered_dat[:, jj])),
                                "o",
                                color="orangered",
                                markeredgecolor="k",
                                zorder=2,
                                alpha=0.3,
                            )
                            ax1.plot(
                                time_series[i_f_max - n_amp_win : i_f_max + n_amp_win]
                                - 1,
                                jj
                                + 1
                                + filtered_dat[
                                    i_f_max - n_amp_win : i_f_max + n_amp_win, jj
                                ]
                                / max(abs(filtered_dat[:, jj])),
                                color="orangered",
                                lw=2,
                                zorder=1,
                            )
                        else:
                            ax1.plot(
                                time_series[i_f_max] - 1,
                                jj + 1 + amp_filt[jj] / max(abs(filtered_dat[:, jj])),
                                "o",
                                color="turquoise",
                                markeredgecolor="k",
                                zorder=2,
                                alpha=0.3,
                            )
                            ax1.plot(
                                time_series[i_f_max - n_amp_win : i_f_max + n_amp_win]
                                - 1,
                                jj
                                + 1
                                + filtered_dat[
                                    i_f_max - n_amp_win : i_f_max + n_amp_win, jj
                                ]
                                / max(abs(filtered_dat[:, jj])),
                                color="turquoise",
                                lw=2,
                                zorder=1,
                            )
                    cos_incoming = np.sqrt(
                        1.0
                        - (
                            p_raypath[jj]["hrz_slowness"]["x"] ** 2
                            + p_raypath[jj]["hrz_slowness"]["y"] ** 2
                        )
                        * p_raypath[jj]["velocity_model_chunk"][-1] ** 2
                    )
                    right_hand_side_filt.append(
                        back_project_amplitude(
                            p_raypath[jj],
                            s_raypath[jj],
                            amp_filt[jj] / cos_incoming,
                            "P",
                        )
                    )
                    inversion_matrix_filt.append(
                        inversion_matrix_row(p_raypath[jj], "P")
                    )
                i_trace_max = (
                    np.argmax(
                        abs(xcor_dat[i_x_max - n_amp_win : i_x_max + n_amp_win, jj])
                    )
                    + i_x_max
                    - n_amp_win
                )
                amp_xcor[jj] = xcor_dat[i_trace_max, jj]
                signal_xcor[jj] = np.std(
                    xcor_dat[i_x_max - n_amp_win : i_x_max + n_amp_win, jj]
                )
                noise_xcor[jj] = np.std(xcor_dat[: 450 + 2 * n_amp_win, jj])
                snr_xcor[jj] = signal_xcor[jj] / noise_xcor[jj]
                if plot_out:
                    ax2.text(-0.39, jj + 1.01, ("%.1f" % (snr_xcor[jj])), fontsize=6)
                if snr_xcor[jj] > snr_threshold:
                    if plot_out:
                        if amp_xcor[jj] > 0:
                            ax2.plot(
                                time_series[i_trace_max] - time_series[-1] / 2,
                                jj + 1 + amp_xcor[jj] / max(abs(xcor_dat[:, jj])),
                                "o",
                                color="orangered",
                                markeredgecolor="k",
                                zorder=2,
                                alpha=0.3,
                            )
                            ax2.plot(
                                time_series[i_x_max - n_amp_win : i_x_max + n_amp_win]
                                - time_series[-1] / 2,
                                jj
                                + 1
                                + xcor_dat[
                                    i_x_max - n_amp_win : i_x_max + n_amp_win, jj
                                ]
                                / max(abs(xcor_dat[:, jj])),
                                color="orangered",
                                lw=2,
                                zorder=1,
                            )
                        else:
                            ax2.plot(
                                time_series[i_x_max] - time_series[-1] / 2,
                                jj + 1 + amp_xcor[jj] / max(abs(xcor_dat[:, jj])),
                                "o",
                                color="turquoise",
                                markeredgecolor="k",
                                zorder=2,
                                alpha=0.3,
                            )
                            ax2.plot(
                                time_series[i_x_max - n_amp_win : i_x_max + n_amp_win]
                                - time_series[-1] / 2,
                                jj
                                + 1
                                + xcor_dat[
                                    i_x_max - n_amp_win : i_x_max + n_amp_win, jj
                                ]
                                / max(abs(xcor_dat[:, jj])),
                                color="turquoise",
                                lw=2,
                                zorder=1,
                            )
                    cos_incoming = np.sqrt(
                        1.0
                        - (
                            p_raypath[jj]["hrz_slowness"]["x"] ** 2
                            + p_raypath[jj]["hrz_slowness"]["y"] ** 2
                        )
                        * p_raypath[jj]["velocity_model_chunk"][-1] ** 2
                    )
                    right_hand_side_xcor.append(
                        back_project_amplitude(
                            p_raypath[jj],
                            s_raypath[jj],
                            amp_xcor[jj] / cos_incoming,
                            "P",
                        )
                    )
                    inversion_matrix_xcor.append(
                        inversion_matrix_row(p_raypath[jj], "P")
                    )
            if sqa(right_hand_side_filt).shape is ():
                n_measurements = 0
            else:
                n_measurements = len(sqa(right_hand_side_filt))
            if n_measurements > 6:
                (
                    mt_gen_f,
                    cn_gen_f,
                    rsq_gen_f,
                    mt_dc_f,
                    cn_dev_f,
                    rsq_dc_f,
                ) = solve_moment_tensor(inversion_matrix_filt, right_hand_side_filt)
                f_f.write(
                    event
                    + ","
                    + str(mid_events[event]["magnitude"])
                    + ","
                    + str(n_measurements)
                )
                f_f.write(
                    ",%.6e,%.6e,%.6e,%.6e,%.6e,%.6e"
                    % (
                        mt_gen_f[0],
                        mt_gen_f[1],
                        mt_gen_f[2],
                        mt_gen_f[3],
                        mt_gen_f[4],
                        mt_gen_f[5],
                    )
                )
                f_f.write(
                    ",%.6e,%.6e,%.6e,%.6e,%.6e,%.6e"
                    % (
                        mt_dc_f[0],
                        mt_dc_f[1],
                        mt_dc_f[2],
                        mt_dc_f[3],
                        mt_dc_f[4],
                        mt_dc_f[5],
                    )
                )
                f_f.write(
                    ",%.1f,%.1f,%.3f,%.3f\n" % (cn_gen_f, cn_dev_f, rsq_gen_f, rsq_dc_f)
                )
            else:
                f_f.write(event + "," + str(mid_events[event]["magnitude"]) + "0")
                f_f.write(",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n")
            if sqa(right_hand_side_xcor).shape is ():
                n_measurements = 0
            else:
                n_measurements = len(sqa(right_hand_side_xcor))
            if n_measurements > 6:
                (
                    mt_gen_x,
                    cn_gen_x,
                    rsq_gen_x,
                    mt_dc_x,
                    cn_dev_x,
                    rsq_dc_x,
                ) = solve_moment_tensor(inversion_matrix_xcor, right_hand_side_xcor)
                f_x.write(
                    event
                    + ","
                    + str(mid_events[event]["magnitude"])
                    + ","
                    + str(n_measurements)
                )
                f_x.write(
                    ",%.6e,%.6e,%.6e,%.6e,%.6e,%.6e"
                    % (
                        mt_gen_x[0],
                        mt_gen_x[1],
                        mt_gen_x[2],
                        mt_gen_x[3],
                        mt_gen_x[4],
                        mt_gen_x[5],
                    )
                )
                f_x.write(
                    ",%.6e,%.6e,%.6e,%.6e,%.6e,%.6e"
                    % (
                        mt_dc_x[0],
                        mt_dc_x[1],
                        mt_dc_x[2],
                        mt_dc_x[3],
                        mt_dc_x[4],
                        mt_dc_x[5],
                    )
                )
                f_x.write(
                    ",%.1f,%.1f,%.3f,%.3f\n" % (cn_gen_x, cn_dev_x, rsq_gen_x, rsq_dc_x)
                )
            else:
                f_x.write(event + "," + str(mid_events[event]["magnitude"]) + "0")
                f_x.write(",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n")
    if plot_out:
        fig2, ax3 = plt.subplots()
        ax3.plot(amp_xcor, amp_filt, ".")
        ax3.set_xlabel("cross correlation amp")
        ax3.set_ylabel("filtered trace amp")
        fig3, ax4 = plt.subplots()
        ax4.set_aspect("equal")
        beachball_gn_f = beach(
            mt_gen_f, xy=(1, 3), zorder=2, width=1, mopad_basis="NED", linewidth=0.5
        )
        beachball_dc_f = beach(
            mt_dc_f, xy=(1, 1), zorder=2, width=1, mopad_basis="NED", linewidth=0.5
        )
        beachball_gn_x = beach(
            mt_gen_x, xy=(3, 3), zorder=2, width=1, mopad_basis="NED", linewidth=0.5
        )
        beachball_dc_x = beach(
            mt_dc_x, xy=(3, 1), zorder=2, width=1, mopad_basis="NED", linewidth=0.5
        )
        ax4.add_collection(beachball_dc_f)
        ax4.add_collection(beachball_dc_x)
        ax4.add_collection(beachball_gn_f)
        ax4.add_collection(beachball_gn_x)
        ax4.set_xlim([0, 4])
        ax4.set_ylim([0, 4])
        ax4.axis("off")
        plt.show()
    if i_event == nevents:
        break
f_f.close()
f_x.close()
