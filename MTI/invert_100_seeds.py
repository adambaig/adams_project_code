import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt

import glob

from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_moment_tensor.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import decompose_MT
from obspy import read
from basic_operations.rotation import rotate_PVH
import mplstereonet as mpls

import sys

sys.path.append("..")
from read_inputs import read_stations, get_velocity_model


seed_dir = "seeds_100_low_noise//"
fig_p, ax_p = mpls.subplots(2, 3, figsize=[16, 12])
fig_v, ax_v = mpls.subplots(2, 3, figsize=[16, 12])
fig_h, ax_h = mpls.subplots(2, 3, figsize=[16, 12])

stations = {k: v for k, v in read_stations().items() if ".02." in k}
velocity_model = get_velocity_model()
Q = {"P": 80, "S": 60}
time_series = np.linspace(0, 4, 2001)
amps = np.zeros(len(stations))
source = {
    "e": np.average([v["e"] for k, v in stations.items()]),
    "n": np.average([v["n"] for k, v in stations.items()]),
    "z": -2700,
}
for Mij in ["m11", "m22", "m33", "m12", "m13", "m23"]:
    seedfiles = glob.glob(seed_dir + Mij + "*.mseed")
    nseed = len(seedfiles)
    p_tr_p, p_tr_t, p_pl_p, p_pl_t = (
        np.zeros(nseed),
        np.zeros(nseed),
        np.zeros(nseed),
        np.zeros(nseed),
    )
    v_tr_p, v_tr_t, v_pl_p, v_pl_t = (
        np.zeros(nseed),
        np.zeros(nseed),
        np.zeros(nseed),
        np.zeros(nseed),
    )
    h_tr_p, h_tr_t, h_pl_p, h_pl_t = (
        np.zeros(nseed),
        np.zeros(nseed),
        np.zeros(nseed),
        np.zeros(nseed),
    )
    for i_seed, seedfile in enumerate(seedfiles):
        st = read(seedfile)
        p_A_matrix, v_A_matrix, h_A_matrix = [], [], []
        p_amps = np.zeros(len(stations))
        v_amps = np.zeros(len(stations))
        h_amps = np.zeros(len(stations))
        for i_station, station in enumerate(stations):
            p_raypath = isotropic_ray_trace(
                source, stations[station], velocity_model, "P"
            )
            s_raypath = isotropic_ray_trace(
                source, stations[station], velocity_model, "S"
            )
            net_code, sta_code, loc_code, chn_code = station.split(".")
            three_c = st.select(station=sta_code)
            waveforms = {
                "e": three_c.select(channel="??E")[0].data,
                "n": three_c.select(channel="??N")[0].data,
                "z": three_c.select(channel="??Z")[0].data,
            }
            i_s_time = int(s_raypath["traveltime"] // time_series[1])
            i_p_time = int(p_raypath["traveltime"] // time_series[1])
            rot_waveforms, linearity = rotate_PVH(
                waveforms, [i_p_time - 5, i_p_time + 20]
            )
            i_p_peak = (
                np.argmax(abs(rot_waveforms["p"][i_p_time : i_p_time + 20]))
                + i_p_time
            )
            p_amps[i_station] = back_project_amplitude(
                p_raypath, s_raypath, rot_waveforms["p"][i_p_peak], "P", Q=80
            )
            p_A_matrix.append(inversion_matrix_row(p_raypath, "P"))
            i_v_peak = (
                np.argmax(abs(rot_waveforms["v"][i_s_time : i_s_time + 40]))
                + i_s_time
            )
            v_amps[i_station] = back_project_amplitude(
                p_raypath, s_raypath, rot_waveforms["v"][i_v_peak], "SV", Q=60
            )
            v_A_matrix.append(inversion_matrix_row(s_raypath, "SV"))
            i_h_peak = (
                np.argmax(abs(rot_waveforms["h"][i_s_time : i_s_time + 40]))
                + i_s_time
            )
            h_amps[i_station] = back_project_amplitude(
                p_raypath, s_raypath, rot_waveforms["h"][i_h_peak], "SH", Q=60
            )
            h_A_matrix.append(inversion_matrix_row(s_raypath, "SH"))
        mt_gen_p, cn_gen_p, rsq_gen_p, mt_dc_p, cn_dev_p, rsq_dev_p = solve_moment_tensor(
            p_A_matrix, p_amps
        )
        mt_gen_v, cn_gen_v, rsq_gen_v, mt_dc_v, cn_dev_v, rsq_dev_v = solve_moment_tensor(
            v_A_matrix, v_amps
        )
        mt_gen_h, cn_gen_h, rsq_gen_h, mt_dc_h, cn_dev_h, rsq_dev_h = solve_moment_tensor(
            h_A_matrix, h_amps
        )

        p_dec = decompose_MT(mt_dc_p)
        v_dec = decompose_MT(mt_dc_v)
        h_dec = decompose_MT(mt_dc_h)
        p_tr_p[i_seed] = p_dec["p_trend"]
        p_pl_p[i_seed] = p_dec["p_plunge"]
        p_tr_t[i_seed] = p_dec["t_trend"]
        p_pl_t[i_seed] = p_dec["t_plunge"]
        v_tr_p[i_seed] = v_dec["p_trend"]
        v_pl_p[i_seed] = v_dec["p_plunge"]
        v_tr_t[i_seed] = v_dec["t_trend"]
        v_pl_t[i_seed] = v_dec["t_plunge"]
        h_tr_p[i_seed] = h_dec["p_trend"]
        h_pl_p[i_seed] = h_dec["p_plunge"]
        h_tr_t[i_seed] = h_dec["t_trend"]
        h_pl_t[i_seed] = h_dec["t_plunge"]
    mt_component = seedfile.split("\\")[-1].split("_")[0]
    mt_1 = int(mt_component[1])
    mt_2 = int(mt_component[2])
    if mt_1 == mt_2:
        axis_p = ax_p[0, mt_1 - 1]
        axis_v = ax_v[0, mt_1 - 1]
        axis_h = ax_h[0, mt_1 - 1]
        i_row = mt_1 - 1
    else:
        axis_p = ax_p[1, mt_1 + mt_2 - 3]
        axis_v = ax_v[1, mt_1 + mt_2 - 3]
        axis_h = ax_h[1, mt_1 + mt_2 - 3]
        i_row = mt_1 + mt_2

    axis_p.line(p_pl_p, p_tr_p, "o", color="seagreen")
    axis_v.line(v_pl_p, v_tr_p, "o", color="seagreen")
    axis_h.line(h_pl_p, h_tr_p, "o", color="seagreen")
    axis_p.line(p_pl_t, p_tr_t, "o", color="mediumorchid")
    axis_v.line(v_pl_t, v_tr_t, "o", color="mediumorchid")
    axis_h.line(h_pl_t, h_tr_t, "o", color="mediumorchid")

fig_p.text(0.5, 0.95, "P wave only inversions", ha="center")
fig_v.text(0.5, 0.95, "SV wave only inversions", ha="center")
fig_h.text(0.5, 0.95, "SH wave only inversions", ha="center")
fig_p.savefig(seed_dir + "P_only.png")
fig_v.savefig(seed_dir + "SV_only.png")
fig_h.savefig(seed_dir + "SH_only.png")\
