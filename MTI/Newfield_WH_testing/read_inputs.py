import numpy as np
from obspy import UTCDateTime


def read_events():
    f = open(r"events.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        events[lspl[-18]] = {
            "t0": UTCDateTime(lspl[3]),
            "latitude": float(lspl[6]),
            "longitude": float(lspl[7]),
            "elevation": -1000 * float(lspl[12]),
            "Mw": float(lspl[10]),
        }
    return events


def read_velocity_model(vmodel_file):
    f = open(vmodel_file)
    f.readline()
    head = f.readline()
    topline = f.readline()
    lines = f.readlines()
    f.close()
    nl = len(lines)
    lspl = topline.split(",")
    vp = float(lspl[1])
    vs = float(lspl[2])
    velocity_model = [{"vp": vp, "vs": vs, "rho": 310 * vp**0.25}]
    for line in lines:
        top, vp, vs = [float(_) for _ in line.split(",")]
        velocity_model.append(
            {"vp": vp, "vs": vs, "rho": 310 * (vp) ** 0.25, "top": -top}
        )
    return velocity_model
