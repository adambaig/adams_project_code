import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import glob
from read_chevron_0204_data import read_events, read_wells
import random
from moment_tensor_inversion import decompose_MT

files = glob.glob("M.gt.minus1//MT_output_location0[0-9].csv")
nfiles = len(files)

wells = read_wells()
events = read_events()
d2r = np.pi / 180.0
rsq_cut = 0.4
n_picks_cut = 10
selected_location = "02"
f = ["" for _ in range(nfiles)]
location = ["" for _ in range(nfiles)]
lines = [[] for _ in range(nfiles)]

for ifile, MT_file in enumerate(files):
    f[ifile] = open(MT_file)
    head = f[ifile].readline()
    location[ifile] = MT_file.split("location")[1].split(".csv")[0]
    lines[ifile] = f[ifile].readlines()


def normalize(matrix):
    return matrix / np.linalg.norm(matrix)


for jj in range(len(lines[0])):
    event = lines[0][jj].split(",")[0]
    for ii, loc in enumerate(location[:3]):
        if loc == selected_location:
            lspl = lines[ii][jj].split(",")
            if int(lspl[2]) > 0:
                gen = [float(_) for _ in lspl[3:9]]
                dc = [float(_) for _ in lspl[9:15]]
                events[event]["MT_GN"] = normalize(
                    np.matrix(
                        [
                            [gen[0], gen[1], gen[3]],
                            [gen[1], gen[2], gen[4]],
                            [gen[3], gen[4], gen[5]],
                        ]
                    )
                )
                events[event]["MT_DC"] = normalize(
                    np.matrix(
                        [
                            [dc[0], dc[1], dc[3]],
                            [dc[1], dc[2], dc[4]],
                            [dc[3], dc[4], dc[5]],
                        ]
                    )
                )
                events[event]["cn_GN"] = float(lspl[-4])
                events[event]["cn_DV"] = float(lspl[-3])
                events[event]["r2_GN"] = float(lspl[-2])
                events[event]["r2_DC"] = float(lspl[-1])
                events[event]["n_picks"] = float(lspl[2])

fig_p, a_p = plt.subplots(1)
a_p.set_aspect("equal")
fig_t, a_t = plt.subplots(1)
a_t.set_aspect("equal")
moment_tensor_events = {k: v for k, v in events.items() if "MT_DC" in v}
n_mt = len(moment_tensor_events)
easting = np.array([v["easting"] for k, v in moment_tensor_events.items()])
northing = np.array([v["northing"] for k, v in moment_tensor_events.items()])
east0 = np.average(easting)
north0 = np.average(northing)
xarrow_taxis = np.zeros(n_mt)
yarrow_taxis = np.zeros(n_mt)
xarrow_paxis = np.zeros(n_mt)
yarrow_paxis = np.zeros(n_mt)
for ii, event in enumerate(moment_tensor_events):
    params = decompose_MT(moment_tensor_events[event]["MT_DC"])
    xarrow_taxis[ii] = np.sin(d2r * params["t_trend"]) * np.cos(
        d2r * params["t_plunge"]
    )
    yarrow_taxis[ii] = np.cos(d2r * params["t_trend"]) * np.cos(
        d2r * params["t_plunge"]
    )
    xarrow_paxis[ii] = np.sin(d2r * params["p_trend"]) * np.cos(
        d2r * params["p_plunge"]
    )
    yarrow_paxis[ii] = np.cos(d2r * params["p_trend"]) * np.cos(
        d2r * params["p_plunge"]
    )

i_good = np.where(
    (np.array([v["r2_DC"] for k, v in moment_tensor_events.items()]) > rsq_cut)
    & (np.array([v["n_picks"] for k, v in moment_tensor_events.items()]) > n_picks_cut)
    & (np.array([v["cluster"] for k, v in moment_tensor_events.items()]) > -1)
)[0]
a_p.quiver(
    easting[i_good] - east0,
    northing[i_good] - north0,
    xarrow_paxis[i_good],
    yarrow_paxis[i_good],
    pivot="mid",
    headwidth=0,
    headlength=0,
    headaxislength=0,
    color="r",
    angles="xy",
    scale_units="xy",
    scale=0.015,
)
a_t.quiver(
    easting[i_good] - east0,
    northing[i_good] - north0,
    xarrow_taxis[i_good],
    yarrow_taxis[i_good],
    pivot="mid",
    headwidth=0,
    headlength=0,
    headaxislength=0,
    color="b",
    angles="xy",
    scale_units="xy",
    scale=0.015,
)
x1, x2 = a_p.get_xlim()
y1, y2 = a_p.get_ylim()

for well in wells:
    a_p.plot(
        wells[well]["easting"] - east0,
        wells[well]["northing"] - north0,
        "0.4",
        lw=4,
        zorder=-12,
    )
    a_p.plot(
        wells[well]["easting"] - east0,
        wells[well]["northing"] - north0,
        "0.75",
        lw=2,
        zorder=-11,
    )
    a_t.plot(
        wells[well]["easting"] - east0,
        wells[well]["northing"] - north0,
        "0.4",
        lw=4,
        zorder=-12,
    )
    a_t.plot(
        wells[well]["easting"] - east0,
        wells[well]["northing"] - north0,
        "0.75",
        lw=2,
        zorder=-11,
    )
a_p.set_xlim([-978.01230234676336, 910.99433475319825])
a_p.set_ylim([-1074.5324085173663, 1208.3212634823751])
a_p.set_xlabel("Relative Easting (m)")
a_p.set_ylabel("Relative Northing (m)")
a_t.set_xlim([-978.01230234676336, 910.99433475319825])
a_t.set_ylim([-1074.5324085173663, 1208.3212634823751])
a_t.set_xlabel("Relative Easting (m)")
a_t.set_ylabel("Relative Northing (m)")

plt.show()
len(i_good)
