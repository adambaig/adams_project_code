import matplotlib.pyplot as plt
import numpy as np
from sms_moment_tensor.MT_math import mt_vector_to_matrix


f = open("DC_constraint//comparison_WTX.csv")
head = f.readline()
lines = f.readlines()
f.close()

events = {}
for line in lines:
    lspl = line.split(",")
    event_id = lspl[0]
    events[event_id] = {
        "datetime": lspl[1],
        "Ml": float(lspl[2]),
        "full MT": mt_vector_to_matrix([float(m) for m in lspl[3:9]]),
        "clvd": float(lspl[9]),
        "iso": float(lspl[10]),
        "dc": float(lspl[11]),
        "full cn": float(lspl[12]),
        "full r": float(lspl[13]),
        "dc cur strike": float(lspl[14]),
        "dc cur dip": float(lspl[15]),
        "dc cur rake": float(lspl[16]),
        "dc cur r": float(lspl[17]),
        "dc prop strike": float(lspl[18]),
        "dc prop dip": float(lspl[19]),
        "dc prop rake": float(lspl[20]),
        "dc prop r": float(lspl[21]),
        "method similarity": float(lspl[22]),
        "variance reduction cur": max([float(lspl[23]), 0]),
        "variance reduction prop": max([float(lspl[24]), 0]),
    }


fig, ax = plt.subplots()
ax.plot(
    [v["dc"] for v in events.values()],
    [v["method similarity"] for v in events.values()],
    ".",
)
ax.set_xlabel("DC percentage of full solution")
ax.set_ylabel("similarity")
fig.savefig("DC_constraint//similarity.png")
fig2, ax2 = plt.subplots()
ax2.plot(
    [v["dc"] for v in events.values()],
    [v["variance reduction cur"] for v in events.values()],
    ".",
)
ax2.plot(
    [v["dc"] for v in events.values()],
    [v["variance reduction prop"] for v in events.values()],
    ".",
)
ax2.set_xlabel("DC percentage of full solution")
ax2.set_ylabel("percentage variance reduction")
ax2.legend(["current", "proposed"])
fig2.savefig("DC_constraint//variance_reduction_comparison.png")
fig3, ax3 = plt.subplots()
bins = np.arange(-101, 101)
ax3.hist(
    [
        v["variance reduction prop"] - v["variance reduction cur"]
        for v in events.values()
    ],
    bins=bins,
)
ax3.set_xlabel("current better    variance reduction difference    proposed better")
y1, y2 = ax3.get_ylim()
ax3.plot([0, 0], [0, y2])
ax3.set_ylim([0, y2])

fig3.savefig("DC_constraint//variance_reduction_difference.png")

fig4, ax4 = plt.subplots()
ax4.plot(
    [v["method similarity"] for v in events.values()],
    [
        v["variance reduction prop"] - v["variance reduction cur"]
        for v in events.values()
    ],
    ".",
)
ax4.set_ylabel("variance reduction difference")
ax4.set_xlabel("similarity")
fig4.savefig("DC_constraint//variance_reduction vs similarity")


var_diff = np.array(
    [
        v["variance reduction prop"] - v["variance reduction cur"]
        for v in events.values()
    ]
)
len(np.argwhere(var_diff > 0))
