import matplotlib

matplotlib.use("Qt5Agg")

import glob
import matplotlib.pyplot as plt
import numpy as np

from sms_moment_tensor.MT_math import sdr_to_mt

csvs = glob.glob("Comparisons//*.csv")

for csv in csvs:
    f = open(csv)
    head1 = f.readline()
    head2 = f.readline()
    lines = f.readlines()
    f.close()
    g = open(csv, "w")
    g.write(head1)
    g.write(head2)
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        strike1, dip1, rake1 = [float(s) for s in lspl[1:4]]
        strike2, dip2, rake2 = [float(s) for s in lspl[5:8]]
        mt1 = sdr_to_mt(strike1, dip1, rake1)
        mt2 = sdr_to_mt(strike2, dip2, rake2)
        similarity = np.tensordot(mt1, mt2)
        angle = 180 * np.arccos(similarity) / np.pi
        g.write(",".join(lspl[:-2]))
        g.write(",%.4f,%.1f\n" % (similarity, angle))
    g.close()
