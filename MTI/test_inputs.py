import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
from obspy import read, UTCDateTime, Trace
from obspy.imaging.mopad_wrapper import beach
from obspy.signal import filter
from obspy.signal.invsim import corn_freq_2_paz
from obspy.core.trace import Stats
from operator import itemgetter
from scipy.fftpack import fft, ifft, fftfreq

from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.interface_scattering import (
    P_stack_transmission,
    SV_stack_transmission,
    SH_stack_transmission,
)
from moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from read_inputs import get_velocity_model, read_stations
from read_chevron_0204_data import read_events, read_waveform, read_picks, read_wells

project = "Chevron"
dr = (
    "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\"
    + "Chevron Synthetics\\2019-04-03T18-28\\"
)
catalog = read_events()
stations = read_stations()
velocity_model = get_velocity_model()
wells = read_wells()
Q = {"P": 70, "S": 50}
explosion = {
    "moment_tensor": np.matrix([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]),
    "stress_drop": 3.0e5,
}
xcorr_time = np.arange(-2.998, 3, 0.002)
ii = 0
paz_10hz = corn_freq_2_paz(10.0, damp=np.sqrt(0.5))
snr_threshold = 1.5
n_stations = 69
bandpass_hf = 50
bandpass_lf = 20


def sqa(x):
    return np.squeeze(np.array(x))


def createTraceStats(network, n1, o1, d1, component):
    # make a stats file for a seed trace
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


big_events = {k: v for (k, v) in catalog.items() if v["magnitude"] > 1}

mid_events = {
    k: v
    for (k, v) in catalog.items()
    if (v["magnitude"] < -0.157 and v["magnitude"] > -0.159)
}

UTCDateTime(picks["t0"]).timestamp
"""
-M0.2  "20161122050510.000000" --> pick a different one, it's in the coda of a larger event
-M0.9  "20161119124645.360000"
-M1.5  "20161122181858.350000"
-M0.3  "20161122045522.020000"
"""
mid_events = {k: v for (k, v) in catalog.items() if (k == "20161122045522.020000")}

mw = []


def sqa(x):
    return np.squeeze(np.array(x))


for event in mid_events:
    st = read_waveform(UTCDateTime(event))
    picks = read_picks(event)
    picks
    time_series = np.arange(st[0].stats.npts) * st[0].stats.delta
    freq = fftfreq(len(time_series) * 2 - 1, time_series[1])
    tsfreq = fftfreq(len(time_series), time_series[1])
    jj = -1
    corr_array = np.zeros([3001, n_stations])
    fig0 = plt.figure()
    fig1 = plt.figure()
    ax0 = fig0.add_axes([0.1, 0.1, 0.8, 0.25])
    an0 = fig0.add_axes([0.1, 0.55, 0.8, 0.35])
    a1 = fig1.add_axes([0.1, 0.45, 0.8, 0.5])
    a2 = fig1.add_axes([0.1, 0.1, 0.8, 0.3])
    # fig = plt.figure()
    # ax1 = fig.add_axes([0.1, 0.4, 0.6, 0.45])
    dat = np.zeros([1501, n_stations])
    syn = np.zeros([1501, n_stations])
    # ax2 = fig.add_axes([0.1, 0.05, 0.6, 0.3])
    # ax3 = fig.add_axes([0.7, 0.7, 0.25, 0.25])
    # ax4 = fig.add_axes([0.7, 0.4, 0.25, 0.25])
    # ax3.set_aspect("equal")
    # ax4.set_aspect("equal")
    amp = -1.0e11 * np.ones(n_stations)
    snr = np.ones(n_stations)
    easting, northing = np.zeros(n_stations), np.zeros(n_stations)
    kk = 0
    rhs, Amat = [], []
    if picks is None:
        print("picks needed for event " + event)
    else:
        p_raypath = [object() for _ in range(n_stations)]
        s_raypath = [object() for _ in range(n_stations)]
        for trace in st:
            if trace.stats.location == "03":
                jj += 1
                id = trace.get_id()
                source = {
                    "x": mid_events[event]["easting"],
                    "y": mid_events[event]["northing"],
                    "z": mid_events[event]["depth"] - 910,
                }
                p_raypath[jj] = isotropic_ray_trace(
                    source, stations[id], velocity_model, "P"
                )
                s_raypath[jj] = isotropic_ray_trace(
                    source, stations[id], velocity_model, "S"
                )

                explosion["moment_magnitude"] = mid_events[event]["magnitude"]
                explosion["x"] = source["x"]
                explosion["y"] = source["y"]
                explosion["z"] = source["z"]
                explosive_response = get_response(
                    p_raypath[jj],
                    s_raypath[jj],
                    explosion,
                    stations[id],
                    Q,
                    time_series,
                    0,
                )
                normalized_response = explosive_response["d"] / np.sqrt(
                    np.correlate(explosive_response["d"], explosive_response["d"])
                )
                expl_channel_object = {
                    "component": "Z",
                    "channel": "CPZ",
                    "station": stations[id],
                    "network": "CV",
                    "location": "04",
                }
                expl_stats = createTraceStats(
                    "CV",
                    trace.stats.npts,
                    trace.stats.starttime,
                    trace.stats.delta,
                    expl_channel_object,
                )
                expl_trace = Trace(data=normalized_response, header=expl_stats)
                # expl_trace.simulate(paz_simulate=paz_10hz)
                xcorr = np.correlate(
                    filter.bandpass(trace.data, bandpass_lf, bandpass_hf, 500),
                    filter.bandpass(expl_trace.data, bandpass_lf, bandpass_hf, 500),
                    mode="full",
                )
                t_residual = (
                    picks[id]["P"]
                    - UTCDateTime(picks["t0"]).timestamp
                    - p_raypath[jj]["traveltime"]
                )
                pick_residual = picks[id]["P"] - UTCDateTime(picks["t0"]).timestamp
                syn[:, jj] = np.real(
                    ifft(
                        fft(
                            filter.bandpass(
                                expl_trace.data, bandpass_lf, bandpass_hf, 500
                            )
                        )
                        * np.exp(
                            2.0j * np.pi * (p_raypath[jj]["traveltime"] - 1) * tsfreq
                        )
                    )
                )
                picks[id]["P"] - UTCDateTime(event).timestamp
                picks[id]["P"] - UTCDateTime(picks["t0"]).timestamp
                dat[:, jj] = np.real(
                    ifft(
                        fft(filter.bandpass(trace.data, bandpass_lf, bandpass_hf, 500))
                        * np.exp(
                            2.0j
                            * np.pi
                            * (picks[id]["P"] - UTCDateTime(event).timestamp - 1)
                            * tsfreq
                        )
                    )
                )
                dat[:, jj] = trace.data
                xshift = np.real(
                    ifft(fft(xcorr) * np.exp(2.0j * np.pi * (0.15 + t_residual) * freq))
                )
                corr_array[:, jj] = xshift / max(abs(xshift))
                amp[jj] = corr_array[
                    np.argsort(abs(corr_array[1499:1520, jj]))[-1] + 1499, jj
                ]

                easting[jj] = stations[id]["x"]
                northing[jj] = stations[id]["y"]
                snr[jj] = abs(amp[jj]) / np.sqrt(np.var(xcorr[1000:1400]))
                if np.isnan(snr[jj]):
                    snr[jj] = 0.0
                if snr[jj] > snr_threshold:
                    kk += 1
                    rhs.append(
                        back_project_amplitude(
                            p_raypath[jj], s_raypath[jj], amp[jj], "P"
                        )
                    )
                    Amat.append(inversion_matrix_row(p_raypath[jj], "P"))
        cdatmax = 4 * np.percentile(abs(dat).flatten(), 67)
        csynmax = np.amax(abs(syn))
        ax0.pcolor(
            range(1, n_stations + 1),
            np.arange(0.0, 3.002, 0.002),
            dat,
            cmap="bwr",
            vmin=-cdatmax,
            vmax=cdatmax,
            zorder=-1,
        )
        dat.shape
        an0.pcolor(
            range(1, n_stations + 1),
            np.arange(0, 3.002, 0.002),
            syn,
            cmap="bwr",
            vmin=-csynmax,
            vmax=csynmax,
            zorder=-1,
        )

        a1.pcolor(
            range(1, n_stations + 1),
            xcorr_time[1299:1700],
            corr_array[1299:1700, :],
            cmap="bwr",
            vmin=-1,
            vmax=1,
            zorder=-1,
        )

        '''
        #        for jj in range(n_stations):
        #            ax0.plot([jj+1,jj+2],[t_residual,t_residual],'k',zorder = 1,lw=0.5)
        cmax = 5 * np.percentile(abs(corr_array).flatten(), 67)


        ax1.pcolor(
            range(1, n_stations + 1),
            xcorr_time[1299:1700],
            corr_array[1299:1700, :],
            cmap="bwr",
            vmin=-cmax,
            vmax=cmax,
            zorder=-1,
        )

        igood = np.where(snr > snr_threshold)[0]
        ibad = np.where(snr <= snr_threshold)[0]
        ax3.scatter(
            easting[igood],
            northing[igood],
            c=amp[igood],
            vmin=-cmax,
            vmax=cmax,
            edgecolor="k",
            cmap="bwr",
        )
        ax3.plot(easting[ibad], northing[ibad], "o", color="0.8", markeredgecolor="k")
        ax3.set_xticklabels("")
        ax3.set_yticklabels("")
        for well in wells:
            ax4.plot(
                wells[well]["easting"],
                wells[well]["northing"],
                color="0.5",
                lw=2,
                zorder=2,
            )
        ax4.plot(
            catalog[event]["easting"],
            catalog[event]["northing"],
            "*",
            color="firebrick",
        )
        ax4.axis("off")
        ax2.plot(range(1, n_stations + 1), 10 * np.log10(snr))
        ax2.plot(
            [1, n_stations],
            [10 * np.log10(snr_threshold), 10 * np.log10(snr_threshold)],
            "firebrick",
        )
        ax2.set_ylim([-10, 50])
        ax2.set_xlim([1, n_stations])
        ax2.text(
            2,
            42,
            ("%i stations above SNR %.1f dB" % (kk, 10 * np.log10(snr_threshold))),
        )

        if kk > 6:
            rhs = np.matrix(rhs)
            Amat = np.matrix(np.squeeze(Amat))
            mt_out, mt_dc, condition_number, rsq_gen, rsq_dc = solve_moment_tensor(
                Amat, rhs
            )
            beachball_out = beach(
                mt_out,
                xy=(1, 3),
                zorder=2,
                width=1,
                mopad_basis="NED",
                linewidth=0.5,
                facecolor="firebrick",
            )
            beachball_dc = beach(
                mt_dc,
                xy=(1, 1),
                zorder=2,
                width=1,
                mopad_basis="NED",
                linewidth=0.5,
                facecolor="firebrick",
            )
            #            outMT = np.matrix([[m11, m12, m13], [m12, m22, m23], [m13, m23, m33]])
            mw.append(catalog[event]["magnitude"])
            ax5 = fig.add_axes([0.75, 0.1, 0.2, 0.4])
            ax5.set_aspect("equal")
            ax5.text(1, 3.6, "gen.", ha="center")
            ax5.text(1, 2.4, ("CN = %.1f" % condition_number), ha="center", va="top")
            #            ax5.text(1, 1.6, "DC", ha="center")
            #            ax5.text(1, 0.4, ("CN = %.1f" % cn_dev[-1]), ha="center", va="top")
            ax5.set_xlim([0, 2])
            ax5.set_ylim([0, 6])
            ax5.add_collection(beachball_out)
            ax5.add_collection(beachball_dc)
            ax5.axis("off")
        if kk > 1:
            fig2 = plt.figure()
            stack_corr = np.zeros(len(xcorr_time) + 2, dtype="complex")
            isnr = -1
            double_corr_array = np.zeros([6001, len(amp)])
            for pol in np.sign(amp):
                isnr += 1
                if snr[isnr] > snr_threshold:
                    stack_corr += (
                        pol * corr_array[:, isnr] / sum(abs(corr_array[:, isnr]))
                    )
            for itr in range(len(amp)):
                double_corr_array[:, itr] = np.correlate(
                    stack_corr, corr_array[:, itr], "full"
                )
            ax6 = fig2.add_axes([0.1, 0.4, 0.6, 0.45])
            dcmax = 5 * np.percentile(abs(double_corr_array).flatten(), 67)

            double_corr_amp = np.zeros(len(amp))
            double_corr_snr = np.zeros(len(amp))
            for jj in range(len(amp)):
                double_corr_amp[jj] = double_corr_array[3001, jj]
                double_corr_snr[jj] = abs(double_corr_array[3001, jj]) / np.sqrt(
                    np.var(double_corr_array[2000:2800, jj])
                )
            double_corr_snr = np.array(double_corr_snr)
            i_dbl_good = np.where(double_corr_snr > 0)[0]
            i_dbl_bad = np.where(double_corr_snr <= snr_threshold)[0]
            normalized_double_corr = double_corr_array / np.tile(
                np.sum(abs(double_corr_array), axis=0), [6001, 1]
            )
            ax6.pcolor(
                range(1, n_stations + 1),
                xcorr_time[1299:1700],
                normalized_double_corr[2799:3200, :],
                cmap="bwr",
                vmin=-0.003,
                vmax=0.003,
                zorder=-1,
            )
            np.tile(np.sum(abs(double_corr_array), axis=0), [6001, 1])
            ax7 = fig2.add_axes([0.1, 0.05, 0.6, 0.3])
            ax7.plot(range(1, n_stations + 1), 10 * np.log10(double_corr_snr))
            ax7.plot(
                [1, n_stations],
                [10 * np.log10(snr_threshold), 10 * np.log10(snr_threshold)],
                "firebrick",
            )
            ax7.set_ylim([-10, 50])
            ax7.set_xlim([1, n_stations])
            """
            ax7.text(
                2,
                42,
                ("%i stations above SNR %.1f dB" % (kk, 10 * np.log10(snr_threshold))),
            )
            """
            ax8 = fig2.add_axes([0.7, 0.7, 0.25, 0.25])
            ax8.set_aspect("equal")
            ax9 = fig2.add_axes([0.72, 0.5, 0.23, 0.15])
            ax8.scatter(
                easting[i_dbl_good],
                northing[i_dbl_good],
                c=double_corr_amp[i_dbl_good],
                vmin=-dcmax,
                vmax=dcmax,
                edgecolor="k",
                cmap="bwr",
            )
            """
            ax8.plot(
                easting[i_dbl_bad],
                northing[i_dbl_bad],
                "o",
                color="0.8",
                markeredgecolor="k",
            )
            """
            ax8.set_xticklabels("")
            ax8.set_yticklabels("")
            ax9.plot(xcorr_time[1299:1800], stack_corr[1299:1800])
            ax9.set_yticklabels("")
            ax9.set_xlabel("lag time (s)")

        igood = range(69)
        n_good = len(igood)

        if n_good > 6:
            rhs2, Amat2 = [], []
            for itr in range(n_good):
                rhs2.append(
                    back_project_amplitude(
                        p_raypath[igood[itr]],
                        s_raypath[igood[itr]],
                        double_corr_amp[igood[itr]],
                        "P",
                    )
                )
                Amat2.append(inversion_matrix_row(p_raypath[igood[itr]], "P"))
            rhs2 = np.matrix(rhs2)
            Amat2 = np.matrix(np.squeeze(Amat2))
            mt2_out, mt2_dc, condition_number2, rsq_gen2, rsq_dc2 = solve_moment_tensor(
                Amat2, rhs2
            )
            beachball_out2 = beach(
                mt2_out,
                xy=(1, 3),
                zorder=2,
                width=1,
                mopad_basis="NED",
                linewidth=0.5,
                facecolor="firebrick",
            )
            beachball_dc2 = beach(
                mt2_dc,
                xy=(1, 1),
                zorder=2,
                width=1,
                mopad_basis="NED",
                linewidth=0.5,
                facecolor="firebrick",
            )
            ax10 = fig2.add_axes([0.75, 0.1, 0.2, 0.4])
            ax10.set_aspect("equal")
            ax10.text(1, 3.6, "gen.", ha="center")
            ax10.text(1, 2.4, ("CN = %.1f" % condition_number2), ha="center", va="top")
            ax10.set_xlim([0, 2])
            ax10.set_ylim([0, 6])
            ax10.add_collection(beachball_out2)
            ax10.add_collection(beachball_dc2)
            ax10.axis("off")
            '''
plt.show()
