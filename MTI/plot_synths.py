import glob
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
from obspy import read


bwr = cm.get_cmap("bwr")
PI = np.pi
D2R = PI / 180
n_stations = 12
source_depth = 10

dir = "C:\\Users\\adambaig\\Project\\MTI\\axitra synthetics\\simple_MT_test\\"
sac_files_z = glob.glob(dir + "axi0??.Z.sac")
sac_files_n = glob.glob(dir + "axi0??.X.sac")
sac_files_e = glob.glob(dir + "axi0??.Y.sac")
delta = 6 / 2048
i_p = int((10 * np.sqrt(2) / 5 + 0.01) // delta)
i_s = int((10 * np.sqrt(6) / 5 + 0.01) // delta)


p_amp_z = np.zeros(n_stations)
p_amp_e = np.zeros(n_stations)
p_amp_n = np.zeros(n_stations)
s_amp_z = np.zeros(n_stations)
s_amp_e = np.zeros(n_stations)
s_amp_n = np.zeros(n_stations)
i_station = 0
for i_station in range(n_stations):
    stz = read(sac_files_z[i_station])[0]
    ste = read(sac_files_e[i_station])[0]
    stn = read(sac_files_n[i_station])[0]
    p_amp_z[i_station] = stz.data[i_p]
    p_amp_e[i_station] = ste.data[i_p]
    p_amp_n[i_station] = stn.data[i_p]
    s_amp_z[i_station] = stz.data[i_s]
    s_amp_e[i_station] = ste.data[i_s]
    s_amp_n[i_station] = stn.data[i_s]

max_amp = max(abs(p_amp_z))

station_n = source_depth * np.cos(np.arange(12) * 30 * D2R)
station_e = source_depth * np.sin(np.arange(12) * 30 * D2R)
fig, ax = plt.subplots()
ax.set_aspect("equal")
ax.scatter(
    station_e,
    station_n,
    c=((s_amp_z - max_amp) / 2 / max_amp + 1),
    marker="o",
    edgecolor="k",
    cmap=bwr,
)

ax.quiver(
    station_e, station_n, 2 * s_amp_e / max_amp, 2 * s_amp_n / max_amp, pivot="mid"
)


# st = read(sac_files_z[1])[0]
# plt.plot(np.linspace(0,6,2048),st.data)
#
