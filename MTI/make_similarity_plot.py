import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import glob
from obspy.imaging.mopad_wrapper import beach

files = glob.glob("MT_output_location0[0-9].csv")
nfiles = len(files)

f = ["" for _ in range(nfiles)]
location = ["" for _ in range(nfiles)]
lines = [[] for _ in range(nfiles)]

for ifile, MT_file in enumerate(files):
    f[ifile] = open(MT_file)
    head = f[ifile].readline()
    location[ifile] = MT_file.split("location")[1].split(".csv")[0]
    lines[ifile] = f[ifile].readlines()


def normalize(matrix):
    return matrix / np.linalg.norm(matrix)


events = {}
gn_similarity, dc_similarity, mag, min_rsq_gn, min_rsq_dc, ev_comp = (
    [],
    [],
    [],
    [],
    [],
    [],
)
for jj in range(len(lines[0])):
    event = lines[0][jj].split(",")[0]
    events[event] = {}
    n_good = 0
    for ii, loc in enumerate(location[:3]):
        lspl = lines[ii][jj].split(",")
        gen = [float(_) for _ in lspl[3:9]]
        dc = [float(_) for _ in lspl[9:15]]
        if int(lspl[2]) > 0:
            n_good += 1
            events[event]["MT_GN_" + loc] = normalize(
                np.matrix(
                    [
                        [gen[0], gen[1], gen[3]],
                        [gen[1], gen[2], gen[4]],
                        [gen[3], gen[4], gen[5]],
                    ]
                )
            )
            events[event]["MT_DC_" + loc] = normalize(
                np.matrix(
                    [
                        [dc[0], dc[1], dc[3]],
                        [dc[1], dc[2], dc[4]],
                        [dc[3], dc[4], dc[5]],
                    ]
                )
            )
            events[event]["cn_GN_" + loc] = float(lspl[-4])
            events[event]["cn_DV_" + loc] = float(lspl[-3])
            events[event]["r2_GN_" + loc] = float(lspl[-2])
            events[event]["r2_DC_" + loc] = float(lspl[-1])
    if n_good > 0:
        gn_similarity.append(
            np.linalg.norm(
                sum([v for k, v in events[event].items() if "MT_GN" in k]) / n_good
            )
        )
        min_rsq_gn.append(min([v for k, v in events[event].items() if "r2_GN" in k]))
        dc_similarity.append(
            np.linalg.norm(
                sum([v for k, v in events[event].items() if "MT_DC" in k]) / n_good
            )
        )
        min_rsq_dc.append(min([v for k, v in events[event].items() if "r2_DC" in k]))
        mag.append(float(lspl[1]))
        ev_comp.append(event)

event = "20161122180529.800000"

dc_similarity
fig, ax = plt.subplots(1)
ax.set_aspect("equal")
mt_gen_01 = [
    events[event]["MT_DC_01"][0, 0],
    events[event]["MT_DC_01"][1, 1],
    events[event]["MT_DC_01"][2, 2],
    events[event]["MT_DC_01"][0, 1],
    events[event]["MT_DC_01"][0, 2],
    events[event]["MT_DC_01"][1, 2],
]
mt_gen_02 = [
    events[event]["MT_DC_02"][0, 0],
    events[event]["MT_DC_02"][1, 1],
    events[event]["MT_DC_02"][2, 2],
    events[event]["MT_DC_02"][0, 1],
    events[event]["MT_DC_02"][0, 2],
    events[event]["MT_DC_02"][1, 2],
]
mt_gen_03 = [
    events[event]["MT_DC_03"][0, 0],
    events[event]["MT_DC_03"][1, 1],
    events[event]["MT_DC_03"][2, 2],
    events[event]["MT_DC_03"][0, 1],
    events[event]["MT_DC_03"][0, 2],
    events[event]["MT_DC_03"][1, 2],
]
beach1 = beach(mt_gen_01, xy=(1, 1), width=1, mopad_basis="NED")
beach2 = beach(mt_gen_02, xy=(3, 1), width=1, mopad_basis="NED")
beach3 = beach(mt_gen_03, xy=(5, 1), width=1, mopad_basis="NED")
ax.add_collection(beach1)
ax.add_collection(beach2)
ax.add_collection(beach3)
ax.set_xlim([0, 6])
ax.set_ylim([0, 2])
ax.set_axis_off()
plt.show()

zip(dc_similarity, ev_comp)
dc_similarity[-33]

ev_comp[-33]
