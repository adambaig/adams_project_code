import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import glob
from read_chevron_0204_data import read_events, read_wells
import random
from moment_tensor_inversion import decompose_MT

files = glob.glob("M.gt.minus1//MT_output_xcorr_location0[0-9].csv")
nfiles = len(files)

wells = read_wells()
events = read_events()
d2r = np.pi / 180.0
rsq_cut = 0.4
n_picks_cut = 7
f = ["" for _ in range(nfiles)]
location = ["" for _ in range(nfiles)]
lines = [[] for _ in range(nfiles)]

for ifile, MT_file in enumerate(files):
    f[ifile] = open(MT_file)
    head = f[ifile].readline()
    location[ifile] = MT_file.split("location")[1].split(".csv")[0]
    lines[ifile] = f[ifile].readlines()


def normalize(matrix):
    return matrix / np.linalg.norm(matrix)


for jj in range(len(lines[0])):
    event = lines[0][jj].split(",")[0]
    number_of_resolved_events = 0
    for ii, loc in enumerate(location):

        lspl = lines[ii][jj].split(",")
        if int(lspl[2]) > 0:
            number_of_resolved_events += 1
            gen = [float(_) for _ in lspl[3:9]]
            dc = [float(_) for _ in lspl[9:15]]
            events[event]["MT_GN_" + loc] = normalize(
                np.matrix(
                    [
                        [gen[0], gen[3], gen[4]],
                        [gen[3], gen[1], gen[5]],
                        [gen[4], gen[5], gen[2]],
                    ]
                )
            )
            events[event]["MT_DC_" + loc] = normalize(
                np.matrix(
                    [
                        [dc[0], dc[3], dc[4]],
                        [dc[3], dc[1], dc[5]],
                        [dc[4], dc[5], dc[2]],
                    ]
                )
            )
            events[event]["cn_GN"] = float(lspl[-4])
            events[event]["cn_DV"] = float(lspl[-3])
            events[event]["r2_GN"] = float(lspl[-2])
            events[event]["r2_DC"] = float(lspl[-1])
            events[event]["n_picks"] = float(lspl[2])
    if number_of_resolved_events == 3:
        events[event]["gn_similarity"] = np.linalg.norm(
            sum([v for k, v in events[event].items() if "MT_GN" in k]) / 3
        )
        events[event]["dc_similarity"] = np.linalg.norm(
            sum([v for k, v in events[event].items() if "MT_DC" in k]) / 3
        )

fig, ax = plt.subplots(1, 2, figsize=[15, 10])
ax[0].set_aspect("equal")
ax[1].set_aspect("equal")
common_events = {k: v for k, v in events.items() if "gn_similarity" in v}
easting = np.array([v["easting"] for k, v in common_events.items()])
northing = np.array([v["northing"] for k, v in common_events.items()])
gn_similarity = np.array([v["gn_similarity"] for k, v in common_events.items()])
dc_similarity = np.array([v["dc_similarity"] for k, v in common_events.items()])
east0 = np.average(easting)
north0 = np.average(northing)

i_good = np.where(
    (np.array([v["r2_DC"] for k, v in common_events.items()]) > rsq_cut)
    & (np.array([v["n_picks"] for k, v in common_events.items()]) > n_picks_cut)
    & (np.array([v["cluster"] for k, v in common_events.items()]) > -1)
)[0]

scatter = ax[0].scatter(
    easting[i_good] - east0,
    northing[i_good] - north0,
    c=gn_similarity[i_good],
    vmin=0,
    vmax=1,
    edgecolor="0.2",
)
scatter = ax[1].scatter(
    easting[i_good] - east0,
    northing[i_good] - north0,
    c=dc_similarity[i_good],
    vmin=0,
    vmax=1,
    edgecolor="0.2",
)

x1, x2 = ax[0].get_xlim()
y1, y2 = ax[0].get_ylim()

for well in wells:
    ax[0].plot(
        wells[well]["easting"] - east0,
        wells[well]["northing"] - north0,
        "0.4",
        lw=4,
        zorder=-12,
    )
    ax[0].plot(
        wells[well]["easting"] - east0,
        wells[well]["northing"] - north0,
        "0.75",
        lw=2,
        zorder=-11,
    )
    ax[1].plot(
        wells[well]["easting"] - east0,
        wells[well]["northing"] - north0,
        "0.4",
        lw=4,
        zorder=-12,
    )
    ax[1].plot(
        wells[well]["easting"] - east0,
        wells[well]["northing"] - north0,
        "0.75",
        lw=2,
        zorder=-11,
    )

ax[0].set_xlim([x1, x2])
ax[0].set_ylim([y1, y2])
ax[1].set_xlim([x1, x2])
ax[1].set_ylim([y1, y2])
ax[0].set_xlabel("Relative Easting (m)")
ax[0].set_ylabel("Relative Northing (m)")
ax[1].set_xlabel("Relative Easting (m)")
ax[1].set_ylabel("Relative Northing (m)")
# cb = fig.colorbar(scatter)
# cb.set_label("similarity")
plt.show()
