import pickle

import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory
from obspy.imaging.mopad_wrapper import beach
import pyproj as pr
from sms_moment_tensor.MT_math import mt_vector_to_matrix, sdr_to_mt
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)

from sms_moment_tensor.plotting import (
    plot_regression,
    individual_bb_plot,
    plot_beachball_from_sdr,
)
from sms_ray_modelling.raytrace import isotropic_ray_trace

f = open("DC_constraint//comparison_WTX.csv")
head = f.readline()
lines = f.readlines()
f.close()

event_id = "271464"
pickle_file = f"DC_constraint//MTI pickles//{event_id}.pickle"
f = open(pickle_file, "rb")
event_pickle = pickle.load(f)
f.close()

origin = event_pickle["eveDict"]["event"]["origin"]
velocity_model = event_pickle["velDict"]
velocity_model[0].pop("top")
amplitudes = event_pickle["ampDict"]

inst_xml_file = "Station_xmls//WTX_Full.xml"
inv = read_inventory(inst_xml_file)
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init="epsg:3081")
origin = event_pickle["eveDict"]["event"]["origin"]
velocity_model = event_pickle["velDict"]
velocity_model[0].pop("top")
amplitudes = event_pickle["ampDict"]

referenceFreq = np.exp(
    np.log(event_pickle["bpFreqs"][0]) + np.log(event_pickle["bpFreqs"][1])
)
# try:
#     long_o, lat_o, elevation_o = (
#         origin["momentTensors"]["longitude"],
#         origin["momentTensors"]["latitude"],
#         -1000 * origin["momentTensors"]["depth"],
#     )
# except:
#     long_o, lat_o, elevation_o = (
#         origin["momentTensors"][0]["longitude"],
#         origin["momentTensors"][0]["latitude"],
#         -1000 * origin["momentTensors"][0]["depth"],
#     )


long_o, lat_o, elevation_o = (
    origin["longitude"],
    origin["latitude"],
    -1000 * origin["depth"],
)

east_o, north_o = pr.transform(latlon_proj, out_proj, long_o, lat_o)
source = {"e": east_o, "n": north_o, "z": elevation_o}
back_proj_amp, forward_mti_amp, forward_fps_amp, row = {}, {}, {}, {}
for station_phase, amp_vals in amplitudes.items():
    network, station, location, channel = station_phase.split(".")
    station_inv = inv.select(network=network, station=station)[0][0]
    east, north = pr.transform(
        latlon_proj, out_proj, station_inv.longitude, station_inv.latitude
    )
    elevation = station_inv.elevation
    receiver = {"e": east, "n": north, "z": elevation}
    p_raypath = isotropic_ray_trace(source, receiver, velocity_model, "P")
    s_raypath = isotropic_ray_trace(source, receiver, velocity_model, "S")
    back_proj_amp[station_phase] = back_project_amplitude(
        p_raypath,
        s_raypath,
        amp_vals["amps"][0],
        channel[-1],
        frequency=referenceFreq,
    )
    row[station_phase] = inversion_matrix_row(
        p_raypath if channel[-1] == "P" else s_raypath, channel[-1]
    )


mt = solve_moment_tensor(list(row.values()), list(back_proj_amp.values()))
mt
