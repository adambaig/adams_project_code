import matplotlib

matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
import numpy as np
import mplstereonet as mpls
from ExternalPrograms.FocMec.FocMecSupport import runFocMec
from sms_moment_tensor.plotting import (
    plot_regression,
    plot_beachballs,
    plot_beachball_from_sdr,
    plot_fault_planes_from_Focmec,
)
from sms_moment_tensor.MT_math import conjugate_sdr
from mplstereonet.stereonet_math import line as mpls_line

focmec_input_atlas = r"C:\Users\adambaig\Project\MTI\Focmec comparison\30661_at.dat"
focmec_input_ll = r"C:\Users\adambaig\Project\MTI\Focmec comparison\19702_ll.inp"

sdrs_atlas, focmec_outpath_atlas = runFocMec(focmec_input_atlas)
sdrs_ll, focmec_outpath_ll = runFocMec(focmec_input_ll)


fig, [ax_atlas, ax_ll] = mpls.subplots(1, 2)

plot_fault_planes_from_Focmec(focmec_input_atlas, focmec_outpath_atlas, ax_atlas, fig)
plot_fault_planes_from_Focmec(focmec_input_ll, focmec_outpath_ll, ax_ll, fig)

plt.show()
