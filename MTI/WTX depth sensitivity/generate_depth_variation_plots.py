import glob
import logging
import os
import pickle

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from nmxseis.model.nslc import NSLCX
from nmxseis.util.obspy_tools import fast_remove_response
from obspy import read, read_inventory, UTCDateTime, Stream
from obspy.imaging.mopad_wrapper import beach
import pyproj as pr

from General.Projections import reproject, calcAziViaLonLat
from NocMeta.Meta import NOC_META
from sms_moment_tensor.MT_math import mt_matrix_to_vector, sdr_to_mt
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from sms_moment_tensor.plotting import individual_bb_plot

# from sms_moment_tensor.plotting import plot_regression, individual_bb_plot, plot_beachball_from_sdr
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.rotation import rotate_to_raypath
from SupportModules.StreamSupport import (
    getConsistentStream,
    removeInstResp,
    selectStreamByCode,
)

BRG = cm.get_cmap("brg")
sourceTag = "WTX"
epsg = NOC_META[sourceTag]["epsg"]
# rotation_mode = "none"
rotation_mode = "zrt"

seed_dir = r"Rotation_PVH_Test\seeds"
pickle_dir = r"Rotation_PVH_Test\event_pickles"
pickle_files = glob.glob(os.path.join(pickle_dir, "*.pickle"))
seed_files = glob.glob(os.path.join(seed_dir, "*.seed"))

inst_xml_file = "Station_xmls//WTX_Full.xml"
inv = read_inventory(inst_xml_file)
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init="epsg:3081")

file = seed_files[0]
seed_lookup = {}
for file in seed_files:
    start_stamp, end_stamp = os.path.basename(file).split(".seed")[0].split("_")
    seed_lookup[file] = {
        "start": UTCDateTime(start_stamp.replace(".", "T", 1)),
        "end": UTCDateTime(end_stamp.replace(".", "T", 1)),
    }


def find_seed(time):
    for seed, time_range in seed_lookup.items():
        if time > time_range["start"] and time < time_range["end"]:
            return seed
    return None


station_locations = {}
for network in inv:
    for station in network:
        for location in station:
            station_locations[f"{network.code}.{station.code}."] = {
                "longitude": station.longitude,
                "latitude": station.latitude,
                "elevation": station.elevation,
            }

for station in station_locations.values():
    station["e"], station["n"] = pr.transform(
        latlon_proj, out_proj, station["longitude"], station["latitude"]
    )
    station["z"] = station["elevation"]

comp_color = {"P": "firebrick", "V": "darkgoldenrod", "H": "royalblue"}

fig, ax = plt.subplots(11, 2, figsize=[10, 55])
comparison = {}
delta_elevations = np.linspace(2500, -2500, 11)
for pickle_file in pickle_files:
    with open(pickle_file, "rb") as f:
        event_pickle = pickle.load(f)
    origin = event_pickle["eveDict"]["event"]["origin"]
    long_o, lat_o, elevation_o = (
        origin["longitude"],
        origin["latitude"],
        -1000 * origin["depth"],
    )
    event_id = os.path.basename(pickle_file).split(".")[0]
    east, north = pr.transform(latlon_proj, out_proj, long_o, lat_o)
    referenceFreq = np.exp(
        np.log(event_pickle["bpFreqs"][0]) + np.log(event_pickle["bpFreqs"][1])
    )
    velocity_model = event_pickle["velDict"]
    amplitudes = event_pickle["ampDict"]
    bpFreqs = event_pickle["bpFreqs"]

    stream = read(find_seed(UTCDateTime(origin["time"])))


    removeInstResp(stream, inv)
    stream, grpIdxDict = getConsistentStream(stream, minCha=3)
    moment_tensors = []
    for i_elevation, delta_elevation in enumerate(delta_elevations):
        stream_copy = stream.copy()
        eveLoc = {"e": east, "n": north, "z": elevation_o + delta_elevation}
        pltSt = Stream()
        p_raypaths = {}
        s_raypaths = {}
        for nsl, grpIdxList in grpIdxDict.items():
            ns = ".".join(nsl.split(".")[:2]) + "."
            for grpIdxs in grpIdxList:
                # Do not bother if had other than three channels
                if len(grpIdxs) != 3:
                    continue
                grpSt = Stream(traces=[stream_copy[i] for i in grpIdxs])
                try:
                    grpSt.rotate("->ZNE", inventory=inv)
                except:
                    print("Failed to rotate traces from %s to ZNE" % (nsl))
                    continue

                waveforms = {
                    comp.lower(): grpSt.select(component=comp)[0].data
                    for comp in ["E", "N", "Z"]
                }
                p_raypaths[nsl] = isotropic_ray_trace(
                    eveLoc, station_locations[ns], velocity_model, "P"
                )
                s_raypaths[nsl] = isotropic_ray_trace(
                    eveLoc, station_locations[ns], velocity_model, "S"
                )
                rotatedWaveforms_pvh = rotate_to_raypath(
                    waveforms, p_raypaths[nsl], mode="pvh"
                )

                for i, (comp, data) in enumerate(rotatedWaveforms_pvh.items()):
                    grpSt[i].stats.channel = "RT" + comp.upper()
                    grpSt[i].data = data

                pltSt += grpSt
        inversion_matrix, right_hand_side = [], []
        for iNslc, (nslc, pick) in enumerate(event_pickle["ampDict"].items()):
            network, station, location, channel = nslc.split(".")
            trace_stream = pltSt.select(
                station=station, network=network, channel=channel
            )
            nsl = ".".join([network, station, location])
            if len(trace_stream) == 0:
                continue
            else:
                trace = trace_stream[0]
            pick_time = UTCDateTime(pick["times"][0])
            time_axis = [UTCDateTime(trace.stats.starttime) + t for t in trace.times()]
            i_pick = np.argmin(abs(np.array([t - pick_time for t in time_axis])))
            if channel[-1] == "P":
                inversion_matrix.append(inversion_matrix_row(p_raypaths[nsl], "P"))
            elif channel[-1] == "V" or channel[-1] == "H":
                inversion_matrix.append(
                    inversion_matrix_row(s_raypaths[nsl], channel[-1])
                )
            else:
                continue
            right_hand_side.append(
                back_project_amplitude(
                    p_raypaths[nsl],
                    s_raypaths[nsl],
                    trace[i_pick],
                    channel[-1],
                    frequency=referenceFreq,
                )
            )
        if len(inversion_matrix) < 6:
            continue
        moment_tensor = solve_moment_tensor(inversion_matrix, right_hand_side)
        moment_tensors.append(moment_tensor)
        dc_vec = mt_matrix_to_vector(moment_tensor["dc"])
        gn_vec = mt_matrix_to_vector(moment_tensor["general"])
        individual_bb_plot(
            dc_vec / np.linalg.norm(dc_vec),
            moment_tensor["r_dc"],
            moment_tensor["condition_number_dev"],
            moment_tensor["number_observations"],
            f"elevation = {elevation_o + delta_elevation} m",
            ax[i_elevation, 0],
            color=BRG(moment_tensor["r_dc"]),
        )
        individual_bb_plot(
            gn_vec / np.linalg.norm(gn_vec),
            moment_tensor["r_gen"],
            moment_tensor["condition_number_gen"],
            moment_tensor["number_observations"],
            "",
            ax[i_elevation, 1],
            color=BRG(moment_tensor["r_gen"]),
        )
        del stream_copy

    fig.savefig(f"WTX depth sensitivity\images\depth_sensitivity_{event_id}.png")

# ax.set_xlabel("original amplitude (m/s)")
# ax.set_ylabel("recomputed amplitude (m/s)")
# x1, x2 = ax.get_xlim()
# y1, y2 = ax.get_ylim()
# max_val = max([-x1, x2, -y1, y2])
# ax.plot([-max_val, max_val], [-max_val, max_val], "0.2", zorder=-2)
# ax.plot([0, 0], [-max_val, max_val], "0.2", zorder=-2)
# ax.plot([-max_val, max_val], [0, 0], "0.2", zorder=-2)
# ax.set_xlim(-max_val, max_val)
# ax.set_ylim(-max_val, max_val)
# ax.set_title(f"rotations is {rotation_mode}")
#
