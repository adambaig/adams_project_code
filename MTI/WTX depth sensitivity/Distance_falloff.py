import matplotlib

matplotlib.use("Qt5agg")

from functools import reduce
import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory
from obspy.imaging.mopad_wrapper import beach
import pickle
from pyproj import Transformer

from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from sms_moment_tensor.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import mt_matrix_to_vector
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_moment_tensor.plotting import (
    plot_regression,
    plot_beachballs,
    plot_beachball_from_sdr,
    plot_fault_planes_from_Focmec,
)

from read_inputs import read_velocity_model


def multiply(x1, x2):
    return x1 * x2


transformer = Transformer.from_crs("epsg:4326", "epsg:3005")

test_pickle = "Pickles//132154.pickle"
velocity_model = read_velocity_model("wtx_simple.txt")
f = open(test_pickle, "rb")
pickle_out = pickle.load(f)
f.close()

# inv = formStaXml(athenaCode="WTX", outDir=".")

inv = read_inventory("WTX_Full.xml")
stations = {}
for network in inv:
    net_code = network.code
    for station in network:
        net_stat = net_code + "." + station.code
        stations[net_stat] = {
            "latitude": station.latitude,
            "longitude": station.longitude,
            "elevation": station.elevation,
        }
        easting, northing = transformer.transform(station.latitude, station.longitude)
        stations[net_stat]["e"] = easting
        stations[net_stat]["n"] = northing
        stations[net_stat]["z"] = station.elevation
pickle_out["eveDict"]
from obspy import UTCDateTime

UTCDateTime(1570523989.2218099)

mean_frequency = np.sqrt(reduce(multiply, pickle_out["bpFreqs"]))
event_latitude = pickle_out["eveDict"]["event"]["origin"]["latitude"]
event_longitude = pickle_out["eveDict"]["event"]["origin"]["longitude"]
event_easting, event_northing = transformer.transform(event_latitude, event_longitude)
event_elevation = -1000.0 * pickle_out["eveDict"]["event"]["origin"]["depth"]
event = {"e": event_easting, "n": event_northing, "z": event_elevation}

fig, ax = plt.subplots()


def distance(source, receiver):
    return np.sqrt(
        (source["e"] - receiver["e"]) ** 2
        + (source["n"] - receiver["n"]) ** 2
        + (source["z"] - receiver["z"]) ** 2
    )


A_matrix = []
right_hand_side = []
color = {"P": "firebrick", "V": "forestgreen", "H": "royalblue"}
NSLCs = []
for nslc, amp_data in pickle_out["ampDict"].items():
    net_stat = ".".join(nslc.split(".")[:2])
    phase = nslc[-1]
    s_raypath = isotropic_ray_trace(event, stations[net_stat], velocity_model, "S")
    p_raypath = isotropic_ray_trace(event, stations[net_stat], velocity_model, "P")
    phase = amp_data["nslc"][-1]
    hypocentral_distance = distance(event, stations[net_stat])
    # if hypocentral_distance < 80000 and nslc not in [
    #     "NX.WTX12..RTP",
    #     "NX.WTX14..RTP",
    # ]:
    if hypocentral_distance < 10000000:
        ax.semilogy(
            hypocentral_distance / 1000,
            abs(amp_data["amps"][0]),
            "o",
            c=color[phase],
        )
        if phase == "P":
            A_matrix.append(inversion_matrix_row(p_raypath, "P"))
        else:
            A_matrix.append(inversion_matrix_row(s_raypath, phase))
        right_hand_side.append(
            back_project_amplitude(
                p_raypath,
                s_raypath,
                amp_data["amps"][0],
                phase,
                frequency=mean_frequency,
            )
        )
        NSLCs.append(nslc)

mt_object = solve_moment_tensor(A_matrix, right_hand_side)
ax.set_xlabel("epicentral distance (km)")
ax.set_ylabel("absolute amplitude (m/s)")

fig = plt.figure()

axDCRegression = fig.add_subplot(2, 2, 1)
axDc = fig.add_subplot(2, 2, 3)
axGeneralRegression = fig.add_subplot(2, 2, 2)
axGen = fig.add_subplot(2, 2, 4)

fig.tight_layout()
plot_regression(
    right_hand_side,
    mt_object["dc"],
    A_matrix,
    axDCRegression,
    fig=fig,
    nslc_tags=NSLCs,
)
plot_regression(
    right_hand_side,
    mt_object["general"],
    A_matrix,
    axGeneralRegression,
    fig=fig,
    nslc_tags=NSLCs,
)
plot_beachballs(mt_object, axDc, axGen)
