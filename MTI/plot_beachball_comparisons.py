import matplotlib.pyplot as plt
import numpy as np
from sms_moment_tensor.MT_math import mt_vector_to_matrix


f = open("DC_constraint//comparison_WTX.csv")
head = f.readline()
lines = f.readlines()
f.close()

events = {}
for line in lines:
    lspl = line.split(",")
    event_id = lspl[0]
    events[event_id] = {
        "datetime": lspl[1],
        "Ml": float(lspl[2]),
        "full MT": mt_vector_to_matrix([float(m) for m in lspl[3:9]]),
        "clvd": float(lspl[9]),
        "iso": float(lspl[10]),
        "dc": float(lspl[11]),
        "full cn": float(lspl[12]),
        "full r": float(lspl[13]),
        "dc cur strike": float(lspl[14]),
        "dc cur dip": float(lspl[15]),
        "dc cur rake": float(lspl[16]),
        "dc cur r": float(lspl[17]),
        "dc prop strike": float(lspl[18]),
        "dc prop dip": float(lspl[19]),
        "dc prop rake": float(lspl[20]),
        "dc prop r": float(lspl[21]),
        "method similarity": float(lspl[22]),
        "variance reduction cur": float(lspl[23]),
        "variance reduction prop": float(lspl[24]),
    }
