import pickle

import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory, UTCDateTime
from obspy.imaging.mopad_wrapper import beach
import pyproj as pr
from sms_moment_tensor.MT_math import mt_vector_to_matrix, sdr_to_mt
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
)
from sms_moment_tensor.plotting import (
    plot_regression,
    individual_bb_plot,
    plot_beachball_from_sdr,
)
from sms_ray_modelling.raytrace import isotropic_ray_trace

f = open("DC_constraint//comparison_WTX.csv")
head = f.readline()
lines = f.readlines()
f.close()

events = {}
for line in lines:
    lspl = line.split(",")
    event_id = lspl[0]
    events[event_id] = {
        "datetime": lspl[1],
        "Ml": float(lspl[2]),
        "full MT": mt_vector_to_matrix([float(m) for m in lspl[3:9]]),
        "clvd": float(lspl[9]),
        "iso": float(lspl[10]),
        "dc": float(lspl[11]),
        "full cn": float(lspl[12]),
        "full r": float(lspl[13]),
        "dc cur strike": float(lspl[14]),
        "dc cur dip": float(lspl[15]),
        "dc cur rake": float(lspl[16]),
        "dc cur r": float(lspl[17]),
        "dc prop strike": float(lspl[18]),
        "dc prop dip": float(lspl[19]),
        "dc prop rake": float(lspl[20]),
        "dc prop r": float(lspl[21]),
        "method similarity": float(lspl[22]),
        "variance reduction cur": float(lspl[23]),
        "variance reduction prop": float(lspl[24]),
        "dc norm cur": float(lspl[25]),
        "dc norm prop": float(lspl[26]),
    }

low_similarity_events = {
    k: v
    for k, v in events.items()
    if v["method similarity"] < 0.8 and v["method similarity"] > 0.6
}

list(low_similarity_events.items())[:1]

test_events = {k: v for k, v in events.items() if k == "438455"}

inst_xml_file = "Station_xmls//WTX_Full.xml"
inv = read_inventory(inst_xml_file)
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init="epsg:3081")

for event_id, event in list(low_similarity_events.items()):
    pickle_file = f"DC_constraint//MTI pickles//{event_id}.pickle"
    f = open(pickle_file, "rb")
    event_pickle = pickle.load(f)
    f.close()
    origin = event_pickle["eveDict"]["event"]["origin"]
    velocity_model = event_pickle["velDict"]
    velocity_model[0].pop("top")
    amplitudes = event_pickle["ampDict"]
    UTCDateTime(origin["time"])
    referenceFreq = np.exp(
        np.log(event_pickle["bpFreqs"][0]) + np.log(event_pickle["bpFreqs"][1])
    )
    # try:
    #     long_o, lat_o, elevation_o = (
    #         origin["momentTensors"]["longitude"],
    #         origin["momentTensors"]["latitude"],
    #         -1000 * origin["momentTensors"]["depth"],
    #     )
    # except:
    #     long_o, lat_o, elevation_o = (
    #         origin["momentTensors"][0]["longitude"],
    #         origin["momentTensors"][0]["latitude"],
    #         -1000 * origin["momentTensors"][0]["depth"],
    #     )
    long_o, lat_o, elevation_o = (
        origin["longitude"],
        origin["latitude"],
        -1000 * origin["depth"],
    )

    east_o, north_o = pr.transform(latlon_proj, out_proj, long_o, lat_o)
    source = {"e": east_o, "n": north_o, "z": elevation_o}
    back_proj_amp, forward_mti_amp, forward_fps_amp, row = {}, {}, {}, {}

    for station_phase, amp_vals in amplitudes.items():
        network, station, location, channel = station_phase.split(".")
        station_inv = inv.select(network=network, station=station)[0][0]
        east, north = pr.transform(
            latlon_proj, out_proj, station_inv.longitude, station_inv.latitude
        )
        elevation = station_inv.elevation
        receiver = {"e": east, "n": north, "z": elevation}
        p_raypath = isotropic_ray_trace(source, receiver, velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, receiver, velocity_model, "S")
        back_proj_amp[station_phase] = back_project_amplitude(
            p_raypath,
            s_raypath,
            amp_vals["amps"][0],
            channel[-1],
            frequency=referenceFreq,
        )
        row[station_phase] = inversion_matrix_row(
            p_raypath if channel[-1] == "P" else s_raypath, channel[-1]
        )
    A_matrix = list(row.values())
    dc_current = event["dc norm cur"] * sdr_to_mt(
        event["dc cur strike"], event["dc cur dip"], event["dc cur rake"]
    )
    dc_proposed = event["dc norm prop"] * sdr_to_mt(
        event["dc prop strike"], event["dc prop dip"], event["dc prop rake"]
    )
    fault_plane_solution = origin["custom MT"]["Fault Plane Solution"]

    if "strike1" in fault_plane_solution:
        fault_plane_solution["strike"] = fault_plane_solution["strike1"]
        fault_plane_solution["dip"] = fault_plane_solution["dip1"]
        fault_plane_solution["rake"] = fault_plane_solution["rake1"]

    fig, ax = plt.subplots(2, 3, figsize=[12, 8])
    plot_beachball_from_sdr(
        event["dc prop strike"],
        event["dc prop dip"],
        event["dc prop rake"],
        ax[1, 0],
        color="darkorange",
    )
    plot_beachball_from_sdr(
        event["dc cur strike"],
        event["dc cur dip"],
        event["dc cur rake"],
        ax[1, 1],
        color="firebrick",
    )
    plot_beachball_from_sdr(
        fault_plane_solution["strike"],
        fault_plane_solution["dip"],
        fault_plane_solution["rake"],
        ax[1, 2],
        color="forestgreen",
    )
    plot_regression(
        list(back_proj_amp.values()),
        dc_proposed,
        A_matrix,
        ax[0, 0],
        fig,
        list(back_proj_amp.keys()),
    )
    plot_regression(
        list(back_proj_amp.values()),
        dc_current,
        A_matrix,
        ax[0, 1],
        fig,
        list(back_proj_amp.keys()),
    )
    fps_sim_current = np.tensordot(
        sdr_to_mt(
            fault_plane_solution["strike"],
            fault_plane_solution["dip"],
            fault_plane_solution["rake"],
        ),
        dc_current / event["dc norm cur"],
    )
    fps_sim_proposed = np.tensordot(
        sdr_to_mt(
            fault_plane_solution["strike"],
            fault_plane_solution["dip"],
            fault_plane_solution["rake"],
        ),
        dc_proposed / event["dc norm prop"],
    )

    ax[0, 2].axis("off")
    ax[0, 2].text(0.5, 0.9, "proposed", ha="center")
    ax[0, 2].text(0.8, 0.9, "current", ha="center")
    ax[0, 2].text(0.0, 0.7, "FPS\nsimilarity")
    ax[0, 2].text(0, 0.55, "Pearson\nR")
    ax[0, 2].text(0, 0.4, "variance\nreduction")
    ax[0, 2].text(0.4, 0.75, f"{fps_sim_proposed:.3f}")
    ax[0, 2].text(0.7, 0.75, f"{fps_sim_current:.3f}")
    ax[0, 2].text(0.4, 0.6, f"{event['dc prop r']:.3f}")
    ax[0, 2].text(0.7, 0.6, f"{event['dc cur r']:.3f}")
    ax[0, 2].text(0.4, 0.45, f"{event['variance reduction prop']:.1f}%")
    ax[0, 2].text(0.7, 0.45, f"{event['variance reduction cur']:.1f}%")

    fig.savefig(f"DC_constraint//next_comparison_{event_id}.png")
