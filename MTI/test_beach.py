import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
from obspy.imaging.mopad_wrapper import beach

from sms_moment_tensor.MT_math import sdr_to_mt, mt_matrix_to_vector, conjugate_sdr

strike = 43.0
dip = 78.0
rake = 112.0

fig = plt.figure()

ax1 = plt.subplot(211)
ax1.set_aspect("equal")
ax2 = plt.subplot(212, projection="stereonet")

mt = sdr_to_mt(strike, dip, rake)

beach_sdr = beach((strike, dip, rake), xy=(0.8, 1), width=1)

beach_mt = beach(mt_matrix_to_vector(mt), xy=(2.2, 1), width=1, mopad_basis="XYZ")


ax1.add_collection(beach_sdr)
ax1.add_collection(beach_mt)
ax1.set_xlim(0, 3)
ax1.set_ylim(0, 2)
ax2.plane(strike, dip)
s2, d2, r2 = conjugate_sdr(strike, dip, rake)
ax2.plane(s2, d2)

plt.show()
