import matplotlib

matplotlib.use("Qt4agg")

import numpy as np
import matplotlib.pyplot as plt

location = "04"
f = open("shift_per_event_M0to0.7_location" + location + ".csv")
head = f.readline()
lines = f.readlines()
f.close()

f = open("statics_from_Ben.txt")
statics_BW = np.array([float(s) for s in f.read().split()])
f.close()

statics = {}
statics_demedian = {}
nstations = len(head.split(",")) - 1
stations = []
for station in head.split(",")[1:]:
    stations.append(station)
    statics[station] = []
    statics_demedian[station] = []

fig, ax = plt.subplots()

for line in lines:
    lspl = line.split(",")
    shifts = np.array([float(t) for t in lspl[1:]])
    median_shift = np.median(shifts[np.where(shifts > -900)[0]])
    for ii, station in enumerate(stations):
        statics[station].append(float(line.split(",")[ii + 1]))
        statics_demedian[station].append(float(line.split(",")[ii + 1]) - median_shift)

f2 = open("shift_per_event_M0to0.7_location" + location + ".csv")
head = f2.readline()
lines = f2.readlines()
f2.close()

for line in lines:
    lspl = line.split(",")
    shifts = np.array([float(t) for t in lspl[1:]])
    median_shift = np.median(shifts[np.where(shifts > -900)[0]])
    for ii, station in enumerate(stations):
        statics[station].append(float(line.split(",")[ii + 1]))
        statics_demedian[station].append(float(line.split(",")[ii + 1]) - median_shift)

med_stat = np.zeros(len(stations))
dev_stat = np.zeros(len(stations))
kurt_stat = np.zeros(len(stations))
for ii, station in enumerate(stations):
    statics[station] = np.array(statics[station])
    statics_demedian[station] = np.array(statics_demedian[station])
    med_stat[ii] = np.median(
        [s for s in statics[station][np.where(statics[station] > -900)[0]]]
    )
    dev_stat[ii] = np.std(
        [s for s in statics[station][np.where(statics[station] > -900)[0]]]
    )

p_line = ax.plot(
    range(1, len(stations) + 1),
    statics_BW[int(location) - 1 :: 4],
    color="firebrick",
    lw=2,
    zorder=1,
)

fliers = dict(marker=".")

ax.boxplot(
    [
        statics_demedian[station][np.where(statics_demedian[station] > -900)[0]]
        for station in stations
    ],
    flierprops=fliers,
)
ax.set_ylabel("time residual (s)")

f1, a1 = plt.subplots()
a1.plot(
    [
        np.median(
            statics_demedian[station][np.where(statics_demedian[station] > -900)[0]]
        )
        for station in stations
    ],
    [
        np.median(statics[station][np.where(statics[station] > -900)[0]])
        for station in stations
    ],
    "k.",
)
a1.plot([-0.05, 0.05], [-0.05, 0.05], color="firebrick", lw=2, zorder=-1)

plt.show()


g = open("aggregate_statics" + location + ".csv", "w")
g.write("station,median statics,25th percentile,75th percentile\n")
for station in stations:
    i_good = np.where(statics_demedian[station] > -900)[0]
    g.write(
        station
        + ",%.4e,%.4e,%.4e\n"
        % (
            np.median(statics_demedian[station][i_good]),
            np.percentile(statics_demedian[station][i_good], 25),
            np.percentile(statics_demedian[station][i_good], 75),
        )
    )
g.close()
