import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
from obspy import read, UTCDateTime, Trace
from obspy.imaging.mopad_wrapper import beach
from obspy.signal import filter
from obspy.signal.invsim import corn_freq_2_paz
from obspy.core.trace import Stats
from operator import itemgetter
from scipy.fftpack import fft, ifft, fftfreq
from scipy.signal import hilbert

from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.interface_scattering import (
    P_stack_transmission,
    SV_stack_transmission,
    SH_stack_transmission,
)
from moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from read_inputs import get_velocity_model, read_stations
from read_chevron_0204_data import (
    read_events,
    read_waveform,
    read_picks,
    read_wells,
)

matplotlib.rcParams.update({"font.size": 12})

dr = (
    "C:\\\\Users\\adambaig\\Project\\Chevron Grizzly\\"
    + "Chevron Synthetics\\2019-04-03T18-28\\"
)
plot_out = False
lwin = 50
catalog = read_events()
stations = read_stations()
velocity_model = get_velocity_model()
wells = read_wells()
Q = {"P": 70, "S": 50}
explosion = {
    "moment_tensor": np.matrix([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]),
    "stress_drop": 3.0e5,
}
xcorr_time = np.arange(-2.998, 3, 0.002)
ii = 0
paz_10hz = corn_freq_2_paz(10.0, damp=np.sqrt(0.5))
snr_threshold = 1.5
n_stations = 69
bandpass_hf = 50.0
bandpass_lf = 20.0
lwin2 = lwin // 2


def sqa(x):
    return np.squeeze(np.array(x))


mid_events = {
    k: v
    for (k, v) in catalog.items()
    if (v["magnitude"] > -1) and v["easting"] < 485395
}

mid_events = {k: v for (k, v) in catalog.items() if (k == "20161121041745.390000")}
xcorr_time = np.arange(-3, 3.001, 0.002)

for location in ["01", "02", "03", "04"]:
    statics = {}
    f = open("aggregate_statics" + location + ".csv")
    head = f.readline()
    for line in f.readlines():
        lspl = line.split(",")
        statics[lspl[0]] = float(lspl[1])
    f.close()
    f = open("MT_output_location" + location + ".csv", "w")
    f.write(
        "event time,magnitude,n picks,m11,m12,m13,m22,m23,m33,dc11,"
        + "dc12,dc13,dc22,dc23,dc33,cn_gen,cn_dv,rsq_gen,rsq_dc\n"
    )
    for event in mid_events:
        jj = -1
        st = read_waveform(UTCDateTime(event))
        picks = read_picks(event)
        if plot_out:
            fig0 = plt.figure(figsize=[16, 9])
            ax0 = fig0.add_axes([0.05, 0.1, 0.25, 0.8])
            ax1 = fig0.add_axes([0.35, 0.1, 0.25, 0.8])
            ax2 = fig0.add_axes([0.65, 0.1, 0.25, 0.8])
        dat = np.zeros([1501, n_stations])
        norm_dat = np.zeros([1501, n_stations])
        filtered_dat = np.zeros([1501, n_stations])
        time_series = np.arange(st[0].stats.npts) * st[0].stats.delta
        tsfreq = fftfreq(len(time_series), time_series[1])
        winfreq = fftfreq(100, time_series[1])
        if picks is None:
            print("picks needed for event " + event)
        else:
            location_picks = {
                k: v for k, v in picks.items() if ("." + location + "." in k)
            }
            if len(location_picks) > 0:
                p_raypath = [object() for _ in range(n_stations)]
                s_raypath = [object() for _ in range(n_stations)]
                for trace in st:
                    if trace.stats.location == location:
                        jj += 1
                        id = trace.get_id()
                        source = {
                            "x": mid_events[event]["easting"],
                            "y": mid_events[event]["northing"],
                            "z": 910 - mid_events[event]["depth"],
                        }
                        p_raypath[jj] = isotropic_ray_trace(
                            source, stations[id], velocity_model, "P"
                        )
                        s_raypath[jj] = isotropic_ray_trace(
                            source, stations[id], velocity_model, "S"
                        )
                        predicted_onset = location_picks[id]["P"] + statics[id]
                        dat[:, jj] = np.real(
                            ifft(
                                fft(trace.data)
                                * np.exp(
                                    2.0j
                                    * np.pi
                                    * (
                                        predicted_onset
                                        - UTCDateTime(event).timestamp
                                        - 0.85
                                    )
                                    * tsfreq
                                )
                            )
                        )
                        disp_spec = np.array(
                            10e-8
                            * abs(fft(dat[500 : 500 + lwin, jj]))[:lwin2]
                            / winfreq[:lwin2]
                            / lwin
                            / 2
                            / np.pi
                        )
                        noise_spec = np.array(
                            10e-8
                            * abs(fft(dat[400 - lwin : 400, jj]))[:lwin2]
                            / winfreq[:lwin2]
                            / lwin
                            / 2
                            / np.pi
                        )
                        igood = np.where(disp_spec / noise_spec > snr_threshold)[0]
                        filtered_dat[:, jj] = filter.bandpass(
                            dat[:, jj],
                            bandpass_lf,
                            bandpass_hf,
                            500,
                            zerophase=True,
                        )
                        if plot_out:
                            ax0.loglog(winfreq[igood], disp_spec[igood], alpha=0.2)
                            ax1.plot(
                                time_series - 1,
                                jj + 1 + dat[:, jj] / max(abs(dat[:, jj])),
                                "0.2",
                            )
                            ax2.plot(
                                time_series - 1,
                                jj
                                + 1
                                + filtered_dat[:, jj] / max(abs(filtered_dat[:, jj])),
                                "0.2",
                                zorder=-1,
                            )
                        norm_dat[:, jj] = filtered_dat[:, jj] / max(
                            abs(filtered_dat[:, jj])
                        )
            envelopes = hilbert(norm_dat, axis=0)
            envelopes *= np.conj(envelopes)
            i_max = np.argmax(np.sum(np.real(envelopes), axis=-1)[500:540]) + 500
            right_hand_side = []
            inversion_matrix = []
            signal, noise, snr = (
                np.zeros(n_stations),
                np.zeros(n_stations),
                np.zeros(n_stations),
            )
            if plot_out:
                ax1.set_xlim([-0.4, 0.4])
                ax2.set_xlim([-0.4, 0.4])
                ax1.set_title(
                    event
                    + (" Mw = %.1f" % mid_events[event]["magnitude"]).replace(
                        "-", "$-$"
                    )
                )
                ax1.set_ylabel("unfiltered data")
                ax0.set_ylabel("displacement spectra (m$\cdot$s)")
                ax2.set_ylabel(
                    "filtered data between %.1f Hz and %.1f Hz "
                    % (bandpass_lf, bandpass_hf)
                )
                ax0.set_xlabel("frequency")
                ax1.set_xlabel("shifted time (s)")
                ax2.set_xlabel("shifted time (s)")
            for jj in range(n_stations):
                n_amp_win = int(500 / bandpass_hf / 2)
                i_trace_max = (
                    np.argmax(
                        abs(filtered_dat[i_max - n_amp_win : i_max + n_amp_win, jj])
                    )
                    + i_max
                    - n_amp_win
                )
                amp = filtered_dat[i_trace_max, jj]
                signal[jj] = np.std(
                    filtered_dat[i_max - n_amp_win : i_max + n_amp_win, jj]
                )
                noise[jj] = np.std(filtered_dat[: 300 + 2 * n_amp_win, jj])
                snr[jj] = signal[jj] / noise[jj]
                if plot_out:
                    ax2.text(-0.39, jj + 1.01, ("%.1f" % (snr[jj])), fontsize=6)
                if snr[jj] > snr_threshold:
                    if plot_out:
                        if amp > 0:
                            ax2.plot(
                                time_series[i_trace_max] - 1,
                                jj + 1 + amp / max(abs(filtered_dat[:, jj])),
                                "o",
                                color="orangered",
                                markeredgecolor="k",
                                zorder=2,
                                alpha=0.3,
                            )
                            ax2.plot(
                                time_series[i_max - n_amp_win : i_max + n_amp_win] - 1,
                                jj
                                + 1
                                + filtered_dat[
                                    i_max - n_amp_win : i_max + n_amp_win, jj
                                ]
                                / max(abs(filtered_dat[:, jj])),
                                color="orangered",
                                lw=2,
                                zorder=1,
                            )
                        else:
                            ax2.plot(
                                time_series[i_max] - 1,
                                jj + 1 + amp / max(abs(filtered_dat[:, jj])),
                                "o",
                                color="turquoise",
                                markeredgecolor="k",
                                zorder=2,
                                alpha=0.3,
                            )
                            ax2.plot(
                                time_series[i_max - n_amp_win : i_max + n_amp_win] - 1,
                                jj
                                + 1
                                + filtered_dat[
                                    i_max - n_amp_win : i_max + n_amp_win, jj
                                ]
                                / max(abs(filtered_dat[:, jj])),
                                color="turquoise",
                                lw=2,
                                zorder=1,
                            )
                    right_hand_side.append(
                        back_project_amplitude(p_raypath[jj], s_raypath[jj], amp, "P")
                    )
                    inversion_matrix.append(inversion_matrix_row(p_raypath[jj], "P"))
                if sqa(right_hand_side).shape is ():
                    n_measurements = 0
                else:
                    n_measurements = len(sqa(right_hand_side))
            if n_measurements > 6:
                mt_gen, cn_gen, rsq_gen, mt_dc, cn_dev, rsq_dc = solve_moment_tensor(
                    inversion_matrix, right_hand_side
                )
                f.write(
                    event
                    + ","
                    + str(mid_events[event]["magnitude"])
                    + ","
                    + str(n_measurements)
                )
                f.write(
                    ",%.6e,%.6e,%.6e,%.6e,%.6e,%.6e"
                    % (
                        mt_gen[0],
                        mt_gen[1],
                        mt_gen[2],
                        mt_gen[3],
                        mt_gen[4],
                        mt_gen[5],
                    )
                )
                f.write(
                    ",%.6e,%.6e,%.6e,%.6e,%.6e,%.6e"
                    % (
                        mt_dc[0],
                        mt_dc[1],
                        mt_dc[2],
                        mt_dc[3],
                        mt_dc[4],
                        mt_dc[5],
                    )
                )
                f.write(",%.1f,%.1f,%.3f,%.3f\n" % (cn_gen, cn_dev, rsq_gen, rsq_dc))
            else:
                f.write(event + "," + str(mid_events[event]["magnitude"]) + "0")
                f.write(",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n")
    if plot_out:
        plt.show()
