import os

import matplotlib.pyplot as plt
import mplstereonet as mpls
from obspy.imaging.mopad_wrapper import beach

from sms_moment_tensor.plotting import (
    plot_fault_planes_from_Focmec,
    plot_strains_from_Focmec,
)


event_id = "9284"
input_dir = r"C:\Users\adambaig\Project\MTI\Artemis testing\7G\FocMec"
output_dir = r"C:\Users\adambaig\Project\MTI\Artemis testing\7G\FocMec\QC"

if not os.path.isdir(output_dir):
    os.makedirs(output_dir)

focmec_in_file = input_dir + os.sep + event_id + ".inp"
focmec_out_file = input_dir + os.sep + event_id + ".out"

fig_yarn, ax_yarn = mpls.subplots()
plot_fault_planes_from_Focmec(focmec_in_file, focmec_out_file, ax_yarn, fig_yarn)
fig_yarn.savefig(output_dir + os.sep + "focmec_QC_" + event_id + ".png")

fig_strain, (ax_p, ax_b, ax_t) = mpls.subplots(1, 3, figsize=[16, 5])
plot_strains_from_Focmec(focmec_out_file, ax_p, ax_b, ax_t, fig_strain)
fig_strain.savefig(output_dir + os.sep + "strain_QC_" + event_id + ".png")
