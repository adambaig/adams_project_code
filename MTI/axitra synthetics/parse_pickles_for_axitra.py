import glob
import numpy as np
from obspy import read_inventory
import os
import pickle
import pyproj as pr
from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from NocMeta.Meta import NOC_META

from make_axitra_inputs import (
    make_axi_hist_and_source,
    make_axi_data,
    make_station_file,
    prepare_velocity_model_for_axitra,
)

athena_code = "WTX"
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META[athena_code]['epsg']}")
base_dir = f"C:\\Users\\adambaig\\Project\\MTI\\axitra synthetics\\"
pickle_dir = (
    f"C:\\Users\\adambaig\\Project\\MTI\\Artemis testing\\" f"Pickles\\{athena_code}\\"
)
station_xml_dir = "C:\\Users\\adambaig\\Project\\MTI\\axitra synthetics\\Station_xmls\\"
pickles = glob.glob(f"{pickle_dir}\\*.pickle")
if not os.path.exists(f"{station_xml_dir}{athena_code}_Full.xml"):
    inv = formStaXml(outDir="Station_xmls//.", athenaCode=athena_code)
else:
    inv = read_inventory(f"{station_xml_dir}{athena_code}_Full.xml")


stations = {}
for network in inv:
    for station in network:
        netstat_code = f"{network.code}.{station.code}"
        #
        # stations[netstat_code] = {"elevation": 0}
        stations[netstat_code] = {"elevation": station.elevation}
        (
            stations[netstat_code]["easting"],
            stations[netstat_code]["northing"],
        ) = pr.transform(latlon_proj, out_proj, station.longitude, station.latitude)


configuration = {
    "n frequencies": 1024,
    "trace length": 80,
    "attenuation weighting": 2,
    "n receivers": len(stations),
    "n sources": 1,
    "medium periodicity": 1000000,
    "maximum iterations": 100000,
    "is in latlon": False,
    "include freesurface": True,
}
q_model = [{"P": 60, "S": 60}]
pickle_files = glob.glob(f"{pickle_dir}*.pickle")
for pickle_file in pickle_files:
    event_id = os.path.split(pickle_file)[1].split(".")[0]
    output_dir = f"{base_dir}{athena_code}\\{event_id}"
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    station_file = make_station_file(stations, output_dir)
    pickle_out = pickle.load(open(pickle_file, "rb"))
    axi_data_file = make_axi_data(
        configuration, pickle_out["velDict"], q_model, output_dir
    )
    event_from_pickle = pickle_out["eveDict"]["event"]["origin"]
    easting, northing = pr.transform(
        latlon_proj,
        out_proj,
        event_from_pickle["longitude"],
        event_from_pickle["latitude"],
    )
    source = {
        "easting": easting,
        "northing": northing,
        "depth": 1000 * event_from_pickle["depth"],
        "strike": event_from_pickle["mt"]["strike"],
        "dip": event_from_pickle["mt"]["dip"],
        "rake": event_from_pickle["mt"]["rake"],
        "moment": 10 ** (1.5 * event_from_pickle["magnitudes"][0]["value"] + 9.05),
    }
    axi_hist, source_file = make_axi_hist_and_source(source, output_dir)
