import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory
from obspy.imaging.mopad_wrapper import beach
import pickle
import pyproj as pr
from sklearn.neighbors import NearestNeighbors, BallTree

from NocMeta.Meta import NOC_META
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.rotation import rotate_to_raypath
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import mt_to_sdr, mt_matrix_to_vector, sdr_to_mt


from sms_moment_tensor.plotting import (
    plot_regression,
    plot_beachballs,
    plot_beachball_from_sdr,
    plot_fault_planes_from_Focmec,
)

from read_inputs import read_source, read_stations, read_velocity_from_axidata

dir = "layered_model//depth6.6//"

stations = read_stations(dir + "station_xyz.tsv")
source = read_source(dir + "source_xyz.tsv")
velocity_model = read_velocity_from_axidata(f"{dir}axi.data")


A_matrix = []
right_hand_side = []
station_tags = []
for station_id, station in stations.items():
    stx = read(f"{dir}axi{station_id.zfill(3)}.X.sac")
    sty = read(f"{dir}axi{station_id.zfill(3)}.Y.sac")
    stz = read(f"{dir}axi{station_id.zfill(3)}.Z.sac")
    waveform = {"e": sty[0].data, "n": stx[0].data, "z": stz[0].data}
    p_raypath = isotropic_ray_trace(source, station, velocity_model, "P")
    s_raypath = isotropic_ray_trace(source, station, velocity_model, "S")
    rotated_waveform = rotate_to_raypath(waveform, p_raypath, mode="pvh")
    npts = stx[0].stats.npts
    delta = stx[0].stats.delta
    time = np.arange(npts) * delta + delta
    amp = np.interp(p_raypath["traveltime"], time, rotated_waveform["p"])
    A_matrix.append(inversion_matrix_row(p_raypath, "P"))
    right_hand_side.append(
        back_project_amplitude(
            p_raypath,
            s_raypath,
            amp,
            "P",
            frequency=1 / 0.1,
            Q=1000,
            transmission=True,
        )
    )
    station_tags.append(f"{station_id}.P")
    amp = np.interp(s_raypath["traveltime"], time, rotated_waveform["v"])
    A_matrix.append(inversion_matrix_row(s_raypath, "V"))
    right_hand_side.append(
        back_project_amplitude(
            p_raypath,
            s_raypath,
            amp,
            "V",
            frequency=1 / 0.1,
            Q=500,
            transmission=True,
        )
    )
    station_tags.append(f"{station_id}.V")
    amp = np.interp(s_raypath["traveltime"], time, rotated_waveform["h"])
    A_matrix.append(inversion_matrix_row(s_raypath, "H"))
    right_hand_side.append(
        back_project_amplitude(
            p_raypath,
            s_raypath,
            amp,
            "H",
            frequency=1 / 0.1,
            Q=500,
            transmission=True,
        )
    )
    station_tags.append(f"{station_id}.H")

    # fig1, ax1 = plt.subplots(figsize=[8, 3])
    # ax1.plot(time, rotated_waveform["p"])
    # y1, y2 = ax1.get_ylim()
    # ax1.plot([p_raypath["traveltime"], p_raypath["traveltime"]], [y1, y2], "k")
    #
    # fig2, ax2 = plt.subplots(figsize=[8, 3])
    # ax2.plot(time, rotated_waveform["v"])
    # y1, y2 = ax2.get_ylim()
    # ax2.plot([s_raypath["traveltime"], s_raypath["traveltime"]], [y1, y2], "k")
    # fig3, ax3 = plt.subplots(figsize=[8, 3])
    # ax3.plot(time, rotated_waveform["h"])
    # y1, y2 = ax3.get_ylim()
    # ax3.plot([s_raypath["traveltime"], s_raypath["traveltime"]], [y1, y2], "k")


moment_tensor = solve_moment_tensor(A_matrix, right_hand_side)
fig_reg, ax_reg = plt.subplots()
plot_regression(
    right_hand_side,
    moment_tensor["general"],
    A_matrix,
    ax_reg,
    fig=fig_reg,
    nslc_tags=station_tags,
)


fig, ax = plt.subplots()
ax.set_aspect("equal")
input_beachball = beach(
    (90, 90, 180), xy=(1, 1), width=0.8, facecolor="indigo", linewidth=0.5
)
output_beachball = beach(
    mt_matrix_to_vector(
        moment_tensor["general"] / np.linalg.norm(moment_tensor["general"])
    ),
    xy=(2, 1),
    width=0.8,
    facecolor="indigo",
    linewidth=0.5,
    mopad_basis="XYZ",
)
ax.add_collection(input_beachball)
ax.add_collection(output_beachball)
ax.set_xlim([0, 3])
ax.set_ylim([0, 2])
ax.text(1, 1.5, "input", ha="center")
ax.text(2, 1.5, "output", ha="center")

ax.axis("off")
plt.show()

np.tensordot(
    sdr_to_mt(90, 90, 180),
    moment_tensor["general"] / np.linalg.norm(moment_tensor["general"]),
)
