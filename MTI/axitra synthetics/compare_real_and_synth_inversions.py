import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory
import pickle
import pyproj as pr
from sklearn.neighbors import NearestNeighbors, BallTree

from NocMeta.Meta import NOC_META
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.rotation import rotate_to_raypath
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import mt_to_sdr


from sms_moment_tensor.plotting import (
    plot_regression,
    plot_beachballs,
    plot_beachball_from_sdr,
    plot_fault_planes_from_Focmec,
)

from read_inputs import read_source, read_stations, read_velocity_from_axidata


athena_code = "WTX"
event_id = "100395"
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META[athena_code]['epsg']}")

dir = f"{athena_code}//{event_id}//"
station_xml_dir = "C:\\Users\\adambaig\\Project\\MTI\\axitra synthetics\\Station_xmls\\"

stations = read_stations(dir + "station.tsv")
source = read_source(dir + "source.tsv")
velocity_model = read_velocity_from_axidata(f"{dir}axi.data")
inv = read_inventory(f"{station_xml_dir}{athena_code}_Full.xml")

station_en = {}
for network in inv:
    for station in network:
        netstat_code = f"{network.code}.{station.code}"
        # station_en[netstat_code] = {"elevation": 0}
        station_en[netstat_code] = {"elevation": station.elevation}
        (
            station_en[netstat_code]["easting"],
            station_en[netstat_code]["northing"],
        ) = pr.transform(latlon_proj, out_proj, station.longitude, station.latitude)

station_array = np.array([[s["easting"], s["northing"]] for s in station_en.values()])
tree = BallTree(station_array)

pickle_file = (
    f"C:\\Users\\adambaig\\Project\\MTI\\Artemis testing\\"
    f"Pickles\\{athena_code}\\{event_id}.pickle"
)
with open(pickle_file, "rb") as f:
    event_from_pickle = pickle.load(f)
event_from_pickle["velDict"]
comp_stations = {}
for comp in "P", "V", "H":
    comp_stations[comp] = [
        ".".join(k.split(".")[:2])
        for k in event_from_pickle["ampDict"].keys()
        if k[-1] == comp
    ]

station_list = list(station_en.keys())

A_matrix = []
right_hand_side = []
NSLCs = list(event_from_pickle["ampDict"].keys())
for station_id, station in stations.items():
    stx = read(f"{dir}axi{station_id.zfill(3)}.X.sac")
    sty = read(f"{dir}axi{station_id.zfill(3)}.Y.sac")
    stz = read(f"{dir}axi{station_id.zfill(3)}.Z.sac")
    distance, index = tree.query([[station["e"], station["n"]]])
    if np.squeeze(distance) < 1:  # if station in found
        found_station = station_list[np.squeeze(index)]

    waveform = {"e": sty[0].data, "n": stx[0].data, "z": stz[0].data}
    p_raypath = isotropic_ray_trace(source, station, velocity_model, "P")
    s_raypath = isotropic_ray_trace(source, station, velocity_model, "S")
    rotated_waveform = rotate_to_raypath(waveform, p_raypath, mode="pvh")
    npts = stx[0].stats.npts
    delta = stx[0].stats.delta
    time = np.arange(npts) * delta + delta
    station["p_amp"] = np.interp(p_raypath["traveltime"], time, rotated_waveform["p"])
    station["v_amp"] = np.interp(s_raypath["traveltime"], time, rotated_waveform["v"])
    station["h_amp"] = np.interp(s_raypath["traveltime"], time, rotated_waveform["h"])
    if found_station in comp_stations["P"]:
        fig, ax = plt.subplots(figsize=[8, 3])
        ax.plot(time, rotated_waveform["p"])
        y1, y2 = ax.get_ylim()
        ax.plot(
            [p_raypath["traveltime"], p_raypath["traveltime"]],
            [0.9 * y1, 0.9 * y2],
        )
        ax.set_xlim([0, 1.2 * p_raypath["traveltime"]])
        xy = fig.ginput(1)
        amp = np.interp(xy[0][0], time, rotated_waveform["p"])
        A_matrix.append(inversion_matrix_row(p_raypath, "P"))
        right_hand_side.append(
            back_project_amplitude(p_raypath, s_raypath, amp, "P", frequency=1 / 0.1)
        )
    if found_station in comp_stations["V"]:
        fig, ax = plt.subplots(figsize=[8, 3])
        ax.plot(time, rotated_waveform["v"])
        y1, y2 = ax.get_ylim()
        ax.plot(
            [s_raypath["traveltime"], s_raypath["traveltime"]],
            [0.9 * y1, 0.9 * y2],
        )
        ax.set_xlim([1.2 * p_raypath["traveltime"], 1.2 * s_raypath["traveltime"]])
        xy = fig.ginput(1)
        amp = np.interp(xy[0][0], time, rotated_waveform["v"])
        A_matrix.append(inversion_matrix_row(s_raypath, "V"))
        right_hand_side.append(
            back_project_amplitude(p_raypath, s_raypath, amp, "V", frequency=1 / 0.1)
        )
    if found_station in comp_stations["H"]:
        fig, ax = plt.subplots(figsize=[8, 3])
        ax.plot(time, rotated_waveform["h"])
        y1, y2 = ax.get_ylim()
        ax.plot(
            [s_raypath["traveltime"], s_raypath["traveltime"]],
            [0.9 * y1, 0.9 * y2],
        )
        ax.set_xlim([1.2 * p_raypath["traveltime"], 1.2 * s_raypath["traveltime"]])
        xy = fig.ginput(1)
        amp = np.interp(xy[0][0], time, rotated_waveform["h"])
        A_matrix.append(inversion_matrix_row(s_raypath, "H"))
        right_hand_side.append(
            back_project_amplitude(p_raypath, s_raypath, amp, "H", frequency=1 / 0.1)
        )


moment_tensor = solve_moment_tensor(A_matrix, right_hand_side)

fig_reg, ax_reg = plt.subplots()
plot_regression(
    right_hand_side,
    moment_tensor["dc"],
    A_matrix,
    ax_reg,
    fig=fig_reg,
    nslc_tags=NSLCs,
)

plt.show()
mt_to_sdr(moment_tensor["dc"])
event_from_pickle.keys()
event_from_pickle["eveDict"]["event"]["origin"]["mt"]
