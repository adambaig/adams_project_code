import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read


from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.rotation import rotate_to_raypath


from read_inputs import read_source, read_stations, read_velocity_from_axidata

dir = "dipslip_MT_test//"
stations = read_stations("dipslip_MT_test//station_xyz.tsv")
source = read_source("dipslip_MT_test//source_xyz.tsv")

velocity_model = read_velocity_from_axidata("dipslip_MT_test//axi.data")
station_id, station = list(stations.items())[4]
for station_id, station in stations.items():
    fig, ax = plt.subplots(figsize=[12, 5])
    stx = read(f"{dir}axi{station_id.zfill(3)}.X.sac")
    sty = read(f"{dir}axi{station_id.zfill(3)}.Y.sac")
    stz = read(f"{dir}axi{station_id.zfill(3)}.Z.sac")
    waveform = {"e": sty[0].data, "n": stx[0].data, "z": stz[0].data}
    p_raypath = isotropic_ray_trace(station, source, velocity_model, "P")
    s_raypath = isotropic_ray_trace(station, source, velocity_model, "S")
    rotated_waveform = rotate_to_raypath(waveform, p_raypath, mode="pvh")
    scale = max([max(abs(component)) for component in rotated_waveform.values()])
    npts = stx[0].stats.npts
    delta = stx[0].stats.delta
    time = np.arange(npts) * delta + delta
    ax.plot(time, rotated_waveform["p"] / scale + 0.2, "firebrick")
    ax.plot(time, rotated_waveform["v"] / scale + 0, "forestgreen")
    ax.plot(time, rotated_waveform["h"] / scale - 0.2, "royalblue")
    ax.plot([p_raypath["traveltime"], p_raypath["traveltime"]], [-1, 1], "0.2")
    ax.plot([s_raypath["traveltime"], s_raypath["traveltime"]], [-1, 1], "0.2")
