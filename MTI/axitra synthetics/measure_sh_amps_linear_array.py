import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory
from obspy.imaging.mopad_wrapper import beach
import pickle
import pyproj as pr
from scipy.fftpack import fft, ifft

from NocMeta.Meta import NOC_META
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.rotation import rotate_to_raypath
from sms_ray_modelling.interface_scattering import SH_scattering
from read_inputs import read_source, read_stations, read_velocity_from_axidata

dir = "layered_model//depth_6.6_linear_array//"

stations = read_stations(dir + "station_xyz.tsv")
source = read_source(dir + "source_xyz.tsv")
velocity_model = read_velocity_from_axidata(f"{dir}axi.data")


def hrz_slowness(raypath):
    hrz_slowness_squared = (
        raypath["hrz_slowness"]["e"] * raypath["hrz_slowness"]["e"]
        + raypath["hrz_slowness"]["n"] * raypath["hrz_slowness"]["n"]
    )
    return np.sqrt(hrz_slowness_squared)


moment = 1000000000000.0
amps = np.zeros(len(stations))
distance = np.zeros(len(stations))
sh_trans_coeff = np.zeros(len(stations))
for i_station, (station_id, station) in enumerate(stations.items()):
    stx = read(f"{dir}axi{station_id.zfill(3)}.X.sac")
    sty = read(f"{dir}axi{station_id.zfill(3)}.Y.sac")
    stz = read(f"{dir}axi{station_id.zfill(3)}.Z.sac")
    waveform = {"e": sty[0].data, "n": stx[0].data, "z": stz[0].data}
    p_raypath = isotropic_ray_trace(source, station, velocity_model, "P")
    s_raypath = isotropic_ray_trace(source, station, velocity_model, "S")
    rotated_waveform = rotate_to_raypath(waveform, p_raypath, mode="pvh")
    npts = stx[0].stats.npts
    delta = stx[0].stats.delta
    time = np.arange(npts) * delta + delta
    # fig3, ax3 = plt.subplots(figsize=[8, 3])
    # ax3.plot(time, rotated_waveform["h"])
    # y1, y2 = ax3.get_ylim()
    # ax3.plot([s_raypath["traveltime"], s_raypath["traveltime"]], [y1, y2], "k")
    s_ttt = s_raypath["traveltime"]
    s_win = [s_ttt - 0.02, s_ttt + 0.4]
    i_win1 = np.where(abs(time - s_win[0]) < delta)[0][0]
    i_win2 = np.where(abs(time - s_win[1]) < delta)[0][1]
    f_sh = fft(rotated_waveform["h"][i_win1:i_win2])
    outgoing_angle = np.arcsin(hrz_slowness(s_raypath) * velocity_model[1]["vs"])
    sh_trans_coeff[i_station] = SH_scattering(
        velocity_model[0], velocity_model[1], hrz_slowness(s_raypath)
    )[0, 1]
    amps[i_station] = (
        np.abs(f_sh[0]) * s_raypath["geometrical_spreading"] / np.sin(outgoing_angle)
    )
    distance[i_station] = station["n"]


fig, (a1, a2) = plt.subplots(1, 2)
a1.plot(distance, amps)
a2.plot(distance, sh_trans_coeff)

2 * 2.4 * 1.87 / (2.4 * 1.87 + 2.7 * 2.886)
SH_scattering(velocity_model[0], velocity_model[1], 0)[1, 0]

plt.show()
