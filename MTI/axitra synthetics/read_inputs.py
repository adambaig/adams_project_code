import numpy as np


def read_stations(station_file):
    f = open(station_file)
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split()
        station_id = lspl[0]
        stations[station_id] = {
            "n": float(lspl[1]),
            "e": float(lspl[2]),
            "z": -float(lspl[3]),
        }
    return stations


def read_source(source_file):
    f = open(source_file)
    line = f.readline()
    f.close()
    lspl = line.split()
    return {"n": float(lspl[1]), "e": float(lspl[2]), "z": -float(lspl[3])}


def read_velocity_from_axidata(axi_data):
    f = open(axi_data)
    velocity_model_lines = f.read().split("//")[1].split("\n")
    f.close()
    velocity_model = []
    top = 99999
    line = velocity_model_lines[1]
    for line in velocity_model_lines:
        if line != "":
            thickness, vp, vs, rho = [float(s) for s in line.split()[:4]]
            layer = {
                "vp": vp,
                "vs": vs,
                "rho": rho,
            }
            if top == 99999:
                top = -thickness
            else:
                top -= thickness
                layer["top"] = top
            velocity_model.append(layer)
    return velocity_model
