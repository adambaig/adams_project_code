import logging
import os


def ensure_ends_in_os_sep(path):
    if path[-1] != os.sep:
        path += os.sep
    return path


def make_axi_hist_and_source(source, output_dir):
    output_dir = ensure_ends_in_os_sep(output_dir)
    axi_hist = f"{output_dir}axi.hist"
    source_file = f"{output_dir}source.tsv"
    f = open(axi_hist, "w")
    f.write(
        f"1\t{source['moment']}\t{source['strike']}\t{source['dip']}\t"
        f"{source['rake']}\t0\t0\t0"
    )
    f.close()
    f = open(source_file, "w")
    f.write(f"1\t{source['northing']}\t{source['easting']}\t{source['depth']}\t")
    f.close()
    return axi_hist, source_file


def make_axi_data(configuration, velocity_model, q_model, output_dir):
    output_dir = ensure_ends_in_os_sep(output_dir)
    axi_data = f"{output_dir}axi.data"
    f_data = open(axi_data, "w")
    f_data.write("&input\n")
    f_data.write(f"nc={len(velocity_model)},")
    f_data.write(f"nfreq={configuration['n frequencies']},")
    f_data.write(f"tl={configuration['trace length']},")
    f_data.write(f"aw={configuration['attenuation weighting']},")
    f_data.write(f"nr={configuration['n receivers']},")
    f_data.write(f"ns={configuration['n sources']},")
    f_data.write(f"xl={configuration['medium periodicity']},")
    f_data.write(f"ikmax={configuration['maximum iterations']},\n")
    f_data.write("latlon=")
    if configuration["is in latlon"]:
        f_data.write(".true.,")
    else:
        f_data.write(".false.,")
    f_data.write("freesurface=")
    if configuration["include freesurface"]:
        f_data.write(".true.,")
    else:
        f_data.write(".false.,")
    f_data.write('sourcefile="source.tsv",statfile="station.tsv"\n//\n')
    velocity_model = prepare_velocity_model_for_axitra(velocity_model, q_model)
    for layer in velocity_model:
        f_data.write(
            f"{layer['thickness']:.1f}\t{layer['vp']:.1f}\t{layer['vs']:.1f}\t"
            f"{layer['rho']:.1f}\t{layer['Qp']:.1f}\t{layer['Qs']:.1f}\n"
        )
    f_data.close()
    return axi_data


def prepare_velocity_model_for_axitra(velocity_model, q_model):
    if type(q_model) != list:
        logging.error("q_model must be a list of dictionaries")
        return None
    if len(q_model) == 1:
        q_model_single_layer = q_model[0]
        q_model = []
        for layer in velocity_model:
            q_model.append(q_model_single_layer)
    for i_layer, layer in enumerate(velocity_model[:-1]):
        layer["thickness"] = layer["top"] - velocity_model[i_layer + 1]["top"]
    velocity_model[-1]["thickness"] = 6371000
    for vm_layer, q_layer in zip(velocity_model, q_model):
        vm_layer["Qp"] = q_layer["P"]
        vm_layer["Qs"] = q_layer["S"]
    return velocity_model


def make_station_file(stations, output_dir):
    output_dir = ensure_ends_in_os_sep(output_dir)
    station_file = f"{output_dir}station.tsv"
    f_station = open(station_file, "w")
    for i_station, station in enumerate(stations.values()):
        f_station.write(
            f"{i_station+1}\t{station['northing']}\t"
            f"{station['easting']}\t{-station['elevation']}\n"
        )
    f_station.close()
    return station_file
