import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read

from logiklyst.PluginsNOC.EventMarking import getAmpFromPickTime
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.rotation import rotate_to_raypath

from read_inputs import read_source, read_stations, read_velocity_from_axidata


dir = "WTX//100395//"
stations = read_stations(dir + "station.tsv")
source = read_source(dir + "source.tsv")
velocity_model = read_velocity_from_axidata(f"{dir}axi.data")


for station_id, station in stations.items():
    stx = read(f"{dir}axi{station_id.zfill(3)}.X.sac")
    sty = read(f"{dir}axi{station_id.zfill(3)}.Y.sac")
    stz = read(f"{dir}axi{station_id.zfill(3)}.Z.sac")
    waveform = {"e": sty[0].data, "n": stx[0].data, "z": stz[0].data}
    p_raypath = isotropic_ray_trace(station, source, velocity_model, "P")
    s_raypath = isotropic_ray_trace(station, source, velocity_model, "S")
    rotated_waveform = rotate_to_raypath(waveform, p_raypath, mode="pvh")
    scale = max([max(abs(component)) for component in rotated_waveform.values()])
    npts = stx[0].stats.npts
    delta = stx[0].stats.delta
    time = np.arange(npts) * delta + delta
    station["p_amp"] = np.interp(p_raypath["traveltime"], time, rotated_waveform["p"])
    station["v_amp"] = np.interp(s_raypath["traveltime"], time, rotated_waveform["v"])
    station["h_amp"] = np.interp(s_raypath["traveltime"], time, rotated_waveform["h"])


fig, (ax_p, ax_v, ax_h) = plt.subplots(1, 3, figsize=[16, 5])
station_east = [v["e"] for v in stations.values()]
station_north = [v["n"] for v in stations.values()]
station_elevation = [v["z"] for v in stations.values()]
station_p = [v["p_amp"] for v in stations.values()]
station_v = [v["v_amp"] for v in stations.values()]
station_h = [v["h_amp"] for v in stations.values()]
for ax in ax_p, ax_v, ax_h:
    ax.set_aspect("equal")
    ax.plot(source["e"], source["n"], "r*")
ax_p.scatter(station_east, station_north, c=np.sign(station_p))
ax_v.scatter(station_east, station_north, c=np.sign(station_v))
ax_h.scatter(station_east, station_north, c=np.sign(station_h))
