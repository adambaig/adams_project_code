import glob
import numpy as np
from sms_moment_tensor.MT_math import mt_to_sdr, mt_vector_to_matrix

PI = np.pi
D2R = PI / 180

dir = "C:\\Users\\adambaig\\Project\\MTI\\axitra synthetics\\simple_MT_test\\"

f_station = open(dir + "station_xyz.tsv", "w")

n_stations = 12
source_depth = 10

for i_station in range(n_stations):
    station_n = source_depth * np.cos(i_station * 30 * D2R)
    station_e = source_depth * np.sin(i_station * 30 * D2R)
    f_station.write(f"{i_station+1}\t{station_n:.1f}\t{station_e:.1f}")
    f_station.write("\t0\n")
f_station.close()

mts = np.eye(6)
f_hist = open(dir + "axi.hist", "w")
f_source = open(dir + "source_xyz.tsv", "w")
for i_mt in range(3, 6):
    mt = mt_vector_to_matrix(mts[i_mt, :])
    strike, dip, rake = mt_to_sdr(mt, conjugate=False)
    f_source.write(f"{i_mt-2}\t{0}\t{0}\t")
    f_source.write(f"{source_depth:.0f}\n")
    f_hist.write(f"{i_mt-2}\t{1.e12}\t{strike:.1f}\t")
    f_hist.write(f"{dip:.1f}\t{rake:.1f}\t0\t0\t0\n")
f_hist.close()
f_source.close()


f_data = open(dir + "axi.data", "w")
f_data.write("&input\n")
f_data.write(f"nc=1,")
f_data.write(f"nfreq=1024,tl=60.,aw=2.,")
f_data.write(f"nr={n_stations},ns={3},xl=1000.,ikmax=100000,\n")
f_data.write(f"latlon=.false.,freesurface=.false.,")
f_data.write('sourcefile="source_xyz.tsv",statfile="station_xyz.tsv"\n')
f_data.write("//")
f_data.write("0.\t5.\t2.886\t2.7\t1000.\t500.")
f_data.close()
