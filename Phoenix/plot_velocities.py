import matplotlib.pyplot as plt
import matplotlib

matplotlib.use("Qt5Agg")


with open("MMo.x-1_VSP_VelocityTable1.txt") as f:
    _head = [f.readline() for i in range(16)]
    lines = f.readlines()
lines
n_layers = len(lines) // 2
layers = [{} for i in range(n_layers)]
for i_line, line in enumerate(lines[:-1]):
    i_layer = (i_line + 1) // 2
    split_line = line.split()
    if i_line % 2 == 0:
        layers[i_layer].update({"vp": float(split_line[2])})
    else:
        layers[i_layer].update({"top": float(split_line[0])})

fig, ax = plt.subplots()

tops = [v["top"] for v in layers if "top" in v]
vps = [v["vp"] for v in layers]
len(tops)
len(vps)

top_curve = [0]
v_curve = []
for t, v in zip(tops, vps[0:]):
    v_curve.append(v)
    top_curve.append(t)
    v_curve.append(v)
    top_curve.append(t)
v_curve.append(v)


fig, ax = plt.subplots(figsize=[6, 13])
ax.plot(v_curve, top_curve)
ax.set_ylim([3500, 0])

fig.ginput(timeout=-1, n=-1)
