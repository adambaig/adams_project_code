import numpy as np

spacing = 250
tvd = 3100
width = 600

tvd * 2 + 2300
tvd * 2 + 600

major_axis = 9800 / 2
minor_axis = 7600 / 2

PI = np.pi
sensors = {}
ii = 0
for i_line in range(6):
    angle = PI / 6 * i_line
    line_end = [major_axis * np.cos(angle), minor_axis * np.sin(angle)]
    line_length = np.sqrt(sum([a * a for a in line_end]))
    n_sensors = int(2 * line_length // spacing)
    for i_sensor in range(n_sensors):
        horizontal_range = spacing * (i_sensor - n_sensors // 2)
        if abs(horizontal_range) > 1:
            ii += 1
            sensors[ii] = {
                "e": horizontal_range * np.sin(angle),
                "n": horizontal_range * np.cos(angle),
            }

len(sensors)

with open("star_sensors_200.csv", "w") as f:
    f.write("index, easting (m), northing (m), elevation (m)\n")
    for sensor_index, sensor in sensors.items():
        f.write(f'{sensor_index},{sensor["e"]:.1f},{sensor["n"]:.1f},0.\n')
