# all units in SI unless otherwise noted.
# simulations are in a rotated coorinate frame, x --> across well, y --> along well

file_prefix = "Pheonix_star_2000_"

scenarios = [
    {
        "geophones_per_string": 12,
        "nodes_per_superstation": 1,
        "station_file": "star_sensors_2000.csv",
    },
    #    {
    #        "geophones_per_string": 6,
    #        "nodes_per_superstation": 31,
    #        "station_file": "stations.csv",
    #    },
]

do_steps = {
    "initial plotting": 1,
    "detectability curve modelling": 1,
    "detectability curve imaging": 1,
    "curve fitting": 1,
    "detectability map modelling": 1,
    "detectability map imaging": 1,
    "detectability plotting": 1,
    "error simulation": 0,
    "error imaging": 0,
    "error reporting": 0,
}

geology_config = "geology_elev.cfg"

n_threads = 6

well_dimensions = {
    "tvd": 3100,
    "pad_width": 600,
    "well_length": 2300,
    "stage_half_length": 200,
    "well_head_elevation": 440,
}

simulation = {
    "sample_rate": 0.002,
    "t_shift": 0.5,
    "trace_length": 5,
    "bandpass_freqs": [20, 50],
    "noise_ppsd": "5pct_average.csv",
    "semblance_weighted_stack_factor": 1,
}

source = {
    "stress_drop": 3.0e5,
    "mechanism": [1, -1, 0, 0, 0, 0],
    "min_magnitude": -3,
    "max_magnitude": 0,
    "magnitude_inc": 0.1,
}

imaging_parameters = {
    "resample_frequency": 125,
    "input_delta_z": 10,
    "input_starting_depth": -well_dimensions["well_head_elevation"],
    "max_hrz_distance": 10000,
    "hrz_spacing_tt_grid": 100,
    "static_sigma": 0.003,
    "n_image_grid": [50, 50, 50],
    "d_image_grid": [4, 4, 4],
    "pre_padding_samples": 125,
}

display_parameters = {
    "SI_units": True,  # Note, display only! All computation in SI regardless of value
    "grid_xy": [400, 400],
    "n_xy": [4, 6],
    "well_files": [
        "Well_1.csv",
        "Well_2.csv",
        "Well_3.csv",
    ],
    "well_trend_deg": 0,
    "colors": ["firebrick", "royalblue", "forestgreen", "darkgoldenrod"],
    "undetectability": -2.4,
    "large_mag": -0.8,
    "sigma_significance": 2,
    "waveform_plots": True,
    "M_colorbar_min": -2.4,
    "M_colorbar_max": -2,
    "elevation_range": [700, -3500],  # in display units (i.e. ft or m)
    "velocity_range": [0, 5500],  # ditto
    "well_center": {"easting": 0, "northing": 0.0},
}

location_error_simulations = {"n_realizations": 20, "M_simulation": 0}
