import matplotlib.pyplot as plt
import numpy as np

bf_16_deviation = r"Project Geometry\Obs_BF16_Deviation Survey_feet.csv"

PI = np.pi
D2R = 180.0 / PI
M2F = 3.28084

with open(bf_16_deviation) as f:
    _head = [f.readline() for i in range(7)]
    lines = f.readlines()

deviation = []
for line in lines:
    split_line = line.split(",")
    deviation.append(
        {
            "md_m": float(split_line[0]),
            "inclination_degrees": float(split_line[1]),
            "azimuth_degrees": float(split_line[2]),
        }
    )

deviation_last = deviation[0]
deviation[0]["easting"] = 0
deviation[0]["northing"] = 0
deviation[0]["z"] = 0

for line in deviation[1:]:
    line["d_md_m"] = line["md_m"] - deviation_last["md_m"]
    line["dz"] = np.cos(line["inclination_degrees"] / D2R) * line["d_md_m"]
    line["d_easting"] = (
        np.sin(line["inclination_degrees"] / D2R)
        * np.sin(line["azimuth_degrees"] / D2R)
        * line["d_md_m"]
    )
    line["d_northing"] = (
        np.sin(line["inclination_degrees"] / D2R)
        * np.cos(line["azimuth_degrees"] / D2R)
        * line["d_md_m"]
    )
    line["easting"] = deviation_last["easting"] + line["d_easting"]
    line["northing"] = deviation_last["northing"] + line["d_northing"]
    line["z"] = deviation_last["z"] + line["dz"]
    deviation_last = line


with open("B16_deviation_relative_coordinates_feet.csv", "w") as f:
    f.write("MD,Easting,Northing, TVD\n")
    for line in deviation:
        f.write(
            f'{M2F*line["md_m"]:.4f},{M2F*line["easting"]:.4f},{M2F*line["northing"]:.4f},{M2F*line["z"]:.4f}\n'
        )


# fig,ax = plt.subplots()
# ax.set_aspect('equal')
#
# ax.plot( [line["easting"] for line in deviation[1:]],  [line["northing"] for line in deviation[1:]] )
