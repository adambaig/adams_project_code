
from __future__ import print_function
import os
import numpy as np
from obspy import UTCDateTime
from joblib import Parallel, delayed

from SupportModules.StreamSupport import getStreamData_NocArchive


def main():
    dataDir = r'C:\Users\adambaig\Project\Mosaic_Esterhazy\perforations\data'
    pickDir = r'C:\Logiklyst\PSM\LogikData\MOC_EST\Picks\perforations'
    outputDir=r'C:\Users\adambaig\Project\Mosaic_Esterhazy\perforations\event_seeds'

    window = [-10, 50]
    getTrimmedSeedsFromArchive(dataDir,pickDir,outputDir,window=window,skipYearDir=True,nJobs=4,seedNameFormat='start_end')


def getWriteSeedForPickFile(pickFile,dataDir,outputDir,window,skipYearDir,seedNameFormat):
    oTime = UTCDateTime.strptime(pickFile.split('_')[1], '%Y%m%d.%H%M%S.%f.picks')
    startTime = oTime + window[0]
    endTime = oTime + window[1]
    if seedNameFormat=='start_end':
        outputName = os.path.join(outputDir,
                              startTime.strftime('%Y%m%d.%H%M%S.%f_') + endTime.strftime('%Y%m%d.%H%M%S.%f.seed'))
    elif seedNameFormat=='id_only':
        outputName = os.path.join(outputDir,str(int(pickFile.split('_')[0]))+'.seed')
    elif seedNameFormat=='matching':
        outputName = os.path.join(outputDir,pickFile.replace('.picks','.seed'))
    print(outputName)
    if os.path.isfile(outputName):
        print('Data already present for', pickFile)
        return
    print('Obtaining data for', pickFile)
    st = getStreamData_NocArchive(archiveDir=dataDir, startTime=startTime, endTime=endTime, skipYearDir=skipYearDir)
    if len(st)==0:
        print('empty stream')
        return
    st.write(outputName, format='MSEED')

def getTrimmedSeedsFromArchive(dataDir,pickDir,outputDir,window=[-10,10],skipYearDir=False,nJobs=1,seedNameFormat='start_end'):
    if not os.path.isdir(outputDir):
        os.makedirs(outputDir)
    pickFiles = np.array([f for f in os.listdir(pickDir) if f.endswith('.picks')])
    pickTimes = np.array([UTCDateTime.strptime(f.split('_')[1],'%Y%m%d.%H%M%S.%f.picks').timestamp for f in pickFiles])
    sortPicks = np.argsort(pickTimes)
    Parallel(n_jobs=nJobs)(delayed(getWriteSeedForPickFile)
                           (pickFile, dataDir, outputDir, window=window, skipYearDir=skipYearDir,seedNameFormat=seedNameFormat)
                       for pickFile in pickFiles[sortPicks])


if __name__ == '__main__':
    main()
