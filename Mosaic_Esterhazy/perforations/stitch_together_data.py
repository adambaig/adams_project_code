from pathlib import Path

from obspy import read, Stream

data_sets = {p.stem.split('_')[-1] for p in Path().joinpath('data').glob('*.seed')}

for data_set in data_sets:
	stream = Stream()
	for seed_file in Path('data').glob(f'*_{data_set}.seed'):
		stream += read(seed_file)
	stream.merge()
	stream.write(f'{data_set}.mseed', format='MSEED')
