from pathlib import Path
from obspy import read, Stream

base_dir = Path(r"C:\Logiklyst\PSM\LogikData\MOC_EST\MSEED\perforations")
st = Stream()
st1 = read(base_dir.joinpath("20230604.214500.000000_20230604.214600.000000.seed"))
st2 = read(base_dir.joinpath("perf_gr114.mseed"))

for tr in st1:
    st += tr
for tr in st2:
    st += tr

st.write(base_dir.joinpath("20230604.214500.000000_20230604.214600.000000.seed"), format="MSEED")
