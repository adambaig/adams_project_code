import csv
import json
from pathlib import Path


def csv_to_json(csv_file, json_file):
    with open(csv_file, "r") as file:
        csv_data = csv.reader(file)
        headers = next(csv_data)  # Get the header row

        json_data = {}
        for row in csv_data:
            row_dict = {headers[i]: row[i] == "TRUE" for i in range(1, len(headers))}
            json_data[row[0]] = row_dict
    with json_file.open("w") as f:
        json.dump(json_data, f)


if __name__ == "__main__":
    base_dir = Path(r"C:\Logiklyst\PSM\LogikData\MOC_EST")
    csv_file = base_dir.joinpath("nsl_health.csv")
    json_file = base_dir.joinpath("nsl_health.json")
    json_output = csv_to_json(csv_file, json_file)
