import glob
import os

import matplotlib.pyplot as plt
from read_inputs import read_station, read_horizon, read_velocity_model

from generalPlots import gray_background_with_grid

gr114 = read_station("GR114.csv")
bf16 = read_station("BF16.csv")

fig, ax = plt.subplots(1, 3, figsize=[18, 6], sharex=True, sharey=True)

horizon_files = glob.glob("Formation Tops//*.txt")
horizon_files[0], horizon_files[1] = horizon_files[1], horizon_files[0]
horizons = {}
for i_horizon, horizon_file in enumerate(horizon_files):
    horizon_name = (
        os.path.basename(horizon_file).split("Horizon ")[1].split(" depth")[0].strip()
    )
    horizons[horizon_name] = read_horizon(horizon_file)
    eastings = horizons[horizon_name]["eastings"]
    northings = horizons[horizon_name]["northings"]
    elevations = horizons[horizon_name]["elevations"]
    ax[i_horizon].set_aspect("equal")
    scatter = ax[i_horizon].scatter(
        eastings, northings, c=elevations, vmin=-1800, vmax=-1200
    )
    ax[i_horizon].set_title(horizon_name)
    ax[i_horizon].set_xlabel("easting")
    ax[i_horizon].plot(gr114["1.1"]["easting"], gr114["1.1"]["northing"], "kv")
    ax[i_horizon].plot(
        bf16["BF16_1_001"]["easting"], bf16["BF16_1_001"]["northing"], "kv"
    )

    # ax[i_horizon].plot(220000, 178000, '.', alpha=0)

    gray_background_with_grid(ax[i_horizon], grid_spacing=500)
ax[0].set_ylabel("northing")


bf16.keys()
bf16["BF16_1_001"]["northing"]
bf16["BF16_1_001"]["easting"]

gr114["1.1"]["northing"]
gr114["1.1"]["easting"]
86.6 * 2048 * 1000

cb = fig.colorbar(scatter, ax=ax)
cb.set_label("elevation above sea level (ft)")

fig.savefig("horizons.png")
