import glob
import json
import os

import matplotlib.pyplot as plt
import numpy as np
from read_inputs import read_station, read_auto_events
from sms_moment_tensor.MT_math import trend_plunge_to_unit_vector
from generalPlots import gray_background_with_grid

PI = np.pi
D2R = PI / 180.0
HODOGRAM_SCALE = 100
bf16 = read_station("BF16.csv")
events = read_auto_events(
    r"C:\Users\adambaig\Project\Mosaic_Esterhazy\hodograms\autos_plus_azimuth_all_rotate.csv"
)
# rekey the station dictionary
stations = {
    (2 * (int(k.split("_")[1]) - 1) + int(k.split("_")[2])): v for k, v in bf16.items()
}
for station in stations.values():
    station["elevation"] = -station["tvdss"]


def rotate_to_plane(angle_degrees):
    angle = angle_degrees * D2R
    return [
        [np.sin(angle), -np.cos(angle), 0],
        [np.cos(angle), np.sin(angle), 0],
        [0, 0, 1],
    ]


for hodogram_json in glob.glob(os.path.join("hodograms", "hodogram_jsons", "*.json")):
    with open(hodogram_json) as f:
        hodograms = json.load(f)
    if not hodograms:
        continue
    for hodogram in hodograms.values():
        hodogram["unit vector"] = trend_plunge_to_unit_vector(
            hodogram["trend"], hodogram["plunge"]
        )
    event_id = os.path.basename(hodogram_json).split("_")[0]

    total_trend = hodograms["event"]["trend"]
    if event_id not in events:
        print(f"{event_id} not found!")
        continue
    event = events[event_id]

    rotation_matrix = rotate_to_plane(total_trend)
    direct_sum, flip_sum = 0, 0
    for station_int, station in stations.items():
        station_str = str(station_int).zfill(2)

        if station_str in hodograms:
            hodogram = hodograms[station_str]
            unit_vector = hodogram["unit vector"]
            rotated_vector = rotation_matrix @ unit_vector
            station_event_2vector = [0, station["elevation"]]
            direct_unit_2vector = [
                event["radial"],
                -station["elevation"] - event["depth"],
            ]
            direct_unit_2vector /= np.linalg.norm(direct_unit_2vector)
            flip_unit_2vector = [
                -event["radial"],
                -station["elevation"] - event["depth"],
            ]
            flip_unit_2vector /= np.linalg.norm(flip_unit_2vector)

            direct_sum += rotated_vector[0::2] @ direct_unit_2vector
            flip_sum += rotated_vector[0::2] @ flip_unit_2vector
    if flip_sum > direct_sum:
        event["flip"] = True
    else:
        event["flip"] = False
    event["flip sum"] = flip_sum
    event["direct sum"] = direct_sum
    event["n hodograms"] = len(hodograms)


center = {
    "e": np.average([v["easting"] for v in stations.values()]),
    "n": np.average([v["northing"] for v in stations.values()]),
}


looked_at_events = {k: v for k, v in events.items() if "flip" in v}
for event in looked_at_events.values():
    flip = -1 if event["flip"] else 1
    event["easting"] = center["e"] + flip * event["radial"] * np.sin(
        D2R * event["azimuth"]
    )
    event["northing"] = center["n"] + flip * event["radial"] * np.cos(
        D2R * event["azimuth"]
    )
    if event["flip"]:
        event["discrimanant"] = (event["flip sum"] - event["direct sum"]) / event[
            "n hodograms"
        ]
    else:
        event["discrimanant"] = (
            -(event["flip sum"] - event["direct sum"]) / event["n hodograms"]
        )


with open("hodograms//flipped_events.csv", "w") as g:
    g.write(
        ",".join(
            [
                "",
                "# Event ID",
                "UTC_Datetime",
                "UTC_timestamp",
                "Radial",
                "Junk",
                "Depth",
                "SNR",
                "rawAmp",
                "Azimuth",
                "Linearity",
                "meanAzimuth",
                "meanLineality",
                "StdAz",
                "nHodograms",
                "isFlipped",
                "flippedEasting",
                "flippedNorthing",
                "hodogramDiscriminant\n",
            ]
        )
    )
    for event_id, event in looked_at_events.items():
        g.write(str(event["trigger id"]) + ",")
        g.write(event_id + ",")
        g.write(str(event["datetime"]) + ",")
        g.write(str(event["timestamp"]) + ",")
        g.write(str(event["radial"]) + ",")
        g.write("0,")
        g.write(str(event["depth"]) + ",")
        g.write(str(event["snr"]) + ",")
        g.write(str(event["raw_amp"]) + ",")
        g.write(str(event["azimuth"]) + ",")
        g.write(str(event["linearity"]) + ",")
        g.write(str(event["mean azimuth"]) + ",")
        g.write(str(event["mean linearity"]) + ",")
        g.write(str(event["std azimuth"]) + ",")
        g.write(str(event["n hodograms"]) + ",")
        g.write(str(event["flip"]) + ",")
        g.write(str(event["easting"]) + ",")
        g.write(str(event["northing"]) + ",")
        g.write(str(event["discrimanant"]) + "\n")

plot_events = {
    k: v
    for k, v in looked_at_events.items()
    if v["n hodograms"] > 4 and v["discrimanant"] > 1.0
}

fig, ax = plt.subplots(figsize=[8, 8])
ax.set_aspect("equal")
ax.scatter(
    [v["easting"] for v in plot_events.values()],
    [v["northing"] for v in plot_events.values()],
    c=np.log10([v["snr"] for v in plot_events.values()]),
    edgecolor="0.2",
)
ax.plot(
    center["e"], center["n"], "v", color="0.3", markeredgecolor="0.1", markersize=10
)
gray_background_with_grid(ax, grid_spacing=100)
fig.savefig("locations_flipped_n_hodo_gt_5.png")
