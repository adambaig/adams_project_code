from matplotlib import pyplot as plt

from nmxseis.interact.athena import AthenaClient

from obspy import UTCDateTime

starttime = UTCDateTime(2023, 4, 1)
endtime = UTCDateTime(2023, 5, 1)


events =