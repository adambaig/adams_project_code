from matplotlib import pyplot as plt
from NocMeta.Meta import NOC_META
from nmxseis.interact.athena import AthenaClient
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick import Pick
from nmxseis.model.seismic_event import EventOrigin
from obspy import UTCDateTime

from athena_queries import get_arrivals_and_location
from plotting import gray_background_with_grid
from read_inputs import get_station_enus, INV, REPROJECT

START_TIME = UTCDateTime(2023, 1, 1)
END_TIME = UTCDateTime(2223, 4, 4, 16, 0)

athena_code = "MOC_EST"
athena_client = AthenaClient(
    base_url=rf'https://{NOC_META[athena_code]["athIP"]}', api_key=NOC_META[athena_code]["athApi"]
)

event_list = athena_client.query_gql(get_arrivals_and_location(START_TIME, END_TIME))
station_enus = get_station_enus()

def arrivals_to_picks(arrivals_dict):
    picks = []
    for arrival in arrivals_dict:
        phase = Phase((pick := arrival['pick'])['phase'])
        time = UTCDateTime(pick['time'])
        nsl = NSL(
			loc = cha['locationCode'] if (cha := pick['channel'])['locationCode']!='--' else '',
			sta = (sta:=cha['station'])['stationCode'],
			net = sta['network']['networkCode']
		)
        picks.append(Pick(phase=phase, time=time, nsl=nsl))
    return picks

def unique_stas(picks):
    return {
        p.nsl.sta if p.nsl.sta[:3] != '122' else 'GR122' for p in manual_picks
            if p.nsl.sta not in ['BF16', 'GR114']
            }

def n_stations(picks):
    return len(unique_stas(picks))


events = {}
for event in event_list['eventList']['events']:
    pref_origin = event['preferredOrigin']
    origin = EventOrigin(
        lat=pref_origin['latitude'],
        lon=pref_origin['longitude'],
        time=UTCDateTime(pref_origin['time']),
        depth_m=1000*pref_origin['depth']
    )
    manual_picks = arrivals_to_picks(pref_origin['arrivals'])
    events[event['id']] = {
        'enu': origin.enu(REPROJECT),
        'n_stations': n_stations(manual_picks),
        'min_pick_sta': min(manual_picks).nsl.sta
    }

three_sta_events = {k:v for k,v in events.items() if v['n_stations']>2}


post_events = {k:v for k,v in events.items() if v['min_pick_sta'] in ['BF16', 'GR114'] or v['n_stations']>2}

for i_dist,distibution in enumerate([events, three_sta_events, post_events]):
    fig, ax = plt.subplots()
    ax.set_aspect('equal')
    scatter = ax.scatter(
        [e['enu'][0] for e in distibution.values()],
        [e['enu'][1] for e in distibution.values()],
        c = [e['n_stations'] for e in distibution.values()],
        cmap='turbo',
        edgecolor='0.1',
        vmin=1
    )
    ax.plot(
        [s[0] for s in station_enus.values()],
        [s[1] for s in station_enus.values()],
        'v',
        mec='0.1',
        color='0.5'
    )
    cb = fig.colorbar(scatter)
    cb.set_label('number of stations')
    gray_background_with_grid(ax, grid_spacing=5000)
    fig.savefig(f'n_stations_{i_dist}.png')

