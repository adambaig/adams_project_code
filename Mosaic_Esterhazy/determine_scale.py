from numpy import array
from numpy.linalg import norm

coords_utm = {
    'CP1': array([5617133.740, 272783.179]),
    'CP2': array([5635141.326, 283174.329])
}

coords_k1 = {
    'CP1': array([57133.702, -16651.898]),
    'CP2': array([117381.233, 15344.431])
}

coords_k3 = {
    'CP1': array([57132.73, 183356.326]),
    'CP2': array([117370.614, 15344.431])
}

dist_utm = norm(coords_utm['CP1'] - coords_utm['CP2'])
dist_k1 = norm(coords_k1['CP1'] - coords_k3['CP2'])
dist_k3 = norm(coords_k1['CP1'] - coords_k3['CP2'])

scale_k1 = dist_utm / dist_k1
scale_k3 = dist_utm / dist_k3
