from pathlib import Path

from heapq import merge
import numpy as np

PPSD_DIR = Path('PPSD_DataDetailed')
FMIN, FMAX = 20, 200
SAMPLE_RATE = 0.002
PI = np.pi

def read_PPSD(PPSD_file, f_min, f_max):
	f = open(PPSD_file)
	head = f.readline()
	lines = f.readlines()
	f.close()
	n_lines = len(lines)
	freq, PSD_dB = np.zeros(n_lines), np.zeros(n_lines)
	for i_line, line in enumerate(lines):
		freq[i_line], PSD_dB[i_line] = [float(s) for s in line.split(",")[:2]]
	velocity_power_density = 10 ** (PSD_dB / 10) / (2 * PI * freq) ** 2
	return np.sqrt(integrate_after_interpolate(velocity_power_density, freq, f_min, f_max))


def integrate_after_interpolate(y, x, x1, x2):
	x_with_endpoints = np.unique(list(merge(x, [x1, x2])))
	i_sort = np.argsort(x)
	y_interp = np.interp(x_with_endpoints, x[i_sort], y[i_sort])
	i1 = np.where(abs(x_with_endpoints - x1) < 1e-10)[0][0]
	i2 = np.where(abs(x_with_endpoints - x2) < 1e-10)[0][0]
	return np.trapz(y_interp[i1: i2 + 1], x_with_endpoints[i1: i2 + 1])


def read_and_process_ppsds():
	noise_levels = {}
	ppsd_files = PPSD_DIR.glob('MOC_EST*.csv')
	for ppsd_file in ppsd_files:
		channel = ppsd_file.stem.split('_')[2]
		noise_levels[channel] = {
			'band_limited': (bl_noise := read_PPSD(ppsd_file, FMIN, FMAX)),
			'extrapolated': bl_noise / np.sqrt(SAMPLE_RATE * (FMAX - FMIN))
		}
	with open(PPSD_DIR.joinpath('Mosaic_noise_levels.csv'), 'w') as f:
		f.write(f'channel,noise between {FMIN} and {FMAX} Hz (nm/s),'
		        'extrapolated to full band (nm/s)\n')
		for channel, noise_level in noise_levels.items():
			f.write(f'{channel},{noise_level["band_limited"]*1e9:.2f},'
			        f'{noise_level["extrapolated"]*1e9:.2f}\n')



if __name__ == "__main__":
	read_and_process_ppsds()
