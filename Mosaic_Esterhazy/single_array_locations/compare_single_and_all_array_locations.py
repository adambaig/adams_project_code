import json
from pathlib import Path

import matplotlib.pyplot as plt
from nmxseis.model.seismic_event import EventOrigin
from numpy import array
from obspy import UTCDateTime

from plotting import gray_background_with_grid
from read_inputs import get_station_enus, REPROJECT

LOGIKDATA = Path(r'C:\Logiklyst\PSM\LogikData')

single_array_dir = LOGIKDATA.joinpath('MOC_EST', 'Located', 'Closest Single Array')
all_array_dir = LOGIKDATA.joinpath('MOC_EST', 'Located', 'All Arrays')
refined_dir = LOGIKDATA.joinpath('MOC_EST', 'Located', 'Closest Array Refined')

all_array_locs = {}
single_array_locs = {}
refined_locs = {}

for locs, dir in zip([all_array_locs, single_array_locs, refined_locs], [all_array_dir, single_array_dir, refined_dir]):
	for event_json in dir.glob('*.json'):
		event_dict = json.loads(event_json.read_text())
		event_id = event_json.stem
		locs[event_id] = {
			'origin': EventOrigin(
					lat = (origin := event_dict['event']['origin'])['latitude'],
					lon = origin['longitude'],
					depth_m = origin['depth']*1000,
					time = UTCDateTime(origin['time'])
				),
			'rms': origin['timeRms']
		}

station_enus = get_station_enus()
single_array_enus = array([ev['origin'].enu(REPROJECT) for ev in single_array_locs.values()])
all_array_enus = array([ev['origin'].enu(REPROJECT) for ev in all_array_locs.values()])
refined_enus = array([ev['origin'].enu(REPROJECT) for ev in refined_locs.values()])
single_array_matching_enus = array([ev['origin'].enu(REPROJECT) for ev_id, ev in single_array_locs.items() if ev_id in refined_locs])
single_array_kwargs = {
	'marker': 'o', 'color': 'firebrick', 'alpha': 0.5, 'mec': '0.2', 'zorder': 21, 'linewidth': 0
}
all_array_kwargs = {**single_array_kwargs, 'color': 'darkgoldenrod', 'zorder': 20}
refined_array_kwargs = {**single_array_kwargs, 'color': 'royalblue', 'zorder': 22}
station_plan_kwargs= {'marker':'v', 'color': '0.4', 'mec': '0.2'}
station_depth_kwargs={**station_plan_kwargs, 'marker':'>'}

fig,(ax_p, ax_d) = plt.subplots(1,2, figsize=[12, 6])
ax_p.plot(single_array_enus[:,0], single_array_enus[:,1],**single_array_kwargs , label='closest array')
ax_p.plot(all_array_enus[:,0], all_array_enus[:,1],**all_array_kwargs, label='all arrays')

ax_d.plot(single_array_enus[:,2], single_array_enus[:,1],**single_array_kwargs )
ax_d.plot(all_array_enus[:,2], all_array_enus[:,1],**all_array_kwargs )

figr,(ax_rp, ax_rd) = plt.subplots(1,2, figsize=[12, 6])
ax_rp.plot(single_array_matching_enus[:,0], single_array_matching_enus[:,1],**single_array_kwargs , label='closest array')
ax_rp.plot(refined_enus[:,0], refined_enus[:,1],**refined_array_kwargs, label='refined')

ax_rd.plot(single_array_matching_enus[:,2], single_array_matching_enus[:,1],**single_array_kwargs )
ax_rd.plot(refined_enus[:,2], refined_enus[:,1],**refined_array_kwargs )

for ax_pair in [(ax_p, ax_d), (ax_rp, ax_rd)]:
	for ax in ax_pair[0], ax_pair[1]:
		ax.set_aspect('equal')
		gray_background_with_grid(ax, grid_spacing=200)

	p_xlim, p_ylim = ax_pair[0].get_xlim(), ax_pair[0].get_ylim()
	d_xlim, d_ylim = ax_pair[1].get_xlim(), ax_pair[1].get_ylim()

	for station, enu in station_enus.items():
		ax_pair[0].plot(enu[0], enu[1], **station_plan_kwargs, zorder=-int(station.loc) if station.loc else -20)
		ax_pair[1].plot(enu[2], enu[1], **station_depth_kwargs, zorder=-int(station.loc) if station.loc else -20)

	ax_pair[0].set_xlim(*p_xlim)
	ax_pair[0].set_ylim(*p_ylim)
	ax_pair[1].set_xlim(*d_xlim[::-1])
	ax_pair[1].set_ylim(*d_ylim)

	ax_pair[0].set_xlabel('Easting $\longrightarrow$')
	ax_pair[0].set_ylabel('Northing $\longrightarrow$')
	ax_pair[1].set_xlabel('Depth $\longrightarrow$')
	ax_pair[0].legend()

fig.savefig('single_vs_all.png')
figr.savefig('single_vs_refined.png')