import json
from pathlib import Path

import matplotlib.pyplot as plt
from numpy import linspace

geophone_dir = Path(r"C:\Users\adambaig\logikdata\MOC_EST\Located\interpolated_bh_sparta")
broadband_dir = Path(r"C:\Users\adambaig\logikdata\MOC_EST\Located\interpolated_surface_sparta")
geophone, broadband = {}, {}
event_ids = [p.stem for p in geophone_dir.glob("*.json")]
for event_id in event_ids:
    broadband_json = broadband_dir.joinpath(f"{event_id}.json")
    geophone_json = geophone_dir.joinpath(f"{event_id}.json")
    geophone[event_id] = json.loads(geophone_json.read_text())
    geophone[event_id]["event"]["origin"]["mw"] = (
        geophone[event_id]["event"]["origin"]["magnitudes"][0]["value"]
        if "magnitudes" in geophone[event_id]["event"]["origin"]
        else None
    )
    geophone[event_id]["event"]["origin"]["mw author"] = (
        geophone[event_id]["event"]["origin"]["magnitudes"][0]["author"]
        if "magnitudes" in geophone[event_id]["event"]["origin"]
        else None
    )
    geophone[event_id]["event"]["origin"]["n_arrivals"] = len(
        geophone[event_id]["event"]["origin"]["arrivals"]
    )
    broadband[event_id] = json.loads(broadband_json.read_text())
    broadband[event_id]["event"]["origin"]["mw"] = (
        broadband[event_id]["event"]["origin"]["magnitudes"][0]["value"]
        if "magnitudes" in broadband[event_id]["event"]["origin"]
        else None
    )
    broadband[event_id]["event"]["origin"]["mw author"] = (
        broadband[event_id]["event"]["origin"]["magnitudes"][0]["author"]
        if "magnitudes" in broadband[event_id]["event"]["origin"]
        else None
    )
    broadband[event_id]["event"]["origin"]["n_arrivals"] = geophone[event_id]["event"]["origin"][
        "n_arrivals"
    ]


def make_comparison_plot(key, log=False, **kwargs):
    geophone_dict = {
        event_id: geophone[event_id]["event"]["origin"][key]
        for event_id in event_ids
        if key in geophone[event_id]["event"]["origin"]
        and geophone[event_id]["event"]["origin"][key] is not None
        and "borehole" in geophone[event_id]["event"]["origin"]["mw author"]
    }
    broadband_dict = {
        event_id: broadband[event_id]["event"]["origin"][key]
        for event_id in event_ids
        if key in broadband[event_id]["event"]["origin"]
        and broadband[event_id]["event"]["origin"][key] is not None
        and "surface" in broadband[event_id]["event"]["origin"]["mw author"]
    }

    common_ids = set(broadband_dict) & set(geophone_dict)
    param_interp = [geophone[event_id]["event"]["origin"][key] for event_id in common_ids]
    param_broadband = [broadband[event_id]["event"]["origin"][key] for event_id in common_ids]
    fig, ax = plt.subplots()
    ax.set_facecolor("0.96")
    if log:
        ax.loglog(param_broadband, param_interp, **kwargs)
    else:
        ax.plot(param_broadband, param_interp, **kwargs)
    max_ax = max(*ax.get_xlim(), *ax.get_ylim())
    min_ax = min(*ax.get_xlim(), *ax.get_ylim())
    if log:
        ax.loglog([min_ax, max_ax], [min_ax, max_ax], "k:", zorder=1, lw=1)
    else:
        ax.plot([min_ax, max_ax], [min_ax, max_ax], "k:", zorder=1, lw=1)
    ax.set_xlim([min_ax, max_ax])
    ax.set_ylim([min_ax, max_ax])

    ax.set_xlabel("broadband dataset")
    ax.set_ylabel("geophone dataset")
    ax.set_title(key)
    ax.set_aspect("equal")
    ax.grid(True)

    f_hist, (a_hist_int, a_hist_fly) = plt.subplots(2, sharex=True, sharey=True)
    bins = linspace(min_ax, max_ax, 100)
    a_hist_fly.hist(param_broadband, bins=bins, facecolor=kwargs["c"])
    a_hist_int.hist(param_interp, bins=bins, facecolor=kwargs["c"])
    a_hist_fly.set_xlabel(key)
    a_hist_fly.set_ylabel("on the fly")
    a_hist_int.set_ylabel("interpolated")
    return fig, f_hist


def make_mw_scatter_color_by_sr(**kwargs):
    geophone_dict = {
        event_id: geophone[event_id]["event"]["origin"]["mw"]
        for event_id in event_ids
        if "mw" in geophone[event_id]["event"]["origin"]
        and geophone[event_id]["event"]["origin"]["mw"] is not None
        and "sourceRadius" in geophone[event_id]["event"]["origin"]
        and geophone[event_id]["event"]["origin"]["sourceRadius"] is not None
        and "borehole" in geophone[event_id]["event"]["origin"]["mw author"]
    }
    broadband_dict = {
        event_id: broadband[event_id]["event"]["origin"]["mw"]
        for event_id in event_ids
        if "mw" in broadband[event_id]["event"]["origin"]
        if "mw" in broadband[event_id]["event"]["origin"]
        and broadband[event_id]["event"]["origin"]["mw"] is not None
        and "sourceRadius" in broadband[event_id]["event"]["origin"]
        and broadband[event_id]["event"]["origin"]["sourceRadius"] is not None
        and "surface" in broadband[event_id]["event"]["origin"]["mw author"]
    }
    common_ids = set(broadband_dict) & set(geophone_dict)
    param_geophone = [geophone[event_id]["event"]["origin"]["mw"] for event_id in common_ids]
    param_broadband = [broadband[event_id]["event"]["origin"]["mw"] for event_id in common_ids]
    color_param = [
        (
            broadband[event_id]["event"]["origin"]["sourceRadius"]
            / geophone[event_id]["event"]["origin"]["sourceRadius"]
        )
        for event_id in common_ids
    ]

    fig, ax = plt.subplots()
    ax.set_facecolor("0.96")
    scatter = ax.scatter(param_broadband, param_geophone, c=color_param, **kwargs)
    max_ax = max(*ax.get_xlim(), *ax.get_ylim())
    min_ax = min(*ax.get_xlim(), *ax.get_ylim())
    ax.plot([min_ax, min_ax], [max_ax, max_ax], "k:", zorder=1, lw=2)
    ax.set_xlim([min_ax, max_ax])
    ax.set_ylim([min_ax, max_ax])
    ax.grid(True)
    ax.set_xlabel("broadband dataset mw")
    ax.set_ylabel("geophone dataset mw")
    ax.plot([min_ax, max_ax], [min_ax, max_ax], "k:", zorder=1, lw=1)
    ax.set_title("mw and source radii")
    ax.set_aspect("equal")
    cbar = fig.colorbar(scatter)
    cbar.set_label("broadband/geophone source radius")
    return fig


# fig_mwc = make_comparison_scatter("mw", "n_arrivals", edgecolor="0.2", cmap="turbo", vmin=1, alpha=0.4)


# fig_rms, hist_rms = make_comparison_plot(
#     "timeRms", marker="o", c="firebrick", mec="0.2", linestyle="None", alpha=0.5
# )
fig_mw, hist_mw = make_comparison_plot(
    "mw", marker="o", c="royalblue", mec="0.2", linestyle="None", alpha=0.5
)

fig_sr, hist_sr = make_comparison_plot(
    "sourceRadius",
    log=True,
    marker="o",
    c="forestgreen",
    mec="0.2",
    linestyle="None",
    alpha=0.5,
)
fig_mw.savefig("bb_vs_geop_mw.png")
fig_sr.savefig("bb_vs_geop_sr.png")
fig_scatter = make_mw_scatter_color_by_sr(vmin=1, vmax=10, cmap="turbo", edgecolor="0.2")
# fig_depth, hist_depth = make_comparison_plot(
#     "depth", marker="o", c="forestgreen", mec="0.2", linestyle="None", alpha=0.5
# )
#
# fig_rms.savefig("rms_comparison.png")
# hist_rms.savefig("rms_hist.png")
# fig_mw.savefig("mw_comparison.png")
# hist_mw.savefig("mw_hist.png")
# fig_depth.savefig("depth_comparison.png")
# hist_depth.savefig("depth_hist.png")
