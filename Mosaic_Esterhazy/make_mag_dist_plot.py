import matplotlib.pyplot as plt
from NocMeta.Meta import NOC_META
from nmxseis.interact.athena import AthenaClient
from nmxseis.model.hodogram import VerticalArray
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.formulas import calculate_source_radius_from_stress_drop, calculate_moment_from_stress_drop
from numpy.linalg import norm
from obspy import UTCDateTime

from athena_queries import get_location_and_source_parameters
from read_inputs import get_station_enus, INV, REPROJECT

START_TIME = UTCDateTime(2023, 4, 1)
END_TIME = UTCDateTime(2223, 5, 11, 0, 0)

athena_code = "MOC_EST"
athena_client = AthenaClient(
    base_url=rf'https://{NOC_META[athena_code]["athIP"]}', api_key=NOC_META[athena_code]["athApi"]
)

event_list = athena_client.query_gql(get_location_and_source_parameters(START_TIME, END_TIME))
station_enus = get_station_enus()

events = {}
for event in event_list['eventList']['events']:
    pref_origin = event['preferredOrigin']
    magnitudes = pref_origin['magnitudes']
    events[event['id']] = {
        'origin': (origin:=EventOrigin(
            lat=pref_origin['latitude'],
            lon=pref_origin['longitude'],
            time=UTCDateTime(pref_origin['time']),
            depth_m=1000*pref_origin['depth']
        )),
        'enu': origin.enu(REPROJECT),
        'seismic_moment': pref_origin['seismicMoment'],
        'stress_drop': pref_origin['stressDrop']*1e6 if pref_origin['stressDrop'] is not None else None,
        'energy': pref_origin['energy'],
        'apparent_stress': pref_origin['apparentStress']*1e6 if pref_origin['apparentStress'] is not None else None,
        'source_radius': pref_origin['sourceRadius']*1000 if pref_origin['sourceRadius'] is not None else None,
        'Mw': mags[0]['value'] if len(mags := [m for m in magnitudes if m['magnitudeType'] == 'Mw']) > 0 else None,
        'sp_quality': (sp_quality := mags[0]['author'] if len(mags) > 0 else None),
        'is_surface': (is_surface := True if sp_quality is not None and "surface" in sp_quality.lower() else False),
        'is_borehole': (False if sp_quality is None else not(is_surface)),
        'quality': None if sp_quality is None else [
            q for q in ['A', 'B', 'C', 'distant'] if q in sp_quality.split('Auto')[1]
        ][0]
    }

events_with_mw = {k:v for k,v in events.items() if v['Mw'] is not None}

for array in ['BF16', 'GR114']:
    vertical_array = VerticalArray([k for k in station_enus.keys() if k.sta==array], INV, REPROJECT)
    fig,ax = plt.subplots()
    ax.plot(
        [norm(e['enu'][:2] - vertical_array.en) for e in events_with_mw.values()],
        [e['Mw'] for e in events_with_mw.values()],
        '.'
    )
    ax.set_title(array)
    ax.set_xlabel('distance (m)')
    ax.set_ylabel('Mw')
    ax.set_xlim(0,5000)
    fig.show()


fig, ax = plt.subplots()
symbol = {
    'borehole', 'o',
    'surface', 's'
}
color = {
    'A': 'forestgreen', 'B': 'royalblue', 'C': 'darkgoldenrod'
}
zorder = {
    'A': 9, 'B': 8, 'C': 7
}

for event in events.values():
    if event['quality']=='distant': continue
    if event['quality']=='C': continue
    if event['source_radius'] is None: continue
    ax.loglog(
        event['source_radius'],
        event['seismic_moment'],
        'o' if event['is_borehole'] else 's',
        color=color[event['quality']],
        mec='0.2',
        zorder=zorder[event['quality']],
        label = ''
    )
ax.set_facecolor('0.96')
ax.set_ylabel('moment (Nm)')
ax.set_xlabel('source radius (m)')


def get_xlim_and_ylim(axis: plt.Axes) -> tuple:
    return (*axis.get_xlim(), *axis.get_ylim())

def set_xlim_and_ylim(axis: plt.Axes, xlim_and_ylim: tuple) -> None:
    ax.set_xlim(xlim_and_ylim[:2])
    ax.set_ylim(xlim_and_ylim[2:])

def add_scaling_line(axis: plt.Axes, stress_drop: float, **kwargs) -> None:
    sr_min, sr_max,mo_min, mo_max = get_xlim_and_ylim(axis)
    mo_from_sr_min = calculate_moment_from_stress_drop(sr_min, stress_drop)
    mo_from_sr_max = calculate_moment_from_stress_drop(sr_max, stress_drop)
    sr_from_mo_min = calculate_source_radius_from_stress_drop(mo_min, stress_drop)
    sr_from_mo_max = calculate_source_radius_from_stress_drop(mo_max, stress_drop)
    if mo_min<mo_from_sr_max:
        mo1, sr1 = mo_min, sr_from_mo_min
    else:
        mo1, sr1 = mo_from_sr_min, sr_min
    if mo_max>mo_from_sr_max:
        mo2, sr2 = mo_max, sr_from_mo_max
    else:
        mo2, sr2 = mo_from_sr_max, sr_max
    axis.loglog([sr1, sr2],[mo1, mo2],  **kwargs)

xlim_and_ylim = get_xlim_and_ylim(ax)
for stress_drop in [1e2, 1e3, 1e4, 1e5,1e6, 1e7]:
    add_scaling_line(ax, stress_drop,zorder=-2)

set_xlim_and_ylim(ax, xlim_and_ylim)
