from pathlib import Path

from nmxseis.model.phase import Phase
from nmxseis.model.pick import Pick
from nmxseis.model.pick_set import PickSet
from obspy import UTCDateTime

from read_inputs import get_station_enus


time_ref = UTCDateTime(2023, 6, 4, 21, 45)

picks = []
for station in get_station_enus():
    picks.append(Pick(time=time_ref + 0.1, nsl=station, phase=Phase.P))


pick_set = PickSet(picks=picks)
perf_pick_file = Path(
    r"C:\Users\adambaig\Project\Mosaic_Esterhazy\0000222680_20230604.214500.000000.picks"
)
pick_set.dump(perf_pick_file)
