from pathlib import Path


from nmxseis.interact.fdsn import FDSNClient
from nmxseis.model.nslc import NSLC
from nmxseis.util.obspy_tools import is_data_channel
from obspy import read_inventory, UTCDateTime, Stream


BASE_PATH = Path()


start_time = UTCDateTime(2023, 7, 29, 19, 45, 32)
end_time = UTCDateTime(2023, 7, 29, 19, 45, 46)

IP = "52.11.206.250:8081"
inv = read_inventory(BASE_PATH.joinpath("MOC_EST_Full.xml"))
nslcs = [
    nslc
    for nslc in NSLC.iter_inv(inv)
    if nslc.cha_info.is_active(time=start_time) and is_data_channel(nslc.cha_info) and nslc.sta=='GR114'
]

client = FDSNClient("172.20.0.67:8081")

stream = client.get_waveforms(nslcs, start_time, end_time)
stream.remove_response(inv)