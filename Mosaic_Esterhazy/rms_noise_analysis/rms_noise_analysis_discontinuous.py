import argparse
from datetime import datetime as dt
from pathlib import Path

import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
from nmxseis.interact.fdsn import FDSNClient
from nmxseis.model.nslc import NSLC
from nmxseis.util.obspy_tools import is_data_channel
from obspy import read_inventory, UTCDateTime, Stream

parser = argparse.ArgumentParser(description='script for Mosaic rms noise analysis')
parser.add_argument('-x', '--station_xml', help='Path to the Station XML')
parser.add_argument('-s', '--start_time', help='start time string expressed as %Y%m%d%M%H%S')
parser.add_argument('-e', '--end_time', help='end time string expressed as %Y%m%d%M%H%S')
parser.add_argument('-o', '--out_path', help='Path to output')

BASE_PATH = Path()

args = parser.parse_args()


start_time = UTCDateTime.strptime(args.start_time, '%Y%m%d%M%H%S')
end_time = UTCDateTime.strptime(args.end_time, '%Y%m%d%M%H%S')

IP = '52.11.206.250:8081'
inv = read_inventory(BASE_PATH.joinpath(args.station_xml))
nslcs = [nslc for nslc in NSLC.iter_inv(inv) if
         nslc.cha_info.is_active(time=start_time) and is_data_channel(nslc.cha_info)]
client = FDSNClient(IP)
clients = {
	'bf16': FDSNClient('172.20.0.3:8081'),
	'gr110': FDSNClient('172.20.0.35:8081'),
	'gr114': FDSNClient('172.20.0.67:8081')
}
station_groups = {
	'broadband surface': {
		'filter_params': {'type': 'bandpass', 'freqmin': 0.5, 'freqmax': 25, 'zerophase': True},
		'include_nsls': "MO.S*.*.*",
		'client': client
	},
	'K3_in_mine': {
		'filter_params': {'type': 'bandpass', 'freqmin': 10, 'freqmax': 30, 'zerophase': True},
		'include_nsls': "MO.K3*.*.*",
		'client': client
	},
	'GR122': {
		'filter_params': {'type': 'highpass', 'freq': 15, 'zerophase': True},
		'include_nsls': "MO.122*.*.*",
		'client': client
	},
	'GR114': {
		'filter_params': {'type': 'highpass', 'freq': 15, 'zerophase': True},
		'include_nsls': "MO.GR114.*.*",
		'client': clients['gr114']
	},
	'GR110': {
		'filter_params': {'type': 'highpass', 'freq': 15, 'zerophase': True},
		'include_nsls': "MO.GR110.*.*",
		'client': clients['gr110']
	},
	'BF16': {
		'filter_params': {'type': 'highpass', 'freq': 15, 'zerophase': True},
		'include_nsls': "MO.BF16.*.*",
		'client': clients['bf16']
	}
}

win_starts = np.arange(start_time, end_time, sample_interval_s)

fig, ax, gr_nslcs, noise_rms_ns = {}, {}, {}, {}
for group, params in station_groups.items():
	fig[group], ax[group] = plt.subplots()
	gr_nslcs[group] = [n for n in nslcs if n.match_wildcard(params['include_nsls'])]
	gr_nslcs[group].sort(key=lambda x: x.__str__(), reverse=True)
	noise_rms_ns[group] = np.ma.zeros([len(win_starts), len(gr_nslcs[group])])

for i_win, win_start in enumerate(win_starts):
	stream = client.get_waveforms(nslcs, win_start, win_start + win_length_s)
	for group, params in station_groups.items():
		if params['client'] == client:
			gr_stream = Stream()
			for nslc in gr_nslcs[group]:
				gr_stream += nslc.select_from_stream(stream)
		else:
			gr_stream = params['client'].get_waveforms(gr_nslcs[group], win_start, win_start + win_length_s)
		gr_stream.filter(**params['filter_params']).taper(0.05).trim(
			win_start + win_length_s * 0.05, win_start + win_length_s - win_length_s * 0.05
		)
		for i_tr, nslc in enumerate(gr_nslcs[group]):
			tr = nslc.select_from_stream(stream).copy().remove_response(inv)
			if len(tr)>0:
				noise_rms_ns[group][i_win, i_tr] = np.sqrt(np.var(tr[0].data)) * 1e9
		seed_path = BASE_PATH.joinpath(
			'rms_noise_analysis',
			'seed',
			f'{group.replace(" ","_")}_{win_start.strftime("%Y%m%d_%H%M%S")}_'
			f'{(win_start + win_length_s).strftime("%Y%m%d_%H%M%S")}.mseed')
		gr_stream.write(str(seed_path), format='MSEED', encoding='FLOAT64')

p_color_kwargs = {'vmax': 5, 'vmin': 1, 'cmap': 'nipy_spectral'}
for group in station_groups:
	fig[group], ax[group] = plt.subplots()
	masked_noise = np.ma.masked_less(noise_rms_ns[group], 1e-7)
	noise_plot = ax[group].pcolor(
		[dt.strptime(t.strftime(format := '%Y%m%d%H%M'), format) for t in win_starts],
		range(len(gr_nslcs[group])),
		np.ma.log10(masked_noise.T),
		**p_color_kwargs
	)
	ax[group].set_title(f'{group} {start_time.strftime("%b %d")} to {end_time.strftime("%b %d")}')
	cb = fig[group].colorbar(noise_plot)
	cb.set_label('noise level (nm/s)')
	cb.set_ticks(range(1, 6))
	cb.set_ticklabels(['1', '10', '100', '1e3', '1e4'])
	ax[group].xaxis.set_major_formatter(md.DateFormatter("%H:%M"))
	ax[group].set_xlabel('UTC time')
	if len(gr_nslcs)<15:
		gr_stas = list({f'{n.sta}.{n.loc}' for n in gr_nslcs[group]})
	else:
		gr_stas = list({f'{n.loc}' for n in gr_nslcs[group]})
	gr_stas.sort(reverse=True)
	ax[group].set_yticklabels([])
	for i_cha, n in enumerate(gr_nslcs[group]):
		ax[group].text(-0.05, (0.5+i_cha)/len(gr_nslcs[group]), n.cha, ha='right', va='center', transform=ax[group].transAxes)
	for i_sta, sta in enumerate(gr_stas):
		ax[group].text(-0.15, (1.5+3*i_sta)/len(gr_nslcs[group]), sta, rotation=90, va='center',  transform=ax[group].transAxes)
	fig[group].savefig(BASE_PATH.joinpath(args.out_path,f'noise_{group}.png'))


