from obspy import UTCDateTime


def get_arrivals_and_location(start_time: UTCDateTime, end_time: UTCDateTime):
    return f"""	
	{{
	  eventList(startTime: {start_time.timestamp},
	            endTime: {end_time.timestamp}
	            analysisType:MANUAL){{
	    events{{
	            id
				preferredOrigin{{
						id
				        depth
				        latitude
				        longitude
				        time
				        arrivals{{
				          time
				          timeResidual
				          pick{{
				            phase
				            time
				            channel{{
				              locationCode
				              station{{
				                stationCode
				                network{{
				                  networkCode
				                }}
				              }}
				            }}
				          }}
				        }}
				      }}
				    }}
				  }}
				}}
	"""


def get_location_and_source_parameters(start_time: UTCDateTime, end_time: UTCDateTime):
    return f"""
    {{
	  eventList(startTime: {start_time.timestamp},
	            endTime: {end_time.timestamp}
	            analysisType:MANUAL){{
	    events{{
	            id
				preferredOrigin{{
						id
				        depth
				        latitude
				        longitude
				        time
				        apparentStress
				        stressDrop
				        energy
				        seismicMoment
				        sourceRadius
						pWaveCornerFrequency
				        sWaveCornerFrequency		
				        locationMethod
				        magnitudes{{
				            magnitudeType
				            author
				            value
				        }}
				          
				        
				    }}
				  }}
				}}	
	}}
	
	
	"""
