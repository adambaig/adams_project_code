import glob
import os

import matplotlib.pyplot as plt
from matplotlib import patches

from read_inputs import read_station, read_horizon, read_velocity_model


def calculate_Poissons_ratio(vp, vs):
    return 0.5 * ((vp / vs) ** 2 - 2) / ((vp / vs) ** 2 - 1)


horizon_files = glob.glob("Formation Tops//*.txt")
horizons = {}
for horizon_file in horizon_files:
    horizon_name = (
        os.path.basename(horizon_file).split("Horizon ")[1].split(" depth")[0].strip()
    )
    horizons[horizon_name] = read_horizon(horizon_file)

velocity_model = read_velocity_model()

fig, ax = plt.subplots(figsize=[6, 8])
vp_curve = [[velocity_model["vps"][0], velocity_model["elevations"][0]]]
vs_curve = [[velocity_model["vss"][0], velocity_model["elevations"][0]]]
for i_layer in range(len(velocity_model["elevations"]) - 1):
    vp_curve.append(
        [velocity_model["vps"][i_layer], velocity_model["elevations"][i_layer + 1]]
    )
    vp_curve.append(
        [velocity_model["vps"][i_layer + 1], velocity_model["elevations"][i_layer + 1]]
    )
    vs_curve.append(
        [velocity_model["vss"][i_layer], velocity_model["elevations"][i_layer + 1]]
    )
    vs_curve.append(
        [velocity_model["vss"][i_layer + 1], velocity_model["elevations"][i_layer + 1]]
    )

vp_curve.append([velocity_model["vps"][-1], -3000])
vs_curve.append([velocity_model["vss"][-1], -3000])
ax.plot([a[0] for a in vp_curve], [a[1] for a in vp_curve], color="royalblue", lw=2)
ax.plot([a[0] for a in vs_curve], [a[1] for a in vs_curve], color="firebrick", lw=2)
ax.legend(["$v_p$", "$v_s$"])

gr114 = read_station("GR114.csv")
bf16 = read_station("BF16.csv")

ax.set_facecolor("0.9")
for station in bf16.values():
    ax.plot(22000, -station["tvdss"], "v", color="0.3", markeredgecolor="k")
ax.text(22000, -station["tvdss"] - 50, "BF16", ha="center", va="top")

for station in gr114.values():
    ax.plot(25000, -station["tvdss"], "v", color="0.5", markeredgecolor="k")
ax.text(25000, -station["tvdss"] - 50, "GR114", ha="center", va="top")

patch_color = {
    "DB4": "forestgreen",
    "Mine Level": "mediumorchid",
    "Shell Lake": "darkgoldenrod",
}

for horizon_name, horizon in horizons.items():
    max_elevation = max(horizon["elevations"])
    min_elevation = min(horizon["elevations"])
    horizon_patch = patches.Rectangle(
        (0, min_elevation),
        28000,
        max_elevation - min_elevation,
        color=patch_color[horizon_name],
        ec="0.2",
        zorder=-5,
        alpha=0.5,
    )
    ax.add_patch(horizon_patch)
    if horizon_name == "Shell Lake":
        ax.text(100, min_elevation + 20, horizon_name)
    else:
        ax.text(100, max_elevation - 100, horizon_name)

ax.set_ylim([-2500, 1700])
ax.set_xlim([0, 28000])
ax.set_xlabel("velocity (ft/s)")
ax.set_ylabel("elevation relative to sea level (ft)")


fig.savefig("velocity_model_and_arrays.png")
