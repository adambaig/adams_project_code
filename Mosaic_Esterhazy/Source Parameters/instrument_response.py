from obspy import read_inventory
from obspy.signal.invsim import corn_freq_2_paz
from obspy.core.inventory.response import Response

paz = corn_freq_2_paz(15)
response = Response.from_paz(
	poles=paz['poles'],
	zeros=paz['zeros'],
	stage_gain=86.6,
	stage_gain_frequency=100,
	normalization_frequency=100
)


response.plot(min_freq=1, sampling_rate=2000)

