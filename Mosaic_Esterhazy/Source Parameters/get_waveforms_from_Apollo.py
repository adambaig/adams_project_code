from pathlib import Path

from nmxseis.interact.fdsn import FDSNClient
from nmxseis.model.nslc import NSLC
from nmxseis.model.pick_set import PickSet
from obspy import read_inventory, UTCDateTime

server_ip = '52.11.206.250:8081'
event_pre = 20
event_post = 40
inv = read_inventory('MO.xml')

nslcs = NSLC.iter_inv(inv)
apollo = FDSNClient(server_ip)

fake_picks = Path('fake_picks').glob('*.picks')


for pick_file in list(fake_picks):
	nslcs = NSLC.iter_inv(inv)
	time = UTCDateTime.strptime(pick_file.stem.split('_')[1], '%Y%m%d.%H%M%S.%f')
	try:
		waveforms = apollo.get_waveforms(nslcs, time-event_pre, time+event_post)
	except:
		print(f"couldn't find waveforms for {time}")
		continue
	waveforms.write(Path('event_waveforms').joinpath(f'{pick_file.stem.replace(".","_")}.mseed'), format='MSEED')
