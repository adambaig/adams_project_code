import matplotlib.pyplot as plt
from nmxseis.interact.athena import AthenaClient
from NocMeta.Meta import NOC_META

ATHENA_CODE = 'MOC_EST'
athena = NOC_META[ATHENA_CODE]
client = AthenaClient(base_url=f"https://{athena['athIP']}", api_key=athena['athApi'])

gql_query = """
{
  eventList{
    events{
      origins{
        id
        latitude
        longitude
        errorAxes{
          azimuth
          dip
          length
        }
      }
    }
  }
}
"""

response = client.query_gql(gql_query)