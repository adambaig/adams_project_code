from datetime import datetime
from pathlib import Path
from zoneinfo import ZoneInfo

from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick import Pick
from nmxseis.model.pick_set import PickSet
from obspy import UTCDateTime, read_inventory

inv = read_inventory('MO.xml')
UTC = ZoneInfo('UTC')

event_times = ["11/21/2022T6:14:01Z",
               "11/21/2022T15:04:32Z",
               "11/21/2022T22:56:09Z",
               "11/22/2022T10:17:36Z",
               "11/22/2022T20:17:00Z",
               "11/23/2022T7:51:14Z",
               "11/23/2022T7:58:07Z",
               "11/23/2022T16:34:06Z",
               "11/23/2022T21:45:26Z",
               "11/25/2022T9:40:24Z",
               "11/25/2022T10:53:14Z",
               "11/25/2022T16:40:30Z",
               "11/26/2022T1:15:00Z",
               "11/27/2022T19:25:24Z",
               "11/27/2022T20:38:36Z",
               "11/28/2022T6:17:31Z",
               "11/29/2022T21:55:19Z",
               "12/1/2022T8:30:29Z",
               "12/3/2022T15:45:16Z",
               "12/4/2022T16:31:41Z",
               "12/4/2022T16:37:06Z",
               "12/5/2022T13:52:19Z"]

for i_ev, ev_time in enumerate(event_times):
	fake_dt = datetime.strptime(ev_time, "%m/%d/%YT%H:%M:%SZ")
	fake_dt.replace(tzinfo=ZoneInfo('America/Regina'))
	fake_pick_time = UTCDateTime(fake_dt.astimezone(UTC))
	picks = []
	for nsl in NSL.iter_inv(inv):
		picks.append(Pick(nsl=nsl, time=fake_pick_time, phase=Phase.P))

	PickSet(picks=picks).dump(
		Path().joinpath(
			'fake_picks',
			f'{str(i_ev).zfill(10)}_{fake_pick_time.strftime("%Y%m%d.%H%M%S.%f")}.picks'))
