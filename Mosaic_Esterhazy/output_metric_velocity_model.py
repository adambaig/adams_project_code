import json

from read_inputs import read_velocity_model

velocity_model_imperial = read_velocity_model()
velocity_model_imperial
F2M = 0.3048
velocity_model_metric = []

for vp, vs, elevation in zip(
    velocity_model_imperial["vps"],
    velocity_model_imperial["vss"],
    velocity_model_imperial["elevations"],
):
    velocity_model_metric.append(
        {
            "vp": vp * F2M,
            "vs": vs * F2M,
            "top": F2M * elevation,
            "rho": 310 * (vp * F2M) ** 0.25,
        }
    )

velocity_model_metric[0].pop("top")


with open("metric_velocity_model.json", "w") as f:
    json.dump(velocity_model_metric, f)
