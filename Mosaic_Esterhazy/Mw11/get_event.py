import json
from pathlib import Path

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.pick_set import PickSet
from NocMeta.Meta import NOC_META

ATHENA_CODE = 'MOC_STG'
EVENT_ID = '15170'

athena = NOC_META[ATHENA_CODE]
client = AthenaClient(base_url= f'http://{athena["athIP"]}', api_key=athena["athApi"])

event, pickset = client.get_preferred_origin_and_picks_for_event(EVENT_ID)



pickset.dump(Path('picks').joinpath(f'{EVENT_ID.zfill(10)}_{event.time.strftime("%Y%m%d.%H%M%S.%f")}.picks'))
