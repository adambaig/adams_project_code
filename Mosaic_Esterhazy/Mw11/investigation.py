import json
from pathlib import Path

from nmxseis.interact.athena import AthenaClient
from NocMeta.Meta import NOC_META

from read_inputs import read_velocity_model, get_station_enus

LOGIK_DIR = Path(r"C:\Logiklyst\PSM\LogikData\MOC_EST")

ATHENA_CODE = 'MOC_STG'
athena = NOC_META[ATHENA_CODE]
client = AthenaClient(base_url= f'http://{athena["athIP"]}', api_key=athena["athApi"])

picks, event = client.get_preferred_origin_and_picks_for_event(16744)


station_enus = get_station_enus()
velocity_model = read_velocity_model()
