from pathlib import Path

import matplotlib.pyplot as plt
from nmxseis.interact.athena import AthenaClient
from nmxseis.model.nslc import NSL
from nmxseis.model.pick import Pick
from nmxseis.model.pick_set import PickSet
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geometry import GridBoundsError
from nmxseis.numerics.velocity_model.vm_1d import VM1DTTInterpolator
import numpy as np

from obspy import UTCDateTime

from read_inputs import get_station_enus, read_velocity_model, REPROJECT, INV

START_TIME = UTCDateTime(2023, 1, 1)
END_TIME = UTCDateTime(2023, 3, 13, 16, 0)
BASE_DIR = Path(r'C:\Users\adambaig\Project\Mosaic_Esterhazy')
interp_dir = (BASE_DIR / "interpolators")
velocity_model = read_velocity_model()
station_enus = get_station_enus()
nsl_order = sorted(tuple(station_enus.keys()))
interpolator = VM1DTTInterpolator.load_from_npz_dir(interp_dir, nsl_order)

pick_set = PickSet.load(BASE_DIR / "BF16_investigation" /
                        "picks" / "0000222680_20230319.074325.568972.picks")

bf16_picks = PickSet(picks=[
	p for p in pick_set.picks if p.nsl.sta == 'BF16' and p.annotation == ""
])

test_loc = station_enus[NSL.parse('MO.BF16.01')] + np.array([0,200,0])

tt_interp = interpolator.get_tt_arr(test_loc)

tt_onthefly = {}

for nsl in [nsl for nsl in station_enus if nsl.sta=='BF16']:
	for ph in [Phase.P, Phase.S]:
		tt_onthefly[nsl, ph] = velocity_model.get_tt_sec(
			test_loc, station_enus[nsl],  ph)

fig,ax = plt.subplots()
for i_nsl, nsl in enumerate(nsl_order):
	if nsl.sta != "BF16":
		continue
	p_f = ax.plot(tt_onthefly[nsl, Phase.P], float(nsl.loc), 'ro')
	p_i = ax.plot(tt_interp[i_nsl, 0], float(nsl.loc), 'rx')
	s_f = ax.plot(tt_onthefly[nsl, Phase.S], float(nsl.loc), 'bo')
	s_i = ax.plot(tt_interp[i_nsl, 1], float(nsl.loc), 'bx')


ax.set_xlim([0,0.1])
ax.legend([p_f[0] ,p_i[0], s_f[0], s_i[0]], ['P on-the-fly',
                                 'P interpolated',
                                 'S on-the-fly',
                                'S interpolated'])
ax.set_ylim([12.5,0.5])
ax.set_xlabel('time (s)')
ax.set_ylabel('location')
fig.savefig('int_onthefly_comparison_bf16.png')


