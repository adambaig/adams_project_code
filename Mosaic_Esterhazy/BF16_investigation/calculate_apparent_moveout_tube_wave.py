from pathlib import Path

import matplotlib.pyplot as plt
from nmxseis.interact.athena import AthenaClient
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from NocMeta.Meta import NOC_META
from numpy import linspace
from numpy.linalg import norm

from read_inputs import get_station_enus, read_velocity_model

ATHENA_CODE = 'MOC_EST'
TUBE_WAVE_EVENT_ID = '218199'

athena_client = AthenaClient(
	base_url=f"http://{NOC_META[ATHENA_CODE]['athIP']}",
	api_key=NOC_META[ATHENA_CODE]['athApi']
)
event, pick_set = athena_client.get_preferred_origin_and_picks_for_event(TUBE_WAVE_EVENT_ID)
waveforms = athena_client.read_event_waveforms(TUBE_WAVE_EVENT_ID)

repicked_pick_set = PickSet.load(Path(r'C:\Logiklyst\PSM\LogikData\MOC_EST\Picks\Tube_wave') /
                                 '0000218199_20230312.192140.401445.picks')

pick_set = repicked_pick_set

velocity_model = read_velocity_model()
station_enus = get_station_enus()


bf_16_picks_p = [p for p in pick_set.picks if p.nsl.sta == 'BF16' and p.phase.is_P and p.annotation==""]
bf_16_picks_s = [p for p in pick_set.picks if p.nsl.sta == 'BF16' and p.phase.is_S and p.annotation==""]

app_vel_p, app_vel_s = [],[]
for app_vel,picks in zip([app_vel_p, app_vel_s],[bf_16_picks_p, bf_16_picks_s]):
	for i_pick, pick_1 in enumerate(picks[:-1]):
		for pick_2 in picks[i_pick:]:
			if pick_1.nsl==pick_2.nsl:
				continue
			dt = abs(pick_1.time - pick_2.time)
			dz = norm(station_enus[pick_1.nsl] - station_enus[pick_2.nsl])
			app_vel.append(dz/dt)


bins = linspace(500,1600,23)
fig,(ax1, ax2) = plt.subplots(2, figsize=[8,8], sharex=True)
ax1.hist(app_vel_p, bins=bins)
ax2.hist(app_vel_s, bins=bins)
ax1.set_ylabel('count first phase')
ax2.set_ylabel('count second phase')
ax2.set_xlabel('apparent velocity (m/s)')
fig.savefig('tube_wave_app_vels.png')

