from pathlib import Path

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.phase import Phase
from nmxseis.model.pick_set import PickSet
from numpy.linalg import norm

from read_inputs import get_station_enus, read_velocity_model

velocity_model = read_velocity_model()
station_enus = get_station_enus()

pick_file = Path(r'C:\Users\adambaig\Project\Mosaic_Esterhazy\BF16_investigation\picks\0000223052_20230321.084442.155857.picks')

pick_set = PickSet.load(pick_file)
bf_16_picks_p = [p for p in pick_set.picks if p.nsl.sta == 'BF16' and p.phase.is_P and p.annotation==""]
bf_16_picks_s = [p for p in pick_set.picks if p.nsl.sta == 'BF16' and p.phase.is_S and p.annotation==""]

app_vel_p, app_vel_s = [],[]
for app_vel,picks in zip([app_vel_p, app_vel_s],[bf_16_picks_p, bf_16_picks_s]):
	for i_pick, pick_1 in enumerate(picks[::-1]):
		for pick_2 in picks[i_pick::]:
			if pick_1.nsl==pick_2.nsl:
				continue

			dt = abs(pick_1.time - pick_2.time)
			dz = norm(station_enus[pick_1.nsl] - station_enus[pick_2.nsl])
			print(dt, dz)
			app_vel.append(dz/dt)



