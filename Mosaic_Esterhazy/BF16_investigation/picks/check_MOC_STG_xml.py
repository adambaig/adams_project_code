
import json
from pathlib import Path

from nmxseis.model.nslc import NSL
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.velocity_model.vm_0d import VelocityModelHomogeneous
from nmxseis.numerics.velocity_model.vm_1d import VMLayer1D, VelocityModel1D

import numpy as np
from obspy import UTCDateTime, read_inventory

BASE_DIR = Path(r"C:\Users\adambaig\Project\Mosaic_Esterhazy")
STATION_XML = Path(r"C:\Logiklyst\PSM\LogikData\MOC_STG\MOC_STG_Full.xml")
SASK_UTM_EPSG = 3158
INV = read_inventory(STATION_XML)
REPROJECT = EPSGReprojector(SASK_UTM_EPSG)
PRODUCTION_EPSG = 3158
PRODUCTION_REPROJECT = EPSGReprojector(PRODUCTION_EPSG)

def get_station_enus():
    return REPROJECT.get_station_enus(INV, NSL.iter_inv(INV))


if __name__ == "__main__":
	station_enus = get_station_enus()