import json
from pathlib import Path

import matplotlib.pyplot as plt
from numpy import linspace

interpolated_dir = Path(r"C:\Users\adambaig\logikdata\MOC_EST\Located\interpolated")
on_the_fly_dir = Path(r"C:\Users\adambaig\logikdata\MOC_EST\Located\on_the_fly")
interp, on_the_fly = {}, {}
event_ids = [p.stem for p in interpolated_dir.glob("*.json")]
for event_id in event_ids:
    on_the_fly_json = on_the_fly_dir.joinpath(f"{event_id}.json")
    interp_json = interpolated_dir.joinpath(f"{event_id}.json")
    interp[event_id] = json.loads(interp_json.read_text())
    interp[event_id]["event"]["origin"]["mw"] = interp[event_id]["event"]["origin"]["magnitudes"][0][
        "value"
    ]
    interp[event_id]["event"]["origin"]["mw author"] = interp[event_id]["event"]["origin"]["magnitudes"][
        0
    ]["author"]
    interp[event_id]["event"]["origin"]["n_arrivals"] = len(
        interp[event_id]["event"]["origin"]["arrivals"]
    )
    on_the_fly[event_id] = json.loads(on_the_fly_json.read_text())
    on_the_fly[event_id]["event"]["origin"]["mw"] = on_the_fly[event_id]["event"]["origin"][
        "magnitudes"
    ][0]["value"]
    on_the_fly[event_id]["event"]["origin"]["mw author"] = on_the_fly[event_id]["event"]["origin"][
        "magnitudes"
    ][0]["author"]
    on_the_fly[event_id]["event"]["origin"]["n_arrivals"] = interp[event_id]["event"]["origin"][
        "n_arrivals"
    ]


def make_comparison_plot(key, **kwargs):
    param_interp = [interp[event_id]["event"]["origin"][key] for event_id in event_ids]
    param_on_the_fly = [on_the_fly[event_id]["event"]["origin"][key] for event_id in event_ids]
    fig, ax = plt.subplots()
    ax.set_facecolor("0.96")
    ax.plot(param_on_the_fly, param_interp, **kwargs)
    max_ax = max(*ax.get_xlim(), *ax.get_ylim())
    min_ax = min(*ax.get_xlim(), *ax.get_ylim())
    ax.plot([min_ax, min_ax], [max_ax, max_ax], "k:", zorder=1, lw=2)
    ax.set_xlim([min_ax, max_ax])
    ax.set_ylim([min_ax, max_ax])

    ax.set_xlabel("on the fly dataset")
    ax.set_ylabel("interpolator dataset")
    ax.set_title(key)
    ax.set_aspect("equal")
    ax.grid(True)

    f_hist, (a_hist_int, a_hist_fly) = plt.subplots(2, sharex=True, sharey=True)
    bins = linspace(min_ax, max_ax, 100)
    a_hist_fly.hist(param_on_the_fly, bins=bins, facecolor=kwargs["c"])
    a_hist_int.hist(param_interp, bins=bins, facecolor=kwargs["c"])
    a_hist_fly.set_xlabel(key)
    a_hist_fly.set_ylabel("on the fly")
    a_hist_int.set_ylabel("interpolated")
    return fig, f_hist


def make_comparison_scatter(key, color_by_key, **kwargs):

    param_interp = [interp[event_id]["event"]["origin"][key] for event_id in event_ids]
    param_on_the_fly = [on_the_fly[event_id]["event"]["origin"][key] for event_id in event_ids]
    color_param = [interp[event_id]["event"]["origin"][color_by_key] for event_id in event_ids]
    fig, ax = plt.subplots()
    ax.set_facecolor("0.96")
    scatter = ax.scatter(param_on_the_fly, param_interp, c=color_param, **kwargs)
    max_ax = max(*ax.get_xlim(), *ax.get_ylim())
    min_ax = min(*ax.get_xlim(), *ax.get_ylim())
    ax.plot([min_ax, min_ax], [max_ax, max_ax], "k:", zorder=1, lw=2)
    ax.set_xlim([min_ax, max_ax])
    ax.set_ylim([min_ax, max_ax])

    ax.set_xlabel("on the fly dataset")
    ax.set_ylabel("interpolator dataset")

    ax.set_title(key)
    ax.set_aspect("equal")
    cbar = fig.colorbar(scatter)
    cbar.set_label(color_by_key)
    return fig


fig_mwc = make_comparison_scatter("mw", "n_arrivals", edgecolor="0.2", cmap="turbo", vmin=1, alpha=0.4)


fig_rms, hist_rms = make_comparison_plot(
    "timeRms", marker="o", c="firebrick", mec="0.2", linestyle="None", alpha=0.5
)
fig_mw, hist_mw = make_comparison_plot(
    "mw", marker="o", c="royalblue", mec="0.2", linestyle="None", alpha=0.5
)
fig_depth, hist_depth = make_comparison_plot(
    "depth", marker="o", c="forestgreen", mec="0.2", linestyle="None", alpha=0.5
)

fig_rms.savefig("rms_comparison.png")
hist_rms.savefig("rms_hist.png")
fig_mw.savefig("mw_comparison.png")
hist_mw.savefig("mw_hist.png")
fig_depth.savefig("depth_comparison.png")
hist_depth.savefig("depth_hist.png")
