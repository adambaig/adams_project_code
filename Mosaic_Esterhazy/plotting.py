from dataclasses import dataclass
from typing import Self

import numpy as np
from matplotlib import pyplot as plt


def gray_background_with_grid(axis: plt.Axes, grid_spacing=250, aspect=1):
    axis.set_facecolor("0.96")
    axis.set_aspect(aspect)
    x1, x2 = axis.get_xlim()
    y1, y2 = axis.get_ylim()
    grid_spacing2 = 2 * grid_spacing
    x_minor_ticks = np.arange(
        np.floor(x1 / grid_spacing) * grid_spacing,
        np.ceil(x2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    x_major_ticks = np.arange(
        np.floor(x1 / grid_spacing2) * grid_spacing2,
        np.ceil(x2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    y_minor_ticks = np.arange(
        np.floor(y1 / grid_spacing) * grid_spacing,
        np.ceil(y2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    y_major_ticks = np.arange(
        np.floor(y1 / grid_spacing2) * grid_spacing2,
        np.ceil(y2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    axis.set_xticks(x_minor_ticks, minor=True)
    axis.set_yticks(y_minor_ticks, minor=True)
    axis.set_xticks(x_major_ticks, minor=False)
    axis.set_yticks(y_major_ticks, minor=False)

    axis.grid(which="both")
    axis.grid(which="minor", alpha=1)
    axis.set_xlim([x1, x2])
    axis.set_ylim([y1, y2])
    axis.set_yticklabels([])
    axis.set_xticklabels([])
    axis.tick_params(which="both", color=(0, 0, 0, 0))
    axis.set_axisbelow(True)
    return axis


@dataclass
class Frame:
    xmin: float
    xmax: float
    ymin: float
    ymax: float

    @classmethod
    def from_axis(cls, axis: plt.Axes) -> Self:
        return cls(
            xmin=axis.get_xlim()[0],
            xmax=axis.get_xlim()[1],
            ymin=axis.get_ylim()[0],
            ymax=axis.get_ylim()[1],
        )

    def set_axis(self, axis: plt.Axes) -> plt.Axes:
        axis.set_xlim([self.xmin, self.xmax])
        axis.set_ylim([self.ymin, self.ymax])
        return axis
