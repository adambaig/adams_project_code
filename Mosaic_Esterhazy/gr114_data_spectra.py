from pathlib import Path

import matplotlib.pyplot as plt
from nmxseis.util.obspy_tools import is_probably_data_trace
from numpy.fft import fft, fftfreq
from obspy import read

seedfile = Path('Data').joinpath('gr114_2023-04-17T000000-000200.mseed')

stream = read(seedfile)


fig, (ax_1, ax_2, ax_z) = plt.subplots(1,3, figsize=[12,8])
plot_dict = {
	'GP1': ax_1,
	'GP2': ax_2,
	'GPZ': ax_z
}
color = {
	'GP1': 'firebrick',
	'GP2': 'forestgreen',
	'GPZ': 'royalblue'
}

for trace in stream:
	location = trace.stats.location
	if (channel:=trace.stats.channel) not in plot_dict:
		continue
	trace.detrend()\
		.filter('bandpass', freqmin=1, freqmax=50)
	data = trace.data[10000:12000]
	times= trace.times()[:2000]
	plot_dict[channel].plot(times, data/2/max(abs(data)) + int(location), c=color[channel])

for ax in ax_1, ax_2, ax_z:
	ax.set_facecolor('0.96')
	ax.set_xlabel('time (s)')


ax_1.set_title('GP1')
ax_2.set_title('GP2')
ax_z.set_title('GPZ')
