import glob
import logging
import json
import os

import matplotlib.pyplot as plt
import numpy as np
from read_inputs import read_station, read_auto_events
from sms_moment_tensor.MT_math import trend_plunge_to_unit_vector


PI = np.pi
D2R = PI / 180.0
HODOGRAM_SCALE = 100
DO_PLOT = False
bf16 = read_station("BF16.csv")
events = read_auto_events(
    r"C:\Users\adambaig\Project\Mosaic_Esterhazy\hodograms\autos_plus_azimuth_all_rotate.csv"
)
stations = {
    (2 * (int(k.split("_")[1]) - 1) + int(k.split("_")[2])): v for k, v in bf16.items()
}
for station in stations.values():
    station["elevation"] = -station["tvdss"]

for hodogram_json in glob.glob(os.path.join("hodograms", "hodogram_jsons", "*.json")):

    # json_path = os.path.join(
    #     "hodograms", "hodogram_jsons", "4283143229086_00001_20210923.143229.086999.json"
    # )

    with open(hodogram_json) as f:
        hodograms = json.load(f)
    if not hodograms:
        logging.info(f"no hodograms found for event {event_id}")
        continue
    for hodogram in hodograms.values():
        hodogram["unit vector"] = trend_plunge_to_unit_vector(
            hodogram["trend"], hodogram["plunge"]
        )
    event_id = os.path.basename(hodogram_json).split("_")[0]

    total_trend = hodograms["event"]["trend"]
    if event_id not in events:
        logging.error(f"{event_id} not found!")
        continue
    event = events[event_id]

    def rotate_to_plane(angle_degrees):
        angle = angle_degrees * D2R
        return [
            [np.sin(angle), -np.cos(angle), 0],
            [np.cos(angle), np.sin(angle), 0],
            [0, 0, 1],
        ]

    if DO_PLOT:
        fig, ax = plt.subplots()
        ax.set_aspect("equal")
    rotation_matrix = rotate_to_plane(total_trend)
    direct_sum, flip_sum = 0, 0
    for station_int, station in stations.items():
        if DO_PLOT:
            ax.plot(0, station["elevation"], "v", color="0.3", markeredgecolor="0.1")
        station_str = str(station_int).zfill(2)

        if station_str in hodograms:
            hodogram = hodograms[station_str]
            unit_vector = hodogram["unit vector"]
            rotated_vector = rotation_matrix @ unit_vector
            station_event_2vector = [0, station["elevation"]]
            direct_unit_2vector = [
                event["radial"],
                -station["elevation"] - event["depth"],
            ]
            direct_unit_2vector /= np.linalg.norm(direct_unit_2vector)
            flip_unit_2vector = [
                -event["radial"],
                -station["elevation"] - event["depth"],
            ]
            flip_unit_2vector /= np.linalg.norm(flip_unit_2vector)
            if DO_PLOT:
                ax.plot(
                    HODOGRAM_SCALE * rotated_vector[0] * np.array([-1, 1]),
                    HODOGRAM_SCALE * rotated_vector[2] * np.array([-1, 1])
                    + station["elevation"],
                    "k",
                )
                ax.plot(
                    HODOGRAM_SCALE * direct_unit_2vector[0] * np.array([-1, 1]),
                    HODOGRAM_SCALE * direct_unit_2vector[1] * np.array([-1, 1])
                    + station["elevation"],
                    "m",
                )
                ax.plot(
                    HODOGRAM_SCALE * flip_unit_2vector[0] * np.array([-1, 1]),
                    HODOGRAM_SCALE * flip_unit_2vector[1] * np.array([-1, 1])
                    + station["elevation"],
                    "c",
                )

            direct_sum += rotated_vector[0::2] @ direct_unit_2vector
            flip_sum += rotated_vector[0::2] @ flip_unit_2vector

    if flip_sum > direct_sum:
        print("flip {event_id}")
    else:
        print("keep {event_id} in place")
    if DO_PLOT:
        ax.plot(
            event["radial"],
            -event["depth"],
            "o",
            color="firebrick",
            markeredgecolor="0.1",
        )
