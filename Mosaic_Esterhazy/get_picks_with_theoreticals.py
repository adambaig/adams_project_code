from pathlib import Path

import numpy as np
from NocMeta.Meta import NOC_META
from nmxseis.interact.athena import AthenaClient
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.pick import Pick
from nmxseis.model.pick_set import PickSet
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geometry import GridBoundsError
from nmxseis.numerics.relocation.optimizing_relocator import get_tricubic_interpolated_tts_and_hodos
from nmxseis.numerics.velocity_model.vm_1d import VelocityModel1D
from numpy import zeros
from obspy import UTCDateTime

from athena_queries import get_arrivals_and_location
from read_inputs import get_station_enus, read_velocity_model, REPROJECT, INV

START_TIME = UTCDateTime(2023, 1, 1)
END_TIME = UTCDateTime(2023, 3, 13, 16, 0)

athena_code = "MOC_EST"
athena_client = AthenaClient(
    base_url=rf'https://{NOC_META[athena_code]["athIP"]}', api_key=NOC_META[athena_code]["athApi"]
)

velocity_model = read_velocity_model()
station_enus = get_station_enus()
nsl_order = sorted(tuple(station_enus.keys()))
event_list = athena_client.query_gql(get_arrivals_and_location(START_TIME, END_TIME))

pass


def arrivals_to_picks(arrivals_dict):
    picks = []
    for arrival in arrivals_dict:
        phase = Phase((pick := arrival['pick'])['phase'])
        time = UTCDateTime(pick['time'])
        nsl = NSL(
            loc=cha['locationCode'] if (cha := pick['channel'])['locationCode'] != '--' else '',
            sta=(sta := cha['station'])['stationCode'],
            net=sta['network']['networkCode']
        )
        picks.append(Pick(phase=phase, time=time, nsl=nsl))
    return picks


def get_theoretical_picks(source_enu: np.ndarray, station_enus: dict[NSL, np.ndarray], velocity_model: VelocityModel1D,
                          nsl_order: list[NSL]) -> list[Pick]:
    picks = []
    try:
        traveltimes, hodograms = get_tricubic_interpolated_tts_and_hodos(
            velocity_model.arr_layers,
            source_enu,
            np.stack([station_enus[nsl] for nsl in nsl_order]),
            50
        )
    except GridBoundsError:
        traveltimes = zeros([len(nsl_order), 2])
        for i_ph, phase in enumerate([Phase.P, Phase.S]):
            for i_nsl, nsl in enumerate(nsl_order):
                traveltimes[i_nsl, i_ph] = velocity_model.get_tt_sec(source_enu, station_enus[nsl], phase)

    for i_ph, phase in enumerate([Phase.P, Phase.S]):
        for i_nsl, nsl in enumerate(nsl_order):
            if nsl.select_from_inv(INV).is_active(origin.time):
                picks.append(
                    Pick(
                        nsl=nsl,
                        phase=phase,
                        time=origin.time + traveltimes[i_nsl, i_ph],
                        annotation='modelled'
                    )
                )
    return picks


for event in event_list['eventList']['events']:
    pref_origin = event['preferredOrigin']
    arrivals = pref_origin['arrivals']
    origin = EventOrigin(
        lat=pref_origin['latitude'],
        lon=pref_origin['longitude'],
        time=UTCDateTime(pref_origin['time']),
        depth_m=1000 * pref_origin['depth']
    )
    manual_picks = arrivals_to_picks(arrivals)
    theoretical_picks = get_theoretical_picks(origin.enu(REPROJECT), station_enus, velocity_model, nsl_order)
    pick_set = PickSet(picks=manual_picks + theoretical_picks)
    out_name = f'{str(event["id"]).zfill(10)}_{UTCDateTime(origin.time).strftime("%Y%m%d.%H%M%S.%f")}.picks'
    pick_set.dump(Path('theoretical_picks').joinpath(out_name))
