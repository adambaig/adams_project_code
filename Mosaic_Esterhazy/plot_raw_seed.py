import matplotlib.pyplot as plt
import numpy as np
from obspy import read
from obspy.io.xseed import Parser


seed_parser = Parser("GR114.dataless")

inv = seed_parser.get_inventory()

stream = read(r"seed\Mosaic_EsterhazyK220210701040030000.mseed")


fig, ax = plt.subplots()
edata, ndata, zdata = (
    np.zeros([12, 20000]),
    np.zeros([12, 20000]),
    np.zeros([12, 20000]),
)
for i_tr, tr in enumerate(stream):
    if tr.stats.channel == "GPE":
        edata[itr // 3, :] = tr.data
    elif tr.stats.channel == "GPN":
        ndata[itr // 3, :] = tr.data
    elif tr.stats.channel == "GPZ":
        zdata[itr // 3, :] = tr.data
