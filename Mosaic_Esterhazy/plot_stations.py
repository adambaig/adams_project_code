import fnmatch

import matplotlib.pyplot as plt
from nmxseis.interact.athena import AthenaClient
from NocMeta.Meta import NOC_META

from plotting import gray_background_with_grid
from read_inputs import get_station_enus, REPROJECT

ATHENA_CODE = 'MOC_EST'
station_enus = get_station_enus()

athena = AthenaClient(
	base_url=f'http://{NOC_META[ATHENA_CODE]["athIP"]}',
	api_key=NOC_META[ATHENA_CODE]["athApi"]
)
event, picks = athena.get_preferred_origin_and_picks_for_event('219798')

event_enu = event.enu(REPROJECT)

fig,(ax1,ax2) = plt.subplots(2)

for ax in ax1,ax2:
	# gray_background_with_grid(ax)
	ax.set_aspect(10)
	ax.set_facecolor('0.96')

group_colors = {
	'K3UG1': 'linen',
	'BF16': 'firebrick',
	'GR110': 'royalblue',
	'GR114': 'forestgreen',
	'122*': 'peru',
	'SR*': 'skyblue'
}

for nsl, enu in station_enus.items():
	color = 'w'
	for group, c in group_colors.items():
		if fnmatch.fnmatch(nsl.sta, group):
			color = c
	e,n,u = enu
	zorder = int(nsl.loc) if nsl.loc!="" else 0
	ax1.plot(e,u,'v',zorder=zorder, mec='0.2', color = color)
	ax2.plot(n,u,'v', zorder=zorder, mec='0.2', color = color)

fig.show()
