import json
from pathlib import Path

from nmxseis.model.nslc import NSL
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.velocity_model.vm_0d import VelocityModelHomogeneous
from nmxseis.numerics.velocity_model.vm_1d import VMLayer1D, VelocityModel1D

import numpy as np
from obspy import UTCDateTime, read_inventory

BASE_DIR = Path(r"C:\Users\adambaig\Project\Mosaic_Esterhazy")
LOGIKDATA_DIR = Path(r"C:\Logiklyst\PSM\LogikData\MOC_EST")
STATION_XML = BASE_DIR.joinpath("station_xml", "MOC_EST_Full.xml")
SASK_UTM_EPSG = 3158
INV = read_inventory(STATION_XML)
REPROJECT = EPSGReprojector(SASK_UTM_EPSG)
PRODUCTION_EPSG = 3158
PRODUCTION_REPROJECT = EPSGReprojector(PRODUCTION_EPSG)


def homogeneous_velocity_model():
    return VelocityModelHomogeneous(vp=5000, vs=3000, rho=2500)


def read_velocity_model(file_path=LOGIKDATA_DIR.joinpath("velocity_model.json")):
    with open(file_path) as f:
        velocity_model_json = json.load(f)
    layers = []
    for layer in velocity_model_json:
        layers.append(
            VMLayer1D(
                vp=layer["vp"],
                vs=layer["vs"],
                rho=layer["rho"],
                top=layer["top"] if "top" in layer else np.inf,
            )
        )
    return VelocityModel1D(layers)


correction = {11: 1, 10: 1, 13: -1}
offset = {11: 0, 10: 0, 13: 514.1976}


def read_old_catalog(filepath=Path("mosaic_catalog.csv")):
    with open(filepath) as f:
        _ = f.readline()
        lines = f.readlines()
    events = {}
    for line in lines:
        split_line = line.split(",")
        event_id = int(split_line[2])
        events[event_id] = {
            "enu": np.array([offset[c] + correction[c] * float(split_line[c]) for c in [10, 11, 13]]),
            "time": UTCDateTime(split_line[3]),
            "mw": float(split_line[16]),
            "corner_freq": float(split_line[17]),
            "stress_drop": float(split_line[18]) * 1e6,
        }
    return events


def read_synthetic_catalog(pathname):
    with open(pathname) as f:
        _ = f.readline()
        lines = f.readlines()
    events = {}
    for line in lines:
        split_line = line.split(",")
        event_id = split_line[0]
        events[event_id] = {
            "enu": np.array([float(split_line[c]) for c in [1, 2, 3]]),
            "mw": float(split_line[4]),
            "stress_drop": float(split_line[5]),
        }
    return events


def get_station_enus():
    return REPROJECT.get_station_enus(INV, NSL.iter_inv(INV))


def read_station(csv):
    with open(csv) as f:
        _head = [f.readline() for i in range(2)]
        lines = f.readlines()
    sensors = {}
    for i_line, line in enumerate(lines):
        if i_line % 3 == 0:
            split_line = line.split(",")
            sensors[split_line[0]] = {
                "easting": float(split_line[2]),
                "northing": float(split_line[1]),
                "tvdss": float(split_line[3]),
            }
    return sensors


def read_horizon(txt_file):
    M2F = 1 / 0.3048
    with open(txt_file) as f:
        lines = f.readlines()
    if txt_file == "Formation Tops\Horizon Shell Lake depth.txt":
        factor = 1
    else:
        factor = M2F
    eastings, northings, elevations = [], [], []
    for line in lines:
        split_line = line.split()
        if split_line[0].isnumeric():
            if split_line[-1] != "-999.99":
                eastings.append(factor * float(split_line[2]))
                northings.append(factor * float(split_line[3]))
                elevations.append(-M2F * float(split_line[4]))
    return {"eastings": eastings, "northings": northings, "elevations": elevations}


def read_auto_events(csv_catalog):
    with open(csv_catalog) as f:
        _head = f.readline()
        lines = f.readlines()
    events = {}
    for line in lines:
        split_line = line.split(",")
        event_id = split_line[1]
        events[event_id] = {
            "trigger id": split_line[0],
            "datetime": split_line[2],
            "timestamp": float(split_line[3]),
            "radial": float(split_line[4]),
            "depth": float(split_line[6]),
            "snr": float(split_line[7]),
            "raw_amp": float(split_line[8]),
            "azimuth": float(split_line[9]),
            "linearity": float(split_line[10]),
            "mean azimuth": float(split_line[11]),
            "mean linearity": float(split_line[12]),
            "std azimuth": float(split_line[13]),
        }
    return events
