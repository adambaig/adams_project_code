import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import minimize
from sms_ray_modelling.raytrace import isotropic_ray_trace

source = {"e": 150, "n": 200, "z": -1210, "t": 0.2}
velocity_model = [{"vp": 4000, "vs": 2200, "rho": 2700}]

stations = {}
tp, ts = [], []
for ii in range(8):
    station_name = f"station_{ii+1}"
    stations[station_name] = {"n": 0, "e": 0, "z": -1000 - 50 * ii}
    tp.append(
        isotropic_ray_trace(source, stations[station_name], velocity_model, "P")[
            "traveltime"
        ]
        + source["t"]
        + 0.001 * np.random.randn()
    )
    ts.append(
        isotropic_ray_trace(source, stations[station_name], velocity_model, "S")[
            "traveltime"
        ]
        + source["t"]
        + 0.002 * np.random.randn()
    )


def tt_residual(xzt):
    xyz = {"e": xzt[0], "n": 0, "z": xzt[1]}
    residual = 0
    for i_station, station in enumerate(stations.values()):
        tp_xz = isotropic_ray_trace(xyz, station, velocity_model, "P")["traveltime"]
        ts_xz = isotropic_ray_trace(xyz, station, velocity_model, "S")["traveltime"]
        residual += (tp_xz - tp[i_station] + xzt[2]) ** 2 + (
            ts_xz - ts[i_station] + xzt[2]
        ) ** 2
    return residual


minimize(tt_residual, [290, -1300, 0.1])
