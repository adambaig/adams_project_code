import numpy as np
from glob import glob
import os

import scipy.io
from obspy import read, read_inventory

waveform_seeds = glob(r"waveforms//*")
waveform_seed = waveform_seeds[9]
st = read(waveform_seed)
st.plot()


catalog = scipy.io.loadmat("PYHASALMI_catalog_7059_seismic_events.mat")

catalog.keys()

Iinv = read_inventory("Pyhasalmi_Mine_seismic_network.XML")

1.13 / np.sqrt(6)
1.13 / np.sqrt(6 * 19)
