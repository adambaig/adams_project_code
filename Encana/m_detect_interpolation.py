import matplotlib

matplotlib.use('Qt5agg')

import glob
import matplotlib.pyplot as plt
import numpy as np

detect_files = glob.glob('m_detect_*.csv')

for detect_file in detect_files:
    f = open(detect_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    configuration = 
