import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (
    MultipleLocator,
    FormatStrFormatter,
    AutoMinorLocator,
    LogLocator,
)

import glob


# assumes input files are mulitple realizations of M0s


def sqa(x):
    return np.squeeze(np.array(x))


center_east, center_north = 0, 0


decoherence_correction = 0.05

image_stats = {}
location_error = {}
for i_scenario, n_stations in enumerate(["15", "30", "60"]):
    csvs = glob.glob(
        "Location Error//" + n_stations + "_stations_??_center//synthetic_catalog_*.csv"
    )

    image_stats[n_stations] = []
    location_error[n_stations] = {}
    m_detect = {}
    quad = -0.2740683228859524
    slope = 2.534516496227919
    threshold = 3.653078492866034e-15

    factor = np.sqrt(13 / 13)

    for csv in csvs:
        position = csv.split("\\")[-2].split("_")[3]
        nnodes = csv.split("\\")[-2].split("_")[2]
        nstations = csv.split("\\")[-2].split("_")[0]
        f = open(csv)
        lines = f.readlines()
        f.close()
        for line in lines:
            lspl = line.split(",")
            true_loc = np.array([float(s) for s in lspl[2:5]])
            magnitude, stress_drop = [float(s) for s in lspl[5:7]]
            m11, m22, m33, m12, m13, m23 = [float(s) for s in lspl[7:13]]
            amplitude = float(lspl[13])
            image_loc = np.array([float(s) for s in lspl[14:17]])
            if abs(magnitude) < 1e-7:
                image_stats[n_stations].append(
                    {
                        "position": position,
                        "nstations": nstations,
                        "nnodes": nnodes,
                        "magnitude": magnitude,
                        "amplitude": amplitude,
                        "location shift": true_loc - image_loc,
                    }
                )

        location_error[n_stations][position] = {
            "vertical": np.percentile(
                abs(
                    np.array(
                        [i_s["location shift"][2] for i_s in image_stats[n_stations]]
                    )
                ),
                50,
            ),
            "horizontal": np.sqrt(
                np.percentile(
                    [
                        x**2 + y**2
                        for x, y in [
                            i_s["location shift"][:2] for i_s in image_stats[n_stations]
                        ]
                    ],
                    50,
                )
            ),
        }
        avg_amplitude = np.average(
            np.array([i_s["amplitude"] for i_s in image_stats[n_stations]])
        )
        m_detect[position] = (
            -slope
            + np.sqrt(
                slope * slope
                - 4 * quad * (np.log10(factor * avg_amplitude / threshold))
            )
        ) / 2.0 / quad + decoherence_correction

for config in ["15", "30", "60"]:
    fig, ax = plt.subplots()
    vert = []
    hrz = []
    for iteration in image_stats[config]:
        vert.append(abs(iteration["location shift"][2]))
        hrz.append(
            np.sqrt(
                iteration["location shift"][0] ** 2
                + iteration["location shift"][1] ** 2
            )
        )
    ax.hist(vert, bins=np.arange(0, 130, 5))
    ax.hist(hrz, bins=np.arange(0, 130, 5), alpha=0.5)
    print(
        config
        + " horizontal error and bias: %.1fm, %.1fm" % (2 * np.std(hrz), np.median(hrz))
    )
    print(
        config
        + " vertical error and bias: %.1fm, %.1fm" % (2 * np.std(vert), np.median(vert))
    )
