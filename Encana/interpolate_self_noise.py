import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np

PI = np.pi
PPSD_5 = r"BDMTA_05_FIXED\PPSD_with_geophone_self_noise\PPSD_05pct_RV.BDMTA.~.HHZ_20190908T000000_20190915T000000.csv"
self_noise = r"BDMTA_05_FIXED\PPSD_with_geophone_self_noise\SG-10 preamp gain 1.csv"

f = open(PPSD_5)
head = f.readline()
lines = f.readlines()
f.close()
freq_PPSD, db_PPSD_05 = np.zeros(len(lines)), np.zeros(len(lines))
for i_line, line in enumerate(lines):
    freq_PPSD[i_line], db_PPSD_05[i_line] = [float(s) for s in line.split(",")]

f = open(self_noise)
lines = f.readlines()
f.close()
freq_self_noise, db_self_noise = np.zeros(len(lines)), np.zeros(len(lines))
for i_line, line in enumerate(lines):
    freq_self_noise[i_line], db_self_noise[i_line] = [float(s) for s in line.split(",")]

fig, ax = plt.subplots()
ax.semilogx(freq_PPSD, db_PPSD_05, "firebrick")
ax.semilogx(freq_self_noise, db_self_noise, "royalblue")

interp_db = np.interp(np.log(freq_PPSD[::-1]), np.log(freq_self_noise), db_self_noise)[
    ::-1
]

max_noise = np.zeros(len(freq_PPSD))
for i_frq, frq in enumerate(freq_PPSD):
    max_noise[i_frq] = max([db_PPSD_05[i_frq], interp_db[i_frq]])

ax.semilogx(freq_PPSD, max_noise, "--", color="darkgoldenrod")

ax.legend(["PPSD 5th percentile", "GS One Self Noise", "maximum"])
ax.set_xlim([5, 50])


velocity_power = 10 ** (max_noise / 10) / (2 * PI * freq_PPSD) ** 2

ms = 0
for i_frq, (freq_bin1, freq_bin2) in enumerate(zip(freq_PPSD[:11], freq_PPSD[1:12])):
    ms += velocity_power[i_frq] * (freq_bin1 - freq_bin2)

rms = np.sqrt(ms)
