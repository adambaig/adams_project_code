import matplotlib.pyplot as plt
import numpy as np
import mplstereonet as mpls


from sms_moment_tensor.plotting import mohr_circle_plot
from sms_moment_tensor.MT_math import (
    trend_plunge_to_unit_vector,
    sdr_to_slip_vector,
    unit_vector_to_trend_plunge,
)
from sms_moment_tensor.stress_inversions import decompose_stress

MTs = [
    {
        "Mw": 3.38,
        "nodal_plane_1": {"strike": 360, "dip": 21.3, "rake": -33.3},
        "nodal_plane_2": {"strike": 121.4, "dip": 78.5, "rake": -108.1},
    },
    {
        "Mw": 2.66,
        "nodal_plane_1": {"strike": 18.0, "dip": 15.8, "rake": -9.1},
        "nodal_plane_2": {"strike": 116.8, "dip": 87.5, "rake": -105.6},
    },
]

regional_stress_state = {
    "R": 0.1,
    "s1": {"trend": 45, "plunge": 0},
    "s2": {"trend": 0, "plunge": 90},
    "s3": {"trend": 135, "plunge": 0},
}

axis_color = {"s1": "firebrick", "s2": "royalblue", "s3": "forestgreen"}

eigenvectors = [
    trend_plunge_to_unit_vector(
        regional_stress_state[s]["trend"], regional_stress_state[s]["plunge"]
    )
    for s in ["s1", "s2", "s3"]
]
eigenvalues = np.diag([1, regional_stress_state["R"], 0])
stress_tensor = np.transpose(eigenvectors) @ eigenvalues @ eigenvectors

fig, ax = plt.subplots(figsize=[8, 4])
mohr_circle_plot(
    ax,
    {k: v for k, v in MTs[0].items() if "nodal_plane_1" in k},
    stress_tensor,
    color="mediumorchid",
    markeredgecolor="k",
    marker="v",
)
mohr_circle_plot(
    ax,
    {k: v for k, v in MTs[1].items() if "nodal_plane_1" in k},
    stress_tensor,
    color="springgreen",
    markeredgecolor="k",
    plot_frame="False",
    marker="v",
)
mohr_circle_plot(
    ax,
    {k: v for k, v in MTs[0].items() if "nodal_plane_2" in k},
    stress_tensor,
    color="mediumorchid",
    markeredgecolor="k",
    plot_frame="False",
    marker="^",
)
mohr_circle_plot(
    ax,
    {k: v for k, v in MTs[1].items() if "nodal_plane_2" in k},
    stress_tensor,
    color="springgreen",
    markeredgecolor="k",
    plot_frame="False",
    marker="^",
)
fig.savefig("Mohr_circle_plot_test.png")


fig, ax = mpls.subplots()
ax.grid()
for axis, color in axis_color.items():
    ax.line(
        regional_stress_state[axis]["plunge"],
        regional_stress_state[axis]["trend"],
        markeredgecolor="k",
        color=color,
    )


mt_colors = ["springgreen", "mediumorchid"]
np_symbols = ["v", "^"]

f1, a1 = plt.subplots()
for i_mt, MT in enumerate(MTs):
    for i_plane, plane in enumerate(["nodal_plane_1", "nodal_plane_2"]):
        strike, dip, rake = [MT[plane][s] for s in ["strike", "dip", "rake"]]
        unit_slip_vector = sdr_to_slip_vector(strike, dip, rake)
        slip_trend, slip_plunge = unit_vector_to_trend_plunge(unit_slip_vector)
        ax.line(
            slip_plunge,
            slip_trend,
            np_symbols[i_plane],
            color=mt_colors[i_plane],
            markeredgecolor="k",
        )
        a1.plot(0, 0, np_symbols[i_plane], color=mt_colors[i_mt], markeredgecolor="k")
fig.savefig("slip_vectors.png")


a1.legend(
    [
        "Oct 5 M3.38, nodal plane 1",
        "Oct 5 M3.38, nodal plane 2",
        "Oct 6 M2.66, nodal plane 1",
        "Oct 6 M2.66, nodal plane 2",
    ]
)
f1.savefig("nodal_plane_legend.png")

fig, ax = plt.subplots()
for axis, color in axis_color.items():
    ax.plot(0, 0, "o", color=color, markeredgecolor="k")
ax.legend(["$\sigma_1$", "$\sigma_2$", "$\sigma_3$"])
fig.savefig("stress_axis_legend.png")
