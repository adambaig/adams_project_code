# import unittest

import matplotlib.pyplot as plt
import numpy as np
from sms_ray_modelling.anisotropy import (
    ray_in_a_vti_layer,
    thomsen_params_to_stiffness_tensor,
    solve_christoffel_equation,
    voigt_to_cjk,
)

PI = np.pi
D2R = PI / 180
EPS = 1e-10

greenhorn = {
    "vp": 3048,
    "vs": 1490,
    "rho": 2420,
    "epsilon": 0.255,
    "delta": -0.050,
    "gamma": 0.480,
}
isotropic = {
    "vp": 3048.0,
    "vs": 1490.0,
    "rho": 2420.0,
    "epsilon": 0.0,
    "delta": 0.0,
    "gamma": 0.0,
}
voigt = thomsen_params_to_stiffness_tensor(greenhorn)
# voigt = thomsen_params_to_stiffness_tensor(isotropic)


angle = PI / 4

cjk = voigt_to_cjk(voigt)
rho = greenhorn["rho"]
p_p, p_sh, p_sv = np.zeros([3, 901]), np.zeros([3, 901]), np.zeros([3, 901])
for i_angle, angle in enumerate(np.arange(901) * D2R / 10):
    unit_slowness = {"e": np.cos(angle), "n": 0, "z": np.sin(angle)}
    unit_vector = np.array([unit_slowness["e"], unit_slowness["n"], unit_slowness["z"]])
    christoffel = (
        unit_slowness["e"] * unit_slowness["e"] * cjk["c_ee"] / rho
        + unit_slowness["e"] * unit_slowness["n"] * (cjk["c_en"] + cjk["c_en"].T) / rho
        + unit_slowness["e"] * unit_slowness["z"] * (cjk["c_ez"] + cjk["c_ez"].T) / rho
        + unit_slowness["n"] * unit_slowness["n"] * cjk["c_nn"] / rho
        + unit_slowness["n"] * unit_slowness["z"] * (cjk["c_nz"] + cjk["c_nz"].T) / rho
        + unit_slowness["z"] * unit_slowness["z"] * cjk["c_zz"] / rho
    )
    e_vals, e_vecs = np.linalg.eig(christoffel)
    e_vec1, e_vec2, e_vec3 = e_vecs.T
    for i_ev, e_vec in enumerate(e_vecs.T):
        if e_vec[2] < EPS:
            p_sh[:, i_angle] = unit_vector / np.sqrt(e_vals[i_ev])
            i1, i2 = (i_ev + 1) % 3, (i_ev + 2) % 3
            if abs(np.dot(e_vecs.T[i1], unit_vector)) > abs(
                np.dot(e_vecs.T[i2], unit_vector)
            ):
                p_sv[:, i_angle] = unit_vector / np.sqrt(e_vals[i2])
                p_p[:, i_angle] = unit_vector / np.sqrt(e_vals[i1])
            else:
                p_sv[:, i_angle] = unit_vector / np.sqrt(e_vals[i1])
                p_p[:, i_angle] = unit_vector / np.sqrt(e_vals[i2])


pp_raytrace, pv_raytrace, ph_raytrace = np.zeros(901), np.zeros(901), np.zeros(901)
x_p, x_v, x_h = np.zeros(901), np.zeros(901), np.zeros(901)
t_p, t_v, t_h = np.zeros(901), np.zeros(901), np.zeros(901)
for i_angle, angle in enumerate(np.arange(901) * D2R / 10):
    pp_raytrace[i_angle], x_p[i_angle], t_p[i_angle] = ray_in_a_vti_layer(
        p_p[0, i_angle], greenhorn, 100, "P"
    )
    pv_raytrace[i_angle], x_v[i_angle], t_v[i_angle] = ray_in_a_vti_layer(
        p_sv[0, i_angle], greenhorn, 100, "SV"
    )
    ph_raytrace[i_angle], x_h[i_angle], t_h[i_angle] = ray_in_a_vti_layer(
        p_sh[0, i_angle], greenhorn, 100, "SH"
    )


fig, ax = plt.subplots()
ax.set_aspect("equal")
ax.plot(p_p[0, :], pp_raytrace, color="firebrick")
ax.plot(p_sv[0, :], pv_raytrace, color="forestgreen")
ax.plot(p_sh[0, :], ph_raytrace, color="royalblue")
ax.plot(p_p[0, :], p_p[2, :], ":", color="firebrick")
ax.plot(p_sv[0, :], p_sv[2, :], ":", color="forestgreen")
ax.plot(p_sh[0, :], p_sh[2, :], ":", color="royalblue")


f2, a2 = plt.subplots()
a2.plot(x_p[-101:], t_p[-101:], color="firebrick")
a2.plot(x_v[-101:], t_v[-101:], color="forestgreen")
a2.plot(x_h[-101:], t_h[-101:], color="royalblue")


test = np.array(
    [
        [11, 12, 13, 14, 15, 16],
        [12, 22, 23, 24, 25, 26],
        [13, 23, 33, 34, 35, 36],
        [14, 24, 34, 44, 45, 46],
        [15, 25, 35, 45, 55, 56],
        [16, 26, 36, 46, 56, 66],
    ]
)


voigt_to_cjk(test)


a = np.random.random_integers(1, 1e7, size=[6, 6])

random_cij = (a + a.T) // 2
random_cij == rank4_to_voigt(devoigt(random_cij))


def kronecker(i, j):
    return int(i == j)


for i in range(3):
    for j in range(3):
        for r in range(3):
            for s in range(3):
                rhs = sum(
                    [
                        c_ijkl[i, j, p, q] * s_ijkl[p, q, r, s]
                        for p in range(3)
                        for q in range(3)
                    ]
                )
                lhs = 0.5 * (
                    kronecker(i, r) * kronecker(j, s)
                    + kronecker(i, s) * kronecker(j, r)
                )
                if abs(rhs - lhs) > 1e-6:
                    print(i, j, r, s, rhs, lhs)


# from scipy.optimize import brentq
# from sms_ray_modelling.anisotropy import ray_in_a_vti_layer, thomsen_params_to_stiffness_tensor
# coordinate system assumes z is up
#
# velocity_model = [
#     {"vp": 3500, "vs": 2000, "rho": 2400, "epsilon": 0.1, "delta": 0.2, "gamma": 0.2},
#     {"vp": 3500, "vs": 2000, "rho": 2400, "epsilon": 0.1, "delta": 0.2, "gamma": 0.2, "top": 1200},
#     {"vp": 3500, "vs": 2000, "rho": 2400, "epsilon": 0.1, "delta": 0.2, "gamma": 0.2, "top": 1500},
# ]
#
#
# src, rec = {"e": 199, "n": 321, "z": 1234}, {"e": 0, "n": 0, "z": 1410}
# # layer = effective_velocity_model[0]
# # stiffness_tensor = thomsen_params_to_stiffness_tensor(velocity_model[0])
# # np.sqrt(stiffness_tensor[0,0] / 2400)
#
#
# layer = {**velocity_model[0]}
#
# p3, dx, dt = ray_in_a_vti_layer(1 / 2001, layer, 100, "SV")
# def vti_ray_trace(src, rec, velocity_model, phase, head_wave=False):
#     from sms_ray_modelling.anisotropy import ray_in_a_vti_layer
#
#     hrz_vector = {"e": rec["e"] - src["e"], "n": rec["n"] - src["n"]}
#     if src["z"] < rec["z"]:  # source is below receiver
#         z_start = src["z"]
#         z_end = rec["z"]
#     else:  # source is above receiver
#         z_start = rec["z"]
#         z_end = src["z"]
#     effective_velocity_model = []
#     for layer in determine_velocity_profile(deepcopy(velocity_model), z_start, z_end):
#         up_down = np.sign(rec["z"] - src["z"])
#         effective_velocity_model.append(
#             {**layer, "h": layer["height"] * up_down, }  # -ve height for downgoing
#         )
#
#     return ray_path
