import matplotlib

matplotlib.use("Qt5Agg")

import glob
import matplotlib.pyplot as plt
import numpy as np


file_root = "homogeneous_model//before_flip//Detectability_Map//060_stations"

csvs = glob.glob(file_root + "*//synthetic_catalog_relocated.csv")


i_xs = np.unique([int(c.split("\\")[-2].split("_")[-1][0:2]) for c in csvs])
i_ys = np.unique([int(c.split("\\")[-2].split("_")[-1][3:5]) for c in csvs])

nx = len(i_xs)
ny = len(i_ys)

M0_amps = np.zeros([ny, nx])
for i_x in i_xs:
    for i_y in i_ys:
        csv = glob.glob(
            file_root
            + "*_"
            + str(i_x).zfill(2)
            + "x"
            + str(i_y).zfill(2)
            + "y//synthetic_catalog_relocated.csv"
        )[0]
        f = open(csv)
        lines = f.readlines()
        f.close()
        M0_amps[i_y, i_x] = float(lines[-1].split(",")[13])


fig, ax = plt.subplots()
ax.set_aspect("equal")
pcolor_plot = ax.pcolor(np.log10(M0_amps))
cb = fig.colorbar(pcolor_plot)
ax.set_xlabel("across pad")
ax.set_ylabel("along wells")
cb.set_label("log of M0 amplitude")
plt.show()
