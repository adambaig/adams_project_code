import glob
import importlib
import logging
import matplotlib.pyplot as plt
import numpy as np
import os
from progress.bar import IncrementalBar
from shutil import move
import sys

from pfi_fs.simulate_events import generate_1Csuperstation_waveforms

# from pfi_fs.imaging_arrivals import image_feasibility_seeds
# from pfi_fs.imaging_location_uncertainty import location_error_seeds

from pfi_fs.plotting import (
    plot_detectability,
    feasibility_fields,
    plot_wells_and_stations,
    plot_velocity_model,
    central_error_distributions,
    plot_colorbar,
)

from pfi_fs.make_inputs import (
    make_pseudorandom_station_grid,
    make_simulation_files,
    make_imaging_file,
    make_source_file,
)

parameter_file = "example_input_params.py"
sys.path.append(os.path.dirname(parameter_file))
input_file = importlib.import_module(
    os.path.basename(parameter_file.replace(".py", ""))
)

if len(input_file.file_prefix) > 0:
    if input_file.file_prefix[-1] != "_":
        input_file.file_prefix += "_"

station_files = make_pseudorandom_station_grid(input_file)
simulation_configs = make_simulation_files(input_file)
imaging_file = make_imaging_file(input_file)
source_curve = make_source_file(input_file, mode="curve")
n_map_points = (
    input_file.display_parameters["n_xy"][0] * input_file.display_parameters["n_xy"][1]
)


if input_file.do_steps["initial plotting"]:
    print("Plotting stations and well geometry...")
    plan_fig = plot_wells_and_stations(
        input_file, station_files, SI_units=input_file.display_parameters["SI_units"]
    )
    plan_fig.savefig("station_and_wells.png")
    plt.close(plan_fig)
    print("Plotting velocity model...")
    vel_fig = plot_velocity_model(
        input_file, SI_units=input_file.display_parameters["SI_units"]
    )
    vel_fig.savefig("velocity_model.png")
    plt.close(vel_fig)

det_curve_directories = []
for i_scenario, scenario in enumerate(input_file.scenarios):
    nx, ny = scenario["mn_stations"]
    n_stations = str(nx * ny).zfill(3)
    out_dir = (
        n_stations
        + "_stations_"
        + str(scenario["nodes_per_superstation"])
        + "_nodes_"
        + str(scenario["geophones_per_string"])
    )
    det_curve_directories.append(out_dir)


if input_file.do_steps["detectability curve modelling"]:
    print("Modelling for detectability curve...")
    for i_scenario, scenario in enumerate(input_file.scenarios):
        print("Writing synthetics to " + det_curve_directories[i_scenario])
        generate_1Csuperstation_waveforms(
            source_curve,
            simulation_configs[i_scenario],
            input_file.geology_config,
            station_files[i_scenario],
            directory_name=det_curve_directories[i_scenario],
            progress_bar=True,
            plot_out=input_file.display_parameters["waveform_plots"],
        )

if input_file.do_steps["detectability curve imaging"]:
    print("Imaging for detectability curve...")
    for i_scenario, scenario in enumerate(input_file.scenarios):
        print("Imaging synthetics from " + det_curve_directories[i_scenario])
        image_feasibility_seeds(
            det_curve_directories[i_scenario],
            imaging_file,
            input_file.n_threads,
            progress_bar=True,
        )

if input_file.do_steps["curve fitting"]:
    print("Determinining detectability parameters")
    os.makedirs("Detectability Curve")

    for directory in det_curve_directories:
        move(directory, "Detectability Curve" + os.sep + directory)

    os.makedirs("Detectability Map")

    fig = plot_detectability(input_file)
    fig.savefig("Detectability Curve" + os.sep + "detectability_curve.png")

if input_file.do_steps["detectability map modelling"]:
    source_files = [
        ["" for i in range(input_file.display_parameters["n_xy"][0])]
        for j in range(input_file.display_parameters["n_xy"][1])
    ]
    for i_across_well in range(input_file.display_parameters["n_xy"][0]):
        for j_along_well in range(input_file.display_parameters["n_xy"][1]):
            source_files[j_along_well][i_across_well] = make_source_file(
                input_file, dx=i_across_well, dy=j_along_well
            )

map_directories = [
    [
        ["" for i in range(input_file.display_parameters["n_xy"][0])]
        for j in range(input_file.display_parameters["n_xy"][1])
    ]
    for k in range(len(input_file.scenarios))
]

if input_file.do_steps["detectability map modelling"]:
    for i_scenario, scenario in enumerate(input_file.scenarios):
        nx, ny = scenario["mn_stations"]
        n_stations = str(nx * ny).zfill(3)
        scenario_summary = (
            n_stations
            + "_stations_"
            + str(scenario["nodes_per_superstation"])
            + "_nodes_"
            + str(scenario["geophones_per_string"])
        )
        print("Simulating M0 synthetics over map for " + scenario_summary + "...")
        bar = IncrementalBar(
            "Simulating", max=n_map_points, suffix="%(index)d/%(max)d %(eta)ds"
        )
        for i_across_well in range(input_file.display_parameters["n_xy"][0]):
            for j_along_well in range(input_file.display_parameters["n_xy"][1]):
                bar.next()
                position = (
                    "_"
                    + str(i_across_well).zfill(2)
                    + "x"
                    + str(j_along_well).zfill(2)
                    + "y"
                )
                map_directories[i_scenario][j_along_well][i_across_well] = (
                    scenario_summary + position
                )
                generate_1Csuperstation_waveforms(
                    source_files[j_along_well][i_across_well],
                    simulation_configs[i_scenario],
                    input_file.geology_config,
                    station_files[i_scenario],
                    directory_name=map_directories[i_scenario][j_along_well][
                        i_across_well
                    ],
                    plot_out=input_file.display_parameters["waveform_plots"],
                )

        bar.finish()

if input_file.do_steps["detectability map imaging"]:
    for i_scenario, scenario in enumerate(input_file.scenarios):
        nx, ny = scenario["mn_stations"]
        n_stations = str(nx * ny).zfill(3)
        scenario_summary = (
            n_stations
            + "_stations_"
            + str(scenario["nodes_per_superstation"])
            + "_nodes_"
            + str(scenario["geophones_per_string"])
        )
        print("Imaging M0 synthetics over map for " + scenario_summary + "...")
        bar = IncrementalBar(
            "Imaging", max=n_map_points, suffix="%(index)d/%(max)d %(eta)ds"
        )
        for i_across_well in range(input_file.display_parameters["n_xy"][0]):
            for j_along_well in range(input_file.display_parameters["n_xy"][1]):
                bar.next()
                image_feasibility_seeds(
                    map_directories[i_scenario][j_along_well][i_across_well],
                    imaging_file,
                    input_file.n_threads,
                )
        bar.finish()

    for directory in [
        item2
        for flat2 in [item1 for flat1 in map_directories for item1 in flat1]
        for item2 in flat2
    ]:
        move(directory, "Detectability Map" + os.sep + directory)

if input_file.do_steps["detectability plotting"]:
    feasibility_fig = feasibility_fields(
        input_file, SI_units=input_file.display_parameters["SI_units"]
    )
    feasibility_fig.savefig("detectability.png")
    M_colorbar = plot_colorbar(
        input_file.display_parameters["M_colorbar_min"],
        input_file.display_parameters["M_colorbar_max"],
        "detectability magnitude",
    )
    M_colorbar.savefig("detectability_colorbar.png")

error_directories = []
for i_scenario, scenario in enumerate(input_file.scenarios):
    nx, ny = scenario["mn_stations"]
    n_stations = str(nx * ny).zfill(3)
    out_dir = (
        n_stations
        + "_stations_"
        + str(scenario["nodes_per_superstation"])
        + "_nodes_"
        + str(scenario["geophones_per_string"])
        + "_error"
    )
    error_directories.append(out_dir)

if input_file.do_steps["error simulation"]:
    print("Modelling for location uncertainty...")
    error_source_file = make_source_file(
        input_file, input_file.location_error_simulations["M_simulation"]
    )
    error_simulation_configs = make_simulation_files(
        input_file, n_iterations=input_file.location_error_simulations["n_realizations"]
    )
    for i_scenario, scenario in enumerate(input_file.scenarios):
        print("Writing synthetics to " + error_directories[i_scenario])
        generate_1Csuperstation_waveforms(
            error_source_file,
            error_simulation_configs[i_scenario],
            input_file.geology_config,
            station_files[i_scenario],
            directory_name=error_directories[i_scenario],
            progress_bar=True,
        )

if input_file.do_steps["error imaging"]:
    print("Imaging for location uncertainty...")
    try:
        os.makedirs("Location Error")
    except:
        pass
    for i_scenario, scenario in enumerate(input_file.scenarios):
        location_error_seeds(
            error_directories[i_scenario],
            imaging_file,
            input_file.n_threads,
            progress_bar=True,
        )

if input_file.do_steps["error reporting"]:
    for directory in error_directories:
        try:
            move(directory, "Location Error" + os.sep + directory)
        except:
            pass
        image_stats = central_error_distributions(
            "Location Error" + os.sep + directory, input_file
        )
        print(directory)
        if input_file.display_parameters["SI_units"]:
            print(
                "horizontal uncertainty: %.1f m"
                % (
                    2
                    * np.median(
                        [
                            np.sqrt(i_s["error"]["x"] ** 2 + i_s["error"]["y"] ** 2)
                            for i_s in image_stats
                        ]
                    )
                )
            )
            print(
                "vertical uncertainty: %.1f m"
                % (2 * np.median([i_s["error"]["z"] for i_s in image_stats]))
            )
        else:
            print(
                "horizontal uncertainty: %.1f ft"
                % (
                    6.56
                    * np.median(
                        [
                            np.sqrt(i_s["error"]["x"] ** 2 + i_s["error"]["y"] ** 2)
                            for i_s in image_stats
                        ]
                    )
                )
            )
            print(
                "vertical uncertainty: %.1f ft"
                % (6.56 * np.median([i_s["error"]["z"] for i_s in image_stats]))
            )
