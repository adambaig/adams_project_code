# all units in SI unless otherwise noted.
# simulations are in a rotated coordinate frame, x --> across well, y --> along well

file_prefix = "EOG_it1"

scenarios = [
    # {"geophones_per_string": 6, "nodes_per_superstation": 13, "mn_stations": [3, 5]},
    {
        "geophones_per_string": 6,
        "nodes_per_superstation": 13,
        "mn_stations": [5, 6],
    },  # use this one
    {"geophones_per_string": 6, "nodes_per_superstation": 19, "mn_stations": [6, 10]},
    {"geophones_per_string": 6, "nodes_per_superstation": 19, "mn_stations": [10, 12]},
]

do_steps = {
    "initial plotting": False,
    "detectability curve modelling": False,
    "detectability curve imaging": False,
    "curve fitting": False,
    "detectability map modelling": False,
    "detectability map imaging": False,
    "detectability plotting": True,
    "error simulation": False,
    "error imaging": False,
    "error reporting": False,
}

geology_config = "geology.cfg"

n_threads = 4

well_dimensions = {
    "tvd": 3750,
    "pad_width": 500,
    "well_length": 1500,
    "stage_half_length": 100,
    "well_head_elevation": 119.634,
}

simulation = {
    "sample_rate": 0.002,
    "t_shift": 0,
    "trace_length": 5,
    "bandpass_freqs": [20, 50],
    "noise_ppsd": "PPSD_05pct_TX.EF04.~.HHZ_20200114T000000_20200128T235959.csv",
    "semblance_weighted_stack_factor": 10,
}

source = {
    "stress_drop": 3.0e5,
    "mechanism": [
        1,
        -1,
        0,
        0,
        0,
        0,
    ],  #  MT one of "explosion", "strike-slip", "dip-slip", "thrust", or specify [Mxx,Myy,Mzz,Mxy,Mxz,Myz]
    "min_magnitude": -3,
    "max_magnitude": 0,
    "magnitude_inc": 0.1,
}

imaging_parameters = {
    "resample_frequency": 125,
    "input_delta_z": 10,
    "input_starting_depth": -well_dimensions["well_head_elevation"],
    "max_hrz_distance": 10000,
    "hrz_spacing_tt_grid": 100,
    "static_sigma": 0.00,
    "n_image_grid": [50, 50, 50],
    "d_image_grid": [4, 4, 4],
    "pre_padding_samples": 125,
}

display_parameters = {
    "SI_units": False,  # Note, display only! All computation in SI regardless of value
    "grid_xy": [400, 400],
    "n_xy": [4, 6],
    "well_files": [
        "Willis_12H.csv",
        "Willis_13H.csv",
        "Willis_14H.csv",
        "Willis_15H.csv",
    ],
    "well_trend_deg": -6,
    "colors": ["firebrick", "royalblue", "forestgreen", "darkgoldenrod"],
    "undetectability": -2.4,
    "large_mag": -0.8,
    "waveform_plots": False,
    "M_colorbar_min": -1.7,
    "M_colorbar_max": -1.2,
    "elevation_range": [400, -15600],  # in display units (i.e. ft or m)
    "velocity_range": [0, 20000],  # ditto
    "well_center": {"easting": 2559679.2, "northing": 569605.25},
}

location_error_simulations = {"n_realizations": 40, "M_simulation": 0}
