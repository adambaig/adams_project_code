import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt

well_csvs = glob.glob("Willis*.csv")

wells = {}
for well_csv in well_csvs:
    well = well_csv.split("#")[1][:3]
    f = open(well_csv)
    [f.readline() for l in range(5)]
    lines = f.readlines()
    f.close()
    wells[well] = {
        "md": [float(l.split(",")[0]) for l in lines],
        "northing": [float(l.split(",")[8]) for l in lines],
        "easting": [float(l.split(",")[9]) for l in lines],
        "tvdss": [float(l.split(",")[12]) for l in lines],
    }


fig, ax = plt.subplots()
ax.set_facecolor("0.95")
ax.set_aspect("equal")
for well in wells.values():
    ax.plot(well["easting"], well["northing"], "firebrick", lw=3, zorder=9)
    ax.plot(well["easting"], well["northing"], "0.2", lw=2, zorder=10)
ax.set_xlabel("easting (ft)")
ax.set_ylabel("northing (ft)")
plt.show()
