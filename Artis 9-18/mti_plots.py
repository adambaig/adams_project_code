import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm
import mplstereonet as mpls
from obspy.imaging.mopad_wrapper import beach
import sys

sys.path.append("..")

from read_inputs import read_final_catalog, read_wells, read_treatment

treatment = read_treatment()


dir = "O:\\O&G\\Projects\\Artis_3-22_PFI_20200104\\EventCatalog\\"
well_dir = r"Inputs\well\PFIformat"
wells = read_wells(well_dir)
all_events = read_final_catalog("Final_Catalogtrim_500_depth1km.csv")

easting, northing, stage = np.array(
    [
        (v["easting"], v["northing"], v["stage"])
        for v in all_events.values()
        if v["well"] == "100/15-27-0"
    ]
).T

slickwater_events = {
    k: v
    for k, v in all_events.items()
    if v["well"] == "100/15-27-0" and v["stage"] < 19
}

gel_events = {
    k: v
    for k, v in all_events.items()
    if v["well"] == "100/15-27-0" and v["stage"] > 18
}

east_events = {k: v for k, v in all_events.items() if v["well"] == "102/15-27-0"}


def gray_background_with_grid(axis, grid_spacing=250):
    axis.set_facecolor("lightgrey")
    axis.set_aspect("equal")
    x1, x2 = axis.get_xlim()
    y1, y2 = axis.get_ylim()
    grid_spacing2 = 2 * grid_spacing
    x_minor_ticks = np.arange(
        np.floor(x1 / grid_spacing) * grid_spacing,
        np.ceil(x2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    x_major_ticks = np.arange(
        np.floor(x1 / grid_spacing2) * grid_spacing2,
        np.ceil(x2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    y_minor_ticks = np.arange(
        np.floor(y1 / grid_spacing) * grid_spacing,
        np.ceil(y2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    y_major_ticks = np.arange(
        np.floor(y1 / grid_spacing2) * grid_spacing2,
        np.ceil(y2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    axis.set_xticks(x_minor_ticks, minor=True)
    axis.set_yticks(y_minor_ticks, minor=True)
    axis.set_xticks(x_major_ticks, minor=False)
    axis.set_yticks(x_major_ticks, minor=False)

    axis.grid(which="both")
    axis.grid(which="minor", alpha=1)
    axis.set_xlim([x1, x2])
    axis.set_ylim([y1, y2])
    axis.set_yticklabels([])
    axis.set_xticklabels([])
    axis.tick_params(which="both", color=(0, 0, 0, 0))
    axis.set_axisbelow(True)

    return axis


for events in gel_events, slickwater_events, east_events, all_events:
    f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
    p_trend, p_plunge, b_trend, b_plunge, t_trend, t_plunge = np.array(
        [
            (
                v["p_trend"],
                v["p_plunge"],
                v["b_trend"],
                v["b_plunge"],
                v["t_trend"],
                v["t_plunge"],
            )
            for v in events.values()
            if "MT_DC" in v and v["DC_confidence"] > 95
        ]
    ).T
    a3[0].density_contourf(
        p_plunge, p_trend, cmap="Greens", measurement="lines", alpha=0.5
    )
    a3[0].line(p_plunge, p_trend, "k.", alpha=0.2)
    a3[0].grid()
    a3[0].set_title("P axis\n\n")
    a3[1].density_contourf(
        b_plunge, b_trend, cmap="Greens", measurement="lines", alpha=0.5
    )
    a3[1].line(b_plunge, b_trend, "k.", alpha=0.2)
    a3[1].grid()
    a3[1].set_title("B axis\n\n")
    a3[2].density_contourf(
        t_plunge, t_trend, cmap="Greens", measurement="lines", alpha=0.5
    )
    a3[2].line(t_plunge, t_trend, "k.", alpha=0.2)
    a3[2].grid()
    a3[2].set_title("T axis\n\n")

    strike1, dip1, rake1, strike2, dip2, rake2 = np.array(
        [
            (
                v["strike"],
                v["dip"],
                v["rake"],
                v["aux strike"],
                v["aux dip"],
                v["aux rake"],
            )
            for v in events.values()
            if "MT_DC" in v and v["DC_confidence"] > 95
        ]
    ).T
    f_fault, a_fault = mpls.subplots()
    strikes = np.hstack([strike1, strike2])
    dips = np.hstack([dip1, dip2])
    rakes = np.hstack([rake1, rake2])
    a_fault.pole(strikes, dips, "k.", alpha=0.2)
    a_fault.density_contourf(
        strikes,
        dips,
        measurement="poles",
        cmap="Reds",
        alpha=0.5,
        zorder=-1,
    )
    a_fault.grid()
    a_fault.set_title("Poles of both fault planes\n\n")


fig, ax = plt.subplots(figsize=[10, 16])


f2, a2 = plt.subplots(1, 2, figsize=[7, 16])

colors = ["orangered", "darkturquoise", "steelblue"]

for event in all_events.values():
    if "MT_DC" in event:
        if event["b_plunge"] > 45:
            event["cluster"] = 0
        else:
            if event["p_plunge"] > 30 and event["t_plunge"] > 30:
                event["cluster"] = 2
            else:
                event["cluster"] = 1

for i_cluster in range(3):
    for event in {
        k: v
        for k, v in all_events.items()
        if "MT_DC" in v and v["DC_confidence"] > 95 and v["cluster"] == i_cluster
    }.values():
        beachball = beach(
            (event["strike"] % 360, event["dip"], event["rake"]),
            xy=(event["easting"], event["northing"]),
            width=100 + 30 * (event["moment_magnitude"]),
            facecolor=colors[i_cluster],
            linewidth=0.25,
        )
        ax.add_collection(beachball)

for i_well, well_name in enumerate(np.unique([v["well"] for v in all_events.values()])):
    for i_cluster in range(3):
        for event in {
            k: v
            for k, v in all_events.items()
            if "MT_DC" in v
            and v["DC_confidence"] > 95
            and v["cluster"] == i_cluster
            and v["well"] == well_name
        }.values():
            beachball = beach(
                (event["strike"] % 360, event["dip"], event["rake"]),
                xy=(event["easting"], event["northing"]),
                width=100 + 30 * (event["moment_magnitude"]),
                facecolor=colors[i_cluster],
                linewidth=0.25,
            )
            a2[i_well].add_collection(beachball)
    for well in wells.values():
        a2[i_well].plot(well["easting"], well["northing"], lw=1, color="k")
    a2[i_well] = gray_background_with_grid(a2[i_well])
for well in wells.values():
    ax.plot(well["easting"], well["northing"], lw=1, color="k")
ax = gray_background_with_grid(ax)


plt.show()
