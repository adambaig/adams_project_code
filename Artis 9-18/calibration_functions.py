import numpy as np


def fit_data(ppvs, distances, mags, fit_type):

    n_rows = len(ppvs)
    if fit_type == "simple":
        # log(ppv * distance) = A + B * Mw
        Amatrix = np.vstack([np.ones(n_rows), mags]).T
        rhs = np.log10(ppvs) + np.log10(distances)
    elif fit_type == "geom_only":
        # log(ppv) = A + B * Mw + C * log(distance)
        Amatrix = np.vstack([np.ones(n_rows), mags, np.log10(distances)]).T
        rhs = np.log10(ppvs)
    elif fit_type == "include_anel":
        # log(ppv + distance) = A + B * Mw + C * distance
        Amatrix = np.vstack([np.ones(n_rows), mags, distances]).T
        rhs = np.log10(ppvs) + np.log10(distances)
    elif fit_type == "geom_and_anel":
        # log(ppv) = A + B * Mw + C * log(distance) + D*distance
        Amatrix = np.vstack([np.ones(n_rows), mags, np.log10(distances), distances]).T
        rhs = np.matrix(np.log10(ppvs))
    return np.linalg.inv(Amatrix.T @ Amatrix) @ Amatrix.T @ rhs.T


def predict_mag(ppv, distance, fit, fit_type):
    if fit_type == "simple":
        predicted_mag = (np.log10(ppv) + np.log10(distance) - fit[0]) / fit[1]
    elif fit_type == "geom_only":
        predicted_mag = (np.log10(ppv) - fit[2] * np.log10(distance) - fit[0]) / fit[1]
    elif fit_type == "include_anel":
        predicted_mag = (
            np.log10(ppv) + np.log10(distance) - fit[2] * distance - fit[0]
        ) / fit[1]
    elif fit_type == "geom_and_anel":
        predicted_mag = (
            np.log10(ppv) - fit[2] * np.log10(distance) - fit[3] * distance - fit[0]
        ) / fit[1]
    return predicted_mag
