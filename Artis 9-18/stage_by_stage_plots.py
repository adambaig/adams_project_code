# import matplotlib
#
# matplotlib.use("Qt5agg")
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
import sys

sys.path.append("..")

from read_inputs import read_final_catalog, read_wells, read_treatment, read_stages

well_dir = r"Inputs\well\PFIformat"
wells = read_wells(well_dir)
all_events = read_final_catalog("Final_Catalogtrim_500_depth1km.csv")
stages = read_stages()
tab10 = cm.get_cmap("tab10")
timestamp = all_events["1051"]["timestamp"]


def gray_background_with_grid(axis, grid_spacing=250):
    axis.set_facecolor("lightgrey")
    axis.set_aspect("equal")
    x1, x2 = axis.get_xlim()
    y1, y2 = axis.get_ylim()
    grid_spacing2 = 2 * grid_spacing
    x_minor_ticks = np.arange(
        np.floor(x1 / grid_spacing) * grid_spacing,
        np.ceil(x2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    x_major_ticks = np.arange(
        np.floor(x1 / grid_spacing2) * grid_spacing2,
        np.ceil(x2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    y_minor_ticks = np.arange(
        np.floor(y1 / grid_spacing) * grid_spacing,
        np.ceil(y2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    y_major_ticks = np.arange(
        np.floor(y1 / grid_spacing2) * grid_spacing2,
        np.ceil(y2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    axis.set_xticks(x_minor_ticks, minor=True)
    axis.set_yticks(y_minor_ticks, minor=True)
    axis.set_xticks(x_major_ticks, minor=False)
    axis.set_yticks(x_major_ticks, minor=False)
    axis.grid(which="both")
    axis.grid(which="minor", alpha=1)
    axis.set_xlim([x1, x2])
    axis.set_ylim([y1, y2])
    axis.set_yticklabels([])
    axis.set_xticklabels([])
    axis.tick_params(which="both", color=(0, 0, 0, 0))
    axis.set_axisbelow(True)
    return axis


def in_stage(timestamp):
    for stage_name, stage in stages.items():
        if stage["starttime"] < timestamp and stage["endtime"] > timestamp:
            return stage_name
    return None


event = all_events["1051"]
stage_name = in_stage(event["timestamp"])
stage = stages[stage_name]
for event in all_events.values():
    event["full_stage_name"] = in_stage(event["timestamp"])
stage_name = list(stages.keys())[0]
stage = stages[stage_name]


stage_name = "102/15-27-031-25W4_11"

for stage_name, stage in stages.items():
    before_events = {
        k: v for k, v in all_events.items() if v["timestamp"] < stage["starttime"]
    }
    stage_events = {
        k: v for k, v in all_events.items() if v["full_stage_name"] == stage_name
    }
    if len(stage_events) > 0:
        mod_stage = int(stage["unique_id"]) % 10
        stage_color = tab10((mod_stage + 0.1) / 10)
        fig, ax = plt.subplots(figsize=[9, 9])
        ax.plot(
            [v["easting"] for v in stage_events.values()],
            [v["northing"] for v in stage_events.values()],
            "o",
            c=stage_color,
            markeredgecolor="k",
            zorder=8,
        )

        x1, x2 = ax.get_xlim()
        y1, y2 = ax.get_ylim()

        for well in wells.values():
            ax.plot(well["easting"], well["northing"], lw=1, color="k")
        for stage_prime in {
            k: v
            for k, v in stages.items()
            if "_Part2" not in k and v["starttime"] < stage["starttime"]
        }.values():
            ax.plot(stage_prime["easting"], stage_prime["northing"], "k+", zorder=6)
        ax.plot(
            stage["easting"],
            stage["northing"],
            "+",
            color=stage_color,
            ms=10,
            lw=2,
            zorder=7,
        )
        ax.plot(
            stage["easting"],
            stage["northing"],
            "k+",
            lw=6,
            ms=10,
            zorder=6,
            markeredgecolor="k",
            markeredgewidth=1,
        )
        ax.plot(
            [v["easting"] for v in before_events.values()],
            [v["northing"] for v in before_events.values()],
            ".",
            c="0.5",
            zorder=-10,
        )
        x_perf = stage["easting"]
        y_perf = stage["northing"]

        if len(stage_events) == 1:
            x_event = [v["easting"] for v in stage_events.values()][0]
            y_event = [v["northing"] for v in stage_events.values()][0]
            xmin = min(x_event - 50, x_perf - 50)
            xmax = max(x_event + 50, x_perf + 50)
            ymin = min(y_event - 50, y_perf - 50)
            ymax = max(y_event + 50, y_perf + 50)
        else:
            xmin = min(x1 - 10, x_perf - 50)
            xmax = max(x2 + 10, x_perf + 50)
            ymin = min(y1 - 10, y_perf - 50)
            ymax = max(y2 + 10, y_perf + 50)
        ax.set_aspect("equal")
        ax.set_xlim([xmin, xmax])
        ax.set_ylim([ymin, ymax])
        ax.set_title(stage_name.replace("W4_", "W4 Stage ").replace("_", " "))
        ax.set_xlabel("grid spacing 50 m")
        ax = gray_background_with_grid(ax, grid_spacing=50)
        fig.savefig("figures/" + stage_name.replace("/", "_") + ".png")
        plt.close(fig)
