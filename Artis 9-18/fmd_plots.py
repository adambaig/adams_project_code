import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm
import mplstereonet as mpls
import sys

sys.path.append("..")

from read_inputs import read_final_catalog, read_wells, read_treatment
from bvalue_plot import plotFMD_from_dict

treatment = read_treatment()


dir = "O:\\O&G\\Projects\\Artis_3-22_PFI_20200104\\EventCatalog\\"
well_dir = r"Inputs\well\PFIformat"
wells = read_wells(well_dir)
all_events = read_final_catalog("Final_Catalogtrim_500_depth1km.csv")

easting, northing, stage = np.array(
    [
        (v["easting"], v["northing"], v["stage"])
        for v in all_events.values()
        if v["well"] == "100/15-27-0"
    ]
).T

slickwater_events = {
    k: v
    for k, v in all_events.items()
    if v["well"] == "100/15-27-0" and v["stage"] < 19
}

gel_events = {
    k: v
    for k, v in all_events.items()
    if v["well"] == "100/15-27-0" and v["stage"] > 18
}

east_events = {k: v for k, v in all_events.items() if v["well"] == "102/15-27-0"}

events_102_heel = {
    k: v
    for k, v in all_events.items()
    if v["well"] == "102/15-27-0" and v["stage"] > 29
}


def gray_background_with_grid(axis, grid_spacing=250):
    axis.set_facecolor("lightgrey")
    axis.set_aspect("equal")
    x1, x2 = axis.get_xlim()
    y1, y2 = axis.get_ylim()
    grid_spacing2 = 2 * grid_spacing
    x_minor_ticks = np.arange(
        np.floor(x1 / grid_spacing) * grid_spacing,
        np.ceil(x2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    x_major_ticks = np.arange(
        np.floor(x1 / grid_spacing2) * grid_spacing2,
        np.ceil(x2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    y_minor_ticks = np.arange(
        np.floor(y1 / grid_spacing) * grid_spacing,
        np.ceil(y2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    y_major_ticks = np.arange(
        np.floor(y1 / grid_spacing2) * grid_spacing2,
        np.ceil(y2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    axis.set_xticks(x_minor_ticks, minor=True)
    axis.set_yticks(y_minor_ticks, minor=True)
    axis.set_xticks(x_major_ticks, minor=False)
    axis.set_yticks(x_major_ticks, minor=False)

    axis.grid(which="both")
    axis.grid(which="minor", alpha=1)
    axis.set_xlim([x1, x2])
    axis.set_ylim([y1, y2])
    axis.set_yticklabels([])
    axis.set_xticklabels([])
    axis.tick_params(which="both", color=(0, 0, 0, 0))
    axis.set_axisbelow(True)

    return axis


def plot_events(ax, events, marker=".", color="0.5"):
    easting, northing, stage = np.array(
        [
            (v["easting"], v["northing"], v["stage"])
            for v in events.values()
            if v["well"] == well and stage == stage
        ]
    ).T
    ax.plot(
        easting,
        northing,
        zorder=10,
        color=color,
        marker=marker,
        markeredgecolor="k",
        linewidth=0.25,
    )
    return ax


colors = ["firebrick", "forestgreen", "#1f77b4"]
fig, ax = plt.subplots()
for i_group, events in enumerate(
    [gel_events, slickwater_events, east_events, events_102_heel]
):
    plotFMD_from_dict(events, key="moment_magnitude")


fig, ax = plt.subplots(figsize=[10, 16])


plt.show()
