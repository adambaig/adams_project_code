import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
import sys

sys.path.append("..")

from read_inputs import read_final_catalog, read_wells, read_treatment, read_stages

well_dir = r"Inputs\well\PFIformat"
wells = read_wells(well_dir)
all_events = read_final_catalog("Final_Catalogtrim_500_depth1km.csv")

stages = read_stages()


first_stage_name = "100/15-27-031-25W4_5"
second_stage_name = "100/15-27-031-25W4_6"

first_stage = stages[first_stage_name]
second_stage = stages[second_stage_name]

before_events = {
    k: v
    for k, v in all_events.items()
    if v["timestamp"] < stages[first_stage_name]["starttime"]
}

events_first_stage = {
    k: v
    for k, v in all_events.items()
    if v["well"] == first_stage_name[:11]
    and v["stage"] == int(first_stage_name.split("_")[-1])
}

events_second_stage = {
    k: v
    for k, v in all_events.items()
    if v["well"] == second_stage_name[:11]
    and v["stage"] == int(second_stage_name.split("_")[-1])
}

stage_color = ["goldenrod", "royalblue"]


def gray_background_with_grid(axis, grid_spacing=250):
    axis.set_facecolor("lightgrey")
    axis.set_aspect("equal")
    x1, x2 = axis.get_xlim()
    y1, y2 = axis.get_ylim()
    grid_spacing2 = 2 * grid_spacing
    x_minor_ticks = np.arange(
        np.floor(x1 / grid_spacing) * grid_spacing,
        np.ceil(x2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    x_major_ticks = np.arange(
        np.floor(x1 / grid_spacing2) * grid_spacing2,
        np.ceil(x2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    y_minor_ticks = np.arange(
        np.floor(y1 / grid_spacing) * grid_spacing,
        np.ceil(y2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    y_major_ticks = np.arange(
        np.floor(y1 / grid_spacing2) * grid_spacing2,
        np.ceil(y2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    axis.set_xticks(x_minor_ticks, minor=True)
    axis.set_yticks(y_minor_ticks, minor=True)
    axis.set_xticks(x_major_ticks, minor=False)
    axis.set_yticks(x_major_ticks, minor=False)

    axis.grid(which="both")
    axis.grid(which="minor", alpha=1)
    axis.set_xlim([x1, x2])
    axis.set_ylim([y1, y2])
    axis.set_yticklabels([])
    axis.set_xticklabels([])
    axis.tick_params(which="both", color=(0, 0, 0, 0))
    axis.set_axisbelow(True)

    return axis


f1, a1 = plt.subplots()
f2, a2 = plt.subplots()


for ax in [a1, a2]:
    ax.plot(
        [v["easting"] for v in events_first_stage.values()],
        [v["northing"] for v in events_first_stage.values()],
        "o",
        c=stage_color[0],
        markeredgecolor="k",
        zorder=8,
    )
a2.plot(
    [v["easting"] for v in events_second_stage.values()],
    [v["northing"] for v in events_second_stage.values()],
    "o",
    c=stage_color[1],
    markeredgecolor="k",
    zorder=9,
)
x1, x2 = a2.get_xlim()
y1, y2 = a2.get_ylim()

# well 102 stage 30 31 zoom in y1, y2 = 5730450, 5730742
# well 102 stage 37 38 zoom in y1, y2 = y1, 5730315
# well 100 stage 5 zoom in
y1 = 5731979
for ax in [a1, a2]:
    for well in wells.values():
        ax.plot(well["easting"], well["northing"], lw=1, color="k")
    for stage in {
        k: v
        for k, v in stages.items()
        if "_Part2" not in k and v["starttime"] < first_stage["starttime"]
    }.values():
        ax.plot(stage["easting"], stage["northing"], "k+", zorder=6)
    ax.plot(
        first_stage["easting"],
        first_stage["northing"],
        "+",
        color=stage_color[0],
        ms=10,
        lw=2,
        zorder=7,
    )
    ax.plot(
        first_stage["easting"],
        first_stage["northing"],
        "k+",
        lw=6,
        ms=10,
        zorder=6,
        markeredgecolor="k",
        markeredgewidth=1,
    )
    ax.plot(
        [v["easting"] for v in before_events.values()],
        [v["northing"] for v in before_events.values()],
        ".",
        c="0.5",
        zorder=-10,
    )
    if ax == a2:
        ax.plot(
            second_stage["easting"],
            second_stage["northing"],
            "k+",
            lw=6,
            ms=10,
            zorder=6,
            markeredgecolor="k",
            markeredgewidth=1,
        )
        ax.plot(
            second_stage["easting"],
            second_stage["northing"],
            "+",
            color=stage_color[1],
            lw=2,
            ms=10,
            zorder=7,
        )
    ax.set_xlim([x1, x2])
    ax.set_ylim([y1, y2])
    ax = gray_background_with_grid(ax, grid_spacing=50)


fig, ax = plt.subplots()
(stage_symbol_1,) = ax.plot(0, 0, "o", c=stage_color[0], markeredgecolor="k")
(stage_symbol_2,) = ax.plot(0, 1, "o", c=stage_color[1], markeredgecolor="k")
(old_event,) = ax.plot(
    0,
    2,
    ".",
    c="0.5",
    zorder=-10,
)
(perf_symbol_1,) = ax.plot(
    1,
    0,
    "+",
    color=stage_color[0],
    ms=10,
    lw=2,
    zorder=7,
)
(perf_symbol_1_back,) = ax.plot(
    1,
    0,
    "k+",
    ms=10,
    lw=6,
    zorder=6,
)

(perf_symbol_2,) = ax.plot(2, 0, "+", color=stage_color[1], ms=10, lw=2, zorder=7)
(perf_symbol_2_back,) = ax.plot(
    2,
    0,
    "k+",
    ms=10,
    lw=6,
    zorder=6,
)

(perf_symbol_old,) = ax.plot(3, 1, "k+", zorder=6)


ax.legend(
    [stage_symbol_1, old_event, (perf_symbol_1_back, perf_symbol_1), perf_symbol_old],
    ["Stage 37 Event", "Previous Event", "Stage 38 Perf", "Previous Perf"],
)

figg, axx = plt.subplots()
axx.legend(
    [
        stage_symbol_2,
        stage_symbol_1,
        old_event,
        (perf_symbol_2_back, perf_symbol_2),
        (perf_symbol_1_back, perf_symbol_1),
        perf_symbol_old,
    ],
    [
        "Stage 38 Event",
        "Stage 37 Event",
        "Previous Event",
        "Stage 38 Perf",
        "Stage 37 Perf",
        "Previous Perf",
    ],
)
axx.axis("off")

plt.show()
