import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
import sys

sys.path.append("..")
from bvalue_plot import plotFMD_from_dict
from read_inputs import read_pfi_catalog, read_events_from_Do, read_ppv
from calibration_functions import predict_mag, fit_data


snr_cut = 4  # minimum signal to noise to accept a ppv measurement
min_stations = (
    3  # minimum number of stations above snr_cut to use an event for ppv calibration
)

root_dir = r"C:\Users\adambaig\Project\Artis 9-18\Magnitude Calibration"

pfi_catalog = {
    k: v
    for k, v in read_pfi_catalog(
        root_dir
        + r"\EventCatalog\relocCatalog_wTM_wOWS_withinStagetime_wStageId_id_confidence_fine5_TrueStackAmp_uncer.csv"
    ).items()
    if v["elevation"] < -1100  # elevation cutoff probably not necessary in most cases
}


specfit_catalog = {
    k: v
    for k, v in read_events_from_Do(
        root_dir + r"\Magnitude\5_Spectral_Fitting\Tables\Do_final.csv"
    ).items()
    if v["depth"] > 1100
}

ground_motions = read_ppv(
    root_dir + r"\Magnitude\4_FAS_Analysis\Tables\FASDatTableALL.csv"
)

# sync up event ids for events within 0.1 seconds
for event_id, event in ground_motions.items():
    for pfi_id, pfi_event in pfi_catalog.items():
        if abs(event["timestamp"] - pfi_event["timestamp"]) < 0.1:
            event["image amplitude"] = pfi_event["raw_amplitude"]
            event["pfi_id"] = pfi_id

n_above_snr = {
    event_id: len(
        np.where(
            np.array(
                [
                    channel["snr"]
                    for channel_id, channel in event.items()
                    if channel_id[:2] == "XX"
                ]
            )
            > snr_cut
        )[0]
    )
    for event_id, event in ground_motions.items()
}

cull_mw_dictionary = {
    k: v for k, v in ground_motions.items() if n_above_snr[k] > min_stations
}

mags, pgv, snr, h_dist = np.array(
    [
        (
            specfit_catalog[event_id]["Mw"],
            channel["pgv"],
            channel["snr"],
            channel["hypocentral distance"],
        )
        for event_id, channels in cull_mw_dictionary.items()
        for channel_id, channel in channels.items()
        if channel_id[:2] == "XX"
        if (channel["snr"] > snr_cut and event_id in specfit_catalog)
    ]
).T

fit = fit_data(pgv, h_dist, mags, "simple")
mws, predict_mw_match = np.array(
    [
        (
            specfit_catalog[event]["Mw"],
            np.median(
                [
                    predict_mag(
                        channel["pgv"], channel["hypocentral distance"], fit, "simple"
                    )
                    for channel_id, channel in ground_motions[event].items()
                    if (channel_id[:2] == "XX" and channel["snr"] > snr_cut)
                ]
            ),
        )
        for event in specfit_catalog.keys()
        if n_above_snr[event] > min_stations
    ]
).T

fig1, ax1 = plt.subplots()
ax1.set_aspect("equal")
ax1.plot(mws, predict_mw_match, "o")
ax1.plot([-1, 0.3], [-1, 0.3], "k:")
ax1.set_xlim([-1, 0.3])
ax1.set_ylim([-1, 0.3])
ax1.set_xlabel("measured spectral fit magnitude")
ax1.set_ylabel("calibrated ppv magnitude")

predicted_mag, median_snr, image_amps_pgv = np.array(
    [
        (
            np.median(
                [
                    predict_mag(
                        channel["pgv"], channel["hypocentral distance"], fit, "simple"
                    )
                    for channel_id, channel in event.items()
                    if channel_id[:2] == "XX"
                ]
            ),
            np.median(
                [
                    channel["snr"]
                    for channel_id, channel in event.items()
                    if (channel_id[:2] == "XX" and channel["snr"] > snr_cut)
                ]
            ),
            pfi_catalog[event["pfi_id"]]["raw_amplitude"],
        )
        for event_id, event in ground_motions.items()
        if (
            n_above_snr[event_id] > min_stations
            and "pfi_id" in event
            and event["pfi_id"] in pfi_catalog
        )
    ]
).T

slope, y_int = np.polyfit(predicted_mag, np.log10(image_amps_pgv), 1)

fig, ax = plt.subplots()

scatterplot = ax.scatter(
    predicted_mag,
    image_amps_pgv,
    c=median_snr,
    marker="o",
    cmap="magma",
    zorder=3,
    vmax=6,
    vmin=1,
    edgecolor="k",
)
x1, x2 = ax.get_xlim()
x_mags = np.linspace(x1, x2, 100)

model_quad = -0.2740683228859524  ### from feasibility modelling
model_linear = 2.534516496227919

model_yint_sample = []
for i_event, amp in enumerate(image_amps_pgv):
    model_yint_sample.append(
        np.log10(amp)
        - model_quad * predicted_mag[i_event] ** 2
        - model_linear * predicted_mag[i_event]
    )

model_yint = np.median(model_yint_sample)

# ax.plot(x_mags, 10 ** (slope * x_mags + y_int))
ax.plot(
    x_mags, 10 ** (model_quad * x_mags * x_mags + model_linear * x_mags + model_yint)
)
ax.set_yscale("log")
# ax.set_ylim([1e-8, 1])
ax.set_xlabel("PGV calibrated magnitude")
ax.set_ylabel("image amplitude")
cb = fig.colorbar(scatterplot, extend="both")
cb.set_label("median PGV signal to noise")


# def mag_from_image_amp(event):
#     return min(np.roots([slope, y_int - np.log10(event["raw_amplitude"])]))


def mag_from_image_amp(event):
    return min(
        np.roots(
            [model_quad, model_linear, model_yint - np.log10(event["raw_amplitude"])]
        )
    )


for event_id, event in pfi_catalog.items():
    if event_id in specfit_catalog:
        pfi_catalog[event_id]["mw"] = specfit_catalog[event_id]["Mw"]
    else:
        pfi_catalog[event_id]["mw"] = mag_from_image_amp(event)


plotFMD_from_dict(pfi_catalog)

plt.show()
