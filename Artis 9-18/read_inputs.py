from datetime import datetime
import glob
import numpy as np
from obspy import UTCDateTime
import os
import pyproj as pr

from NocMeta.Meta import NOC_META
from sms_moment_tensor.MT_math import mt_vector_to_matrix


latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:3402")


def read_pfi_catalog(filepath):
    f = open(filepath)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        events[event_id] = {
            "timestamp": UTCDateTime(lspl[1]),
            "easting": float(lspl[2]),
            "northing": float(lspl[3]),
            "elevation": -float(lspl[4]),
            "SNR": float(lspl[5]),
            "sws_amplitude": float(lspl[6]),
            "raw_amplitude": float(lspl[7]),
        }
    return events


def read_pfi_mt_catalog(filepath):
    f = open(filepath)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        events[event_id] = {
            "timestamp": UTCDateTime(lspl[1]),
            "easting": float(lspl[2]),
            "northing": float(lspl[3]),
            "elevation": -float(lspl[4]),
            "SNR": float(lspl[5]),
            "sws_amplitude": float(lspl[6]),
            "raw_amplitude": float(lspl[7]),
            "MT_gen": mt_vector_to_matrix([float(s) for s in lspl[8:14]]),
            "MT_gen_R": float(lspl[14]),
            "MT_gen_CN": float(lspl[15]),
            "MT_DC": mt_vector_to_matrix([float(s) for s in lspl[16:22]]),
            "MT_DC_R": float(lspl[22]),
            "MT_DC_CN": float(lspl[23]),
            "well": lspl[24],
            "well_id": int(lspl[25]),
            "stage": int(lspl[26]),
            "well_stage": int(lspl[27]),
            "DC_confidence": float(lspl[28]),
            "easting_err": float(lspl[29]),
            "northing_err": float(lspl[30]),
            "elevation_err": float(lspl[31]),
        }
    return events


def read_pfi_mt_noise(filepath):
    f = open(filepath)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        events[event_id] = {
            "timestamp": UTCDateTime(lspl[1]),
            "easting": float(lspl[2]),
            "northing": float(lspl[3]),
            "elevation": -float(lspl[4]),
            "MT_gen": mt_vector_to_matrix([float(s) for s in lspl[5:11]]),
            "MT_gen_R": float(lspl[11]),
            "MT_gen_CN": float(lspl[12]),
            "MT_DC": mt_vector_to_matrix([float(s) for s in lspl[13:19]]),
            "MT_DC_R": float(lspl[19]),
            "MT_DC_CN": float(lspl[20]),
        }
    return events


def read_events_from_Do(filepath):
    f = open(filepath)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        id = lspl[0].split(".")[0]
        if lspl[-1][0] == "y":
            events[id] = {}
            events[id]["UTC"] = UTCDateTime(lspl[1])
            lat, lon = [float(s) for s in lspl[2:4]]
            # events[id]["easting"], events[id]["northing"], dum1, dum2 = utm.from_latlon(
            #     lat, lon
            # )
            events[id]["depth"] = 1000 * float(lspl[4])
            events[id]["Mw"] = float(lspl[-9])
    return events


def read_ppv(filepath):
    f = open(filepath)
    head = f.readline()
    lines = f.readlines()
    f.close()

    pgv = {}
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0].split(".")[0]
        if event_id not in pgv:
            pgv[event_id] = {"timestamp": UTCDateTime(lspl[1])}
        channel = lspl[7]
        pgv[event_id][channel] = {
            "hypocentral distance": float(lspl[13]) * 1000.0,
            "azimuth": float(lspl[14]),
            "snr": float(lspl[15]),
            "pga": float(lspl[19]) / 100.0,
            "pgv": float(lspl[20]) / 100.0,
            "pgd": float(lspl[21]) / 100.0,
        }
    return pgv


def read_wells(well_dir, from_latlot=False):
    well_files = glob.glob(well_dir + "//*.well")
    wells = {}
    for well_file in well_files:
        well_name = os.path.split(well_file)[-1].split(".")[0]
        f = open(well_file)
        head = f.readline()
        lines = f.readlines()
        f.close()
        if from_latlot:
            n_line = len(lines)
            east, north, elevation = (
                np.zeros(n_line),
                np.zeros(n_line),
                np.zeros(n_line),
            )
            for i_line, line in enumerate(lines):
                lspl = line.split(",")
                east[i_line], north[i_line] = pr.transform(
                    latlon_proj, out_proj, float(lspl[2]), float(lspl[1])
                )
                elevation[i_line] = -float(lspl[0])
            wells[well_name] = {
                "easting": east,
                "northing": north,
                "elevation": elevation,
            }
        else:
            wells[well_name] = {
                "easting": np.array([float(line.split(",")[1]) for line in lines]),
                "northing": np.array([float(line.split(",")[2]) for line in lines]),
                "elevation": -np.array([float(line.split(",")[0]) for line in lines]),
            }
    return wells


def read_final_catalog(catalog_file):
    f = open(catalog_file)
    events = {}
    head = [f.readline() for ii in range(3)]
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        if event_id[:5] != "90000":
            events[event_id] = {
                "timestamp": UTCDateTime(lspl[1]),
                "easting": float(lspl[2]),
                "northing": float(lspl[3]),
                "elevation": -float(lspl[4]),
                "easting_err": float(lspl[5]),
                "northing_err": float(lspl[6]),
                "elevation_err": float(lspl[7]),
                "well": lspl[8],
                "well_id": int(lspl[9]),
                "stage": int(lspl[10]),
                "well_stage": int(lspl[11]),
                "SNR": float(lspl[12]),
                "raw_amplitude": float(lspl[13]),
                "moment_magnitude": float(lspl[14]),
                "seismic_moment": float(lspl[15]),
                "MT_gen": mt_vector_to_matrix([float(s) for s in lspl[16:22]]),
                "MT_gen_R": float(lspl[22]),
                "MT_gen_CN": float(lspl[23]),
                "dc_percent": float(lspl[24]),
                "clvd_percent": float(lspl[25]),
                "iso_percent": float(lspl[26]),
                "MT_DC": mt_vector_to_matrix([float(s) for s in lspl[27:33]]),
                "MT_DC_R": float(lspl[33]),
                "MT_DC_CN": float(lspl[34]),
                "DC_confidence": float(lspl[35]),
                "strike": float(lspl[36]),
                "dip": float(lspl[37]),
                "rake": float(lspl[38]),
                "aux strike": float(lspl[39]),
                "aux dip": float(lspl[40]),
                "aux rake": float(lspl[41]),
                "p_trend": float(lspl[42]),
                "p_plunge": float(lspl[43]),
                "b_trend": float(lspl[44]),
                "b_plunge": float(lspl[45]),
                "t_trend": float(lspl[46]),
                "t_plunge": float(lspl[47]),
            }
        else:
            events[event_id] = {
                "timestamp": UTCDateTime(lspl[1]),
                "easting": float(lspl[2]),
                "northing": float(lspl[3]),
                "elevation": -float(lspl[4]),
                "easting_err": float(lspl[5]),
                "northing_err": float(lspl[6]),
                "elevation_err": float(lspl[7]),
                "well": lspl[8],
                "well_id": int(lspl[9]),
                "stage": int(lspl[10]),
                "well_stage": int(lspl[11]),
                "SNR": float(lspl[12]),
                "raw_amplitude": float(lspl[13]),
                "moment_magnitude": float(lspl[14]),
                "seismic_moment": float(lspl[15]),
            }
    return events


def read_treatment():
    well_dir = "Inputs\\pump_data\\"
    data = {}
    for well_csv in glob.glob(well_dir + "*.csv"):
        f = open(well_csv)
        head = f.readline()
        lines = f.readlines()
        f.close()
        well = os.path.split(well_csv)[1].split()[0]
        for line in lines:
            lspl = line.split(",")
            if len(lspl) == 19:
                dt = datetime.strptime(lspl[18].replace("\n", ""), "%Y-%m-%d %H:%M:%S")
                timestamp = datetime.strftime(dt, "%Y%m%d%H%M%S.000000")
                data[timestamp] = {
                    "well": well,
                    "stage": lspl[4],
                    "pressure": float(lspl[5]),
                    "slurry_rate": float(lspl[14]),
                    "proppant_conc": float(lspl[15]),
                }
            else:
                dt = datetime.strptime(lspl[16].replace("\n", ""), "%Y-%m-%d %H:%M:%S")
                timestamp = datetime.strftime(dt, "%Y%m%d%H%M%S.000000")
                data[timestamp] = {
                    "well": well,
                    "stage": lspl[4],
                    "pressure": float(lspl[5]),
                    "slurry_rate": float(lspl[13]),
                    "proppant_conc": float(lspl[14]),
                }
    return data


def read_stages():
    well_file = r"Inputs\Stage_time_all_epsg26911_wStageId.csv"
    f = open(well_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    stages = {}

    for line in lines:
        lspl = line.split(",")
        well_stage = lspl[0] + "_" + lspl[1]
        if well_stage in stages:
            well_stage += "_Part2"
        stages[well_stage] = {
            "easting": float(lspl[2]),
            "northing": float(lspl[3]),
            "elevation": -float(lspl[4]),
            "starttime": UTCDateTime(lspl[5]),
            "endtime": UTCDateTime(lspl[6]),
            "unique_id": int(lspl[7]),
        }
    return stages


def read_ISM_stations_locations():
    file_name = r"Inputs\ISM\Artis_Stations.csv"

    f = open(file_name)
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}

    for line in lines:
        lspl = line.split(",")
        station_name = ".".join(lspl[:2])
        if station_name not in stations:
            east, north = pr.transform(
                latlon_proj, out_proj, float(lspl[8]), float(lspl[7])
            )
            stations[station_name] = {
                "easting": east,
                "northing": north,
                "elevation": float(lspl[9]),
            }

    return stations


def read_ISM_events():
    file_name = r"Inputs\ISM\Artis_EventSummary.csv"
    f = open(file_name)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        east, north = pr.transform(
            latlon_proj, out_proj, float(lspl[7]), float(lspl[6])
        )

        events[lspl[1]] = {
            "easting": east,
            "northing": north,
            "magnitude": float(lspl[10]),
            "elevation": -1000 * float(lspl[12]),
        }

    return events
