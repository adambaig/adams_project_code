import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
from obspy.imaging.mopad_wrapper import beach
import sys

from sms_moment_tensor.MT_math import (
    mt_to_sdr,
    decompose_MT,
    trend_plunge_to_unit_vector,
    clvd_iso_dc,
    mt_matrix_to_vector,
)

sys.path.append("..")

from read_inputs import (
    read_pfi_mt_catalog,
    read_pfi_mt_noise,
    read_wells,
    read_treatment,
)

mt_file = r"Inputs\relocCatalog_wTM_wOWS_withinStagetime_wStageId_id_confidence_fine5_TrueStackAmp_uncer.csv"
well_dir = r"Inputs\well\PFIformat"

wells = read_wells(well_dir)
events = read_pfi_mt_catalog(mt_file)


def calibrate_magnitude(stack_amplitude):
    quad = -0.2740683228859524
    linear = 2.534516496227919
    yint = -1.569257503233548
    return min(np.roots([quad, linear, yint - np.log10(stack_amplitude)]))


def moment(magnitude):
    return 10 ** (1.5 * magnitude + 9)


def scaled_mt(moment_tensor, magnitude):
    moment = 10 ** (1.5 * magnitude + 9)
    return moment_tensor * moment / np.linalg.norm(moment_tensor)


def gray_background_with_grid(axis, grid_spacing=250):
    axis.set_facecolor("lightgrey")
    axis.set_aspect("equal")
    x1, x2 = axis.get_xlim()
    y1, y2 = axis.get_ylim()
    grid_spacing2 = 2 * grid_spacing
    x_minor_ticks = np.arange(
        np.floor(x1 / grid_spacing) * grid_spacing,
        np.ceil(x2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    x_major_ticks = np.arange(
        np.floor(x1 / grid_spacing2) * grid_spacing2,
        np.ceil(x2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    y_minor_ticks = np.arange(
        np.floor(y1 / grid_spacing) * grid_spacing,
        np.ceil(y2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    y_major_ticks = np.arange(
        np.floor(y1 / grid_spacing2) * grid_spacing2,
        np.ceil(y2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    axis.set_xticks(x_minor_ticks, minor=True)
    axis.set_yticks(y_minor_ticks, minor=True)
    axis.set_xticks(x_major_ticks, minor=False)
    axis.set_yticks(x_major_ticks, minor=False)

    axis.grid(which="both")
    axis.grid(which="minor", alpha=1)
    axis.set_xlim([x1, x2])
    axis.set_ylim([y1, y2])
    axis.set_yticklabels([])
    axis.set_xticklabels([])
    axis.tick_params(which="both", color=(0, 0, 0, 0))
    axis.set_axisbelow(True)

    return axis


for event in events.values():
    event["magnitude"] = calibrate_magnitude(event["raw_amplitude"])
    event["moment"] = moment(event["magnitude"])
    event["MT_DC"] = scaled_mt(event["MT_DC"], event["magnitude"])
    event["MT_gen"] = scaled_mt(event["MT_gen"], event["magnitude"])


noise_file = r"Inputs\relocNoiseMTs_5m.csv"
noise = read_pfi_mt_noise(noise_file)
ref_direction = 45
ref_unit_vector = [
    np.sin(ref_direction * np.pi / 180),
    np.cos(ref_direction * np.pi / 180),
]
ref_ds_unit_vector = trend_plunge_to_unit_vector(310, 45)
for event in {k: v for k, v in events.items() if "MT_DC" in v}.values():
    strike, dip, rake, aux_strike, aux_dip, aux_rake = mt_to_sdr(event["MT_DC"])
    decomp = decompose_MT(event["MT_DC"])
    p_vector = trend_plunge_to_unit_vector(decomp["p_trend"], decomp["p_plunge"])
    p_2vector = p_vector[:2] / np.linalg.norm(p_vector[:2])
    if decomp["b_plunge"] > 45:  # strike slip
        event["cluster"] = 0
        if np.abs(np.dot(p_2vector, ref_unit_vector)) > np.sqrt(
            0.5
        ):  # flip if p_trend in NE-SW direction
            event["DC_flip"] = event["MT_DC"]
            event["gen_flip"] = event["MT_gen"]
        else:
            event["DC_flip"] = -event["MT_DC"]
            event["gen_flip"] = -event["MT_gen"]
    else:
        if decomp["p_plunge"] > 45:
            event["DC_flip"] = -event["MT_DC"]
            event["gen_flip"] = -event["MT_gen"]
        else:
            event["DC_flip"] = event["MT_DC"]
            event["gen_flip"] = event["MT_gen"]
        flip_decomp = decompose_MT(event["DC_flip"])
        p_vector = trend_plunge_to_unit_vector(
            flip_decomp["p_trend"], flip_decomp["p_plunge"]
        )
        #        if np.dot(ref_ds_unit_vector, p_vector)<0.87:
        if decomp["p_plunge"] > 30 and decomp["t_plunge"] > 30:
            event["cluster"] = 2
        else:
            event["cluster"] = 1
    # event["DC_flip"] = event["MT_DC"]
    strike, dip, rake, aux_strike, aux_dip, aux_rake = mt_to_sdr(event["DC_flip"])
    decomp_flip = decompose_MT(event["DC_flip"])
    clvd, iso, dc = clvd_iso_dc(event["gen_flip"])
    event["p_trend"] = decomp_flip["p_trend"]
    event["b_trend"] = decomp_flip["b_trend"]
    event["t_trend"] = decomp_flip["t_trend"]
    event["p_plunge"] = decomp_flip["p_plunge"]
    event["b_plunge"] = decomp_flip["b_plunge"]
    event["t_plunge"] = decomp_flip["t_plunge"]
    event["strike"] = strike
    event["dip"] = dip
    event["rake"] = rake
    event["aux_strike"] = aux_strike
    event["aux_dip"] = aux_dip
    event["aux_rake"] = aux_rake
    event["clvd"] = clvd
    event["iso"] = iso
    event["dc"] = dc

for noise_sample in noise.values():
    decomp = decompose_MT(noise_sample["MT_DC"])
    noise_sample["p_trend"] = decomp["p_trend"]
    noise_sample["b_trend"] = decomp["b_trend"]
    noise_sample["t_trend"] = decomp["t_trend"]
    noise_sample["p_plunge"] = decomp["p_plunge"]
    noise_sample["b_plunge"] = decomp["b_plunge"]
    noise_sample["t_plunge"] = decomp["t_plunge"]
    noise_sample["strike"] = strike
    noise_sample["dip"] = dip
    noise_sample["rake"] = rake

for i_cluster in range(3):
    p_plunge, p_trend = np.array(
        [
            (v["p_plunge"], v["p_trend"])
            for k, v in events.items()
            if v["DC_confidence"] > 0.95
            and k[:5] != "90000"
            and v["SNR"] > 1
            and v["cluster"] == i_cluster
        ]
    ).T
    b_plunge, b_trend = np.array(
        [
            (v["b_plunge"], v["b_trend"])
            for k, v in events.items()
            if v["DC_confidence"] > 0.95
            and k[:5] != "90000"
            and v["SNR"] > 1
            and v["cluster"] == i_cluster
        ]
    ).T
    t_plunge, t_trend = np.array(
        [
            (v["t_plunge"], v["t_trend"])
            for k, v in events.items()
            if v["DC_confidence"] > 0.95
            and k[:5] != "90000"
            and v["SNR"] > 1
            and v["cluster"] == i_cluster
        ]
    ).T
    #
    # p_plunge, p_trend = np.array([(v["p_plunge"], v["p_trend"]) for k,v in noise.items() ]).T
    # b_plunge, b_trend = np.array([(v["b_plunge"], v["b_trend"]) for k,v in noise.items() ]).T
    # t_plunge, t_trend = np.array([(v["t_plunge"], v["t_trend"]) for k,v in noise.items() ]).T

    fig, ax = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = ax[0].density_contourf(
        p_plunge, p_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    ax[0].line(p_plunge, p_trend, "k.")
    cax_b = ax[1].density_contourf(
        b_plunge, b_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    ax[1].line(b_plunge, b_trend, "k.")
    cax_t = ax[2].density_contourf(
        t_plunge, t_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    ax[2].line(t_plunge, t_trend, "k.")


fig, ax = plt.subplots(figsize=[10, 16])

colors = ["orangered", "darkturquoise", "steelblue"]

for i_cluster in range(3):
    for event in {
        k: v
        for k, v in events.items()
        if v["DC_confidence"] > 95
        and k[:5] != "90000"
        and v["SNR"] > 1
        and v["cluster"] == i_cluster
    }.values():
        beachball = beach(
            (event["strike"] % 360, event["dip"], event["rake"]),
            xy=(event["easting"], event["northing"]),
            width=100 + 30 * (event["magnitude"]),
            facecolor=colors[i_cluster],
            linewidth=0.25,
        )
        ax.add_collection(beachball)
for well in wells.values():
    ax.plot(well["easting"], well["northing"], lw=2, color="0.4", markeredgecolor="k")
ax = gray_background_with_grid(ax)


f2, a2 = plt.subplots(1, 2, figsize=[10, 16], sharex=True, sharey=True)


for i_well in range(2):
    a2[i_well].set_aspect("equal")

    for i_cluster in range(3):
        for event in {
            k: v
            for k, v in events.items()
            if v["DC_confidence"] > 95
            and k[:5] != "90000"
            and v["SNR"] > 1
            and v["cluster"] == i_cluster
            and v["well_id"] == i_well + 1
        }.values():
            beachball = beach(
                (event["strike"] % 360, event["dip"], event["rake"]),
                xy=(event["easting"], event["northing"]),
                width=100 + 30 * (event["magnitude"]),
                facecolor=colors[i_cluster],
                linewidth=0.25,
            )
            a2[i_well].add_collection(beachball)
    for well in wells.values():
        a2[i_well].plot(
            well["easting"], well["northing"], lw=2, color="0.4", markeredgecolor="k"
        )
    a2[i_well] = gray_background_with_grid(a2[i_well])
    a2[i_well].set_title(event["well"] + " completion")


def write_catalog(events, file_name):
    f = open(file_name, "w")
    f.write("Location,,,,,,,,Stage,,,,Source and Image Parameters,,,,")
    f.write("General MT solutions,,,,,,,,,,,DC constrained MT solutions,,,,,,,,,")
    f.write(
        "DC Nodal Plane 1,,,DC Nodal Plane 2,,, P strain axis,, B strain Axis,, T strain axis,\n"
    )
    f.write("event ID, T0, Easting, Northing, TVDss,")
    f.write(
        "Easting location uncertainty, Northing location uncertainty, depth location uncertainty,"
    )
    f.write("Well Name, Well ID, Stage, Unique Well Stage ID,")
    f.write("Stack Signal to Noise, Stack Amplitude, Moment Magnitude, Seismic Moment,")
    f.write("Mee,Mnn,Mzz,Men,Mez,Mnz,Pearson R,Condition Number,")
    f.write("DC Component, CLVD Component,ISO component,")
    f.write("Mee,Mnn,Mzz,Men,Mez,Mnz,Pearson R,Condition Number,Confidence")
    [f.write(",Strike,Dip,Rake") for ii in range(2)]
    [f.write(",Trend,Plunge") for ii in range(3)]
    f.write("\n")
    f.write(",YYYY-MM-DD HH:MM:SS.000,")
    [f.write("(m),") for ii in range(6)]
    f.write(",,,,,,,")
    [f.write("(Nm),") for ii in range(7)]
    f.write(",,%,%,%")
    [f.write(",(Nm)") for ii in range(6)]
    f.write(",,,%")
    [f.write(",deg") for ii in range(12)]
    f.write("\n")
    for event_id, event in events.items():
        f.write(event_id + event["timestamp"].strftime(",%Y-%m-%d %H:%M:%S.%f")[:-3])
        f.write(
            f",{event['easting']:.1f}, {event['northing']:.1f}, {-event['elevation']:.1f},"
        )
        f.write(
            f"{event['easting_err']:.1f}, {event['northing_err']:.1f}, {-event['elevation_err']:.1f},"
        )
        f.write(
            f"{event['well']}, {event['well_id']}, {event['stage']}, {event['well_stage']},"
        )
        f.write(f"{event['SNR']:.2e}, {event['raw_amplitude']:.3e},")
        f.write(f"{event['magnitude']:.2f},{event['moment']:.3e},")
        if event_id[:5] == "90000":
            [f.write("-,") for ii in range(31)]
            f.write("-\n")
        else:
            [f.write(f"{m:.3e},") for m in mt_matrix_to_vector(event["gen_flip"])]
            f.write(f"{event['MT_gen_R']:.3f},{event['MT_gen_CN']:.1f},")
            f.write(f"{event['dc']:.1f},{event['clvd']:.1f},{event['iso']:.1f},")
            [f.write(f"{m:.3e},") for m in mt_matrix_to_vector(event["DC_flip"])]
            f.write(
                f"{event['MT_DC_R']:.3f},{event['MT_DC_CN']:.1f},{event['DC_confidence']*100:.1f},"
            )
            f.write(
                f"{event['strike']%360:.1f},{event['dip']:.1f},{event['rake']:.1f},"
            )
            f.write(
                f"{event['aux_strike']%360:.1f},{event['aux_dip']:.1f},{event['aux_rake']:.1f},"
            )
            f.write(f"{event['p_trend']%360:.1f},{event['p_plunge']:.1f},")
            f.write(f"{event['b_trend']%360:.1f},{event['b_plunge']:.1f},")
            f.write(f"{event['t_trend']%360:.1f},{event['t_plunge']:.1f}\n")
    return None


f2.savefig("confidence_95_both_wells.png", bbox_inches="tight")
fig.savefig("confidence_95.png", bbox_inches="tight")

write_catalog(events, "Final_Catalog.csv")


noise_r = [v["MT_DC_R"] for v in noise.values()]
plt.plot(
    np.hstack([0, np.sort(noise_r), 1]),
    np.hstack([0, np.cumsum(noise_r) / sum(noise_r), 1]),
)
plt.show()


#
#
# plt.semilogx([v["SNR"] for v in events.values()],[v["DC_confidence"] for v in events.values()],'.')
# plt.show()
