import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np

from obspy import read, UTCDateTime


waveform = r"Magnitude Calibration\Event_Check\20200110.223000.000000_20200110.223500.000000.seed"
st = read(waveform)

st.trim(
    starttime=UTCDateTime("2020-01-10T22:30:28.684000Z"),
    endtime=UTCDateTime("2020-01-10T22:30:33.684000Z"),
)

st.plot()
