import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
import sys

from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge, strike_dip_to_normal
from sms_moment_tensor.stress_inversions import (
    iterate_Micheal_stress_inversion,
    decompose_stress,
    most_unstable_sdr,
    resolve_shear_and_normal_stress,
)

sys.path.append("..")

from read_inputs import read_final_catalog

PI = np.pi

n_iterations = 100
confidence_threshold = 95
events = read_final_catalog("Final_Catalog.csv")
axis_color = {"s1": "firebrick", "s2": "forestgreen", "s3": "royalblue"}

r_bins = np.linspace(0, 1, 101)
circle_x, circle_y = [], []
for i_rad in np.arange(0, PI + PI / 100, PI / 100):
    circle_x.append(np.cos(i_rad))
    circle_y.append(np.sin(i_rad))
circle_x = np.array(circle_x)
circle_y = np.array(circle_y)


colors = ["orangered", "darkturquoise", "steelblue"]

for well_id in range(1, 3):
    subset = {
        k: v
        for k, v in events.items()
        if "MT_DC" in v
        and v["DC_confidence"] > confidence_threshold
        and v["well_id"] == well_id
    }
    n_events = len(subset)
    best_stress_tensor, stress_tensors = iterate_Micheal_stress_inversion(
        subset, n_iterations, output_iterations=True
    )

    fig, ax = mpls.subplots(figsize=[2, 2])
    f_hist, a_hist = plt.subplots(figsize=[3, 2])
    r_iterations, axes_iteration = [], []
    for i_iteration in range(n_iterations):
        R, stress_axes = decompose_stress(stress_tensors[i_iteration])
        r_iterations.append(R)
        axes_iteration.append(stress_axes)
        for axis, vector in stress_axes.items():
            trend, plunge = unit_vector_to_trend_plunge(vector)
            ax.line(
                plunge,
                trend,
                ".",
                alpha=0.05,
                color=axis_color[axis],
                zorder=3,
            )

    a_hist.hist(
        r_iterations,
        r_bins,
        facecolor="seagreen",
        edgecolor="0.1",
        zorder=2,
    )
    y1, y_max = a_hist.get_ylim()
    a_hist.plot([R, R], [0, y_max], "k")
    a_hist.set_xlim([0, 1])
    a_hist.set_ylim([0, y_max])

    a_hist.set_xlabel("stress ratio, R")
    a_hist.set_ylabel("count")
    for event in subset.values():
        if event["b_plunge"] > 45:  # strike slip
            event["cluster"] = 0
        else:
            if event["p_plunge"] > 30 and event["t_plunge"] > 30:
                event["cluster"] = 2
            else:
                event["cluster"] = 1
    patches = []
    for axis, vector in stress_axes.items():
        trend, plunge = unit_vector_to_trend_plunge(vector)
        (patch,) = ax.line(
            plunge,
            trend,
            "o",
            color=axis_color[axis],
            zorder=4,
            markeredgecolor="k",
            linewidth="0.25",
        )
        patches.append(patch)
    R, stress_axes = decompose_stress(best_stress_tensor)
    fig.savefig(f"Stress Inversion/axes_well_{well_id}.png", bbox_inches="tight")
    f_hist.savefig(f"Stress Inversion/ratio_well_{well_id}.png", bbox_inches="tight")
    f_mc, a_mc = plt.subplots(figsize=[8, 4])
    a_mc.set_aspect("equal")
    a_mc.plot(circle_x, circle_y, "0.3", zorder=10)
    a_mc.plot(R * circle_x + 1 - R, R * circle_y, "0.3", zorder=10)
    a_mc.plot((1 - R) * circle_x - R, (1 - R) * circle_y, "0.3", zorder=10)

    for i_cluster in range(3):
        sub_subset = {k: v for k, v in subset.items() if v["cluster"] == i_cluster}
        n_events = len(sub_subset)
        taus, sigmas = np.zeros(n_events), np.zeros(n_events)
        for i_event, event in enumerate(sub_subset):
            sdr_unstable = most_unstable_sdr(subset[event]["MT_DC"], best_stress_tensor)
            subset[event]["true strike"] = sdr_unstable[0]
            subset[event]["true dip"] = sdr_unstable[1]
            subset[event]["true rake"] = sdr_unstable[2]
            normal_vector = strike_dip_to_normal(sdr_unstable[0], sdr_unstable[1])
            sigmas[i_event], taus[i_event] = resolve_shear_and_normal_stress(
                best_stress_tensor, normal_vector
            )

        a_mc.plot(sigmas, taus, "o", color=colors[i_cluster], markeredgecolor="k")
    a_mc.arrow(-1.1, 0, 2.2, 0, head_width=0.06, head_length=0.1, fc="k", ec="k")
    a_mc.arrow(-1.1, 0, 0, 1.1, head_width=0.06, head_length=0.1, fc="k", ec="k")
    a_mc.text(-1.1, 1.22, "$\\tau$", ha="center")
    a_mc.text(1.21, 0.0, "${\\sigma}$", va="center")
    a_mc.text(-0.98, -0.1, "$\\sigma_3$")
    a_mc.text(-0.98, -0.1, "$\\sigma_3$")
    a_mc.text(1 - 2 * R + 0.03, -0.1, "$\\sigma_2$")
    a_mc.text(1.02, -0.1, "$\\sigma_1$")
    a_mc.axis("off")
    a_mc.set_xlim(-1.3, 1.3)
    a_mc.set_ylim(-0.15, 1.3)

plt.show()
