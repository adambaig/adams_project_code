from numpy import sin, cos, zeros, matrix, sqrt


def p_Mee(phi, theta):
    return cos(phi) * cos(phi) * sin(theta) * sin(theta)


def p_Mnn(phi, theta):
    return sin(phi) * sin(phi) * sin(theta) * sin(theta)


def p_Mzz(phi, theta):
    return cos(theta) * cos(theta)


def p_Men(phi, theta):
    return sin(2 * phi) * sin(theta) * sin(theta)


def p_Mez(phi, theta):
    return cos(phi) * sin(2 * theta)


def p_Mnz(phi, theta):
    return sin(phi) * sin(2 * theta)


def sv_Mee(phi, theta):
    return 0.5 * cos(phi) * cos(phi) * sin(2 * theta)


def sv_Mnn(phi, theta):
    return 0.5 * sin(phi) * sin(phi) * sin(2 * theta)


def sv_Mzz(phi, theta):
    return -0.5 * sin(2 * theta)


def sv_Men(phi, theta):
    return 0.5 * sin(2 * phi) * sin(2 * theta)


def sv_Mez(phi, theta):
    return cos(phi) * cos(2 * theta)


def sv_Mnz(phi, theta):
    return sin(phi) * cos(2 * theta)


def sh_Mee(phi, theta):
    return -0.5 * sin(2 * phi) * sin(theta)


def sh_Mnn(phi, theta):
    return 0.5 * sin(2 * phi) * sin(theta)


def sh_Mzz(phi, theta):
    return 0


def sh_Men(phi, theta):
    return cos(2 * phi) * sin(theta)


def sh_Mez(phi, theta):
    return -sin(phi) * cos(theta)


def sh_Mnz(phi, theta):
    return cos(phi) * cos(theta)


def MT(term):
    moment_tensor = matrix(zeros([3, 3]))
    if term == "Mee":
        moment_tensor[0, 0] = 1
    if term == "Mnn":
        moment_tensor[1, 1] = 1
    if term == "Mzz":
        moment_tensor[2, 2] = 1
    if term == "Men":
        moment_tensor[0, 1] = sqrt(2)
        moment_tensor[1, 0] = sqrt(2)
    if term == "Mez":
        moment_tensor[0, 2] = sqrt(2)
        moment_tensor[2, 0] = sqrt(2)
    if term == "Mnz":
        moment_tensor[1, 2] = sqrt(2)
        moment_tensor[2, 1] = sqrt(2)
    return moment_tensor
