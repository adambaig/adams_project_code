import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response

from basic_operations.rotation import rotate_PVH
import radiation_pattern_functions as rp


PI = np.pi
D2R = 180 / PI
source = {
    "e": 0.0,
    "n": 0.0,
    "z": -1400.0,
    "moment_tensor": np.matrix([[1, 0, 0], [0, 0, 0], [0, 0, 0]]),
    "moment_magnitude": 0,
    "stress_drop": 1e6,
}

velocity_model = [{"vp": 3500.0, "vs": 1500.0, "rho": 2400}]


Q = {"P": 70, "S": 70}
time_series = np.linspace(0, 4, 4001)
horizontal_range = 2800
n_stations = 16
stations, waveforms = {}, {}
theta = 0.1

rp_functions = [f for f in dir(rp) if (("_" in f) and (f[0] != "_"))]

exec("test = rp.p_" + comp)

color = ["royalblue", "firebrick"]

for comp in ["Mee", "Mnn", "Mzz", "Men", "Mez", "Mnz"]:
    exec("p_rad = rp.p_" + comp)
    exec("sv_rad = rp.sv_" + comp)
    exec("sh_rad = rp.sh_" + comp)
    fig, ax = plt.subplots(1, 3, sharey=True)
    for ii in range(3):
        ax[ii].set_aspect("equal")

    for ii in range(360):
        angle = ii * np.pi / 180.0
        ax[0].plot(
            abs(p_rad(angle, theta)) * np.sin(angle),
            abs(p_rad(angle, theta)) * np.cos(angle),
            ".",
            color=color[p_rad(angle, theta) > 0],
        )
        ax[1].plot(
            abs(sv_rad(angle, theta)) * np.sin(angle),
            abs(sv_rad(angle, theta)) * np.cos(angle),
            ".",
            color=color[sv_rad(angle, theta) > 0],
        )
        ax[2].plot(
            abs(sh_rad(angle, theta)) * np.sin(angle),
            abs(sh_rad(angle, theta)) * np.cos(angle),
            ".",
            color=color[sh_rad(angle, theta) > 0],
        )
    for ii in range(3):
        ax[ii].set_xlim([-1, 1])
        ax[ii].set_ylim([-1, 1])
plt.show()
