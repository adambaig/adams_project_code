import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response

from basic_operations.rotation import rotate_PVH

PI = np.pi
D2R = 180 / PI
source = {
    "e": 0.0,
    "n": 0.0,
    "z": -2800.0,
    "moment_tensor": np.matrix([[0, 1, 0], [1, 0, 0], [0, 0, 0]]),
    "moment_magnitude": 0,
    "stress_drop": 1e6,
}

velocity_model = [{"vp": 3500.0, "vs": 1500.0, "rho": 2400}]

fig, ax = plt.subplots(1, 2, sharey=True)
for ii in range(2):
    ax[ii].set_aspect("equal")

Q = {"P": 70, "S": 70}
time_series = np.linspace(0, 4, 4001)
horizontal_range = source["z"]
n_stations = 24
stations, waveforms = {}, {}
for ii in range(n_stations):
    angle = 2 * PI * (ii + 0.5) / n_stations
    stations[ii] = {
        "e": horizontal_range * np.sin(angle),
        "n": horizontal_range * np.cos(angle),
        "z": 0,
    }

    p_raypath = isotropic_ray_trace(source, stations[ii], velocity_model, "P")
    s_raypath = isotropic_ray_trace(source, stations[ii], velocity_model, "S")
    waveforms[ii] = get_response(
        p_raypath, s_raypath, source, stations[ii], Q, time_series, 1e-19
    )
    i_p_time = int(p_raypath["traveltime"] // time_series[1])
    i_s_time = int(s_raypath["traveltime"] // time_series[1])
    if waveforms[ii]["z"][i_p_time + 4] > 0:
        color = "firebrick"
    else:
        color = "royalblue"
    ax[0].quiver(
        stations[ii]["e"],
        stations[ii]["n"],
        1e9 * waveforms[ii]["e"][i_p_time + 4],
        1e9 * waveforms[ii]["n"][i_p_time + 4],
        color=color,
    )

    if waveforms[ii]["z"][i_s_time + 5] > 0:
        color = "firebrick"
    else:
        color = "royalblue"
    ax[1].quiver(
        stations[ii]["e"],
        stations[ii]["n"],
        1e9 * waveforms[ii]["e"][i_s_time + 5],
        1e9 * waveforms[ii]["n"][i_s_time + 5],
        color=color,
    )

plt.show()
