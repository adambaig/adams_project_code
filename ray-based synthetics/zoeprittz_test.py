# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 14:33:25 2019

@author: adambaig
"""

import numpy as np
import matplotlib.pyplot as plt
from interface_scattering import PSV_scattering

velocityModel = [
    {"vp": 3000, "vs": 1500, "rho": 2000},
    {"vp": 4000, "vs": 2000, "rho": 2200},
]

pxes = np.arange(0, 1 / 3000.0, 1 / 3000.0 / 1000.0)
PdPu = np.zeros(len(pxes))
PdSu = np.zeros(len(pxes))
SdPu = np.zeros(len(pxes))
SdSu = np.zeros(len(pxes))

ii = -1
for px in pxes:
    scatmat = PSV_scattering(velocityModel[0], velocityModel[1], px)
    ii += 1
    PdPu[ii] = scatmat[0, 0]
    PdSu[ii] = scatmat[0, 1]
    SdPu[ii] = scatmat[1, 0]
    SdSu[ii] = scatmat[1, 1]

plt.plot(np.arcsin(3000 * pxes) * 180 / np.pi, PdPu)
plt.plot(np.arcsin(3000 * pxes) * 180 / np.pi, SdPu)
