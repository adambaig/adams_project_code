# -*- coding: utf-8 -*-
"""
Created on Thu Feb 21 16:35:08 2019

@author: adambaig
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft, fftfreq
import matplotlib.patches as mpatches


def plot_waveform_spectrum(waveform, windows, time_series):
    t1, t2 = windows["signal"]
    n1, n2 = windows["noise"]
    time_sample = time_series[1] - time_series[0]
    i1 = np.where(time_series <= t1)[0][-1]
    i2 = np.where(time_series >= t2)[0][0]
    in1 = np.where(time_series <= n1)[-1][-1]
    in2 = np.where(time_series >= n2)[0][0]
    signal = waveform[i1 : i2 + 1]
    noise = waveform[in1 : in2 + 1]

    signal_freqs = fftfreq(len(signal), time_sample)
    noise_freqs = fftfreq(len(noise), time_sample)
    f_signal = fft(signal) / signal_freqs / 2.0 / np.pi
    f_noise = fft(noise) / noise_freqs / 2.0 / np.pi
    ipos_signal = np.where(signal_freqs >= 0)[0]
    ipos_noise = np.where(noise_freqs >= 0)[0]

    fig = plt.figure()
    ax1 = fig.add_axes([0.15, 0.7, 0.7, 0.15])
    ax2 = fig.add_axes([0.15, 0.15, 0.7, 0.5])
    signal_patch = mpatches.Rectangle(
        [t1, -1], t2 - t1, 2, color="firebrick", alpha=0.2, edgecolor="k", zorder=2
    )
    noise_patch = mpatches.Rectangle(
        [n1, -1], n2 - n1, 2, color="grey", alpha=0.2, edgecolor="k", zorder=2
    )
    ax1.add_patch(signal_patch)
    ax1.add_patch(noise_patch)
    ax1.plot(time_series, waveform / max(abs(waveform)), "k")
    ax2.loglog(
        signal_freqs[ipos_signal], abs(f_signal[ipos_signal]), "firebrick", alpha=0.8
    )
    ax2.loglog(noise_freqs[ipos_noise], abs(f_noise[ipos_noise]), "grey", zorder=-1)
    return fig
