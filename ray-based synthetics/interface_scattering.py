# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 13:15:02 2019

@author: adambaig
"""

import numpy as np
from numpy.lib.scimath import sqrt


def PSV_scattering(layer1, layer2, p):
    # see Aki and Richards 2000, pg 144
    vp1 = layer1["vp"]
    vp2 = layer2["vp"]
    vs1 = layer1["vs"]
    vs2 = layer2["vs"]
    rho1 = layer1["rho"]
    rho2 = layer2["rho"]
    aa = rho2 * (1.0 - 2.0 * vs2 * vs2 * p * p) - rho1 * (1.0 - 2.0 * vs1 * vs1 * p * p)
    bb = rho2 * (1.0 - 2.0 * vs2 * vs2 * p * p) + 2.0 * rho1 * vs1 * vs1 * p * p
    cc = rho1 * (1.0 - 2.0 * vs1 * vs1 * p * p) + 2.0 * rho2 * vs2 * vs2 * p * p
    dd = 2.0 * (rho2 * vs2 * vs2 - rho1 * vs1 * vs1)
    cosi1a = sqrt(1.0 - vp1 * vp1 * p * p) / vp1
    cosi2a = sqrt(1.0 - vp2 * vp2 * p * p) / vp2
    cosj1b = sqrt(1.0 - vs1 * vs1 * p * p) / vs1
    cosj2b = sqrt(1.0 - vs2 * vs2 * p * p) / vs2
    E = bb * cosi1a + cc * cosi2a
    F = bb * cosj1b + cc * cosj2b
    G = aa - dd * cosi1a * cosj2b
    H = aa - dd * cosi2a * cosj1b
    D = E * F + G * H * p * p
    PdPu = (
        (bb * cosi1a - cc * cosi2a) * F - (aa + dd * cosi1a * cosj2b) * H * p * p
    ) / D
    PdSu = (-2.0 * cosi1a * (aa * bb + cc * dd * cosi2a * cosj2b) * p * vp1) / vs1 / D
    PdPd = (2.0 * rho1 * cosi1a * F * vp1) / vp2 / D
    PdSd = (2.0 * rho1 * cosi1a * H * p * vp1) / vs2 / D
    SdPu = (-2.0 * cosj1b * (aa * bb + cc * dd * cosi2a * cosj2b) * p * vs1) / vp1 / D
    SdSu = (
        -((bb * cosj1b - cc * cosj2b) * E - (aa + dd * cosi2a * cosj2b) * G * p * p) / D
    )
    SdPd = (-2.0 * rho1 * cosj1b * G * p * vs1) / vp2 / D
    SdSd = (2.0 * rho1 * cosj1b * E * vs1) / vs2 / D
    PuPu = (2.0 * rho2 * cosi2a * F * p * vp2) / vs1 / D
    PuSu = (2.0 * rho2 * cosi2a * G * p * vp2) / vs1 / D
    PuPd = (
        -((bb * cosi1a - cc * cosi2a) * F + (aa + dd * cosi2a * cosj1b) * G * p * p) / D
    )
    PuSd = (2.0 * cosi2a * (aa * cc + bb * dd * cosi1a * cosj2b) * p * vp2) / vs2 / D
    SuPu = (2.0 * rho2 * cosj2b * H * p * vs2) / vp1 / D
    SuSu = (2.0 * rho2 * cosj2b * E * p * vs2) / vs1 / D
    SuPd = (2.0 * cosj2b * (aa * cc + bb * dd * cosi1a * cosj1b) * p * vs2) / vp2 / D
    SuSd = (
        (bb * cosj1b - cc * cosj2b) * E + (aa + dd * cosi1a * cosj2b) * H * p * p
    ) / D
    return np.matrix(
        [
            [PdPu, SdPu, PuPu, SuPu],
            [PdSu, SdSu, PuSu, SuSu],
            [PdPd, SdPd, PuPd, SuPd],
            [PdSd, SdSd, PuSd, SuSd],
        ]
    )


def SH_scattering(layer1, layer2, p):
    # see Aki and Richards 2000, pg 139
    vs1 = layer1["vs"]
    vs2 = layer2["vs"]
    rho1 = layer1["rho"]
    rho2 = layer2["rho"]
    cosj1b = sqrt(1.0 - vs1 * vs1 * p * p) / vs1
    cosj2b = sqrt(1.0 - vs2 * vs2 * p * p) / vs2
    D = rho1 * vs1 * cosj1b + rho2 * vs2 * cosj2b
    SdSu = (rho1 * vs1 * cosj1b - rho2 * vs2 * cosj2b) / D
    SuSu = (2.0 * rho1 * vs1 * cosj1b) / D
    SdSd = (2.0 * rho2 * vs2 * cosj2b) / D
    SuSd = -SdSu
    return np.matrix([[SdSu, SuSu], [SdSd, SuSd]])


def PSV_free_surface(layer, p):
    vp = layer["vp"]
    vs = layer["vs"]
    cosi = sqrt(1.0 - vp * vp * p * p)
    cosj = sqrt(1.0 - vs * vs * p * p)
    den = (1 / vs / vs - 2 * p * p) * (
        1 / vs / vs - 2 * p * p
    ) + 4 * p * p * cosi * cosj / vp / vs
    PuPd = (
        -(1 / vs / vs - 2 * p * p) * (1 / vs / vs - 2 * p * p)
        + 4 * p * p * cosi * cosj / vp / vs
    ) / den
    PuSd = 4 * p * cosi * (1 / vs / vs - 2 * p * p) / vs / den
    SuPd = 4 * p * cosj * (1 / vs / vs - 2 * p * p) / vp / den
    SuSd = (
        (1 / vs / vs - 2 * p * p) * (1 / vs / vs - 2 * p * p)
        - 4 * p * p * cosi * cosj / vp / vs
    ) / den
    return np.matrix([[PuPd, SuPd], [PuSd, SuSd]])


def P_stack_transmission(velocity_model, p):
    nlayers = len(velocity_model)
    transmission_coeff = 1
    if nlayers > 1:
        for ilayer in range(nlayers - 1):
            scatmat = PSV_scattering(
                velocity_model[ilayer], velocity_model[ilayer + 1], p
            )
            transmission_coeff *= scatmat[2, 0]
    return transmission_coeff


def SV_stack_transmission(velocity_model, p):
    nlayers = len(velocity_model)
    transmission_coeff = 1
    if nlayers > 1:
        for ilayer in range(nlayers - 1):
            scatmat = PSV_scattering(
                velocity_model[ilayer], velocity_model[ilayer + 1], p
            )
            transmission_coeff *= scatmat[3, 1]
    return transmission_coeff


def SH_stack_transmission(velocity_model, p):
    nlayers = len(velocity_model)
    transmission_coeff = 1
    if nlayers > 1:
        for ilayer in range(nlayers - 1):
            scatmat = SH_scattering(
                velocity_model[ilayer], velocity_model[ilayer + 1], p
            )
            transmission_coeff *= scatmat[1, 0]
    return transmission_coeff
