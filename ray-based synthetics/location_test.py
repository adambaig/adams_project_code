# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 14:15:04 2018

@author: baig
"""

import numpy as np
from scipy.optimize import minimize
from raytrace import traceRaysIso
import matplotlib.pyplot as plt

location = {"x": 100.0, "y": 0.0, "z": 2800.0}

stations = [
    {"x": 200.0, "y": 0.0, "z": 2200.0},
    {"x": 200.0, "y": 0.0, "z": 2300.0},
    {"x": 200.0, "y": 0.0, "z": 2400.0},
    {"x": 200.0, "y": 0.0, "z": 2500.0},
    {"x": 200.0, "y": 0.0, "z": 2600.0},
]

velocityModel = [
    {"vp": 4100.0, "vs": 2000.0},
    {"vp": 4300.0, "vs": 2700.0, "top": 2500.0},
    {"vp": 3500.0, "vs": 2000.0, "top": 2600.0},
    {"top": 2750.0, "vp": 5400.0, "vs": 3200.0},
    {"top": 3020.0, "vp": 4000.0, "vs": 2500.0},
    {"top": 3100.0, "vp": 5200.0, "vs": 3000.0},
    {"top": 3220.0, "vp": 5890.0, "vs": 3400.0},
]

ptimes = []
stimes = []
for ii in range(len(stations)):
    ptimes.append(
        traceRaysIso(location, stations[ii], velocityModel, "P")[0]
        + np.random.normal(0.002, 0.002)
    )
    stimes.append(
        traceRaysIso(location, stations[ii], velocityModel, "S")[0]
        + np.random.normal(0.004, 0.004)
    )


def objective_function(test_xyzt):
    residual = 0
    origin_time = test_xyzt[-1]
    test_loc = {"x": test_xyzt[0], "y": test_xyzt[1], "z": test_xyzt[2]}
    ii = -1
    for ptime in ptimes:
        ii += 1
        if ptime > 0:
            tt, p, geometrical_spreading = traceRaysIso(
                test_loc, stations[ii], velocityModel, "P"
            )
            residual += (ptime - tt - origin_time) * (ptime - tt - origin_time)
    ii = -1
    for stime in stimes:
        ii += 1
        if stime > 0:
            tt, p, geometrical_spreading = traceRaysIso(
                test_loc, stations[ii], velocityModel, "S"
            )
            residual += (stime - tt - origin_time) * (stime - tt - origin_time)
    return residual


def objective_function2(test_xyz):
    residual = 0
    test_loc = {"x": test_xyz[0], "y": test_xyz[1], "z": test_xyz[2]}
    ii = -1
    p_th_sum, p_obs_sum, np = 0, 0, 0
    p_th = []
    for ptime in ptimes:
        ii += 1
        if ptime > 0:
            p_th.append(traceRaysIso(test_loc, stations[ii], velocityModel, "P")[0])
            p_th_sum += p_th[-1]
            p_obs_sum += ptime
            np += 1
        else:
            p_th.append(0)
    ii = -1
    s_th = []
    s_th_sum, s_obs_sum, ns = 0, 0, 0
    for stime in stimes:
        ii += 1
        if stime > 0:
            s_th.append(traceRaysIso(test_loc, stations[ii], velocityModel, "S")[0])
            s_th_sum += s_th[-1]
            s_obs_sum += stime
            ns += 1
        else:
            s_th.append(0)
    origin_time_estimate = (
        p_obs_sum / float(np)
        + s_obs_sum / float(ns)
        - p_th_sum / float(np)
        - s_th_sum / float(ns)
    )
    ii = -1
    for ptime in ptimes:
        ii += 1
        if ptime > 0:
            residual += (ptime - p_th[ii] - origin_time_estimate) * (
                ptime - p_th[ii] - origin_time_estimate
            )
    ii = -1
    for stime in stimes:
        if stime > 0:
            residual += (stime - s_th[ii] - origin_time_estimate) * (
                stime - s_th[ii] - origin_time_estimate
            )
    return residual


solution = minimize(objective_function, [80.0, 0.0, 2801.0, 0.0], method="Nelder-Mead")
x_grid = np.linspace(0, 300, 60)
z_grid = np.linspace(2200, 3200, 200)
residual_grid = np.zeros([len(x_grid), len(z_grid)])
ix = -1
for x in x_grid:
    ix += 1
    iz = -1
    for z in z_grid:
        iz += 1
        residual_grid[ix, iz] = objective_function([x, 0.0, z, solution.x[3]])
fig, ax = plt.subplots(1)
cs = ax.contourf(x_grid, z_grid, np.log10(residual_grid).T)
y1, y2 = ax.get_ylim()
ax.set_ylim(y2, y1)
cbar = fig.colorbar(cs)
ax.plot(location["x"], location["z"], "wx")
ax.plot(solution.x[0], solution.x[2], "wo")
