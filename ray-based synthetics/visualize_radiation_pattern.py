import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response

from basic_operations.rotation import rotate_PVH
import radiation_pattern_functions as rp


PI = np.pi
D2R = 180 / PI
source = {
    "e": 0.0,
    "n": 0.0,
    "z": -2800.0,
    "moment_magnitude": 0,
    "stress_drop": 1e6,
}

velocity_model = [{"vp": 3500.0, "vs": 1500.0, "rho": 2400}]


Q = {"P": 70, "S": 70}
time_series = np.linspace(0, 4, 4001)
horizontal_range = 2800
n_stations = 16
stations, waveforms = {}, {}
point_color = {False: "royalblue", True: "firebrick"}

theta = np.arctan(-horizontal_range / source["z"])
comp_index = {"e": 0, "n": 1, "z": 2}

for comp in ["Mee", "Mnn", "Mzz", "Men", "Mez", "Mnz"]:
    exec("p_rad = rp.p_" + comp)
    exec("sv_rad = rp.sv_" + comp)
    exec("sh_rad = rp.sh_" + comp)
    fig, ax = plt.subplots(2, 3, sharey=True)
    ii = comp_index[comp[1]]
    jj = comp_index[comp[2]]
    source["moment_tensor"] = np.matrix(np.zeros([3, 3]))
    source["moment_tensor"][ii, jj] = 1
    source["moment_tensor"][jj, ii] = 1
    for ii in range(3):
        for jj in range(2):
            ax[jj, ii].set_aspect("equal")

    for ii in range(n_stations):
        angle = 2 * PI * (ii + 0.5) / n_stations
        stations[ii] = {
            "e": horizontal_range * np.sin(angle),
            "n": horizontal_range * np.cos(angle),
            "z": 0,
        }

        p_raypath = isotropic_ray_trace(source, stations[ii], velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, stations[ii], velocity_model, "S")
        waveforms[ii] = get_response(
            p_raypath, s_raypath, source, stations[ii], Q, time_series, 1e-19
        )
        i_p_time = int(p_raypath["traveltime"] // time_series[1])
        i_s_time = int(s_raypath["traveltime"] // time_series[1])
        if waveforms[ii]["z"][i_p_time + 4] > 0:
            color = "firebrick"
        else:
            color = "royalblue"
        ax[0, 0].quiver(
            stations[ii]["e"],
            stations[ii]["n"],
            1e9 * waveforms[ii]["e"][i_p_time + 4],
            1e9 * waveforms[ii]["n"][i_p_time + 4],
            color=color,
        )
        ax[0, 0].set_title("P wave")
        rot_waveform, linearity = rotate_PVH(waveforms[ii], [i_p_time, i_p_time + 10])
        azimuth = np.arctan2(stations[ii]["n"], stations[ii]["e"])
        if waveforms[ii]["z"][i_s_time + 5] > 0:
            color = "firebrick"
        else:
            color = "royalblue"
        ax[0, 1].quiver(
            stations[ii]["e"],
            stations[ii]["n"],
            1e9 * np.cos(azimuth) * rot_waveform["v"][i_s_time + 5],
            1e9 * np.sin(azimuth) * rot_waveform["v"][i_s_time + 5],
            color=color,
        )
        ax[0, 1].set_title("SV wave")

        ax[0, 2].quiver(
            stations[ii]["e"],
            stations[ii]["n"],
            1e9 * np.sin(azimuth) * rot_waveform["h"][i_s_time + 5],
            -1e9 * np.cos(azimuth) * rot_waveform["h"][i_s_time + 5],
            color="0.3",
        )
        ax[0, 2].set_title("SH wave")
    for ii in range(360):
        angle = np.pi / 2 - ii * np.pi / 180.0
        ax[1, 0].plot(
            horizontal_range * abs(p_rad(angle, theta)) * np.sin(np.pi / 2 - angle),
            horizontal_range * abs(p_rad(angle, theta)) * np.cos(np.pi / 2 - angle),
            ".",
            color=point_color[p_rad(angle, theta) > 0],
        )
        ax[1, 1].plot(
            horizontal_range * abs(sv_rad(angle, theta)) * np.sin(np.pi / 2 - angle),
            horizontal_range * abs(sv_rad(angle, theta)) * np.cos(np.pi / 2 - angle),
            ".",
            color=point_color[sv_rad(angle, theta) > 0],
        )
        ax[1, 2].plot(
            horizontal_range * abs(sh_rad(angle, theta)) * np.sin(np.pi / 2 - angle),
            horizontal_range * abs(sh_rad(angle, theta)) * np.cos(np.pi / 2 - angle),
            ".",
            color=point_color[sh_rad(angle, theta) > 0],
        )
    fig.text(0.5, 0.8, comp, ha="center")
plt.show()
