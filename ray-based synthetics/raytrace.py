# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 16:25:45 2019

@author: adambaig
"""

import numpy as np


def isotropic_ray_trace(src, rec, velocityModel, phase):
    XY = {"x": src["x"] - rec["x"], "y": src["y"] - rec["y"]}
    X = np.sqrt(
        (src["x"] - rec["x"]) * (src["x"] - rec["x"])
        + (src["y"] - rec["y"]) * (src["y"] - rec["y"])
    )
    if src["z"] < rec["z"]:
        # determine if the source is above the receiver or vice versa
        zStart = src["z"]
        zEnd = rec["z"]
    else:
        zStart = rec["z"]
        zEnd = src["z"]
    effectiveVelocityModel = []
    if phase == "P":
        for layer in determine_velocity_profile(velocityModel, zStart, zEnd):
            effectiveVelocityModel.append(
                {
                    "v": layer["vp"],
                    "h": layer["height"] * np.sign(src["z"] - rec["z"]),
                    "rho": layer["rho"],
                }
            )
    elif phase == "S":
        for layer in determine_velocity_profile(velocityModel, zStart, zEnd):
            effectiveVelocityModel.append(
                {
                    "v": layer["vs"],
                    "h": layer["height"] * np.sign(src["z"] - rec["z"]),
                    "rho": layer["rho"],
                }
            )
    if src["z"] > rec["z"]:
        effectiveVelocityModel.reverse()
    return get_direct_ray_path(effectiveVelocityModel, X, XY)


def get_direct_ray_path(velocityModel, hrzDistance, XY):
    def Fq(qi, velocityModel, X, vMax, hMax):
        F = 0
        for layer in velocityModel:
            velRatio = layer["v"] / vMax
            F += (
                velRatio
                * abs(layer["h"])
                / np.sqrt(hMax * hMax + (1 - velRatio * velRatio) * qi * qi)
            )
        F = q * F - X
        return F

    def dFq(qi, velocityModel, vMax, hMax):
        dF = 0
        for layer in velocityModel:
            velRatio = float(layer["v"]) / float(vMax)
            dF += (
                velRatio
                * (1 - velRatio * velRatio)
                * abs(layer["h"])
                * (hMax * hMax + (1 - velRatio * velRatio) * qi * qi) ** (-1.5)
            )
        dF = -qi * qi * dF
        for layer in velocityModel:
            velRatio = float(layer["v"]) / float(vMax)
            dF += (
                velRatio
                * abs(layer["h"])
                / np.sqrt(hMax * hMax + (1 - velRatio * velRatio) * qi * qi)
            )
        return dF

    if len(velocityModel) > 1:
        vMax = 0.0
        epsilon = 1.0e-7
        ii = 0
        aSum, bSum = 0.0, 0.0
        traveltime = 0.0
        for layer in velocityModel:
            if layer["v"] > vMax:
                vMax = layer["v"]
                hMax = abs(layer["h"])
        for layer in velocityModel:
            velRatio = float(layer["v"]) / float(vMax)
            aSum += velRatio * abs(layer["h"])
            if abs(layer["v"] - vMax) > epsilon:
                bSum += velRatio * abs(layer["h"]) / np.sqrt(1 - velRatio * velRatio)
        aSum = aSum / hMax
        if hrzDistance < aSum * bSum / (aSum - 1.0):
            q = hrzDistance / aSum
        else:
            q = hrzDistance - bSum

        max_iterations = 50
        n_it = 0
        while n_it < max_iterations:
            n_it += 1
            correction = Fq(q, velocityModel, hrzDistance, vMax, hMax) / dFq(
                q, velocityModel, vMax, hMax
            )
            if abs(correction) < epsilon:
                n_it = max_iterations + 1
            q = q - correction

        p = q / vMax / np.sqrt(hMax * hMax + q * q)
        traveltime = 0
        for layer in velocityModel:
            traveltime += (
                abs(layer["h"])
                / np.sqrt(1.0 - p * p * layer["v"] * layer["v"])
                / layer["v"]
            )
        curvatureRadius1 = []
        for layer in velocityModel:
            curvatureRadius1.append(
                np.sqrt(
                    layer["h"] * layer["h"]
                    + p
                    * p
                    * layer["h"]
                    * layer["h"]
                    / (1.0 / layer["v"] / layer["v"] - p * p)
                )
            )
        ii = 1
        for radius in curvatureRadius1[1:]:
            v1, v0 = velocityModel[ii]["v"], velocityModel[ii - 1]["v"]
            cos1sq = 1.0 - v1 * v1 * p * p
            cos0sq = 1.0 - v0 * v0 * p * p
            curvatureRadius2 = curvatureRadius1[0] * v0 * cos1sq / v1 / cos0sq + radius
            ii += 1
            geometricalSpreading = np.sqrt(curvatureRadius2 * sum(curvatureRadius1))
    else:
        geometricalSpreading = np.sqrt(
            hrzDistance * hrzDistance + velocityModel[0]["h"] * velocityModel[0]["h"]
        )
        traveltime = geometricalSpreading / velocityModel[0]["v"]
        p = hrzDistance / velocityModel[0]["v"] / geometricalSpreading
    angle = np.arctan2(XY["y"], XY["x"])
    px = p * np.cos(angle)
    py = p * np.sin(angle)
    return {
        "hrz_slowness": {"x": px, "y": py},
        "traveltime": traveltime,
        "geometrical_spreading": geometricalSpreading,
        "velocity_model_chunk": velocityModel,
    }


def determine_velocity_profile(velocityModel, zStart, zEnd):
    nLayers = len(velocityModel)
    epsilon = 1e-3
    effectiveVelocityModel = []
    if nLayers > 1:
        iStart = 0
        iEnd = 0
        if zStart > velocityModel[1]["top"]:
            for ii in range(1, nLayers):
                if zStart < velocityModel[ii]["top"] and iStart == 0:
                    iStart = ii - 1
        if zEnd > velocityModel[1]["top"]:
            for ii in range(1, nLayers):
                if zEnd < velocityModel[ii]["top"] and iEnd == 0:
                    iEnd = ii - 1
        if zStart >= velocityModel[-1]["top"]:
            iStart = nLayers - 1
        if zEnd >= velocityModel[-1]["top"]:
            iEnd = nLayers - 1
        if iStart == iEnd:
            # one layer
            effectiveVelocityModel.append(velocityModel[iStart])
            effectiveVelocityModel[0]["top"] = zStart
            effectiveVelocityModel[0]["height"] = zEnd - zStart
        else:
            # multi-layer
            test1 = velocityModel[iStart + 1]["top"] - zStart
            if test1 > epsilon:
                effectiveVelocityModel.append(velocityModel[iStart])
                effectiveVelocityModel[-1]["top"] = zStart
                effectiveVelocityModel[-1]["height"] = (
                    velocityModel[iStart + 1]["top"] - zStart
                )
            for ii in range(iStart + 1, iEnd):
                effectiveVelocityModel.append(velocityModel[ii])
                effectiveVelocityModel[-1]["height"] = (
                    velocityModel[ii + 1]["top"] - velocityModel[ii]["top"]
                )
            if zEnd - velocityModel[iEnd]["top"] > epsilon:
                effectiveVelocityModel.append(velocityModel[iEnd])
                effectiveVelocityModel[-1]["height"] = zEnd - velocityModel[iEnd]["top"]
    else:
        effectiveVelocityModel.append(velocityModel[0])
        effectiveVelocityModel[0]["top"] = zStart
        effectiveVelocityModel[0]["height"] = zEnd - zStart
    return effectiveVelocityModel
