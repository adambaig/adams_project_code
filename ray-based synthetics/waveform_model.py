# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 09:01:40 2019

@author: adambaig
"""

from numpy import pi, sin, cos, log, exp, conj, real, zeros, sqrt
import numpy as np
from scipy.fftpack import ifft, fftfreq
from interface_scattering import (
    P_stack_transmission,
    SV_stack_transmission,
    SH_stack_transmission,
    PSV_free_surface,
)


def calc_pulse(fly_time, Q, corner_frequency, time_series):
    dt = time_series[1] - time_series[0]
    freq = fftfreq(len(time_series), dt)
    reference_omega = 2 * pi * 100
    # reference frequency of 100 Hz for Q and velocity
    omega_corner = 2 * pi * corner_frequency
    Fpulse = zeros(len(freq), dtype=complex)
    nt = len(time_series)
    for ii in range(1, nt // 2 - 1):
        omega = 2 * pi * freq[ii]
        pulse = 1j * omega / ((omega_corner + 1j * omega) ** 2)
        atten = exp(
            -1j
            * omega
            * fly_time
            / (1 + log(omega / reference_omega) / pi / Q + 1j / 2 / Q)
        )
        Fpulse[ii] = pulse * atten
        Fpulse[nt - ii] = conj(Fpulse[ii])
    return ifft(Fpulse) * nt


def calc_disp(fly_time, Q, corner_frequency, time_series):
    dt = time_series[1] - time_series[0]
    freq = fftfreq(len(time_series), dt)
    reference_omega = 2 * pi * 100
    # reference frequency of 100 Hz for Q and velocity
    omega_corner = 2 * pi * corner_frequency
    Fpulse = zeros(len(freq), dtype=complex)
    nt = len(time_series)
    for ii in range(1, nt // 2 - 1):
        omega = 2 * pi * freq[ii]
        pulse = 1 / ((omega_corner + 1j * omega) ** 2)
        atten = exp(
            -1j
            * omega
            * fly_time
            / (1 + log(omega / reference_omega) / pi / Q + 1j / 2 / Q)
        )
        Fpulse[ii] = pulse * atten
        Fpulse[nt - ii] = conj(Fpulse[ii])
    return ifft(Fpulse) * nt


def calc_accel(fly_time, Q, corner_frequency, time_series):
    dt = time_series[1] - time_series[0]
    freq = fftfreq(len(time_series), dt)
    reference_omega = 2 * pi * 100
    # reference frequency of 100 Hz for Q and velocity
    omega_corner = 2 * pi * corner_frequency
    Fpulse = zeros(len(freq), dtype=complex)
    nt = len(time_series)
    for ii in range(1, nt // 2 - 1):
        omega = 2 * pi * freq[ii]
        pulse = -omega * omega / ((omega_corner + 1j * omega) ** 2)
        atten = exp(
            -1j
            * omega
            * fly_time
            / (1 + log(omega / reference_omega) / pi / Q + 1j / 2 / Q)
        )
        Fpulse[ii] = pulse * atten
        Fpulse[nt - ii] = conj(Fpulse[ii])
    return ifft(Fpulse) * nt


def get_P_radiation_pattern(moment_tensor, phi, theta):
    # implementation of Chapman, eqs 4.6.18
    # amplitudes of P radiation pattern
    # given colatitude angle, theta and azimuth (from North)
    # phi.  Theta and phi in radians
    Mxx = moment_tensor[0, 0]
    Myy = moment_tensor[1, 1]
    Mzz = moment_tensor[2, 2]
    Mxy = moment_tensor[0, 1]
    Mxz = moment_tensor[0, 2]
    Myz = moment_tensor[1, 2]
    p_radiation = (
        (Mxx * cos(phi) * cos(phi) + Myy * sin(phi) * sin(phi) + Mxy * sin(2 * phi))
        * sin(theta)
        * sin(theta)
        + Mzz * cos(theta) * cos(theta)
        + (Mxz * cos(phi) + Myz * sin(phi)) * sin(2 * theta)
    )
    return p_radiation


def get_SV_radiation_pattern(moment_tensor, phi, theta):
    # implementation of Chapman, eqs 4.6.19
    # amplitudes of SV radiation pattern
    # given colatitude angle, theta and azimuth (from North)
    # phi.  Theta and phi in radians
    Mxx = moment_tensor[0, 0]
    Myy = moment_tensor[1, 1]
    Mzz = moment_tensor[2, 2]
    Mxy = moment_tensor[0, 1]
    Mxz = moment_tensor[0, 2]
    Myz = moment_tensor[1, 2]
    sv_radiation = 0.5 * (
        Mxx * cos(phi) * cos(phi) + Myy * sin(phi) * sin(phi) - Mzz + Mxy * sin(2 * phi)
    ) * sin(2 * theta) + (Mxz * cos(phi) + Myz * sin(phi)) * cos(2 * theta)
    return sv_radiation


def get_SH_radiation_pattern(moment_tensor, phi, theta):
    # implementation of Chapman, eqs 4.6.20
    # amplitudes of SH radiation pattern
    # given colatitude angle, theta and azimuth (from North)
    # phi.  Theta and phi in radians
    Mxx = moment_tensor[0, 0]
    Myy = moment_tensor[1, 1]
    Mxy = moment_tensor[0, 1]
    Mxz = moment_tensor[0, 2]
    Myz = moment_tensor[1, 2]
    sh_radiation = (0.5 * (Myy - Mxx) * sin(2 * phi) + Mxy * cos(2 * phi)) * sin(
        theta
    ) + (Myz * cos(phi) - Mxz * sin(phi)) * cos(theta)
    return sh_radiation


def hrz_slowness_sq(slowness):
    return slowness["x"] * slowness["x"] + slowness["y"] * slowness["y"]


def get_response(pRaypath, sRaypath, source, receiver, Q, time_series, units="V"):
    # simulate first arrival waveforms in 1D media
    # need to implement Zoepritz' equations at the interfaces
    # and add surface conversion coefficients as well
    p_slowness_sq = hrz_slowness_sq(pRaypath["hrz_slowness"])
    s_slowness_sq = hrz_slowness_sq(sRaypath["hrz_slowness"])
    velocity_and_rho = []
    for layer in zip(
        pRaypath["velocity_model_chunk"], sRaypath["velocity_model_chunk"]
    ):
        velocity_and_rho.append(
            {"rho": layer[0]["rho"], "vp": layer[0]["v"], "vs": layer[1]["v"]}
        )
    vp_source = pRaypath["velocity_model_chunk"][0]["v"]
    vp_receiver = pRaypath["velocity_model_chunk"][-1]["v"]
    vs_source = sRaypath["velocity_model_chunk"][0]["v"]
    vs_receiver = sRaypath["velocity_model_chunk"][-1]["v"]
    rho_source = pRaypath["velocity_model_chunk"][0]["rho"]
    rho_receiver = pRaypath["velocity_model_chunk"][-1]["rho"]
    azimuth = np.arctan2(receiver["y"] - source["y"], receiver["x"] - source["x"])
    theta_source_p = np.arccos(sqrt(1 - vp_source * vp_source * p_slowness_sq))
    theta_source_s = np.arccos(sqrt(1 - vs_source * vs_source * s_slowness_sq))
    theta_receiver_p = np.arccos(sqrt(1 - vp_receiver * vp_receiver * p_slowness_sq))
    theta_receiver_s = np.arccos(sqrt(1 - vs_receiver * vs_receiver * s_slowness_sq))
    moment = 10 ** (1.5 * source["moment_magnitude"] + 9.05)
    p_rad = get_P_radiation_pattern(source["moment_tensor"], azimuth, theta_source_p)
    v_rad = get_SV_radiation_pattern(source["moment_tensor"], azimuth, theta_source_s)
    h_rad = get_SH_radiation_pattern(source["moment_tensor"], azimuth, theta_source_s)
    corner_freq_S = (
        0.21 * vs_source * np.cbrt(16 * source["stress_drop"] / (7 * moment))
    )
    corner_freq_P = 0.32 * corner_freq_S / 0.21  # Madariaga, 1976, v_rupture= 0.9beta
    p_trans = P_stack_transmission(velocity_and_rho, np.sqrt(p_slowness_sq))
    sv_trans = SV_stack_transmission(velocity_and_rho, np.sqrt(s_slowness_sq))
    sh_trans = SH_stack_transmission(velocity_and_rho, np.sqrt(s_slowness_sq))

    if units == "V":
        pPulse = calc_pulse(pRaypath["traveltime"], Q["P"], corner_freq_P, time_series)
        sPulse = calc_pulse(sRaypath["traveltime"], Q["S"], corner_freq_S, time_series)
    elif units == "D":
        pPulse = calc_disp(pRaypath["traveltime"], Q["P"], corner_freq_P, time_series)
        sPulse = calc_disp(sRaypath["traveltime"], Q["S"], corner_freq_S, time_series)
    elif units == "A":
        pPulse = calc_accel(pRaypath["traveltime"], Q["P"], corner_freq_P, time_series)
        sPulse = calc_accel(sRaypath["traveltime"], Q["S"], corner_freq_S, time_series)
    pWave = (
        pPulse
        * p_trans
        * moment
        * corner_freq_P
        * corner_freq_P
        / pi
        / sqrt(rho_source * rho_receiver)
        / sqrt(vp_source * vp_receiver)
        / vp_receiver
        / vp_receiver
        / pRaypath["geometrical_spreading"]
    )
    svWave = (
        sPulse
        * sv_trans
        * moment
        * corner_freq_S
        * corner_freq_S
        / pi
        / sqrt(rho_source * rho_receiver)
        / sqrt(vs_source * vs_receiver)
        / vs_receiver
        / vs_receiver
        / sRaypath["geometrical_spreading"]
    )
    shWave = (
        sPulse
        * sh_trans
        * moment
        * corner_freq_S
        * corner_freq_S
        / pi
        / sqrt(rho_source * rho_receiver)
        / sqrt(vs_source * vs_receiver)
        / vs_receiver
        / vs_receiver
        / sRaypath["geometrical_spreading"]
    )
    p_pol = [
        cos(azimuth) * sin(theta_receiver_p),
        sin(azimuth) * sin(theta_receiver_p),
        cos(theta_receiver_p),
    ]
    v_pol = [
        cos(azimuth) * cos(theta_receiver_s),
        sin(azimuth) * cos(theta_receiver_s),
        -sin(theta_receiver_s),
    ]
    h_pol = [sin(azimuth), cos(azimuth), 0]
    return {
        "n": np.real(
            p_pol[0] * p_rad * pWave
            + v_pol[0] * v_rad * svWave
            + h_pol[0] * h_rad * shWave
            + add_noise(time_series, 1e-7)
        ),
        "e": np.real(
            p_pol[1] * p_rad * pWave
            + v_pol[1] * v_rad * svWave
            + h_pol[1] * h_rad * shWave
            + add_noise(time_series, 1e-7)
        ),
        "d": np.real(
            p_pol[2] * p_rad * pWave
            + v_pol[2] * v_rad * svWave
            + h_pol[2] * h_rad * shWave
            + add_noise(time_series, 1e-7)
        ),
    }


def add_noise(time_series, rms):
    nt = len(time_series)
    freq = fftfreq(nt, time_series[1] - time_series[0])
    phase = 2.0 * pi * np.random.rand(nt // 2)
    itransform = zeros(len(freq), dtype=complex)
    for ii in range(1, nt // 2 - 1):
        itransform[ii] = exp(1j * phase[ii])
        itransform[nt - ii] = conj(itransform[ii])
    return rms * real(ifft(itransform)) * nt / np.sqrt(nt - 4)


# The weird np.sqrt(nt-4) term I beleive is a consequence
#    of the Nyquist and DC frequencies being set to zero.
