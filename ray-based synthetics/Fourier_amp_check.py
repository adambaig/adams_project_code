# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 12:04:52 2019

@author: adambaig
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft, ifft, fftfreq

dt = 0.001
time_series = np.arange(-1, 1, dt)
nt = len(time_series)

tau = 0.03

boxcar = np.zeros(nt)
ii = -1
for it in time_series:
    ii += 1
    if abs(it) < tau:
        boxcar[ii] = 1

freqs = fftfreq(nt, dt)
fig, ax = plt.subplots(2)
ax[0].plot(time_series, boxcar)
fboxcar = ifft(boxcar)
ax[1].plot(freqs, nt * dt * np.abs(fboxcar) / 2)

ax[0].plot(time_series, np.real(fft(fboxcar)), "--")
