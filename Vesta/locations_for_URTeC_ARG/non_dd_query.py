import os
import requests
import numpy as np
from obspy import UTCDateTime
import time
import ast
import json

###############inputs
host = "athena.1082c.nanometrics.net"
apikey = "fed58a767d650fd86f0672a43c6f4d40"
startTime = "2017-04-01 00:00:00"
endTime = "2020-10-07 00:00:00"
numberChunks = 20
outputPath = (
    r"C:\users\adambaig\Project\Vesta\locatoins_for_URTeC_ARG\non_HP_locations.csv"
)
#######################################


dirPath = os.path.dirname(outputPath)
if not os.path.isdir(dirPath):
    os.makedirs(dirPath)

startTime = UTCDateTime(startTime).timestamp
endTime = UTCDateTime(endTime).timestamp

graphqlurl = "https://{}/graphql?apiKey={}".format(host, apikey)


timeRange = np.linspace(startTime, endTime, numberChunks, endpoint=True)
nbody = {"data": {"eventList": {"size": 0, "events": []}}}

out_file = open(outputPath, "w")
iTime = timeRange[0]
i = 0

for i, iTime in enumerate(timeRange[:-1]):
    print(
        "Querying Time Range "
        + UTCDateTime(iTime).strftime("%Y%m%d.%H%M%S")
        + "to "
        + UTCDateTime(timeRange[i + 1]).strftime("%Y%m%d.%H%M%S")
    )
    query = (
        """
    query {
        eventList(
          limit: 10000,
          startTime: """
        + str(iTime)
        + """,
          endTime: """
        + str(timeRange[i + 1])
        + """,
        ) {
          size
          totalSize
          events {
            id
            externalId
            origins {
              momentTensors{
                strike
                dip
                rake
              }
              analysisType
              time
              latitude
              longitude
              referenceElevation
              depth
              preferredMagnitude {
                magnitudeType
                value
              }
            }
          }
        }
      }
    """
    )
    r = requests.post(graphqlurl, data={"query": query})

    status = r.status_code
    reason = r.reason
    body = r.text

    bodyt = ast.literal_eval(json.dumps(body))
    nbody = json.loads(bodyt)
    print("  event count, ", nbody["data"]["eventList"]["size"])
    for event in nbody["data"]["eventList"]["events"]:
        try:
            id = event["id"]
            externalID = event["externalId"].split()[0]
            eveTime = event["origins"][0]["time"]
            eveLat = event["origins"][0]["latitude"]
            eveLon = event["origins"][0]["longitude"]
            eveDepth = event["origins"][0]["depth"]
            eveMag = event["origins"][0]["preferredMagnitude"]["value"]
            out_file.write(
                str(id)
                + ","
                + str(externalID)
                + ","
                + str(eveTime)
                + ","
                + str(eveLat)
                + ","
                + str(eveLon)
                + ","
                + str(eveDepth)
                + ","
                + str(eveMag)
            )
            if len(event["origins"][0]["momentTensors"]) > 0:
                strike = event["origins"][0]["momentTensors"][0]["strike"]
                dip = event["origins"][0]["momentTensors"][0]["dip"]
                rake = event["origins"][0]["momentTensors"][0]["rake"]
                out_file.write("," + str(strike) + "," + str(dip) + "," + str(rake))
            out_file.write("\n")

        except:
            print("failed", event["id"])
            continue

    time.sleep(2)
out_file.close()
