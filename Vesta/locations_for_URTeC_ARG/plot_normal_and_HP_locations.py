import json
import os

import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import numpy as np
import pyproj as pr
from obspy import read_inventory
from obspy.imaging.beachball import beach
from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from sklearn.cluster import DBSCAN

from sms_moment_tensor.stress_inversions import (
    iterate_Micheal_stress_inversion,
    determine_SH_max,
    decompose_stress,
    Simpson_Aphi,
)
from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge


# import custom_colormaps
from read_inputs import read_hp_locations, read_mts

PI = np.pi
D2R = PI / 180.0

if not os.path.exists("Station_xml//VE_Full.xml"):
    inv = formStaXml(outDir="Station_xml//.", athenaCode="VE")
else:
    inv = read_inventory("Station_xml//VE_Full.xml")


majorLocator1 = MultipleLocator(10000)
latlon_proj = pr.Proj(init="epsg:3401")
out_proj = pr.Proj(init="epsg:4326")
# transformer = Transformer.from_crs("epsg:4326", "epsg:3081")
hp_events = read_hp_locations(r"DD_locations.csv")
mt_events = read_mts(r"events_with_MTs.csv")

search_radius = 20000  # in m
grid_spacing = 10000
min_neighbours = 10
stress_axis_scale = 100
mt_size = 1000
stress_map = custom_colormaps.stress_map()


def r_nearest_neighbours(grid_point, events, radius):
    radius_sq = radius * radius
    ref_east, ref_north = grid_point["east"], grid_point["north"]
    cull_events = {
        k: v
        for k, v in events.items()
        if ((v["easting"] - ref_east) ** 2 + (v["northing"] - ref_north) ** 2 < radius_sq)
    }
    return cull_events


lat = [v["latitude"] for v in hp_events.values()]
lon = [v["longitude"] for v in hp_events.values()]
eastings, northings = pr.transform(out_proj, latlon_proj, lon, lat)
for i_event, event in enumerate(hp_events):
    hp_events[event]["easting"] = eastings[i_event]
    hp_events[event]["northing"] = northings[i_event]
    hp_events[event]["elevation"] = -1000 * hp_events[event]["depth_km"]

stations = {}
for network in inv:
    netcode = network.code
    for station in network:
        easting, northing = pr.transform(out_proj, latlon_proj, station.longitude, station.latitude)
        stations[f"{netcode}.{station.code}"] = {"easting": easting, "northing": northing}

eastings, northings = np.array([(v["easting"], v["northing"]) for k, v in mt_events.items()]).T
east_range = np.arange(min(eastings) - grid_spacing, max(eastings) + grid_spacing, grid_spacing)
north_range = np.arange(min(northings) - grid_spacing, max(northings) + grid_spacing, grid_spacing)
quiver_east, quiver_north, trend_north, trend_east, a_phi = [], [], [], [], []





fig, (a1,a2) = plt.subplots(figsize=[16, 12])
[ax.set_aspect("equal") for ax in a1,a2]
hp_eastings, hp_northings = np.array([(v["easting"], v["northing"]) for k, v in hp_events.items()]).T
non_hp_eastings, non_hp_northings = np.array([(v["easting"], v["northing"]) for k, v in non_hp_events.items()]).T
a1.plot(
    non_hp_eastings, non_hp_northings, ".", color="k", ms=1, markeredgecolor="k", zorder=-5,
)
a2.plot(
    hp_eastings, hp_northings, ".", color="k", ms=1, markeredgecolor="k", zorder=-5,
)


[ax.plot(
    [v["easting"] for v in stations.values()],
    [v["northing"] for v in stations.values()],
    "v",
    color="0.3",
    markeredgecolor="k",
) for ax in a1,a2]
ax.set_facecolor("0.95")
ax.grid(True)
ax.xaxis.set_major_locator(MultipleLocator(2000))
ax.yaxis.set_major_locator(MultipleLocator(2000))
ax.set_yticklabels([])
ax.set_xticklabels([])
ax.tick_params(axis="both", which="both", color="w")
xmin = np.percentile(hp_eastings, 0.5)
xmax = np.percentile(hp_eastings, 99.5)
ymin = np.percentile(hp_northings, 0.5)
ymax = np.percentile(hp_northings, 99.5)
ax.set_xlim([xmin - 0.1 * (xmax - xmin), xmax + 0.1 * (xmax - xmin)])
ax.set_ylim([ymin - 0.1 * (ymax - ymin), ymax + 0.1 * (ymax - ymin)])
fig.savefig("Vesta_MTs_and_HP_locations_with_SHmax.png", bbox_inches="tight")


fg_cb = plt.figure(figsize=[4, 1])
ax_cb = fg_cb.add_axes([0.1, 0.6, 0.8, 0.2])
colorbar = np.array([np.arange(0, 255), np.arange(0, 255)])
ax_cb.pcolor(colorbar, cmap=stress_map)
ax_cb.set_xticks([0, 85, 170, 255])
ax_cb.set_xticklabels([])
ax_cb.tick_params(which="minor", color="w")
ax_cb.set_xticks([42.5, 127.5, 212.5], minor=True)
ax_cb.set_xticklabels(["normal", "strike-slip", "thrust"], minor=True)
ax_cb.set_yticks([])
ax_cb.set_xlabel("Stress State")
fg_cb.savefig("a_phi_legend.png", transparent=True)
plt.show()
