# import matplotlib
#
# matplotlib.use("Qt5agg")
#

import json
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import matplotlib.cm as cm
import numpy as np
import pyproj as pr
import os

from obspy import read_inventory
from obspy.imaging.beachball import beach
from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from sklearn.cluster import DBSCAN

from generalPlots import gray_background_with_grid
from sms_moment_tensor.stress_inversions import (
    iterate_Micheal_stress_inversion,
    determine_SH_max,
    decompose_stress,
    Simpson_Aphi,
)
from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge


import custom_colormaps
from read_inputs import read_hp_locations, read_mts

PI = np.pi
D2R = PI / 180.0

tab10 = cm.get_cmap("tab10")

if not os.path.exists("Station_xml//VE_Full.xml"):
    inv = formStaXml(outDir="Station_xml//.", athenaCode="VE")
else:
    inv = read_inventory("Station_xml//VE_Full.xml")

majorLocator1 = MultipleLocator(10000)
latlon_proj = pr.Proj(init="epsg:3401")
out_proj = pr.Proj(init="epsg:4326")
# transformer = Transformer.from_crs("epsg:4326", "epsg:3081")
hp_events = read_hp_locations(r"DD_locations.csv")
mt_events = read_mts(r"events_with_MTs.csv")
search_radius = 20000  # in m
grid_spacing = 10000
min_neighbours = 10
stress_ax_scale = 100
mt_size = 1000
stress_map = custom_colormaps.stress_map()


def r_nearest_neighbours(grid_point, events, radius):
    radius_sq = radius * radius
    ref_east, ref_north = grid_point["east"], grid_point["north"]
    cull_events = {
        k: v
        for k, v in events.items()
        if (
            (v["easting"] - ref_east) ** 2 + (v["northing"] - ref_north) ** 2
            < radius_sq
        )
    }
    return cull_events


stations = {}
for network in inv:
    netcode = network.code
    for station in network:
        easting, northing = pr.transform(
            out_proj, latlon_proj, station.longitude, station.latitude
        )
        stations[f"{netcode}.{station.code}"] = {
            "easting": easting,
            "northing": northing,
        }

lat = [v["latitude"] for v in hp_events.values()]
lon = [v["longitude"] for v in hp_events.values()]
eastings, northings = pr.transform(out_proj, latlon_proj, lon, lat)
for i_event, event in enumerate(hp_events):
    hp_events[event]["easting"] = eastings[i_event]
    hp_events[event]["northing"] = northings[i_event]
    hp_events[event]["elevation"] = -1000 * hp_events[event]["depth_km"]

xy = np.array([(v["easting"], v["northing"]) for v in hp_events.values()])
clustering = DBSCAN(eps=50, min_samples=4).fit(xy)
labels = clustering.labels_
i_cluster = np.where(labels > -1)[0]
i_outlier = np.where(labels == -1)[0]
mod_label = labels % 10
label = 100
i_xy = np.where(labels == label)[0]


clusters = {}
for label in np.unique(labels[i_cluster]):
    i_xy = np.where(labels == label)[0]
    xy_mean = np.average(xy[i_xy], axis=0)
    xy_demeaned = xy[i_xy] - xy_mean
    covariance = xy_demeaned.T @ xy_demeaned
    eig_vals, eig_vecs = np.linalg.eig(covariance)
    strike_rad = np.arctan2(*eig_vecs[:, np.argmax(eig_vals)])
    R = np.array(
        [
            [np.cos(strike_rad), -np.sin(strike_rad)],
            [np.sin(strike_rad), np.cos(strike_rad)],
        ]
    )
    clusters[label] = {
        "strike": strike_rad / D2R,
        "linearity": 1 - eig_vals[np.argmin(eig_vals)] / eig_vals[np.argmax(eig_vals)],
        "center": {"easting": xy_mean[0], "northing": xy_mean[1]},
        "variance": np.var([v[0] for v in xy_demeaned @ R.T]),
        "n_events": len(i_xy),
        "locations": xy[i_xy],
    }


fig, ax = plt.subplots(figsize=[12, 12])
ax.set_aspect("equal")
for i_cluster in [
    k for k, v in clusters.items() if v["linearity"] > 0.0 and v["n_events"] > 1
]:
    i_xy = np.where(labels == i_cluster)[0]
    ax.plot(
        xy[i_xy, 0],
        xy[i_xy, 1],
        "o",
        ms=3,
        c=tab10(((i_cluster + 0.5) % 10) / 10),
        zorder=1,
    )
ax.plot(xy[i_outlier, 0], xy[i_outlier, 1], ".", zorder=-3, c="0.3", ms=1.5)

xmin = np.percentile([v["easting"] for v in hp_events.values()], 0.5)
xmax = np.percentile([v["easting"] for v in hp_events.values()], 99.5)
ymin = np.percentile([v["northing"] for v in hp_events.values()], 0.5)
ymax = np.percentile([v["northing"] for v in hp_events.values()], 99.5)
ax.set_xlim([xmin - 0.1 * (xmax - xmin), xmax + 0.1 * (xmax - xmin)])
ax.set_ylim([ymin - 0.1 * (ymax - ymin), ymax + 0.1 * (ymax - ymin)])


ax.plot(
    [v["easting"] for v in stations.values()],
    [v["northing"] for v in stations.values()],
    "v",
    color="0.3",
    markeredgecolor="k",
)


ax = gray_background_with_grid(ax, grid_spacing=1000)


fig.savefig("all_identified_clusters.png")


fig, ax = plt.subplots()
ax.hist(
    [v["linearity"] for v in clusters.values() if v["n_events"] > 10],
    bins=np.arange(0, 1.001, 0.05),
    facecolor="firebrick",
)
ax.set_xlabel("linearity")
ax.set_ylabel("number of clusters")
ax.set_title("Minimum Number of 10 events in cluster")
fig.savefig("linearity_hist.png")

len(hp_events)

list(hp_events.items())[0]


min_lat = min([v["latitude"] for v in hp_events.values()])
min_time = min([v["UTC_time"] for v in hp_events.values()])
min_long = min([v["longitude"] for v in hp_events.values()])
max_lat = max([v["latitude"] for v in hp_events.values()])
max_time = max([v["UTC_time"] for v in hp_events.values()])
max_long = max([v["longitude"] for v in hp_events.values()])


mags = [v["magnitude"] for v in hp_events.values()]


from obspy.clients.fdsn.client import Client

client = Client("IRIS")
catalog = client.get_events(
    starttime=min([v["UTC_time"] for v in hp_events.values()]),
    endtime=max([v["UTC_time"] for v in hp_events.values()]),
    minlatitude=min([v["latitude"] for v in hp_events.values()]),
    maxlatitude=max([v["latitude"] for v in hp_events.values()]),
    minlongitude=min([v["longitude"] for v in hp_events.values()]),
    maxlongitude=max([v["longitude"] for v in hp_events.values()]),
)

magbins = np.arange(-1, 3, 0.1)
n_events = np.zeros(magbins.shape)
for i_bin, bin in enumerate(magbins):
    n_events[i_bin] = len(np.argwhere(mags > bin))

i_min, i_max = 7, 21
x = magbins[i_min:i_max]
y = np.log10(n_events[i_min:i_max])

A = np.vstack([x, np.ones(len(x))]).T

(slope, y_int) = np.linalg.lstsq(A, y, rcond=None)[0]

fig_b, ax_b = plt.subplots(figsize=[4, 4])

ax_b.semilogy(magbins, n_events, "o", color="royalblue", markeredgecolor="0.2")
ax_b.semilogy(
    [magbins[i_min], magbins[i_max]],
    [10 ** (magbins[i_min] * slope + y_int), 10 ** (magbins[i_max] * slope + y_int)],
    "firebrick",
    lw=2,
)

ax_b.set_ylabel("number of events")
ax_b.set_xlabel("local magnitude (Hutton and Boore, 1987)")
ax_b.text(0.9, 0.9, f"b = {-slope: .1f}", transform=ax_b.transAxes, ha="right")
fig_b.savefig("b_value_all.png", bbox_inches="tight")
