import matplotlib

matplotlib.use("Qt5agg")

import json
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import numpy as np
import pyproj as pr
import os

from obspy import read_inventory
from obspy.imaging.beachball import beach
from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from sklearn.cluster import DBSCAN

from generalPlots import gray_background_with_grid
from sms_moment_tensor.stress_inversions import (
    iterate_Micheal_stress_inversion,
    determine_SH_max,
    decompose_stress,
    Simpson_Aphi,
)
from sms_moment_tensor.plotting import stereonet_plot_stress_tensor
from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge
import mplstereonet as mpls

import custom_colormaps
from read_inputs import read_hp_locations, read_mts


hp_events = read_hp_locations(r"DD_locations.csv")
mt_events = read_mts(r"events_with_MTs.csv")


def determine_SH_max_spread(stress_tensors, n_sigma=2):
    sh_max = determine_SH_max(np.sum(stress_tensors, axis=0))
    sh_max_iteration = []
    for stress_tensor in iterations:
        sh_max_central = determine_SH_max(stress_tensor)
        possible_sh_maxes = np.array(
            [sh_max_central - 180.0, sh_max_central, sh_max_central + 180.0]
        )
        i_sh_max = np.argmin(abs(possible_sh_maxes - sh_max))
        sh_max_iteration.append(possible_sh_maxes[i_sh_max])
    return n_sigma * np.std(sh_max_iteration)


def determine_Aphi_spread(stress_tensors, n_sigma=2):
    a_phis = []
    for stress_tensor in stress_tensors:
        a_phis.append(Simpson_Aphi(stress_tensor))
    return n_sigma * np.std(a_phis)


stress_tensor, iterations = iterate_Micheal_stress_inversion(
    mt_events, 1000, output_iterations=True
)

fig = plt.figure()
ax = fig.add_subplot(111, projection="stereonet")


f_hist, a_hist = plt.subplots()
r_iterations, axes_iteration, sh_max_iteration = [], [], []
for iteration in iterations:
    R, stress_axes = decompose_stress(iteration)
    r_iterations.append(R)
    axes_iteration.append(stress_axes)
    for axis, vector in stress_axes.items():
        trend, plunge = unit_vector_to_trend_plunge(vector)
        ax.line(
            plunge,
            trend,
            ".",
            alpha=0.15,
            color=axis_color[axis],
            zorder=3,
        )
    sh_max_iteration.append(determine_SH_max(iteration))
ax.grid()
sh_max = determine_SH_max(stress_tensor)


sh_max = determine_SH_max(stress_tensor)
sh_max_error = determine_SH_max_spread(iterations)
Aphi_error = determine_Aphi_spread(iterations)
# quiver_east.append(grid_point["east"])
# quiver_north.append(grid_point["north"])
median_east = np.median(np.array([v["easting"] for k, v in nn_events.items()]))
median_north = np.median(np.array([v["northing"] for k, v in nn_events.items()]))
quiver_east.append(median_east)
quiver_north.append(median_north)
R, stress_axes = decompose_stress(stress_tensor)
trend_s2, plunge_s2 = unit_vector_to_trend_plunge(stress_axes["s2"])
trend_east.append(stress_axis_scale * np.sin(np.pi * sh_max / 180.0))
trend_north.append(stress_axis_scale * np.cos(np.pi * sh_max / 180.0))
a_phi.append(Simpson_Aphi(stress_tensor))
longitude, latitude = eastings, northings = pr.transform(
    latlon_proj, out_proj, median_east, median_north
)
