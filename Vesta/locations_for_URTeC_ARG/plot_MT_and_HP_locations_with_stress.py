import matplotlib

matplotlib.use("Qt5agg")

import json
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import numpy as np
import pyproj as pr
import os

from obspy import read_inventory
from obspy.imaging.beachball import beach
from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from sklearn.cluster import DBSCAN

from generalPlots import gray_background_with_grid
from sms_moment_tensor.stress_inversions import (
    iterate_Micheal_stress_inversion,
    determine_SH_max,
    decompose_stress,
    Simpson_Aphi,
)
from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge


import custom_colormaps
from read_inputs import read_hp_locations, read_mts

PI = np.pi
D2R = PI / 180.0

if not os.path.exists("Station_xml//VE_Full.xml"):
    inv = formStaXml(outDir="Station_xml//.", athenaCode="VE")
else:
    inv = read_inventory("Station_xml//VE_Full.xml")

majorLocator1 = MultipleLocator(10000)
latlon_proj = pr.Proj(init="epsg:3401")
out_proj = pr.Proj(init="epsg:4326")
# transformer = Transformer.from_crs("epsg:4326", "epsg:3081")
hp_events = read_hp_locations(r"DD_locations.csv")
mt_events = read_mts(r"events_with_MTs.csv")

search_radius = 5000  # in m
grid_spacing = 2500
min_neighbours = 8
stress_axis_scale = 100
mt_size = 1000
stress_map = custom_colormaps.stress_map()


def r_nearest_neighbours(grid_point, events, radius):
    radius_sq = radius * radius
    ref_east, ref_north = grid_point["east"], grid_point["north"]
    cull_events = {
        k: v
        for k, v in events.items()
        if (
            (v["easting"] - ref_east) ** 2 + (v["northing"] - ref_north) ** 2
            < radius_sq
        )
    }
    return cull_events


lat = [v["latitude"] for v in hp_events.values()]
lon = [v["longitude"] for v in hp_events.values()]
eastings, northings = pr.transform(out_proj, latlon_proj, lon, lat)
for i_event, event in enumerate(hp_events):
    hp_events[event]["easting"] = eastings[i_event]
    hp_events[event]["northing"] = northings[i_event]
    hp_events[event]["elevation"] = -1000 * hp_events[event]["depth_km"]

xy = np.array([(v["easting"], v["northing"]) for v in hp_events.values()])

clustering = DBSCAN(eps=50, min_samples=4).fit(xy)

labels = clustering.labels_

i_cluster = np.where(labels > -1)[0]
i_outlier = np.where(labels == -1)[0]
mod_label = labels % 10
fig, ax = plt.subplots(figsize=[12, 12])
ax.set_aspect("equal")
ax.scatter(
    xy[i_cluster, 0],
    xy[i_cluster, 1],
    marker=".",
    s=1,
    c=mod_label[i_cluster],
    vmin=-0.5,
    vmax=9.5,
    cmap="tab10",
)
ax.plot(xy[i_outlier, 0], xy[i_outlier, 1], ".", zorder=-3, c="0.3")
ax = gray_background_with_grid(ax, grid_spacing=1000)


for mt_event in mt_events:
    if mt_event in hp_events:
        mt_events[mt_event]["latitude"] = hp_events[mt_event]["latitude"]
        mt_events[mt_event]["longitude"] = hp_events[mt_event]["longitude"]
        mt_events[mt_event]["depth_km"] = hp_events[mt_event]["depth_km"]
        mt_events[mt_event]["easting"] = hp_events[mt_event]["easting"]
        mt_events[mt_event]["northing"] = hp_events[mt_event]["northing"]
        mt_events[mt_event]["elevation"] = hp_events[mt_event]["elevation"]
    else:
        easting, northing = pr.transform(
            out_proj,
            latlon_proj,
            mt_events[mt_event]["longitude"],
            mt_events[mt_event]["latitude"],
        )
        mt_events[mt_event]["easting"] = easting
        mt_events[mt_event]["northing"] = northing
        mt_events[mt_event]["elevation"] = -1000 * mt_events[mt_event]["depth_km"]

stations = {}
for network in inv:
    netcode = network.code
    for station in network:
        easting, northing = pr.transform(
            out_proj, latlon_proj, station.longitude, station.latitude
        )
        stations[f"{netcode}.{station.code}"] = {
            "easting": easting,
            "northing": northing,
        }

eastings, northings = np.array(
    [(v["easting"], v["northing"]) for k, v in mt_events.items()]
).T
east_range = np.arange(
    min(eastings) - grid_spacing, max(eastings) + grid_spacing, grid_spacing
)
north_range = np.arange(
    min(northings) - grid_spacing, max(northings) + grid_spacing, grid_spacing
)
quiver_east, quiver_north, trend_north, trend_east, a_phi = [], [], [], [], []


def determine_SH_max_spread(stress_tensors, n_sigma=2):
    sh_max = determine_SH_max(np.sum(stress_tensors, axis=0))
    sh_max_iteration = []
    for stress_tensor in iterations:
        sh_max_central = determine_SH_max(stress_tensor)
        possible_sh_maxes = np.array(
            [sh_max_central - 180.0, sh_max_central, sh_max_central + 180.0]
        )
        i_sh_max = np.argmin(abs(possible_sh_maxes - sh_max))
        sh_max_iteration.append(possible_sh_maxes[i_sh_max])
    return n_sigma * np.std(sh_max_iteration)


def determine_Aphi_spread(stress_tensors, n_sigma=2):
    a_phis = []
    for stress_tensor in stress_tensors:
        a_phis.append(Simpson_Aphi(stress_tensor))
    return n_sigma * np.std(a_phis)


f = open("shmax_orientations.csv", "w")
f.write(
    "latitude,longitude,SH max, 2 sigma err," "A phi, 2 sigma err, number of events\n"
)


east, north = pr.transform(
    out_proj,
    latlon_proj,
    -103.813649476043,
    31.7572601089813,
)
grid_point = {"east": east, "north": north}
nn_events = r_nearest_neighbours(grid_point, mt_events, search_radius)


stress_tensor, iterations = iterate_Micheal_stress_inversion(
    mt_events, 1000, output_iterations=True
)


for east in east_range:
    for north in north_range:
        grid_point = {"east": east, "north": north}
        nn_events = r_nearest_neighbours(grid_point, mt_events, search_radius)
        if len(nn_events) >= min_neighbours:
            stress_tensor, iterations = iterate_Micheal_stress_inversion(
                nn_events, 100, output_iterations=True
            )

            sh_max = determine_SH_max(stress_tensor)
            sh_max_error = determine_SH_max_spread(iterations)
            Aphi_error = determine_Aphi_spread(iterations)
            # quiver_east.append(grid_point["east"])
            # quiver_north.append(grid_point["north"])
            median_east = np.median(
                np.array([v["easting"] for k, v in nn_events.items()])
            )
            median_north = np.median(
                np.array([v["northing"] for k, v in nn_events.items()])
            )
            quiver_east.append(median_east)
            quiver_north.append(median_north)
            R, stress_axes = decompose_stress(stress_tensor)
            trend_s2, plunge_s2 = unit_vector_to_trend_plunge(stress_axes["s2"])
            trend_east.append(stress_axis_scale * np.sin(np.pi * sh_max / 180.0))
            trend_north.append(stress_axis_scale * np.cos(np.pi * sh_max / 180.0))
            a_phi.append(Simpson_Aphi(stress_tensor))
            longitude, latitude = eastings, northings = pr.transform(
                latlon_proj, out_proj, median_east, median_north
            )
            f.write(
                f"{latitude:.4f},{longitude:.4f},"
                f"{sh_max:.1f},{sh_max_error:.1f},"
                f"{a_phi[-1]:.3f},{Aphi_error:.3f},{len(nn_events)}\n"
            )
#            print(f"{len(nn_events)} events: east is {east}, north is {north}")
f.close()

fig, ax = plt.subplots(figsize=[16, 12])
ax.set_aspect("equal")
hp_eastings, hp_northings = np.array(
    [(v["easting"], v["northing"]) for k, v in hp_events.items()]
).T
ax.plot(
    hp_eastings,
    hp_northings,
    ".",
    color="k",
    ms=1,
    markeredgecolor="k",
    zorder=-5,
)


for event in mt_events:
    beachball = beach(
        np.array(
            [
                mt_events[event]["strike"],
                mt_events[event]["dip"],
                mt_events[event]["rake"],
            ]
        ),
        xy=(mt_events[event]["easting"], mt_events[event]["northing"]),
        width=mt_size,
        facecolor="darkgoldenrod",
        alpha=0.8,
        linewidth=0.25,
        zorder=3,
    )
    ax.add_collection(beachball)

ax.quiver(
    quiver_east,
    quiver_north,
    trend_east,
    trend_north,
    color=stress_map(np.array(a_phi) / 3),
    pivot="mid",
    width=0.003,
    zorder=5,
    headlength=0,
    headaxislength=0,
    headwidth=0,
    scale=1 / stress_axis_scale,
    scale_units="x",
    edgecolor="k",
    linewidth=0.5,
)


ax.plot(
    [v["easting"] for v in stations.values()],
    [v["northing"] for v in stations.values()],
    "v",
    color="0.3",
    markeredgecolor="k",
)
ax.set_facecolor("0.95")
ax.grid(True)
ax.xaxis.set_major_locator(MultipleLocator(2000))
ax.yaxis.set_major_locator(MultipleLocator(2000))
ax.set_yticklabels([])
ax.set_xticklabels([])
ax.tick_params(axis="both", which="both", color="w")
xmin = np.percentile(hp_eastings, 0.5)
xmax = np.percentile(hp_eastings, 99.5)
ymin = np.percentile(hp_northings, 0.5)
ymax = np.percentile(hp_northings, 99.5)
ax.set_xlim([xmin - 0.1 * (xmax - xmin), xmax + 0.1 * (xmax - xmin)])
ax.set_ylim([ymin - 0.1 * (ymax - ymin), ymax + 0.1 * (ymax - ymin)])
fig.savefig("MTs_locations.png", bbox_inches="tight")


fg_cb = plt.figure(figsize=[4, 1])
ax_cb = fg_cb.add_axes([0.1, 0.6, 0.8, 0.2])
colorbar = np.array([np.arange(0, 255), np.arange(0, 255)])
ax_cb.pcolor(colorbar, cmap=stress_map)
ax_cb.set_xticks([0, 85, 170, 255])
ax_cb.set_xticklabels([])
ax_cb.tick_params(which="minor", color="w")
ax_cb.set_xticks([42.5, 127.5, 212.5], minor=True)
ax_cb.set_xticklabels(["normal", "strike-slip", "thrust"], minor=True)
ax_cb.set_yticks([])
ax_cb.set_xlabel("Stress State")
fg_cb.savefig("a_phi_legend.png", transparent=True)
plt.show()
