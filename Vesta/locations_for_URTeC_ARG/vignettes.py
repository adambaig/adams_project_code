import json
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import matplotlib.cm as cm
import numpy as np
import pyproj as pr
import os

from obspy import read_inventory
from obspy.imaging.beachball import beach
from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from sklearn.cluster import DBSCAN

from generalPlots import gray_background_with_grid
from sms_moment_tensor.stress_inversions import (
    iterate_Micheal_stress_inversion,
    determine_SH_max,
    decompose_stress,
    resolve_shear_and_normal_stress,
)
from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge, strike_dip_to_normal


# import custom_colormaps
from read_inputs import read_hp_locations, read_mts

PI = np.pi
D2R = PI / 180.0

tab10 = cm.get_cmap("tab10")

if not os.path.exists("Station_xml//VE_Full.xml"):
    inv = formStaXml(outDir="Station_xml//.", athenaCode="VE")
else:
    inv = read_inventory("Station_xml//VE_Full.xml")

majorLocator1 = MultipleLocator(10000)
latlon_proj = pr.Proj(init="epsg:3401")
out_proj = pr.Proj(init="epsg:4326")
# transformer = Transformer.from_crs("epsg:4326", "epsg:3081")
hp_events = read_hp_locations(r"DD_locations.csv")
mt_events = read_mts(r"events_with_MTs.csv")
search_radius = 20000  # in m
grid_spacing = 10000
min_neighbours = 10
stress_ax_scale = 100
mt_size = 1000
stress_map = custom_colormaps.stress_map()


def r_nearest_neighbours(grid_point, events, radius):
    radius_sq = radius * radius
    ref_east, ref_north = grid_point["east"], grid_point["north"]
    cull_events = {
        k: v
        for k, v in events.items()
        if (
            (v["easting"] - ref_east) ** 2 + (v["northing"] - ref_north) ** 2
            < radius_sq
        )
    }
    return cull_events


stations = {}
for network in inv:
    netcode = network.code
    for station in network:
        easting, northing = pr.transform(
            out_proj, latlon_proj, station.longitude, station.latitude
        )
        stations[f"{netcode}.{station.code}"] = {
            "easting": easting,
            "northing": northing,
        }

lat = [v["latitude"] for v in hp_events.values()]
lon = [v["longitude"] for v in hp_events.values()]
eastings, northings = pr.transform(out_proj, latlon_proj, lon, lat)
for i_event, event in enumerate(hp_events):
    hp_events[event]["easting"] = eastings[i_event]
    hp_events[event]["northing"] = northings[i_event]
    hp_events[event]["elevation"] = -1000 * hp_events[event]["depth_km"]


xy = np.array([(v["easting"], v["northing"]) for v in hp_events.values()])
clustering = DBSCAN(eps=50, min_samples=4).fit(xy)
labels = clustering.labels_
i_cluster = np.where(labels > -1)[0]
i_outlier = np.where(labels == -1)[0]
mod_label = labels % 10
label = 100
i_xy = np.where(labels == label)[0]


frames = {
    "north_cluster": {
        "left": 65191.66510291767,
        "right": 70780.7619633665,
        "bottom": 5798290.463504646,
        "top": 5804565.015640433,
        "b_range": [-0.3, 0.8],
    },
    "middle_cluster": {
        "left": 69300.79286746324,
        "right": 74101.14852711605,
        "bottom": 5790252.082879158,
        "top": 5795442.711356669,
        "b_range": [-0.2, 1.9],
    },
    "block": {
        "left": 63153.838649759826,
        "right": 66203.51245704984,
        "bottom": 5781460.934834806,
        "top": 5785106.610883622,
        "b_range": [0, 1.3],
    },
    "bookcase": {
        "left": 64874.603282419805,
        "right": 66725.17957541352,
        "bottom": 5775653.685851586,
        "top": 5778834.363855169,
        "b_range": [-0.3, 0.4],
    },
}


clusters = {}
for label in np.unique(labels[i_cluster]):
    i_xy = np.where(labels == label)[0]
    xy_mean = np.average(xy[i_xy], axis=0)
    xy_demeaned = xy[i_xy] - xy_mean
    covariance = xy_demeaned.T @ xy_demeaned
    eig_vals, eig_vecs = np.linalg.eig(covariance)
    strike_rad = np.arctan2(*eig_vecs[:, np.argmax(eig_vals)])
    R = np.array(
        [
            [np.cos(strike_rad), -np.sin(strike_rad)],
            [np.sin(strike_rad), np.cos(strike_rad)],
        ]
    )
    clusters[label] = {
        "strike": strike_rad / D2R,
        "linearity": 1 - eig_vals[np.argmin(eig_vals)] / eig_vals[np.argmax(eig_vals)],
        "center": {"easting": xy_mean[0], "northing": xy_mean[1]},
        "variance": np.var([v[0] for v in xy_demeaned @ R.T]),
        "n_events": len(i_xy),
        "locations": xy[i_xy],
    }


fig, ax = plt.subplots(figsize=[12, 12])
ax.set_aspect("equal")
for i_cluster in [
    k for k, v in clusters.items() if v["linearity"] > 0.0 and v["n_events"] > 1
]:
    i_xy = np.where(labels == i_cluster)[0]
    ax.plot(
        xy[i_xy, 0],
        xy[i_xy, 1],
        "o",
        ms=3,
        c=tab10(((i_cluster + 0.5) % 10) / 10),
        zorder=1,
    )
ax.plot(xy[i_outlier, 0], xy[i_outlier, 1], ".", zorder=-3, c="0.3", ms=1.5)

xmin = np.percentile([v["easting"] for v in hp_events.values()], 0.5)
xmax = np.percentile([v["easting"] for v in hp_events.values()], 99.5)
ymin = np.percentile([v["northing"] for v in hp_events.values()], 0.5)
ymax = np.percentile([v["northing"] for v in hp_events.values()], 99.5)
ax.set_xlim([xmin - 0.1 * (xmax - xmin), xmax + 0.1 * (xmax - xmin)])
ax.set_ylim([ymin - 0.1 * (ymax - ymin), ymax + 0.1 * (ymax - ymin)])


ax.plot(
    [v["easting"] for v in stations.values()],
    [v["northing"] for v in stations.values()],
    "v",
    color="0.3",
    markeredgecolor="k",
)


circle_x, circle_y = [], []
for i_rad in np.arange(0, 2 * PI + PI / 100, PI / 100):
    circle_x.append(np.cos(i_rad))
    circle_y.append(np.sin(i_rad))
circle_x = np.array(circle_x)
circle_y = np.array(circle_y)

stress_tensor, iterations = iterate_Micheal_stress_inversion(
    mt_events, 100, output_iterations=True
)
R_shape, stress_axes = decompose_stress(stress_tensor)

dip = 90
for vignette, frame in frames.items():
    framed_clusters = {
        k: v
        for k, v in clusters.items()
        if v["center"]["easting"] > frame["left"]
        and v["center"]["easting"] < frame["right"]
        and v["center"]["northing"] > frame["bottom"]
        and v["center"]["northing"] < frame["top"]
        and v["linearity"] > 0.8
        and v["n_events"] > 9
    }
    framed_events = {
        k: v
        for k, v in hp_events.items()
        if v["easting"] > frame["left"]
        and v["easting"] < frame["right"]
        and v["northing"] > frame["bottom"]
        and v["northing"] < frame["top"]
    }
    i_min = np.argmin(abs(frame["b_range"][0] - magbins))
    i_max = np.argmin(abs(frame["b_range"][1] - magbins))
    # taus, sigmas = np.zeros(len(framed_clusters)), np.zeros(len(framed_clusters))
    # for i_cluster, cluster in enumerate(framed_clusters.values()):
    #     print('{vignette} {i_cluster}')
    #     normal_vector = strike_dip_to_normal(cluster["strike"], dip)
    #     sigmas[i_cluster], taus[i_cluster] = resolve_shear_and_normal_stress(stress_tensor, normal_vector)
    mags = [v["magnitude"] for v in framed_events.values()]
    magbins = np.arange(-1, 3, 0.1)
    n_events = np.zeros(magbins.shape)
    for i_bin, bin in enumerate(magbins):
        n_events[i_bin] = len(np.argwhere(mags > bin))
    x = magbins[i_min:i_max]
    y = np.log10(n_events[i_min:i_max])
    A = np.vstack([x, np.ones(len(x))]).T
    (slope, y_int) = np.linalg.lstsq(A, y, rcond=None)[0]
    fig_b, ax_b = plt.subplots(figsize=[4, 4])
    ax_b.semilogy(magbins, n_events, "o", color="royalblue", markeredgecolor="0.2")
    ax_b.semilogy(
        [magbins[i_min], magbins[i_max]],
        [
            10 ** (magbins[i_min] * slope + y_int),
            10 ** (magbins[i_max] * slope + y_int),
        ],
        "firebrick",
        lw=2,
    )
    ax_b.set_ylabel("number of events")
    ax_b.set_xlabel("local magnitude (Hutton and Boore, 1987)")
    ax_b.text(0.9, 0.9, f"b = {-slope: .1f}", transform=ax_b.transAxes, ha="right")
    fig_b.savefig(f"b_value_{vignette}.png", bbox_inches="tight")

    fig_mc, a_mc = plt.subplots()
    a_mc.set_aspect("equal")
    a_mc.plot(circle_x, circle_y, "0.3", zorder=10)
    a_mc.plot(R_shape * circle_x + 1 - R_shape, R_shape * circle_y, "0.3", zorder=10)
    a_mc.plot(
        (1 - R_shape) * circle_x - R_shape, (1 - R_shape) * circle_y, "0.3", zorder=10
    )
    for i_cluster, cluster in framed_clusters.items():
        normal_vector = strike_dip_to_normal(cluster["strike"], dip)
        sigma, tau = resolve_shear_and_normal_stress(
            stress_tensor, normal_vector, sign_shear=True
        )
        a_mc.plot(
            sigma, tau, "o", c=tab10(((i_cluster + 0.5) % 10) / 10), markeredgecolor="k"
        )
    a_mc.arrow(-1.1, 0, 2.2, 0, head_width=0.06, head_length=0.1, fc="k", ec="k")
    a_mc.arrow(-1.1, 0, 0, 1.1, head_width=0.06, head_length=0.1, fc="k", ec="k")
    a_mc.arrow(-1.1, 0, 0, -1.1, head_width=0.06, head_length=0.1, fc="k", ec="k")
    a_mc.text(-1.1, 1.22, "$\\tau$", ha="center")
    a_mc.text(-1.1, -1.22, "$-\\tau$", ha="center", va="top")
    a_mc.text(1.21, 0.0, "${\\sigma}$", va="center")
    a_mc.text(-0.98, -0.1, "$\\sigma_3$")
    a_mc.text(-0.98, -0.1, "$\\sigma_3$")
    a_mc.text(1 - 2 * R_shape + 0.03, -0.1, "$\\sigma_2$")
    a_mc.text(1.02, -0.1, "$\\sigma_1$")
    a_mc.axis("off")
    a_mc.set_xlim(-1.3, 1.3)
    a_mc.set_ylim(-1.3, 1.3)
    fig_mc.savefig(f"mohr_circle_{vignette}.png", bbox_inches="tight")

    # fig_z_scatter, ax_z = plt.subplots()
    # ax_z.set_aspect("equal")
    # ax_z.scatter(
    #     [v["easting"] for v in framed_events.values()],
    #     [v["northing"] for v in framed_events.values()],
    #     c=[v["elevation"] for v in framed_events.values()],
    # )
    # fig_z_scatter.savefig(f"coloured_by_depth_{vignette}.png", bbox_inches="tight")
    fig_c, ax_c = plt.subplots()
    ax_c.set_aspect("equal")
    ax_c.plot(
        [v["easting"] for v in framed_events.values()],
        [v["northing"] for v in framed_events.values()],
        ".",
        c="0.3",
        ms=1.5,
        zorder=-3,
    )
    for i_cluster, cluster in framed_clusters.items():
        ax_c.plot(
            cluster["locations"][:, 0],
            cluster["locations"][:, 1],
            "o",
            ms=4,
            c=tab10(((i_cluster + 0.5) % 10) / 10),
            markeredgecolor="k",
        )

    ax_c = gray_background_with_grid(ax_c, grid_spacing=1000)
    fig_c.savefig(f"coloured_by_cluster_{vignette}.png", bbox_inches="tight")

# cluster['locations'][:,0]
# frame = frames["north_cluster"]
#
# framed_events = {
#     k: v
#     for k, v in hp_events.items()
#     if v["easting"] > frame["left"]
#     and v["easting"] < frame["right"]
#     and v["northing"] > frame["bottom"]
#     and v["northing"] < frame["top"]
# }
#
# f, a = plt.subplots()
# a.set_aspect("equal")
# a.plot(
#     [v["northing"] - v["easting"] for v in framed_events.values()],
#     [v["elevation"] for v in framed_events.values()],
#     ".",
# )
