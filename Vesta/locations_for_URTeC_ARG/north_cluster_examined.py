import json
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import matplotlib.cm as cm
import numpy as np
import pyproj as pr
import os

from obspy import read_inventory
from obspy.imaging.beachball import beach
from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from sklearn.cluster import DBSCAN

from generalPlots import gray_background_with_grid
from pfi_qi.rotations import rotate_from_cardinal

from sms_moment_tensor.stress_inversions import (
    iterate_Micheal_stress_inversion,
    determine_SH_max,
    decompose_stress,
    resolve_shear_and_normal_stress,
)
from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge, strike_dip_to_normal

import custom_colormaps
from read_inputs import read_hp_locations, read_mts

PI = np.pi
D2R = PI / 180.0
tab10 = cm.get_cmap("tab10")

if not os.path.exists("Station_xml//VE_Full.xml"):
    inv = formStaXml(outDir="Station_xml//.", athenaCode="VE")
else:
    inv = read_inventory("Station_xml//VE_Full.xml")

majorLocator1 = MultipleLocator(10000)
latlon_proj = pr.Proj(init="epsg:3401")
out_proj = pr.Proj(init="epsg:4326")
# transformer = Transformer.from_crs("epsg:4326", "epsg:3081")
hp_events = read_hp_locations(r"DD_locations.csv")
mt_events = read_mts(r"events_with_MTs.csv")
search_radius = 20000  # in m
grid_spacing = 10000
min_neighbours = 10
stress_ax_scale = 100
mt_size = 1000
stress_map = custom_colormaps.stress_map()


def r_nearest_neighbours(grid_point, events, radius):
    radius_sq = radius * radius
    ref_east, ref_north = grid_point["east"], grid_point["north"]
    cull_events = {
        k: v
        for k, v in events.items()
        if (
            (v["easting"] - ref_east) ** 2 + (v["northing"] - ref_north) ** 2
            < radius_sq
        )
    }
    return cull_events


stations = {}
for network in inv:
    netcode = network.code
    for station in network:
        easting, northing = pr.transform(
            out_proj, latlon_proj, station.longitude, station.latitude
        )
        stations[f"{netcode}.{station.code}"] = {
            "easting": easting,
            "northing": northing,
        }

lat = [v["latitude"] for v in hp_events.values()]
lon = [v["longitude"] for v in hp_events.values()]
eastings, northings = pr.transform(out_proj, latlon_proj, lon, lat)
for i_event, event in enumerate(hp_events):
    hp_events[event]["easting"] = eastings[i_event]
    hp_events[event]["northing"] = northings[i_event]
    hp_events[event]["elevation"] = -1000 * hp_events[event]["depth_km"]


for mt_event in mt_events:
    if mt_event in hp_events:
        mt_events[mt_event]["latitude"] = hp_events[mt_event]["latitude"]
        mt_events[mt_event]["longitude"] = hp_events[mt_event]["longitude"]
        mt_events[mt_event]["depth_km"] = hp_events[mt_event]["depth_km"]
        mt_events[mt_event]["easting"] = hp_events[mt_event]["easting"]
        mt_events[mt_event]["northing"] = hp_events[mt_event]["northing"]
        mt_events[mt_event]["elevation"] = hp_events[mt_event]["elevation"]
    else:
        easting, northing = pr.transform(
            out_proj,
            latlon_proj,
            mt_events[mt_event]["longitude"],
            mt_events[mt_event]["latitude"],
        )
        mt_events[mt_event]["easting"] = easting
        mt_events[mt_event]["northing"] = northing
        mt_events[mt_event]["elevation"] = -1000 * mt_events[mt_event]["depth_km"]

xy = np.array([(v["easting"], v["northing"]) for v in hp_events.values()])
clustering = DBSCAN(eps=50, min_samples=4).fit(xy)
labels = clustering.labels_
i_cluster = np.where(labels > -1)[0]
i_outlier = np.where(labels == -1)[0]
mod_label = labels % 10

for i_event, event in enumerate(hp_events.values()):
    event["label"] = labels[i_event]


frames = {
    "north_cluster": {
        "left": 65191.66510291767,
        "right": 70780.7619633665,
        "bottom": 5798290.463504646,
        "top": 5804565.015640433,
        "b_range": [-0.3, 0.8],
    },
    "middle_cluster": {
        "left": 69300.79286746324,
        "right": 74101.14852711605,
        "bottom": 5790252.082879158,
        "top": 5795442.711356669,
        "b_range": [-0.2, 1.9],
    },
    "block": {
        "left": 63153.838649759826,
        "right": 66203.51245704984,
        "bottom": 5781460.934834806,
        "top": 5785106.610883622,
        "b_range": [0, 1.3],
    },
    "bookcase": {
        "left": 64874.603282419805,
        "right": 66725.17957541352,
        "bottom": 5775653.685851586,
        "top": 5778834.363855169,
        "b_range": [-0.3, 0.4],
    },
}

center = {"easting": 68000, "northing": 5800000}
clusters = {}
for label in np.unique(labels[i_cluster]):
    i_xy = np.where(labels == label)[0]
    xy_mean = np.average(xy[i_xy], axis=0)
    xy_demeaned = xy[i_xy] - xy_mean
    covariance = xy_demeaned.T @ xy_demeaned
    eig_vals, eig_vecs = np.linalg.eig(covariance)
    strike_rad = np.arctan2(*eig_vecs[:, np.argmax(eig_vals)])
    R = np.array(
        [
            [np.cos(strike_rad), -np.sin(strike_rad)],
            [np.sin(strike_rad), np.cos(strike_rad)],
        ]
    )
    clusters[label] = {
        "strike": strike_rad / D2R,
        "linearity": 1 - eig_vals[np.argmin(eig_vals)] / eig_vals[np.argmax(eig_vals)],
        "center": {"easting": xy_mean[0], "northing": xy_mean[1]},
        "variance": np.var([v[0] for v in xy_demeaned @ R.T]),
        "n_events": len(i_xy),
        "locations": xy[i_xy],
    }


for vignette, frame in {
    k: v for k, v in frames.items() if k == "north_cluster"
}.items():
    framed_clusters = {
        k: v
        for k, v in clusters.items()
        if v["center"]["easting"] > frame["left"]
        and v["center"]["easting"] < frame["right"]
        and v["center"]["northing"] > frame["bottom"]
        and v["center"]["northing"] < frame["top"]
        and v["linearity"] > 0.8
        and v["n_events"] > 9
    }
    framed_events = {
        k: v
        for k, v in hp_events.items()
        if v["easting"] > frame["left"]
        and v["easting"] < frame["right"]
        and v["northing"] > frame["bottom"]
        and v["northing"] < frame["top"]
    }

    fig_c, ax_c = plt.subplots()
    ax_c.set_aspect("equal")
    ax_c.plot(
        [v["easting"] for v in framed_events.values()],
        [v["northing"] for v in framed_events.values()],
        ".",
        c="0.3",
        ms=1.5,
        zorder=-3,
    )
    for i_cluster, cluster in framed_clusters.items():
        ax_c.plot(
            cluster["locations"][:, 0],
            cluster["locations"][:, 1],
            "o",
            ms=4,
            c=tab10(((i_cluster + 0.5) % 10) / 10),
            markeredgecolor="k",
        )
    for event in mt_events:
        beachball = beach(
            np.array(
                [
                    mt_events[event]["strike"],
                    mt_events[event]["dip"],
                    mt_events[event]["rake"],
                ]
            ),
            xy=(mt_events[event]["easting"], mt_events[event]["northing"]),
            width=mt_size,
            facecolor="darkgoldenrod",
            alpha=0.8,
            linewidth=0.25,
            zorder=3,
        )
        ax_c.add_collection(beachball)

    ax_c = gray_background_with_grid(ax_c, grid_spacing=1000)
    fig_c.savefig(f"coloured_by_cluster_with_MT_{vignette}.png", bbox_inches="tight")

    framed_events = rotate_from_cardinal(framed_events, PI / 4, center)

    fig_d, ax_d = plt.subplots()
    ax_d.set_aspect("equal")
    for event in framed_events.values():
        if abs(event["perp to trend"]) < 500 and event["magnitude"] > -3:
            if event["label"] == -1:
                ax_d.plot(
                    event["along trend"], event["elevation"], ".", c="0.3", zorder=0
                )
            else:
                ax_d.plot(
                    event["along trend"],
                    event["elevation"],
                    "o",
                    zorder=3,
                    ms=4,
                    c=tab10(((event["label"] + 0.5) % 10) / 10),
                    markeredgecolor="k",
                )
    ax_d.set_xlim([-707, 3000])
    ax_d = gray_background_with_grid(ax_d, grid_spacing=100)

    fig_d.savefig("north_cluster_depth_view.png", bbox_inches="tight")
