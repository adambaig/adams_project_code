import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import mplstereonet as mpls

strike1 = 302.4
dip1 = 82.9
rake1 = 67.9
strike2 = 195.5
dip2 = 23.2
rake2 = 161.7


fix, ax = mpls.subplots()
ax.plane(strike1, dip1, "royalblue")
ax.pole(strike2, dip2)
ax.plane(strike2, dip2, "firebrick")
ax.pole(strike1, dip1)


ax.grid()
plt.show()
