import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt, mpld3
import matplotlib.cm as cm
import numpy as np
from obspy.imaging.mopad_wrapper import beach
from pyproj import Transformer

from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import mt_matrix_to_vector
from sms_moment_tensor.plotting import plot_regression
from sms_ray_modelling.raytrace import (
    isotropic_ray_trace,
    takeoff_angle_Focmec,
)

from read_inputs import read_velocity_model

bwr = cm.get_cmap("bwr")

transformer = Transformer.from_crs("epsg:4326", "epsg:3401")


velocity_model = read_velocity_model("inputs//vesta_simple.txt")
frequency = 1
dr = "regmti_amps//"
for amp_file in glob.glob(dr + "*.csv"):
    inversion_name = amp_file.split("\\")[-1].split(".")[0]
    f = open(amp_file)
    #    head = f.readline()
    lines = f.readlines()
    f.close()
    lspl = lines[0].split(",")
    easting, northing = transformer.transform(float(lspl[7]), float(lspl[8]))
    picks = {"e": easting, "n": northing, "z": float(lspl[9])}
    f_map, ax_map = plt.subplots(1, 2, sharex=True, sharey=True)
    [ax_map[_].set_aspect("equal") for _ in range(2)]
    ax_map[0].plot(picks["e"], picks["n"], "*")
    ax_map[1].plot(picks["e"], picks["n"], "*")
    for line in lines:
        lspl = line.split(",")
        station, channel = lspl[5:7]
        stat_chan = station + "." + channel
        if station not in picks:
            station_easting, station_northing = transformer.transform(
                float(lspl[10]), float(lspl[11])
            )
            picks[station] = {
                "e": station_easting,
                "n": station_northing,
                "z": float(lspl[12]),
            }
        picks[station][channel] = {"phase": lspl[1], "amp": float(lspl[2])}

    A_matrix = []
    right_hand_side = []
    amps = []
    tags = []
    weights = []
    fig, ax = plt.subplots()

    for station in {k: v for k, v in picks.items() if k not in ["e", "n", "z"]}:
        p_amp = picks[station]["HHZ"]["amp"]
        p_raypath = isotropic_ray_trace(picks, picks[station], velocity_model, "P")
        s_raypath = isotropic_ray_trace(picks, picks[station], velocity_model, "S")
        azimuth = np.arctan2(
            p_raypath["hrz_slowness"]["n"], p_raypath["hrz_slowness"]["e"]
        )

        ax_map[0].plot(
            picks[station]["e"],
            picks[station]["n"],
            "v",
            c=bwr((np.sign(p_amp) + 1) / 2),
        )
        P_back_project = back_project_amplitude(
            p_raypath, s_raypath, 1.0, "P", frequency=frequency
        )
        SV_back_project = back_project_amplitude(
            p_raypath, s_raypath, 1.0, "SV", frequency=frequency
        )
        SH_back_project = back_project_amplitude(
            p_raypath, s_raypath, 1.0, "SH", frequency=frequency
        )
        A_matrix.append(inversion_matrix_row(p_raypath, "P"))
        right_hand_side.append(p_amp * P_back_project)
        amps.append(p_amp)
        tags.append(station + ".P")

        weights.append(P_back_project)
        sn_amp = picks[station]["HHN"]["amp"]
        se_amp = picks[station]["HHE"]["amp"]

        sv_amp = se_amp * np.cos(azimuth) - sn_amp * np.sin(azimuth)
        sh_amp = se_amp * np.sin(azimuth) + sn_amp * np.cos(azimuth)

        ax_map[1].quiver(picks[station]["e"], picks[station]["n"], se_amp, sn_amp)
        A_matrix.append(inversion_matrix_row(s_raypath, "SV"))
        right_hand_side.append(sv_amp * SV_back_project)
        A_matrix.append(inversion_matrix_row(s_raypath, "SH"))
        right_hand_side.append(sh_amp * SH_back_project)

        amps.append(se_amp)
        tags.append(station + ".V")
        weights.append(SV_back_project)
        amps.append(sn_amp)
        tags.append(station + ".H")
        weights.append(SH_back_project)
    weights = np.ones(len(weights))
    mt = solve_moment_tensor(A_matrix, right_hand_side, weights=weights)
    mt_dc = mt["dc"]
    beachball = beach(
        mt_matrix_to_vector(mt_dc / np.linalg.norm(mt_dc)),
        xy=(1, 1),
        width=1,
        facecolor="steelblue",
        linewidth=0.5,
        mopad_basis="XYZ",
    )

    ax.set_aspect("equal")
    ax.add_collection(beachball)
    ax.set_xlim([0, 2])
    ax.set_ylim([0, 2])
    ax.axis("off")
    ax.set_title(amp_file.split(".")[0])
    A_matrix = np.squeeze(np.array(A_matrix))
    f2, a2 = plt.subplots()
    a2.set_aspect("equal")
    modelled_amps = []
    stations = np.unique([s.split(".")[0] for s in tags])
    for i_station, station in enumerate(stations):
        i_p = 3 * i_station
        i_v = 3 * i_station + 1
        i_h = 3 * i_station + 2
        modelled_amps.append(A_matrix[i_p] @ mt_matrix_to_vector(mt_dc).reshape(6, 1))
        azimuth = np.arctan2(
            picks["n"] - picks[station]["n"], picks["e"] - picks[station]["e"]
        )
        v_modelled_amp = A_matrix[i_v] @ mt_matrix_to_vector(mt_dc).reshape(6, 1)
        h_modelled_amp = A_matrix[i_h] @ mt_matrix_to_vector(mt_dc).reshape(6, 1)

        modelled_amps.append(  # east comp
            v_modelled_amp * np.sin(azimuth) - h_modelled_amp * np.cos(azimuth)
        )
        modelled_amps.append(  # north comp
            v_modelled_amp * np.cos(azimuth) + h_modelled_amp * np.sin(azimuth)
        )
    # a2.plot(
    #     amps[::3], modelled_amps[::3], "o", c="firebrick", markeredgecolor="k"
    # )
    # a2.plot(
    #     amps[1::3], modelled_amps[1::3], "o", c="seagreen", markeredgecolor="k"
    # )
    # a2.plot(
    #     amps[2::3],
    #     modelled_amps[2::3],
    #     "o",
    #     c="mediumorchid",
    #     markeredgecolor="k",
    # )
    # line, = a2.plot(amps, modelled_amps, "o", alpha=0)
    #
    # for i_point, tag in enumerate(tags):
    #     annot = a2.annotate(
    #         "",
    #         xy=(amps[i_point], modelled_amps[i_point]),
    #         xytext=(40, 40),
    #         textcoords="offset pixels",
    #         bbox=dict(boxstyle="round", fc="w"),
    #         arrowprops=dict(arrowstyle="->"),
    #     )
    #     annot.set_visible(False)
    #
    # def update_annot(ind):
    #     x, y = line.get_data()
    #     annot.xy = (x[ind["ind"][0]], y[ind["ind"][0]])
    #     text = "{}".format(" ".join([tags[n] for n in ind["ind"]]))
    #     annot.set_text(text)
    #     annot.get_bbox_patch().set_facecolor("0.95")
    #
    # def hover(event):
    #     vis = annot.get_visible()
    #     if event.inaxes == a2:
    #         cont, ind = line.contains(event)
    #         if cont:
    #             update_annot(ind)
    #             annot.set_visible(True)
    #             f2.canvas.draw_idle()
    #         else:
    #             if vis:
    #                 annot.set_visible(False)
    #                 f2.canvas.draw_idle()

    # x1, x2 = a2.get_xlim()
    # y1, y2 = a2.get_ylim()
    # ax_max = max([-x1, x2, -y1, y2])
    # a2.plot([0, 0], [-ax_max, ax_max], ":", color="0.5", zorder=-1)
    # a2.plot([-ax_max, ax_max], [0, 0], ":", color="0.5", zorder=-1)
    # a2.plot([-ax_max, ax_max], [-ax_max, ax_max], "-.", color="0.5", zorder=-1)
    # a2.legend(["P", "SE", "SN"], numpoints=1)
    # a2.set_xlim([-ax_max, ax_max])
    # a2.set_ylim([-ax_max, ax_max])
    plot_regression(
        right_hand_side, mt["dc"], np.array(A_matrix), a2, f2, nslc_tags=tags
    )
    # f2.canvas.mpl_connect("motion_notify_event", hover)
    fig.savefig(dr + "beachball_" + inversion_name + ".png")
    f2.savefig(dr + "ENZ_regression_" + inversion_name + ".png")
    f_map.savefig(dr + "map_" + inversion_name + ".png")
    print(inversion_name + " " + str(np.sqrt(mt["r_dc"])))
plt.show()
