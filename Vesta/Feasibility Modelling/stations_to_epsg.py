import matplotlib

matplotlib.use("Qt5agg")

import glob
import numpy
import matplotlib.pyplot as plt
from pyproj import Transformer

station_files = glob.glob("Artis_rough_stations_??.csv")
transformer = Transformer.from_crs("epsg:4326", "epsg:3401")
elevation = 900

for station_file in station_files:
    out_file = station_file.replace("stations_", "stations_epsg3401_")
    f = open(station_file)
    g = open(out_file, "w")
    head = f.readline()
    g.write("station,northing (m), easting (m), elevation (m)\n")
    for line in f.readlines():
        lspl = line.split(",")
        station = lspl[0]
        longitude, latitude = [float(s) for s in lspl[1:]]
        easting, northing = transformer.transform(latitude, longitude)
        g.write(station + (",%.1f,%.1f,%.1f\n" % (northing, easting, elevation)))
    f.close()
    g.close()
