import matplotlib

matplotlib.use("Qt5agg")
import glob
import matplotlib.pyplot as plt
import numpy as np

east_source, north_source = 0, 0

fig, ax = plt.subplots(2, 2, figsize=[16, 14], sharex=True, sharey=True)

station_files = glob.glob("Vesta_random_grid_??.csv")
for i_file, station_file in enumerate(station_files):
    i_row = i_file // 2
    i_col = i_file % 2
    f = open(station_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    n_lines = len(lines)
    east, north = np.zeros(n_lines), np.zeros(n_lines)
    for i_line, line in enumerate(lines):
        lspl = line.split(",")
        north[i_line], east[i_line] = [float(s) for s in lspl[1:3]]
    a = ax[i_row, i_col]
    a.set_aspect("equal")
    a.plot(east, north, "v")
    a.plot(east_source, north_source, "*")
    a.set_title(station_file)

ax[1, 1].axis("off")
