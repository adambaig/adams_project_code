import matplotlib

matplotlib.use("Qt5agg")
import glob
import matplotlib.pyplot as plt
import numpy as np

from sms_ray_modelling.raytrace import isotropic_ray_trace

east_source, north_source = 108833.52, 5761839.87
source = {"e": 108833.52, "n": 5761839.87, "z": -1350}
fig, ax = plt.subplots()

station_file = "Artis_random_grid_20.csv"
f = open(station_file)
head = f.readline()
lines = f.readlines()
f.close()
n_lines = len(lines)
east, north = np.zeros(n_lines), np.zeros(n_lines)
stations = {}
tt = []
velocity_model = [{"vp": 4000, "vs": 2200, "rho": 2300}]
for i_line, line in enumerate(lines):
    lspl = line.split(",")
    id = lspl[0]
    stations[id] = {"n": float(lspl[1]), "e": float(lspl[2]), "z": -900}
    p_raypath = isotropic_ray_trace(source, stations[id], velocity_model, "P")
    tt.append(p_raypath["traveltime"])

ax.plot(tt, "_")
ax.set_ylim([2.6, 0])
