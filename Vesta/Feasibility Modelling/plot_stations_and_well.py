import matplotlib

matplotlib.use("Qt5agg")
import glob
import matplotlib.pyplot as plt
import numpy as np

from pfi_fs.configuration import geology

center_east, center_north = 0, 0
M2F = 1
PI = np.pi
angle = PI * (180 - 127.1) / 180
wells = {}
for well in glob.glob("fake_well*.csv"):
    f = open(well)
    head = f.readline()
    lines = f.readlines()
    f.close()
    wells[well.split(".")[0][-1]] = {
        "easting": np.array([float(line.split(",")[0]) for line in lines])
        - center_east,
        "northing": np.array([float(line.split(",")[1]) for line in lines])
        - center_north,
    }

fig, ax = plt.subplots(1, 4, figsize=[16, 7], sharex=True, sharey=True)
for axis in [ax[0], ax[1], ax[2], ax[3]]:
    for tag, well in wells.items():
        axis.plot(
            np.array(well["easting"]),
            np.array(well["northing"]),
            c="0.8",
            lw=4,
            zorder=3,
            alpha=0.5,
        )
        axis.plot(
            np.array(well["easting"]),
            np.array(well["northing"]),
            c="0.2",
            lw=2,
            zorder=4,
            alpha=0.5,
        )
station_files = glob.glob("Vesta_random_grid_*.csv")
station_files.insert(4, station_files[0])
station_files.pop(0)
for i_file, station_file in enumerate(station_files):
    f = open(station_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    n_lines = len(lines)
    east, north = np.zeros(n_lines), np.zeros(n_lines)
    for i_line, line in enumerate(lines):
        lspl = line.split(",")
        north[i_line], east[i_line] = [float(s) for s in lspl[1:3]]
    a = ax[i_file]
    a.set_aspect("equal")
    a.plot(east - center_east, north - center_north, "v", zorder=6)
    a.set_xlabel("easting (m)")

    a.text(-10500, 12000, "ABCD"[i_file], size="large")

ax[0].set_ylabel("northing (m)")
plt.show()
