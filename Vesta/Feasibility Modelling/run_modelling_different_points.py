from pfi_fs.simulate_events import generate_1Csuperstation_waveforms

dx, dy = 800, 1400  # x--> easting  y--> northing

f = open("source_0e0n.cfg")

line = f.readline()
lines = f.readlines()
f.close()
data, comment = line.split("#", 1)
east, north, elevation = [float(s) for s in data.split(",")]

positions = [
    "0e3n",
    "1e3n",
    "2e3n",
    "1e0n",
    "0e1n",
    "1e1n",
    "2e0n",
    "0e2n",
    "2e2n",
    "1e2n",
    "2e1n",
    "0e0n",
]

# positions["1e0n", "0e1n", "1e1n", "2e0n", "0e2n", "2e2n", "1e2n","2e1n","0e0n"]:

for position in positions:
    nx, ny = int(position[0]), int(position[2])
    outfile = "source_" + position + ".cfg"
    g = open(outfile, "w")
    g.write("%.2f,%.2f,%.2f #" % (east + nx * dx, north + ny * dy, elevation))
    g.write(comment)
    for line in lines:
        g.write(line)
    g.close()


for directory_name in [
    #    "5_stations_7",
    # "15_stations_13",
    # "30_stations_13",
    # "60_stations_19",
    "120_stations_37"
]:
    nsuper, _, nstations = directory_name.split("_")
    sim_config = "simulation" + nstations + ".cfg"
    for position in positions:
        source_config = "source_" + position + ".cfg"

        station_file = "Vesta_random_grid_" + nsuper + ".csv"
        generate_1Csuperstation_waveforms(
            source_config,
            sim_config,
            "geology.cfg",
            station_file,
            directory_name=directory_name + "_" + position,
        )
