import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np

dh_file = "2019HOU0030_Artis_3_18_ALLSTAGES.csv"
f = open(dh_file)
head = f.readline()
unit = f.readline()
events = {}
for line in f.readlines():
    lspl = line.split(",")
    events[lspl[0]] = {
        "easting": float(lspl[2]),
        "northing": float(lspl[3]),
        "elevation": float(lspl[4]),
        "distance": float(lspl[5]),
        "moment": float(lspl[6]),
        "moment magnitude": float(lspl[7]),
    }

mw = np.array([v["moment magnitude"] for k, v in events.items()])
dist = np.array([v["distance"] for k, v in events.items()])

fig, ax = plt.subplots()
ax.plot(dist, mw, ".", c="indigo")
ax.set_xlabel("listening distance (m)")
ax.set_ylabel("moment magnitude")
ax.set_title("Magnitude-Distance Plot from Downhole Catalog")
plt.show()
