from pfi_fs.simulate_events import generate_1Csuperstation_waveforms

for directory_name in [
    # "15_stations_13_center",
    # "30_stations_13_center",
    # "60_stations_19_center",
    "120_stations_37_center"
]:
    nsuper, _, nstations, position = directory_name.split("_")
    sim_config = "simulation" + nstations + ".cfg"
    if position == "center":
        source_config = "source.cfg"
    elif position == "toe":
        source_config = "toe_source.cfg"
    station_file = "Vesta_random_grid_" + nsuper + ".csv"
    generate_1Csuperstation_waveforms(
        source_config,
        sim_config,
        "geology.cfg",
        station_file,
        directory_name=directory_name,
    )
