import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory, UTCDateTime

waveforms = glob.glob(
    "C:\\Users\\adambaig\\Project\\Artis 9-18\\2_WaveformProcessing\\mSeeds_processed\\*"
)
waveforms = glob.glob(
    "C:\\Users\\adambaig\\Project\\Artis 9-18\\1_DataFetch\\mSeeds\\?????.MSEED"
)

inv = read_inventory(
    "C:\\Users\\adambaig\\Project\\Artis 9-18\\1_DataFetch\\Instrument_Response\\ART_Full.xml"
)
st = read(waveforms[0])


def read_pick_table():
    f = open(r"2_WaveformProcessing\PSPicksFull.Table.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    picks = {}
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        if event_id not in picks:
            picks[event_id] = {}
            picks[event_id]["t0"] = UTCDateTime(float(lspl[8]))
        station = lspl[7] + "." + lspl[12]
        if station not in picks:
            print(picks[event_id])
            picks[event_id][station] = {}
        if lspl[10] == "S":
            picks[event_id][station]["S"] = float(lspl[11])
        if lspl[10] == "P":
            picks[event_id][station]["P"] = float(lspl[11])
    return picks


# picks = read_pick_table()


def rms(x):
    return np.sqrt(np.mean(x**2))


waveform = waveforms[0]
vert_noise = {}
for waveform in waveforms:
    st = read(waveform).remove_response(inventory=inv)
    for trace in st.select(channel="HHZ"):
        if trace.stats.station not in vert_noise:
            vert_noise[trace.stats.station] = []
        filt_data = trace.filter("bandpass", freqmin=20, freqmax=50).data
        vert_noise[trace.stats.station].append(rms(filt_data[1000:2000]))


plt.plot(filt_data)


for station, noise in vert_noise.items():
    print(station, np.median(noise))
