<<<<<<< HEAD
import matplotlib

matplotlib.use('Qt5agg')

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import pearsonr

f = open('Digitized_regression_23080.csv')

lines = f.readlines()
f.close()
nl = len(lines)
dx,y = np.zeros(nl),np.zeros(nl)

for i_line,line in enumerate(lines):
    dx[i_line],y[i_line] = [float(s) for s in line.split(',')]

x = 7*(dx+40000)/8 - 40000 # I goofed entering the wrong value in the online digitizer

fig,ax = plt.subplots()
ax.set_aspect('equal')
ax.plot(x,y,'o')
x1,x2 = ax.get_xlim()
y1,y2 = ax.get_ylim()
ax_max = max([-x1,x2,-y1,y2])
ax.plot([-ax_max,ax_max],[-ax_max,ax_max],'r:',zorder=-2)
ax.set_xlabel('digitized x axis')
ax.set_ylabel('digitized y axis')

r, p = pearsonr(x,y)

r2 = np.corrcoef(x,y)[0,1]**2

ax.set_title('R = %.3f, R$^2$ = %.3f' % (r, r2)  )

f= open("regmti_amps//ve_23080_2_grp2_1.csv")
lines = f.readlines()
f.close()
picks=[]
for line in lines:
    lspl = line.split(",")
    picks.append(float(lspl[2]))


picks
picks.sort()
x.sort()
f2,a2 = plt.subplots()
a2.set_aspect('equal')
a2.plot(picks, picks-x)
x1,x2= a2.get_xlim()
a2.set_ylim(x1,x2)
a2.set_xlabel('sorted database pick amplitude')
a2.set_ylabel('difference of sorted amplitudes')
plt.show()
=======
import numpy as np
import matplotlib.pyplot as plt
>>>>>>> 31c45103857afc8408f8544c0b6b9c8098edb3ce
