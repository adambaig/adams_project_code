import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
from obspy import read, UTCDateTime, read_inventory
from obspy.signal import filter
import utm
from read_inputs import read_channels, read_velocity_model, read_pick_table, read_events
from scipy import integrate
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_moment_tensor.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)
import mplstereonet as mpls

seedfile = "2_WaveformProcessing//mSeeds_processed//19932_Processed.MSEED"
D2R = np.pi / 180.0
lf, hf = 0.001, 100
inst_xml_file = "1_DataFetch//Instrument_Response//VE_Full.xml"
inv = read_inventory(inst_xml_file)
st = read(seedfile)

events = read_events()
events["19932"]["t0"]


def sqa(x):
    return np.squeeze(np.array(x))


for network in inv.networks:
    for station in network.stations:
        print(station.code + " " + str(len(station.channels)))

st.select(network=network.code)

velocity_model = read_velocity_model("inputs//vesta_simple.txt")
inv = read_inventory(inst_xml_file)
t0 = st[0].stats.starttime
spectral_level = {}
spectral_level["P"] = {}
spectral_level["SV"] = {}
spectral_level["SH"] = {}
delta = st[0].stats.delta
picks = read_pick_table()
stations = read_channels()
picks["19932"]
for line in lines:
    lspl = line.split()
    pol_station = [k for k, v in stations.items() if lspl[0] in k][0]
    if lspl[-1][-1] == "C":
        polarities[pol_station] = 1
    elif lspl[-1][-1] == "D":
        polarities[pol_station] = -1

for pick in {k: v for k, v in picks.items() if "." in k}:
    net, name = pick.split(".")[:2]
    st_3c = st.select(station=name).copy()
    station = stations[net + "." + name]
    back_azimuth = np.arctan2(
        station["easting"] - event_easting,
        station["northing"] - event_northing,
    )

    st_3c.remove_response(inventory=inv)
    try:
        st_3c.rotate("->ZNE", inventory=inv)
        rotated = True
    except:
        print("pick can't be rotated")
        rotated = False
    if rotated:
        # if pick == 'TX.PB01.00.CPZ':
        if "P" in picks[pick]:
            df = st_3c[0].stats.sampling_rate
            st_3c.rotate("NE->RT", back_azimuth=np.mod(back_azimuth / D2R, 360))
            z_comp = st_3c[0].data
            r_comp = st_3c[1].data
            t_comp = st_3c[2].data
            times = st_3c[0].times(reftime=t0)
            filt_z_comp = filter.bandpass(z_comp, lf, hf, df)
            filt_r_comp = filter.bandpass(r_comp, lf, hf, df)
            filt_t_comp = integrate.cumtrapz(
                filter.bandpass(t_comp, lf, hf, df), times, initial=0
            )

            p_pick = UTCDateTime(picks[pick]["P"]) - t0

            fig = plt.figure(figsize=[12, 8])
            ax_t = fig.add_axes([0.1, 0.75, 0.8, 0.2])
            ax_s = fig.add_axes([0.1, 0.08, 0.8, 0.6])
            ax_t.plot([p_pick, p_pick], [-1.1, 1.1], "k")
            i_p = np.where(times > p_pick)[0][0]
            if "S" in picks[pick]:
                s_pick = UTCDateTime(picks[pick]["S"]) - t0
                ax_t.plot([s_pick, s_pick], [-1.1, 1.1], "k")
                i_s = np.where(times > s_pick)[0][0]
            else:
                i_s = np.where(times > p_pick + 5)[0][0]
            i_s = np.where(times > p_pick + 0.5)[0][0]

            demean_z = filt_z_comp[i_p:i_s] - np.average(filt_z_comp[i_p:i_s])
            demean_r = filt_r_comp[i_p:i_s] - np.average(filt_r_comp[i_p:i_s])
            zr_data = np.matrix(np.vstack([demean_z, demean_r]))
            eig_vals, eig_vecs = np.linalg.eig(zr_data * zr_data.T)
            i_sort = np.argsort(eig_vals)
            p_vec = sqa(eig_vecs[:, i_sort[1]])
            inclination = np.arctan(p_vec[0] / p_vec[1])
            zr_full = np.matrix(np.vstack([z_comp, r_comp]))
            pv_full = eig_vecs[:, i_sort[::-1]].T * zr_full
            p_comp, v_comp = [sqa(_) for _ in pv_full]
            filt_p_comp = integrate.cumtrapz(
                filter.bandpass(p_comp, lf, hf, df), times, initial=0
            )
            filt_v_comp = integrate.cumtrapz(
                filter.bandpass(v_comp, lf, hf, df), times, initial=0
            )
            max_trace = max(
                [
                    max(abs(filt_p_comp)),
                    max(abs(filt_v_comp)),
                    max(abs(filt_t_comp)),
                ]
            )
            ax_t.plot(
                times,
                0.2 + filt_p_comp / max_trace,
                color="firebrick",
                zorder=1,
                lw=2,
            )
            ax_t.plot(
                times,
                filt_v_comp / max_trace,
                color="forestgreen",
                alpha=0.3,
                zorder=0,
            )
            ax_t.plot(
                times,
                -0.2 + filt_t_comp / max_trace,
                color="royalblue",
                alpha=0.3,
                zorder=0,
            )
            ax_t.set_yticks([])
            ax_t.set_xlabel("relative time (s)")
            ax_t.set_title(name)
            # hodogram plot
            #
            # ax_s.set_aspect("equal")
            # ax_s.plot(filt_r_comp[i_p:i_s], filt_z_comp[i_p:i_s])
            # x1, x2 = ax_s.get_xlim()
            # y1, y2 = ax_s.get_ylim()
            # xx = max([-x1, x2, -y1, y2])
            # ax_s.set_xlim([x1,x2])
            # ax_s.set_ylim([y1,y2])
            # ax_s.plot(
            #      [-np.cos(inclination) * xx, np.cos(inclination)*xx],
            #      [-np.sin(inclination)*xx, np.sin(inclination)*xx],
            #      'r'
            #  )

            i_f_win = np.where(times > p_pick + 5)[0][0]
            nfreq = i_f_win - i_p
            i_n_win1 = int(i_p - 6.0 / delta)
            i_n_win2 = int(i_p - 1.0 / delta)
            freq = np.fft.fftfreq(nfreq, delta)
            ax_s.loglog(
                freq[1 : nfreq // 2],
                abs(np.fft.fft(p_comp[i_p:i_f_win])[1 : nfreq // 2])
                / 2
                / np.pi
                / freq[1 : nfreq // 2],
                "firebrick",
            )
            ax_s.loglog(
                freq[1 : nfreq // 2],
                abs(np.fft.fft(p_comp[i_n_win1:i_n_win2])[1 : nfreq // 2])
                / 2
                / np.pi
                / freq[1 : nfreq // 2],
                "0.5",
            )
            ax_s.set_xlabel("frequency (Hz)")
            ax_s.set_ylabel("displacement spectrum (m$\cdot$s)")


st[0].stats


plt.show()
