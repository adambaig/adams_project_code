import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
from numpy import sin, cos, arccos, arcsin, arctan, arctan2
import matplotlib.pyplot as plt
import mplstereonet as mpls

from stress_inversions import (
    Michael_stress_inversion,
    stereonet_plot_stress_tensor,
    unit_plane_normal,
    trend_plunge_2_unit_vector,
)

PI = np.pi

s1 = {"trend": -127.35258200187074, "plunge": 2.88006730625949}
s2 = {"trend": -8.582941126236484, "plunge": 84.0325592128621}
s3 = {"trend": 142.3839728950899, "plunge": 5.222010465534509}
R = 0.6878611674006717


def sqa(x):
    return np.squeeze(np.array(x))


s1_axis = np.matrix(trend_plunge_2_unit_vector(s1["trend"], s1["plunge"])).T
s2_axis = np.matrix(trend_plunge_2_unit_vector(s2["trend"], s2["plunge"])).T
s3_axis = np.matrix(trend_plunge_2_unit_vector(s3["trend"], s3["plunge"])).T
stress_axes = np.matrix(np.hstack([s1_axis, s2_axis, s3_axis]))

strike1, dip1, rake1 = 302.4, 82.9, 67.9
strike2, dip2, rake2 = 195.5, 23.2, 161.7
friction_angle = 0.6

normal1 = unit_plane_normal(strike1, dip1)
normal2 = unit_plane_normal(strike2, dip2)

tau, sigma = np.zeros(2), np.zeros(2)
instability = np.zeros(2)
for jj, n in enumerate([normal1, normal2]):
    n1, n2, n3 = sqa(n * stress_axes)
    sign_tau = np.sign(n3)
    sigma[jj] = n1 * n1 + (1.0 - 2.0 * R) * n2 * n2 - n3 * n3
    tau[jj] = sign_tau * np.sqrt(
        n1 * n1 + (1 - 2 * R) ** 2 * n2 * n2 + n3 * n3 - sigma[jj] ** 2
    )
    instability[jj] = (abs(tau[jj]) - friction_angle * (sigma[jj] - 1.0)) / (
        friction_angle + np.sqrt(1.0 + friction_angle * friction_angle)
    )


f4, a4 = plt.subplots()
a4.set_aspect("equal")
circle_x, circle_y = [], []
for i_rad in np.arange(0, 2 * PI + PI / 100, PI / 100):
    circle_x.append(np.sin(i_rad))
    circle_y.append(np.cos(i_rad))
circle_x = np.array(circle_x)
circle_y = np.array(circle_y)

a4.plot(circle_x, circle_y, "0.3")
a4.plot(R * circle_x + 1 - R, R * circle_y, "0.3")
a4.plot((1 - R) * circle_x - R, (1 - R) * circle_y, "0.3")
a4.plot(sigma[0], tau[0], "o", color="forestgreen", markeredgecolor="k")
a4.plot(sigma[1], tau[1], "o", color="royalblue", markeredgecolor="k")
a4.arrow(-1.1, 0, 2.2, 0, head_width=0.06, head_length=0.1, fc="k", ec="k")
a4.arrow(-1.1, 0, 0, 1.1, head_width=0.06, head_length=0.1, fc="k", ec="k")
a4.arrow(-1.1, 0, 0, -1.1, head_width=0.06, head_length=0.1, fc="k", ec="k")
a4.text(-1.1, 1.22, "$\\tau$", ha="center")
a4.text(-1.1, -1.21, "$-\\tau$", va="top", ha="center")
a4.text(1.21, 0.0, "${\\sigma}$", va="center")
a4.text(-0.98, -0.1, "$\\sigma_3$")
a4.text(-0.98, -0.1, "$\\sigma_3$")
a4.text(1 - 2 * R + 0.03, -0.1, "$\\sigma_2$")
a4.text(1.02, -0.1, "$\\sigma_1$")
a4.axis("off")
a4.set_xlim(-1.3, 1.3)
a4.set_ylim(-1.3, 1.3)


plt.show()
