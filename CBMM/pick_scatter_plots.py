import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory
import pyproj as pr

from NocMeta.Meta import NOC_META
from generalPlots import gray_background_with_grid

athena_code = "CBMM"
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META[athena_code]['epsg']}")
inv = read_inventory("CBMM_Full.xml")

stations = {}
for network in inv:
    for station in network:
        east, north = pr.transform(
            latlon_proj, out_proj, station.longitude, station.latitude
        )
        stations[network.code + "." + station.code + "."] = {
            "e": east,
            "n": north,
            "z": station.elevation,
        }


with open("0000002371_20201003.145749.680000.picks") as f:
    pick_lines = f.readlines()

p_picks, s_picks = {}, {}
for line in pick_lines:
    station, phase, pick_string = line.split(",")
    if phase == "P":
        p_picks[station] = float(pick_string)
    if phase == "S":
        s_picks[station] = float(pick_string)


fig, (ax_p, ax_s) = plt.subplots(1, 2, figsize=[16, 8])

min_pick = min([pick for pick in p_picks.values()])
max_diff = max([pick for pick in s_picks.values()]) - min_pick


p_pick_map = np.array(
    [(v["e"], v["n"], p_picks[k] - min_pick) for k, v in stations.items()]
)
s_pick_map = np.array(
    [(v["e"], v["n"], s_picks[k] - min_pick) for k, v in stations.items()]
)

p_scatter = ax_p.scatter(
    p_pick_map[:, 0],
    p_pick_map[:, 1],
    c=p_pick_map[:, 2],
    vmin=0,
    vmax=max_diff,
    cmap="nipy_spectral",
    edgecolor="k",
)
s_scatter = ax_s.scatter(
    s_pick_map[:, 0],
    s_pick_map[:, 1],
    c=s_pick_map[:, 2],
    vmin=0,
    vmax=max_diff,
    cmap="nipy_spectral",
    edgecolor="k",
)

for ax in ax_p, ax_s:
    ax.set_aspect("equal")
    gray_background_with_grid(ax)
    for station_name, location in stations.items():
        station_only = station_name.split(".")[1]
        ax.text(location["e"], location["n"] - 200, station_only, ha="center")

fig.subplots_adjust(bottom=0.2)
cbar_ax = fig.add_axes([0.2, 0.15, 0.6, 0.03])
cbar = fig.colorbar(p_scatter, cax=cbar_ax, orientation="horizontal")

ax_p.set_title("relative P wave times")
ax_s.set_title("relative S wave times")
cbar.set_label("time after first P pick (s)")

fig.savefig("pick_map.png", bbox_inches="tight")
