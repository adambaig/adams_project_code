from datetime import datetime
from pathlib import Path

import numpy as np

from nmxseis.numerics.moment_tensor import MomentTensor, SDR


def read_catalog(filename):
    with open(filename) as f:
        head = f.readline()
        lines = f.readlines()
    events = {}
    for line in lines:
        split_line = line.split(",")
        event_id = split_line[0]
        events[event_id] = {
            "utc datetime": datetime.strptime(split_line[1], "%Y-%m-%d %H:%M:%S.%f"),
            "timestamp": float(split_line[2]),
            "enu": np.array(
                [float(split_line[3]), float(split_line[4]), -float(split_line[5])]
            ),
            "snr": float(split_line[6]),
            "mw": float(split_line[7]),
            "raw amp": float(split_line[8]),
            "reloc amp": float(split_line[9]),
            "fc": float(split_line[10]),
            "q": float(split_line[11]),
            "general MT": MomentTensor.from_vector(
                np.array([float(v) for v in split_line[12:18]])
            ),
            "gen r": float(split_line[18]),
            "gen CN": float(split_line[19]),
            "dc MT": MomentTensor.from_vector(
                np.array([float(v) for v in split_line[20:26]])
            ),
            "dc r": float(split_line[26]),
            "dc CN": float(split_line[27]),
            "confidence": float(split_line[28]),
            "log raw amp": float(split_line[29]),
        }
    return events


def read_final_catalog(
    filename=Path("catalogs").joinpath(
        "VistaPAD48_final_catalog_toDeliver_withStages_withSW_1000mOffZone_perfTagged.csv"
    ),
):
    with open(filename) as f:
        head = f.readline()
        lines = f.readlines()
    events = {}
    for line in lines:
        split_line = line.split(",")
        event_id = split_line[0]
        events[event_id] = {
            "utc datetime": datetime.strptime(split_line[1], "%Y-%m-%d %H:%M:%S.%f"),
            "timestamp": float(split_line[2]),
            "enu": np.array(
                [float(split_line[3]), float(split_line[4]), -float(split_line[5])]
            ),
            "log_stack_snr": float(split_line[6]),
            "enu_uncertainties": np.array(
                [float(split_line[7]), float(split_line[8]), float(split_line[9])]
            ),
            "mw": float(split_line[10]),
            "moment": float(split_line[11]),
            "well": split_line[12],
            "stage": split_line[13],
            "stage_id": int(split_line[14]),
            "hrz_distance_from_stage": float(split_line[15]),
            "total_distance_from_stage": float(split_line[16]),
            "elapsed_time": float(split_line[17]),
            "general MT": MomentTensor.from_vector(
                np.array([float(v) for v in split_line[18:24]])
            ),
            "gen r": float(split_line[24]),
            "gen CN": float(split_line[25]),
            "clvd": float(split_line[26]),
            "iso": float(split_line[27]),
            "dc": float(split_line[28]),
            "dc MT": MomentTensor.from_vector(
                np.array([float(v) for v in split_line[29:35]])
            ),
            "dc r": float(split_line[35]),
            "dc CN": float(split_line[36]),
            "confidence": float(split_line[37]),
            "sdr": SDR(
                strike=float(split_line[38]),
                dip=float(split_line[39]),
                rake=float(split_line[40]),
            ),
            "aux_sdr": SDR(
                strike=float(split_line[41]),
                dip=float(split_line[42]),
                rake=float(split_line[43]),
            ),
            "event_type": split_line[50],
            "correct_time": split_line[51] == "TRUE",
        }
    return events
