import matplotlib.pyplot as plt
import numpy as np

from nmxseis.numerics.moment_tensor import SDR


def plot_one_sided_sdr_rosettes(
    sdrs, bin_degrees=5, color="steelblue", orientation="horizontal"
):
    """
    Function for plotting strike dip rake rosettes as three subfigures on one
    plot from a vectors of strikes, dips, and rakes.  Does not enforce plotting
    of the auxilliary plane from an MT -- see plot_strike_dip_rake_rosettes_from_MT
    for that functionality.
    Inputs
        strikes - a vector of strikes in degrees
        dips - a vector of dips in degrees
        rakes - a vector of rakes in degrees
        bin_degrees - the number of degrees to bin by on the rosettes
        color - any color that python (or perhaps just matplotlib) can understand
    Outputs
        fig - a matplotlib figure instance
    """
    strikes = [sdr.strike for sdr in sdrs]
    strikes_180 = np.array(strikes) % 180
    strikes_360 = strikes_180 + 180
    mirrored_strikes = np.hstack([strikes_180, strikes_360])
    if orientation == "horizontal":
        fig = plt.figure(figsize=[16, 5])
        ax_strike = fig.add_axes([0.05, 0.1, 0.38, 0.8], projection="polar")
        ax_dip = fig.add_axes([0.44, 0.1, 0.12, 0.8], projection="polar")
        ax_rake = fig.add_axes([0.57, 0.1, 0.38, 0.8], projection="polar")
    elif orientation == "vertical":
        fig = plt.figure(figsize=[5, 16])
        ax_strike = fig.add_axes([0.1, 0.57, 0.8, 0.38], projection="polar")
        ax_dip = fig.add_axes([0.1, 0.44, 0.8, 0.12], projection="polar")
        ax_rake = fig.add_axes([0.1, 0.05, 0.8, 0.38], projection="polar")
    strike_bins = np.arange(0, 360.0001, bin_degrees)
    n_strike, _ = np.histogram(mirrored_strikes, strike_bins)
    ax_strike.bar(
        np.deg2rad(strike_bins[:-1] + bin_degrees / 2),
        n_strike / 2.0,
        width=np.deg2rad(bin_degrees),
        color=color,
    )
    ax_strike.set_theta_zero_location("N")
    ax_strike.set_theta_direction(-1)
    ax_strike.set_thetagrids(
        np.arange(0, 360, 45), ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
    )

    dip_bins = np.arange(0, 90.0001, bin_degrees)
    n_dip, _ = np.histogram([sdr.dip for sdr in sdrs], dip_bins)
    ax_dip.bar(
        np.deg2rad(dip_bins[:-1] + bin_degrees / 2),
        n_dip,
        width=np.deg2rad(bin_degrees),
        color=color,
    )
    ax_dip.set_theta_direction(-1)
    ax_dip.set_thetamax(90)

    rake_bins = np.arange(-180, 180.0001, bin_degrees)
    n_rake, _ = np.histogram([sdr.rake for sdr in sdrs], rake_bins)
    ax_rake.bar(
        np.deg2rad(rake_bins[:-1] + bin_degrees / 2),
        n_rake,
        width=np.deg2rad(bin_degrees),
        color=color,
    )
    ax_rake.set_thetagrids(np.arange(0, 360, 90), ["LL", "TH", "RL", "NR"])
    return fig
