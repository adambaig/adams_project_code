import os
import json
from datetime import datetime

from nmxseis.numerics.moment_tensor import MomentTensor

from read_inputs import read_catalog


from sklearn.cluster import AgglomerativeClustering
import numpy as np
import mplstereonet as mpls
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

catalog_csv = "relocCatalog_MwMT_merged_confidence_mwUpdated_noduplicate_T0.1_updateNewAmp_updateMw_fit.csv"

events = read_catalog(catalog_csv)


def norm_MT(mt):
    return mt / np.linalg.norm(mt)


def moment(event):
    return 10 ** (1.5 * event["magnitude"] + 9)


def scaled_mt(mt, moment_value):
    moment = 10 ** (1.5 * event["magnitude"] + 9)
    return np.sqrt(2) * mt.mat * moment / np.linalg.norm(event["dc MT"])


def make_simple_gradient_from_white_cmap(end_color):
    red, green, blue = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, 1.0, 1.0), (1, red, red)],
        "green": [(0.0, 1.0, 1.0), (1, green, green)],
        "blue": [(0.0, 1.0, 1.0), (1, blue, blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


CUTOFF_PERCENTILE = 95 / 100


good_mts = {k: v for k, v in events.items() if v["confidence"] > CUTOFF_PERCENTILE}


colors = [
    "firebrick",
    "forestgreen",
    "darkgoldenrod",
    "steelblue",
    "indigo",
    "darkorange",
    "turquoise",
    "black",
]

n_mt = len(good_mts)
all_normalized_mts = np.zeros([n_mt, 3, 3])
for i_mt, mt in enumerate(good_mts.values()):
    all_normalized_mts[i_mt, :, :] = mt["dc MT"].mat / np.linalg.norm(mt["dc MT"].mat)

dot_product_pairs = np.tensordot(
    all_normalized_mts, all_normalized_mts, axes=([1, 2], [1, 2])
)

distance_mat = abs(1 - abs(dot_product_pairs))

fig, ax = plt.subplots()
ax.set_aspect("equal")
ax.pcolor(distance_mat)
n_clusters = 6
clustering = AgglomerativeClustering(
    n_clusters=n_clusters, affinity="precomputed", linkage="complete"
).fit(distance_mat)
labels = clustering.labels_
for i_mt, (id, mt) in enumerate(good_mts.items()):
    mt["mt cluster"] = labels[i_mt]

flip = [1, 1, -1, 1, 1, 1, 1, 1]

for event in events.values():
    event["is flipped"] = False
for i_cluster in range(n_clusters):
    mt_cluster = {k: v for k, v in good_mts.items() if v["mt cluster"] == i_cluster}

    custom_cmap = make_simple_gradient_from_white_cmap(colors[i_cluster])
    mt_avg = flip[i_cluster] * sum(
        [v["dc MT"].mat / np.linalg.norm(v["dc MT"].mat) for k, v in mt_cluster.items()]
    )

    p_trend, b_trend, t_trend = [], [], []
    p_plunge, b_plunge, t_plunge = [], [], []

    for id, mt in mt_cluster.items():
        mt["old dc MT"] = mt["dc MT"]
        mt["old gen MT"] = mt["general MT"]
        if np.tensordot(mt_avg, mt["dc MT"].mat) < 0:
            mt["dc MT"] = MomentTensor.from_matrix(-mt["dc MT"].mat)
            mt["general MT"] = MomentTensor.from_matrix(-mt["general MT"].mat)
        decomp = mt["dc MT"].decompose()
        p_trend.append(decomp.p_trend)
        b_trend.append(decomp.b_trend)
        t_trend.append(decomp.t_trend)
        p_plunge.append(decomp.p_plunge)
        b_plunge.append(decomp.b_plunge)
        t_plunge.append(decomp.t_plunge)
    f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = a3[0].density_contourf(
        p_plunge, p_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_b = a3[1].density_contourf(
        b_plunge, b_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_t = a3[2].density_contourf(
        t_plunge, t_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    f3.savefig(f"agglomerative_clustering_{i_cluster}.png")


for event in good_mts.values():
    if np.tensordot(event["old dc MT"].mat, event["dc MT"].mat) < 0:
        event["is flipped"] = True


cluster_mts = [
    sum(
        mt["dc MT"].mat / np.linalg.norm(mt["dc MT"].mat)
        for mt in good_mts.values()
        if mt["mt cluster"] == ii
    )
    for ii in range(n_clusters)
]

low_q_frac = {k: v for k, v in events.items() if v["confidence"] < CUTOFF_PERCENTILE}
for event in low_q_frac.values():
    dot_products = np.zeros(n_clusters)
    for i_cluster in range(n_clusters):
        dot_products[i_cluster] = np.tensordot(
            norm_MT(event["dc MT"].mat), norm_MT(cluster_mts[i_cluster])
        )
    i_cluster_belongs = np.argmax(abs(dot_products))
    if dot_products[i_cluster_belongs] < 0:
        event["dc MT"] = MomentTensor.from_matrix(-event["dc MT"].mat)
        event["general MT"] = MomentTensor.from_matrix(-event["general MT"].mat)
        event["is flipped"] = True


out_events = {}
for event_id, event in events.items():
    out_event = {}
    for key in event:
        if key in ["general MT", "dc MT"]:
            out_event[key] = event[key].vec.tolist()
        elif key == "utc datetime":
            out_event[key] = datetime.strftime(
                event["utc datetime"], "%Y-%m-%d %H:%M:%S.%f"
            )
        elif key in ["enu", "mt cluster", "old dc MT", "old gen MT"]:
            pass
        else:
            out_event[key] = event[key]
        out_event["easting"] = event["enu"][0].tolist()
        out_event["northing"] = event["enu"][1].tolist()
        out_event["depth"] = -event["enu"][2].tolist()
    out_events[event_id] = out_event


with open("flipped_events.json", "w") as f:
    json.dump(out_events, f)
