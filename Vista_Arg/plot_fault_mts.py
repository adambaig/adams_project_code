import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm

tab20 = cm.get_cmap("tab20")

from plotting import plot_one_sided_sdr_rosettes
from read_inputs import read_final_catalog

events = read_final_catalog()

ne_normal = np.array([-np.sqrt(0.5), np.sqrt(0.5), 0])

fault_color = {
    "North Fault Event": tab20(1),
    "NE Fault Event": tab20(3),
    "East Fault Event": tab20(5),
    "NW Fault Event": tab20(7),
    "Upper Fault Event": tab20(8),
    "SW Fault Event": tab20(9),
    # 'Frac Event': tab20(12),
    # 'Perf': tab20(10),
    # 'Off Zone': tab20(14),
}

fig, ax = plt.subplots(figsize=[12, 12])

for group, color in fault_color.items():
    fault_events = {
        k: v
        for k, v in events.items()
        if v["event_type"] == group and v["confidence"] > 0.95
    }

    actual_sdrs = []
    for event in fault_events.values():
        fp1_check = abs(np.dot(ne_normal, event["sdr"].to_normal()))
        fp2_check = abs(np.dot(ne_normal, event["aux_sdr"].to_normal()))
        if fp1_check > fp2_check:
            actual_sdrs.append(event["sdr"])
        else:
            actual_sdrs.append(event["aux_sdr"])
        ax.plot(*event["enu"][:2], ".", color=color)
    fig_rose = plot_one_sided_sdr_rosettes(actual_sdrs, color=color)
    fig_rose.savefig(f'rosette_{group.replace(" ", "_").lower()}.png')

plt.show()
