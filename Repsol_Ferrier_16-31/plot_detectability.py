import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (
    MultipleLocator,
    FormatStrFormatter,
    AutoMinorLocator,
    LogLocator,
)

import glob


matplotlib.rcParams.update({"font.size": 18})


def sqa(x):
    return np.squeeze(np.array(x))


def keys_exists(element, *keys):
    """
    Check if *keys (nested) exists in `element` (dict).
    """
    if type(element) is not dict:
        raise AttributeError("keys_exists() expects dict as first argument.")
    if len(keys) == 0:
        raise AttributeError("keys_exists() expects at least two arguments, one given.")

    _element = element
    for key in keys:
        try:
            _element = _element[key]
        except KeyError:
            return False
    return True


directories = ["30_stations_13_center", "50_stations_7_center", "50_stations_13_center"]

# ncolor = {"7": "firebrick", "13": "royalblue", "19": "forestgreen"}

ncolor = {
    "30_stations_13_center": "firebrick",
    "50_stations_7_center": "royalblue",
    "50_stations_13_center": "forestgreen",
}

factor = {"7": 7.0 / 19.0, "13": 13.0 / 19.0, "19": 1}

marker = {"toe": "s", "center": "o"}

# marker_size = {"30": 5, "50": 9}

noise_sample = []
xMajorLocator = MultipleLocator(0.5)
xMinorLocator = MultipleLocator(0.1)
yMajorLocator = LogLocator(10)
yMinorLocator = LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9])
image_stats = {}
M_detect = {}
for dr in directories:
    nsuper, dum2, nstations, position = dr.split("_")
    if not (nsuper in image_stats):
        image_stats[nsuper] = {}
        M_detect[nsuper] = {}
    if not (position in image_stats[nsuper]):
        image_stats[nsuper][position] = {}
        M_detect[nsuper][position] = {}
    if not (nstations in image_stats[nsuper][position]):
        image_stats[nsuper][position][nstations] = []
        M_detect[nsuper][position][nstations] = {}
    f = open(dr + "//synthetic_catalog_relocated.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        true_loc = np.array([float(s) for s in lspl[2:5]])
        magnitude, stress_drop = [float(s) for s in lspl[5:7]]
        m11, m22, m33, m12, m13, m23 = [float(s) for s in lspl[7:13]]
        amplitude = float(lspl[13])
        image_loc = np.array([float(s) for s in lspl[14:17]])
        image_stats[nsuper][position][nstations].append(
            {
                "magnitude": magnitude,
                "amplitude": amplitude,
                "location shift": true_loc - image_loc,
            }
        )
        if magnitude < -1.4:
            noise_sample.append(factor[nstations] * amplitude)

threshold = np.average(noise_sample) + 3 * np.std(noise_sample)

f1, a1 = plt.subplots(figsize=[16, 10])
patches, labels = [], []
ii = -1
tags = ["A", "B", "C"]
for nsuper in ["30", "50"]:
    for position in ["toe", "center"]:
        for nstations in ["7", "13", "19"]:
            if keys_exists(image_stats, nsuper, position, nstations):
                mags = np.unique(
                    [v["magnitude"] for v in image_stats[nsuper][position][nstations]]
                )
                amp = []
                ii += 1
                for mag in mags:
                    amp.append(
                        np.average(
                            np.array(
                                [
                                    v["amplitude"]
                                    for v in image_stats[nsuper][position][nstations]
                                    if v["magnitude"] == mag
                                ]
                            )
                        )
                    )
                amp = np.array(amp)
                """
                ilarge = np.where(mag > -0.5)[0]
                large_amps = np.matrix(np.log10(factor[nstations] * amp[ilarge]))
                Amatrix = np.matrix(
                    np.vstack(
                        [mag[ilarge] * mag[ilarge], mag[ilarge], np.ones(len(ilarge))]
                    )
                ).T
                quad, slope, yint = [
                    sqa(s)
                    for s in np.linalg.inv(Amatrix.T * Amatrix)
                    * Amatrix.T
                    * large_amps.T
                ]
                detectability_line = 10 ** (mag * mag * quad + mag * slope + yint)
                """
                #                M_detect[nsuper][position][nstations] = (
                #                    (-slope + np.sqrt(slope * slope - 4 * quad * (yint - np.log10(threshold))))
                #                    / 2
                #                    / quad
                #                )
                M_detect[nsuper][position][nstations] = np.interp(
                    threshold, factor[nstations] * amp, mags
                )
                (p,) = a1.semilogy(
                    mags,
                    factor[nstations] * amp,
                    marker="h",
                    #                    ms=marker_size[nsuper],
                    color=ncolor[nsuper + "_stations_" + nstations + "_" + position],
                    markeredgecolor="k",
                )
                patches.append(p)
                labels.append(
                    "Option "
                    + tags[ii]
                    + (", MC = %.2f" % M_detect[nsuper][position][nstations]).replace(
                        "-", "$-$"
                    )
                )
                a1.set_xlim([-2, -0])
                a1.set_ylim([0.001, 20])
                a1.grid(True, which="both")
a1.yaxis.set_major_locator(yMajorLocator)
a1.yaxis.set_minor_locator(yMinorLocator)
a1.xaxis.set_major_locator(xMajorLocator)
a1.xaxis.set_minor_locator(xMinorLocator)
a1.set_title("detectable magnitudes")
a1.set_ylabel("amplitude")
a1.set_xlabel("magnitude")
# a1.plot(mag, detectability_line, "k:", lw=1)
a1.plot([-2, 0], [threshold, threshold], "k-.", lw=2)
a1.text(-1.7, 1.05 * threshold, "2$\sigma$ detection threshold", fontsize=14)
a1.legend(patches, labels)
plt.show()


threshold
amp
mags
