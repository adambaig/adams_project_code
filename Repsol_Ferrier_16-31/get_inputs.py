import matplotlib

matplotlib.use("Qt5Agg")
import numpy as np
import matplotlib.pyplot as plt

layer_colors = [
    "darkkhaki",
    "goldenrod",
    "indigo",
    "slategrey",
    "cadetblue",
    "sienna",
    "darksteelblue",
    "darkorange",
    "saddlebrown",
    "darkgreen",
    "olivedrab",
]


def read_stations(station_file):
    f = open(station_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        stations[lspl[0]] = {}
        stations[lspl[0]]["x"] = float(lspl[2])
        stations[lspl[0]]["y"] = float(lspl[1])
        stations[lspl[0]]["z"] = float(lspl[3])
    return stations


def place_stations(writeout=False):
    target_depth = 3550
    lateral_length = 6000
    pad_width = 600
    xwide = target_depth + pad_width / 2
    ywide = target_depth + lateral_length / 2
    nx = 8
    ny = 10
    easting_ctr, northing_ctr = 0, 0
    xrange = np.linspace(easting_ctr - xwide, easting_ctr + xwide, nx)
    yrange = np.linspace(northing_ctr - ywide, northing_ctr + ywide, ny)
    stations = []
    jj = -1
    for ix in xrange:
        for iy in yrange:
            jj += 1
            stations.append({"name": str(jj), "x": ix, "y": iy, "z": -1000.0})
    writeout = True
    if writeout:
        fileout = "Repsol_%i_stations.csv" % (nx * ny)
        g = open(fileout, "w")
        g.write("station, northing (m), easting (m), tvdss (m)\n")
        for station in stations:
            g.write(
                station["name"]
                + ",%.1f,%.1f,%.1f\n" % (station["y"], station["x"], station["z"])
            )
        g.close()
    return stations


def get_homogeneous_model():
    return [{"vp": 5000, "vs": 3000, "rho": 2400}]


def get_velocity_model():
    kb = 984
    depths = [
        -984.0,
        -933.0,
        -753.0,
        -623.0,
        -433.0,
        417.0,
        667.0,
        1317.0,
        1767.0,
        2117.0,
    ]
    vels = [
        1500.0,
        2600.0,
        3200.0,
        2800.0,
        3600.0,
        4000.0,
        4400.0,
        5500.0,
        3350.0,
        6500.0,
    ]
    velocity_model = [
        {
            "vp": vels[0],
            "vs": vels[0] / np.sqrt(3.0),
            "rho": 310.0 * vels[0] ** (0.25),
            "color": layer_colors[0],
        }
    ]
    ii = 0
    for depth in depths[1:]:
        ii += 1
        velocity_model.append(
            {
                "top": depth,
                "vp": vels[ii],
                "vs": vels[ii] / np.sqrt(3.0),
                "rho": 310.0 * vels[ii] ** (0.25),
                "color": layer_colors[ii],
            }
        )
    return velocity_model


def read_velocity_model():
    kb = 600
    dr = "O:\\O&G\\SalesSupport\\20190305_Paramount_2-28\\MC\\"
    f = open(dr + "DSA.mdl")
    lines = f.readlines()
    f.close()
    velocity_model = []
    ii = -1
    for line in lines:
        ii += 1
        lspl = line.split()
        vp = 1000 * float(lspl[0])
        vs = 1000 * float(lspl[1])
        rho = 310 * vp ** (0.25)  # Gardner's relation
        top = 1000 * float(lspl[2]) - kb
        if ii == 0:
            velocity_model.append(
                {"rho": rho, "vp": vp, "vs": vs, "color": layer_colors[ii]}
            )
        else:
            velocity_model.append(
                {"top": top, "rho": rho, "vp": vp, "vs": vs, "color": layer_colors[ii]}
            )
    return velocity_model


def output_discretized_vm():
    f = open("discretized_vm.csv", "w")
    f.write("velocity (m/s)\n")
    velocity_model = get_velocity_model()
    depth, md = -984, 0
    tops = np.array([layer["top"] for layer in velocity_model[1:]])
    while md < 4000:
        ilayer = np.where(tops < depth)[0]
        if len(ilayer) == 0:
            velocity = velocity_model[0]["vp"]
        else:
            ii = ilayer[-1] + 1
            velocity = velocity_model[ii]["vp"]
        f.write("%.0f\n" % velocity)
        depth += 10
        md += 10
    f.close()
