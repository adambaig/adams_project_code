import numpy as np

import utm

f = open("moved_stations.csv")
line = f.readline()
lines = f.readlines()
f.close()

stations = {}
for line in lines:
    lspl = line.split(",")
    station = lspl[0]
    stations[station] = {}
    (
        stations[station]["easting"],
        stations[station]["northing"],
        zone,
        d2,
    ) = utm.from_latlon(float(lspl[2]), float(lspl[1]))


shifts = {
    "SS1": [0, -7],
    "SS3": [0, -5],
    "SS6": [0, -20],
    "SS7": [22, 0],
    "SS8": [20, -20],
    "SS11": [-5, 0],
    "SS12": [20, 5],
    "SS13": [0, -10],
    "SS14": [-10, 0],
    "SS15": [0, -13],
    "SS23": [-10, -10],
    "SS24": [0, -5],
    "SS25": [-15, -15],
    "SS30": [15, -15],
}

f = open("stations_19x30.csv", "w")
f.write("name,longitude,latitude\n")
for station in stations:
    if station[:2] == "SS":
        ii = 0
        if station in shifts:
            lat, lon = utm.to_latlon(
                stations[station]["easting"] + shifts[station][0],
                stations[station]["northing"] + shifts[station][1],
                11,
                "U",
            )
        else:
            lat, lon = utm.to_latlon(
                stations[station]["easting"], stations[station]["northing"], 11, "U"
            )
        f.write(station + (".00,%.8f,%.8f\n" % (lon, lat)))
        for ii in range(3):
            radius = 7.5 * (ii + 1)
            for jj in range(6):
                angle = (ii * 20 + jj * 60) * np.pi / 180.0
                dx = radius * np.sin(angle)
                dy = radius * np.cos(angle)
                if station in shifts:
                    dx += shifts[station][0]
                    dy += shifts[station][1]
                lat, lon = utm.to_latlon(
                    stations[station]["easting"] + dx,
                    stations[station]["northing"] + dy,
                    11,
                    "U",
                )
                f.write(station + ".%02d,%.8f,%.8f\n" % (6 * ii + jj + 1, lon, lat))
    else:
        lat, lon = utm.to_latlon(
            stations[station]["easting"], stations[station]["northing"], 11, "U"
        )
        f.write(station + (",%.8f,%.8f\n" % (lon, lat)))

f.close()
