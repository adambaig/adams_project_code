from pfi_fs.simulate_events import generate_1Csuperstation_waveforms

for directory_name in [
    "30_stations_13_center",
    "50_stations_7_center",
    "50_stations_13_center",
]:
    nsuper, dum, nstations, position = directory_name.split("_")
    sim_config = "simulation" + nstations + ".cfg"
    if position == "center":
        source_config = "source.cfg"
    elif source_config == "toe":
        source_config = "toe_source.cfg"
    station_file = "Repsol_" + nsuper + "_stations.csv"
    generate_1Csuperstation_waveforms(
        source_config,
        sim_config,
        "geology.cfg",
        station_file,
        directory_name=directory_name,
    )
