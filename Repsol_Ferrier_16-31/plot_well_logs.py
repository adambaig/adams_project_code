import matplotlib

matplotlib.use("Qt5Agg")

import glob
import numpy as np
import matplotlib.pyplot as plt

from read_las import read_las

logfiles = glob.glob("LAS\\*.las")

wells = {}

for logfile in logfiles:
    welldata = read_las(logfile)
    wellname = logfile.split("\\")[-1].split(".las")[0]
    wells[wellname] = welldata


fig, ax = plt.subplots(1, 7)
ii = -1
yrange, xrange = [], []
for well in wells:
    ii += 1
    vp = 1.0e6 / wells[well]["DTC_PP"]
    ax[ii].plot(vp, wells[well]["DEPTH"], "royalblue")
    if "DTS_PP" in wells[well]:
        vs = 1.0e6 / wells[well]["DTS_PP"]
        ax[ii].plot(vs, wells[well]["DEPTH"], "firebrick")
    if ii != 0:
        ax[ii].set_yticklabels("")
    yrange.append(ax[ii].get_ylim())
    xrange.append(ax[ii].get_xlim())


y1 = min(np.array(yrange)[:, 0])
y2 = max(np.array(yrange)[:, 1])
x2 = max(np.array(xrange)[:, 1])

for a in ax:
    a.set_ylim([y2, y1])
    a.set_xlim([0, x2])
    a.set_xlabel("velocity (m/s)")

ax[0].set_ylabel("depth (m)")
plt.show()
