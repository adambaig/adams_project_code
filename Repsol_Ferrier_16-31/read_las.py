from numpy import array


def read_las(logfile):
    f = open(logfile)
    file = f.read()
    f.close()

    paramlines = file.split("~Curve\n")[1].split("\n~Parameter")[0].split("\n")
    params = []
    for line in paramlines:
        params.append(line.split(" : ")[1])
    datalines = file.split("~Ascii\n")[1].split("\n")
    nl = len(datalines)
    well = {}
    for param in params:
        well[param] = []
    for line in datalines:
        lspl = [float(s) for s in line.split()]
        ii = -1
        if len(lspl) > 0:
            for param in params:
                ii += 1
                well[param].append(lspl[ii])
    for param in params:
        well[param] = array(well[param])
    return well
