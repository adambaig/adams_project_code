log = r"Logs\SIS_NORTHEAST_NATURAL_ENERGY_MIP_3H_PILOT_Marcellus_SSCAN_Iso_MechPro_result.las"

f = open(log)
las_file = f.read()
f.close()

ind_curve = las_file.find("~Curve")
ind_ascii = las_file.find("~Ascii")
curve_information_block = las_file[ind_curve : ind_ascii - 1].split("\n")
parameter_lines = [
    line for line in curve_information_block if not (line[0] == "#" or line[0] == "~")
]
head = [line.split(".")[0].strip() for line in parameter_lines]
unit = [line.split(".")[1].split()[0] for line in parameter_lines]
n_parameters = len(parameter_lines)
ind_return = ind_ascii + las_file[ind_ascii:].find("\n")
data = las_file[ind_return + 1 :].split()
if len(data) % n_parameters == 0:
    print("all is right with the universe")
else:
    print("uh-oh, the length of data and the number of parameters don't match")

g = open(log.replace(".las", ".csv"), "w")
g.write(",".join(head) + "\n")
g.write(",".join(unit) + "\n")
n_rows = len(data) // n_parameters
for i_row in range(n_rows):
    for i_parameter in range(n_parameters):
        i_data = i_row * n_parameters + i_parameter
        g.write(data[i_data])
        if i_parameter == n_parameters - 1:
            g.write("\n")
        else:
            g.write(",")

g.close()
