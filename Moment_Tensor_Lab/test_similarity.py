from nmxseis.numerics.moment_tensor import MomentTensor, SDR

sdr_dc = SDR(strike =103.3, dip=31.6, rake = -80.4)
sdr_fp = SDR(strike = 83.0, dip=38.9, rake = -89.1)
sim1 = sdr_dc.calculate_similarity(sdr_fp)
sim2 = sdr_fp.calculate_similarity(sdr_dc)
