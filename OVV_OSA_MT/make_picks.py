import json
from pathlib import Path

from nmxseis.model.nslc import NSL
from nmxseis.model.pick import Pick
from nmxseis.model.pick_set import PickSet
from nmxseis.model.phase import Phase
from obspy import UTCDateTime

event_jsons = Path('JSONs').glob('*.json')

for event_json in event_jsons:
	event_dict = json.loads(event_json.read_text())
	origin = [o for o in event_dict['origins'] if o['id']==event_dict['preferred_origin_id']][0]
	picks = []
	for pick in [p['pick'] for p in origin['associations'] if 'pick' in p]:
		pick_time = UTCDateTime(pick['time'])
		nsl = NSL(
			loc=cha['location_code'] if (cha := pick['channel'])['location_code']!='--' else '',
			sta= (sta:=cha['station'])['station_code'],
			net= sta['network']['network_code']
		)
		phase = Phase(pick['phase'])
		picks.append(Pick(time=pick_time, nsl=nsl, phase=phase))
	pick_set = PickSet(picks = picks)
	out_name = f'{str(event_dict["id"]).zfill(10)}_{UTCDateTime(origin["time"]).strftime("%Y%m%d.%H%M%S.%f")}.picks'
	pick_set.dump(Path('Picks').joinpath(out_name))
