import json
from pathlib import Path

json_files = Path("Located").glob("*.json")

for json_file in json_files:
    event_id = json_file.stem
    with open(json_file) as f:
        event = json.load(f)
    if "magnitudes" not in event["event"]["origin"]:
        print(event_id)
