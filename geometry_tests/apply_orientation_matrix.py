from pathlib import Path


from matplotlib import pyplot as plt
from nmxseis.model.nslc import NSL
from nmxseis.model.pick_set import PickSet
import numpy as np
from obspy import read

basedir = Path('C:\Logiklyst\PSM\LogikData\MOC_EST')
shot = read(basedir.joinpath(
	'MSEED',
	'perforations',
	'20230604.214500.000000_20230604.214600.000000.seed')).filter(
		'bandpass', freqmin=2, freqmax=40
		)

nsl = NSL(net='MO', sta='BF16', loc='05')


picks = PickSet.load(basedir.joinpath('Picks','perforations', '0000222680_20230604.214500.000000.picks')).phase_picks

# orientation_matrix = np.array(
# 	[[ 0.94543155, -0.3255869,   0.00816958],
# 	 [ 0.32547723,  0.94543038,  0.01440078],
# 	 [ 0.01521937,  0.01253243, -0.99986811]]
# 	)
orientation_matrix = np.array(
	[[ 0.32556749, 0.94535647, 0.01593342],
	 [-0.94551721, 0.32551024, 0.00733193],
	 [-0.00173473, 0.01858822, -0.99984682]]
)

pick = [p for p in picks if p.nsl == nsl][0]

trc_3c = nsl.select_from_stream(shot).trim(pick.time - 0.001, pick.time + 0.06)
data_3c = np.array([trc_3c[2].data, trc_3c[0].data, trc_3c[1].data])

data_enu = orientation_matrix@data_3c

fig, ax = plt.subplots()
ax.set_aspect('equal')
ax.plot(data_enu[0], data_enu[1], 'k')
fig.show()

