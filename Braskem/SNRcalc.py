import numpy as np
import math
from os.path import isfile, isdir, join
from os import listdir
from obspy import read, Stream
import time
from obspy import UTCDateTime
import matplotlib.pyplot as plt
from glob import glob

# Calculates SNR based on P and S picks (should be already manually reviewed)

# INPUTS #
pickDir = r"C:\Users\adambaig\logikdata\FBK\Picks\Select"
seedDir = r"C:\Users\adambaig\logikdata\FBK\MSEED\Select"
outputFile = r"C:\Users\adambaig\Project\Braskem\SNR\SembStack.csv"

acceptPhase = ["P", "S"]

pWin = 0.02  # window after P arrival to calculate signal
sWin = 0.06  # window after S arrival to calculate signal
buffer = 0.01  # pre pick buffer - gets added to pWin and sWin
noiseGap = 0.1  # gap between pick and end of noise window
# (makes noise window for P and S separately, if using S picks make sure to make large enough to not include P signal)
noiseWin = 0.05  # noise window (seconds)


# for qc
doplot = True

# END INPUTS #

pickFiles = [f for f in listdir(pickDir) if f.split(".")[-1] in ["picks"]]

# SNRoutput = np.empty(shape=[len(pickFiles),6])
SNR = []
for i, pickFile in enumerate(pickFiles):
    picks = np.genfromtxt(join(pickDir, pickFile), delimiter=",", dtype=str)
    nsls = np.array([f for f in picks[:, 0]])
    picks = picks[np.argsort(nsls)]
    # print pickFile
    # find matching seed
    firstPick = np.min(picks[:, 2].astype(float))
    lastPick = np.max(picks[:, 2].astype(float))
    window = [
        UTCDateTime(firstPick - (2 * noiseWin) - buffer),
        UTCDateTime(lastPick + (2 * sWin) + buffer),
    ]
    # assuming continuous seed naming format
    seedFiles = glob(join(seedDir, window[0].strftime("%Y%m%d.%H") + "*"))
    seedFiles += glob(join(seedDir, window[1].strftime("%Y%m%d.%H") + "*"))
    seedFiles = np.unique(np.array(seedFiles))
    st = Stream()
    for seed in seedFiles:
        st += read(seed)
    st.trim(starttime=window[0], endtime=window[1])
    st.merge(fill_value="interpolate")
    st.detrend(type="demean")
    st.filter(type="bandpass", freqmin=10.0, freqmax=50.0, corners=6)
    stations = []
    st.sort(keys=["station", "location"])
    for tr in st:
        stations.append([tr.meta.station])
    if len(st) == 0:
        print("--> Failed, couldnt find st")
        continue
    staCount = np.max(np.unique(np.array(stations), return_counts=True)[1])

    if doplot:
        plt.figure(figsize=(12, 8))
    iterSNR = []
    # Loop through picks to calculate SNR
    for j, pick in enumerate(picks):
        pickSta = pick[0].split(".")[1]
        pickLoc = pick[0].split(".")[2]
        pickTime = UTCDateTime(pick[2]).timestamp
        pickPhase = pick[1]
        if pickPhase not in acceptPhase:
            continue

        if pickPhase == "P":
            pickComps = ["Z"]
        elif pickPhase == "S":
            pickComps = ["1", "2"]
        else:
            print("pickComp not recognized", pick)
            continue
        # Find matching traces
        for pickComp in pickComps:
            try:
                if pickPhase == "P":
                    window = pWin
                elif pickPhase == "S":
                    window = sWin
                else:
                    print("Dont recognize phase " + str(pickPhase))
                    continue

                st2 = st.select(station=pickSta, component=pickComp, location=pickLoc)
                if len(st2) == 0:
                    continue
                jdata = st2.traces[0]

                # Signal bit
                startSample = int(
                    np.floor(
                        (
                            pickTime
                            - buffer
                            - UTCDateTime(jdata.meta.starttime).timestamp
                        )
                        * jdata.meta.sampling_rate
                    )
                )
                endSample = int(
                    np.ceil(
                        (
                            (pickTime + window)
                            - UTCDateTime(jdata.meta.starttime).timestamp
                        )
                        * jdata.meta.sampling_rate
                    )
                )
                meanSample = np.mean(jdata.data[startSample:endSample])
                sample = jdata.data[startSample:endSample] - meanSample

                signal_rms = np.sqrt(np.sum(sample**2) / len(sample))

                # Noise bit
                if pickPhase == "S":
                    noiseEnd = int(
                        np.ceil(startSample - (noiseGap * jdata.meta.sampling_rate))
                    )
                else:
                    noiseEnd = int(
                        np.ceil(startSample - (noiseGap * jdata.meta.sampling_rate))
                    )
                noiseStart = int(
                    np.floor(noiseEnd - (noiseWin * jdata.meta.sampling_rate))
                )
                if noiseStart < 0:
                    noiseStart = 0
                meanNoise = np.mean(jdata.data[noiseStart:noiseEnd])
                noiseSample = jdata.data[noiseStart:noiseEnd] - meanNoise
                noise_rms = np.sqrt(np.sum(noiseSample**2) / len(noiseSample))

                if doplot:
                    plt.subplot(np.ceil(len(picks) / staCount), staCount, j + 1)
                    plt.plot(np.arange(startSample, endSample, 1), sample, "g-")
                    plt.plot(np.arange(noiseStart, noiseEnd, 1), noiseSample, "r-")
                    plt.text(
                        noiseStart, 0, str(10 * math.log10(signal_rms / noise_rms))
                    )
                    plt.xlabel("Sample")
                    plt.ylabel(pickSta + " " + pickLoc + " " + pickPhase, fontsize=8)
                    plt.gca().get_yaxis().set_ticks([])
                    plt.gca().get_xaxis().set_ticks([])
                    # plt.show()
                iterSNR.append(10 * math.log10(signal_rms / noise_rms))

                SNR.append(
                    [
                        pickFile,
                        pickSta,
                        pickPhase,
                        pickComp,
                        pickLoc,
                        signal_rms / noise_rms,
                        10 * math.log10(signal_rms / noise_rms),
                    ]
                )
            except:
                SNR.append(
                    [pickFile, pickSta, pickPhase, pickComp, pickLoc, "nan", "nan"]
                )
                print("FAIL", pickSta, pickPhase, pickComp, pickLoc, "nan")

    print(
        pickFile
        + " Mean SNR: "
        + str(np.nanmean(np.array(iterSNR)))
        + " Max: "
        + str(np.nanmax(np.array(iterSNR)))
        + " Min: "
        + str(np.nanmin(np.array(iterSNR)))
    )
    if doplot:
        plt.subplots_adjust(
            top=0.925, bottom=0.075, left=0.075, right=0.925, wspace=0.1, hspace=0.0
        )
        plt.suptitle(
            pickFile
            + "\nMean SNR (dB): "
            + str(np.nanmean(np.array(iterSNR)))
            + "  Max: "
            + str(np.nanmax(np.array(iterSNR)))
            + "  Min: "
            + str(np.nanmin(np.array(iterSNR))),
            fontsize=12,
        )
        plt.show()

np.savetxt(
    outputFile,
    SNR,
    delimiter=",",
    fmt="%s",
    header="pickFile,pickSta,pickPhase,pickComp,pickLoc,SNR,SNR_db",
)
