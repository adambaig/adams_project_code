from datetime import datetime
import glob
from obspy import read, UTCDateTime, read_inventory
import os


pickfiles = glob.glob("picks//Feb_1//*.picks")
inv = read_inventory(r"Station_xml/BR.xml")
apiUrl_prefix = r"http://35.155.92.119:8081/fdsnws/dataselect/1/query?network=BR&station=*&location=*&channel=HH*,DP*,GP*&"

mseed_dir = f"mseeds{os.sep}Jan_12"

for pickfile in pickfiles:
    f = open(pickfile)
    picks = {
        f'{l.split(",")[0]}_{l.split(",")[1]}': float(l.split(",")[2])
        for l in f.readlines()
        if l.split(",")[1] in ["P", "S"]
    }
    f.close()
    stations = [k.split(".")[1] for k in picks.keys()]
    # if "ESM04" in stations and "ESM06" in stations and "ESM09" in stations:
    min_picks = UTCDateTime(min(picks.values()))
    max_picks = UTCDateTime(max(picks.values()))
    starttime = str(min_picks - 2)[:-4]
    endtime = str(max_picks + 15)[:-4]
    apiUrl = f"{apiUrl_prefix}starttime={starttime}&endtime={endtime}"
    start_dt = datetime.strptime(f"{starttime}000", "%Y-%m-%dT%H:%M:%S.%f")
    end_dt = datetime.strptime(f"{endtime}000", "%Y-%m-%dT%H:%M:%S.%f")
    seedfile = (
        f"{datetime.strftime(start_dt, '%Y%m%d.%H%M%S.%f')}_"
        f"{datetime.strftime(end_dt, '%Y%m%d.%H%M%S.%f')}.seed"
    )
    st = read(apiUrl, format="mseed")
    # for tr in st.select(station='ESM0[4,6]'):
    #     ch_last_2 = tr.meta.channel[1:]
    #     tr.meta.channel = 'D'+ch_last_2
    st.write(f"{mseed_dir}{os.sep}{seedfile}", format="MSEED")
    # else:
    #     root, basename = os.path.split(pickfile)
    #     os.rename(pickfile, os.sep.join([root, "no event", basename]))
