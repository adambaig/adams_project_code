import numpy as np
from obspy import UTCDateTime, read

from SupportModules.StreamSupport import getDataWithPrePost


apollo_server_ip = "35.155.92.119:8081"
channels = [
    "BR.ESM09..GPZ",
    "BR.ESM09..GP1",
    "BR.ESM09..GP2",
    "BR.ESM04..GPZ",
    "BR.ESM04..GP1",
    "BR.ESM04..GP2",
]
delta = 600


start_time = UTCDateTime(2021, 8, 4, 0)
end_time = UTCDateTime(2021, 8, 5, 0)
times = np.arange(
    UTCDateTime(start_time).timestamp, UTCDateTime(end_time).timestamp, delta
)
getDataWithPrePost(
    None,
    [["ApolloServer", channels]],
    0,
    delta,
    times,
    r"InSite_Testing\continuous_data",
    delta=delta,
    apolloserverIp=apollo_server_ip,
)


# st = read('20210805.000000.000000_20210805.000500.000000.seed')
# st.plot()
