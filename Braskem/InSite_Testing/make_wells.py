from obspy import read_inventory
import pyproj as pr


from NocMeta.Meta import NOC_META

INV = read_inventory(r"Station_xml/FBK_Full.xml")
LATLON_PROJ = pr.Proj(init="epsg:4326")
OUT_PROJ = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")

stations = {}
for net in INV:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(
                    LATLON_PROJ, OUT_PROJ, station.longitude, station.latitude
                )
                stations[f"{net.code}.{station.code}."] = {
                    "abs z": -channel.depth,
                    "abs e": east,
                    "abs n": north,
                }

stations
