import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory
import os
import pyproj as pr

from NocMeta.Meta import NOC_META

latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
inv = read_inventory(r"Station_xml/FBK_Full.xml")
surface_stations = ["ESM02", "ESM03", "ESM05", "ESM07"]
sweep_locations = {
    "VB1": {
        "latitude": -9 - 38 / 60.0 - 25.560 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 44.944 / 3600.0,
        "elevation": 6,
    },
    "VB2": {
        "latitude": -9 - 38 / 60.0 - 7.308 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 59.252 / 3600.0,
        "elevation": 7,
    },
    "VB3": {
        "latitude": -9 - 37 / 60.0 - 53.020 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 45.273 / 3600.0,
        "elevation": 57,
    },
}
for sweep, location in sweep_locations.items():
    east, north = pr.transform(
        latlon_proj, out_proj, location["longitude"], location["latitude"]
    )
    location["e"], location["n"] = east, north
data_dir = r"RecordedSweepsAndPicks\SweepSamples\SweepSamples\VB?"
data_seeds = glob.glob(f"{data_dir}{os.sep}*.mseed")
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
stations = {}


def calc_epi_distance(point1, point2):
    distance_sq = sum([(point1[c] - point2[c]) ** 2 for c in ["e", "n"]])
    return np.sqrt(distance_sq)


def distance_from_key(key):
    shot, station, _ = key.split("_")
    return calc_epi_distance(stations[f"BR.{station}"], sweep_locations[shot])


for net in inv:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(
                    latlon_proj, out_proj, station.longitude, station.latitude
                )
                stations[f"{net.code}.{station.code}"] = {
                    "z": -channel.depth,
                    "e": east,
                    "n": north,
                }


fig, ax = {}, {}
for shot in ["VB1", "VB2", "VB3"]:
    fig[shot] = {}
    ax[shot] = {}
    for station in ["ESM02", "ESM03", "ESM05", "ESM07"]:
        fig[shot][station], ax[shot][station] = plt.subplots(figsize=[6, 12])
seeds = glob.glob("decon_seeds//*.mseed")

i_shot = {"VB1": 0, "VB2": 0, "VB3": 0}
for seed in seeds:
    st = read(seed)
    shot = seed.split("\\")[1].split("_")[0]
    i_shot[shot] += 1
    for station in ["ESM02", "ESM03", "ESM05", "ESM07"]:
        three_c = st.select(station=station)
        comp_z = three_c.select(channel="*Z")[0].data
        times = three_c[0].times("matplotlib")
        times -= times[0]
        try:
            comp_1 = three_c.select(channel="*1")[0].data
        except:
            comp_1 = three_c.select(channel="*E")[0].data
        try:
            comp_2 = three_c.select(channel="*2")[0].data
        except:
            comp_2 = three_c.select(channel="*Z")[0].data
        i_zmax = np.argmax(comp_z)
        i_1max = np.argmax(comp_1)
        i_2max = np.argmax(comp_2)
        i_max = min([i_zmax, i_1max, i_2max])
        i_start = i_max - 100
        i_end = i_max + 300
        i_start = 5800
        i_end = 6200
        norm = max([max(abs(s)) for s in [comp_1, comp_2, comp_z]])

        ax[shot][station].plot(
            times[i_start:i_end] - times[i_start],
            i_shot[shot] + comp_z[i_start:i_end] / norm,
            color="0.2",
        )

len(comp_z)


for shot in ["VB1", "VB2", "VB3"]:
    for station in ["ESM02", "ESM03", "ESM05", "ESM07"]:
        distance = calc_epi_distance(sweep_locations[shot], stations[f"BR.{station}"])
        ax[shot][station].set_title(
            f"{shot} recorded on {station}, distance {distance:.0f}m"
        )
        ax[shot][station].set_xlabel("time (s)")
        fig[shot][station].savefig(f"deconvolved_sweep_images//{shot}_{station}.png")
