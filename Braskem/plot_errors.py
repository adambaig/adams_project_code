import matplotlib.pyplot as plt
import numpy as np

from read_inputs import read_catalog

class_A = read_catalog("Catalogs//Class_A_for_error_plotting.csv")
class_H = read_catalog("Catalogs//Class_H_for_error_plotting.csv")

fig, ax = plt.subplots()
(p_A,) = ax.plot(
    [v["n_phases"] for v in class_A.values()],
    [v["ellipsoid"][2, 2] for v in class_A.values()],
    "o",
    alpha=0.51,
)
(p_H,) = ax.plot(
    [v["n_phases"] for v in class_H.values()],
    [v["ellipsoid"][2, 2] for v in class_H.values()],
    "o",
    alpha=1,
)
ax.set_xlabel("number of phases picked")
ax.set_ylabel("depth error (m)")
ax.set_title("depth errors")
ax.set_ylim([0, 500])
ax.grid()
ax.legend([p_A, p_H], ["Class A", "Class H"])
#
fig_h, ax_h = plt.subplots()
# ax_h.plot(
#     [v["n_phases"] for v in class_H.values()],
#     [np.sqrt(v["ellipsoid"][0, 0] ** 2 + v["ellipsoid"][1, 1] ** 2) for v in class_H.values()],
#     "o",
#     alpha=1,
# )
# ax_h.plot(
#     [v["n_phases"] for v in class_A.values()],
#     [np.sqrt(v["ellipsoid"][0, 0] ** 2 + v["ellipsoid"][1, 1] ** 2) for v in class_A.values()],
#     "o",
#     alpha=0.51,
# )
ax_h.plot(
    [v["n_phases"] for v in class_H.values()],
    [max([v["ellipsoid"][0, 0], v["ellipsoid"][1, 1]]) for v in class_H.values()],
    "o",
    alpha=1,
)
ax_h.plot(
    [v["n_phases"] for v in class_A.values()],
    [max([v["ellipsoid"][0, 0], v["ellipsoid"][1, 1]]) for v in class_A.values()],
    "o",
    alpha=0.51,
)

ax_h.set_ylim([0, 200])
ax_h.legend([p_A, p_H], ["Class A", "Class H"])
ax_h.set_title("horizontal errors")
ax_h.set_ylabel("horizontal error (m)")
ax_h.set_xlabel("number of phases picked")
ax_h.grid()
fig.savefig("depth_errors.png")
fig_h.savefig("hrz_errors.png")

fig_hyp, ax_hyp = plt.subplots()
ax_hyp.plot(
    [v["n_phases"] for v in class_H.values()],
    [
        np.sqrt(
            v["ellipsoid"][0, 0] ** 2
            + v["ellipsoid"][1, 1] ** 2
            + v["ellipsoid"][2, 2] ** 2
        )
        for v in class_H.values()
    ],
    "o",
    alpha=1,
)
ax_hyp.plot(
    [v["n_phases"] for v in class_A.values()],
    [
        np.sqrt(
            v["ellipsoid"][0, 0] ** 2
            + v["ellipsoid"][1, 1] ** 2
            + v["ellipsoid"][2, 2] ** 2
        )
        for v in class_A.values()
    ],
    "o",
    alpha=0.51,
)
ax_hyp.set_ylim([0, 500])
ax_hyp.legend([p_A, p_H], ["Class A", "Class H"])
ax_hyp.set_title("hypocentral errors")
ax_hyp.set_ylabel("hypocentral error (m)")
ax_hyp.set_xlabel("number of phases picked")
ax_hyp.grid()
fig.savefig("depth_errors.png")
fig_h.savefig("hrz_errors.png")
fig_hyp.savefig("hypocentral_errors.png")
