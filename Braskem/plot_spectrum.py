from datetime import datetime
import glob
import matplotlib.pyplot as plt
from matplotlib import patches
import numpy as np
import os
from obspy import read_inventory
from scipy.fftpack import fft, fftfreq


from obspy import read, UTCDateTime

pick_dir = "picks//manual_repicked"
seed_dir = "mseeds//repicked"
events = glob.glob(f"{pick_dir}{os.sep}*.picks")
seeds = glob.glob(f"{seed_dir}{os.sep}*.seed")
event = events[-3]
inv = read_inventory("Station_xml//FBK_Full.xml")
waveform_seeds = {}
for seed in seeds:
    basename = os.path.basename(seed).split(".seed")[0]
    start, end = [datetime.strptime(s, "%Y%m%d.%H%M%S.%f") for s in basename.split("_")]
    waveform_seeds[seed] = {"start": start, "end": end}


def find_seed(event_time):
    for seed, waveform in waveform_seeds.items():
        if event_time > waveform["start"] and event_time < waveform["end"]:
            return seed
    return None


event_time = datetime.strptime(
    os.path.basename(event).split("_")[1].split(".picks")[0], "%Y%m%d.%H%M%S.%f"
)
stream = (
    read(find_seed(event_time))
    .trim(
        starttime=UTCDateTime(UTCDateTime(event_time)) - 1,
        endtime=UTCDateTime(UTCDateTime(event_time)) + 2,
    )
    .filter("highpass", freq=1)
)

stream = read(find_seed(event_time)).rotate("->ZNE", inventory=inv)
station = "ESM09"
channel = "GPN"
channel = stream.select(station=station, channel=channel).detrend(type="linear")[0]
three_c = stream.select(station=station)
f = open(event)
picks = {
    f'{l.split(",")[0]}_{l.split(",")[1]}': float(l.split(",")[2])
    for l in f.readlines()
    if l.split(",")[1] in ["P", "S"]
}
f.close()
three_c.plot()


trend = 297.1
plunge = 57.5
PI = np.pi
D2R = PI / 180
colatitude = (90 + plunge) * D2R
longitude = trend * D2R
P_pol = np.array(
    [
        np.sin(colatitude) * np.cos(longitude),
        np.sin(colatitude) * np.sin(longitude),
        np.cos(colatitude),
    ]
)
V_pol = np.array(
    [
        np.cos(colatitude) * np.cos(longitude),
        np.cos(colatitude) * np.sin(longitude),
        -np.sin(colatitude),
    ]
)
H_pol = np.array([-np.sin(longitude), np.cos(longitude), 0])

rotation_matrix = np.array([P_pol.T, V_pol.T, H_pol.T])
three_c_data = np.vstack(
    [
        three_c.select(channel="*E")[0].data,
        three_c.select(channel="*N")[0].data,
        three_c.select(channel="*Z")[0].data,
    ]
)

rotated_data = rotation_matrix.T @ three_c_data

fig, (ax1, ax2, ax3) = plt.subplots(3)

ax1.plot(rotated_data[0, :])
ax2.plot(rotated_data[1, :])
ax3.plot(rotated_data[2, :])

times = channel.times()
data = channel.data
fig = plt.figure(figsize=[12, 8])
ax_trace = fig.add_axes([0.1, 0.8, 0.8, 0.1])
ax_spec = fig.add_axes([0.1, 0.1, 0.8, 0.6])
ax_trace.plot(times, data)
pick = [UTCDateTime(t) for p, t in picks.items() if station in p and p[-1] == "P"]
if len(pick) > 0:
    i_window_length = 20
    i_pick = np.argmin(abs(times - pick))
    i_end = i_pick + i_window_length
    i_start_n = i_pick - i_window_length - 3
    i_end_n = i_pick - 3

    y1, y2 = ax_trace.get_ylim()
    window_signal = patches.Rectangle(
        (times[i_pick], y1),
        times[i_end - 1] - times[i_start],
        y2 - y1,
        facecolor="lightblue",
        edgecolor="k",
        alpha=0.3,
    )
    window_noise = patches.Rectangle(
        (times[i_start_n], y1),
        times[i_end_n - 1] - times[i_start_n],
        y2 - y1,
        facecolor="0.7",
        edgecolor="k",
        alpha=0.3,
    )
    ax_trace.add_artist(window_signal)
    ax_trace.add_artist(window_noise)
    signal = data[i_start:i_end]
    noise = data[i_start_n:i_end_n]
    freq_signal = fftfreq(len(signal), 0.001)
    freq_noise = fftfreq(len(noise), 0.001)
    i_one_side = np.where(freq_signal > 0)[0]
    i_one_side_n = np.where(freq_noise > 0)[0]

ax_trace.set_xlabel("time (s)")


ax_spec.loglog(
    freq_signal[i_one_side],
    abs(fft(signal))[i_one_side] / 2 / np.pi / freq_signal[i_one_side] / len(signal),
)
ax_spec.loglog(
    freq_noise[i_one_side_n],
    abs(fft(noise))[i_one_side_n] / 2 / np.pi / freq_noise[i_one_side_n] / len(noise),
    color="0.7",
    zorder=-3,
)

ax_spec.set_xlabel("frequency(Hz)")
ax_spec.set_ylabel("displacement spectrum (counts$\cdot$ s)")

ax_spec.plot([5, 35, 350], [1, 1, 1e-4], "r")
ax_spec.plot([5, 500], [0.03, 3e-4], "k")
fig.savefig("spectrum_example.png")
