import glob
from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory
import os
import pyproj as pr
from scipy.fftpack import fftfreq, fft

from NocMeta.Meta import NOC_META
from generalPlots import gray_background_with_grid

inv = read_inventory(r"Station_xml/FBK_Full.xml")

surface_stations = ["ESM02", "ESM03", "ESM05", "ESM07"]
sweep_locations = {
    "VB1": {
        "latitude": -9 - 38 / 60.0 - 25.560 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 44.944 / 3600.0,
        "elevation": 6,
    },
    "VB2": {
        "latitude": -9 - 38 / 60.0 - 7.308 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 59.252 / 3600.0,
        "elevation": 7,
    },
    "VB3": {
        "latitude": -9 - 37 / 60.0 - 53.020 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 45.273 / 3600.0,
        "elevation": 57,
    },
}

data_dir = r"RecordedSweepsAndPicks\SweepSamples\SweepSamples\VB?"
data_seeds = glob.glob(f"{data_dir}{os.sep}*.mseed")
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
fig_map, ax_map = plt.subplots(figsize=[12, 12])
ax_map.set_aspect("equal")
stations = {}

for net in inv:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(
                    latlon_proj, out_proj, station.longitude, station.latitude
                )
                stations[f"{net.code}.{station.code}"] = {
                    "z": -channel.depth,
                    "e": east,
                    "n": north,
                }
                if station.code in surface_stations:
                    p_surface_station = ax_map.plot(
                        east,
                        north,
                        "v",
                        ms=14,
                        color="forestgreen",
                        markeredgecolor="k",
                    )
                    ax_map.text(east + 20, north, station.code, va="center")
                else:
                    p_downhole_station = ax_map.plot(
                        east, north, "v", ms=10, color="0.2", markeredgecolor="k"
                    )


for sweep, location in sweep_locations.items():
    east, north = pr.transform(
        latlon_proj, out_proj, location["longitude"], location["latitude"]
    )
    location["e"], location["n"] = east, north
    p_vibe_location = ax_map.plot(
        east, north, "o", ms=14, color="lightsteelblue", markeredgecolor="k"
    )
    ax_map.text(east, north - 30, sweep, ha="center", va="top")
ax_map = gray_background_with_grid(ax_map, grid_spacing=100)


ax_map.legend(
    [p_surface_station[0], p_downhole_station[0], p_vibe_location[0]],
    ["Surface Sensor", "Downhole Sensor", "Vibroseis Sweep"],
)

fig, ax = {}, {}
for station in surface_stations:
    fig[station], ax[station] = plt.subplots()
    ax[station].set_facecolor("0.9")

colors = {"VB1": "firebrick", "VB2": "royalblue", "VB3": "gold"}

custom_lines = [
    Line2D([0], [0], color="firebrick"),
    Line2D([0], [0], color="royalblue"),
    Line2D([0], [0], color="gold"),
]

for seed in data_seeds:
    st = read(seed)
    shot = seed.split(os.sep)[-2]
    for station in surface_stations:
        three_c = st.select(station=station)
        trz = three_c.select(station=station, channel="*z")[0].remove_response(
            inventory=inv
        )
        freq = fftfreq(len(trz), trz.stats.delta)
        # freq = fftfreq(8000, trz.stats.delta)
        i_one_side = np.where(freq > 0)[0]
        f_response = fft(trz)
        ax[station].loglog(
            freq[i_one_side],
            abs(f_response[i_one_side]) / len(trz),
            color=colors[shot],
            alpha=0.2,
        )

for station in surface_stations:
    ax[station].set_xlim([1, 100])
    ax[station].set_title(station)
    ax[station].set_ylabel("velocity spectrum (m)")
    ax[station].set_xlabel("frequency (Hz)")
    ax[station].legend(custom_lines, ["VB1", "VB2", "VB3"])
    y1, y2 = ax[station].get_ylim()
    ax[station].loglog([16, 16], [y1, y2], "--", color="0.2")
    ax[station].text(18, 2 * y1, "16 Hz")
    ax[station].set_ylim([y1, y2])
    fig[station].savefig(f"vibe_spectrum_{station}.png")

fig_map.savefig("surface_stations_and_vibes.png")
