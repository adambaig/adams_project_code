import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
from obspy import read, read_inventory, UTCDateTime
from obspy.core.event import read_events
import pyproj as pr


from NocMeta.Meta import NOC_META

inv = read_inventory("Station_xml//FBK_Full.xml")

st = read("event_seed//event_2020-11-28T08_56_43.207Z.seed")

latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")

event_t0 = "2020-11-28T08:56:43Z"
latitude = -9.63318543337359
longitude = -35.749515818433544
depth_km = 0.78

east, north = pr.transform(latlon_proj, out_proj, longitude, latitude)
inv.select(station="ESM04")[0][0].latitude
dh_stations = {}
for station in ["ESM04", "ESM06", "ESM09"]:
    station_inventory = inv.select(station=station, channel="DP?")[0][0]
    station_east, station_north = pr.transform(
        latlon_proj, out_proj, station_inventory.longitude, station_inventory.latitude
    )
    dh_stations[station] = {
        "e": station_east,
        "n": station_north,
        "z": station_inventory.elevation - station_inventory[0].depth,
    }

quakeml = read_events("quakeml//2967.quakeml")
f = open("picks//0000002964_20201128.085634.856791.picks")
picks = {
    f'{l.split(",")[0]}_{l.split(",")[1]}': float(l.split(",")[2])
    for l in f.readlines()
    if l.split(",")[1] in ["P", "S"]
}
f.close()

t_start = UTCDateTime(min(picks.values()) - 0.1)
t_end = UTCDateTime(max(picks.values()) + 0.5)

dh_stream = st.select(station="ESM0[4,6,9]").filter("bandpass", freqmin=1, freqmax=50)
dh_stream.rotate("->ZNE", inventory=inv).trim(starttime=t_start, endtime=t_end)
times = dh_stream[0].times("utcdatetime")
fig = plt.figure(figsize=[16, 6])
ax = fig.add_axes([0.1, 0.1, 0.6, 0.8])
ax_hodo = [
    fig.add_axes([0.7, 0.1, 0.1, 0.26666]),
    fig.add_axes([0.7, 0.36666, 0.1, 0.26666]),
    fig.add_axes([0.7, 0.63333, 0.1, 0.26666]),
]
for a in ax_hodo:
    a.set_aspect("equal")
    a.set_xticks([])
    a.set_yticks([])
hodo_length = 15
for i_station, station in enumerate(dh_stations):
    three_c = dh_stream.select(station=station)
    z_comp = three_c.select(channel="DPZ")[0].data
    n_comp = three_c.select(channel="DPN")[0].data
    e_comp = three_c.select(channel="DPE")[0].data
    max_trace = max([max(abs(z_comp)), max(abs(n_comp)), max(abs(e_comp))]) * 2
    ax.plot(times, i_station + z_comp / max_trace, "0.3")
    ax.plot(times, i_station + e_comp / max_trace, "firebrick")
    ax.plot(times, i_station + n_comp / max_trace, "royalblue")
    pick = [UTCDateTime(t) for p, t in picks.items() if station in p and p[-1] == "P"][
        0
    ]
    ax.plot([pick, pick], [i_station - 0.5, i_station + 0.5], "k", zorder=2)
    i_pick = np.argmin(abs(times - pick))
    ax_hodo[i_station].plot(
        e_comp[i_pick : i_pick + hodo_length],
        z_comp[i_pick : i_pick + hodo_length],
        color="0.2",
    )
    ax_hodo[i_station].plot([0, 0], [-max_trace, max_trace], "k:", zorder=-1)
    ax_hodo[i_station].plot([-max_trace, max_trace], [0, 0], "k:", zorder=-1)
    ax_hodo[i_station].set_xlim([-0.5 * max_trace, 0.5 * max_trace])
    ax_hodo[i_station].set_ylim([-0.5 * max_trace, 0.5 * max_trace])
    window = Rectangle(
        (times[i_pick], i_station - 0.5),
        times[hodo_length + i_pick] - times[i_pick],
        1,
        facecolor="lightblue",
        edgecolor="k",
        alpha=0.3,
    )
    ax.add_artist(window)
ax.set_yticks([0, 1, 2])
ax.set_yticklabels(dh_stations.keys())

fig_map, ax_map = plt.subplots()
ax_map.set_aspect("equal")
ax_map.plot(east, north, "*", color="firebrick", ms=14)
ax_map.plot(
    [v["e"] for v in dh_stations.values()], [v["n"] for v in dh_stations.values()], "v"
)
