from datetime import datetime
import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory
import os
import pyproj as pr
from scipy.optimize import minimize

from NocMeta.Meta import NOC_META
from sms_moment_tensor.MT_math import mt_vector_to_matrix
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_moment_tensor.MT_math import (
    unit_vector_to_trend_plunge,
    trend_plunge_to_unit_vector,
)
from generalPlots import gray_background_with_grid

from read_inputs import read_velocity_model

pick_dir = "picks"
seed_dir = "mseeds"
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")

events = glob.glob(f"{pick_dir}{os.sep}*.picks")
seeds = glob.glob(f"{seed_dir}{os.sep}*.seed")

event = events[0]
f = open(event)
picks = {
    f'{l.split(",")[0]}_{l.split(",")[1]}': float(l.split(",")[2])
    for l in f.readlines()
    if l.split(",")[1] in ["P", "S"]
}
f.close()
ref_east, ref_north = 198261.53764946375, 8933884.42307478
inv = read_inventory(r"Station_xml/FBK_Full.xml")
station
stations = {}
for net in inv:
    for station in net:
        if station.code in [p.split(".")[1] for p in picks]:
            channel = inv.select(station=station.code, channel="?PZ")
            if len(channel) == 0:
                channel = inv.select(station=station.code, channel="HHZ")
            east, north = pr.transform(
                latlon_proj, out_proj, station.longitude, station.latitude
            )
            stations[f"{net.code}.{station.code}"] = {
                "z": -channel[0][0][0].depth,
                "e": east - ref_east,
                "n": north - ref_north,
            }


waveform_seeds = {}
for seed in seeds:
    basename = os.path.basename(seed).split(".seed")[0]
    start, end = [datetime.strptime(s, "%Y%m%d.%H%M%S.%f") for s in basename.split("_")]
    waveform_seeds[seed] = {"start": start, "end": end}


def find_seed(event_time):
    for seed, waveform in waveform_seeds.items():
        if event_time > waveform["start"] and event_time < waveform["end"]:
            return seed
    return None


def tt_residual(xyzt, picks, velocity_model):
    xyz = {"e": xyzt[0], "n": xyzt[1], "z": xyzt[2]}
    residual = 0
    t_zero = min([p for p in picks.values()])
    for stationphase, pick in picks.items():
        station = stations[".".join(stationphase.split(".")[:2])]
        phase = stationphase.split("._")[-1]
        t_xyz = isotropic_ray_trace(xyz, station, velocity_model, phase)["traveltime"]
        residual += (t_xyz - pick + t_zero + xyzt[3]) ** 2
    return residual


velocity_model = read_velocity_model()
unique_stations = np.unique([".".join(k.split(".")[:2]) for k in picks])
starting_location = np.average(
    [(stations[v]["e"], stations[v]["n"], stations[v]["z"]) for v in unique_stations],
    axis=0,
)

minimizer = minimize(
    tt_residual,
    [*starting_location, 0],
    args=(picks, velocity_model),
    method="Nelder-Mead",
)
simp, fun = minimizer.final_simplex
simp
