from datetime import datetime
import glob
import matplotlib.pyplot as plt
import numpy as np
import os
from obspy import read, read_inventory, UTCDateTime

from read_inputs import read_catalog, dh_station_locations

inv = read_inventory(r"Station_xml/FBK_Full.xml")
st = read(r"mseeds\20201120.014723.770000_20201120.014725.799000.seed")


catalog = read_catalog("Catalogs//some_more_deeper_events.csv")


pick_dir = "picks"
seed_dir = "mseeds"
events = glob.glob(f"{pick_dir}{os.sep}*.picks")
seeds = glob.glob(f"{seed_dir}{os.sep}*.seed")

catalog = read_catalog("Catalogs//some_more_deeper_events.csv")
stations = dh_station_locations()
waveform_seeds = {}
for seed in seeds:
    basename = os.path.basename(seed).split(".seed")[0]
    start, end = [datetime.strptime(s, "%Y%m%d.%H%M%S.%f") for s in basename.split("_")]
    waveform_seeds[seed] = {"start": start, "end": end}


stations


def find_seed(event_time):
    for seed, waveform in waveform_seeds.items():
        if event_time > waveform["start"] and event_time < waveform["end"]:
            return seed
    return None


event = glob.glob("picks//*.picks")[0]

int(os.path.basename(event).split("_")[0]) in event_ids


pick_id = 1317
event_e, event_n = np.array(
    [
        [v["easting"], v["northing"]]
        for v in catalog.values()
        if pick_id == int(v["athena database id"])
    ]
).T


f = open(event)
picks = {
    f'{l.split(",")[0]}_{l.split(",")[1]}': float(l.split(",")[2])
    for l in f.readlines()
    if l.split(",")[1] in ["P", "S"]
}
f.close()
event_time = datetime.strptime(
    os.path.basename(event).split("_")[1].split(".picks")[0], "%Y%m%d.%H%M%S.%f"
)
stream = (
    read(find_seed(event_time))
    .trim(
        starttime=UTCDateTime(UTCDateTime(event_time)) - 1,
        endtime=UTCDateTime(UTCDateTime(event_time)) + 2,
    )
    .detrend(type="linear")
    .taper(type="hann", max_percentage=None, max_length=0.2, side="both")
    .filter(type="highpass", freq=2)
)

st.rotate("->ZNE", inventory=inv)

for tr in st:
    print(tr)


stations = [p.split("._")[0] for p, t in picks.items() if p[-1] == "P"]
