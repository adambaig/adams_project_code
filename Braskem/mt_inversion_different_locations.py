from glob import glob
import json
import os
import requests

import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import numpy as np
from obspy import read_inventory, read, UTCDateTime
import pyproj as pr
from scipy.optimize import minimize
from scipy.fftpack import fft, fftfreq

from NocMeta.Meta import NOC_META
from sms_moment_tensor.MT_math import mt_vector_to_matrix
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from sms_moment_tensor.MT_math import (
    unit_vector_to_trend_plunge,
    trend_plunge_to_unit_vector,
)
from generalPlots import gray_background_with_grid

from read_inputs import read_velocity_model

velocity_model = read_velocity_model()

D2R = np.pi / 180.0
tab10 = get_cmap("tab10")

x1, x2 = 197986.58688787775, 198854.75025504341
y1, y2 = 8932712.230823414, 8934324.638987586

dx = 1.1 * (x2 - x1)
dy = 1.1 * (y2 - y1)
dz = 1300

x1p = (x1 + x2) / 2 - 0.5 * dx
x2p = (x1 + x2) / 2 + 0.5 * dx
y1p = (y1 + y2) / 2 - 0.5 * dy
y2p = (y1 + y2) / 2 + 0.5 * dy
z1 = -1200
z2 = 100

inv = read_inventory(r"Station_xml/FBK_Full.xml")
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
stations = {}
for net in inv:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(
                    latlon_proj, out_proj, station.longitude, station.latitude
                )
                stations[f"{net.code}.{station.code}."] = {
                    "abs z": -channel.depth,
                    "abs e": east,
                    "abs n": north,
                }


east, north, elevation = np.average(
    [(v["abs e"], v["abs n"], v["abs z"]) for v in stations.values()], axis=0
)

for station in stations.values():
    station["e"] = station["abs e"] - east
    station["n"] = station["abs n"] - north
    station["z"] = station["abs z"] - elevation

velocity_model = [
    {"rho": 2100, "vp": 1510.0, "vs": 1510.0 / 2.03},
    {"rho": 2200, "vp": 2940.0, "vs": 2940.0 / 2.03, "top": -320},
    {"rho": 2200, "vp": 3610.0, "vs": 3610.0 / 2.03, "top": -420},
    {"rho": 2400, "vp": 4100.0, "vs": 4100.0 / 2.03, "top": -920},
]
cardinal_vector = {
    "U": np.array([0, 0, 1]),
    "E": np.array([1, 0, 0]),
    "N": np.array([0, 1, 0]),
    "D": np.array([0, 0, -1]),
    "W": np.array([-1, 0, 0]),
    "S": np.array([0, -1, 0]),
}
Q = {"P": 60, "S": 60}
pick_color = {"P": "cyan", "S": "green"}
min_buffer, max_buffer = 0.1, 0.5

with open("waveform_features//11743_wf.json") as f:
    waveform_features = json.load(f)

first_motion = {}
with open("FirstMotion_signs//11743.csv") as f:
    head = f.readline()
    for line in f.readlines():
        lspl = line.split(",")
        first_motion[lspl[0]] = {}
