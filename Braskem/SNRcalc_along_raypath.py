from glob import glob
import json
import logging
import math
from os.path import isfile, isdir, join
from os import listdir
import requests
import time

import matplotlib.pyplot as plt
import numpy as np
from obspy import read, Stream, read_inventory
from obspy import UTCDateTime
import pyproj as pr

from NocMeta.Meta import NOC_META
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.rotation import rotate_to_raypath


# Calculates SNR based on P and S picks (should be already manually reviewed)

# INPUTS #


pickDir = r"C:\Users\adambaig\logikdata\FBK\Picks\Select"
seedDir = r"C:\Users\adambaig\logikdata\FBK\MSEED\Select"
outputFile = r"C:\Users\adambaig\Project\Braskem\SNR\SembStack.csv"
station_xml_file = r"C:\Users\adambaig\Project\Braskem\Station_xml\FBK_Full.xml"
sourceTag = "FBK"

acceptPhase = ["P", "S"]

pWin = 0.02  # window after P arrival to calculate signal
sWin = 0.06  # window after S arrival to calculate signal
buffer = 0.01  # pre pick buffer - gets added to pWin and sWin
noiseGap = 0.1  # gap between pick and end of noise window
# (makes noise window for P and S separately, if using S picks make sure to make large enough to not include P signal)
noiseWin = 0.05  # noise window (seconds)


# for qc
doplot = False

velocity_model = [
    {"rho": 2100, "vp": 1510.0, "vs": 1510.0 / 2.03},
    {"rho": 2200, "vp": 2940.0, "vs": 2940.0 / 2.03, "top": -320},
    {"rho": 2200, "vp": 3610.0, "vs": 3610.0 / 2.03, "top": -420},
    {"rho": 2400, "vp": 4100.0, "vs": 4100.0 / 2.03, "top": -920},
]
Q = {"P": 60, "S": 60}


# END INPUTS #

pickFiles = [f for f in listdir(pickDir) if f.split(".")[-1] in ["picks"]]
inv = read_inventory(station_xml_file)

latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
stations = {}
for net in inv:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(
                    latlon_proj, out_proj, station.longitude, station.latitude
                )
                stations[f"{net.code}.{station.code}."] = {
                    "z": -channel.depth,
                    "e": east,
                    "n": north,
                }


SNR = []
eventId = str(int(pickFile.split("_")[0]))


def gather_rays(source, stations):
    raypaths = {}
    for station_name, station in stations.items():
        for phase in ["P", "S"]:
            raypaths[station_name + "." + phase] = isotropic_ray_trace(
                source, station, velocity_model, phase
            )
    return raypaths


def vertical_slowness_at_sensor(raypath):
    up_plus_minus = np.sign(raypath["velocity_model_chunk"][-1]["h"])
    abs_slowness = 1.0 / raypath["velocity_model_chunk"][-1]["v"]
    hrz_slowness_sq = sum([v * v for v in raypath["hrz_slowness"].values()])
    return up_plus_minus * np.sqrt(abs_slowness**2 - hrz_slowness_sq)


for i, pickFile in enumerate(pickFiles):
    picks = np.genfromtxt(join(pickDir, pickFile), delimiter=",", dtype=str)
    nsls = np.array([f for f in picks[:, 0]])
    response = requests.get(
        r"https://"
        + NOC_META[sourceTag]["athIP"]
        + r"/events/"
        + eventId
        + ".json?expand=all&apiKey="
        + NOC_META[sourceTag]["athApi"]
    )
    if not response.ok:
        logging.error(
            "Error {} retrieving event {} from Athena: {}".format(
                response.status_code, eventId, response.reason
            )
        )
        continue
    event = json.loads(response.content.decode("utf-8"))
    picks = picks[np.argsort(nsls)]
    # print pickFile
    # find matching seed
    firstPick = np.min(picks[:, 2].astype(float))
    lastPick = np.max(picks[:, 2].astype(float))
    window = [
        UTCDateTime(firstPick - (2 * noiseWin) - buffer),
        UTCDateTime(lastPick + (2 * sWin) + buffer),
    ]
    # assuming continuous seed naming format
    seedFiles = glob(join(seedDir, window[0].strftime("%Y%m%d.%H") + "*"))
    seedFiles += glob(join(seedDir, window[1].strftime("%Y%m%d.%H") + "*"))
    seedFiles = np.unique(np.array(seedFiles))
    st = Stream()
    for seed in seedFiles:
        st += read(seed)
    st.trim(starttime=window[0], endtime=window[1])
    st.merge(fill_value="interpolate")
    st.detrend(type="demean")
    st.filter(type="bandpass", freqmin=10.0, freqmax=50.0, corners=6)
    seed_stations = []
    st.sort(keys=["station", "location"])
    st.rotate("->ZNE", inventory=inv)
    for tr in st:
        seed_stations.append([tr.meta.station])
    if len(st) == 0:
        print("--> Failed, couldnt find st")
        continue
    staCount = np.max(np.unique(np.array(seed_stations), return_counts=True)[1])

    if doplot:
        plt.figure(figsize=(12, 8))
    iterSNR = []
    # Loop through picks to calculate SNR
    picked_stations = np.unique(picks[:, 0])
    event.keys()
    preferred_origin = [
        origin
        for origin in event["origins"]
        if origin["id"] == event["preferred_origin_id"]
    ][0]
    east, north = pr.transform(
        latlon_proj,
        out_proj,
        preferred_origin["longitude"],
        preferred_origin["latitude"],
    )
    source = {"e": east, "n": north, "z": 1000 * preferred_origin["depth"]}
    rays = gather_rays(
        source, {name: sta for name, sta in stations.items() if name in picked_stations}
    )

    for j, pick in enumerate(picks):
        pickNet = pick[0].split(".")[0]
        pickSta = pick[0].split(".")[1]
        pickLoc = pick[0].split(".")[2]
        pickTime = UTCDateTime(pick[2]).timestamp
        pickPhase = pick[1]
        if pickPhase not in acceptPhase:
            continue

        st_3comp = st.select(station=pickSta, location=pickLoc)
        waveform_cardinal = {
            "e": st_3comp.select(channel="??E")[0].data,
            "n": st_3comp.select(channel="??N")[0].data,
            "z": st_3comp.select(channel="??Z")[0].data,
        }
        ray_key = ".".join([pickNet, pickSta, pickLoc, pickPhase])
        raypath = rays[ray_key]
        rotated_waveform = rotate_to_raypath(waveform_cardinal, raypath, mode="pvh")
        if pickPhase == "P":
            window = pWin
            pickComps = ["p"]
        elif pickPhase == "S":
            window = sWin
            pickComps = ["v", "h"]
        else:
            continue
        for pickComp in pickComps:
            if pickPhase == "P":
                window = pWin
            elif pickPhase == "S":
                window = sWin
            else:
                print("Dont recognize phase " + str(pickPhase))
                continue

            jdata = st_3comp.traces[0]
            # Signal bit
            startSample = int(
                np.floor(
                    (pickTime - buffer - UTCDateTime(jdata.meta.starttime).timestamp)
                    * jdata.meta.sampling_rate
                )
            )
            endSample = int(
                np.ceil(
                    ((pickTime + window) - UTCDateTime(jdata.meta.starttime).timestamp)
                    * jdata.meta.sampling_rate
                )
            )
            signal_rms = np.std(rotated_waveform[pickComp][startSample:endSample])

            # Noise bit
            if pickPhase == "S":
                noiseEnd = int(
                    np.ceil(startSample - (noiseGap * jdata.meta.sampling_rate))
                )
            else:
                noiseEnd = int(
                    np.ceil(startSample - (noiseGap * jdata.meta.sampling_rate))
                )
            noiseStart = int(np.floor(noiseEnd - (noiseWin * jdata.meta.sampling_rate)))
            if noiseStart < 0:
                noiseStart = 0
            noise_rms = np.std(rotated_waveform[pickComp][noiseStart:noiseEnd])

            if doplot:
                plt.subplot(np.ceil(len(picks) / staCount), staCount, j + 1)
                plt.plot(np.arange(startSample, endSample, 1), sample, "g-")
                plt.plot(np.arange(noiseStart, noiseEnd, 1), noiseSample, "r-")
                plt.text(noiseStart, 0, str(10 * math.log10(signal_rms / noise_rms)))
                plt.xlabel("Sample")
                plt.ylabel(pickSta + " " + pickLoc + " " + pickPhase, fontsize=8)
                plt.gca().get_yaxis().set_ticks([])
                plt.gca().get_xaxis().set_ticks([])
                # plt.show()
            iterSNR.append(10 * math.log10(signal_rms / noise_rms))

            SNR.append(
                [
                    pickFile,
                    pickSta,
                    pickPhase,
                    pickComp,
                    pickLoc,
                    signal_rms / noise_rms,
                    10 * math.log10(signal_rms / noise_rms),
                ]
            )

    print(
        pickFile
        + " Mean SNR: "
        + str(np.nanmean(np.array(iterSNR)))
        + " Max: "
        + str(np.nanmax(np.array(iterSNR)))
        + " Min: "
        + str(np.nanmin(np.array(iterSNR)))
    )
    if doplot:
        plt.subplots_adjust(
            top=0.925, bottom=0.075, left=0.075, right=0.925, wspace=0.1, hspace=0.0
        )
        plt.suptitle(
            pickFile
            + "\nMean SNR (dB): "
            + str(np.nanmean(np.array(iterSNR)))
            + "  Max: "
            + str(np.nanmax(np.array(iterSNR)))
            + "  Min: "
            + str(np.nanmin(np.array(iterSNR))),
            fontsize=12,
        )
        plt.show()

np.savetxt(
    outputFile,
    SNR,
    delimiter=",",
    fmt="%s",
    header="pickFile,pickSta,pickPhase,pickComp,pickLoc,SNR,SNR_db",
)
