import pyproj as pr
from obspy import read_inventory

from NocMeta.Meta import NOC_META

latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
inv = read_inventory(r"Station_xml/FBK_Full.xml")
esm06 = inv.select(station="ESM06", channel="DP*")

# well_head_easting, well_head_northing = pr.transform(latlon_proj, out_proj,esm06[0][0].longitude,esm06[0][0].latitude)
bottom_hole_easting, bottom_hole_northing = (
    198237.30,
    8933907.27,
)  # values from spreadsheet
well_head_elevation = esm06[0][0].elevation
bottom_hole_elevation = well_head_elevation - 798.54
bottom_hole_longitude, bottom_hole_latitude = pr.transform(
    out_proj, latlon_proj, bottom_hole_easting, bottom_hole_northing
)

print(bottom_hole_longitude, bottom_hole_latitude, bottom_hole_elevation)
