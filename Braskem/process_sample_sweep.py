import matplotlib.pyplot as plt
import numpy as np

from NocMeta.Meta import NOC_META
from obspy import read, Stream, Trace, UTCDateTime
from obspy.core.trace import Stats
from obspy.io.segy.core import _read_segy
from obspy.signal.invsim import corn_freq_2_paz
from scipy.signal import decimate


def createTraceStats(network, n1, o1, d1, component):
    # make a stats file for a seed trace
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


sweep = _read_segy("gravacao_sweep_4_geofones.sgy")

paz_10Hz = corn_freq_2_paz(1.0, damp=0.707)
paz_10Hz["sensitivity"] = 22800 * 6

fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5, figsize=[12, 12])
times = segy_traces[1].times()
times -= times[0]
tr1 = sweep[43].simulate(paz_remove=paz_10Hz).data
tr2 = sweep[44].simulate(paz_remove=paz_10Hz).data
tr3 = sweep[45].simulate(paz_remove=paz_10Hz).data
tr4 = sweep[47].simulate(paz_remove=paz_10Hz).data
ax1.plot(times, segy_traces[1])
ax2.plot(times, tr1)
ax3.plot(times, tr2)
ax4.plot(times, tr3)
ax5.plot(times, tr4)

ax1.set_ylabel("input sweep\ncounts")
for i_ax, ax in enumerate([ax2, ax3, ax4, ax5]):
    ax.set_ylabel(f"z. off. geophone {i_ax+1}\nvelocity (m/s)")

ax5.set_xlabel("time (s)")

for ax in [ax1, ax2, ax3, ax4]:
    ax.set_xticklabels([])

for ax in [ax1, ax2, ax3, ax4, ax5]:
    ax.set_xlim([0, 20])
fig.savefig("zero_offset_sweeps.png")
