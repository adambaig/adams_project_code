from glob import glob
import json
import os
import requests

import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import numpy as np
from obspy import read_inventory, read, UTCDateTime
import pyproj as pr
from scipy.optimize import minimize
from scipy.fftpack import fft, fftfreq

from NocMeta.Meta import NOC_META
from sms_moment_tensor.MT_math import mt_vector_to_matrix
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge, trend_plunge_to_unit_vector
from generalPlots import gray_background_with_grid

from read_inputs import read_velocity_model

velocity_model = read_velocity_model()

D2R = np.pi / 180.0
tab10 = get_cmap("tab10")
relative_weighting = 1
x1, x2 = 197986.58688787775, 198854.75025504341
y1, y2 = 8932712.230823414, 8934324.638987586

dx = 1.1 * (x2 - x1)
dy = 1.1 * (y2 - y1)
dz = 1300

x1p = (x1 + x2) / 2 - 0.5 * dx
x2p = (x1 + x2) / 2 + 0.5 * dx
y1p = (y1 + y2) / 2 - 0.5 * dy
y2p = (y1 + y2) / 2 + 0.5 * dy
z1 = -1200
z2 = 100

inv = read_inventory(r"Station_xml/FBK_Full.xml")
channel.response
for channel in inv.select(network='BR', station='ESM09', location='')[0][0]:
    if channel.response:
        print(channel.response.instrument_sensitivity.input_units)

channel.response.

latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
stations = {}
for net in inv:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(latlon_proj, out_proj,
                                           station.longitude, station.latitude)
                stations[f"{net.code}.{station.code}."] = {
                    "abs z": -channel.depth, "abs e": east, "abs n": north}

avg_east, avg_north, avg_elevation = np.average(
    [(v["abs e"], v["abs n"], v["abs z"]) for v in stations.values()], axis=0
)

for station in stations.values():
    station["e"] = station["abs e"] - avg_east
    station["n"] = station["abs n"] - avg_north
    station["z"] = station["abs z"] - avg_elevation

velocity_model = [
    {"rho": 2100, "vp": 1510.0, "vs": 1510.0 / 2.03},
    {"rho": 2200, "vp": 2940.0, "vs": 2940.0 / 2.03, "top": -320},
    {"rho": 2200, "vp": 3610.0, "vs": 3610.0 / 2.03, "top": -420},
    {"rho": 2400, "vp": 4100.0, "vs": 4100.0 / 2.03, "top": -920},
]
Q = {"P": 60, "S": 60}
pick_color = {"P": "cyan", "S": "green"}
min_buffer, max_buffer = 0.1, 0.5


def vertical_slowness_at_sensor(raypath):
    up_plus_minus = np.sign(raypath["velocity_model_chunk"][-1]["h"])
    abs_slowness = 1.0 / raypath["velocity_model_chunk"][-1]["v"]
    hrz_slowness_sq = sum([v * v for v in raypath["hrz_slowness"].values()])
    return up_plus_minus * np.sqrt(abs_slowness ** 2 - hrz_slowness_sq)


def getChannelPath(channel):
    return {
        "locationCode": channel["location_code"].replace("-", ""),
        "channelCode": channel["channel_code"],
        "stationCode": channel["station"]["station_code"],
        "networkCode": channel["station"]["network"]["network_code"],
    }


def packageArrivals(arrival):
    pick = arrival["pick"]
    return {
        "eventAzimuth": arrival["event_azimuth"],
        "eventDistance": arrival["event_distance"],
        "time": pick["time"] - arrival["time_residual"],  # I *hope* this is correct
        "pick": {"phase": pick["phase"], "time": pick["time"], "channelPath": getChannelPath(pick["channel"])},
    }


def get_starting_enzt(picks):
    enz = np.zeros(3)
    i_pick = 0
    t_start = []
    for station_phase in picks:
        network, station, location, phase = station_phase.split(".")
        if phase == "P" or phase == "S":
            i_pick += 1
            nsl = ".".join([network, station, location])
            enz += np.array([stations[nsl]["e"], stations[nsl]["n"], stations[nsl]["z"]])
    enz /= i_pick
    start_loc = {"e": enz[0], "n": enz[1], "z": enz[2]}
    for station, pick in picks.items():
        network, station, location, phase = station_phase.split(".")
        if phase == "P" or phase == "S":
            nsl = ".".join([network, station, location])
            raypath = isotropic_ray_trace(start_loc, stations[nsl], velocity_model, phase)
            t_start.append(pick - raypath["traveltime"])
    return [*enz, np.average(t_start)]


def rescale_picks(picks):
    min_pick = min([v for v in picks.values()])
    rescaled_picks = {}
    for station, phase_pick in picks.items():
        rescaled_picks[station] = phase_pick - min_pick
    return rescaled_picks


pick_files = glob(r"High_quality_events_backup\picks\*.picks")
seed_dir = r"High_quality_events_backup\seeds"
feature_files = glob("waveform_features//*.json")
figure_dir = "waveforms_and_hodogram_figures"
scale = 100


def tt_hodo_residual(xyzt, relative_weighting, picks, hodograms, velocity_model):
    xyz = {"e": xyzt[0], "n": xyzt[1], "z": xyzt[2]}
    residual_tt, residual_hodogram = 0, 0
    for station_phase, pick in picks.items():
        phase = station_phase.split(".")[-1]
        if phase in ["P", "S"]:
            station = stations[".".join(station_phase.split(".")[:3])]
            t_xyz = isotropic_ray_trace(xyz, station, velocity_model, phase)["traveltime"]
        residual_tt += (t_xyz - pick + xyzt[3]) ** 2
    for station, hodogram in hodograms.items():
        if hodogram:
            p_raypath = isotropic_ray_trace(xyz, stations[station], velocity_model, phase)
            hrz_slowness = p_raypath["hrz_slowness"]
            slowness = {**hrz_slowness, "z": vertical_slowness_at_sensor(p_raypath)}
            unit_slowness = np.array([slowness["e"], slowness["n"], slowness["z"]])
            unit_slowness /= np.linalg.norm(unit_slowness)
            unit_hodogram = trend_plunge_to_unit_vector(hodogram["trend"], hodogram["plunge"])
            residual_hodogram += 1 - abs(np.dot(unit_slowness, unit_hodogram))
    return residual_tt + relative_weighting * residual_hodogram


def get_tab10_color(num_string):
    return tab10(float(num_string[-1]) / 10 + 0.01)


# minimizer = minimize(
#     tt_hodo_residual,
#     get_starting_enzt(rescale_picks(picks)),
#     args=(1, rescale_picks(picks), features["hodograms"], velocity_model),
# )

event_file = feature_files[17]

event_id = os.path.basename(event_file).split("_")[0]
response = requests.get(
    rf"https://{NOC_META['FBK']['athIP']}/events/{event_id}.json?expand=all&apiKey={NOC_META['FBK']['athApi']}"
)
event = json.loads(response.content.decode("utf-8"))
preferred_origin = [origin for origin in event["origins"]
                    if origin["id"] == event["preferred_origin_id"]][0]
east, north = pr.transform(latlon_proj, out_proj,
                           preferred_origin["longitude"], preferred_origin["latitude"])
elevation = -1000 * preferred_origin["depth"]
with open(event_file) as f:
    features = json.load(f)
pick_file = [file for file in pick_files if os.path.basename(file).split("_")[
    0] == event_id.zfill(10)][0]
with open(pick_file) as f:
    pick_lines = f.readlines()
picks = {}
for line in pick_lines:
    station, phase, string_pick = line.split(",")
    picks[f"{station}.{phase}"] = float(string_pick)


e_start, n_start, z_start, t_start = get_starting_enzt(rescale_picks(picks))

grid_spacing = 30
easts = np.arange(
    min([v["e"] for v in stations.values()]) - grid_spacing,
    max([v["e"] for v in stations.values()]) + grid_spacing,
    grid_spacing,
)
norths = np.arange(
    min([v["n"] for v in stations.values()]) - grid_spacing,
    max([v["n"] for v in stations.values()]) + grid_spacing,
    grid_spacing,
)

elevations = np.arange(-1000, -200, grid_spacing)


fig = plt.figure(figsize=[14, 14])
ax_depth1 = fig.add_axes([0.05, 0.05, 0.9 * dx / (dx + dy), 0.9 * dz / (dy + dz)])
ax_depth2 = fig.add_axes([0.05 + 0.9 * dx / (dx + dy), 0.05, 0.9 *
                          dy / (dx + dy), 0.9 * dz / (dy + dz)])
ax_plan = fig.add_axes([0.05, 0.05 + 0.9 * dz / (dz + dy), 0.9 *
                        dx / (dx + dy), 0.9 * dy / (dz + dy)])

ax_waveforms = fig.add_axes([0.4, 0.5, 0.55, 0.45])

ax_plan.set_aspect("equal")
ax_depth2.set_aspect("equal")
ax_depth1.set_aspect("equal")

for station_name, station in stations.items():
    ax_plan.plot(
        station["abs e"], station["abs n"], "v", color=get_tab10_color(station_name[-2]), markeredgecolor="0.2"
    )
    ax_depth1.plot(
        station["abs e"], station["abs z"], "v", color=get_tab10_color(station_name[-2]), markeredgecolor="0.2"
    )
    ax_depth2.plot(
        station["abs n"], station["abs z"], "v", color=get_tab10_color(station_name[-2]), markeredgecolor="0.2"
    )
ax_plan.plot(east, north, "o", color="orangered", markeredgecolor="k")
ax_depth1.plot(east, elevation, "o", color="orangered", markeredgecolor="k")
ax_depth2.plot(north, elevation, "o", color="orangered", markeredgecolor="k")


tt_residual_en = np.zeros([len(easts), len(norths)])
tt_residual_ez = np.zeros([len(easts), len(elevations)])
tt_residual_nz = np.zeros([len(norths), len(elevations)])
for i_east, east_1 in enumerate(easts):
    for i_north, north_1 in enumerate(norths):
        tt_residual_en[i_east, i_north] = tt_hodo_residual(
            [east_1, north_1, elevation - avg_elevation, t_start],
            relative_weighting,
            rescale_picks(picks),
            features["hodograms"],
            velocity_model,
        )
for i_east, east_1 in enumerate(easts):
    for i_elevation, elevation_1 in enumerate(elevations):
        tt_residual_ez[i_east, i_elevation] = tt_hodo_residual(
            [east_1, north - avg_north, elevation_1 - avg_elevation, t_start],
            relative_weighting,
            rescale_picks(picks),
            features["hodograms"],
            velocity_model,
        )

for i_north, north_1 in enumerate(norths):
    for i_elevation, elevation_1 in enumerate(elevations):
        tt_residual_nz[i_north, i_elevation] = tt_hodo_residual(
            [east - avg_east, north_1, elevation_1 - avg_elevation, t_start],
            relative_weighting,
            rescale_picks(picks),
            features["hodograms"],
            velocity_model,
        )

vmax = max([np.amax(tt_residual_ez), np.amax(tt_residual_en), np.amax(tt_residual_nz)])
vmin = min([np.amin(tt_residual_ez), np.amin(tt_residual_en), np.amin(tt_residual_nz)])

ax_plan.pcolor(
    easts + avg_east,
    norths + avg_north,
    tt_residual_en.T,
    cmap="nipy_spectral",
    zorder=-3,
    alpha=0.5,
    vmin=vmin,
    vmax=vmax,
)
ax_depth1.pcolor(
    easts + avg_east, elevations, tt_residual_ez.T, cmap="nipy_spectral", zorder=-3, alpha=0.5, vmin=vmin, vmax=vmax
)
ax_depth2.pcolor(
    norths + avg_north, elevations, tt_residual_nz.T, cmap="nipy_spectral", zorder=-3, alpha=0.5, vmin=vmin, vmax=vmax
)

ax_plan.set_xlim(x1p, x2p)
ax_plan.set_ylim(y1p, y2p)
ax_depth1.set_xlim(x1p, x2p)
ax_depth1.set_ylim(z1, z2)
ax_depth2.set_xlim(y1p, y2p)
ax_depth2.set_ylim(z1, z2)

for station, hodogram in features["hodograms"].items():
    if hodogram:
        trend, plunge = hodogram["trend"], hodogram["plunge"]

        ax_plan.plot(
            [
                stations[station]["abs e"] + scale * np.cos(D2R * plunge) * np.sin(D2R * trend),
                stations[station]["abs e"] - scale * np.cos(D2R * plunge) * np.sin(D2R * trend),
            ],
            [
                stations[station]["abs n"] + scale * np.cos(D2R * plunge) * np.cos(D2R * trend),
                stations[station]["abs n"] - scale * np.cos(D2R * plunge) * np.cos(D2R * trend),
            ],
            zorder=-2,
            color=get_tab10_color(station[-2]),
        )
        ax_depth1.plot(
            [
                stations[station]["abs e"] + scale * np.cos(D2R * plunge) * np.sin(D2R * trend),
                stations[station]["abs e"] - scale * np.cos(D2R * plunge) * np.sin(D2R * trend),
            ],
            [
                stations[station]["abs z"] - scale * np.sin(D2R * plunge),
                stations[station]["abs z"] + scale * np.sin(D2R * plunge),
            ],
            zorder=-2,
            color=get_tab10_color(station[-2]),
        )

        ax_depth2.plot(
            [
                stations[station]["abs n"] + scale * np.cos(D2R * plunge) * np.cos(D2R * trend),
                stations[station]["abs n"] - scale * np.cos(D2R * plunge) * np.cos(D2R * trend),
            ],
            [
                stations[station]["abs z"] - scale * np.sin(D2R * plunge),
                stations[station]["abs z"] + scale * np.sin(D2R * plunge),
            ],
            zorder=-2,
            color=get_tab10_color(station[-2]),
        )

for ax in ax_plan, ax_depth1, ax_depth2:
    ax = gray_background_with_grid(ax, grid_spacing=50)
date_str = str(UTCDateTime(preferred_origin["time"])).split("T")[0]
ax_waveforms.set_ylim(0, 11)
ax_waveforms.set_title(f"{event_id} {date_str}")
ax_waveforms.set_facecolor("0.1")
picks = [packageArrivals(v) for v in preferred_origin["associations"]]
pick_times = [p["pick"]["time"] for p in picks]
theoretical_times = [p["time"] for p in picks]
max_pick = max([*pick_times, *theoretical_times])
min_pick = min([*pick_times, *theoretical_times])

stream = read(os.path.join(seed_dir, f"{event_id}.seed")).detrend().filter("highpass", freq=10)
stream.rotate("->ZNE", inventory=inv)
stream.trim(starttime=UTCDateTime(min_pick - min_buffer),
            endtime=UTCDateTime(max_pick + max_buffer))
for station in stations:
    station_name = station.split(".")[1]
    station_int = int(station_name[3:])
    stream_3c = stream.select(station=station_name)
    if len(stream_3c) == 3:
        z_comp = stream_3c.select(channel="??Z")[0].data
        e_comp = stream_3c.select(channel="??E")[0].data
        n_comp = stream_3c.select(channel="??N")[0].data
        norm = 2 * max([max(abs(z_comp)), max(abs(e_comp)), max(abs(n_comp)), ])
        time_axis = stream_3c[0].times() + min_pick - min_buffer
        ax_waveforms.plot(time_axis, station_int + z_comp / norm, color="0.9", alpha=0.8)
        ax_waveforms.plot(time_axis, station_int + e_comp / norm, color="deepskyblue", alpha=0.8)
        ax_waveforms.plot(time_axis, station_int + n_comp / norm, color="orange", alpha=0.8)
        station_picks = [pick for pick in picks if pick["pick"]
                         ["channelPath"]["stationCode"] == station_name]

        for pick in station_picks:
            phase = pick["pick"]["phase"]
            ax_waveforms.plot(
                [pick["time"], pick["time"]], [station_int - 0.5, station_int + 0.5], ":", color=pick_color[phase]
            )
            ax_waveforms.plot(
                [pick["pick"]["time"], pick["pick"]["time"]],
                [station_int - 0.5, station_int + 0.5],
                color=pick_color[phase],
            )
ax_waveforms.set_xlim(min_pick - min_buffer, max_pick + max_buffer)
ax_waveforms.set_yticks(range(1, 11))
[ax_waveforms.get_yticklabels()[i - 1].set_color(get_tab10_color(str(i))) for i in range(1, 11)]
xticks = ax_waveforms.get_xticks()
ax_waveforms.set_xticklabels([str(UTCDateTime(t)).split("T")[1][:-6] for t in xticks])
ax_waveforms.set_xlabel("UTC time")


fig.savefig(os.path.join(figure_dir, f"{event_id}.png"))
