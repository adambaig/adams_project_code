from datetime import datetime, timedelta
import os
import time

import numpy as np
from obspy import UTCDateTime
from obspy.clients.fdsn import Client

client = Client("http://35.155.92.119:8081")

out_dir = r"InSite_Testing\continuous_data"

start_time = UTCDateTime(2021, 8, 4, 0)
end_time = UTCDateTime(2021, 8, 5, 0)
interval_seconds = 600


for interval_start in np.arange(start_time, end_time, interval_seconds):
    st = client.get_waveforms(
        "BR", "ESM??", "*", "HH?,GP?", interval_start, interval_start + interval_seconds
    )
    out_stream = st.select(channel="??[1,2,Z,E,N]")
    datetime_start = datetime.strptime(str(interval_start), "%Y-%m-%dT%H:%M:%S.%fZ")
    datetime_end = datetime_start + timedelta(0, interval_seconds)
    out_seed_name = f"{datetime.strftime(datetime_start, '%Y%m%d_%H%M%S')}.mseed"
    out_stream.write(os.path.join(out_dir, out_seed_name))
    time.sleep(interval_seconds / 2)
    # os.remove(os.path.join(out_dir, out_seed_name))
