import matplotlib.pyplot as plt
from obspy import read


def normed(signal):
    return signal / abs(max(signal))


st = read("event_2020-07-05T00_24_41.670Z.seed")
st.plot()


st.select(channel="??Z", station="ESM0[2,3]")

fig, ax = plt.subplots(figsize=[12, 8])
ax.plot(normed(st[0][5000:7500]) + 1)
st[0].data *= -1
ax.plot(normed(st[0][5000:7500]) - 1)
