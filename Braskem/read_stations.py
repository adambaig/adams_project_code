from FileTypeSpecific.StationXml.AutoStationXml import formStaXml
from NocMeta.Meta import NOC_META

athena_code = "FBK"
inv = formStaXml(outDir="Station_xml///.", athenaCode=athena_code)


for net in inv:
    for station in net:
        print(station.code, station.latitude, station.longitude, station.elevation)
check = inv.select(station="ESM06", channel="DP?")[0][0]
for c in check:
    print(c.azimuth)
