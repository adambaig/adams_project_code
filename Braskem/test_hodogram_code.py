import matplotlib

matplotlib.use("Qt5Agg")

from datetime import datetime
import glob
from IPython.display import display
from ipywidgets import interact
import ipywidgets as widgets
from matplotlib.widgets import Button
from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory, UTCDateTime
import os

from read_inputs import read_catalog, dh_station_locations

PI = np.pi
R2D = 180.0 / PI
D2R = 1 / R2D
P_PICK_LABEL, S_PICK_LABEL, ROTATION_WINDOW_LABEL = (
    "P pick",
    "S pick",
    "rotation\nwindow",
)

pick_dir = "picks//January_15_2021"
seed_dir = "mseeds//Jan_12"
events = glob.glob(f"{pick_dir}{os.sep}*.picks")
seeds = glob.glob(f"{seed_dir}{os.sep}*.seed")


catalog = read_catalog("Catalogs//events_dec23_24.csv")
stations = dh_station_locations()
inv = read_inventory(r"Station_xml/FBK_Full.xml")

test = inv.select(station="ESM09", channel="GPZ")
test[0][0][0]
for network in inv:
    for station in network:
        print(f"{network}.{station}")

waveform_seeds = {}
for seed in seeds:
    basename = os.path.basename(seed).split(".seed")[0]
    start, end = [datetime.strptime(s, "%Y%m%d.%H%M%S.%f") for s in basename.split("_")]
    waveform_seeds[seed] = {"start": start, "end": end}


def find_seed(event_time):
    for seed, waveform in waveform_seeds.items():
        if event_time > waveform["start"] and event_time < waveform["end"]:
            return seed
    return None


def unit_vector_to_trend_plunge(u):
    if u[2] > 0:
        u = -u
    trend = np.arctan2(u[0], u[1]) * R2D
    plunge = np.arctan(-u[2] / np.sqrt(u[0] * u[0] + u[1] * u[1])) * R2D
    return [trend % 360, plunge]


def calc_hodogram(wave_12z):
    wave_mat = np.array([wave - np.mean(wave) for wave in wave_12z])
    eig_vals, eig_vecs = np.linalg.eig(wave_mat @ wave_mat.T)
    i_sort = np.argsort(eig_vals)
    linearity = 1 - eig_vals[i_sort[1]] / eig_vals[i_sort[2]]
    trend, plunge = unit_vector_to_trend_plunge(eig_vecs[:, i_sort[2]])
    wave_mat_2d = np.array([wave - np.mean(wave) for wave in wave_12z[:-1]])
    eig_vals_2d, eig_vecs_2d = np.linalg.eig(wave_mat_2d @ wave_mat_2d.T)
    i_sort_2d = np.argsort(eig_vals_2d)
    linearity_2d = 1 - eig_vals_2d[i_sort_2d[0]] / eig_vals_2d[i_sort_2d[1]]
    trend_2d, _ = unit_vector_to_trend_plunge([*eig_vecs_2d[:, i_sort_2d[1]], 0])
    return {
        "trend": float(trend % 360),
        "plunge": float(plunge),
        "linearity": linearity,
        "2D trend": trend_2d % 360,
        "2D linearity": linearity_2d,
    }


selected_event = events[0]
f = open(selected_event)
picks = {
    f'{l.split(",")[0]}_{l.split(",")[1]}': float(l.split(",")[2])
    for l in f.readlines()
    if l.split(",")[1] in ["P", "S"]
}
f.close()
event_id = os.path.basename(selected_event).split("_")[0].lstrip("0")
event_time = datetime.strptime(
    os.path.basename(selected_event).split("_")[1].split(".picks")[0],
    "%Y%m%d.%H%M%S.%f",
)
stream = (
    read(find_seed(event_time))
    .detrend(type="linear")
    .taper(type="hann", max_percentage=None, max_length=0.2, side="both")
    .filter(type="highpass", freq=10, zerophase="True")
)
stream.rotate("->ZNE", inventory=inv)
# found_post = posts.find_one({"event time": event_time, "event id": event_id})
selected_station = "BR.ESM08"
st = stream.select(station=selected_station.split(".")[1]).copy()

times = st[0].times()
times -= times[0]
z_comp = st.select(channel="??Z")[0].data
e_comp = st.select(channel="??E")[0].data
n_comp = st.select(channel="??N")[0].data
comp_1, comp_2, comp_3 = e_comp, n_comp, z_comp
f = open(selected_event)
picks = {
    f'{l.split(",")[0]}_{l.split(",")[1]}': float(l.split(",")[2])
    for l in f.readlines()
    if l.split(",")[1] in ["P", "S"]
}
f.close()
p_pick = [
    UTCDateTime(t) for p, t in picks.items() if selected_station in p and p[-1] == "P"
]
s_pick = [
    UTCDateTime(t) for p, t in picks.items() if selected_station in p and p[-1] == "S"
]
i_p_pick = np.argmin(abs(st[0].times("utcdatetime") - p_pick)) if p_pick else 0
i_s_pick = np.argmin(abs(st[0].times("utcdatetime") - s_pick)) if s_pick else 0
i_length = 20
i_start = max(i_p_pick, i_length)


i_end = i_start + i_length
fig = plt.figure(figsize=[9, 4.5])
ax_rotate = fig.add_axes([0.63, 0.1, 0.1, 0.075])
ax_axis_reset = fig.add_axes([0.75, 0.1, 0.1, 0.075])
ax_pick_mode = fig.add_axes([0.87, 0.1, 0.1, 0.075])
ax_trace = fig.add_axes([0.1, 0.3, 0.5, 0.6])
ax_mini_trace = fig.add_axes([0.1, 0.1, 0.5, 0.15])
ax_en_hodo = fig.add_axes([0.57, 0.6, 0.3, 0.3])
ax_ze_hodo = fig.add_axes([0.57, 0.3, 0.3, 0.3])
ax_zn_hodo = fig.add_axes([0.72, 0.3, 0.3, 0.3])
(comp_3,) = ax_trace.plot(times, z_comp, "0.2")
(comp_1,) = ax_trace.plot(times, e_comp, "firebrick")
(comp_2,) = ax_trace.plot(times, n_comp, "royalblue")

ax_trace.set_xlim(times[i_start - 2 * i_length], times[i_end + 2 * i_length])
ch_e = st.select(channel="??E")[0].data
ch_n = st.select(channel="??N")[0].data
ch_z = st.select(channel="??Z")[0].data

ylim_u = 1.05 * max(abs(np.hstack([ch_e, ch_n, ch_z])))
ax_trace.set_ylim([-ylim_u, ylim_u])
(p_pick_tick,) = ax_trace.plot(
    [times[i_p_pick], times[i_p_pick]],
    [0.05, 0.95],
    "k",
    zorder=2,
    transform=ax_trace.get_xaxis_transform(),
)
(s_pick_tick,) = ax_trace.plot(
    [times[i_s_pick], times[i_s_pick]],
    [0.05, 0.95],
    "springgreen",
    zorder=2,
    transform=ax_trace.get_xaxis_transform(),
)
if not p_pick:
    p_pick_tick.set_visible(False)
if not s_pick:
    s_pick_tick.set_visible(False)
ax_trace.set_xlim([times[i_start - 2 * i_length], times[i_end + 2 * i_length]])
hodogram = calc_hodogram(
    [e_comp[i_start:i_end], n_comp[i_start:i_end], z_comp[i_start:i_end]]
)
(comp_3_mini,) = ax_mini_trace.plot(times, z_comp, "0.2")
(comp_1_mini,) = ax_mini_trace.plot(times, e_comp, "firebrick")
(comp_2_mini,) = ax_mini_trace.plot(times, n_comp, "royalblue")
y1, y2 = ax_trace.set_ylim()
window = Rectangle(
    (times[i_start], y1),
    times[i_end - 1] - times[i_start],
    y2 - y1,
    facecolor="lightblue",
    alpha=0.2,
    zorder=2,
    edgecolor="k",
)
ax_mini_trace.add_artist(window)
y1, y2 = ax_mini_trace.get_ylim()
window2 = Rectangle(
    (times[i_start], y1),
    times[i_end - 1] - times[i_start],
    y2 - y1,
    facecolor="lightblue",
    alpha=0.2,
    zorder=2,
    edgecolor="k",
)

trace_window = ax_trace.add_artist(window2)
(en_hodo,) = ax_en_hodo.plot(e_comp[i_start:i_end], n_comp[i_start:i_end], color="0.3")
ax_en_hodo.set_ylabel("northing", color="royalblue")
(ze_hodo,) = ax_ze_hodo.plot(e_comp[i_start:i_end], z_comp[i_start:i_end], color="0.3")
ax_ze_hodo.set_ylabel("comp z", color="0.2")
ax_ze_hodo.set_xlabel("easting", color="firebrick")
(zn_hodo,) = ax_zn_hodo.plot(n_comp[i_start:i_end], z_comp[i_start:i_end], color="0.3")
ax_zn_hodo.set_xlabel("nothing", color="royalblue")
max_hodo = 1.5 * max(
    [
        abs(max(z_comp[i_start:i_end])),
        abs(max(e_comp[i_start:i_end])),
        abs(max(n_comp[i_start:i_end])),
    ]
)
max_trace = 1.05 * max([abs(max(z_comp)), abs(max(e_comp)), abs(max(n_comp))])
for ax in [ax_en_hodo, ax_ze_hodo, ax_zn_hodo]:
    ax.set_aspect("equal")
    ax.set_xticks([])
    ax.set_yticks([])
    ax.plot([0, 0], [-max_hodo, max_hodo], ":", c="0.1", zorder=-1)
    ax.plot([-max_hodo, max_hodo], [0, 0], ":", c="0.1", zorder=-1)
    ax.set_xlim([-max_hodo, max_hodo])
    ax.set_ylim([-max_hodo, max_hodo])
trend_text = fig.text(0.8, 0.85, f'trend: {hodogram["trend"]:.1f}$\degree$')
plunge_text = fig.text(0.8, 0.81, f'plunge: {hodogram["plunge"]:.1f}$\degree$')
linearity_text = fig.text(0.8, 0.77, f'linearity: {hodogram["linearity"]:.3f}')
trend_2d_text = fig.text(0.8, 0.73, f'2D trend: {hodogram["2D trend"]:.1f}$\degree$')
linearity_2d_text = fig.text(0.8, 0.69, f'2D linearity: {hodogram["2D linearity"]:.3f}')
# hodogram = found_post[selected_station.replace(".", "_")]["hodogram"]

hodogram = calc_hodogram(
    np.vstack([ch_e[i_start:i_end], ch_n[i_start:i_end], ch_z[i_start:i_end]])
)


class Rotate:
    ind = 0

    def call(self, event):
        azimuth, colatitude = D2R * hodogram["trend"], PI / 2 + D2R * hodogram["plunge"]
        print(hodogram["trend"])
        rotation_matrix = np.array(
            [
                [
                    np.sin(colatitude) * np.sin(azimuth),
                    np.sin(colatitude) * np.cos(azimuth),
                    np.cos(colatitude),
                ],
                [
                    np.cos(colatitude) * np.sin(azimuth),
                    np.cos(colatitude) * np.cos(azimuth),
                    -np.sin(colatitude),
                ],
                [-np.cos(azimuth), np.sin(azimuth), 0],
            ]
        )
        ch_e = st.select(channel="??E")[0].data
        ch_n = st.select(channel="??N")[0].data
        ch_z = st.select(channel="??Z")[0].data
        data_3c = np.vstack([ch_e, ch_n, ch_z])
        rotated_data = rotation_matrix @ data_3c
        ylim_r = 1.05 * np.amax(
            abs(rotated_data[:, i_start - 2 * i_length : i_end + 2 * i_length])
        )
        ylim_u = 1.05 * max(abs(np.hstack([ch_e, ch_n, ch_z])))
        if self.ind % 2 == 0:
            comp_1.set_ydata(rotated_data[0, :])
            comp_2.set_ydata(rotated_data[1, :])
            comp_3.set_ydata(rotated_data[2, :])
            comp_1.set_color("forestgreen")
            comp_2.set_color("mediumorchid")
            ax_trace.set_ylim([-ylim_r, ylim_r])
            comp_1_mini.set_ydata(rotated_data[0, :])
            comp_2_mini.set_ydata(rotated_data[1, :])
            comp_3_mini.set_ydata(rotated_data[2, :])
            comp_1_mini.set_color("forestgreen")
            comp_2_mini.set_color("mediumorchid")
            ax_trace.set_ylim([-ylim_r, ylim_r])
            b_rotate.label.set_text("Unrotate")
        else:
            comp_1.set_ydata(ch_e)
            comp_2.set_ydata(ch_n)
            comp_3.set_ydata(ch_z)
            comp_1.set_color("firebrick")
            comp_2.set_color("royalblue")
            comp_1_mini.set_ydata(ch_e)
            comp_2_mini.set_ydata(ch_n)
            comp_3_mini.set_ydata(ch_z)
            comp_1_mini.set_color("firebrick")
            comp_2_mini.set_color("royalblue")
            ax_trace.set_ylim([-ylim_r, ylim_r])
            b_rotate.label.set_text("Rotate")
        plt.draw()
        self.ind += 1


class PickMode:
    # the behaviour of the figure should be completely dictated by
    # what the text in this button is set to
    ind = 0

    def call(self, event):
        if self.ind % 3 == 0:
            b_pick_mode.label.set_text(S_PICK_LABEL)
        elif self.ind % 3 == 1:
            b_pick_mode.label.set_text(ROTATION_WINDOW_LABEL)
        else:
            b_pick_mode.label.set_text(P_PICK_LABEL)
        self.ind += 1
        plt.draw()


class Set_Axis:
    def call(self, event):
        window_time, _ = window2.get_xy()
        window_width = window2.get_width()
        i_win_1 = np.argmin(abs(times - window_time))
        i_win_2 = np.argmin(abs(times - window_time - window_width))
        n_width = i_win_2 - i_win_1
        i_start = max(i_win_1 - 2 * n_width, 0)
        i_end = min(i_win_2 + 2 * n_width, len(e_comp) - 1)

        ax_trace.set_xlim(times[i_start], times[i_end])


b_rotate = Button(ax_rotate, "Rotate")
b_rotate.on_clicked(Rotate().call)
b_pick_mode = Button(ax_pick_mode, P_PICK_LABEL)
b_pick_mode.on_clicked(PickMode().call)
b_axis_reset = Button(ax_axis_reset, "Set Axis")
b_axis_reset.on_clicked(Set_Axis().call)


def onclick(event):
    if event.inaxes == ax_trace:
        if b_pick_mode.label.get_text() == P_PICK_LABEL:
            p_pick_tick.set_data([event.xdata, event.xdata], [0.05, 0.95])
            p_pick_tick.set_visible(True)
        if b_pick_mode.label.get_text() == S_PICK_LABEL:
            s_pick_tick.set_data([event.xdata, event.xdata], [0.05, 0.95])
            s_pick_tick.set_visible(True)
        if b_pick_mode.label.get_text() == ROTATION_WINDOW_LABEL:
            window_x, window_y = window2.get_xy()
            window_width = window2.get_width()
            if event.xdata < window_x:
                window2.set_xy((event.xdata, window_y))
                window2.set_width(window_width + window_x - event.xdata)
            elif event.xdata > window_x + window_width:
                window2.set_width(event.xdata - window_x)
            else:
                if event.button == 1:
                    window2.set_xy((event.xdata, window_y))
                    window2.set_width(window_width + window_x - event.xdata)
                elif event.button == 3:
                    window2.set_width(event.xdata - window_x)
            window_time, _ = window2.get_xy()
            window_width = window2.get_width()
            i_win_1 = np.argmin(abs(times - window_time))
            i_win_2 = np.argmin(abs(times - window_time - window_width))
            en_hodo.set_xdata(e_comp[i_win_1:i_win_2])
            en_hodo.set_ydata(n_comp[i_win_1:i_win_2])
            ze_hodo.set_xdata(e_comp[i_win_1:i_win_2])
            ze_hodo.set_ydata(z_comp[i_win_1:i_win_2])
            zn_hodo.set_xdata(n_comp[i_win_1:i_win_2])
            zn_hodo.set_ydata(z_comp[i_win_1:i_win_2])
            n_width = i_win_2 - i_win_1
            i_start = max(i_win_1 - 2 * n_width, 0)
            i_end = min(i_win_2 + 2 * n_width, len(e_comp) - 1)
            # ax_trace.set_xlim(times[i_start], times[i_end])
            hodogram = calc_hodogram(
                np.vstack(
                    [
                        e_comp[i_win_1:i_win_2],
                        n_comp[i_win_1:i_win_2],
                        z_comp[i_win_1:i_win_2],
                    ]
                )
            )
            trend_text.set_text(f'trend: {hodogram["trend"]:.1f}$\degree$')
            plunge_text.set_text(f'plunge: {hodogram["plunge"]:.1f}$\degree$')
            linearity_text.set_text(f'linearity: {hodogram["linearity"]:.3f}')
            trend_2d_text.set_text(f'2D trend: {hodogram["2D trend"]:.1f}$\degree$')
            linearity_2d_text.set_text(f'2D linearity: {hodogram["2D linearity"]:.3f}')
    plt.draw()


def onpress(event):
    if event.key.lower() == "w":
        b_pick_mode.label.set_text(ROTATION_WINDOW_LABEL)
        PickMode.ind = 2
    if event.key.lower() == "p":
        b_pick_mode.label.set_text(P_PICK_LABEL)
        PickMode.ind = 0
    if event.key.lower() == "s":
        b_pick_mode.label.set_text(S_PICK_LABEL)
        PickMode.ind = 1
    if event.key.lower() == "1":
        b_pick_mode.label.set_text(P_PICK_LABEL)
        PickMode.ind = 0
    if event.key.lower() == "2":
        b_pick_mode.label.set_text(S_PICK_LABEL)
        PickMode.ind = 1
    plt.draw()


cid_click = fig.canvas.mpl_connect("button_press_event", onclick)
cid_press = fig.canvas.mpl_connect("key_press_event", onpress)


plt.show()
