from obspy import read

from obspy.io.segy.core import _read_segy

st = _read_segy("testsweep//sweep_20s.sgy")


st
st.plot()
st[0].plot()

st[0].spectrogram()

import matplotlib.pyplot as plt
import numpy as np

traces = read("testsweep//FILE_EXAMPLE.sgy")

traces[0]


traces[0].plot()
from obspy.imaging.spectrogram import spectrogram

data = st[0].data

st[0].plot()

spectrogram(data[:5000], 2000)


spectrogram(data[35000:], 2000)
plt.plot(data[:5000])

plt.plot(data[5000:10000])

plt.plot(data[10000:15000])
plt.plot(data[15000:20000])
plt.plot(data[15000:20000])
plt.plot(np.correlate(traces[0].data, sweep, "full")[7401:8000])


fig, ax = plt.subplots(figsize=[10, 4])
ax.plot(4 + sweep / max(abs(sweep)))
ax.plot(2 + traces[0].data / max(abs(traces[0].data)))
decon_trace = np.correlate(traces[0].data, sweep, "full")[7500:]
ax.plot(decon_trace / max(abs(decon_trace)))
fig.savefig("example.png")
