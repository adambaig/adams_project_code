import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory
import os
import pyproj as pr
from scipy.optimize import minimize
from scipy.fftpack import fft, fftfreq

from NocMeta.Meta import NOC_META
from sms_moment_tensor.MT_math import mt_vector_to_matrix
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from sms_moment_tensor.MT_math import (
    unit_vector_to_trend_plunge,
    trend_plunge_to_unit_vector,
)
from generalPlots import gray_background_with_grid

from read_inputs import read_velocity_model

velocity_model = read_velocity_model()


def calc_hodogram(wave_12z):
    wave_mat = np.array([wave - np.mean(wave) for wave in wave_12z])
    eig_vals, eig_vecs = np.linalg.eig(wave_mat @ wave_mat.T)
    i_sort = np.argsort(eig_vals)
    linearity = 1 - eig_vals[i_sort[1]] / eig_vals[i_sort[2]]
    trend, plunge = unit_vector_to_trend_plunge(eig_vecs[:, i_sort[2]])
    wave_mat_2d = np.array([wave - np.mean(wave) for wave in wave_12z[:-1]])
    eig_vals_2d, eig_vecs_2d = np.linalg.eig(wave_mat_2d @ wave_mat_2d.T)
    i_sort_2d = np.argsort(eig_vals_2d)
    linearity_2d = 1 - eig_vals_2d[i_sort_2d[0]] / eig_vals_2d[i_sort_2d[1]]
    trend_2d, _ = unit_vector_to_trend_plunge([*eig_vecs_2d[:, i_sort_2d[1]], 0])
    return {
        "trend": float(trend % 360),
        "plunge": float(plunge),
        "linearity": linearity,
        "2D trend": trend_2d % 360,
        "2D linearity": linearity_2d,
    }


inv = read_inventory(r"Station_xml/FBK_Full.xml")
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
stations = {}
for net in inv:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(
                    latlon_proj, out_proj, station.longitude, station.latitude
                )
                stations[f"{net.code}.{station.code}"] = {
                    "z": -channel.depth,
                    "e": east,
                    "n": north,
                }

east, north = np.average([(v["e"], v["n"]) for v in stations.values()], axis=0)

velocity_model = [
    {"rho": 2100, "vp": 1510.0, "vs": 1510.0 / 2.03},
    {"rho": 2200, "vp": 2940.0, "vs": 2940.0 / 2.03, "top": -320},
    {"rho": 2200, "vp": 3610.0, "vs": 3610.0 / 2.03, "top": -420},
    {"rho": 2400, "vp": 4100.0, "vs": 4100.0 / 2.03, "top": -920},
]
Q = {"P": 60, "S": 60}

picks = {}
source = {
    "e": east + 40,
    "n": north - 19,
    "z": -866,
    "stress_drop": 1e7,
    "moment_magnitude": -1.0,
    "moment_tensor": mt_vector_to_matrix([0, 0, 0, 0, 0, 1]),
}

sample_rate = 250
time_series = np.arange(0, 4.0, 1.0 / sample_rate)
noise_level = 2e-8
threshold = 1e-7
for i_rec, (rec_id, receiver) in enumerate(stations.items()):
    p_raypath, s_raypath = [
        isotropic_ray_trace(source, receiver, velocity_model, phase)
        for phase in ["P", "S"]
    ]
    waveform = get_response(
        p_raypath, s_raypath, source, receiver, Q, time_series, noise_level
    )
    waveform_norm = max([max(abs(waveform[c])) for c in ["n", "e", "z"]])
    i_p = int(p_raypath["traveltime"] // (1 / sample_rate))
    i_s = int(s_raypath["traveltime"] // (1 / sample_rate))

    p_max = max(
        abs(np.hstack([waveform[c][i_p - 5 : i_p + 5] for c in ["e", "n", "z"]]))
    )
    s_max = max(
        abs(np.hstack([waveform[c][i_s - 5 : i_s + 5] for c in ["e", "n", "z"]]))
    )
    if p_max > threshold:
        e_max = np.argmax(waveform["e"][i_p - 5 : i_p + 5]) + i_p - 5
        n_max = np.argmax(waveform["n"][i_p - 5 : i_p + 5]) + i_p - 5
        z_max = np.argmax(waveform["z"][i_p - 5 : i_p + 5]) + i_p - 5
        i_pick = min([e_max, n_max, z_max])
        picks[rec_id + ".P"] = i_pick / sample_rate
        picks[rec_id + ".P.hodo"] = calc_hodogram(
            [
                waveform["e"][i_pick - 3 : i_pick + 7],
                waveform["n"][i_pick - 3 : i_pick + 7],
                waveform["z"][i_pick - 3 : i_pick + 7],
            ]
        )
    if s_max > threshold:
        e_max = np.argmax(waveform["e"][i_s - 5 : i_s + 5]) + i_s - 5
        n_max = np.argmax(waveform["n"][i_s - 5 : i_s + 5]) + i_s - 5
        z_max = np.argmax(waveform["z"][i_s - 5 : i_s + 5]) + i_s - 5
        picks[rec_id + ".S"] = min([e_max, n_max, z_max]) / sample_rate


def vertical_slowness_at_sensor(raypath):
    up_plus_minus = np.sign(raypath["velocity_model_chunk"][-1]["h"])
    abs_slowness = 1.0 / raypath["velocity_model_chunk"][-1]["v"]
    hrz_slowness_sq = sum([v * v for v in raypath["hrz_slowness"].values()])
    return up_plus_minus * np.sqrt(abs_slowness**2 - hrz_slowness_sq)


unique_stations = np.unique([".".join(k.split(".")[:2]) for k in picks])
starting_location = np.average(
    [(stations[v]["e"], stations[v]["n"], stations[v]["z"]) for v in unique_stations],
    axis=0,
)
t_starts = []
for s in unique_stations:
    p_pick, s_pick = s + ".P", s + ".S"
    if s + ".P" in picks.keys() and s + ".S" in picks.keys():
        t_starts.append(2 * picks[p_pick] - picks[s_pick])

wrong_velocity_model = [
    {"rho": 2100, "vp": 2000.0, "vs": 2000.0 / 1.87},
    {"rho": 2200, "vp": 2300, "vs": 2300 / 2.03, "top": -320},
    {"rho": 2200, "vp": 3400, "vs": 3400 / 1.87, "top": -420},
    {"rho": 2400, "vp": 4100.0, "vs": 4100.0 / 2.03, "top": -920},
]


def tt_residual(xyzt, picks, velocity_model):
    xyz = {"e": xyzt[0], "n": xyzt[1], "z": xyzt[2]}
    residual = 0
    for stationphase, pick in picks.items():
        station = stations[".".join(stationphase.split(".")[:2])]
        phase = stationphase.split(".")[-1]
        if phase in ["P", "S"]:
            t_xyz = isotropic_ray_trace(xyz, station, velocity_model, phase)[
                "traveltime"
            ]
            residual += (t_xyz - pick + xyzt[3]) ** 2
    return residual


def tt_hodo_residual(xyzt, relative_weighting, picks, velocity_model):
    xyz = {"e": xyzt[0], "n": xyzt[1], "z": xyzt[2]}
    residual_tt, residual_hodogram = 0, 0
    for stationphase, pick in {
        k: v for k, v in picks.items() if ".hodo" not in k
    }.items():
        station = stations[".".join(stationphase.split(".")[:2])]
        phase = stationphase.split(".")[-1]
        t_xyz = isotropic_ray_trace(xyz, station, velocity_model, phase)["traveltime"]
        residual_tt += (t_xyz - pick + xyzt[3]) ** 2
    for station, hodogram in {
        ".".join(k.split(".")[:2]): v for k, v in picks.items() if ".hodo" in k
    }.items():
        p_raypath = isotropic_ray_trace(xyz, stations[station], velocity_model, phase)
        hrz_slowness = p_raypath["hrz_slowness"]
        slowness = {**hrz_slowness, "z": vertical_slowness_at_sensor(p_raypath)}
        unit_slowness = np.array([slowness["e"], slowness["n"], slowness["z"]])
        unit_slowness /= np.linalg.norm(unit_slowness)
        unit_hodogram = trend_plunge_to_unit_vector(
            hodogram["trend"], hodogram["plunge"]
        )
        residual_hodogram += 1 - abs(np.dot(unit_slowness, unit_hodogram))
    return residual_tt + relative_weighting * residual_hodogram


minimizer = minimize(
    tt_hodo_residual,
    [*starting_location, np.average(t_starts)],
    args=(0.01, picks, velocity_model),
)
wrong_minimizer = minimize(
    tt_residual,
    [*starting_location, np.average(t_starts)],
    args=(picks, wrong_velocity_model),
)

ax_depth.set_aspect("equal")
for station in stations.values():
    ax_depth.plot(station["e"], station["z"], "v", color="0.8", markeredgecolor="0.2")
ax_depth.plot(source["e"], source["z"], "o", color="orangered", markeredgecolor="0.2")

ax_depth2.set_aspect("equal")
for station in stations.values():
    ax_depth2.plot(station["n"], station["z"], "v", color="0.8", markeredgecolor="0.2")
ax_depth2.plot(source["n"], source["z"], "o", color="orangered", markeredgecolor="0.2")
scale = 200
uv_easting, uv_northing, uv_elevation = [], [], []
for station, hodogram in {
    ".".join(k.split(".")[:2]): v for k, v in picks.items() if ".hodo" in k
}.items():
    unit_vector = trend_plunge_to_unit_vector(hodogram["trend"], hodogram["plunge"])
    uv_easting.append(scale * unit_vector[0])
    uv_northing.append(scale * unit_vector[1])
    uv_elevation.append(scale * unit_vector[2])
    p_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "P")
    hrz_slowness_sq = sum([p_raypath["hrz_slowness"][c] ** 2 for c in ["e", "n"]])
    vertexes = [source]
    ray_east = source["e"]
    ray_north = source["n"]
    ray_elevation = source["z"]
    for layer in p_raypath["velocity_model_chunk"]:
        segment_time = (
            abs(layer["h"])
            / layer["v"]
            / np.sqrt(1 - layer["v"] ** 2 * hrz_slowness_sq)
        )
        ray_east += segment_time * p_raypath["hrz_slowness"]["e"] * layer["v"] ** 2
        ray_north += segment_time * p_raypath["hrz_slowness"]["n"] * layer["v"] ** 2
        ray_elevation += layer["h"]
        vertexes.append({"e": ray_east, "n": ray_north, "z": ray_elevation})
    ax_depth.plot(
        [v["e"] for v in vertexes],
        [v["z"] for v in vertexes],
        lw=0.5,
        color="firebrick",
        zorder=-2,
    )
    ax_depth2.plot(
        [v["n"] for v in vertexes],
        [v["z"] for v in vertexes],
        lw=0.5,
        color="firebrick",
        zorder=-2,
    )

ax.quiver(
    [v["e"] for v in stations.values()],
    [v["n"] for v in stations.values()],
    uv_easting,
    uv_northing,
    headlength=0,
    headaxislength=0,
    headwidth=0,
    pivot="middle",
)
ax_depth.quiver(
    [v["e"] for v in stations.values()],
    [v["z"] for v in stations.values()],
    uv_easting,
    uv_elevation,
    headlength=0,
    headaxislength=0,
    headwidth=0,
    pivot="middle",
)
ax_depth2.quiver(
    [v["n"] for v in stations.values()],
    [v["z"] for v in stations.values()],
    uv_northing,
    uv_elevation,
    headlength=0,
    headaxislength=0,
    headwidth=0,
    pivot="middle",
)


ax = gray_background_with_grid(ax, grid_spacing=100)
ax_depth = gray_background_with_grid(ax_depth, grid_spacing=100)
ax_depth2 = gray_background_with_grid(ax_depth2, grid_spacing=100)
