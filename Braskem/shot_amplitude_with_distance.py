import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory
import os
import pyproj as pr

from NocMeta.Meta import NOC_META

from read_sweeps import read_input_sweep

latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
inv = read_inventory(r"Station_xml/FBK_Full.xml")
surface_stations = ["ESM02", "ESM03", "ESM05", "ESM07"]
sweep_locations = {
    "VB1": {
        "latitude": -9 - 38 / 60.0 - 25.560 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 44.944 / 3600.0,
        "elevation": 6,
    },
    "VB2": {
        "latitude": -9 - 38 / 60.0 - 7.308 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 59.252 / 3600.0,
        "elevation": 7,
    },
    "VB3": {
        "latitude": -9 - 37 / 60.0 - 53.020 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 45.273 / 3600.0,
        "elevation": 57,
    },
}
for sweep, location in sweep_locations.items():
    east, north = pr.transform(
        latlon_proj, out_proj, location["longitude"], location["latitude"]
    )
    location["e"], location["n"] = east, north
data_dir = r"RecordedSweepsAndPicks\SweepSamples\SweepSamples\VB?"
data_seeds = glob.glob(f"{data_dir}{os.sep}*.mseed")
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
stations = {}


def calc_epi_distance(point1, point2):
    distance_sq = sum([(point1[c] - point2[c]) ** 2 for c in ["e", "n"]])
    return np.sqrt(distance_sq)


def distance_from_key(key):
    shot, station = key.split("_")
    return calc_epi_distance(stations[f"BR.{station}"], sweep_locations[shot])


for net in inv:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(
                    latlon_proj, out_proj, station.longitude, station.latitude
                )
                stations[f"{net.code}.{station.code}"] = {
                    "z": -channel.depth,
                    "e": east,
                    "n": north,
                }

sweep = read_input_sweep()

autocorr = np.correlate(sweep, sweep)

seeds = glob.glob("decon_seeds//*.mseed")

shot_amps = {
    "VB1": {"ESM02": [], "ESM03": [], "ESM05": [], "ESM07": []},
    "VB2": {"ESM02": [], "ESM03": [], "ESM05": [], "ESM07": []},
    "VB3": {"ESM02": [], "ESM03": [], "ESM05": [], "ESM07": []},
}

colors = {"VB1": "firebrick", "VB2": "royalblue", "VB3": "gold"}
markers = {"ESM02": "o", "ESM03": "s", "ESM05": "d", "ESM07": "^"}

for seed in seeds:
    st = read(seed)
    shot = seed.split("\\")[1].split("_")[0]

    for station in ["ESM02", "ESM03", "ESM05", "ESM07"]:
        three_c = st.select(station=station)
        comp_z = three_c.select(channel="*Z")[0].data
        times = three_c[0].times("matplotlib")
        times -= times[0]
        try:
            comp_1 = three_c.select(channel="*1")[0].data
        except:
            comp_1 = three_c.select(channel="*E")[0].data
        try:
            comp_2 = three_c.select(channel="*2")[0].data
        except:
            comp_2 = three_c.select(channel="*Z")[0].data
        i_zmax = np.argmax(comp_z)
        shot_amps[shot][station].append(abs(comp_z[i_zmax]))

power = {}
for shot in shot_amps.keys():
    for station, samples in shot_amps[shot].items():
        power[f"{shot}_{station}"] = np.percentile(samples, 90) / autocorr

np.array(shot_amps["VB3"]["ESM07"]) / autocorr

power["VB3_ESM07"]
fig, ax = plt.subplots()
ax.set_facecolor("0.9")
for key, p in power.items():
    shot, station = key.split("_")
    ax.loglog(
        distance_from_key(key),
        p,
        "o",
        color=colors[shot],
        marker=markers[station],
        markeredgecolor="0.1",
    )
