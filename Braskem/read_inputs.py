import numpy as np
from obspy import read_inventory
import pyproj as pr


from NocMeta.Meta import NOC_META

# from sms_moment_tensor.MT_math import trend_plunge_to_unit_vector

latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")


def read_velocity_model():
    f = open("velocity//NewVelocityProfile_average.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    for i_line, line in enumerate(lines):
        lspl = line.split(",")
        if i_line == 0:
            velocity_model = [
                {"vp": 1000 * float(lspl[1]), "vs": 1000 * float(lspl[2])}
            ]
        else:
            velocity_model.append(
                {
                    "top": -1000 * float(lspl[0]),
                    "vp": 1000 * float(lspl[1]),
                    "vs": 1000 * float(lspl[2]),
                }
            )
    for layer in velocity_model:
        layer["rho"] = 310 * layer["vp"] ** 0.25
    return velocity_model


def read_old_velocity_model():
    f = open("velocity//VelocityProfile_average.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    for i_line, line in enumerate(lines[:14]):
        lspl = line.split(",")
        if i_line == 0:
            velocity_model = [
                {"vp": 1000 * float(lspl[1]), "vs": 1000 * float(lspl[2])}
            ]
        else:
            velocity_model.append(
                {
                    "top": -1000 * float(lspl[0]),
                    "vp": 1000 * float(lspl[1]),
                    "vs": 1000 * float(lspl[2]),
                }
            )
    for layer in velocity_model:
        layer["rho"] = 310 * layer["vp"] ** 0.25
    return velocity_model


def read_catalog(filepath):
    f = open(filepath)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        athena_id = lspl[1]
        axis_lengths = [float(v) * 1000 for v in lspl[20:27:3]]
        axis_1 = trend_plunge_to_unit_vector(*[float(v) for v in lspl[21:23]])
        axis_2 = trend_plunge_to_unit_vector(*[float(v) for v in lspl[24:26]])
        axis_3 = trend_plunge_to_unit_vector(*[float(v) for v in lspl[27:29]])
        orthonormal = np.array([axis_1.T, axis_2.T, axis_3.T])
        ellipsoid = orthonormal @ np.diag(axis_lengths) @ orthonormal.T
        events[athena_id] = {
            "external id": lspl[2],
            "datetime": lspl[3],
            "latitude": float(lspl[6]),
            "longitude": float(lspl[7]),
            "local magnitude": float(lspl[10]),
            "depth_km": float(lspl[12]),
            "athena database id": lspl[19],
            "analysis type": lspl[17],
            "depth error": float(lspl[13]),
            "n_stations": int(lspl[14]),
            "n_phases": int(lspl[15]),
            "ellipsoid": ellipsoid,
        }
        east, north = pr.transform(
            latlon_proj,
            out_proj,
            events[athena_id]["longitude"],
            events[athena_id]["latitude"],
        )
        events[athena_id] = {
            **events[athena_id],
            "easting": east,
            "northing": north,
            "depth": events[athena_id]["depth_km"] * 1000.0,
        }
    return events


def dh_station_locations():
    inv = read_inventory(r"Station_xml/FBK_Full.xml")
    stations = {}
    for net in inv:
        for station in net:
            channel = inv.select(station=station.code, channel="?PZ")
            if len(channel) == 0:
                channel = inv.select(station=station.code, channel="HHZ")
            east, north = pr.transform(
                latlon_proj, out_proj, station.longitude, station.latitude
            )
            stations[f"{net.code}.{station.code}."] = {
                "z": station.elevation - channel[0][0][0].depth,
                "e": east,
                "n": north,
            }
    return stations
