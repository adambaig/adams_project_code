import pandas as pd
import numpy as np
import sys

#%%
catalog = pd.read_csv(r"Braskem_class_a.csv")
catalog.loc[:, "Mw"] = catalog["Mw Estimated from Ross et al. (Mw = 0.754*Ml + 0.88)"]
Mbin = 0.1
Mmax = 10
minNGR = 10

Mmax_real = np.max(catalog.Mw)
iMmax_real = [nmm for nmm, iimm in enumerate(catalog.Mw) if iimm == Mmax_real][0]

catalog.sort_values(by="athena database id", inplace=True)
catalog.reset_index(drop=True, inplace=True)
mmin = np.min(catalog.Mw)
mbins = np.arange(mmin - Mbin - Mbin / 2.0, np.max(catalog.Mw) + Mbin + Mbin, Mbin)

[Nm, mBin] = np.histogram(catalog.Mw, bins=mbins)
mBin = mBin + Mbin / 2.0 * np.ones([len(mBin)])
n = list(Nm)
n.reverse()
mcum = list(np.cumsum(n))
mcum.reverse()

mfbin = []
mmin = int([nmi for nmi, iinm in enumerate(Nm) if iinm == max(Nm)][0]) + 1

for ib in range(len(mBin)):
    mfbin.append(float(" %.1f" % mBin[ib]))
mfbin = np.array(mfbin)

if Mmax == 10:
    immax = int(
        [
            nmi
            for nmi, iinm in enumerate(mfbin)
            if iinm == float(" %.1f" % np.max(catalog.Mw))
        ][0]
    )
if Mmax != 10:
    x = [nmi for nmi, iinm in enumerate(mfbin) if iinm == float(" %.1f" % Mmax)]
    if len(x) == 0:
        sys.exit(f"Select Mmax from: {mfbin}")
    immax = int(
        [nmi for nmi, iinm in enumerate(mfbin) if iinm == float(" %.1f" % Mmax)]
    )

mBinfit = mBin[mmin : immax + 1]
mcumfit = mcum[mmin : immax + 1]
mBinfit = list(mBinfit)

dup = []
lendup = min(len(mBinfit), len(mcumfit)) - 1
for ir in range(lendup):
    if mcumfit[ir] == mcumfit[ir + 1]:
        dup.append(ir)

dup.reverse()

for idup in dup:
    mcumfit.remove(mcumfit[idup])
    mBinfit.remove(mBinfit[idup])

scenarios = [1, 2]
z = np.array([0, 0])


# scenario1: Regular b_value calculation using all magnitude bins larger than Mc
if len(mBinfit) >= 2:

    z, cov = np.polyfit(mBinfit, np.log10(mcumfit), 1, cov=True)
    coeff_errors = np.sqrt(np.diag(cov))

    if len(np.where(~np.isfinite(np.log10(mcumfit)))[0]) != 0:
        idel = np.where(~np.isfinite(np.log10(mcumfit)))[0][0]
        del mBinfit[idel]
        del mcumfit[idel]
        z, cov = np.polyfit(mBinfit, np.log10(mcumfit), 1, cov=True)
        coeff_errors = np.sqrt(np.diag(cov))

    scenario1_Z0 = z[0]
    scenario1_Z1 = z[1]
    coeff_errors_scenario1_Z0 = coeff_errors[0]
    coeff_errors_scenario1_Z1 = coeff_errors[1]


if len(mBinfit) < 2:
    sys.exit(
        "Cannot fit a line to less than 2 data points! please increase the number of events (MINIMUM_EVENTS_TO_COMPUTE)!"
    )

scenario1_mye = scenario1_Z1 + scenario1_Z0 * mBin
scenario1_a_value = 10**scenario1_Z1

# scenario2: b_value calculation using all magnitude bins larger than Mc, where there is minimum observations in magnitude bins

lessthan3 = []
tempmcumfit, tempmBinfit = [], []
for imcumfit in range(len(mcumfit)):
    tempmcumfit.append(mcumfit[imcumfit])
for imBinfit in range(len(mBinfit)):
    tempmBinfit.append(mBinfit[imBinfit])
lendup = min(len(mBinfit), len(tempmcumfit))
for ir in range(lendup):
    if tempmcumfit[ir] < minNGR + 1:
        lessthan3.append(ir)

lessthan3.reverse()

for ilessthan3 in lessthan3:
    tempmcumfit.remove(tempmcumfit[ilessthan3])
    tempmBinfit.remove(tempmBinfit[ilessthan3])

if len(tempmBinfit) >= 2:
    z, cov = np.polyfit(tempmBinfit, np.log10(tempmcumfit), 1, cov=True)
    coeff_errors = np.sqrt(np.diag(cov))

    if len(np.where(~np.isfinite(np.log10(tempmcumfit)))[0]) != 0:
        idel = np.where(~np.isfinite(np.log10(tempmcumfit)))[0][0]
        del tempmBinfit[idel]
        del tempmcumfit[idel]
        z, cov = np.polyfit(tempmBinfit, np.log10(tempmcumfit), 1, cov=True)
        coeff_errors = np.sqrt(np.diag(cov))

    scenario2_Z0 = z[0]
    scenario2_Z1 = z[1]
    coeff_errors_scenario2_Z0 = coeff_errors[0]
    coeff_errors_scenario2_Z1 = coeff_errors[1]

scenario2_mye = scenario2_Z1 + scenario2_Z0 * mBin
scenario2_a_value = 10**scenario2_Z1

Mw_Mc = float(mbins[[nmi for nmi, iinm in enumerate(Nm) if iinm == max(Nm)][-1]])
Ml_Mc = (Mw_Mc - 0.88) / 0.754

print(coeff_errors_scenario1_Z0, coeff_errors_scenario1_Z1)
print(coeff_errors_scenario2_Z0, coeff_errors_scenario2_Z1)
filename = "Mc_output"
TXT_GR = open(filename.split(".csv")[0] + ".txt", "w")
TXT_GR.write(filename + "\n" + "\n")
TXT_GR.write(
    "Mc(Mw): "
    + str(float(mbins[[nmi for nmi, iinm in enumerate(Nm) if iinm == max(Nm)][-1]]))
    + "\n"
)
TXT_GR.write("Mc(Ml equivilant): " + str(Ml_Mc) + " based on Ross et al.\n")
TXT_GR.write("Mmax_observed: " + str(float(np.max(catalog.Mw))) + "\n" + "\n")


TXT_GR.write("b_value_Senario1: " + str(-1 * scenario1_Z0) + "\n")
TXT_GR.write("a_value_Senario1: " + str(10**scenario1_Z1) + "\n")
TXT_GR.write("err_b_value_Senario1: " + str(coeff_errors_scenario1_Z0) + "\n")
TXT_GR.write(
    "err_a_value_Senario1: " + str(10**coeff_errors_scenario1_Z1) + "\n" + "\n"
)

TXT_GR.write("b_value_Senario2: " + str(-1 * scenario2_Z0) + "\n")
TXT_GR.write("a_value_Senario2: " + str(10**scenario2_Z1) + "\n")
TXT_GR.write("err_b_value_Senario2: " + str(coeff_errors_scenario2_Z0) + "\n")
TXT_GR.write("err_a_value_Senario2: " + str(10**coeff_errors_scenario2_Z1) + "\n")

TXT_GR.close()


#%%


import plotly.graph_objs as go
from plotly import tools
import numpy as np
import plotly


def b_value_Prob_Den_plot(
    xNC, yNC, xCum, yCum, xCumUsed, yCumUsed, xLine, yLine, b_val, Mc, NMc, MmaxR
):
    # %% b-value
    trace1 = go.Scatter(
        x=xNC,
        y=yNC,
        hoverinfo="text",
        text=["No of events: " + str(int(i)) for i in yNC],
        mode="markers",
        visible=True,
        showlegend=True,
        legendgroup="group1",
        name="No of events",
        marker=dict(
            size=6,
            color="rgb(160, 160, 160)",
            symbol="square",
            line=dict(
                color="rgb(0,0,0)",
                width=0,
            ),
        ),
    )

    trace2 = go.Scatter(
        x=xCum,
        y=yCum,
        hoverinfo="text",
        text=["No of cumulative events: " + str(int(i)) for i in yCum],
        mode="markers",
        visible=True,
        showlegend=True,
        legendgroup="group1",
        name="No of cumulative events",
        marker=dict(
            size=6,
            color="#66c2a5",
            symbol="square",
            line=dict(
                color="rgb(0,0,0)",
                width=0,
            ),
        ),
    )

    trace3 = go.Scatter(
        x=xCumUsed,
        y=yCumUsed,
        hoverinfo="text",
        text=["No of cumulative events used: " + str(int(i)) for i in yCumUsed],
        mode="markers",
        visible=True,
        showlegend=True,
        legendgroup="group1",
        name="No of cumulative events used",
        marker=dict(
            size=6,
            color="#3288bd",
            symbol="square",
            line=dict(
                color="rgb(0,0,0)",
                width=0,
            ),
        ),
    )

    trace4 = go.Scatter(
        x=xLine,
        y=yLine,
        mode="lines",
        visible=True,
        showlegend=True,
        legendgroup="group1",
        name="b_value",
        text="b_value: %0.2f" % b_val,
        hoverinfo="text",
        line=dict(
            color="#f46d43",
            width=3,
        ),
    )

    # %%
    fig = tools.make_subplots(rows=1, cols=1, print_grid=False)
    fig.append_trace(trace1, 1, 1)
    fig.append_trace(trace2, 1, 1)
    fig.append_trace(trace3, 1, 1)
    fig.append_trace(trace4, 1, 1)

    fig["layout"]["yaxis1"].update(
        title="No of Events",
        titlefont=dict(
            family="Arial, Helvetica, sans-serif", size=12, color="rgb(0,0,0)"
        ),
        tickfont=dict(
            family="Arial, Helvetica, sans-serif", size=12, color="rgb(0,0,0)"
        ),
        mirror=True,
        type="log",
        range=[np.log10(1), np.log10(2 * max(yCum))],
        tick0=1,
        linecolor="#636363",
        linewidth=2,
    )

    fig["layout"]["xaxis1"].update(
        tickfont=dict(
            family="Arial, Helvetica, sans-serif", size=12, color="rgb(0,0,0)"
        ),
        hoverformat=".2f",
        #        range=rangeX,
        dtick=0.5,
        zeroline=False,
        #        tick0=round(rangeX[0], 1),
        linecolor="#636363",
        linewidth=2,
    )
    fig["layout"]["xaxis1"].update(mirror=True)
    #    fig['layout']['xaxis2'].update(title='Magnitude',
    #                                   titlefont=dict(
    #                                       family='Arial, Helvetica, sans-serif',
    #                                       size=12,
    #                                       color='rgb(0,0,0)'
    #                                   ),
    #                                   tickfont=dict(
    #                                       family='Arial, Helvetica, sans-serif',
    #                                       size=12,
    #                                       color='rgb(0,0,0)'
    #                                   ),
    #                                   hoverformat='.2f',
    #                                   mirror=True,
    #                                   range=rangeX,
    #                                   dtick=0.5,
    #                                   zeroline=False,
    #                                   tick0=round(rangeX[0], 1),
    #                                   linecolor='#636363',
    #                                   linewidth=2)
    fig["layout"].update(hovermode="closest")
    fig["layout"]["margin"] = dict(r=10, b=50, l=70, t=30)
    fig["layout"]["paper_bgcolor"] = "white"
    fig["layout"]["plot_bgcolor"] = "white"

    fig["layout"]["legend"] = dict(
        font=dict(family="Arial, Helvetica, sans-serif", size=12, color="rgb(0,0,0)")
    )
    fig["layout"]["legend"]["orientation"] = "h"
    fig["layout"]["legend"]["y"] = -0.2
    fig["layout"]["legend"]["x"] = -0.1
    #    return plotly.plotly.io.write_image(dr+"/fig1.png")
    return plotly.offline.plot(
        fig,
        filename="b_value_plot.html",
        config={
            "showLink": False,
            "displaylogo": False,
            "modeBarButtonsToRemove": ["sendDataToCloud"],
        },
        auto_open=True,
    )


b_value_Prob_Den_plot(
    xNC=mBin[:-1],
    yNC=Nm,
    xCum=mBin[:-1],
    yCum=mcum,
    xCumUsed=mBinfit,
    yCumUsed=mcumfit,
    xLine=mBin,
    yLine=10 ** (scenario1_mye),
    b_val=(-1 * scenario1_Z0),
    Mc=float(mbins[[nmi for nmi, iinm in enumerate(Nm) if iinm == max(Nm)][-1]]),
    NMc=float(mcumfit[0]),
    MmaxR=float(Mmax_real),
)
