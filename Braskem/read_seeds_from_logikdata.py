import glob

from obspy import read, read_inventory

logikdata_dir = r"C:\Users\adambaig\logikdata\FBK\MSEED\Select"
seeds = glob.glob(f"{logikdata_dir}//*.seed")
st = read(seeds[0])
inv = read_inventory(r"C:\Users\adambaig\logikdata\FBK\FBK_Full.xml")
stationTraces = st.select(station=station, channel="??[Z,N,E,1,2]")
channel_orientations = [tr.stats.channel[2] for tr in stationTraces]


stationTraces.rotate("->ZNE", inventory=inv)
