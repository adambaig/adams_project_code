import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory
import os
import pyproj as pr
from scipy.optimize import minimize
from scipy.fftpack import fft, fftfreq

from NocMeta.Meta import NOC_META
from sms_moment_tensor.MT_math import mt_vector_to_matrix
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response

from pfi_fs.make_inputs import read_PPSD

noise_dir = "Noise//PPSD Data"
fmin, fmax = 10, 100
noise_files = glob.glob(f"{noise_dir}//*.csv")
noise = {}
for noise_file in noise_files:
    station_name = noise_file.split(".~.")[0].split("_")[-1]
    if station_name not in noise:
        noise[station_name] = {}
    channel_name = noise_file.split(".~.")[1].split("_")[0]
    noise[station_name][channel_name] = read_PPSD(noise_file, fmin, fmax)

noise["BR.ESM01"] = noise["BR.ESM08"]


inv = read_inventory(r"Station_xml/FBK_Full.xml")
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
stations = {}
for net in inv:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(
                    latlon_proj, out_proj, station.longitude, station.latitude
                )
                stations[f"{net.code}.{station.code}"] = {
                    "z": -channel.depth,
                    "e": east,
                    "n": north,
                    "rms_noise": np.average(
                        [v for v in noise[f"{net.code}.{station.code}"].values()]
                    )
                    * np.sqrt(channel.sample_rate / (fmax - fmin)),
                }


east, north = np.average([(v["e"], v["n"]) for v in stations.values()], axis=0)
east = stations["BR.ESM08"]["e"] + 50
north = stations["BR.ESM08"]["n"] + 50

velocity_model = [
    {"rho": 2100, "vp": 1510.0, "vs": 1510.0 / 2.03},
    {"rho": 2200, "vp": 2940.0, "vs": 2940.0 / 2.03, "top": -320},
    {"rho": 2200, "vp": 3610.0, "vs": 3610.0 / 2.03, "top": -420},
    {"rho": 2400, "vp": 4100.0, "vs": 4100.0 / 2.03, "top": -920},
]
Q = {"P": 60, "S": 60}

threshold = 5e-8
picks = []
for m in np.arange(0, -2, -0.2):
    source = {
        "e": east,
        "n": north,
        "z": -866,
        "stress_drop": 1e6,
        "moment_magnitude": m,
        "moment_tensor": mt_vector_to_matrix([0, 0, 0, 0, 1, 0]),
    }
    sample_rate = 1000
    time_series = np.arange(0, 1, 1.0 / sample_rate)
    fig, ax = plt.subplots(figsize=[16, 9])
    ax.set_facecolor("0.9")
    ytick = []
    for i_rec, (rec_id, receiver) in enumerate(stations.items()):
        p_raypath, s_raypath = [
            isotropic_ray_trace(source, receiver, velocity_model, phase)
            for phase in ["P", "S"]
        ]
        waveform = get_response(
            p_raypath,
            s_raypath,
            source,
            receiver,
            Q,
            time_series,
            receiver["rms_noise"],
        )
        waveform_norm = 1.2 * max([max(abs(waveform[c])) for c in ["n", "e", "z"]])
        ax.plot(time_series, i_rec + waveform["z"] / waveform_norm, color="0.2")
        ax.plot(time_series, i_rec + waveform["n"] / waveform_norm, color="royalblue")
        ax.plot(time_series, i_rec + waveform["e"] / waveform_norm, color="firebrick")
        # y1, y2 = ax.get_ylim()
        # ax.plot([p_raypath["traveltime"], p_raypath["traveltime"]], [y1, y2], "0.2")
        # ax.plot([s_raypath["traveltime"], s_raypath["traveltime"]], [y1, y2], "0.2")
        #

        i_p = int(p_raypath["traveltime"] // (1 / sample_rate))
        i_s = int(s_raypath["traveltime"] // (1 / sample_rate))
        if i_rec == 0:
            s_window = waveform["z"][i_s - 10 : 2 * i_s - i_p - 10]
            freq = fftfreq(len(s_window), 1 / sample_rate)[: len(s_window) // 2]
            ax_s.loglog(
                freq,
                np.abs(
                    fft(s_window)[: len(s_window) // 2]
                    / freq
                    / 2
                    / np.pi
                    / len(s_window)
                ),
            )
        p_max = max(
            abs(np.hstack([waveform[c][i_p - 5 : i_p + 5] for c in ["e", "n", "z"]]))
        )
        s_max = max(
            abs(np.hstack([waveform[c][i_s - 5 : i_s + 5] for c in ["e", "n", "z"]]))
        )
        if p_max > threshold:
            e_max = np.argmax(waveform["e"][i_p - 5 : i_p + 5]) + i_p - 5
            n_max = np.argmax(waveform["n"][i_p - 5 : i_p + 5]) + i_p - 5
            z_max = np.argmax(waveform["z"][i_p - 5 : i_p + 5]) + i_p - 5
            picks.append(
                {
                    rec_id + ".P": min([e_max, n_max, z_max]) / sample_rate,
                }
            )
        if s_max > threshold:
            e_max = np.argmax(waveform["e"][i_s - 5 : i_s + 5]) + i_s - 5
            n_max = np.argmax(waveform["n"][i_s - 5 : i_s + 5]) + i_s - 5
            z_max = np.argmax(waveform["z"][i_s - 5 : i_s + 5]) + i_s - 5
            picks.append(
                {
                    rec_id + ".S": min([e_max, n_max, z_max]) / sample_rate,
                }
            )
        ytick.append(rec_id)
    ax.set_yticks(range(10))
    ax.set_yticklabels(ytick)
    ax.set_xlabel("time (s)")
    ax.set_xlim([0, 1])
    ax.set_title(f'M$_w {source["moment_magnitude"]:.1f}$; Q is {Q["P"]}')
    fig.savefig(f'synthetics{os.sep}M{source["moment_magnitude"]:.1f}Q_{Q["P"]}.png')
