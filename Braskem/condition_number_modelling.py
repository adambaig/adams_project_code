import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory
import pyproj as pr


from sms_moment_tensor.moment_tensor_inversion import inversion_matrix_row
from sms_ray_modelling.raytrace import isotropic_ray_trace
from NocMeta.Meta import NOC_META
from generalPlots import gray_background_with_grid

inv = read_inventory(r"Station_xml/FBK_Full.xml")
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
stations = {}
for net in inv:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(
                    latlon_proj, out_proj, station.longitude, station.latitude
                )
                stations[f"{net.code}.{station.code}"] = {
                    "z": -channel.depth,
                    "e": east,
                    "n": north,
                }

grid_spacing = 30
easts = np.arange(
    min([v["e"] for v in stations.values()]) - grid_spacing,
    max([v["e"] for v in stations.values()]) + grid_spacing,
    grid_spacing,
)
norths = np.arange(
    min([v["n"] for v in stations.values()]) - grid_spacing,
    max([v["n"] for v in stations.values()]) + grid_spacing,
    grid_spacing,
)
elevations = np.arange(-1000, -200, grid_spacing)

east, north = np.average([(v["e"], v["n"]) for v in stations.values()], axis=0)
east = stations["BR.ESM08"]["e"] + 50
north = stations["BR.ESM08"]["n"] + 50
velocity_model = [
    {"rho": 2100, "vp": 1510.0, "vs": 1510.0 / 2.03},
    {"rho": 2200, "vp": 2940.0, "vs": 2940.0 / 2.03, "top": -320},
    {"rho": 2200, "vp": 3610.0, "vs": 3610.0 / 2.03, "top": -420},
    {"rho": 2400, "vp": 4100.0, "vs": 4100.0 / 2.03, "top": -920},
]

dh_stations = {k: v for k, v in stations.items() if v["z"] < -10}
min_stations = 4
min_S_stations = 5

condition_numbers = np.zeros([len(easts), len(norths), len(elevations)])

for i_east, east in enumerate(easts):
    for i_north, north in enumerate(norths):
        for i_elevation, elevation in enumerate(elevations):
            location = {"e": east, "n": north, "z": elevation}
            p_raypaths, s_raypaths = {}, {}
            for station_name, station in dh_stations.items():
                p_raypaths[station_name] = isotropic_ray_trace(
                    location, station, velocity_model, "P"
                )
                s_raypaths[station_name] = isotropic_ray_trace(
                    location, station, velocity_model, "S"
                )

            s_order = sorted(
                dh_stations.keys(),
                key=lambda station: s_raypaths[station]["geometrical_spreading"],
            )
            p_order = sorted(
                dh_stations.keys(),
                key=lambda station: s_raypaths[station]["geometrical_spreading"],
            )

            A_matrix = []
            for raypath in [
                v for k, v in s_raypaths.items() if k in p_order[:min_stations]
            ]:
                A_matrix.append(inversion_matrix_row(raypath, "P"))
            for raypath in [
                v for k, v in s_raypaths.items() if k in s_order[:min_S_stations]
            ]:
                A_matrix.append(inversion_matrix_row(raypath, "V"))
                A_matrix.append(inversion_matrix_row(raypath, "H"))

            condition_number = np.linalg.cond(np.squeeze(np.array(A_matrix)))

            if np.isnan(condition_number):
                condition_numbers[i_east, i_north, i_elevation] = 1e6
            else:
                condition_numbers[i_east, i_north, i_elevation] = condition_number


fig, ax = plt.subplots(figsize=[9, 9])
ax.set_aspect("equal")
pcolor_object = ax.pcolor(easts, norths, condition_numbers[:, :, 20].T, vmin=1, vmax=10)
ax.plot(
    [v["e"] for v in dh_stations.values()], [v["n"] for v in dh_stations.values()], "kv"
)
for station_name, station in dh_stations.items():
    ax.text(station["e"], station["n"] + 30, station_name[-2:])
x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()
ax.set_xlim(x1 - 2 * grid_spacing, x2 + 2 * grid_spacing)
ax.set_ylim(y1 - 2 * grid_spacing, y2 + 2 * grid_spacing)
ax = gray_background_with_grid(ax, grid_spacing=100)
fig.savefig("CN_5stations_perfect_depth400.png")
