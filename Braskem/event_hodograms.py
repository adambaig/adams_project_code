from datetime import datetime
import glob
import matplotlib.pyplot as pltno
from matplotlib.patches import Rectangle
import numpy as np
from obspy import read, read_inventory, UTCDateTime
import os
from read_inputs import read_catalog, dh_station_locations

event_catalog = read_catalog("Catalogs//some_more_deeper_events.csv")
stations = dh_station_locations()
inv = read_inventory(r"Station_xml/FBK_Full.xml")
pick_dir = "picks"
seed_dir = "mseeds"
events = glob.glob(f"{pick_dir}{os.sep}*.picks")
seeds = glob.glob(f"{seed_dir}{os.sep}*.seed")
R2D = 180.0 / np.pi
waveform_seeds = {}
for seed in seeds:
    basename = os.path.basename(seed).split(".seed")[0]
    start, end = [datetime.strptime(s, "%Y%m%d.%H%M%S.%f") for s in basename.split("_")]
    waveform_seeds[seed] = {"start": start, "end": end}


def find_seed(event_time):
    for seed, waveform in waveform_seeds.items():
        if event_time > waveform["start"] and event_time < waveform["end"]:
            return seed
    return None


def unit_vector_to_trend_plunge(u):
    if u[2] > 0:
        u = -u
    trend = np.arctan2(u[0], u[1]) * R2D
    plunge = np.arctan(-u[2] / np.sqrt(u[0] * u[0] + u[1] * u[1])) * R2D
    return [trend % 360, plunge]


def calc_hodogram(wave_12z):
    wave_mat = np.array([wave - np.mean(wave) for wave in wave_12z])
    eig_vals, eig_vecs = np.linalg.eig(wave_mat @ wave_mat.T)
    i_sort = np.argsort(eig_vals)
    linearity = 1 - eig_vals[i_sort[1]] / eig_vals[i_sort[2]]
    trend, plunge = unit_vector_to_trend_plunge(eig_vecs[:, i_sort[2]])
    wave_mat_2d = np.array([wave - np.mean(wave) for wave in wave_12z[:-1]])
    eig_vals_2d, eig_vecs_2d = np.linalg.eig(wave_mat_2d @ wave_mat_2d.T)
    i_sort_2d = np.argsort(eig_vals_2d)
    linearity_2d = 1 - eig_vals_2d[i_sort_2d[0]] / eig_vals_2d[i_sort_2d[1]]
    trend_2d, _ = unit_vector_to_trend_plunge([*eig_vecs_2d[:, i_sort_2d[1]], 0])
    return {
        "trend": float(trend % 360),
        "plunge": float(plunge),
        "linearity": linearity,
        "2D trend": trend_2d % 360,
        "2D linearity": linearity_2d,
    }


event = events[3]
f = open(event)
picks = {
    f'{l.split(",")[0]}_{l.split(",")[1]}': float(l.split(",")[2])
    for l in f.readlines()
    if l.split(",")[1] in ["P", "S"]
}
f.close()
event_id = os.path.basename(event).split("_")[0]

station = "BR.ESM04"
[p.split(".")[1] for p in picks]
pick = [UTCDateTime(t) for p, t in picks.items() if station in p and p[-1] == "P"]
event_time = datetime.strptime(
    os.path.basename(event).split("_")[1].split(".picks")[0], "%Y%m%d.%H%M%S.%f"
)
stream = (
    read(find_seed(event_time))
    .trim(
        starttime=UTCDateTime(UTCDateTime(event_time)) - 1,
        endtime=UTCDateTime(UTCDateTime(event_time)) + 2,
    )
    .detrend(type="linear")
    .taper(type="hann", max_percentage=None, max_length=0.2, side="both")
)
st = stream.select(station=station.split(".")[1]).copy()
st.rotate("->ZNE", inventory=inv)


times = st[0].times()
times -= times[0]
z_comp = st.select(channel="??Z")[0].data
e_comp = st.select(channel="??E")[0].data
n_comp = st.select(channel="??N")[0].data

i_pick = np.argmin(abs(st[0].times("utcdatetime") - pick))
i_start = i_pick
i_length = 25
i_end = i_start + i_length

fig = plt.figure(figsize=[16, 8])
ax_trace = fig.add_axes([0.1, 0.3, 0.5, 0.6])
ax_mini_trace = fig.add_axes([0.1, 0.1, 0.5, 0.15])
ax_en_hodo = fig.add_axes([0.55, 0.5, 0.3, 0.3])
ax_ze_hodo = fig.add_axes([0.55, 0.2, 0.3, 0.3])
ax_zn_hodo = fig.add_axes([0.7, 0.2, 0.3, 0.3])
ax_trace.plot(
    times[i_start - 2 * i_length : i_end + 2 * i_length],
    z_comp[i_start - 2 * i_length : i_end + 2 * i_length],
    "0.2",
)
ax_trace.plot(
    times[i_start - 2 * i_length : i_end + 2 * i_length],
    e_comp[i_start - 2 * i_length : i_end + 2 * i_length],
    "firebrick",
)
ax_trace.plot(
    times[i_start - 2 * i_length : i_end + 2 * i_length],
    n_comp[i_start - 2 * i_length : i_end + 2 * i_length],
    "royalblue",
)
pick[0] - st[0].times("utcdatetime")[0]
y1, y2 = ax.get_ylim()
ax_trace.plot([times[i_pick], times[i_pick]], [0.9 * y1, 0.9 * y2], "k", zorder=2)
hodogram = calc_hodogram(
    [e_comp[i_start:i_end], n_comp[i_start:i_end], z_comp[i_start:i_end]]
)
ax_mini_trace.plot(times, z_comp, "0.2")
ax_mini_trace.plot(times, e_comp, "firebrick")
ax_mini_trace.plot(times, n_comp, "royalblue")
window = Rectangle(
    (times[i_start], y1),
    times[i_end - 1] - times[i_start],
    y2 - y1,
    facecolor="lightblue",
    alpha=0.2,
    zorder=2,
    edgecolor="k",
)
ax_mini_trace.add_artist(window)
window2 = Rectangle(
    (times[i_start], y1),
    times[i_end - 1] - times[i_start],
    y2 - y1,
    facecolor="lightblue",
    alpha=0.2,
    zorder=2,
    edgecolor="k",
)
ax_trace.add_artist(window2)
ax_en_hodo.plot(e_comp[i_start:i_end], n_comp[i_start:i_end], color="0.3")
ax_en_hodo.set_ylabel("northing", color="royalblue")
ax_ze_hodo.plot(e_comp[i_start:i_end], z_comp[i_start:i_end], color="0.3")
ax_ze_hodo.set_ylabel("comp z", color="0.2")
ax_ze_hodo.set_xlabel("easting", color="firebrick")
ax_zn_hodo.plot(n_comp[i_start:i_end], z_comp[i_start:i_end], color="0.3")
ax_zn_hodo.set_xlabel("nothing", color="royalblue")
max_hodo = 1.05 * max(
    [
        abs(max(z_comp[i_start:i_end])),
        abs(max(e_comp[i_start:i_end])),
        abs(max(n_comp[i_start:i_end])),
    ]
)
max_trace = 1.05 * max([abs(max(z_comp)), abs(max(e_comp)), abs(max(n_comp))])
for ax in [ax_en_hodo, ax_ze_hodo, ax_zn_hodo]:
    ax.set_aspect("equal")
    ax.set_xticks([])
    ax.set_yticks([])
    ax.plot([0, 0], [-max_hodo, max_hodo], ":", c="0.1", zorder=-1)
    ax.plot([-max_hodo, max_hodo], [0, 0], ":", c="0.1", zorder=-1)
    ax.set_xlim([-max_hodo, max_hodo])
    ax.set_ylim([-max_hodo, max_hodo])
fig.text(0.83, 0.75, f'apparent trend: {hodogram["trend"]:.1f}$\degree$')
fig.text(0.83, 0.71, f'plunge: {hodogram["plunge"]:.1f}$\degree$')
fig.text(0.83, 0.67, f'linearity: {hodogram["linearity"]:.3f}')
fig.text(0.83, 0.63, f'apparent 2D trend: {hodogram["2D trend"]:.1f}$\degree$')
fig.text(0.83, 0.59, f'2D linearity: {hodogram["2D linearity"]:.3f}')
