import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory
from scipy.signal import decimate

from read_sweeps import read_input_sweep, read_zero_offset_sweeps

inv = read_inventory(r"Station_xml/FBK_Full.xml")


def normalize(signal):
    return signal / max(abs(signal))


sweep = read_input_sweep()
zero_offset = read_zero_offset_sweeps()
st = read(r"RecordedSweepsAndPicks\SweepSamples\SweepSamples\VB3\2030.mseed")
times = zero_offset["times"]
fig, ax = plt.subplots(figsize=[20, 6])
ax.plot(times, normalize(sweep) + 4)
ax.plot(times, normalize(zero_offset["zero_offset_1"]) + 2)
max(abs(zero_offset["zero_offset_1"]))
trz = st.select(station="ESM03", channel="*z")[0].remove_response(inventory=inv)
trz2 = st.select(station="ESM02", channel="*z")[0].remove_response(inventory=inv)
trz3 = st.select(station="ESM05", channel="*z")[0].remove_response(inventory=inv)
trz4 = st.select(station="ESM07", channel="*z")[0].remove_response(inventory=inv)
dec_sweep = decimate(sweep, 10)
correlation = np.correlate(trz.data, dec_sweep, "full")
i_max = np.argmax(correlation)
i_start = len(trz.data) - i_max
i_adjust = 200
ax.plot(
    times[::10], normalize(trz.data[i_start - i_adjust : i_start + 4001 - i_adjust])
)
correlation2 = np.correlate(trz2.data, dec_sweep, "full")
i_max2 = np.argmax(correlation2)
i_start2 = len(trz2.data) - i_max2
i_adjust2 = 200
ax.plot(
    times[::10],
    normalize(trz2.data[i_start2 - i_adjust2 : i_start2 + 4001 - i_adjust2]) - 2,
)
max(abs(trz2.data))
times = trz.copy().times(type="utcdatetime")

# trz.copy().trim(starttime=times[i_start-i_adjust-150], endtime=times[i_start+4001-i_adjust+150]).spectrogram()


trz.spectrogram()
correlation3 = np.correlate(trz3.data, dec_sweep, "full")
i_max3 = np.argmax(correlation3)
i_start3 = len(trz3.data) - i_max3
i_adjust3 = 200
ax.plot(
    times[::10],
    normalize(trz3.data[i_start3 - i_adjust3 : i_start3 + 4001 - i_adjust3]) - 4,
)
max(abs(trz3.data))

correlation4 = np.correlate(trz4.data, dec_sweep, "full")
i_max4 = np.argmax(correlation4)
i_start4 = len(trz4.data) - i_max4
i_adjust4 = 200
ax.plot(
    times[::10],
    normalize(trz4.data[i_start4 - i_adjust4 : i_start4 + 4001 - i_adjust4]) - 6,
)
max(abs(trz4.data))


ax.set_yticks([-6, -4, -2, 0, 2, 4])
ax.set_yticklabels(
    [
        "ESM07\n$\pm$0.006 mm/s",
        "ESM05\n$\pm$0.016 mm/s",
        "ESM02\n$\pm$0.210 mm/s",
        "ESM03\n$\pm$0.530 mm/s",
        "Near-Offset\n$\pm$ 1.744mm/s",
        "Input",
    ],
    rotation=0,
    va="center",
)
ax.set_xlim([0, 20])
ax.set_xlabel("time (s)")
fig.savefig("compare_sweep_response.png")
#
# f1,a1 = plt.subplots()
# for zo_sweep in [v for k,v in zero_offset.items() if 'zero' in k]:
#     a1.plot(normalize(np.correlate(zo_sweep, sweep ,'full'))[40000:41000])
