import glob
from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
import numpy as np
from obspy import read, read_inventory
import os
import pyproj as pr
from scipy.fftpack import fftfreq, fft

from NocMeta.Meta import NOC_META
from generalPlots import gray_background_with_grid

from read_sweeps import read_zero_offset_sweeps

inv = read_inventory(r"Station_xml/FBK_Full.xml")

latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
surface_stations = ["ESM02", "ESM03", "ESM05", "ESM07"]
downhole_stations = ["ESM01", "ESM04", "ESM06", "ESM08", "ESM09", "ESM10"]
sweep_locations = {
    "VB1": {
        "latitude": -9 - 38 / 60.0 - 25.560 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 44.944 / 3600.0,
        "elevation": 6,
    },
    "VB2": {
        "latitude": -9 - 38 / 60.0 - 7.308 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 59.252 / 3600.0,
        "elevation": 7,
    },
    "VB3": {
        "latitude": -9 - 37 / 60.0 - 53.020 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 45.273 / 3600.0,
        "elevation": 57,
    },
}
for sweep, location in sweep_locations.items():
    east, north = pr.transform(
        latlon_proj, out_proj, location["longitude"], location["latitude"]
    )
    location["e"], location["n"], location["z"] = east, north, location["elevation"]
data_dir = r"RecordedSweepsAndPicks\SweepSamples\SweepSamples\VB?"
data_seeds = glob.glob(f"{data_dir}{os.sep}*.mseed")

stations = {}


def calc_epi_distance(point1, point2):
    distance_sq = sum([(point1[c] - point2[c]) ** 2 for c in ["e", "n"]])
    return np.sqrt(distance_sq)


def calc_hypo_distance(point1, point2):
    distance_sq = sum([(point1[c] - point2[c]) ** 2 for c in ["e", "n", "z"]])
    return np.sqrt(distance_sq)


def distance_from_key(key):
    shot, station, _ = key.split("_")
    return calc_epi_distance(stations[f"BR.{station}"], sweep_locations[shot])


for net in inv:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(
                    latlon_proj, out_proj, station.longitude, station.latitude
                )
                stations[f"{net.code}.{station.code}"] = {
                    "z": -channel.depth,
                    "e": east,
                    "n": north,
                }

n_len = 8000
freq = fftfreq(n_len, 0.005)
responses = {}
for seed in data_seeds:
    st = read(seed)
    shot = seed.split(os.sep)[-2]
    if shot not in responses:
        responses[shot] = {}
        for station in downhole_stations:
            responses[shot][station] = []
    for station in downhole_stations:
        three_c = st.select(station=station)
        trz = three_c.select(station=station, channel="*z")[0].remove_response(
            inventory=inv
        )
        i_one_side = np.where(freq > -0)[0]
        f_response = fft(trz[:n_len])
        responses[shot][station].append(abs(f_response[i_one_side]) / n_len)


sweeps = read_zero_offset_sweeps()


freq_sweep = fftfreq(len(sweeps["zero_offset_1"]), sweeps["times"][1])
i_one_side_sweep = np.where(freq_sweep > -0)[0]
f_sweep = []
for i_sweep, sweep in enumerate([v for k, v in sweeps.items() if "zero" in k]):
    f_sweep.append(abs(fft(sweep))[i_one_side_sweep] / len(sweep))

colors = {"VB1": "firebrick", "VB2": "royalblue", "VB3": "gold"}
fig, ax = {}, {}
for station in downhole_stations:
    fig[station], ax[station] = plt.subplots()
    ax[station].set_facecolor("0.9")
    for shot in ["VB1", "VB2", "VB3"]:
        ax[station].loglog(
            freq[i_one_side],
            np.median(np.array(responses[shot][station]), 0),
            colors[shot],
        )
    # ax[station].loglog(freq_sweep[i_one_side_sweep], np.median(np.array(f_sweep), 0), "0.2")


custom_lines = [
    Line2D([0], [0], color="firebrick"),
    Line2D([0], [0], color="royalblue"),
    Line2D([0], [0], color="gold"),
    Line2D([0], [0], color="0.2"),
]


for station in downhole_stations:
    distance = {}
    for sweep, location in sweep_locations.items():
        distance[sweep] = calc_hypo_distance(location, stations[f"BR.{station}"])
    ax[station].set_xlim([3, 100])
    ax[station].set_title(station)
    ax[station].set_ylabel("velocity spectrum (m)")
    ax[station].set_xlabel("frequency (Hz)")
    ax[station].legend(
        custom_lines,
        [
            f"VB1 {distance['VB1']:.0f}m",
            f"VB2 {distance['VB2']:.0f}m",
            f"VB3 {distance['VB3']:.0f}m",
            # "zero-offset sweep",
        ],
    )
    y1, y2 = ax[station].get_ylim()
    ax[station].loglog([16, 16], [y1, y2], "--", color="0.2")
    ax[station].text(18, 2 * y1, "16 Hz")
    ax[station].set_ylim([y1, y2])
    fig[station].savefig(f"median_vibe_spectrum_{station}.png")
