from datetime import datetime
import glob
import matplotlib.pyplot as plt
from matplotlib import patches
import numpy as np
from obspy import read, UTCDateTime, read_inventory
import os
from pymongo import MongoClient
from scipy.fftpack import fft, fftfreq

from generalPlots import gray_background_with_grid
from read_inputs import read_catalog, dh_station_locations

PI = np.pi
D2R = PI / 180

hodogram_scale = 100


pick_dir = "picks//December_24_2020_manual"
seed_dir = "mseeds//repicked"
events = glob.glob(f"{pick_dir}{os.sep}*.picks")
seeds = glob.glob(f"{seed_dir}{os.sep}*.seed")
catalog = read_catalog("Catalogs//events_dec23_24.csv")
dh_stations = dh_station_locations()
client = MongoClient()
db = client.Braskem_test
posts = db.event_waveforms
athena_ids = [int(v["athena database id"]) for v in catalog.values()]
inv = read_inventory(r"Station_xml/FBK_Full.xml")
waveform_seeds = {}
for seed in seeds:
    basename = os.path.basename(seed).split(".seed")[0]
    start, end = [datetime.strptime(s, "%Y%m%d.%H%M%S.%f") for s in basename.split("_")]
    waveform_seeds[seed] = {"start": start, "end": end}


def find_seed(event_time):
    for seed, waveform in waveform_seeds.items():
        if event_time > waveform["start"] and event_time < waveform["end"]:
            return seed
    return None


"""
def rotate_to_hodogram(hodogram, waveform):
   trend, plunge = hodogram["trend"], plunge["plunge"]
   rotation_matrix = np.array([

   ])
"""

selected_event = events[0]
event_time = datetime.strptime(
    os.path.basename(selected_event).split("_")[1].split(".picks")[0],
    "%Y%m%d.%H%M%S.%f",
)
stream = (
    read(find_seed(event_time))
    .trim(
        starttime=UTCDateTime(UTCDateTime(event_time)) - 1,
        endtime=UTCDateTime(UTCDateTime(event_time)) + 2,
    )
    .detrend(type="linear")
    .taper(type="hann", max_percentage=None, max_length=0.2, side="both")
    .filter(type="highpass", freq=2)
    .rotate("->ZNE", inventory=inv)
)
stations = np.unique([tr.id.split(".")[1] for tr in stream.traces])
f = open(selected_event)
picks = {
    f'{l.split(",")[0]}_{l.split(",")[1]}': float(l.split(",")[2])
    for l in f.readlines()
    if l.split(",")[1] in ["P", "S"]
}
f.close()

found_post = posts.find_one({"event time": event_time})

selected_station = "BR.ESM06"


hodogram = found_post[selected_station.replace(".", "_")]["hodogram"]
azimuth, colatitude = D2R * hodogram["trend"], PI / 2 + D2R * hodogram["plunge"]

rotation_matrix = np.array(
    [
        [
            np.sin(colatitude) * np.sin(azimuth),
            np.sin(colatitude) * np.cos(azimuth),
            np.cos(colatitude),
        ],
        [
            np.cos(colatitude) * np.sin(azimuth),
            np.cos(colatitude) * np.cos(azimuth),
            -np.sin(colatitude),
        ],
        [-np.cos(azimuth), np.sin(azimuth), 0],
    ]
)


selected_station.replace(".", "_") in found_post

st = stream.select(station=selected_station.split(".")[1]).copy()

ch_e = st.select(channel="??E")[0].data
ch_n = st.select(channel="??N")[0].data
ch_z = st.select(channel="??Z")[0].data

data_3c = np.vstack([ch_e, ch_n, ch_z])


fig, ax = plt.subplots(figsize=[8, 3])
ax.plot(data_3c[0, 1000:1500], "firebrick")
ax.plot(data_3c[1, 1000:1500], "royalblue")
ax.plot(data_3c[2, 1000:1500], "0.2")


rotated_data = rotation_matrix @ data_3c

fig, ax = plt.subplots(figsize=[8, 3])
ax.plot(rotated_data[0, 1000:1500], "forestgreen")
ax.plot(rotated_data[1, 1000:1500], "mediumorchid")
ax.plot(rotated_data[2, 1000:1500], "0.2")


ch = st.select(channel=selected_channel)[0]
data = ch.data
times = ch.times("utcdatetime")
pick = [
    UTCDateTime(t) for p, t in picks.items() if selected_station in p and p[-1] == "P"
]
i_length = 20
if len(pick) > 0:
    i_pick = np.argmin(abs(times - pick[0]))
    i_start = i_pick
else:
    i_start = np.argmax(ch.data) - 4

fig = plt.figure(figsize=[12, 8])
ax_trace = fig.add_axes([0.1, 0.8, 0.5, 0.1])
ax_spec = fig.add_axes([0.1, 0.1, 0.5, 0.6])
ax_map = fig.add_axes([0.6, 0.15, 0.35, 0.7])
ax_map.set_aspect("equal")
for station_code, location in dh_stations.items():
    ax_map.plot(
        location["e"],
        location["n"],
        "v",
        zorder=-2,
        ms=10,
        color="lightsteelblue",
        markeredgecolor="k",
    )
    ax_map.text(location["e"], location["n"] - 70, station_code[-2:], ha="center")
    net_sta = station_code.replace(".", "_")
    if net_sta in found_post:
        hodogram = found_post[net_sta]["hodogram"]
        delta_e = hodogram_scale * np.sin(D2R * hodogram["trend"])
        delta_n = hodogram_scale * np.cos(D2R * hodogram["trend"])
        ax_map.plot(
            [location["e"] - delta_e, location["e"] + delta_e],
            [location["n"] - delta_n, location["n"] + delta_n],
            lw=2,
            color="orangered",
            zorder=-1,
        )
pick_id = int(os.path.basename(selected_event).split("_")[0])
if pick_id in athena_ids:
    event_e, event_n = np.array(
        [
            [v["easting"], v["northing"]]
            for v in catalog.values()
            if pick_id == int(v["athena database id"])
        ]
    ).T
    ax_map.plot(event_e, event_n, "*", ms=10, color="orangered", markeredgecolor="k")
ax_map = gray_background_with_grid(ax_map, grid_spacing=100)
ax_trace.plot(times, data)
i_end = i_start + i_length
i_start_n = i_start - i_length - 3
i_end_n = i_start - 3
y1, y2 = ax_trace.get_ylim()
window_signal = patches.Rectangle(
    (times[i_pick], y1),
    times[i_end - 1] - times[i_start],
    y2 - y1,
    facecolor="lightblue",
    edgecolor="k",
    alpha=0.3,
)
window_noise = patches.Rectangle(
    (times[i_start_n], y1),
    times[i_end_n - 1] - times[i_start_n],
    y2 - y1,
    facecolor="0.7",
    edgecolor="k",
    alpha=0.3,
)
ax_trace.add_artist(window_signal)
ax_trace.add_artist(window_noise)
signal = data[i_start:i_end]
noise = data[i_start_n:i_end_n]
freq_signal = fftfreq(len(signal), ch.stats.delta)
freq_noise = fftfreq(len(noise), ch.stats.delta)
i_one_side = np.where(freq_signal > 0)[0]
i_one_side_n = np.where(freq_noise > 0)[0]
ax_trace.set_xlabel("time (s)")
ax_spec.loglog(
    freq_signal[i_one_side],
    abs(fft(signal))[i_one_side] / 2 / np.pi / freq_signal[i_one_side] / len(signal),
)
ax_spec.loglog(
    freq_noise[i_one_side_n],
    abs(fft(noise))[i_one_side_n] / 2 / np.pi / freq_noise[i_one_side_n] / len(noise),
    color="0.7",
    zorder=-3,
)
ax_spec.set_xlabel("frequency(Hz)")
ax_spec.set_ylabel("displacement spectrum (counts$\cdot$ s)")
