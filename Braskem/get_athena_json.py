import json
import requests
from NocMeta.Meta import NOC_META
from ExternalPrograms.Atlas.AtlasSupport import loadEventDict, getEveSumFromDict
import ast

athena_code = "WTX"
event_id = 424719
NOC_META[athena_code]
jsonInfo = requests.get(
    f"https://{NOC_META[athena_code]['athIP']}/events/{event_id}.json?expand=all&apiKey={NOC_META[athena_code]['athApi']}"
).content

event = json.loads(jsonInfo)
preferred_origin = [
    v for v in event["origins"] if v["id"] == event["preferred_origin_id"]
][0]

magnitudes = [
    v
    for v in preferred_origin["magnitudes"]
    if v["origin_id"] == event["preferred_origin_id"]
][0]
arrivals = [
    v
    for v in preferred_origin["associations"]
    if v["origin_id"] == event["preferred_origin_id"]
]


arrival = preferred_origin["associations"][0]


def filterErrorAxes(errorAxes):
    outAxes = []
    for axis in errorAxes:
        outAxes.append(
            {"dip": axis["dip"], "azimuth": axis["azimuth"], "length": axis["length"]}
        )
    return outAxes


def getChannelPath(channel):
    return {
        "locationCode": channel["location_code"].replace("-", ""),
        "channelCode": channel["channel_code"],
        "stationCode": channel["station"]["station_code"],
        "networkCode": channel["station"]["network"]["network_code"],
    }


def packageStationMagnitude(stationMagnitude):
    amplitude = stationMagnitude["amplitude"]
    return {
        "eventAzimuth": stationMagnitude["event_azimuth"],
        "eventDistance": stationMagnitude["event_distance"],
        "value": stationMagnitude["value"],
        "magnitudeType": stationMagnitude["magnitude_type"],
        "amplitude": {
            "time": amplitude["time"],
            "period": amplitude["period"],
            "value": amplitude["value"],
            "amplitudeType": amplitude["amplitude_type"],
            "channelPath": getChannelPath(stationMagnitude["amplitude"]["channel"]),
        },
    }


def packageArrivals(arrival):
    pick = arrival["pick"]
    return {
        "eventAzimuth": arrival["event_azimuth"],
        "eventDistance": arrival["event_distance"],
        "time": pick["time"] + arrival["time_residual"],  # I *hope* this is correct
        "pick": {
            "phase": pick["phase"],
            "time": pick["time"],
            "channelPath": getChannelPath(pick["channel"]),
        },
    }


origin = {
    "errorAxes": filterErrorAxes(preferred_origin["error_axes"]),
    "depthError": preferred_origin["depth_error"],
    "azimuthGap": preferred_origin["azimuth_gap"],
    "analysisType": preferred_origin["analysis_type"],
    "timeRms": preferred_origin["time_rms"],
    "referenceElevation": preferred_origin["reference_elevation"],
    "distanceMaximum": preferred_origin["max_distance"],
    "distanceMinimum": preferred_origin["min_distance"],
    "referenceElevation": preferred_origin["reference_elevation"],
    "comment": preferred_origin["comment"],
    "author": preferred_origin["author"],
    "latitude": preferred_origin["latitude"],
    "longitude": preferred_origin["longitude"],
    "depth": preferred_origin["depth"],
    "azimuthGap": preferred_origin["azimuth_gap"],
    "magnitudes": [
        packageStationMagnitude(v) for v in magnitudes["station_magnitudes"]
    ],
    "arrivals": [packageArrivals(v) for v in arrivals],
}

jsonOut = {"event": {"origin": origin}}
