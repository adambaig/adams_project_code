import numpy as np

from NocMeta.Meta import NOC_META
from obspy.io.segy.core import _read_segy
from obspy.signal.invsim import corn_freq_2_paz


sweep = _read_segy("gravacao_sweep_4_geofones.sgy")

paz_10Hz = corn_freq_2_paz(1.0, damp=0.707)
paz_10Hz["sensitivity"] = 22800 * 6


def read_input_sweep():
    return sweep[1].data


def read_zero_offset_sweeps():
    s_copy = sweep.copy()
    return {
        "zero_offset_1": s_copy[43].simulate(paz_remove=paz_10Hz).data,
        "zero_offset_2": s_copy[44].simulate(paz_remove=paz_10Hz).data,
        "zero_offset_3": s_copy[45].simulate(paz_remove=paz_10Hz).data,
        "zero_offset_4": s_copy[47].simulate(paz_remove=paz_10Hz).data,
        "times": s_copy[43].times(),
    }
