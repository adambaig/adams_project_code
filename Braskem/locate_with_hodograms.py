from glob import glob
import json
import os
import requests

import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory
import pyproj as pr
from scipy.optimize import minimize
from scipy.fftpack import fft, fftfreq

from NocMeta.Meta import NOC_META
from sms_moment_tensor.MT_math import mt_vector_to_matrix
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from sms_moment_tensor.MT_math import (
    unit_vector_to_trend_plunge,
    trend_plunge_to_unit_vector,
)
from generalPlots import gray_background_with_grid

from read_inputs import read_velocity_model

velocity_model = read_velocity_model()

D2R = np.pi / 180.0


def calc_hodogram(wave_12z):
    wave_mat = np.array([wave - np.mean(wave) for wave in wave_12z])
    eig_vals, eig_vecs = np.linalg.eig(wave_mat @ wave_mat.T)
    i_sort = np.argsort(eig_vals)
    linearity = 1 - eig_vals[i_sort[1]] / eig_vals[i_sort[2]]
    trend, plunge = unit_vector_to_trend_plunge(eig_vecs[:, i_sort[2]])
    wave_mat_2d = np.array([wave - np.mean(wave) for wave in wave_12z[:-1]])
    eig_vals_2d, eig_vecs_2d = np.linalg.eig(wave_mat_2d @ wave_mat_2d.T)
    i_sort_2d = np.argsort(eig_vals_2d)
    linearity_2d = 1 - eig_vals_2d[i_sort_2d[0]] / eig_vals_2d[i_sort_2d[1]]
    trend_2d, _ = unit_vector_to_trend_plunge([*eig_vecs_2d[:, i_sort_2d[1]], 0])
    return {
        "trend": float(trend % 360),
        "plunge": float(plunge),
        "linearity": linearity,
        "2D trend": trend_2d % 360,
        "2D linearity": linearity_2d,
    }


inv = read_inventory(r"Station_xml/FBK_Full.xml")
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
stations = {}
for net in inv:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(
                    latlon_proj, out_proj, station.longitude, station.latitude
                )
                stations[f"{net.code}.{station.code}."] = {
                    "abs z": -channel.depth,
                    "abs e": east,
                    "abs n": north,
                }


east, north, elevation = np.average(
    [(v["abs e"], v["abs n"], v["abs z"]) for v in stations.values()], axis=0
)

for station in stations.values():
    station["e"] = station["abs e"] - east
    station["n"] = station["abs n"] - north
    station["z"] = station["abs z"] - elevation

velocity_model = [
    {"rho": 2100, "vp": 1510.0, "vs": 1510.0 / 2.03},
    {"rho": 2200, "vp": 2940.0, "vs": 2940.0 / 2.03, "top": -320},
    {"rho": 2200, "vp": 3610.0, "vs": 3610.0 / 2.03, "top": -420},
    {"rho": 2400, "vp": 4100.0, "vs": 4100.0 / 2.03, "top": -920},
]
Q = {"P": 60, "S": 60}


def vertical_slowness_at_sensor(raypath):
    up_plus_minus = np.sign(raypath["velocity_model_chunk"][-1]["h"])
    abs_slowness = 1.0 / raypath["velocity_model_chunk"][-1]["v"]
    hrz_slowness_sq = sum([v * v for v in raypath["hrz_slowness"].values()])
    return up_plus_minus * np.sqrt(abs_slowness**2 - hrz_slowness_sq)


wrong_velocity_model = [
    {"rho": 2100, "vp": 2000.0, "vs": 2000.0 / 1.87},
    {"rho": 2200, "vp": 2300, "vs": 2300 / 2.03, "top": -320},
    {"rho": 2200, "vp": 3400, "vs": 3400 / 1.87, "top": -420},
    {"rho": 2400, "vp": 4100.0, "vs": 4100.0 / 2.03, "top": -920},
]


def tt_residual(xyzt, picks, velocity_model):
    xyz = {"e": xyzt[0], "n": xyzt[1], "z": xyzt[2]}
    residual = 0
    for stationphase, pick in picks.items():
        station = stations[".".join(stationphase.split(".")[:2])]
        phase = stationphase.split(".")[-1]
        if phase in ["P", "S"]:
            t_xyz = isotropic_ray_trace(xyz, station, velocity_model, phase)[
                "traveltime"
            ]
            residual += (t_xyz - pick + xyzt[3]) ** 2
    return residual


def tt_hodo_residual(xyzt, relative_weighting, features, velocity_model):
    xyz = {"e": xyzt[0], "n": xyzt[1], "z": xyzt[2]}
    residual_tt, residual_hodogram = 0, 0
    for station_name, p_pick in {
        k: v["P"] for k, v in features["rescaled_picks"].items()
    }.items():
        station = stations[station_name]
        t_xyz = isotropic_ray_trace(xyz, station, velocity_model, "P")["traveltime"]
        residual_tt += (t_xyz - p_pick + xyzt[3]) ** 2
    for station_name, s_pick in {
        k: v["S"] for k, v in features["rescaled_picks"].items()
    }.items():
        station = stations[station_name]
        t_xyz = isotropic_ray_trace(xyz, station, velocity_model, "S")["traveltime"]
        residual_tt += (t_xyz - s_pick + xyzt[3]) ** 2
    for station, hodogram in features["hodograms"].items():
        p_raypath = isotropic_ray_trace(xyz, stations[station], velocity_model, "P")
        hrz_slowness = p_raypath["hrz_slowness"]
        slowness = {**hrz_slowness, "z": vertical_slowness_at_sensor(p_raypath)}
        unit_slowness = np.array([slowness["e"], slowness["n"], slowness["z"]])
        unit_slowness /= np.linalg.norm(unit_slowness)
        unit_hodogram = trend_plunge_to_unit_vector(
            hodogram["trend"], hodogram["plunge"]
        )
        residual_hodogram += 1 - abs(np.dot(unit_slowness, unit_hodogram))
    return residual_tt + relative_weighting * residual_hodogram


def get_starting_enzt(picks):
    enz = np.zeros(3)
    t_start = []
    for station, station_picks in picks.items():
        if "P" in station_picks and "S" in station_picks:
            t_start.append(2 * station_picks["P"] - station_picks["S"])
            enz += np.array(
                [stations[station]["e"], stations[station]["n"], stations[station]["z"]]
            )
    enz /= len(picks)
    return [*enz, np.average(t_start)]


def rescale_tt(features):
    min_pick = min([min(v) for v in [p.values() for p in features["picks"].values()]])
    features["rescaled_picks"] = {}
    for station, phasepick in features["picks"].items():
        features["rescaled_picks"][station] = {}
        for phase, pick in phasepick.items():
            features["rescaled_picks"][station][phase] = pick - min_pick


event_file = glob("waveform_features//*.json")[0]

for event_file in glob("waveform_features//*.json"):
    event_id = os.path.basename(event_file).split("_")[0]
    response = requests.get(
        rf"https://{NOC_META['FBK']['athIP']}/events/{event_id}.json?expand=all&apiKey={NOC_META['FBK']['athApi']}"
    )
    event = json.loads(response.content.decode("utf-8"))
    with open(event_file) as f:
        features = json.load(f)
        rescale_tt(features)
        min_pick = min(
            [min(v) for v in [p.values() for p in features["picks"].values()]]
        )
    try:
        minimizer = minimize(
            tt_hodo_residual,
            get_starting_enzt(features["rescaled_picks"]),
            args=(1, features, velocity_model),
        )
        rel_east, rel_north, rel_elevation, rel_time = minimizer.x
        loc_east = east + rel_east
        loc_north = north + rel_north
        loc_elevation = elevation + rel_elevation
        loc_time = min_pick + rel_time
        fig, (ax, ax_depth, ax_depth2) = plt.subplots(1, 3, figsize=[12, 6])
        ax.set_aspect("equal")
        for station in stations.values():
            ax.plot(
                station["abs e"],
                station["abs n"],
                "v",
                color="0.8",
                markeredgecolor="0.2",
            )
        ax.plot(loc_east, loc_north, "o", color="orangered", markeredgecolor="0.2")

        # ax.quiver(
        #     [v["e"] for v in stations.values()],
        #     [v["n"] for v in stations.values()],
        #     uv_easting,
        #     uv_northing,
        #     headlength=0,
        #     headaxislength=0,
        #     headwidth=0,
        #     pivot="middle",
        # )
        ax_depth.set_aspect("equal")
        for station in stations.values():
            ax_depth.plot(
                station["abs e"],
                station["abs z"],
                "v",
                color="0.8",
                markeredgecolor="0.2",
            )
        ax_depth.plot(
            loc_east, loc_elevation, "o", color="orangered", markeredgecolor="0.2"
        )

        ax_depth2.set_aspect("equal")
        for station in stations.values():
            ax_depth2.plot(
                station["abs n"],
                station["abs z"],
                "v",
                color="0.8",
                markeredgecolor="0.2",
            )
        ax_depth2.plot(
            loc_north, loc_elevation, "o", color="orangered", markeredgecolor="0.2"
        )
        scale = 50
        for station, hodogram in features["hodograms"].items():
            ax.plot(
                [
                    stations[station]["abs e"],
                    stations[station]["abs e"]
                    + scale * np.sin(D2R * hodogram["trend"]),
                ],
                [
                    stations[station]["abs n"],
                    stations[station]["abs n"]
                    + scale * np.cos(D2R * hodogram["trend"]),
                ],
                color="c",
            )
            ax.plot(
                [
                    stations[station]["abs e"],
                    stations[station]["abs e"]
                    - scale * np.sin(D2R * hodogram["trend"]),
                ],
                [
                    stations[station]["abs n"],
                    stations[station]["abs n"]
                    - scale * np.cos(D2R * hodogram["trend"]),
                ],
                color="r",
            )

        ax = gray_background_with_grid(ax, grid_spacing=100)
        ax_depth = gray_background_with_grid(ax_depth, grid_spacing=100)
        ax_depth2 = gray_background_with_grid(ax_depth2, grid_spacing=100)
    except:
        pass

features["hodograms"]
