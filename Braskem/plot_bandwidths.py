from datetime import datetime
import glob
import matplotlib.pyplot as plt
from matplotlib import patches
import numpy as np
import os
from scipy.fftpack import fft, fftfreq


from obspy import read, UTCDateTime

pick_dir = "picks"
seed_dir = "mseeds"
events = glob.glob(f"{pick_dir}{os.sep}*.picks")
seeds = glob.glob(f"{seed_dir}{os.sep}*.seed")

event = events[1]

waveform_seeds = {}
for seed in seeds:
    basename = os.path.basename(seed).split(".seed")[0]
    start, end = [datetime.strptime(s, "%Y%m%d.%H%M%S.%f") for s in basename.split("_")]
    waveform_seeds[seed] = {"start": start, "end": end}


def find_seed(event_time):
    for seed, waveform in waveform_seeds.items():
        if event_time > waveform["start"] and event_time < waveform["end"]:
            return seed
    return None


event_time = datetime.strptime(
    os.path.basename(event).split("_")[1].split(".picks")[0], "%Y%m%d.%H%M%S.%f"
)
stream = read(find_seed(event_time)).trim(
    starttime=UTCDateTime(UTCDateTime(event_time)) - 0.2,
    endtime=UTCDateTime(UTCDateTime(event_time)) + 1,
)


f = open(event)
picks = {
    f'{l.split(",")[0]}_{l.split(",")[1]}': float(l.split(",")[2])
    for l in f.readlines()
    if l.split(",")[1] in ["P", "S"]
}
f.close()


np.unique([tr.id.split(".")[1] for tr in stream.traces])
tr = stream.traces[0]
station = "ESM09"
channel = "GPZ"

pick = [UTCDateTime(t) for p, t in picks.items() if station in p and p[-1] == "P"]
channel = (
    stream.select(station="ESM09", channel="GPZ")[0]
    .detrend(type="linear")
    .taper(type="hann", max_percentage=None, max_length=0.1, side="both")
)
times = channel.times() - channel.times()[0]
# f1,(a1,a2,a3,a4) = plt.subplots(4, figsize=[12,10])
# a1.plot(times, channel.data)
# a2.plot(times,channel.copy().filter(type='highpass', freq=30).data)
# a3.plot(times,channel.copy().filter(type='highpass', freq=60).data)
# a4.plot(times,channel.copy().filter(type='highpass', freq=120).data)
# a1.text(0.,0, 'no filter')
# a2.text(0.,0, 'highpass 30 Hz')
# a3.text(0.,0, 'highpass 60 Hz')
# a4.text(0.,0, 'highpass 120 Hz')
# a4.set_xlabel('times (s)')
# f1.savefig('progressive_highpass.png')
#
# f2,(a1,a2,a3,a4) = plt.subplots(4, figsize=[12,10])
# a1.plot(times,channel.data)
# a2.plot(times,channel.copy().filter(type='lowpass', freq=120).data)
# a3.plot(times,channel.copy().filter(type='lowpass', freq=60).data)
# a4.plot(times,channel.copy().filter(type='lowpass', freq=30).data)
# a1.text(0.,0, 'no filter')
# a2.text(0.,0, 'lowpass 120 Hz')
# a3.text(0.,0, 'lowpass 60 Hz')
# a4.text(0.,0, 'lowpass 30 Hz')
# a4.set_xlabel('times (s)')
# f2.savefig('progressive_lowpass.png')
st = stream.select(station=station)
i_pick = [
    np.argmin(abs(st[0].times("utcdatetime") - pick)),
    np.argmin(abs(st[0].times("utcdatetime")[::2] - pick)),
    np.argmin(abs(st[0].times("utcdatetime")[::4] - pick)),
]
f3, (a1, a2, a3) = plt.subplots(3, figsize=[12, 7])
a1.plot(times, channel.data)
a2.plot(
    times[:-1:2], channel.copy().resample(500).filter(type="lowpass", freq=200).data
)
a3.plot(
    times[:-1:4], channel.copy().resample(250).filter(type="lowpass", freq=100).data
)
a2.text(0.35, 0, "resampled at 500 Hz")
a3.text(0.35, 0, "resampled at 250 Hz")
a3.set_xlabel("times (s)")
f3.savefig("progressive_resample.png")
for i_ax, ax in enumerate([a1, a2, a3]):
    y1, y2 = ax.get_ylim()
    ax.plot(
        [times[:: 2**i_ax][i_pick[i_ax]], times[:: 2**i_ax][i_pick[i_ax]]],
        [y1, y2],
        "k",
        zorder=2,
    )
    ax.set_ylim([y1, y2])
    ax.set_xlim([0.35, 0.5])

a1.get_xlim()

f3.savefig("picking_zoom.png")
times = channel.times("utcdatetime")
data = channel.data
fig = plt.figure(figsize=[12, 8])
ax_trace = fig.add_axes([0.1, 0.8, 0.8, 0.1])
ax_spec = fig.add_axes([0.1, 0.1, 0.8, 0.6])
ax_trace.plot(times, data)
pick = [UTCDateTime(t) for p, t in picks.items() if station in p and p[-1] == "P"][0]

if len(pick) > 0:
    i_window_length = 20
    i_pick = np.argmin(abs(times - pick))
    i_end = i_pick + i_window_length
    i_start_n = i_pick - i_window_length - 3
    i_end_n = i_pick - 3

    y1, y2 = ax_trace.get_ylim()
    window_signal = patches.Rectangle(
        (times[i_pick], y1),
        times[i_end - 1] - times[i_start],
        y2 - y1,
        facecolor="lightblue",
        edgecolor="k",
        alpha=0.3,
    )
    window_noise = patches.Rectangle(
        (times[i_start_n], y1),
        times[i_end_n - 1] - times[i_start_n],
        y2 - y1,
        facecolor="0.7",
        edgecolor="k",
        alpha=0.3,
    )
    ax_trace.add_artist(window_signal)
    ax_trace.add_artist(window_noise)
    signal = data[i_start:i_end]
    noise = data[i_start_n:i_end_n]
    freq_signal = fftfreq(len(signal), 0.001)
    freq_noise = fftfreq(len(noise), 0.001)
    i_one_side = np.where(freq_signal > 0)[0]
    i_one_side_n = np.where(freq_noise > 0)[0]

ax_trace.set_xlabel("time (s)")

ax_spec.loglog(
    freq_signal[i_one_side],
    abs(fft(signal))[i_one_side] / 2 / np.pi / freq_signal[i_one_side] / len(signal),
)
ax_spec.loglog(
    freq_noise[i_one_side_n],
    abs(fft(noise))[i_one_side_n] / 2 / np.pi / freq_noise[i_one_side_n] / len(noise),
    color="0.7",
    zorder=-3,
)

ax_spec.set_xlabel("frequency(Hz)")
ax_spec.set_ylabel("displacement spectrum (counts$\cdot$ s)")

ax_spec.plot([5, 35, 350], [1, 1, 1e-4], "r")
ax_spec.plot([5, 500], [0.03, 3e-4], "k")
fig.savefig("spectrum_example.png")
