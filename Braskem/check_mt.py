import json
import requests

import numpy as np
from obspy import read_inventory
import pyproj as pr

from NocMeta.Meta import NOC_META
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_moment_tensor.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)

EVENT_ID = "14354"

with open(f"MT_cache//{EVENT_ID}.json") as f:
    mt_json = json.load(f)

with open(f"waveform_features//{EVENT_ID}_wf.json") as f:
    wf_json = json.load(f)

response = requests.get(
    rf"https://{NOC_META['FBK']['athIP']}/events/{EVENT_ID}.json?expand=all&apiKey={NOC_META['FBK']['athApi']}"
)
event = json.loads(response.content.decode("utf-8"))

inv = read_inventory(r"Station_xml/FBK_Full.xml")
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
stations = {}
for net in inv:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(
                    latlon_proj, out_proj, station.longitude, station.latitude
                )
                stations[f"{net.code}.{station.code}."] = {
                    "z": -channel.depth,
                    "e": east,
                    "n": north,
                }

velocity_model = mt_json["velDict"]
preferred_origin = [
    origin
    for origin in event["origins"]
    if origin["id"] == event["preferred_origin_id"]
][0]
east, north = pr.transform(
    latlon_proj, out_proj, preferred_origin["longitude"], preferred_origin["latitude"]
)
elevation = -1000 * preferred_origin["depth"]
location = {"e": east, "n": north, "z": elevation}
station_phases, amp_strings = np.array(
    [(k, v["amps"][0]) for k, v in mt_json["ampDict"].items()]
).T
amps = [float(a) for a in amp_strings]

A_matrix, right_hand_side = [], []
for st_ph, amp in zip(station_phases, amps):
    station = stations[".".join(st_ph.split(".")[:3])]
    phase = st_ph.split(".")[-1][-1]
    raypaths = {
        "P": isotropic_ray_trace(location, station, velocity_model, "P"),
        "S": isotropic_ray_trace(location, station, velocity_model, "S"),
    }
    if phase == "P":
        A_matrix.append(inversion_matrix_row(raypaths["P"], "P"))
    else:
        A_matrix.append(inversion_matrix_row(raypaths["S"], phase))
    right_hand_side.append(
        back_project_amplitude(raypaths["P"], raypaths["S"], amp, phase)
    )
