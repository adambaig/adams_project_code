import json

import numpy as np

import pyproj as pr
from read_inputs import dh_station_locations

from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_moment_tensor.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)
from NocMeta.Meta import NOC_META
from sms_moment_tensor.MT_math import mt_to_sdr, strike_dip_to_normal

import mplstereonet as mpls

ATHENA_CODE = "FBK"
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META[ATHENA_CODE]['epsg']}")

from obspy import read_inventory

inv = read_inventory(r"Station_xml/oldFBK_Full.xml")
stations = {}
for net in inv:
    for station in net:
        channel = inv.select(station=station.code, channel="?PZ")
        if len(channel) == 0:
            channel = inv.select(station=station.code, channel="HHZ")
        east, north = pr.transform(
            latlon_proj, out_proj, station.longitude, station.latitude
        )
        stations[f"{net.code}.{station.code}."] = {
            "z": station.elevation - channel[0][0][0].depth,
            "e": east,
            "n": north,
        }


with open(r"Nov5_2021_event\41698_waveform.json") as f:
    waveform_features = json.load(f)

with open(r"Nov5_2021_event\41698.json") as f:
    event = json.load(f)

with open(r"Nov5_2021_event\41698_moment_tensor.json") as f:
    mt_attributes = json.load(f)

bpFreqs = mt_attributes["bpFreqs"]
referenceFreq = np.sqrt(bpFreqs[0] * bpFreqs[1])

origin = event["event"]["origin"]
source = {}
source["e"], source["n"] = pr.transform(
    latlon_proj, out_proj, origin["longitude"], origin["latitude"]
)
source["z"] = -origin["depth"] * 1000
# stations = dh_station_locations()

velocity_model = mt_attributes["velDict"]
ampDict = mt_attributes["ampDict"]
pRaypaths, sRaypaths = {}, {}
for station_id, station in stations.items():
    pRaypaths[station_id] = isotropic_ray_trace(source, station, velocity_model, "P")
    sRaypaths[station_id] = isotropic_ray_trace(source, station, velocity_model, "S")


back_project_amplitude(
    pRaypaths["BR.ESM10."],
    sRaypaths["BR.ESM10."],
    mt_attributes["ampDict"]["BR.ESM10..RTH"]["amps"][0],
    "H",
    frequency=referenceFreq,
)

back_projected_amplitudes = {
    ".".join(
        [
            v["channelPath"]["networkCode"],
            v["channelPath"]["stationCode"],
            v["channelPath"]["locationCode"],
            f'RT{v["phase"]}',
        ]
    ): v["backProjectedAmplitude"]
    for v in mt_attributes["eveDict"]["event"]["origin"]["momentTensors"][0][
        "polarityPicks"
    ]
}

inversionMatrix = []
for station_phase in back_projected_amplitudes:
    network, station, location, channel = station_phase.split(".")
    nsl = ".".join([network, station, location])
    if channel[-1] == "P":
        inversionMatrix.append(inversion_matrix_row(pRaypaths[nsl], "P"))
    elif channel[-1] == "V" or channel[-1] == "H":
        inversionMatrix.append(inversion_matrix_row(sRaypaths[nsl], channel[-1]))

moment_tensor = solve_moment_tensor(
    inversionMatrix, list(back_projected_amplitudes.values())
)


moment_tensor["dc"]
mt_to_sdr(moment_tensor["dc"])
from sms_moment_tensor.MT_math import sdr_to_mt, similarity_from_sdr

old_mt = sdr_to_mt(320.8, 9.8, -83.6)
similarity_from_sdr(
    {"strike": 320.8, "dip": 9.8, "rake": -83.6},
    {"strike": 348.3, "dip": 9.6, "rake": -54.8},
)


spectral_fits = waveform_features["spectra"]

strikes, dips, rakes, aux_strikes, aux_dips, aux_rakes = [], [], [], [], [], []
normal_1, normal_2 = [], []
for i in range(len(inversionMatrix)):
    new_matrix = inversionMatrix.copy()
    new_back_projected_amplitudes = list(back_projected_amplitudes.values())
    new_back_projected_amplitudes.pop(i)
    new_matrix.pop(i)
    new_moment_tensor = solve_moment_tensor(new_matrix, new_back_projected_amplitudes)
    strike1, dip1, rake1, strike2, dip2, rake2 = mt_to_sdr(new_moment_tensor["dc"])
    strikes.append(strike1)
    dips.append(dip1)
    rakes.append(rake1)
    aux_strikes.append(strike2)
    aux_dips.append(dip2)
    aux_rakes.append(rake2)
    normal_1.append(strike_dip_to_normal(strike1, dip1))
    normal_2.append(strike_dip_to_normal(strike2, dip2))

fig, ax = mpls.subplots()
ax.plane(strikes, dips, "0.2")
ax.plane(aux_strikes, aux_dips, "0.2")
ax.grid()
fig.savefig("Nov5_2021_event//jackknife_uncertainties.png")

central_1 = sum(normal_1) / np.linalg.norm(sum(normal_1))
central_2 = sum(normal_2) / np.linalg.norm(sum(normal_2))

angle_2 = []
for vector in normal_2:
    angle_2.append(180 * np.arccos(central_2 @ vector) / np.pi)

angle_2
np.std(dips)


def calculate_mw(moment, moment_uncertainty=None):
    mw = 2.0 / 3.0 * np.log10(moment) - 6.03
    if moment_uncertainty is not None:
        mw_uncertainty = 2.0 / (3 * np.log(10)) * moment_uncertainty / moment
    else:
        mw_uncertainty = None
    return mw, mw_uncertainty


calculate_mw(np.linalg.norm(moment_tensor["dc"]) / np.sqrt(2))
