import matplotlib.pyplot as plt
import json

from NocMeta.Meta import NOC_META
import numpy as np
import pyproj as pr
from read_inputs import dh_station_locations
from sms_ray_modelling.raytrace import isotropic_ray_trace

ATHENA_CODE = "FBK"
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META[ATHENA_CODE]['epsg']}")

with open(r"Nov5_2021_event\41698_waveform.json") as f:
    waveform_features = json.load(f)

with open(r"Nov5_2021_event\41698.json") as f:
    event = json.load(f)
with open(r"Nov5_2021_event\41698_moment_tensor.json") as f:
    mt_attributes = json.load(f)


velocity_model = mt_attributes["velDict"]
origin = event["event"]["origin"]
source = {}
source["e"], source["n"] = pr.transform(
    latlon_proj, out_proj, origin["longitude"], origin["latitude"]
)
source["z"] = -origin["depth"] * 1000
spectral_fits = waveform_features["spectra"]
stations = dh_station_locations()


def Gardners_relationship(vp):
    return 310 * vp**0.25


def radiation_pattern_correction(phase):
    if phase == "P":
        return 0.52
    return 0.63


vp = velocity_model[0]["vp"]
vs = velocity_model[0]["vs"]
rho = Gardners_relationship(vp)


def calculate_mw(moment, moment_uncertainty=None):
    mw = 2.0 / 3.0 * np.log10(moment) - 6.03
    if moment_uncertainty is not None:
        mw_uncertainty = 2.0 / (3 * np.log(10)) * moment_uncertainty / moment
    else:
        mw_uncertainty = None
    return mw, mw_uncertainty


surface_stations = ["BR.ESM02.", "BR.ESM03.", "BR.ESM05.", "BR.ESM07."]


station_moments, s_corner_frequencies = {}, {}
distance = {}
for station, phases in spectral_fits.items():
    if station[:2] == "BR":
        for phase, features in phases.items():
            raypath = isotropic_ray_trace(
                source, stations[station], velocity_model, phase[0]
            )
            surface_correction = 1
            if station in surface_stations:
                surface_correction = 2
            if phase == "P":
                velocity = vp
            else:
                velocity = vs
            if features["omega0"]:
                station_moments[f"{station}.{phase}"] = (
                    4
                    * np.pi
                    * velocity**3
                    * rho
                    * raypath["geometrical_spreading"]
                    * features["omega0"]
                    / radiation_pattern_correction(phase)
                    / surface_correction
                )
                if phase != "P":
                    s_corner_frequencies[f"{station}.{phase}"] = features[
                        "cornerFrequency"
                    ]
                distance[f"{station}.{phase}"] = raypath["geometrical_spreading"]


np.average([calculate_mw(moment)[0] for moment in station_moments.values()])

colors = {"P": "firebrick", "SV": "darkgoldenrod", "SH": "royalblue"}


fig, ax = plt.subplots()
for station_phase, moment in station_moments.items():
    network, station, location, phase = station_phase.split(".")

    if ".".join([network, station, location]) in surface_stations:
        marker = "s"
    else:
        marker = "o"
    ax.plot(
        distance[station_phase],
        calculate_mw(moment)[0],
        marker,
        color=colors[phase],
        markeredgecolor="0.2",
    )
    ax.text(distance[station_phase] + 10, calculate_mw(moment)[0], station[-2:])
ax.set_ylabel("station magnitude (Mw)")
ax.set_xlabel("distance (m)")
fig.savefig("distance_vs_station_mag.png")
