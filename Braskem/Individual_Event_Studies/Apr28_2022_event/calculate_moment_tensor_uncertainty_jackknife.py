import json

import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
from obspy import read_inventory
from obspy.imaging.beachball import beachball
import pyproj as pr

from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.moment_tensor.inversion import MTSolution, MTType, inversion_matrix_row, back_project_amplitude
from nmxseis.numerics.moment_tensor.plotting import individual_bb_plot
from nmxseis.numerics.ray_modeling import Raypath
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D
from NocMeta.Meta import NOC_META
from read_inputs import dh_station_locations

ATHENA_CODE = "FBK"
latlon_proj = pr.Proj("epsg:4326")
out_proj = pr.Proj(f"epsg:{NOC_META[ATHENA_CODE]['epsg']}")
inv = read_inventory(r"Station_xml/FBK_Full.xml")

out_proj(station.longitude, station.latitude)

stations = {}
for net in inv:
    for station in net:
        channel = inv.select(station=station.code, channel="?PZ")
        if len(channel) == 0:
            channel = inv.select(station=station.code, channel="H?Z")
        locations = {chn.location_code for chn in station}
        east, north = out_proj(station.longitude, station.latitude)
        for location in locations:
            if len(channel)>0:
                stations[NSL(net=net.code, sta=station.code, loc=location )] = {
                    'enu': [east, north, station.elevation - channel[0][0][0].depth]
                }

with open(r"Individual_Event_Studies\Apr28_2022_event\Data\71381_located.json") as f:
    event_json = json.load(f)

origin = event_json['event']['origin']
event = SeismicEvent(lat = origin['latitude'], lon=origin['longitude'], depth_m = 1000*origin['depth'], origin_time = origin['time'], reproject=out_proj)

with open(r"Individual_Event_Studies\Apr28_2022_event\Data\71381_mt.json") as f:
    mt_attributes = json.load(f)

bpFreqs = mt_attributes["bpFreqs"]
referenceFreq = np.sqrt(bpFreqs[0] * bpFreqs[1])
mt_data = mt_attributes['eveDict']['event']['origin']['custom MT']
polarity_picks = mt_data['Artemis General']['polarityPicks']

layers = []
for i_layer, layer in enumerate(mt_attributes['velDict']):
    if i_layer==0:
        layers.append(VMLayer1D(vp = layer['vp'], vs = layer['vs'], rho = layer['rho']))
    else:
        layers.append(VMLayer1D(vp = layer['vp'], vs = layer['vs'], rho = layer['rho'], top= layer['top']))

velocity_model = VelocityModel1D(layers)

def pick_to_nsl(pick):
    channel_path = pick['channelPath']
    return NSL(net=channel_path['networkCode'], sta = channel_path['stationCode'], loc=channel_path['locationCode'])

stations

back_projected_amplitudes = []
inversion_matrix = []
for pick in polarity_picks:
    phase = Phase.parse(pick['phase'])
    raypath = Raypath.isotropic_ray_trace(event.enu, stations[pick_to_nsl(pick)]['enu'], velocity_model, phase)
    back_projected_amplitudes.append(pick['backProjectedAmplitude'])
    inversion_matrix.append(inversion_matrix_row(raypath, phase))

MTSolution.solve(inversion_matrix, back_projected_amplitudes)


general_mt = MomentTensor.from_vector(
    np.array([mt_data['Artemis General'][comp] for comp in ['mee', 'mnn', 'mzz', 'men', 'mez', 'mnz']])
)

dev_mt = MomentTensor.from_matrix(general_mt.mat - np.trace(general_mt.mat)*np.eye(3)/3)
dc_mt = dev_mt.general_to_dc()
clvd_mt = MomentTensor.from_matrix(dev_mt.mat - dc_mt.mat)

clvd_mt


calculate_mw(np.linalg.norm(moment_tensor["dc"]) / np.sqrt(2))

general_mt.get_clvd_iso_dc()

n_gen = np.linalg.norm(general_MT.mat)
n_dc = np.linalg.norm(dc_mt.mat)
n_clvd = np.linalg.norm(clvd_mt.mat)

fig,ax = plt.subplots()

beach =
