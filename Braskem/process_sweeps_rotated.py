import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read, Stream, Trace, UTCDateTime, read_inventory
from obspy.core.trace import Stats
from obspy.io.segy.core import _read_segy
import os
from scipy.signal import decimate
from read_sweeps import read_input_sweep


def createTraceStats(network, n1, o1, d1, component):
    # make a stats file for a seed trace
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


st = read(r"RecordedSweepsAndPicks\SweepSamples\SweepSamples\VB3\2030.mseed")
inv = read_inventory(r"Station_xml/FBK_Full.xml")


# sweep = _read_segy("testsweep//sweep_20s.sgy")[0]
sweep = read_input_sweep()

auto_corr_sweep = np.correlate(sweep, sweep)
data_dir = r"RecordedSweepsAndPicks\SweepSamples\SweepSamples\VB?"
decon_dir = "decon_downhole_rotated_seeds"
figure_dir = "figures"
data_seeds = glob.glob(f"{data_dir}{os.sep}*.mseed")


for seed in data_seeds:
    dir_tree, root = os.path.split(seed)
    out_name = f'{dir_tree.split(os.sep)[-1]}_{root.split(".")[0]}'
    section = read(seed).sort()
    selection = section.select(station="ESM0[4,6,9]").rotate("->ZNE", inventory=inv)
    deconvolved_traces = []
    channel_names = []
    # fig, ax = plt.subplots(figsize=[8,12])
    channel_color = ["0.2", "firebrick", "royalblue"]

    sweep_seconds = 20

    st = Stream()
    for i_trace, trace in enumerate(selection):
        # if trace.stats.sampling_rate==250:
        channel_obj = {
            "component": trace.stats.channel[-1],
            "channel": trace.stats.channel,
            "station": trace.stats.station,
            "network": trace.stats.network,
            "location": trace.stats.location,
        }
        decimation_factor = int(2000 // trace.stats.sampling_rate)
        dec_sweep = decimate(sweep, decimation_factor)
        time_axis = (
            np.arange(-len(dec_sweep) + 1, len(trace.data)) / trace.stats.sampling_rate
        )
        deconvolved_traces.append(np.correlate(trace, dec_sweep, "full"))
        xStats = createTraceStats(
            "",
            len(deconvolved_traces[-1]),
            trace.stats.starttime - sweep_seconds,
            trace.stats.sampling_rate,
            channel_obj,
        )
        tr = Trace(data=deconvolved_traces[-1] / auto_corr_sweep, header=xStats)
        st += tr
        # ax.plot(
        #     time_axis, i_trace + deconvolved_traces[-1] / max(abs(deconvolved_traces[-1])), channel_color[i_trace % 3]
        # )
        channel_names.append(trace.stats.station + "." + trace.stats.channel)
    # ax.set_yticks(range(i_trace+1))
    # ax.set_yticklabels(channel_names)
    # ax.set_xlim([50,60])
    # ax.set_xlabel('time (s)')
    # fig.savefig('testsweep//section2.png')

    st.write(f"{decon_dir}//{out_name}.mseed")
