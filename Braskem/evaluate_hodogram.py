import glob
import numpy as np
from obspy import read

import matplotlib.pyplot as plt, mpld3
from matplotlib.patches import Rectangle
from ipywidgets import interact
from sms_moment_tensor.MT_math import unit_vector_to_trend_plunge

shot_directory = "testsweep"


def calc_hodogram(wave_12z):
    wave_mat = np.array([wave - np.mean(wave) for wave in wave_12z])
    eig_vals, eig_vecs = np.linalg.eig(wave_mat @ wave_mat.T)
    i_sort = np.argsort(eig_vals)
    linearity = 1 - eig_vals[i_sort[1]] / eig_vals[i_sort[2]]
    trend, plunge = unit_vector_to_trend_plunge(eig_vecs[:, i_sort[2]])
    wave_mat_2d = np.array([wave - np.mean(wave) for wave in wave_12z[:-1]])
    eig_vals_2d, eig_vecs_2d = np.linalg.eig(wave_mat_2d @ wave_mat_2d.T)
    i_sort_2d = np.argsort(eig_vals_2d)
    linearity_2d = 1 - eig_vals_2d[i_sort_2d[0]] / eig_vals_2d[i_sort_2d[1]]
    trend_2d, _ = unit_vector_to_trend_plunge([*eig_vecs_2d[:, i_sort_2d[1]], 0])
    return {
        "trend": float(trend % 360),
        "plunge": float(plunge),
        "linearity": linearity,
        "2D trend": trend_2d % 360,
        "2D linearity": linearity_2d,
    }


event = read(f"{shot_directory}//decon_trace.mseed")
three_c = event.select(station="ESM06")
comp_z = three_c.select(channel="*Z")[0]
times = comp_z.times("matplotlib")
comp_1 = three_c.select(channel="*1")[0]
comp_2 = three_c.select(channel="*2")[0]
comp_3 = comp_z
i_zmax = np.argmax(comp_z)
i_1max = np.argmax(comp_1)
i_2max = np.argmax(comp_2)
i_max = min([i_zmax, i_1max, i_2max])
i_start = 6029
i_length = 10
i_end = i_start + i_length
data_1 = comp_1.data
data_2 = comp_2.data
data_z = comp_z.data
times = comp_1.times("matplotlib")
fig = plt.figure(figsize=[16, 8])
ax_trace = fig.add_axes([0.1, 0.3, 0.5, 0.6])
ax_mini_trace = fig.add_axes([0.1, 0.1, 0.5, 0.15])
ax_12_hodo = fig.add_axes([0.55, 0.5, 0.3, 0.3])
ax_z1_hodo = fig.add_axes([0.55, 0.2, 0.3, 0.3])
ax_z2_hodo = fig.add_axes([0.7, 0.2, 0.3, 0.3])
hodogram = calc_hodogram(
    [data_1[i_start:i_end], data_2[i_start:i_end], data_z[i_start:i_end]]
)
ax_trace.plot(times[i_start:i_end], data_z[i_start:i_end], color="0.15")
ax_trace.plot(times[i_start:i_end], data_1[i_start:i_end], color="firebrick")
ax_trace.plot(times[i_start:i_end], data_2[i_start:i_end], color="royalblue")
ax_mini_trace.plot(times, data_z, color="0.15")
ax_mini_trace.plot(times, data_1, color="firebrick")
ax_mini_trace.plot(times, data_2, color="royalblue")
y1, y2 = ax_trace.get_ylim()
window = Rectangle(
    (times[i_start], y1),
    times[i_end - 1] - times[i_start],
    y2 - y1,
    facecolor="lightblue",
    edgecolor="k",
    alpha=0.3,
)
ax_mini_trace.add_artist(window)
ax_12_hodo.plot(data_1[i_start:i_end], data_2[i_start:i_end], color="0.3")
ax_z1_hodo.plot(data_1[i_start:i_end], data_z[i_start:i_end], color="0.3")
ax_z2_hodo.plot(data_2[i_start:i_end], data_z[i_start:i_end], color="0.3")
max_trace = 1.05 * max([abs(max(data_z)), abs(max(data_1)), abs(max(data_2))])
for ax in [ax_12_hodo, ax_z1_hodo, ax_z2_hodo]:
    ax.set_aspect("equal")
    ax.set_xticks([])
    ax.set_yticks([])
    ax.plot([0, 0], [-max_trace, max_trace], ":", c="0.1", zorder=-1)
    ax.plot([-max_trace, max_trace], [0, 0], ":", c="0.1", zorder=-1)
    ax.set_xlim([-max_trace, max_trace])
    ax.set_ylim([-max_trace, max_trace])
fig.text(0.83, 0.75, f'apparent trend: {hodogram["trend"]:.1f}$\degree$')
fig.text(0.83, 0.71, f'plunge: {hodogram["plunge"]:.1f}$\degree$')
fig.text(0.83, 0.67, f'linearity: {hodogram["linearity"]:.3f}')
fig.text(0.83, 0.63, f'apparent 2D trend: {hodogram["2D trend"]:.1f}$\degree$')
fig.text(0.83, 0.59, f'2D linearity: {hodogram["2D linearity"]:.3f}')


R2D = 180 / np.pi


def unit_vector_to_trend_plunge(u):
    if u[2] > 0:
        u = -u
    trend = np.arctan2(u[0], u[1]) * R2D
    plunge = np.arctan(-u[2] / np.sqrt(u[0] * u[0] + u[1] * u[1])) * R2D
    return [trend % 360, plunge]


unit_vector = eig_vecs[:, i_sort[2]]
uv2 = unit_vector
uv2[2] = -uv2[2]
unit_vector_to_trend_plunge(unit_vector)
unit_vector_to_trend_plunge(uv2)


u = unit_vector
u
if u[2] < 0:
    u = -u
u[0], u[1]
