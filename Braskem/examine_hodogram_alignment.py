from glob import glob
import json
import os
import requests

import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import numpy as np
from obspy import read_inventory, read, UTCDateTime
import pyproj as pr
from scipy.optimize import minimize
from scipy.fftpack import fft, fftfreq

from NocMeta.Meta import NOC_META
from sms_moment_tensor.MT_math import mt_vector_to_matrix
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from sms_moment_tensor.MT_math import (
    unit_vector_to_trend_plunge,
    trend_plunge_to_unit_vector,
)
from generalPlots import gray_background_with_grid

from read_inputs import read_velocity_model

velocity_model = read_velocity_model()

D2R = np.pi / 180.0
tab10 = get_cmap("tab10")

x1, x2 = 197986.58688787775, 198854.75025504341
y1, y2 = 8932712.230823414, 8934324.638987586

dx = 1.1 * (x2 - x1)
dy = 1.1 * (y2 - y1)
dz = 1300

x1p = (x1 + x2) / 2 - 0.5 * dx
x2p = (x1 + x2) / 2 + 0.5 * dx
y1p = (y1 + y2) / 2 - 0.5 * dy
y2p = (y1 + y2) / 2 + 0.5 * dy
z1 = -1200
z2 = 100

inv = read_inventory(r"Station_xml/FBK_Full.xml")
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")
stations = {}
for net in inv:
    for station in net:
        for channel in station:
            if "DPZ" in channel.code or "HHZ" in channel.code:
                east, north = pr.transform(
                    latlon_proj, out_proj, station.longitude, station.latitude
                )
                stations[f"{net.code}.{station.code}."] = {
                    "abs z": -channel.depth,
                    "abs e": east,
                    "abs n": north,
                }


east, north, elevation = np.average(
    [(v["abs e"], v["abs n"], v["abs z"]) for v in stations.values()], axis=0
)

for station in stations.values():
    station["e"] = station["abs e"] - east
    station["n"] = station["abs n"] - north
    station["z"] = station["abs z"] - elevation

velocity_model = [
    {"rho": 2100, "vp": 1510.0, "vs": 1510.0 / 2.03},
    {"rho": 2200, "vp": 2940.0, "vs": 2940.0 / 2.03, "top": -320},
    {"rho": 2200, "vp": 3610.0, "vs": 3610.0 / 2.03, "top": -420},
    {"rho": 2400, "vp": 4100.0, "vs": 4100.0 / 2.03, "top": -920},
]
Q = {"P": 60, "S": 60}
pick_color = {"P": "cyan", "S": "green"}
min_buffer, max_buffer = 0.1, 0.5


def vertical_slowness_at_sensor(raypath):
    up_plus_minus = np.sign(raypath["velocity_model_chunk"][-1]["h"])
    abs_slowness = 1.0 / raypath["velocity_model_chunk"][-1]["v"]
    hrz_slowness_sq = sum([v * v for v in raypath["hrz_slowness"].values()])
    return up_plus_minus * np.sqrt(abs_slowness**2 - hrz_slowness_sq)


def getChannelPath(channel):
    return {
        "locationCode": channel["location_code"].replace("-", ""),
        "channelCode": channel["channel_code"],
        "stationCode": channel["station"]["station_code"],
        "networkCode": channel["station"]["network"]["network_code"],
    }


def packageArrivals(arrival):
    pick = arrival["pick"]
    return {
        "eventAzimuth": arrival["event_azimuth"],
        "eventDistance": arrival["event_distance"],
        "time": pick["time"] - arrival["time_residual"],  # I *hope* this is correct
        "pick": {
            "phase": pick["phase"],
            "time": pick["time"],
            "channelPath": getChannelPath(pick["channel"]),
        },
    }


pick_files = glob(r"High_quality_events_backup\picks\*.picks")
seed_dir = r"High_quality_events_backup\seeds"
feature_files = glob("waveform_features//*.json")
figure_dir = "waveforms_and_hodogram_figures"
scale = 100


def get_tab10_color(num_string):
    return tab10(float(num_string[-1]) / 10 + 0.1)


for event_file in feature_files[24:25]:
    event_id = os.path.basename(event_file).split("_")[0]
    if not (os.path.isfile(os.path.join(seed_dir, f"{event_id}.seed"))):
        continue
    response = requests.get(
        rf"https://{NOC_META['FBK']['athIP']}/events/{event_id}.json?expand=all&apiKey={NOC_META['FBK']['athApi']}"
    )
    event = json.loads(response.content.decode("utf-8"))
    preferred_origin = [
        origin
        for origin in event["origins"]
        if origin["id"] == event["preferred_origin_id"]
    ][0]
    east, north = pr.transform(
        latlon_proj,
        out_proj,
        preferred_origin["longitude"],
        preferred_origin["latitude"],
    )
    elevation = -1000 * preferred_origin["depth"]
    with open(event_file) as f:
        feature_json = json.load(f)

    fig = plt.figure(figsize=[14, 14])
    ax_depth1 = fig.add_axes([0.05, 0.05, 0.9 * dx / (dx + dy), 0.9 * dz / (dy + dz)])
    ax_depth2 = fig.add_axes(
        [0.05 + 0.9 * dx / (dx + dy), 0.05, 0.9 * dy / (dx + dy), 0.9 * dz / (dy + dz)]
    )
    ax_plan = fig.add_axes(
        [0.05, 0.05 + 0.9 * dz / (dz + dy), 0.9 * dx / (dx + dy), 0.9 * dy / (dz + dy)]
    )

    ax_waveforms = fig.add_axes([0.4, 0.5, 0.55, 0.45])

    ax_plan.set_aspect("equal")
    ax_depth2.set_aspect("equal")
    ax_depth1.set_aspect("equal")

    for station_name, station in stations.items():
        ax_plan.plot(
            station["abs e"],
            station["abs n"],
            "v",
            color=get_tab10_color(station_name[-2]),
            markeredgecolor="0.2",
        )
        ax_depth1.plot(
            station["abs e"],
            station["abs z"],
            "v",
            color=get_tab10_color(station_name[-2]),
            markeredgecolor="0.2",
        )
        ax_depth2.plot(
            station["abs n"],
            station["abs z"],
            "v",
            color=get_tab10_color(station_name[-2]),
            markeredgecolor="0.2",
        )
    ax_plan.plot(east, north, "o", color="orangered", markeredgecolor="k")
    ax_depth1.plot(east, elevation, "o", color="orangered", markeredgecolor="k")
    ax_depth2.plot(north, elevation, "o", color="orangered", markeredgecolor="k")
    ax_plan.set_xlim(x1p, x2p)
    ax_plan.set_ylim(y1p, y2p)
    ax_depth1.set_xlim(x1p, x2p)
    ax_depth1.set_ylim(z1, z2)
    ax_depth2.set_xlim(y1p, y2p)
    ax_depth2.set_ylim(z1, z2)

    for station, hodogram in feature_json["hodograms"].items():
        if hodogram:
            trend, plunge = hodogram["trend"], hodogram["plunge"]

            ax_plan.plot(
                [
                    stations[station]["abs e"]
                    + scale * np.cos(D2R * plunge) * np.sin(D2R * trend),
                    stations[station]["abs e"]
                    - scale * np.cos(D2R * plunge) * np.sin(D2R * trend),
                ],
                [
                    stations[station]["abs n"]
                    + scale * np.cos(D2R * plunge) * np.cos(D2R * trend),
                    stations[station]["abs n"]
                    - scale * np.cos(D2R * plunge) * np.cos(D2R * trend),
                ],
                zorder=-2,
                color=get_tab10_color(station[-2]),
            )
            ax_depth1.plot(
                [
                    stations[station]["abs e"]
                    + scale * np.cos(D2R * plunge) * np.sin(D2R * trend),
                    stations[station]["abs e"]
                    - scale * np.cos(D2R * plunge) * np.sin(D2R * trend),
                ],
                [
                    stations[station]["abs z"] - scale * np.sin(D2R * plunge),
                    stations[station]["abs z"] + scale * np.sin(D2R * plunge),
                ],
                zorder=-2,
                color=get_tab10_color(station[-2]),
            )

            ax_depth2.plot(
                [
                    stations[station]["abs n"]
                    + scale * np.cos(D2R * plunge) * np.cos(D2R * trend),
                    stations[station]["abs n"]
                    - scale * np.cos(D2R * plunge) * np.cos(D2R * trend),
                ],
                [
                    stations[station]["abs z"] - scale * np.sin(D2R * plunge),
                    stations[station]["abs z"] + scale * np.sin(D2R * plunge),
                ],
                zorder=-2,
                color=get_tab10_color(station[-2]),
            )

    for ax in ax_plan, ax_depth1, ax_depth2:
        ax = gray_background_with_grid(ax, grid_spacing=50)
    date_str = str(UTCDateTime(preferred_origin["time"])).split("T")[0]
    ax_waveforms.set_ylim(0, 11)
    ax_waveforms.set_title(f"{event_id} {date_str}")
    ax_waveforms.set_facecolor("0.1")
    picks = [packageArrivals(v) for v in preferred_origin["associations"]]
    pick_times = [p["pick"]["time"] for p in picks]
    theoretical_times = [p["time"] for p in picks]
    max_pick = max([*pick_times, *theoretical_times])
    min_pick = min([*pick_times, *theoretical_times])

    stream = (
        read(os.path.join(seed_dir, f"{event_id}.seed"))
        .detrend()
        .filter("highpass", freq=10)
    )
    stream.rotate("->ZNE", inventory=inv)
    stream.trim(
        starttime=UTCDateTime(min_pick - min_buffer),
        endtime=UTCDateTime(max_pick + max_buffer),
    )
    for station in stations:
        station_name = station.split(".")[1]
        station_int = int(station_name[3:])
        stream_3c = stream.select(station=station_name)
        if len(stream_3c) == 3:
            z_comp = stream_3c.select(channel="??Z")[0].data
            e_comp = stream_3c.select(channel="??E")[0].data
            n_comp = stream_3c.select(channel="??N")[0].data
            norm = 2 * max(
                [
                    max(abs(z_comp)),
                    max(abs(e_comp)),
                    max(abs(n_comp)),
                ]
            )
            time_axis = stream_3c[0].times() + min_pick - min_buffer
            ax_waveforms.plot(
                time_axis, station_int + z_comp / norm, color="0.9", alpha=0.8
            )
            ax_waveforms.plot(
                time_axis, station_int + e_comp / norm, color="deepskyblue", alpha=0.8
            )
            ax_waveforms.plot(
                time_axis, station_int + n_comp / norm, color="orange", alpha=0.8
            )
            station_picks = [
                pick
                for pick in picks
                if pick["pick"]["channelPath"]["stationCode"] == station_name
            ]

            for pick in station_picks:
                phase = pick["pick"]["phase"]
                ax_waveforms.plot(
                    [pick["time"], pick["time"]],
                    [station_int - 0.5, station_int + 0.5],
                    ":",
                    color=pick_color[phase],
                )
                ax_waveforms.plot(
                    [pick["pick"]["time"], pick["pick"]["time"]],
                    [station_int - 0.5, station_int + 0.5],
                    color=pick_color[phase],
                )
    ax_waveforms.set_xlim(min_pick - min_buffer, max_pick + max_buffer)
    ax_waveforms.set_yticks(range(1, 11))
    [
        ax_waveforms.get_yticklabels()[i - 1].set_color(get_tab10_color(str(i)))
        for i in range(1, 11)
    ]
    xticks = ax_waveforms.get_xticks()
    ax_waveforms.set_xticklabels(
        [str(UTCDateTime(t)).split("T")[1][:-6] for t in xticks]
    )
    ax_waveforms.set_xlabel("UTC time")
    fig.savefig(os.path.join(figure_dir, f"{event_id}.png"))
