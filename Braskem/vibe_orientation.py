import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory
import pyproj as pr

from NocMeta.Meta import NOC_META
from generalPlots import gray_background_with_grid
from sms_moment_tensor.MT_math import trend_plunge_to_unit_vector

PI = np.pi
D2R = PI / 180.0
latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")

f = open("vibe_spreadsheet_with_hodogram_data.csv")
head = f.readline()
lines = f.readlines()
f.close()

deep_stations = ["ESM04", "ESM06", "ESM09"]
inv = read_inventory("Station_xml//FBK_Light.xml")

vibe_recording = {}
for line in lines:
    lspl = line.split(",")
    vibe, event_number, station = lspl[:3]
    vibe_recording[f"{vibe} {event_number} {station}"] = {
        "window length": int(lspl[3]),
        "trend": (float(lspl[4])) % 360,
        "plunge": float(lspl[5]),
        "linearity": float(lspl[6]),
        "2d trend": float(lspl[7]),
        "2d linearity": float(lspl[8]),
        "notes": lspl[9].strip(),
    }

sweep_locations = {
    "VB1": {
        "latitude": -9 - 38 / 60.0 - 25.560 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 44.944 / 3600.0,
        "elevation": 6,
    },
    "VB2": {
        "latitude": -9 - 38 / 60.0 - 7.308 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 59.252 / 3600.0,
        "elevation": 7,
    },
    "VB3": {
        "latitude": -9 - 37 / 60.0 - 53.020 / 3600.0,
        "longitude": -35 - 44 / 60.0 - 45.273 / 3600.0,
        "elevation": 57,
    },
}

fig_map, ax_map = plt.subplots(figsize=[9, 9])

for sweep, location in sweep_locations.items():
    east, north = pr.transform(
        latlon_proj, out_proj, location["longitude"], location["latitude"]
    )
    location["e"], location["n"] = east, north
    ax_map.plot(east, north, "o", ms=14, markeredgecolor="k", color="lightsteelblue")
    ax_map.text(east, north + 30, sweep, ha="center")

stations = {}
for station in deep_stations:
    channels = inv.select(station=station, channel="DPZ")[0][0]
    east, north = pr.transform(
        latlon_proj, out_proj, channels.longitude, channels.latitude
    )
    stations[channels.code] = {
        "e": east,
        "n": north,
        "z": channels.elevation - channels[0].depth,
    }
    p_downhole_station = ax_map.plot(
        east, north, "v", ms=14, color="firebrick", markeredgecolor="k"
    )
    ax_map.text(east + 20, north, channels.code, va="center")

ax_map = gray_background_with_grid(ax_map, grid_spacing=100)


def calc_azimuth(point1, point2):
    # from point1 to point2
    return np.arctan2(point2["e"] - point1["e"], point2["n"] - point1["n"]) / D2R


def calc_azimuth_from_key(key):
    vibe, _, station = key.split()
    return calc_azimuth(sweep_locations[vibe], stations[station])


fig_map.savefig("Orientations//map.png", bbox_inches="tight")

vibe_color = {"VB1": "firebrick", "VB2": "royalblue", "VB3": "goldenrod"}

scale = 500
good_vibes = {}
for station in deep_stations:
    fig, ax = plt.subplots(figsize=[9, 9])
    ax.set_aspect("equal")
    vibes = {
        k: v
        for k, v in vibe_recording.items()
        if station in k
        and v["notes"] == ""
        and v["linearity"] > 0.98
        and v["2d linearity"] > 0.98
    }
    trend_east, trend_north, vibe_east, vibe_north, color = [], [], [], [], []
    for vibe_key, hodogram in vibes.items():
        vibe, event, station_again = vibe_key.split()
        unit_vector = trend_plunge_to_unit_vector(
            hodogram["trend"] + 0, hodogram["plunge"]
        )
        trend_east.append(unit_vector[0] * scale)
        trend_north.append(unit_vector[1] * scale)
        color.append(vibe_color[vibe_key[:3]])
        good_vibes[vibe_key] = {
            **hodogram,
            "actual azimuth": calc_azimuth_from_key(vibe_key),
            "difference": (hodogram["trend"] - calc_azimuth_from_key(vibe_key) + 360)
            % 360,
            "unit vector": unit_vector,
        }
    for sweep, location in sweep_locations.items():
        east, north = pr.transform(
            latlon_proj, out_proj, location["longitude"], location["latitude"]
        )
        location["e"], location["n"] = east, north
        ax.plot(east, north, "o", ms=14, markeredgecolor="k", color=vibe_color[sweep])
        ax.text(east, north + 30, sweep, ha="center")

    ax.quiver(
        stations[station]["e"],
        stations[station]["n"],
        trend_east,
        trend_north,
        color=color,
        scale=1,
        scale_units="x",
    )

    ax.set_xlim(197800, 199000)
    ax.set_ylim(8932700, 8934800)
    ax = gray_background_with_grid(ax, grid_spacing=100)
    fig.savefig(f"Orientations//apparent_hodo_{station}.png", bbox_inches="tight")

np.arctan2(*phase_sum) / D2R
station = "ESM04"
shift = {}
for station in deep_stations:
    shift[station] = (
        np.arctan2(
            *np.sum(
                [
                    [np.sin(v["difference"] * D2R), np.cos(v["difference"] * D2R)]
                    for k, v in good_vibes.items()
                    if station in k
                ],
                axis=0,
            )
        )
        / D2R
    )


for station in deep_stations:
    fig, ax = plt.subplots(figsize=[9, 9])
    ax.set_aspect("equal")
    vibes = {
        k: v
        for k, v in vibe_recording.items()
        if station in k
        and v["notes"] == ""
        and v["linearity"] > 0.98
        and v["2d linearity"] > 0.98
    }
    trend_east, trend_north, vibe_east, vibe_north, color = [], [], [], [], []
    for vibe_key, hodogram in vibes.items():
        vibe, event, station_again = vibe_key.split()
        unit_vector = trend_plunge_to_unit_vector(
            hodogram["trend"] - shift[station], hodogram["plunge"]
        )
        trend_east.append(unit_vector[0] * scale)
        trend_north.append(unit_vector[1] * scale)
        color.append(vibe_color[vibe_key[:3]])
        good_vibes[vibe_key] = {
            **hodogram,
            "actual azimuth": calc_azimuth_from_key(vibe_key),
            "difference": (hodogram["trend"] - calc_azimuth_from_key(vibe_key) + 360)
            % 360,
            "unit vector": unit_vector,
        }
    for sweep, location in sweep_locations.items():
        east, north = pr.transform(
            latlon_proj, out_proj, location["longitude"], location["latitude"]
        )
        location["e"], location["n"] = east, north
        ax.plot(east, north, "o", ms=14, markeredgecolor="k", color=vibe_color[sweep])
        ax.text(east, north + 30, sweep, ha="center")

    ax.quiver(
        stations[station]["e"],
        stations[station]["n"],
        trend_east,
        trend_north,
        color=color,
        scale=1,
        scale_units="x",
    )

    ax.set_xlim(197800, 199000)
    ax.set_ylim(8932700, 8934800)
    ax = gray_background_with_grid(ax, grid_spacing=100)
    fig.savefig(f"Orientations//shifted_hodo_{station}.png", bbox_inches="tight")


for s in shift.values():
    print(f"{90 - s:.1f} {- s:.1f}")
