import glob
import os

pngs = glob.glob("Orientations//VB?//*.png")
f = open("vibe_spreadsheet.csv", "w")
f.write("vibe locations,event number,station\n")
for png in pngs:
    vibe = png.split(os.sep)[1]
    station, eventpng = png.split(os.sep)[2].split("_")
    event, _ = eventpng.split(".")
    f.write(f"{vibe},{event},{station}\n")
f.close()
