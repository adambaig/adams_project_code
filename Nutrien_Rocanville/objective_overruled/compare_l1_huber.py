import matplotlib.pyplot as plt
import numpy as np

from read_inputs import LOGIKDATA, read_located_events


l1_dir = LOGIKDATA.joinpath('Located', 'Loki-i_L1')
huber_dir = LOGIKDATA.joinpath('Located', 'Loki-i_Huber')

loc_l1 = read_located_events(l1_dir)
loc_huber = read_located_events(huber_dir)

fig, ax = plt.subplots()

fig_p, ax_p = plt.subplots()
fig_e, (ax_e_l, ax_e_n) = plt.subplots(1,2, figsize=[8,8],sharey=True, sharex=True)
l1_kwargs = {'color': 'seagreen', 'zorder': 3, 'mec': '0.2', 'marker': 'o', 'alpha': 0.5, 'lw':0, 'label': 'L1'}
huber_kwargs = {**l1_kwargs, 'color': 'royalblue', 'zorder': 1, 'label': 'Huber'}

for loc, kwargs, ax_e in zip([loc_l1, loc_huber], [l1_kwargs, huber_kwargs], [ax_e_n, ax_e_l]):
	enus = np.array([e['enu'] for e in loc.values()])
	v_error = np.array([e['vrt_error'] for e in loc.values()])
	n_picks = np.array([e['n_picks'] for e in loc.values()])
	ax.plot(enus[:, 0], enus[:, 2], **kwargs)
	ax_p.plot(enus[:, 0], enus[:, 1], **kwargs)
	scatter = ax_e.scatter(v_error, enus[:,2], c=n_picks, cmap='turbo', alpha = 0.6, edgecolor='0.2', marker='o', vmin=5, vmax=20)
	# ax_e.set_xscale('log')

for a in (ax,ax_p, ax_e_n, ax_e_l):
	a.set_facecolor('0.96')

ax.legend()
ax_p.legend()
ax_p.set_label('easting (m)')
ax_p.set_label('northing (m)')
fig.savefig('l1_vs_huber_depth.png')
fig_p.savefig('l1_vs_huber_plan.png')
cbar = fig_e.colorbar(scatter, orientation='horizontal', ax=[ax_e_l ,ax_e_n])
cbar.set_label('# of picks')
ax_e_l.set_ylim([-2000,600])

fig_hist, (ax_hist_l1, ax_hist_huber) = plt.subplots(1, 2, figsize=[8, 10])
depth_bins = np.arange(-1000,360, 10)
ax_hist_l1.hist([e['enu'][2] for e in loc_l1.values()], bins = depth_bins, orientation='horizontal')
ax_hist_huber.hist([e['enu'] [2] for e in loc_huber.values()], bins = depth_bins, orientation='horizontal')
ax_hist_l1.set_title('L1')
ax_hist_huber.set_title('huber')
ax_hist_l1.set_ylabel('elevation (m)')
ax_hist_l1.set_xlabel('count')
ax_hist_huber.set_xlabel('count')
fig_hist.savefig('l1_huber_depth_hist.png')
for a in (ax_hist_huber, ax_hist_l1):
	a.set_facecolor('0.96')

enus_huber = np.array([e['enu'] for e in loc_huber.values()])
arrow = np.array([loc_l1[ev_id]['enu'] - loc_huber[ev_id]['enu'] for ev_id in loc_l1])



fig_arrow,ax_arrow = plt.subplots(2, sharex=True, figsize=[6,12])

ax_arrow[0].set_aspect('equal')
ax_arrow[0].quiver(enus_huber[:, 0], enus_huber[:, 1], arrow[:, 0], arrow[:, 1])
ax_arrow[1].quiver(enus_huber[:, 0], enus_huber[:, 2], arrow[:, 0], arrow[:, 2])


fig_error_comp, (ax_error_comp_hrz, ax_error_comp_vrt) = plt.subplots(1,2, figsize=[10,5])
ax_error_comp_hrz.loglog([e['hrz_error'] for e in loc_l1.values()], [e['hrz_error'] for e in loc_huber.values()], 'o', color='firebrick', mec='0.2')
ax_error_comp_vrt.loglog([e['vrt_error'] for e in loc_l1.values()], [e['vrt_error'] for e in loc_huber.values()], 'o', color='forestgreen', mec='0.2')

for ax in [ax_error_comp_hrz, ax_error_comp_vrt]:
	ax.set_facecolor('0.96')
	xlim = ax.get_xlim()
	ylim = ax.get_ylim()

ax_dims = [*ax_error_comp_hrz.get_xlim(),*ax_error_comp_hrz.get_ylim(),*ax_error_comp_vrt.get_xlim(),*ax_error_comp_vrt.get_ylim()]
max_xy = max(ax_dims)
min_xy = min(ax_dims)
for ax in (ax_error_comp_hrz,ax_error_comp_vrt):
	ax.loglog([min_xy, max_xy], [min_xy, max_xy],'k:', zorder=-2)
	ax.set_xlim([min_xy, max_xy])
	ax.set_ylim([min_xy, max_xy])
	ax.set_xlabel('L1')
	ax.set_ylabel('Huber')

ax_error_comp_vrt.set_title('vertical error (m)')
ax_error_comp_hrz.set_title('horizontal error (m)')
fig_error_comp.savefig('error_comparison.png')
plt.show()



