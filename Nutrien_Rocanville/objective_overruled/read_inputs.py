import json
from pathlib import Path

from nmxseis.model.nslc import NSL
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator
from nmxseis.numerics.velocity_model.vm_1d import VM1DTTInterpolator, VM1DIncomingEnuInterpolator
from obspy import read_inventory, UTCDateTime

LOGIKDATA = Path(r'C:\Logiklyst\PSM\LogikData\NTR_RCN')
EPSG = '32613'
REPROJECT = EPSGReprojector(EPSG)
INV = read_inventory(LOGIKDATA.joinpath('NTR_RCN_Full.xml'))

station_enus = REPROJECT.get_station_enus(INV, NSL.iter_inv(INV))

def get_optimizing_relocator():
	interp_dir = LOGIKDATA.joinpath('interpolators')
	nsl_order = sorted(tuple(station_enus.keys()))
	return OptimizingRelocator(
		station_enus,
		EPSG,
		nsl_order,
		vm = LOGIKDATA.joinpath('velocity_model.json'),
		tt_interpolator= VM1DTTInterpolator.load_from_npz_dir(interp_dir, nsl_order),
		ray_interpolator= VM1DIncomingEnuInterpolator.load_from_npz_dir(interp_dir, nsl_order)
	)

def read_located_events(located_dir):
	events = {}
	for event_json in located_dir.glob('*.json'):
		event_id = event_json.stem
		event_dict = json.loads(event_json.read_text())
		events[event_id] = {'origin': (e_o := EventOrigin(
			lat=(origin := event_dict['event']['origin'])['latitude'],
			lon=origin['longitude'],
			depth_m=1000 * origin['depth'],
			time=UTCDateTime(origin['time']),
		)),
		                 'enu': e_o.enu(REPROJECT),
		                 'vrt_error': origin['depthError']*1000,
		                 'hrz_error': origin['horizontalError']*1000,
		                 'rms': origin['timeRms'],
		                'n_picks': len(origin['arrivals'])
		                 }
	return events