from itertools import product
import logging

from nmxseis.model.pick_set import PickSet
from nmxseis.numerics._internal_util import dot_last_axis
from nmxseis.numerics.geometry import GridBoundsError
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocator, DEFAULT_PICK_ERR_MS, DEFAULT_HODO_ERROR, DEFAULT_XATOL, INTERP_PHASE_ORDER
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocatorHint

import numpy as np

def make_objective_function(pick_set: PickSet, station_enus: [dict, np.ndarray], relocator: OptimizingRelocator, hint: OptimizingRelocatorHint):

	sta_loc_arr = np.array([v for v in station_enus.values()])

	t_ref_sec = min(pick_set.picks).time.timestamp
	enu_ref_m = sta_loc_arr.mean(axis=0)
	nsta, npha = relocator.nsta, relocator.npha

	# which picks are valid to be summed in the loss
	pick_validity_arr = np.zeros((nsta, npha), dtype=bool)
	pick_sigma_arr_ms = np.full((nsta, npha), DEFAULT_PICK_ERR_MS, dtype=np.float64)
	pick_time_arr_lms = np.zeros((nsta, npha), dtype=np.float64)

	# which hodograms are valid to be summed in the loss
	hodo_validity_arr = np.zeros((nsta, npha), dtype=bool)
	hodos_arr = np.zeros((nsta, npha, 3), dtype=np.float64)
	# ones not to blow up division on invalid values; they are masked after-the-fact
	hodo_errs_rad = np.full((nsta, npha), DEFAULT_HODO_ERROR, dtype=np.float64)

	for pick in pick_set.picks:
		try:
			nx = relocator.nsl_order.index(pick.nsl)
		except IndexError:
			logging.warning(f"Skipping pick for {pick.nsl=}: station not in TT grid!")
			continue
		px = INTERP_PHASE_ORDER.index(pick.phase)
		pick_validity_arr[nx, px] = True

		key = pick.nsl, pick.phase
		if hint.pick_errors and key in hint.pick_errors:
			pick_sigma_arr_ms[nx, px] = hint.pick_errors[key] * 1000
			pick_time_arr_lms[nx, px] = 1000 * (pick.time.timestamp - t_ref_sec)

		if hint.hodograms and key in hint.hodograms:
			hodo_validity_arr[nx, px] = True
			hodos_arr[nx, px, :] = hint.hodograms[key].unit_vector_enu
			hodo_errs_rad[nx, px] = np.radians(hint.hodograms[key].error_deg)

	do_hodos = hodo_validity_arr.sum() > 0

	def to_local(enut: np.ndarray) -> np.ndarray:
		out = enut.copy()
		out[:3] -= enu_ref_m
		out[3] -= t_ref_sec
		out[3] *= 1000
		return out


	def to_global(enut_lms: np.ndarray) -> np.ndarray:
		out = enut_lms.copy()
		out[3] /= 1000
		out[:3] += enu_ref_m
		out[3] += t_ref_sec
		return out

	def get_raytraced_arrs(src_enu: np.ndarray) -> tuple[np.ndarray, np.ndarray]:
		"""
		Gets arrays by ray-tracing the vm directly. Used when we are outside the interpolator
		grid.
		"""
		assert relocator.vm is not None
		tts = np.zeros((relocator.nsta, 2))
		enus = np.zeros((relocator.nsta, 2, 3))
		for (nx, nsl), (px, pha) in product(
				enumerate(relocator.nsl_order), enumerate(INTERP_PHASE_ORDER)
		):
			ray = relocator.vm.raytrace(src_enu, station_enus[nsl], pha)
			tts[nx, px] = ray.get_tt_sec()
			enus[nx, px] = ray.get_unit_incoming_enu()
		return tts, enus

	def objective(enut):
		enut_ms = to_local(enut)
		χ2 = 0.0
		src_enu = to_global(enut_ms)[:3]
		if relocator.tt_interpolator is not None:
			try:
				tt_from_src = relocator.tt_interpolator.get_tt_arr(src_enu)
				hodo_from_src = None
			except GridBoundsError:
				# do the hodo array here as well so we don't need to ray trace twice
				if relocator.vm is not None:
					tt_from_src, hodo_from_src = get_raytraced_arrs(src_enu)
				else:
					raise
		else:
			tt_from_src, hodo_from_src = get_raytraced_arrs(src_enu)

		want_arrivals = 1000 * tt_from_src + enut_ms[3]
		have_arrivals = pick_time_arr_lms


		χ2 += (
				(((want_arrivals - have_arrivals) / pick_sigma_arr_ms) ** 2) * pick_validity_arr
		).sum()

		if do_hodos:
			if hodo_from_src is None and relocator.ray_interpolator is not None:
				hodo_from_src = relocator.ray_interpolator.get_unit_incoming_enu(src_enu=src_enu)
			assert hodo_from_src is not None
			hodo_dot = abs(dot_last_axis(hodo_from_src, hodos_arr))
			χ2 += (
					relocator.hodogram_weight
					* (((np.arccos(hodo_dot) / hodo_errs_rad) ** 2) * hodo_validity_arr).sum()
			)

		return χ2
	return objective