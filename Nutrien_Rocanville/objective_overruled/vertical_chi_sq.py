import matplotlib
matplotlib.use('TkAgg')

import matplotlib.pyplot as plt
import numpy as np

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocatorHint
from NocMeta.Meta import NOC_META

from objective_function import make_objective_function
from read_inputs import station_enus, INV,  LOGIKDATA, get_optimizing_relocator, read_located_events

ATHENA_CODE = 'NTR_RCN'
athena = AthenaClient.from_noc_meta(NOC_META, ATHENA_CODE)
pick_error = {Phase.P: 0.010, Phase.S: 0.020}
pick_errors = {
    (nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(INV) for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
    (nsl, ph): 1000*pick_error[ph] for nsl in NSL.iter_inv(INV) for ph in [Phase.P, Phase.S]
}

loki_events = read_located_events(LOGIKDATA.joinpath('Located','Loki-i'))

relocator = get_optimizing_relocator()

events_to_investigate = {ev_id: ev for ev_id, ev in loki_events.items() if ev['vrt_error']<0.1}

enu_ref_m = (*np.array([enu[:2] for enu in station_enus.values()]).mean(axis =0), 0)

for event_id, event in events_to_investigate.items():

    stream = athena.read_event_waveforms(event_id)
    _ , pick_set = athena.get_preferred_origin_and_picks_for_event(event_id)

    hodograms = Hodogram.measure_around_windows(
        stream, Hodogram.get_windows_around_picks(pick_set, 0.05, 0.20)
    )

    hint = OptimizingRelocatorHint(
        pick_errors=pick_errors,
        hodograms=hodograms,
        locator_options={"maxfev": 2000, "maxiter": 2000},
    )
    objective = make_objective_function(pick_set, station_enus, relocator, hint)
    enut = (*event['enu'], event['origin'].time.timestamp)
    dx = np.linspace(-1000,1,1002)
    fig,ax = plt.subplots(figsize=[6,10])
    ax.plot([objective(enut+np.array([0,0,x,0])) for x in dx],dx+enut[2], 'purple')
    ax.set_ylabel('depth (m)')
    ax.set_xlabel('objective function')
    fig.savefig(f'{event_id}.png')
    plt.close(fig)











