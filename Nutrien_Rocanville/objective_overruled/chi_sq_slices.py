import matplotlib

matplotlib.use('qt5agg')

import matplotlib.pyplot as plt
import numpy as np

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.hodogram import Hodogram
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.numerics.relocation.optimizing_relocator import OptimizingRelocatorHint
from NocMeta.Meta import NOC_META

from objective_function import make_objective_function
from read_inputs import station_enus, INV,  LOGIKDATA, get_optimizing_relocator, read_located_events

ATHENA_CODE = 'NTR_RCN'
athena = AthenaClient.from_noc_meta(NOC_META, ATHENA_CODE)
pick_error = {Phase.P: 0.010, Phase.S: 0.020}
pick_errors = {
    (nsl, ph): pick_error[ph] for nsl in NSL.iter_inv(INV) for ph in [Phase.P, Phase.S]
}
pick_errors_ms = {
    (nsl, ph): 1000*pick_error[ph] for nsl in NSL.iter_inv(INV) for ph in [Phase.P, Phase.S]
}

loki_events = read_located_events(LOGIKDATA.joinpath('Located','Loki-i'))

relocator = get_optimizing_relocator()

events_to_investigate = {ev_id: ev for ev_id, ev in loki_events.items() if ev['vrt_error']<0.1}

enu_ref_m = (*np.array([enu[:2] for enu in station_enus.values()]).mean(axis =0), 0)


for event_id, event in list(events_to_investigate.items())[2:]:

    stream = athena.read_event_waveforms(event_id)
    _ , pick_set = athena.get_preferred_origin_and_picks_for_event(event_id)

    hodograms = Hodogram.measure_around_windows(
        stream, Hodogram.get_windows_around_picks(pick_set, 0.05, 0.20)
    )

    hint = OptimizingRelocatorHint(
        pick_errors=pick_errors,
        hodograms=hodograms,
        locator_options={"maxfev": 2000, "maxiter": 2000},
    )
    objective = make_objective_function(pick_set, station_enus, relocator, hint)
    enut = (*event['enu'], event['origin'].time.timestamp)
    dx = dy = np.linspace(-2000,2000,201)
    dz = np.linspace(0,-1100, 12)
    slices = []
    for iz,z in enumerate(dz):
        print(iz)
        slices.append(np.zeros([201,201]))
        for ix, x in enumerate(dx):
            for iy, y in enumerate(dy):
                slices[iz][ix,iy] = objective(enut + np.array([x,y,z,0]))
    smin = min(np.amin(slice) for slice in slices)
    smax = max(np.amax(slice) for slice in slices)
    fig,ax = plt.subplots(4,3,figsize=[10,12], sharex=True, sharey=True)
    for iz, z in enumerate(dz):
        ax[iz // 3, iz % 3].set_aspect('equal')
        ax[iz//3, iz%3].set_title(f'Elevation = {(enut[2] + z):.0f} m'.replace('-', '$-$'))
        pc = ax[iz//3, iz%3].pcolor(dx, dy, np.log10(slices[iz]).T, vmin=np.log10(smin), vmax=np.log10(smax), cmap='turbo')
    plt.savefig(f'objective_function_slices_{event_id}')
    plt.close(fig)










