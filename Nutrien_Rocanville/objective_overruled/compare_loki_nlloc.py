import matplotlib.pyplot as plt
import numpy as np

from read_inputs import LOGIKDATA, read_located_events

nlloc_dir = LOGIKDATA.joinpath('Located', 'Nlloc')
lokii_dir = LOGIKDATA.joinpath('Located', 'Loki-i')

loc_nlloc = read_located_events(nlloc_dir)
loc_lokii = read_located_events(lokii_dir)

fig, ax = plt.subplots()
fig_e, (ax_e_l, ax_e_n) = plt.subplots(1,2, figsize=[8,8],sharey=True, sharex=True)
loki_kwargs = {'color': 'seagreen', 'zorder': 3, 'mec': '0.2', 'marker': 'o', 'alpha': 0.5, 'lw':0}
nlloc_kwargs = {**loki_kwargs, 'color': 'mediumorchid', 'zorder': 1,}

for loc, kwargs, ax_e in zip([loc_nlloc, loc_lokii], [nlloc_kwargs, loki_kwargs], [ax_e_n, ax_e_l]):
	enus = np.array([e['enu'] for e in loc.values()])
	v_error = np.array([e['vrt_error'] for e in loc.values()])
	n_picks = np.array([e['n_picks'] for e in loc.values()])
	ax.plot(enus[:, 0], enus[:, 2], **kwargs)
	scatter = ax_e.scatter(n_picks, enus[:,2], c=n_picks, cmap='turbo', alpha = 0.6, edgecolor='0.2', marker='o', vmin=5, vmax=20)
	# ax_e.set_xscale('log')

cbar = fig_e.colorbar(scatter, orientation='horizontal', ax=[ax_e_l ,ax_e_n])
cbar.set_label('# of picks')
ax_e_l.set_ylim([-2000,600])
plt.show()