import json
from pathlib import Path

sp_diagnostics = {}
for json_file in Path('json').glob('*.json'):
	with open(json_file) as f:
		sp_diagnostics[json_file.stem.split('_')[-1]] = json.load(f)


