import glob
import matplotlib.pyplot as plt
import numpy as np
import pickle

pickle_dir = r"\\192.168.238.150\data3\O&G\Projects\Tourmaline Historical Reprocessing\MT_pickle_jar"
pickle_files = glob.glob(f"{pickle_dir}//4*.pickle")
n_mt = len(pickle_files)
r_values, cns, similarities = np.zeros(n_mt), np.zeros(n_mt), np.zeros(n_mt)
for i_pickle, pickle_file in enumerate(pickle_files):
    f = open(pickle_file, "rb")
    pickle_out = pickle.load(f)
    f.close()
    DC_solution = pickle_out["eveDict"]["event"]["origin"]["custom MT"]["Artemis DC"]
    FP_solution = pickle_out["eveDict"]["event"]["origin"]["custom MT"][
        "Fault Plane Solution"
    ]
    r_values[i_pickle] = DC_solution["Pearson R"]
    cns[i_pickle] = DC_solution["Condition Number"]
    dc_mt = np.array(
        [
            [DC_solution["Mee"], DC_solution["Men"], DC_solution["Mez"]],
            [DC_solution["Men"], DC_solution["Mnn"], DC_solution["Mnz"]],
            [DC_solution["Mez"], DC_solution["Mnz"], DC_solution["Mzz"]],
        ]
    )
    fp_mt = np.array(
        [
            [FP_solution["Mee"], FP_solution["Men"], FP_solution["Mez"]],
            [FP_solution["Men"], FP_solution["Mnn"], FP_solution["Mnz"]],
            [FP_solution["Mez"], FP_solution["Mnz"], FP_solution["Mzz"]],
        ]
    )
    similarities[i_pickle] = np.tensordot(dc_mt, fp_mt)

bins = np.arange(0, 1.0001, 0.02)
fig_r, ax_r = plt.subplots()
plt.hist(r_values, bins=bins, facecolor="firebrick", edgecolor="0.2")
ax_r.set_xlabel("Pearson R")
fig_s, ax_s = plt.subplots()
plt.hist(similarities, bins=bins, facecolor="royalblue", edgecolor="0.2")
ax_s.set_xlabel("similarity to FPS")
fig_c, ax_c = plt.subplots()
plt.hist(cns, bins=np.arange(0, 10.0001, 0.2), facecolor="forestgreen", edgecolor="0.2")
ax_c.set_xlabel("condition number")
for ax in ax_r, ax_s, ax_c:
    ax.set_ylabel("count")
fig_r.savefig("r_value.png")
fig_s.savefig("similarity.png")
fig_c.savefig("condition_number.png")
