import numpy as np
import utm
import glob
from obspy import UTCDateTime

pick_dir = "picks//"
pfiles = glob.glob(pick_dir + "*.picks")

pickfile = {}
for pfile in pfiles:
    event_id, timestr = pfile.split("\\")[-1].split(".picks")[0].split("_")
    ymd, hms, mu_sec = timestr.split(".")
    pickfile[pfile.split("\\")[-1]] = {
        "serial_time": UTCDateTime(ymd + hms + "." + mu_sec),
        "event_id": event_id,
    }


def read_picks(event_time):
    time_tolerance = 60
    ifile = np.where(
        abs(
            np.array([v["serial_time"] for (k, v) in pickfile.items()])
            - UTCDateTime(event_time)
        )
        < time_tolerance
    )[0]
    if len(ifile > 0):
        f = open(pfiles[ifile[0]])
        lines = f.readlines()
        f.close()
        picks = {}
        picks["id"] = pickfile[pfiles[ifile[0]].split("\\")[-1]]["event_id"]
        picks["t0"] = pickfile[pfiles[ifile[0]].split("\\")[-1]]["serial_time"]
        for line in lines:
            lspl = line.split(",")
            station = lspl[0] + ".CPZ"
            if not (station in picks):
                picks[station] = {}

            if lspl[1] == "P":
                picks[station]["P"] = float(lspl[2])
            elif lspl[1] == "S":
                picks[station]["S"] = float(lspl[2])
        return picks


def read_events():
    f = open(r"1_DataFetch\EventCatalogFiltered.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        events[lspl[0]] = {
            "t0": UTCDateTime(lspl[1]),
            "latitude": float(lspl[2]),
            "longitude": float(lspl[3]),
            "depth": float(lspl[4]),
            "Mw": float(lspl[5]),
        }
    return events


def read_texnet(filename):
    f = open(filename)
    haed = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        events[event_id] = {
            "timestamp": lspl[2] + "T" + lspl[3] + "Z",
            "magnitude": float(lspl[4]),
            "latitude": float(lspl[6]),
            "longitude": float(lspl[8]),
            "depth_km": float(lspl[10]),
        }
    return events


def read_hp_locations(filename):
    f = open(filename)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        id = lspl[0]
        events[id] = {
            "external_id": lspl[1].split()[0],
            "UTC_time": UTCDateTime(float(lspl[2])),
            "latitude": float(lspl[3]),
            "longitude": float(lspl[4]),
            "depth_km": float(lspl[5]),
            "magnitude": float(lspl[6]),
        }
    return events


mt_file = r"Catalogs/events_Apr1_to_Aug4.csv"


def read_mts(mt_file):
    # mt file is a csv file generated from athena output of "details" csv
    f = open(mt_file)
    headers = f.readline().split(",")
    lines = f.readlines()
    f.close()
    events = {}
    i_dip = [
        i for i, header in enumerate(headers) if "moment tensor np1 dip" in header
    ][0]
    i_strike = [
        i for i, header in enumerate(headers) if "moment tensor np1 strike" in header
    ][0]
    i_rake = [
        i for i, header in enumerate(headers) if "moment tensor np1 rake" in header
    ][0]
    for line in lines:
        lspl = line.split(",")
        if lspl[i_dip] != "":
            id = lspl[-18]
            events[id] = {
                "UTCDateTime": UTCDateTime(lspl[3]),
                "latitude": float(lspl[6]),
                "longitude": float(lspl[7]),
                "depth_km": float(lspl[12]),
                "dip": float(lspl[i_dip]),
                "strike": float(lspl[i_strike]),
                "rake": float(lspl[i_rake]),
            }
    return events


def read_pick_table():
    f = open(r"2_WaveformProcessing\PSPicksFull.Table.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    picks = {}
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        if event_id not in picks:
            picks[event_id] = {}
            picks[event_id]["t0"] = UTCDateTime(float(lspl[8]))
        station = lspl[7] + "." + lspl[12]
        if station not in picks:
            print(picks[event_id])
            picks[event_id][station] = {}
        if lspl[10] == "S":
            picks[event_id][station]["S"] = float(lspl[11])
        if lspl[10] == "P":
            picks[event_id][station]["P"] = float(lspl[11])
    return picks


def read_channels():
    f = open("inputs//channels_pruned.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    line = lines[10]
    for line in lines:
        lspl = line.split(",")
        network, station_code = lspl[:2]
        name = network + "." + station_code
        if not (name in stations):
            stations[name] = {}
            latitude, longitude, stations[name]["elevation"] = [
                float(_) for _ in lspl[7:10]
            ]
            (
                stations[name]["easting"],
                stations[name]["northing"],
                d1,
                d2,
            ) = utm.from_latlon(latitude, longitude)
        channel = lspl[3]
        stations[name][channel] = {}
        stations[name][channel]["azimuth"], stations[name][channel]["dip"] = [
            float(_) for _ in lspl[10:12]
        ]
    return stations


def read_velocity_model(vmodel_file):
    f = open(vmodel_file)
    f.readline()
    topline = f.readline()
    lines = f.readlines()
    f.close()
    nl = len(lines)

    lspl = topline.split()
    vp_km_s = float(lspl[0])
    velocity_model = [
        {
            "vp": vp_km_s * 1000.0,
            "vs": vp_km_s * 1000.0 / np.sqrt(3.0),
            "rho": 310 * (vp_km_s * 1000.0) ** 0.25,
        }
    ]
    for line in lines:
        vp_km_s, top_km = [float(_) for _ in line.split()]
        velocity_model.append(
            {
                "vp": vp_km_s * 1000.0,
                "vs": vp_km_s * 1000.0 / np.sqrt(3.0),
                "rho": 310 * (vp_km_s * 1000.0) ** 0.25,
                "top": -top_km * 1000.0,
            }
        )
    return velocity_model
