import matplotlib.pyplot as plt
import numpy as np
from obspy import UTCDateTime
import mplstereonet as mpls

from sms_moment_tensor.MT_math import conjugate_sdr, decompose_MT, sdr_to_mt

PI = np.pi
D2R = PI / 180

mts = {}
f = open("events.csv")
headers = f.readline().split(",")
lines = f.readlines()
f.close()
events = {}
i_dip = [i for i, header in enumerate(headers) if "moment tensor np1 dip" in header][0]
i_strike = [
    i for i, header in enumerate(headers) if "moment tensor np1 strike" in header
][0]
i_rake = [i for i, header in enumerate(headers) if "moment tensor np1 rake" in header][
    0
]
for line in lines:
    lspl = line.split(",")
    if lspl[i_dip] != "":
        id = lspl[-18]
        mts[id] = {
            "UTCDateTime": UTCDateTime(lspl[3]),
            "latitude": float(lspl[6]),
            "longitude": float(lspl[7]),
            "depth_km": float(lspl[12]),
            "dip": float(lspl[i_dip]),
            "strike": float(lspl[i_strike]),
            "rake": float(lspl[i_rake]),
        }


def plot_strike_dip_rake_rosettes(strikes, dips, rakes, color="steelblue"):
    strikes_180 = strikes % 180
    strikes_360 = strikes_180 + 180
    mirrored_strikes = np.hstack([strikes_180, strikes_360])
    fig = plt.figure(figsize=[16, 5])
    ax_strike = fig.add_axes([0.05, 0.1, 0.38, 0.8], projection="polar")
    ax_dip = fig.add_axes([0.44, 0.1, 0.12, 0.8], projection="polar")
    ax_rake = fig.add_axes([0.57, 0.1, 0.38, 0.8], projection="polar")
    bin_degrees = 5
    strike_bins = np.arange(0, 360.0001, bin_degrees)
    n_strike, _ = np.histogram(mirrored_strikes, strike_bins)
    ax_strike.bar(
        D2R * (strike_bins[:-1] + bin_degrees / 2),
        n_strike,
        width=D2R * bin_degrees,
        color=color,
    )
    ax_strike.set_theta_zero_location("N")
    ax_strike.set_theta_direction(-1)
    ax_strike.set_thetagrids(
        np.arange(0, 360, 45), ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
    )

    dip_bins = np.arange(0, 90.0001, bin_degrees)
    n_dip, _ = np.histogram(dips, dip_bins)
    ax_dip.bar(
        D2R * (dip_bins[:-1] + bin_degrees / 2),
        n_dip,
        width=D2R * bin_degrees,
        color=color,
    )
    ax_dip.set_theta_direction(-1)
    ax_dip.set_thetamax(90)

    rake_bins = np.arange(-180, 180.0001, bin_degrees)
    n_rake, _ = np.histogram(rakes, rake_bins)
    ax_rake.bar(
        D2R * (rake_bins[:-1] + bin_degrees / 2),
        n_rake,
        width=D2R * bin_degrees,
        color=color,
    )
    ax_rake.set_thetagrids(np.arange(0, 360, 90), ["LL", "TH", "RL", "NR"])
    return fig


n_mt = len(mts)
strike, dip, rake = np.zeros(n_mt), np.zeros(n_mt), np.zeros(n_mt)
aux_strike, aux_dip, aux_rake = np.zeros(n_mt), np.zeros(n_mt), np.zeros(n_mt)
for i_mt, (id, mt) in enumerate(mts.items()):
    strike1, dip1, rake1 = mt["strike"], mt["dip"], mt["rake"]
    strike2, dip2, rake2 = conjugate_sdr(strike1, dip1, rake1)
    if abs(strike1 % 180 - 65) < 20:
        strike[i_mt] = strike1
        dip[i_mt] = dip1
        rake[i_mt] = rake1
        aux_strike[i_mt] = strike2
        aux_dip[i_mt] = dip2
        aux_rake[i_mt] = rake2
    else:
        strike[i_mt] = strike2
        dip[i_mt] = dip2
        rake[i_mt] = rake2
        aux_strike[i_mt] = strike1
        aux_dip[i_mt] = dip1
        aux_rake[i_mt] = rake1
    mts[id] = {**mt, **decompose_MT(sdr_to_mt(strike1, dip1, rake1))}


fig = plot_strike_dip_rake_rosettes(strike, dip, rake, color="firebrick")

fig.savefig("fault_plane_sdr.png")

f_pbt, (ax_p, ax_b, ax_t) = mpls.subplots(1, 3, figsize=[16, 5])

ax_p.density_contourf(
    [v["p_plunge"] for v in mts.values()],
    [v["p_trend"] for v in mts.values()],
    measurement="lines",
    cmap="Greens",
    alpha=0.6,
)
ax_b.density_contourf(
    [v["b_plunge"] for v in mts.values()],
    [v["b_trend"] for v in mts.values()],
    measurement="lines",
    cmap="Greens",
    alpha=0.6,
)
ax_t.density_contourf(
    [v["t_plunge"] for v in mts.values()],
    [v["t_trend"] for v in mts.values()],
    measurement="lines",
    cmap="Greens",
    alpha=0.6,
)
ax_p.line(
    [v["p_plunge"] for v in mts.values()], [v["p_trend"] for v in mts.values()], "k."
)
ax_b.line(
    [v["b_plunge"] for v in mts.values()], [v["b_trend"] for v in mts.values()], "k."
)
ax_t.line(
    [v["t_plunge"] for v in mts.values()], [v["t_trend"] for v in mts.values()], "k."
)
for ax in (ax_p, ax_b, ax_t):
    ax.grid()

f_pbt.savefig("strain_axes.png")
