import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt

from obspy import read, UTCDateTime

from randomize_seismicity.moment_tensor import dc
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_moment_tensor.MT_math import sdr_to_mt, mt_vector_to_matrix
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)

plt.close("all")

dt = 0.001  # sample rate
n_nodes = 1
noise_level = base_noise / np.sqrt(n_nodes)


def sqa(x):
    return np.squeeze(np.array(x))


# creates trace Stats for the playback code above
def createTraceStats(network, n1, o1, d1, component):
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


velocity_model = [{"rho": 2734.0, "vp": 6052.0, "vs": 3303.0, "color": "indigo"}]

Q = {"P": 70.0, "S": 50.0}


seed_dir = "strike_to_dip_slip//"

f = open(seed_dir + "stations.csv")
head = f.readline()
lines = f.readlines()
f.close()


stations = {}
for line in lines:
    lspl = line.split(",")
    stations[lspl[0]] = {}
    stations[lspl[0]]["x"] = float(lspl[3])
    stations[lspl[0]]["y"] = float(lspl[4])
    stations[lspl[0]]["z"] = float(lspl[5])

amps = np.zeros(len(stations))
f = open(seed_dir + "synthetic_catalog.csv")
head = f.readline()
lines = f.readlines()
f.close()
g = open(seed_dir + "inverted_catalog_out.csv", "w")
g.write(head[:-1] + ",gn11, gn22, gn33,gn12,gn13,gn23,rsq_gn,gn_similarity,")
g.write("dc11,dc22,dc33,dc12,dc13,dc23,rsq_dc,dc_similarity\n")

lspl = lines[0].split(",")
for line in lines:
    lspl = line.split(",")
    st = read(
        seed_dir + "synthetic" + lspl[0] + "T" + lspl[1].replace(":", "-") + "Z.mseed"
    )
    A_matrix = []
    for i_station, station in enumerate(stations):
        p_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "S")
        cos_incoming = np.sqrt(
            1.0
            - (
                p_raypath["hrz_slowness"]["x"] ** 2
                + p_raypath["hrz_slowness"]["y"] ** 2
            )
            * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
        )
        trace = st[i_station].data
        i_p_time = int(p_raypath["traveltime"] // time_series[1])
        i_peak = np.argmax(abs(trace[i_p_time : i_p_time + 20])) + i_p_time
        amps[i_station] = (
            back_project_amplitude(p_raypath, s_raypath, trace[i_peak], "P", Q=80)
            / cos_incoming
        )
        A_matrix.append(inversion_matrix_row(p_raypath, "P"))
    mt_gen, cn_gen, rsq_gen, mt_dc, cn_dev, rsq_dc = solve_moment_tensor(A_matrix, amps)
    MT_in = mt_vector_to_matrix([float(s) for s in lspl[7:13]])
    MT_out = mt_vector_to_matrix(mt_gen)
    MT_dc = mt_vector_to_matrix(mt_dc)
    gn_similarity = (
        np.tensordot(MT_out, MT_in) / np.linalg.norm(MT_in) / np.linalg.norm(MT_out)
    )
    dc_similarity = (
        np.tensordot(MT_dc, MT_in) / np.linalg.norm(MT_in) / np.linalg.norm(MT_dc)
    )
    g.write(line[:-1])
    g.write(
        6 * ",%.3f" % (mt_gen[0], mt_gen[1], mt_gen[2], mt_gen[3], mt_gen[4], mt_gen[5])
    )

    g.write(2 * ",%.3f" % (rsq_gen, gn_similarity))
    g.write(6 * ",%.3f" % (mt_dc[0], mt_dc[1], mt_dc[2], mt_dc[3], mt_dc[4], mt_dc[5]))
    g.write(2 * ",%.3f" % (rsq_dc, dc_similarity))
    g.write("\n")
g.close()
