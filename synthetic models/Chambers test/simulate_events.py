import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import glob
import os

from obspy.imaging.mopad_wrapper import beach
from obspy import Stream, Trace, UTCDateTime
from obspy.core.trace import Stats

from randomize_seismicity.moment_tensor import dc

from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.raytrace import isotropic_ray_trace


plt.close("all")

dt = 0.001  # sample rate
base_noise = 1.0e-9
n_nodes = 1
noise_level = base_noise / np.sqrt(n_nodes)

stationsCSV = "kit_stations.csv"


def sqa(x):
    return np.squeeze(np.array(x))


f = open(stationsCSV)
head = f.readline()
lines = f.readlines()
f.close()

stations = {}
for line in lines:
    lspl = line.split(",")
    station_name = lspl[0]
    easting = float(lspl[1])
    northing = float(lspl[2])
    tvdss = float(lspl[3])
    stations[station_name] = {"x": easting, "y": northing, "z": tvdss}


# creates trace Stats for the playback code above
def createTraceStats(network, n1, o1, d1, component):
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


velocityModel = [{"rho": 2734.0, "vp": 6052.0, "vs": 3303.0, "color": "indigo"}]

Q = {"P": 70.0, "S": 50.0}


MT = [
    np.matrix([[1, 0, 0], [0, 0, 0], [0, 0, 0]]),
    np.matrix([[0, 0, 0], [0, 1, 0], [0, 0, 0]]),
    np.matrix([[0, 0, 0], [0, 0, 0], [0, 0, 1]]),
    np.matrix([[0, 1, 0], [1, 0, 0], [0, 0, 0]]) / np.sqrt(2),
    np.matrix([[0, 0, 1], [0, 0, 0], [1, 0, 0]]) / np.sqrt(2),
    np.matrix([[0, 0, 0], [0, 0, 1], [0, 1, 0]]) / np.sqrt(2),
]

seedname = ["M11", "M22", "M33", "M12", "M13", "M23"]

outdir = "half_space_model"

if not (os.path.isdir(outdir)):
    os.mkdir(outdir)
g = open(outdir + "//run_parameters.txt", "w")
g.write("station file: " + stationsCSV + "\n")
g.write("noise level (m/s): " + str(base_noise) + "\n")
g.write("nodes per superstation: " + str(n_nodes) + "\n")
g.close()


g = open(outdir + "//synthetic_catalog.csv", "w")
g.write("date,time,x,y,z,magnitude,stress drop,m11,m22,m33,m12,m13,m23\n")
for ievent in range(len(MT)):
    source = {
        "x": 0,
        "y": 0,
        "z": 3000,
        "moment_magnitude": 0,
        "stress_drop": 1.0e6,  # static stress drop
        "moment_tensor": MT[ievent],
    }

    m11, m12, m13, d1, m22, m23, d2, d3, m33 = sqa(source["moment_tensor"].reshape(9))
    source["moment_tensor"] = source["moment_tensor"] / np.linalg.norm(
        source["moment_tensor"]
    )
    top, bottom = -1000, 3500

    time_series = np.arange(0, 4.00, dt)
    waveforms = []
    max_waveform = 0
    st = Stream()
    for station in stations:
        pRaypath = isotropic_ray_trace(source, stations[station], velocityModel, "P")
        sRaypath = isotropic_ray_trace(source, stations[station], velocityModel, "S")
        waveforms.append(
            get_response(
                pRaypath,
                sRaypath,
                source,
                stations[station],
                Q,
                time_series,
                noise_level,
            )
        )
        mwave = max(
            [
                max(abs(waveforms[-1]["n"])),
                max(abs(waveforms[-1]["e"])),
                max(abs(waveforms[-1]["d"])),
            ]
        )
        if max_waveform < mwave:
            max_waveform = mwave

    for ii in range(len(waveforms)):
        seedtime = UTCDateTime.now()
        channel_obj = {
            "component": "Z",
            "channel": "CPZ",
            "station": station,
            "network": "CV",
            "location": "00",
        }
        xStats = createTraceStats("CV", len(time_series), seedtime, dt, channel_obj)
        tr = Trace(
            data=1e8 * waveforms[ii]["d"], header=xStats
        )  # 1e8 is the scaling factor deduced from the SEG2 data
        st += tr
    outfile = (outdir + "//" + seedname[ievent] + "_synthetic.mseed").replace(":", "-")

    st.write(outfile)
    dat, tim = str(seedtime).split("T")
    g.write(dat + "," + tim[:-1] + ",")
    g.write("%.1f,%.1f,%.1f," % (source["x"], source["y"], source["z"]))
    g.write("%.3f,%.4e," % (source["moment_magnitude"], source["stress_drop"]))
    g.write("%.3f,%.3f,%.3f,%.3f,%.3f,%.3f\n" % (m11, m22, m33, m12, m13, m23))

g.close()
