import glob
import numpy as np
import matplotlib.pyplot as plt
from obspy import read, UTCDateTime
import utm


def read_events_from_Do():
    f = open(
        "O:\\O&G\\Projects\\P00016 (Paramount 2-28 PFI)\\"
        + "Magnitude\\5_Spectral_Fitting\\Tables\\Do_Final_mod.csv"
    )
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        id = lspl[0].split(".")[0]
        events[id] = {}
        events[id]["UTC"] = UTCDateTime(float(lspl[1]))
        lat, lon = [float(s) for s in lspl[2:4]]
        events[id]["easting"], events[id]["northing"], dum1, dum2 = utm.from_latlon(
            lat, lon
        )
        events[id]["depth"] = 1000 * float(lspl[4])
        events[id]["Mw"] = float(lspl[119])
    return events


def read_events():
    # f = open(
    #     "O:\\O&G\\Projects\\P00016 (Paramount 2-28 PFI)\\Catalogs\\"
    #     + "reloc_events_mod-it6-102_5m_simpleStack_wMw.csv"
    # )
    f = open(
        "C:\\Users\\adambaig\\Project\\Paramount 2-28\\inputs\\"
        # + "reloc_events_mod-it6-102_1m_deg_wStageId_trim_noDup_wMw.csv"
        + "reloc_events_merge_final_it2_2m_wStageId_trim_wMag_wError.csv"
    )
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        id = lspl[0].split(".")[0]
        events[id] = {}
        events[id]["UTC"] = lspl[1]

        # for reading in reloc_events_mod-it6-102_1m_deg_wStageId_trim_noDup_wMw.csv
        # events[id]["easting"], events[id]["northing"], d1, d2 = utm.from_latlon(
        #     float(lspl[3]), float(lspl[2])
        # )
        # events[id]["depth"] = 1000 * float(lspl[4])
        # events[id]["Mw"] = float(lspl[12])
        # events[id]["stack_amp"] = float(lspl[5])
        # events[id]["filt_amp"] = float(lspl[6])
        # events[id]["amp"] = float(lspl[7])
        # events[id]["well"] = lspl[8]
        # events[id]["stage"] = lspl[10]
        # events[id]["spec_fit_Mw"] = float(lspl[12])

        events[id]["easting"] = float(lspl[2])
        events[id]["northing"] = float(lspl[3])
        events[id]["depth"] = float(lspl[4])
        events[id]["Mw"] = float(lspl[10])
        events[id]["amp"] = float(lspl[5])
        events[id]["well"] = lspl[6]
        events[id]["stage"] = lspl[9]
        events[id]["spec_fit_Mw"] = float(lspl[14])

    # f = open(
    #     "C:\\Users\\adambaig\\Project\\Paramount 2-28\\inputs\\events_trim_deg.csv"
    # )
    # head = f.readline()
    # lines = f.readlines()
    # f.close()
    # for line in lines:
    #     lspl = line.split(",")
    #     id = lspl[0].split(".")[0]
    #     events[id]["well"] = lspl[8]
    #     events[id]["stage"] = lspl[10]
    return events


# pick_dir = "O:\\O&G\\Projects\\P00016 (Paramount 2-28 PFI)\\EventReview\\Final_Picks\\"

pick_dir = "O:\\O&G\\Projects\\P00016 (Paramount 2-28 PFI)\\Picks\\ampPicks\\"

pfiles = glob.glob(pick_dir + "*.picks")

pickfile = {}
for pfile in pfiles:
    event_id, timestr = pfile.split("\\")[-1].split(".picks")[0].split("_")
    ymd, hms, mu_sec = timestr.split(".")
    pickfile[pfile.split("\\")[-1]] = {
        "serial_time": UTCDateTime(ymd + hms + "." + mu_sec).timestamp,
        "event_id": event_id,
    }


amp_pick_dir = (
    "C:\\Users\\adambaig\\Project\\Paramount 2-28\\MTI\\picks\\pfi\\allPicks\\"
)

amp_pfiles = glob.glob(amp_pick_dir + "*.picks")

amp_pickfile = {}
for pfile in amp_pfiles:
    event_id, timestr = pfile.split("\\")[-1].split(".picks")[0].split("_")
    ymd, hms, mu_sec, dum = timestr.split(".")
    amp_pickfile[pfile.split("\\")[-1]] = {
        "serial_time": UTCDateTime(ymd + hms + "." + mu_sec).timestamp,
        "event_id": str(int(event_id)),
    }


def read_wells():
    dr = "C:\\Users\\adambaig\\Project\\Paramount 2-28\\inputs\\wells\\"
    wells = {}
    for well_file in glob.glob(dr + "DS_04*"):
        ext = well_file.split(".")[-1]
        well = well_file.split("~")[1]
        wells[well] = {}

        f = open(well_file)
        for ii in range(24):
            dumhead = f.readline()
        head = f.readline()
        lines = f.readlines()
        f.close()
        nlines = len(lines)
        wells[well]["tvdss"] = np.zeros(nlines)
        wells[well]["easting"] = np.zeros(nlines)
        wells[well]["northing"] = np.zeros(nlines)
        ii = -1
        for line in lines:
            ii += 1
            if ext == "csv":
                lspl = line.split(",")
            elif ext == "txt":
                lspl = line.split()
            wells[well]["tvdss"][ii] = float(lspl[4])
            wells[well]["easting"][ii] = float(lspl[9])
            wells[well]["northing"][ii] = float(lspl[10])
    return wells


def read_picks(event_id):
    ifile = np.where(
        np.array([int(v["event_id"]) for (k, v) in pickfile.items()]) == int(event_id)
    )[0]
    if len(ifile > 0):
        f = open(pfiles[ifile[0]])
        lines = f.readlines()
        f.close()
        picks = {}
        picks["id"] = pickfile[pfiles[ifile[0]].split("\\")[-1]]["event_id"]
        picks["t0"] = pickfile[pfiles[ifile[0]].split("\\")[-1]]["serial_time"]
        for line in lines:
            lspl = line.split(",")
            station = lspl[0].split(".VC")[0]
            picks[station] = {}

            if lspl[1] == "P":
                picks[station]["P"] = float(lspl[2])
            elif lspl[1] == "S":
                picks[station]["S"] = float(lspl[2])
        return picks


def read_amp_picks(event_id, dr=amp_pick_dir):
    ifile = np.where(
        np.array([int(v["event_id"]) for (k, v) in amp_pickfile.items()])
        == int(event_id)
    )[0]
    if len(ifile > 0):
        f = open(amp_pfiles[ifile[0]])
        lines = f.readlines()
        f.close()
        picks = {}
        picks["id"] = amp_pickfile[amp_pfiles[ifile[0]].split("\\")[-1]]["event_id"]
        picks["t0"] = amp_pickfile[amp_pfiles[ifile[0]].split("\\")[-1]]["serial_time"]
        for line in lines:
            lspl = line.split(",")
            station = lspl[0].split(".VC")[0]
            if not (station in picks):
                picks[station] = {}
            picks[station][lspl[1]] = float(lspl[2])

        return picks


def read_waveform(id, t_start, trace_length, raw=False):
    t_end = t_start + trace_length
    if raw:
        # dr = (
        #     "O:\\O&G\\Projects\\P00016 (Paramount 2-28 PFI)\\"
        #     + "Catalogs\\sembStack_trimmedSeeds\\"
        # )
        # filename = glob.glob(dr + str(id).zfill(8) + "*.seed")[0]
        # dr = "O:\\O&G\\Projects\\P00016 (Paramount 2-28 PFI)\\Magnitude\\1_DataFetch\\mSeeds\\"
        dr = "C:\\Users\\adambaig\\Project\\Paramount 2-28\\mSeeds\\"
        filename = glob.glob(dr + str(id) + "*.seed")[0]
    else:
        dr = (
            "O:\\O&G\\Projects\\P00016 (Paramount 2-28 PFI)\\Magnitude\\"
            + "2_WaveformProcessing\\mSeeds_processed\\"
        )
        filename = dr + str(id) + "_Processed.MSEED"
    st = read(filename)
    return st.trim(starttime=t_start, endtime=t_end)


def read_stations():
    f = open(
        "C:\\Users\\adambaig\\Project\\Paramount 2-28\\"
        + "magnitude calibration\\228_stations_epsg26911.csv"
    )
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        stations[lspl[0]] = {}
        stations[lspl[0]]["e"] = float(lspl[1])
        stations[lspl[0]]["n"] = float(lspl[2])
        stations[lspl[0]]["z"] = -float(lspl[3])

    return stations


def read_velocity_model(mode="pick"):
    if mode == "pick":
        f = open("bestvp-000554.csv")
        lines = f.readlines()
        f.close()
        vp = []
        depth = [-1000]
        for line in lines:
            depth.append(depth[-1] + 10)
            vp.append(float(line) * 1.02)
        depth[2]
        fig, ax = plt.subplots()
        ax.plot(depth[:-1], vp, "steelblue")
        points = fig.ginput(-1)
        nlayers = len(points) // 2
        velocity_model = []
        for ii in range(nlayers):
            layer = {}
            layer["vp"] = 0.5 * (points[2 * ii][1] + points[2 * ii + 1][1])
            layer["vs"] = layer["vp"] / np.sqrt(3)
            layer["rho"] = 310 * layer["vp"] ** 0.25
            if ii > 0:
                layer["top"] = 0.5 * (points[2 * ii - 1][0] + points[2 * ii][0])
            velocity_model.append(layer)
        x1, x2 = ax.get_xlim()
        ax.plot(
            [x1, velocity_model[1]["top"]],
            [velocity_model[0]["vp"], velocity_model[0]["vp"]],
            color="firebrick",
        )
        for ii in range(len(velocity_model) - 2):
            ax.plot(
                [velocity_model[ii + 1]["top"], velocity_model[ii + 1]["top"]],
                [velocity_model[ii]["vp"], velocity_model[ii + 1]["vp"]],
                color="firebrick",
            )
            ax.plot(
                [velocity_model[ii + 1]["top"], velocity_model[ii + 2]["top"]],
                [velocity_model[ii + 1]["vp"], velocity_model[ii + 1]["vp"]],
                color="firebrick",
            )
        ax.plot(
            [velocity_model[-1]["top"], velocity_model[-1]["top"]],
            [velocity_model[-2]["vp"], velocity_model[-1]["vp"]],
            color="firebrick",
        )
        ax.plot(
            [velocity_model[-1]["top"], x2],
            [velocity_model[-1]["vp"], velocity_model[-1]["vp"]],
            color="firebrick",
        )
        ax.set_xlim([x1, x2])
        # plt.show()
    elif mode == "no lvl":
        velocity_model = [
            {
                "vp": 3358.0274291654287,
                "vs": 1938.7580401748073,
                "rho": 2359.8410615626417,
            },
            {
                "vp": 4185.545219483219,
                "vs": 2416.5256591739876,
                "rho": 2493.444485244485,
                "top": 742.7741935483873,
            },
            {
                "vp": 5169.198441936441,
                "vs": 2984.4381119465984,
                "rho": 2628.5575806284633,
                "top": 1195.193548387097,
            },
            {
                "vp": 5793.74017047817,
                "vs": 3345.01744704032,
                "rho": 2704.5900595984394,
                "top": 2437.8387096774195,
            },
        ]
    else:
        velocity_model = [
            {
                "vp": 3358.0274291654287,
                "vs": 1938.7580401748073,
                "rho": 2359.8410615626417,
            },
            {
                "vp": 4185.545219483219,
                "vs": 2416.5256591739876,
                "rho": 2493.444485244485,
                "top": 742.7741935483873,
            },
            {
                "vp": 5169.198441936441,
                "vs": 2984.4381119465984,
                "rho": 2628.5575806284633,
                "top": 1195.193548387097,
            },
            {
                "vp": 4287.03325037125,
                "vs": 2475.1198011267174,
                "rho": 2508.423765887138,
                "top": 2184.4838709677424,
            },
            {
                "vp": 5793.74017047817,
                "vs": 3345.01744704032,
                "rho": 2704.5900595984394,
                "top": 2437.8387096774195,
            },
        ]
    return velocity_model


def read_pick_snr():
    f = open(
        "C:\\Users\\adambaig\\Project\\Paramount 2-28\\inputs\\Final_picks_snr.csv"
    )
    head = f.readline()
    lines = f.readlines()
    snr = {}
    for line in lines:
        lspl = line.split(",")
        event_id = str(int(lspl[0].split("_")[0]))
        if not (event_id in snr):
            snr[event_id] = {}
        if lspl[5] != "nan":
            sta_ph = lspl[1] + lspl[2]
            snr[event_id][sta_ph] = float(lspl[5])
    return snr


def read_pgv(event="all"):
    pgv_dir = "C:\\Users\\adambaig\\Project\\Paramount 2-28\\inputs\\PGV\\"
    if event == "all":
        csvs = glob.glob(pgv_dir + "*.csv")
        pgv = {}
        csv = csvs[100]
        for csv in csvs:
            event = str(int(csv.split("\\")[-1].split("_")[0]))
            pgv[event] = {}
            f = open(csv)
            lines = f.readlines()
            f.close()
            for line in lines:
                lspl = line.split(",")
                station = lspl[1].split(".VC")[0]
                if lspl[4] != "0":
                    pgv[event][station] = float(lspl[4]) / 59000.0
    return pgv


def read_polarities():
    f = open("polarity_QC//polarities.csv")
    lines = f.readlines()
    f.close()
    polarities = {}
    for line in lines:
        lspl = line.split(",")
        event = str(int(lspl[0].split(".")[0].split("_")[1]))
        if int(lspl[1]) == 0:
            polarities[event] = -1
        else:
            polarities[event] = 1
    return polarities


def read_old_MTs():
    f = open(r"C:\Users\adambaig\Project\Paramount 2-28\MTI\archive\moment_tensors.csv")
    line = f.readline()
    lines = f.readlines()
    f.close()
    moment_tensors = {}
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        moment_tensors[event_id] = {
            "UTC": lspl[1],
            "strike1": float(lspl[2]),
            "dip1": float(lspl[3]),
            "rake1": float(lspl[4]),
            "strike2": float(lspl[5]),
            "dip2": float(lspl[6]),
            "rake2": float(lspl[7]),
            "rsq": float(lspl[8]),
        }
    return moment_tensors
