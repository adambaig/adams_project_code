import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import glob

from obspy import read


from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    solve_moment_tensor,
    inversion_matrix_row,
)
from sms_moment_tensor.MT_math import mt_matrix_to_vector, mt_vector_to_matrix

from read_inputs import read_stations

plt.close("all")

dt = 0.001  # sample rate
base_noise = 1.0e-9
n_nodes = 7
noise_level = base_noise / np.sqrt(n_nodes)


def sqa(x):
    return np.squeeze(np.array(x))


stations = read_stations()

# creates trace Stats for the playback code above
def createTraceStats(network, n1, o1, d1, component):
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


velocity_model = [{"rho": 2734.0, "vp": 6052.0, "vs": 3303.0}]

Q = {"P": 70.0, "S": 50.0}


seed_dir = "half_space_model_lower_noise"
source

source = {
    "e": np.average([v["e"] for k, v in stations.items()]),
    "n": np.average([v["n"] for k, v in stations.items()]),
    "z": -2100,
    "moment_magnitude": 0,
    "stress_drop": 1.0e6,  # static stress drop
}
time_series = np.arange(0, 4.00, dt)
g = open(seed_dir + "//synthetic_catalog.csv")
head = g.readline()
lines = g.readlines()
g.close()

f = open(seed_dir + "//inversions.csv", "w")

f.write(head[:-1] + ",gn11, gn22, gn33,gn12,gn13,gn23,rsq_gn,gn_similarity,")
f.write("dc11,dc22,dc33,dc12,dc13,dc23,rsq_dc,dc_similarity\n")
seedfiles = glob.glob(seed_dir + "//*.mseed")
for i_file, line in enumerate(lines):
    lspl = line.split(",")
    st = read(seedfiles[i_file])
    A_matrix = []
    amps = np.zeros(15)
    for i_station, station in enumerate(stations):
        p_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "S")
        cos_incoming = np.sqrt(
            1.0
            - (
                p_raypath["hrz_slowness"]["e"] ** 2
                + p_raypath["hrz_slowness"]["n"] ** 2
            )
            * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
        )
        trace = st[i_station].data
        i_p_time = int(p_raypath["traveltime"] // time_series[1])
        i_peak = np.argmax(abs(trace[i_p_time : i_p_time + 20])) + i_p_time
        amps[i_station] = (
            back_project_amplitude(p_raypath, s_raypath, trace[i_peak], "P", Q=80)
            / cos_incoming
        )
        A_matrix.append(inversion_matrix_row(p_raypath, "P"))
    mt_gen, cn_gen, rsq_gen, mt_dc, cn_dev, rsq_dc = solve_moment_tensor(A_matrix, amps)
    MT_in = mt_vector_to_matrix([float(s) for s in lspl[7:13]])
    MT_out = mt_vector_to_matrix(mt_gen)
    MT_dc = mt_vector_to_matrix(mt_dc)
    gn_similarity = (
        np.tensordot(MT_out, MT_in) / np.linalg.norm(MT_in) / np.linalg.norm(MT_out)
    )
    dc_similarity = (
        np.tensordot(MT_dc, MT_in) / np.linalg.norm(MT_in) / np.linalg.norm(MT_dc)
    )
    f.write(line[:-1])
    f.write(
        6 * ",%.3f" % (mt_gen[0], mt_gen[1], mt_gen[2], mt_gen[3], mt_gen[4], mt_gen[5])
    )

    f.write(2 * ",%.3f" % (rsq_gen, gn_similarity))
    f.write(6 * ",%.3f" % (mt_dc[0], mt_dc[1], mt_dc[2], mt_dc[3], mt_dc[4], mt_dc[5]))
    f.write(2 * ",%.3f" % (rsq_dc, dc_similarity))
    f.write("\n")
f.close()
