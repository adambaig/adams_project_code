import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import glob

from obspy import read

from randomize_seismicity.moment_tensor import dc

from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    solve_moment_tensor,
    inversion_matrix_row,
)
from sms_moment_tensor.MT_math import mt_matrix_to_vector, mt_vector_to_matrix

from read_inputs import read_stations

plt.close("all")

dt = 0.001  # sample rate
base_noise = 1.0e-9
n_nodes = 7
noise_level = base_noise / np.sqrt(n_nodes)


def sqa(x):
    return np.squeeze(np.array(x))


stations = read_stations()

# creates trace Stats for the playback code above
def createTraceStats(network, n1, o1, d1, component):
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


velocity_model = [{"rho": 2734.0, "vp": 6052.0, "vs": 3303.0}]

Q = {"P": 70.0, "S": 50.0}


seed_dir = "half_space_model_lower_noise"


source = {
    "e": np.average([v["e"] for k, v in stations.items()]),
    "n": np.average([v["n"] for k, v in stations.items()]),
    "z": -2100,
    "moment_magnitude": 0,
    "stress_drop": 1.0e6,  # static stress drop
}

g = open(seed_dir + "//synthetic_catalog.csv")
head = g.readline()
lines = g.readlines()
g.close()
time_series = np.arange(0, 4.00, dt)

seedfiles = glob.glob(seed_dir + "//*.mseed")
for i_file, line in enumerate(lines[:1]):
    lspl = line.split(",")
    st = read(seedfiles[i_file])
    A_matrix = []
    amps = np.zeros(len(stations))
    for i_station, station in enumerate(stations):
        p_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "S")
        cos_incoming = np.sqrt(
            1.0
            - (
                p_raypath["hrz_slowness"]["e"] ** 2
                + p_raypath["hrz_slowness"]["n"] ** 2
            )
            * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
        )
        trace = st[i_station].data
        i_p_time = int(p_raypath["traveltime"] // time_series[1])
        i_peak = np.argmax(abs(trace[i_p_time : i_p_time + 20])) + i_p_time
        amps[i_station] = (
            back_project_amplitude(p_raypath, s_raypath, trace[i_peak], "P", Q=80)
            / cos_incoming
        )
        A_matrix.append(inversion_matrix_row(p_raypath, "P"))
        print(i_peak)
    mt_gen, cn_gen, rsq_gen, mt_dc, cn_dev, rsq_dc = solve_moment_tensor(A_matrix, amps)
    MT_in = mt_vector_to_matrix([float(s) for s in lspl[7:13]])
    MT_out = mt_vector_to_matrix(mt_gen)
    MT_dc = mt_vector_to_matrix(mt_dc)
    gn_similarity = (
        np.tensordot(MT_out, MT_in) / np.linalg.norm(MT_in) / np.linalg.norm(MT_out)
    )
    dc_similarity = (
        np.tensordot(MT_dc, MT_in) / np.linalg.norm(MT_in) / np.linalg.norm(MT_dc)
    )
    print(seedfiles[i_file], amps)
