import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt

from obspy import read

seedfile = "half_space_model/2019-07-31T14-05-19.077266Z_synthetic.mseed"

st = read(seedfile)
