# all units in SI unless otherwise noted.
# simulations are in a rotated coordinate frame, x --> across well, y --> along well

file_prefix = "Cimarex_Carel_Elder"

scenarios = [
    {"geophones_per_string": 6, "nodes_per_superstation": 13, "mn_stations": [5, 6]},
    {
        "geophones_per_string": 6,
        "nodes_per_superstation": 19,
        "mn_stations": [5, 9],
    },  # use this one
    {"geophones_per_string": 6, "nodes_per_superstation": 19, "mn_stations": [9, 10]},
    # {
    # "geophones_per_string": 6,
    # "nodes_per_superstation": 19,
    # "station_file": "stations.csv",
    # },
    # {"geophones_per_string": 6, "nodes_per_superstation": 31, "mn_stations": [6, 10]},
    #    {"geophones_per_string": 6, "nodes_per_superstation": 19, "mn_stations": [10, 12]},
]

do_steps = {
    "initial plotting": 0,
    "detectability curve modelling": 0,
    "detectability curve imaging": 0,
    "curve fitting": 1,
    "detectability map modelling": 0,
    "detectability map imaging": 0,
    "detectability plotting": 0,
    "error simulation": 0,
    "error imaging": 0,
    "error reporting": 0,
}

geology_config = "geology.cfg"

n_threads = 3

well_dimensions = {
    "tvd": 3506,
    "pad_width": 1610,
    "well_length": 3220,
    "stage_half_length": 400,
    "well_head_elevation": 366,
}

simulation = {
    "sample_rate": 0.002,
    "t_shift": 0.5,
    "trace_length": 5,
    "bandpass_freqs": [20, 50],
    "noise_ppsd": "PPSD_05pct_NF.NFX06.~.HHZ_20190525T000000_20190531T200000.csv",
    "semblance_weighted_stack_factor": 10,
}

source = {
    "stress_drop": 3.0e5,
    "mechanism": [1, -1, 0, 0, 0, 0],
    "min_magnitude": -2.5,
    "max_magnitude": 1,
    "magnitude_inc": 0.1,
}

imaging_parameters = {
    "resample_frequency": 125,
    "input_delta_z": 10,
    "input_starting_depth": -well_dimensions["well_head_elevation"],
    "max_hrz_distance": 10000,
    "hrz_spacing_tt_grid": 100,
    "static_sigma": 0.003,
    "n_image_grid": [50, 50, 50],
    "d_image_grid": [4, 4, 4],
    "pre_padding_samples": 125,
}

display_parameters = {
    "SI_units": False,  # Note, display only! All computation in SI regardless of value
    "grid_xy": [400, 400],
    "n_xy": [4, 6],
    "well_files": [
        "well1.csv",
        "well2.csv",
        "well3.csv",
        "well4.csv",
        "well5.csv",
    ],
    "well_trend_deg": 0,
    "colors": ["firebrick", "royalblue", "forestgreen", "darkgoldenrod"],
    "undetectability": -1.8,
    "large_mag": -0,
    "waveform_plots": True,
    "M_colorbar_min": -1.7,
    "M_colorbar_max": -1.3,
    "elevation_range": [1200, -15000],  # in display units (i.e. ft or m)
    "velocity_range": [0, 20000],  # ditto
    "well_center": {"easting": 0, "northing": 0},
    "sigma_significance": 2,
}

location_error_simulations = {"n_realizations": 20, "M_simulation": 0}
