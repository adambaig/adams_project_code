import glob
import matplotlib.pyplot as plt
from matplotlib import cm

tab10 = cm.get_cmap('tab10')
noise_files_05 = glob.glob('Noise OVV//*05pct*HHZ*')
noise_files_50 = glob.glob('Noise OVV//*50pct*HHZ*')
noise_files_80 = glob.glob('Noise OVV//*80pct*HHZ*')

fig,ax = plt.subplots()
for i_file,file in enumerate(noise_files_05):
    f = open(file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    ax.semilogx([float(l.split(',')[0]) for l in lines], [float(l.split(',')[1]) for l in lines], color=tab10(i_file/10+0.01))

for i_file,file in enumerate(noise_files_50):
    f = open(file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    ax.semilogx([float(l.split(',')[0]) for l in lines], [float(l.split(',')[1]) for l in lines], color=tab10(i_file/10+0.01))

for i_file,file in enumerate(noise_files_80):
    f = open(file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    ax.semilogx([float(l.split(',')[0]) for l in lines], [float(l.split(',')[1]) for l in lines], color=tab10(i_file/10+0.01))
|
