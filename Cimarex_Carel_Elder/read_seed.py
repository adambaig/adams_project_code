import matplotlib.pyplot as plt
import numpy as np
import glob
from obspy import read, UTCDateTime
from scipy.fftpack import fft, fftfreq
from scipy.optimize import curve_fit
from pfi_fs.configuration import source, geology, read_stations
from sms_ray_modelling.raytrace import isotropic_ray_trace
import mpmath

x = 1
mpmath.meijerg([[0.5]], [[0, 0.5, 1.5]], x)

PI = np.pi

source_file = "Cimarex_Carel_Elder_curve_00x00y_source.cfg"
station_file = "Cimarex_Carel_Elder_stations_030.csv"
geology_file = "geology.cfg"
source_dict = source(source_file)[0]
geology_dict = geology(geology_file)
station_dict = read_stations(station_file)


def distance(src, rec):
    return np.sqrt(sum([(src[c] - rec[c]) ** 2 for c in ["e", "n", "z"]]))


def omega1(f1, DC1, fc1, n1, gamma1, Q, TT):
    omega = (DC1 * np.exp(-PI * f1 * TT / Q)) / (1.0 + (f1 / fc1) ** (gamma1 * n1)) ** (
        1.0 / gamma1
    )
    return omega


def optParams(omega1, f1, omg1, initDC, Fc, N, gamma, Q, TT, verb=False):
    # Optimal parameters and covariance matrix
    bounds = (
        [-np.inf, Fc[1], N[1], gamma[1], Q[1]],
        [np.inf, Fc[2], N[2], gamma[2], Q[2]],
    )
    custom_omega = lambda f, DC, fc, n, gamma, Qin: omega1(f, DC, fc, n, gamma, Qin, TT)
    popt1, pcov1 = curve_fit(
        custom_omega,
        f1,
        omg1,
        p0=(initDC, Fc[0], N[0], gamma[0], Q[0]),
        bounds=bounds,
        maxfev=10000,
    )

    pred1 = omega1(f1, popt1[0], popt1[1], popt1[2], popt1[3], popt1[4], TT)
    omg1 = np.array(omg1)
    residuals1 = np.log10(omg1) - np.log10(pred1)  # /omg1
    fres1 = np.sqrt(sum(residuals1**2) / len(omg1))
    pcov1 = np.sqrt(np.diag(pcov1))
    if verb:
        print("DC level : %.3e " % (popt1[0]))
        print("Corner frequency : %5.2f " % (popt1[1]))
        print("Fall-off : %5.2f " % (popt1[2]))
        print("Gamma : %5.2f " % (popt1[3]))
        print("Q : %5.2f " % (popt1[4]))
        print("Residual : %.3e" % fres1)

    return pred1, popt1[0], popt1[1], popt1[2], popt1[3], popt1[4]


fmin = 1
fmax = 120

cornerIn = [30, 1, 300]
gammaIn = [1, 1, 1.00001]
nIn = [2, 2, 2.00001]
Qin = [60, 1, 120]

src = source_dict
Q, velocity_model = geology_dict
time_axis = np.arange(0, 5, 0.002)
seeds = glob.glob(r"030_stations_13_nodes_6\*.mseed")


for seed in seeds:
    st = read(seed)
    mws = []
    for tr in st:
        rec = station_dict[tr.stats.station]
        trace = tr.data
        P_ray = isotropic_ray_trace(src, rec, velocity_model, "P")
        tr_signal = tr.copy().trim(
            starttime=UTCDateTime(tr.stats.starttime + P_ray["traveltime"] + 0.45),
            endtime=UTCDateTime(tr.stats.starttime + P_ray["traveltime"] + 0.95),
        )
        tr_noise = tr.copy().trim(
            starttime=UTCDateTime(tr.stats.starttime + P_ray["traveltime"] - 0.75),
            endtime=UTCDateTime(tr.stats.starttime + P_ray["traveltime"] - 0.25),
        )

        dist = distance(source_dict, rec)
        p_arr = P_ray["traveltime"] + 0.5
        spec_tr_signal = fft(tr_signal.integrate().data) / len(tr_signal.data)
        spec_tr_noise = fft(tr_noise.integrate().data) / len(tr_noise.data)
        freq_signal = fftfreq(len(tr_signal), tr.stats.delta)
        freq_noise = fftfreq(len(tr_noise), tr.stats.delta)
        inds = np.where((freq_signal >= 10) & (freq_signal <= 60))[0]
        spec_temp = np.array(np.abs(spec_tr_signal[0 : tr_noise.stats.npts // 2]))
        noise_temp = np.array(np.abs(spec_tr_noise[0 : tr_signal.stats.npts // 2]))
        if np.mean(noise_temp) > 0:
            snr = np.mean(spec_temp) / np.mean(noise_temp)
        dat_specs = []
        noise_specs = []
        initDC = 0.5
        if snr > 3:
            dat_specs.append(spec_temp)
            noise_specs.append(noise_temp)
            dat_spec = np.array(spec_temp)
            noise_spec = np.array(noise_temp)

            omg1 = dat_spec[inds]
            max_amp = np.max(omg1)
            freq_snr = freq_signal[inds]
            norm_omg = omg1 / max(omg1)
            specFit, DC, Fc, n, gamma, Q = optParams(
                omega1,
                freq_snr,
                norm_omg,
                initDC,
                cornerIn,
                nIn,
                gammaIn,
                Qin,
                p_arr,
                verb=False,
            )
            moment = (
                4
                * max_amp
                * PI
                * DC
                * P_ray["geometrical_spreading"]
                * (P_ray["velocity_model_chunk"][0]["v"]) ** 2.5
                * np.sqrt(
                    P_ray["velocity_model_chunk"][-1]["v"]
                    * P_ray["velocity_model_chunk"][0]["rho"]
                    * P_ray["velocity_model_chunk"][-1]["rho"]
                )
                / 0.52
            )
            mws.append(2 / 3 * np.log10(moment) - 6.03)
    print(f"{len(mws)} measurements, Mw ={np.average(mws):.2f}")
