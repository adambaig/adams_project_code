import glob
import importlib
import logging
import matplotlib.pyplot as plt
import numpy as np
import os
from progress.bar import IncrementalBar
from shutil import copy2, move
import sys


from pfi_fs.simulate_events import generate_1Csuperstation_waveforms
from pfi_fs.configuration import read_synthetic_catalog

# from pfi_fs.imaging_arrivals import image_feasibility_seeds

from pfi_fs.plotting import (
    plot_detectability,
    feasibility_fields,
    plot_wells_and_stations,
    plot_velocity_model,
    central_error_distributions,
    plot_colorbar,
    plot_wells,
)

from pfi_fs.make_inputs import (
    make_pseudorandom_station_grid,
    make_simulation_files,
    make_imaging_file,
    make_source_file,
    make_rotated_station_file,
)

# if len(sys.argv) > 1:
#     parameter_file = sys.argv[1]
# else:
parameter_file = "Input_Params.py"
sys.path.append(os.path.dirname(parameter_file))
input_file = importlib.import_module(
    os.path.basename(parameter_file.replace(".py", ""))
)


def interp_field_no_linear(x, y, coeffs):
    return coeffs[0] + coeffs[1] * x * x + coeffs[2] * y * y


def relocated_csv_to_image_stats(csv):
    f = open(csv)
    head = f.readline()
    lines = f.readlines()
    f.close()
    image_stats = []
    for line in lines:
        lspl = line.split(",")
        true_location = np.array([float(s) for s in lspl[2:5]])
        magnitude, stress_drop = [float(s) for s in lspl[5:7]]
        m11, m22, m33, m12, m13, m23 = [float(s) for s in lspl[7:13]]
        amplitude = float(lspl[13])
        image_location = np.array([float(s) for s in lspl[14:17]])
        image_stats.append(
            {
                "magnitude": magnitude,
                "amplitude": amplitude,
                "location shift": true_location - image_location,
            }
        )
    return image_stats


station_files = make_pseudorandom_station_grid(input_file, random_jostle=0.01)
n_map_points = (
    input_file.display_parameters["n_xy"][0] * input_file.display_parameters["n_xy"][1]
)
map_directories = [
    [
        ["" for i in range(input_file.display_parameters["n_xy"][0])]
        for j in range(input_file.display_parameters["n_xy"][1])
    ]
    for k in range(len(input_file.scenarios))
]

SI_units = input_file.display_parameters["SI_units"]
decoherence_correction = 0.05
M_detect = {}
tag = ["" for scenario in input_file.scenarios]
f = open("Detectability_Map" + os.sep + "detectability_parameters.csv")
quad = float(f.readline().split(",")[1])
slope = float(f.readline().split(",")[1])
threshold = float(f.readline().split(",")[1])
f.close()
units = 3.28
text_units = "ft"

low_mag = input_file.display_parameters["undetectability"] + 0.3

detectability_catalogs = glob.glob(
    "Detectability_Curve//*//synthetic_catalog_relocated.csv"
)

for directory in [os.path.dirname(path) for path in detectability_catalogs]:
    scenario = directory.split(os.sep)[-1]
    n_superstations, n_nodes, n_geophones = [int(s) for s in scenario.split("_")[::2]]
    total_geophones = n_superstations * n_nodes * n_geophones
    events = read_synthetic_catalog(directory, relocated=True)
    amps = [
        v["image_amplitude"] * total_geophones
        for v in events.values()
        if v["moment_magnitude"] < low_mag
    ]
    print(amps)
    mags = [v["moment_magnitude"] for v in events.values()]


noise_catalogs = glob.glob("Noise_simulations//*//synthetic_catalog_relocated.csv")

avg_amp, n_phones = [], []
for directory in [os.path.dirname(path) for path in noise_catalogs]:
    scenario = directory.split(os.sep)[-1]
    n_superstations, n_nodes, n_geophones = [int(s) for s in scenario.split("_")[::2]]
    events = read_synthetic_catalog(directory, relocated=True)
    amps = [v["image_amplitude"] for v in events.values()]
    avg_amp.append(np.average(amps))
    n_phones.append(n_geophones * (n_superstations * n_nodes))

plt.plot(np.array(n_phones) * np.array(avg_amp))

plt.hist(amps)

xlabel = "easting (" + text_units + ")"
ylabel = "northing (" + text_units + ")"
n_scenarios = len(input_file.scenarios)
fig, ax = plt.subplots(1, n_scenarios, sharey=True, figsize=[14, 9])
for i_scenario, scenario in enumerate(input_file.scenarios):
    scenario = input_file.scenarios[i_scenario]
    axis = ax[i_scenario]
    nx, ny = scenario["mn_stations"]
    n_stations = str(nx * ny).zfill(3)
    prefix = n_stations + "_stations_"
    n_station_int = int(n_stations)
    input_file.scenarios[i_scenario]["n_stations"] = n_stations

    tag[i_scenario] = (
        prefix
        + str(scenario["nodes_per_superstation"])
        + "_nodes_"
        + str(scenario["geophones_per_string"])
    )
    csvs = glob.glob(
        "Detectability_Map"
        + os.sep
        + tag[i_scenario]
        + "*x*y"
        + os.sep
        + "synthetic_catalog_relocated.csv"
    )
    n_total_sensors = (
        scenario["nodes_per_superstation"]
        * scenario["geophones_per_string"]
        * n_station_int
    )
    for csv in csvs:
        position = os.path.split(csv)[0].split("_")[-1]
        image_stats = relocated_csv_to_image_stats(csv)
        M0_amp = (
            scenario["nodes_per_superstation"] ** 2
            * scenario["geophones_per_string"] ** 2
            * n_station_int**2
            * np.average(
                [v["amplitude"] for v in image_stats if abs(v["magnitude"]) < 1e-7]
            )
        )
        M_detect[position] = (
            -slope
            + np.sqrt(slope * slope - 4 * quad * (np.log10(M0_amp * 30 / threshold)))
        ) / 2.0 / quad + decoherence_correction
    n_rows = len(M_detect)
    # A_matrix, rhs_detectability = np.zeros([n_rows, 5]), np.zeros(n_rows)
    A_matrix, rhs_detectability = np.zeros([n_rows, 3]), np.zeros(n_rows)
    x_max, y_max = (
        np.array(input_file.display_parameters["grid_xy"])
        * np.array(input_file.display_parameters["n_xy"])
        * units
    )
    for i_row, (position, mag) in enumerate(M_detect.items()):
        x = int(position[0:2]) * input_file.display_parameters["grid_xy"][0]
        y = int(position[3:5]) * input_file.display_parameters["grid_xy"][1]
        # A_matrix[i_row, :] = [1, x, y, x * x, y * y]
        A_matrix[i_row, :] = [1, x * x, y * y]
        rhs_detectability[i_row] = mag
    detectability_coeffs = np.linalg.lstsq(A_matrix, rhs_detectability)[0]
    x_pos = np.linspace(-x_max, x_max, 101)
    y_pos = np.linspace(-y_max, y_max, 241)
    detect_mag = np.zeros([len(x_pos), len(y_pos)])
    for i_x, x in enumerate(x_pos):
        for i_y, y in enumerate(y_pos):
            detect_mag[i_x, i_y] = interp_field_no_linear(x, y, detectability_coeffs)
    print("Detectability the at center of the grid: M%.2f" % detect_mag[51, 121])
    axis.set_aspect("equal")
    axis = plot_wells(
        axis,
        input_file.display_parameters["well_files"],
        rotation_degrees=input_file.display_parameters["well_trend_deg"],
        center=input_file.display_parameters["well_center"],
        SI_units=SI_units,
    )
    x1, x2 = axis.get_xlim()
    y1, y2 = axis.get_ylim()
    x1 -= units * input_file.well_dimensions["stage_half_length"]
    x2 += units * input_file.well_dimensions["stage_half_length"]
    y1 -= units * input_file.well_dimensions["stage_half_length"]
    y2 += units * input_file.well_dimensions["stage_half_length"]
    axis.pcolor(
        x_pos * units,
        y_pos * units,
        detect_mag.T,
        zorder=0,
        cmap="viridis",
        vmin=input_file.display_parameters["M_colorbar_min"],
        vmax=input_file.display_parameters["M_colorbar_max"],
    )
    if "detectability_map_x_extent" in input_file.display_parameters:
        xmax = input_file.display_parameters["detectability_map_x_extent"]
        axis.set_xlim([-xmax, xmax])
    else:
        axis.set_xlim([max(x1, -x_max), min(x2, x_max)])
    if "detectability_map_y_extent" in input_file.display_parameters:
        ymax = input_file.display_parameters["detectability_map_y_extent"]
        axis.set_xlim([-ymax, ymax])
    else:
        axis.set_ylim([max(y1, -y_max), min(y2, y_max)])
    axis.set_xlabel(xlabel)
if n_scenarios == 1:
    ax.set_ylabel(ylabel)
    if abs(input_file.display_parameters["well_trend_deg"]) > 1e-7:
        ax = plot_north_arrow(ax, input_file.display_parameters["well_trend_deg"])
else:
    ax[0].set_ylabel(ylabel)
    for axis in ax:
        if abs(input_file.display_parameters["well_trend_deg"]) > 1e-7:
            axis = plot_north_arrow(
                axis, input_file.display_parameters["well_trend_deg"]
            )
fig.savefig("detectability.png")
M_colorbar = plot_colorbar(
    input_file.display_parameters["M_colorbar_min"],
    input_file.display_parameters["M_colorbar_max"],
    "detectability magnitude",
)
M_colorbar.savefig("detectability_colorbar.png")
