import matplotlib.pyplot as plt
import numpy as np
import pyproj as pr
from pyproj import CRS, Transformer

from read_inputs import read_wells


buffer_length = 2000
spacing = 100
wells = read_wells()


latlon_crs = CRS.from_epsg(4326)
utm11N_crs = CRS.from_epsg(26911)
transformer = Transformer.from_crs(utm11N_crs, latlon_crs)

well = wells[list(wells.keys())[0]]

fig, ax = plt.subplots()
ax.set_aspect("equal")
for well_id, well in wells.items():
    ax.plot(well["eastings"], well["northings"], "k")
    ax.text(well["eastings"][-1], well["northings"][-1], well_id)
all_eastings = np.hstack([v["eastings"] for v in wells.values()])
all_northings = np.hstack([v["northings"] for v in wells.values()])

all_eastings_except_wsw = np.hstack(
    [v["eastings"] for k, v in wells.items() if "5-36" not in k]
)
all_northings_except_wsw = np.hstack(
    [v["northings"] for k, v in wells.items() if "5-36" not in k]
)


eastings_range = np.arange(min(all_eastings) - 3000, max(all_eastings) + 3000, spacing)
northings_range = np.arange(
    min(all_northings) - 3000, max(all_northings) + 3000, spacing
)
len(eastings_range)

grid = np.meshgrid(eastings_range, northings_range)


def distance_to_wells(e, n):
    return min(
        [
            np.linalg.norm([e - all_e, n - all_n])
            for all_e, all_n in zip(all_eastings, all_northings)
        ]
    )


e_grid, n_grid = grid
distances = np.zeros([len(eastings_range), len(northings_range)])
for i_e, e in enumerate(eastings_range):
    for i_n, n in enumerate(northings_range):
        distances[i_e, i_n] = distance_to_wells(e, n)


cs = ax.contour(eastings_range, northings_range, distances.T, [2000])
p = cs.collections[0].get_paths()[0]

buffer_e = p.vertices[:, 0]
buffer_n = p.vertices[:, 1]

with open("buffer_all_wells_2_km.csv", "w") as f:
    f.write("Latitude, Longitude,\n")
    for buf_e, buf_n in zip(buffer_e, buffer_n):
        lat, lon = transformer.transform(buf_e, buf_n)
        f.write(f"{lat:.7f},{lon:.7f}\n")


def distance_to_wells_except_wsw(e, n):
    return min(
        [
            np.linalg.norm([e - all_e, n - all_n])
            for all_e, all_n in zip(all_eastings_except_wsw, all_northings_except_wsw)
        ]
    )


distances_except_wsw = np.zeros([len(eastings_range), len(northings_range)])
for i_e, e in enumerate(eastings_range):
    for i_n, n in enumerate(northings_range):
        distances_except_wsw[i_e, i_n] = distance_to_wells_except_wsw(e, n)


cs_ex_wsw = ax.contour(eastings_range, northings_range, distances_except_wsw.T, [2000])
p = cs_ex_wsw.collections[0].get_paths()[0]

buffer_e = p.vertices[:, 0]
buffer_n = p.vertices[:, 1]

with open("buffer_all_wells_2_km_except_wsw.csv", "w") as f:
    f.write("Latitude, Longitude,\n")
    for buf_e, buf_n in zip(buffer_e, buffer_n):
        lat, lon = transformer.transform(buf_e, buf_n)
        f.write(f"{lat:.7f},{lon:.7f}\n")
