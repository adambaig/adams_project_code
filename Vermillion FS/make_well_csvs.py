import pyproj as pr
from pyproj import CRS, Transformer


from read_inputs import read_wells

wells = read_wells()


latlon_crs = CRS.from_epsg(4326)
utm11N_crs = CRS.from_epsg(26911)
transformer = Transformer.from_crs(utm11N_crs, latlon_crs)

well = wells[list(wells.keys())[0]]

for well_id, well in wells.items():
    lats, lons = transformer.transform(well["eastings"], well["northings"])
    with open(f"{well_id.replace(' ','_')}.csv", "w") as f:
        f.write("Longitude,Latitude\n")
        for lon, lat in zip(lons, lats):
            f.write(f"{lon:.7f},{lat:.7f}\n")
