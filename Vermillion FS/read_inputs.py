import os
from glob import glob

import numpy as np


def read_wells():
    well_files = glob(r"Wells/*.txt")
    wells = {}
    for well_file in well_files:
        well_name = os.path.basename(well_file).split("Leucrotta ")[1].split(".")[0]
        wells[well_name] = {}

        with open(well_file) as f:
            _head = [f.readline() for ii in range(13)]
            lines = f.readlines()
            (
                wells[well_name]["mds"],
                wells[well_name]["eastings"],
                wells[well_name]["northings"],
                wells[well_name]["elevation"],
            ) = zip(
                *[[float(line.split()[ii]) for ii in [0, 11, 9, 3]] for line in lines]
            )
    return wells


import matplotlib.pyplot as plt
from generalPlots import gray_background_with_grid

wells = read_wells()
fig, ax = plt.subplots()
ax.set_aspect("equal")
ax.set_facecolor("0.9")
for well in wells.values():
    ax.plot(well["eastings"], well["northings"], "k")
gray_background_with_grid(ax)
# def read_velocity_model()
fig.savefig("wells.png")
