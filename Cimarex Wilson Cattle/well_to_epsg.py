import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
from pyproj import Transformer

transformer = Transformer.from_crs("epsg:4326", "epsg:32125")

f = open("well_latlon.csv")
head = f.readline()
lines = f.readlines()
f.close()
len(lines)
easting, northing = np.zeros(len(lines)), np.zeros(len(lines))
g = open("well_epsg.csv", "w")
g.write("easting, northing\n")
for i_line, line in enumerate(lines):
    long, lat = [float(s) for s in line.split(",")[:2]]
    easting[i_line], northing[i_line] = transformer.transform(lat, long)
    g.write("%.2f,%.2f\n" % (easting[-1], northing[-1]))
g.close()

np.arctan2(np.diff(easting), np.diff(northing)) * 180 / np.pi

np.sqrt(np.diff(easting) * np.diff(easting) + np.diff(northing) * np.diff(northing))
plt.plot(easting, northing)

np.average()
