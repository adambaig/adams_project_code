import matplotlib

matplotlib.use("Qt5Agg")

import matplotlib.pyplot as plt
import numpy
from obspy import read

synth_doombar = "Doombar detectability check\\15_stations_13_center\\synthetic2019-11-12T20-27-10.766841Z.mseed"
synth_local = "Detectability Curve\\15_stations_13_center\\synthetic2019-11-11T20-56-55.908048Z.mseed"


st_doombar = read(synth_doombar)
st_local = read(synth_local)
#

fig, ax1 = plt.subplots(1)

abs_max = 0
for trace in st_doombar.sort():
    if max(abs(trace.data)) > abs_max:
        abs_max = max(abs(trace.data))


for i_trace, trace in enumerate(st_doombar):
    ax1.plot(
        trace.times(),
        i_trace + 1 + trace.data / abs_max,
        color="firebrick",
        lw=1,
    )
    ax1.plot(
        trace.times(),
        i_trace + 1.05 + st_local[i_trace].data / abs_max,
        color="royalblue",
        lw=1,
    )
