import numpy as np


def read_velocity_model():
    kb = 1000
    dr = "O:\\O&G\\SalesSupport\\20190305_Paramount_2-28\\MC\\"
    f = open(dr + "DSA.mdl")
    lines = f.readlines()
    f.close()

    velocity_model = []
    ii = -1
    for line in lines:
        ii += 1
        lspl = line.split()
        vp = 1000 * float(lspl[0])
        vs = 1000 * float(lspl[1])
        rho = 310 * vp ** (0.25)  # Gardner's relation
        top = -1000 * float(lspl[2]) + kb
        if ii == 0:
            velocity_model.append(
                {
                    "rho": rho,
                    "vp": vp,
                    "vs": vs,
                }
            )
        else:
            velocity_model.append(
                {
                    "top": top,
                    "rho": rho,
                    "vp": vp,
                    "vs": vs,
                }
            )

    return velocity_model


for layer in velocity_model:
    if "top" in layer:
        print(
            ("%.0f,%.1f,%.1f,%.1f")
            % (layer["top"], layer["vp"], layer["vs"], layer["rho"])
        )
    else:
        print(("%.1f,%.1f,%.1f") % (layer["vp"], layer["vs"], layer["rho"]))
