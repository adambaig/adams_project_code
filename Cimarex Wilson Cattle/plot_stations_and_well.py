import matplotlib

matplotlib.use("Qt5agg")
import glob
import matplotlib.pyplot as plt
import numpy as np
from pyproj import Transformer

from pfi_fs.configuration import geology

transformer = Transformer.from_crs("epsg:4326", "epsg:26911")

center_east, center_north = 0, 0
M2F = 3.28
PI = np.pi
angle = PI * (180 - 127.1) / 180
wells = {}
for well in glob.glob("fake_well*.csv"):
    f = open(well)
    head = f.readline()
    lines = f.readlines()
    f.close()
    wells[well.split(".")[0][-1]] = {
        "easting": np.array([float(line.split(",")[0]) for line in lines])
        - center_east,
        "northing": np.array([float(line.split(",")[1]) for line in lines])
        - center_north,
    }

fig, ax = plt.subplots(1, 3, figsize=[16, 7], sharex=True, sharey=True)
for axis in [ax[0], ax[1], ax[2]]:
    for tag, well in wells.items():
        axis.plot(
            M2F * np.array(well["easting"]),
            M2F * np.array(well["northing"]),
            c="0.8",
            lw=4,
            zorder=3,
            alpha=0.5,
        )
        axis.plot(
            M2F * np.array(well["easting"]),
            M2F * np.array(well["northing"]),
            c="0.2",
            lw=2,
            zorder=4,
            alpha=0.5,
        )
station_files = glob.glob("Cimarex_WC_random_grid_??.csv")
for i_file, station_file in enumerate(station_files):
    f = open(station_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    n_lines = len(lines)
    east, north = np.zeros(n_lines), np.zeros(n_lines)
    for i_line, line in enumerate(lines):
        lspl = line.split(",")
        north[i_line], east[i_line] = [M2F * float(s) for s in lspl[1:3]]
    a = ax[i_file]
    a.set_aspect("equal")
    a.plot(east - center_east, north - center_north, "v", zorder=6)
    a.set_xlabel("distance across well (ft)")
    a.arrow(
        -10000,
        10000,
        1000 * np.sin(angle),
        1000 * np.cos(angle),
        color="firebrick",
        overhang=0.3,
        width=10,
        head_width=500,
        head_length=700,
    )
    a.text(
        -10000 + 1700 * np.sin(angle),
        10500 + 1700 * np.cos(angle),
        "N",
        color="firebrick",
        ha="left",
        va="top",
        family="serif",
    )
    a.text(-10500, 12000, "ABC"[i_file], size="large")

ax[0].set_ylabel("distance along well (ft)")


Q, velocity_model = geology("geology-canyon.cfg")

tops = [250 * M2F / 1000]
vp = [velocity_model[0]["vp"] * M2F, velocity_model[0]["vp"] * M2F]
vs = [velocity_model[0]["vs"] * M2F, velocity_model[0]["vs"] * M2F]

for layer in velocity_model[1:]:
    for ii in range(2):
        tops.append(M2F * layer["top"] / 1000)
        vp.append(M2F * layer["vp"])
        vs.append(M2F * layer["vs"])
tops.append(M2F * layer["top"] / 1000)
v_fig, v_ax = plt.subplots()
v_ax.plot(vp, tops, "firebrick")
v_ax.plot(vs, tops, "royalblue")
v_ax.plot([0, 15000], [-6.750, -6.750], "darkgoldenrod", lw=3)
v_ax.text(1500, -6.25, "target depth\n7500 ft TVD")
v_ax.plot([0, 15000], [tops[0], tops[0]], "k")
v_ax.set_ylim([-10, 1])
v_ax.set_xlim([0, 15000])
v_ax.legend(["$v_p$", "$v_s$"])
v_ax.set_xlabel("velocity (ft/s)")
v_ax.set_ylabel("elevation above sea level (X1000 ft)")
