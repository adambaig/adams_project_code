from pfi_fs.simulate_events import generate_1Csuperstation_waveforms
import logging
import os
import sys
import shutil
import time


destination_directory = "Detectability Curve"
start = time.time()
if not os.path.exists(destination_directory):
    os.mkdir(destination_directory)
else:
    logging.error(
        "Detectability Curve Directory exists already.  Please rename or remove.  Stopping"
    )
    sys.exit()
for directory_name in [
    "15_stations_13_center",
    "30_stations_13_center",
    "60_stations_19_center",
]:
    nsuper, _, nstations, position = directory_name.split("_")
    sim_config = "simulation" + nstations + ".cfg"
    if position == "center":
        source_config = "source.cfg"
    elif position == "toe":
        source_config = "toe_source.cfg"
    station_file = "Cimarex_WC_random_grid_" + nsuper + ".csv"
    generate_1Csuperstation_waveforms(
        source_config,
        sim_config,
        "geology-canyon.cfg",
        station_file,
        directory_name=directory_name,
        # plot_out=True,
    )
    shutil.copytree(directory_name, destination_directory + "//" + directory_name)
    shutil.rmtree(directory_name)
end = time.time()
print(end - start)
