from pfi_fs.simulate_events import generate_1Csuperstation_waveforms
import logging
import os
import sys
import shutil
import time

destination_directory = "Detectability Map"
start = time.time()
if not os.path.exists(destination_directory):
    os.mkdir(destination_directory)
else:
    logging.error(
        "Detectability Directory exists already.  Please rename or remove.  Stopping"
    )
    sys.exit()

dx, dy = 400, 800  # x--> easting  y--> northing

f = open("source_0e0n.cfg")

line = f.readline()
lines = f.readlines()
f.close()
data, comment = line.split("#", 1)
east, north, elevation = [float(s) for s in data.split(",")]

positions = [
    "1e0n",
    "0e1n",
    "1e1n",
    "2e0n",
    "0e2n",
    "2e2n",
    "1e2n",
    "2e1n",
    "0e0n",
    "0e3n",
    "1e3n",
    "2e3n",
]

for position in positions:
    nx, ny = int(position[0]), int(position[2])
    outfile = "source_" + position + ".cfg"
    g = open(outfile, "w")
    g.write("%.2f,%.2f,%.2f #" % (east + nx * dx, north + ny * dy, elevation))
    g.write(comment)
    for line in lines:
        g.write(line)
    g.close()


for directory_name in ["15_stations_13", "30_stations_13", "60_stations_19"]:
    nsuper, _, nstations = directory_name.split("_")
    sim_config = "simulation" + nstations + ".cfg"
    for position in positions:
        source_config = "source_" + position + ".cfg"

        station_file = "Cimarex_WC_random_grid_" + nsuper + ".csv"
        generate_1Csuperstation_waveforms(
            source_config,
            sim_config,
            "geology-canyon.cfg",
            station_file,
            directory_name=directory_name + "_" + position,
        )
        shutil.copytree(
            directory_name + "_" + position,
            destination_directory + "//" + directory_name + "_" + position,
        )
        shutil.rmtree(directory_name + "_" + position)
