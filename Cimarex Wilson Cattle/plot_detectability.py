import matplotlib


matplotlib.use("Qt5Agg")


import glob

import matplotlib.pyplot as plt
from matplotlib.ticker import (
    MultipleLocator,
    FormatStrFormatter,
    AutoMinorLocator,
    LogLocator,
)
import numpy as np
import os


def sqa(x):
    return np.squeeze(np.array(x))


csvs = glob.glob("Detectability Curve//*//synthetic_catalog_relocated.csv")

# one realization per multiple magnitude
ncolor = {
    "5": "firebrick",
    "15": "royalblue",
    "30": "forestgreen",
    "60": "darkgoldenrod",
}
factor = {"5": np.sqrt(1), "15": np.sqrt(1), "30": np.sqrt(1), "60": 1.0}
marker = {"toe": "s", "center": "o"}
ms = {"7": 5, "13": 7, "19": 9}
noise_sample = []
xMajorLocator = MultipleLocator(0.5)
xMinorLocator = MultipleLocator(0.1)
yMajorLocator = LogLocator(10)
yMinorLocator = LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9])
image_stats = {}
M_detect = {}

for csv in csvs:
    position = csv.split("\\")[-2].split("_")[3]
    nnodes = csv.split("\\")[-2].split("_")[2]
    nstations = csv.split("\\")[-2].split("_")[0]
    node_factor = np.sqrt(13.0 / int(nnodes))
    if not (position in image_stats):
        image_stats[position] = {}
        M_detect[position] = {}
    if not (nstations in image_stats[position]):
        image_stats[position][nstations] = {}
        M_detect[position][nstations] = {}
    if not (nnodes in image_stats[position][nstations]):
        image_stats[position][nstations][nnodes] = []
        M_detect[position][nstations][nnodes] = {}
    f = open(csv)
    head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        true_loc = np.array([float(s) for s in lspl[2:5]])
        magnitude, stress_drop = [float(s) for s in lspl[5:7]]
        m11, m22, m33, m12, m13, m23 = [float(s) for s in lspl[7:13]]
        amplitude = float(lspl[13])
        image_loc = np.array([float(s) for s in lspl[14:17]])
        image_stats[position][nstations][nnodes].append(
            {
                "magnitude": magnitude,
                "amplitude": amplitude,
                "location shift": true_loc - image_loc,
            }
        )
        if magnitude < -2.0:
            noise_sample.append(node_factor * factor[nstations] * amplitude)

threshold = np.average(noise_sample) + 1 * np.std(noise_sample)

quad, slope = np.zeros(len(csvs)), np.zeros(len(csvs))
f1, a1 = plt.subplots(figsize=[16, 10])
patches, labels = [], []
for position in ["center"]:
    for i_measure, nstations in enumerate(["15", "30", "60"]):
        for nnodes in image_stats[position][nstations]:
            mag = np.array(
                [v["magnitude"] for v in image_stats[position][nstations][nnodes]]
            )
            amp = np.array(
                [v["amplitude"] for v in image_stats[position][nstations][nnodes]]
            )
            ilarge = np.where(mag > -1.0)[0]
            large_amps = np.matrix(
                np.log10(node_factor * factor[nstations] * amp[ilarge])
            )
            Amatrix = np.matrix(
                np.vstack(
                    [
                        mag[ilarge] * mag[ilarge],
                        mag[ilarge],
                        np.ones(len(ilarge)),
                    ]
                )
            ).T
            quad[i_measure], slope[i_measure], yint = [
                sqa(s)
                for s in np.linalg.inv(Amatrix.T * Amatrix) * Amatrix.T * large_amps.T
            ]
            detectability_line = 10 ** (
                mag * mag * quad[i_measure] + mag * slope[i_measure] + yint
            )
            M_detect[position][nstations][nnodes] = (
                (
                    -slope[i_measure]
                    + np.sqrt(
                        slope[i_measure] * slope[i_measure]
                        - 4 * quad[i_measure] * (yint - np.log10(threshold))
                    )
                )
                / 2
                / quad[i_measure]
            )
            (p,) = a1.semilogy(
                mag,
                node_factor * factor[nstations] * amp,
                marker=marker[position],
                color=ncolor[nstations],
                markeredgecolor="k",
                markersize=ms[nnodes],
            )
            patches.append(p)
            labels.append(
                nstations
                + " superstations, "
                + nnodes
                + " nodes: "
                + position
                + " Mc = "
                + str(M_detect[position][nstations][nnodes])[:5].replace(
                    "-", "$\minus$"
                )
            )
            # a1.set_xlim([-3, -0])
            # a1.set_ylim([0.01, 1000])
            a1.grid(True, which="both")
            a1.yaxis.set_major_locator(yMajorLocator)
            a1.yaxis.set_minor_locator(yMinorLocator)
            a1.xaxis.set_major_locator(xMajorLocator)
            a1.xaxis.set_minor_locator(xMinorLocator)
            a1.set_title("Detectability")
            a1.set_ylabel("amplitude")
            a1.set_xlabel("magnitude")
            a1.plot(mag, detectability_line, "k:", lw=1)
            a1.plot([-2.5, 0], [threshold, threshold], "k-.", lw=2)
            a1.text(
                -2.3,
                1.05 * threshold,
                "2$\sigma$ detection threshold",
                fontsize=14,
            )
a1.legend(patches, labels)
plt.show()

if os.path.isdir("Detectability Map"):
    f = open("Detectability Map\\detectability_parameters.csv", "w")
else:
    f = open("detectability_parameters.csv", "w")
f.write("quadratic term, %.9e\n" % np.median(quad))
f.write("linear term, %.9e\n" % np.median(slope))
f.write("13 station threshold, %.9e" % threshold)
f.close()
