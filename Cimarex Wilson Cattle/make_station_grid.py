import numpy as np
from pyproj import Transformer

transformer = Transformer.from_crs("epsg:26911", "epsg:4326")
prefix = "Cimarex"
surface_elevation = 250
well_trajectory = 127.1 * np.pi / 180
target_depth = 2287
lateral_length = 1871
pad_width = 400
easting_ctr, northing_ctr = 0, 0


def place_stations(writeout=False):

    xwide = target_depth + pad_width / 2
    ywide = target_depth + lateral_length / 2
    nx = 3
    ny = 5
    easting_ctr, northing_ctr = 0, 0

    xrange = np.linspace(easting_ctr - xwide, easting_ctr + xwide, nx)
    yrange = np.linspace(northing_ctr - ywide, northing_ctr + ywide, ny)

    stations = []
    jj = -1
    for ix in xrange:
        for iy in yrange:
            jj += 1
            stations.append({"name": str(jj), "x": ix, "y": iy, "z": 250.0})
    writeout = True
    if writeout:
        fileout = "Cimarex_grid_%i.csv" % (nx * ny)
        g = open(fileout, "w")
        g.write("station, along well (m), perp to well (m), tvdss (m)\n")
        for station in stations:
            g.write(
                station["name"]
                + ",%.1f,%.1f,%.1f\n" % (station["y"], station["x"], station["z"])
            )


def place_5_pilot_stations(writeout=False):

    ywide = target_depth + pad_width / 2
    xwide = target_depth + lateral_length / 2
    easting_ctr, northing_ctr = 460237, 6054180
    stations = []
    stations.append(
        {
            "name": "1",
            "x": easting_ctr - xwide,
            "y": northing_ctr + ywide,
            "z": 880.0,
        }
    )
    stations.append(
        {
            "name": "2",
            "x": easting_ctr - xwide,
            "y": northing_ctr - ywide,
            "z": 880.0,
        }
    )
    stations.append({"name": "3", "x": easting_ctr, "y": northing_ctr, "z": 880.0})
    stations.append(
        {
            "name": "4",
            "x": easting_ctr + xwide,
            "y": northing_ctr + ywide,
            "z": 880.0,
        }
    )
    stations.append(
        {
            "name": "5",
            "x": easting_ctr + xwide,
            "y": northing_ctr - ywide,
            "z": 880.0,
        }
    )
    writeout = True
    if writeout:
        fileout = "ARC_pilot_grid.csv"
        g = open(fileout, "w")
        g.write("station, latitude, longitude, elevation (m)\n")
        for station in stations:
            latitude, longitude = transformer.transform(station["x"], station["y"])
            g.write(
                station["name"]
                + ",%.7f,%.7f,%.1f\n" % (latitude, longitude, -station["z"])
            )
        g.close()
    return stations


def place_random_stations(writeout=False):
    xwide = target_depth + pad_width / 2
    ywide = target_depth + lateral_length / 2
    nx = 3
    ny = 5
    xrange = np.linspace(easting_ctr - xwide, easting_ctr + xwide, nx)
    yrange = np.linspace(northing_ctr - ywide, northing_ctr + ywide, ny)
    x_spacing = 200
    y_spacing = 200
    stations = []
    jj = -1
    for ix in xrange:
        for iy in yrange:
            jj += 1
            stations.append(
                {
                    "name": str(jj),
                    "x": ix + x_spacing * np.random.randn(),
                    "y": iy + y_spacing * np.random.randn(),
                    "z": surface_elevation,
                }
            )
    writeout = True
    if writeout:
        fileout = "Cimarex_WC_random_grid_%i.csv" % (nx * ny)
        g = open(fileout, "w")
        g.write("station, northing (m), easting (m), tvdss (m)\n")
        for station in stations:
            g.write(
                station["name"]
                + ",%.1f,%.1f,%.1f\n" % (station["y"], station["x"], station["z"])
            )
        g.close()
    return stations
