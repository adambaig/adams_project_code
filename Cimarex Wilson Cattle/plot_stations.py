import matplotlib

matplotlib.use("Qt5Agg")
import glob
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (
    MultipleLocator,
    FormatStrFormatter,
    AutoMinorLocator,
    LogLocator,
)

center_east, center_north = 0, 0
wells = {}
for well in glob.glob("fake_well*.csv"):
    f = open(well)
    head = f.readline()
    lines = f.readlines()
    f.close()
    wells[well.split(".")[0][-1]] = {
        "easting": [float(line.split(",")[0]) for line in lines],
        "northing": [float(line.split(",")[1]) for line in lines],
    }


station_files = [
    "Artis_random_grid_15.csv",
    "Artis_random_grid_30.csv",
    "Artis_random_grid_60.csv",
    "Artis_grid_5.csv",
]
nodes = ["13", "13", "19", "7"]
stations = {}
fig, ax = plt.subplots(1, 4, sharey=True)
for i_scenario, station_file in enumerate(station_files):
    n_stations = station_file.split(".")[0].split("_")[-1]
    f = open(station_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        id = lspl[0]
        stations[id] = {
            "easting": float(lspl[2]) - center_east,
            "northing": float(lspl[1]) - center_north,
            "elevation": float(lspl[3]),
        }

    ax[i_scenario].set_aspect("equal")
    ax[i_scenario].set_xlim([-3000, 3000])
    ax[i_scenario].set_ylim([-4000, 4000])
    for tag, well in wells.items():
        ax[i_scenario].plot(
            well["easting"],
            well["northing"],
            c="0.8",
            lw=4,
            zorder=3,
            alpha=0.5,
        )
        ax[i_scenario].plot(
            well["easting"],
            well["northing"],
            c="0.2",
            lw=2,
            zorder=4,
            alpha=0.5,
        )
    for id, station in stations.items():
        ax[i_scenario].plot(
            station["easting"],
            station["northing"],
            "h",
            markeredgecolor="w",
            c="k",
            zorder=5,
        )
    ax[i_scenario].set_xlabel("easting (m)")
    ax[i_scenario].set_title(
        n_stations + " " + nodes[i_scenario] + "-node superstations"
    )
ax[0].set_ylabel("northing (m)")
