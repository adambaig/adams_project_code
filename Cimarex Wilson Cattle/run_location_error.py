from pfi_fs.simulate_events import generate_1Csuperstation_waveforms
import logging
import os
import sys
import shutil
import time


destination_directory = "Location Error"
start = time.time()
if not os.path.exists(destination_directory):
    os.mkdir(destination_directory)
else:
    logging.error("Location Error exists already.  Please rename or remove.  Stopping")
    sys.exit()
for directory_name in [
    "15_stations_13_error",
    "30_stations_13_error",
    "60_stations_19_error",
]:
    nsuper, _, nstations, position = directory_name.split("_")
    sim_config = "simulation" + nstations + "_100.cfg"
    source_config = "source_0e0n.cfg"
    station_file = "Cimarex_WC_random_grid_" + nsuper + ".csv"
    generate_1Csuperstation_waveforms(
        source_config,
        sim_config,
        "geology-canyon.cfg",
        station_file,
        directory_name=directory_name,
        # plot_out=True,
    )
    shutil.copytree(directory_name, destination_directory + "//" + directory_name)
    shutil.rmtree(directory_name)
end = time.time()
print(end - start)
