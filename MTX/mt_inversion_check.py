import json
import logging
import os

import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory
import pyproj as pr

from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_moment_tensor.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)
from sms_moment_tensor.plotting import plot_regression, plot_beachballs

from NocMeta.Meta import NOC_META

LATLONG_PROJ = pr.Proj(init="epsg:4326")

archive_json = "MT_json//53019.json"

with open(archive_json) as f:
    archive = json.load(f)


inv_path = read_inventory("Station_xml//MTX_Full.xml")

velocity_model = archive["velDict"]
velocity_model[0].pop("top")
low_f, high_f = archive["bpFreqs"]
amplitudes = archive["ampDict"]

station_elev = []
for network in inv_path:
    for station in network:
        station_elev.append(station.elevation)

mean_elev = np.average(station_elev)
for layer in velocity_model:
    if "top" in layer:
        layer["top"] += mean_elev

with open("MTX_velocity_model.json", "w") as f:
    json.dump(velocity_model, f)


def get_station_coordinates_from_inv(network_station_location, inv):
    network, station, location = network_station_location.split(".")
    station_inv = inv.select(network=network, station=station, location=location)
    if len(station_inv) == 0:
        logging.error(f"{network_station_location} not in inventory")
        return
    return {
        "latitude": station_inv[0][0][0].latitude,
        "longitude": station_inv[0][0][0].longitude,
        "elevation": station_inv[0][0][0].elevation,
    }


def artemis_check(archive_json, inv, network_code="MTX"):
    out_proj = pr.Proj()
    with open(archive_json) as f:
        archive = json.load(f)
    velocity_model = archive["velDict"]
    velocity_model[0].pop("top")
    low_f, high_f = archive["bpFreqs"]
    amplitudes = archive["ampDict"]
    ref_freq = np.sqrt(low_f * high_f)

    unique_stations = [".".join(k.split(".")[:3]) for k in amplitudes.keys()]
    missing_stations = []
    for network_station_location in unique_stations:
        network, station, location = network_station_location.split(".")
        is_there = (
            len(inv.select(network=network, station=station, location=location)) > 0
        )
        if not is_there:
            missing_stations.append(network_station_location)
    if missing_stations:
        logging.error(
            f"the following stations are not found in the inventory: {' '.join(missing_stations)}"
        )
        return

    from obspy import UTCDateTime

    UTCDateTime(event_origin["time"])
    event_origin = archive["eveDict"]["event"]["origin"]
    event_origin["latitude"]
    1000 * event_origin["referenceElevation"]
