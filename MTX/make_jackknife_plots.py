import json

import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
from obspy import read_inventory
import pyproj


from NocMeta.Meta import NOC_META
from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import SeismicEvent
from nmxseis.numerics.velocity_model.one_dimensional import VelocityModel1D, VMLayer1D
from nmxseis.numerics.moment_tensor.inversion import (
    MTSolution,
    inversion_matrix_row,
    back_project_amplitude,
)
from nmxseis.numerics.moment_tensor.plotting import plot_main_and_jackknife_dc_solutions
from nmxseis.numerics.ray_modeling import Raypath


ATHENA_CODE = "MTX"
NOC_META

mt_json = r"MT_json\76420.json"
reproject = pyproj.Proj("epsg:3081")

with open(mt_json) as f:
    solutions = json.load(f)


solutions["velDict"][0].pop("top")
layers = []
for layer in solutions["velDict"]:
    layers.append(
        VMLayer1D(
            vp=layer["vp"],
            vs=layer["vs"],
            top=layer["top"] if "top" in layer else np.inf,
            rho=layer["rho"],
        )
    )

event = SeismicEvent(
    lat=(origin := solutions["eveDict"]["event"]["origin"])["latitude"],
    lon=origin["longitude"],
    origin_time=origin["time"],
    depth_m=origin["depth"] * 1000,
    reproject=reproject,
)

velocity_model = VelocityModel1D(layers)
inv = read_inventory(r"Station_xml\MTX_Full.xml")
station_enus = {
    v: (*reproject(v.sta_info.longitude, v.sta_info.latitude), v.sta_info.elevation)
    for v in NSL.iter_inv(inv)
}

rays = {}
for nsl, enu in station_enus.items():
    for phase in [Phase.P, Phase.S]:
        rays[nsl, phase] = Raypath.isotropic_ray_trace(
            event.enu, enu, velocity_model, phase
        )

reference_freq = np.sqrt(solutions["bpFreqs"][0] * solutions["bpFreqs"][1])

n_obs = len(solutions["ampDict"])

amplitudes, A_matrix = np.zeros(n_obs), np.zeros([n_obs, 6])
for i_obs, (nslp, amp_data) in enumerate(solutions["ampDict"].items()):
    nsl = NSL.parse(".".join(nslp.split(".")[:3]))
    phase = Phase.parse(nslp[-1])
    rec_enu = [v for k, v in station_enus.items() if k == nsl][0]
    if phase.is_S:
        A_matrix[i_obs, :] = inversion_matrix_row(rays[nsl, Phase.S], phase)
    else:
        A_matrix[i_obs, :] = inversion_matrix_row(rays[nsl, Phase.P], phase)
    amplitudes[i_obs] = np.real(
        back_project_amplitude(
            rays[nsl, Phase.P],
            rays[nsl, Phase.S],
            amp_data["amps"][0],
            phase,
            reference_freq,
        )
    )

gn_soln, dc_soln = MTSolution.solve(A_matrix, amplitudes)
jackknife_solutions = MTSolution.jackknife(A_matrix, amplitudes)

dc_jackknifes_mts = [jk_solns[1].mt for jk_solns in jackknife_solutions]

fig, ax = mpls.subplots()
ax.set_azimuth_ticks([])
plot_main_and_jackknife_dc_solutions(ax, dc_soln.mt, dc_jackknifes_mts)
ax.grid()


fig.savefig("m2.8_jackknife_test.png")
