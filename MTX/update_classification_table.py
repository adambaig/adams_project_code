import ast
import json
import os
import requests
import time


import numpy as np
from obspy import UTCDateTime
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

from nmxseis.numerics.moment_tensor import SDR
from NocMeta.Meta import NOC_META


# inputs
athena_code = "MTX"
startTime = "2023-04-01 00:00:00"
endTime = "2023-07-01 00:00:00"
outputPath = r"C:\users\adambaig\Project\MTX\classification_table_to_Q2_2023.csv"
numberChunks = 2
#######################################

host = NOC_META[athena_code]["athIP"]
apiKey = NOC_META[athena_code]["athApi"]


geojson_dir = r"C:\Users\AdamBaig\GeoJSON"


dirPath = os.path.dirname(outputPath)
if not os.path.isdir(dirPath):
    os.makedirs(dirPath)

startTime = UTCDateTime(startTime).timestamp
endTime = UTCDateTime(endTime).timestamp

graphqlurl = f"https://{host}/graphql?apiKey={apiKey}"


timeRange = np.linspace(startTime, endTime, numberChunks, endpoint=True)
nbody = {"data": {"eventList": {"size": 0, "events": []}}}

# out_file = open(outputPath, "w")
iTime = timeRange[0]
i = 0

events = {}

mti_fps_missing = {}
events = run_split_query(
    athena_code:
    start_time: UTCDateTime,
    end_time: UTCDateTime,
    query_function: Callable[[float, float], str],
    n_chunks:,
)


for event in nbody["data"]["eventList"]["events"]:
    if event["id"]:
        id = event["id"]
        moment_tensor_solutions = event["origins"][0]["momentTensors"]
        mt_solutions = {}
        for moment_tensor_solution in moment_tensor_solutions:
            if moment_tensor_solution["conditionNumber"]:
                mt_solutions["mti"] = moment_tensor_solution
            else:
                mt_solutions["fps"] = moment_tensor_solution
        event_point = Point(
            event["origins"][0]["longitude"], event["origins"][0]["latitude"]
        )
        if "fps" in mt_solutions and "mti" in mt_solutions:
            mti_sdr = SDR(
                **{
                    k: v
                    for k, v in mt_solutions["mti"].items()
                    if k in ["strike", "dip", "rake"]
                }
            )
            fps_sdr = SDR(
                **{
                    k: v
                    for k, v in mt_solutions["fps"].items()
                    if k in ["strike", "dip", "rake"]
                }
            )
            similarity = mti_sdr.calculate_similarity(fps_sdr)
            in_county = "outside"
            events[id] = {
                "external_id": event["externalId"].split()[0],
                "origin_id": event["origins"][0]["id"],
                "time": event["origins"][0]["time"],
                "latitude": event["origins"][0]["latitude"],
                "longitude": event["origins"][0]["longitude"],
                "depth": event["origins"][0]["depth"],
                "magnitude": event["origins"][0]["preferredMagnitude"]["value"],
                "MT solution": (
                    "MTI" if mt_solutions["mti"]["isPreferred"] else "FPS"
                ),
                "MTI CN": mt_solutions["mti"]["conditionNumber"],
                "MTI R": mt_solutions["mti"]["pearsonR"],
                "FPS number of errors": mt_solutions["fps"]["numberPolarityErrors"],
                "MTI FPS similarity": similarity,
                "Classification": event["origins"][0][
                    "momentTensorSolutionQuality"
                ],
                "county": in_county,
            }
        elif not aoi_polygon.contains(event_point):
            print(f'event { event["origins"][0]["id"]} outside AOI')
        else:
            mti_fps_missing[id] = event["origins"][0]
            mti_fps_missing[id]["datetime"] = UTCDateTime(
                event["origins"][0]["time"]
            )
            in_county = "outside"
            for county, polygon in county_polygons.items():
                if polygon.contains(event_point):
                    in_county = county
            print(
                f'both mt solutions not posted for event { event["origins"][0]["id"]}\n'
                f"possible class C in county: {in_county}"
            )

class_a_events = set({k: v for k, v in events.items() if v["Classification"] == "A"})
class_b_events = set({k: v for k, v in events.items() if v["Classification"] == "B"})
mti_events = set({k: v for k, v in events.items() if v["MT solution"] == "MTI"})
fps_events = set({k: v for k, v in events.items() if v["MT solution"] == "FPS"})
print(f"Count of all AB events is {len(events)}")
print(f" of those {len(class_a_events)} are class A")
print(f"          {len(class_b_events&mti_events)} are class B MTI")
print(f"          {len(class_b_events&fps_events)} are class B FPS")
