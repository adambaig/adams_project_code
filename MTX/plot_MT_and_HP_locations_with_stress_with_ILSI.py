import json
from pathlib import Path

import ILSI
# noinspection PyUnresolvedReferences
import colorcet as cc
import matplotlib.pyplot as plt
import numpy as np
import pyproj as pr
from matplotlib.ticker import MultipleLocator
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.geometry import enu_to_trend_plunge
from nmxseis.numerics.moment_tensor.stress_inversion import (
    translate_stress_tensor,
    determine_sh_max,
    simpson_aphi,
    decompose_stress,
)
from obspy.imaging.beachball import beach

from source_parameters.read_inputs import read_hp_locations, read_mts_from_query

import sys

sys.path.append(r"C:/Users/AdamBaig/nmx_repos/nmx_plot")
from geojson import plot_topojson_arcs

PI = np.pi
D2R = PI / 180.0
EPSG = 3081

majorLocator1 = MultipleLocator(10000)
latlon_proj = pr.Proj(init="epsg:3081")
out_proj = pr.Proj(init="epsg:4326")
# transformer = Transformer.from_crs("epsg:4326", "epsg:3081")
hp_events = read_hp_locations(
    r"C:\users\adambaig\Project\MTX\hypoDD_summary\DD_with_MT_to_Q2_2023.csv"
)
mt_events = read_mts_from_query(
    r"MT_event_catalog\moment_tensors_up_to_Q2_2023.csv"
)
out_file = "MTX_shmax_orientations_with_tickmarks_to_Q2_2023.csv"
out_image = "stress_axes_up_to_Q2_2023.png"

reproject = EPSGReprojector(EPSG)

search_radius = 10000  # in m
grid_spacing = 5000
min_neighbours = 20
stress_axis_scale = 100
mt_size = 3000
stress_map =  cc.m_CET_R4_r

stress_ticks = {}


def r_nearest_neighbours(grid_point, events, radius):
    radius_sq = radius * radius
    ref_east, ref_north = grid_point["east"], grid_point["north"]
    cull_events = {
        k: v
        for k, v in events.items()
        if (
            (v["easting"] - ref_east) ** 2 + (v["northing"] - ref_north) ** 2
            < radius_sq
        )
    }
    return cull_events


for event in hp_events.values():
    event |= {
        'easting': (enu := event['origin'].enu(reproject))[0],
        'northing': enu[1],
        'elevation': enu[2]
    }





for mt_event in mt_events:
    if mt_event in hp_events:
        mt_events[mt_event]["easting"] = hp_events[mt_event]["easting"]
        mt_events[mt_event]["northing"] = hp_events[mt_event]["northing"]
        mt_events[mt_event]["elevation"] = hp_events[mt_event]["elevation"]
    else:
        easting, northing = pr.transform(
            out_proj,
            latlon_proj,
            mt_events[mt_event]["longitude"],
            mt_events[mt_event]["latitude"],
        )
        mt_events[mt_event]["easting"] = easting
        mt_events[mt_event]["northing"] = northing
        mt_events[mt_event]["elevation"] = -1000 * mt_events[mt_event]["depth_km"]


eastings, northings = np.array(
    [(v["easting"], v["northing"]) for k, v in mt_events.items()]
).T
east_range = np.arange(
    min(eastings) - grid_spacing, max(eastings) + grid_spacing, grid_spacing
)
north_range = np.arange(
    min(northings) - grid_spacing, max(northings) + grid_spacing, grid_spacing
)
quiver_east, quiver_north, trend_north, trend_east, a_phi = [], [], [], [], []


def determine_SH_max_spread(stress_tensors, n_sigma=2):
    sh_max = determine_sh_max(np.sum(stress_tensors, axis=0))
    sh_max_iteration = []
    for stress_tensor in stress_tensors:
        sh_max_central = determine_sh_max(stress_tensor)
        possible_sh_maxes = np.array(
            [sh_max_central - 180.0, sh_max_central, sh_max_central + 180.0]
        )
        i_sh_max = np.argmin(abs(possible_sh_maxes - sh_max))
        sh_max_iteration.append(possible_sh_maxes[i_sh_max])
    return n_sigma * np.std(sh_max_iteration)


# def determine_Aphi_spread(stress_tensors, n_sigma=2):
#     a_phis = []
#     for stress_tensor in stress_tensors:
#         a_phis.append(Simpson_Aphi(stress_tensor))
#     return n_sigma * np.std(a_phis)


f = open(out_file, "w")
f.write(
    "id,latitude,longitude,SH max, 2 sigma err," "A phi, 2 sigma err, number of events"
)
f.write(
    ",tickmark latitude1,tickmark longitude1,tickmark latitude2,tickmark longitude2\n"
)

#
# east, north = pr.transform(out_proj, latlon_proj, -103.813649476043, 31.7572601089813,)
# grid_point = {"east": east, "north": north}
# nn_events = r_nearest_neighbours(grid_point, mt_events, search_radius)


i_id = 0
for east in east_range:
    for north in north_range:
        grid_point = {"east": east, "north": north}
        nn_events = r_nearest_neighbours(grid_point, mt_events, search_radius)
        if len(nn_events) >= min_neighbours:
            i_id += 1

            strikes, dips, rakes = np.array(
                [(v["strike"], v["dip"], v["rake"]) for v in nn_events.values()]
            ).T
            (
                stress_tensor_nwu,
                principal_stresses_comp_pos,
                principal_directions_nwu,
            ) = ILSI.ilsi.inversion_one_set(
                strikes,
                dips,
                rakes,
            )
            ilsi_out = ILSI.ilsi.inversion_one_set(strikes, dips, rakes)
            stress_tensor_nwu,principal_stresses_comp_neg, principal_directions_nwu = ilsi_out["stress_tensor"],ilsi_out["principal_stresses"],ilsi_out["principal_directions"]
            boot_out = ILSI.ilsi.inversion_bootstrap(strikes, dips, rakes)
            boot_stress_tensors_nwu,boot_principal_stresses_comp_neg,boot_axes_nwu, = boot_out["boot_stress_tensor"],boot_out["boot_principal_stresses"],boot_out["boot_principal_directions"]
            stress_tensor = translate_stress_tensor(stress_tensor_nwu)
            boot_stress_tensors = translate_stress_tensor(boot_stress_tensors_nwu)
            sh_max = determine_sh_max(stress_tensor)
            sh_maxes = determine_sh_max(boot_stress_tensors)
            sh_max_error = determine_SH_max_spread(boot_stress_tensors)
            a_phis = simpson_aphi(boot_stress_tensors)
            a_phi_error = np.std(2 * a_phis)
            median_east = np.median(
                np.array([v["easting"] for k, v in nn_events.items()])
            )
            median_north = np.median(
                np.array([v["northing"] for k, v in nn_events.items()])
            )
            average_east = np.median(
                np.array([v["easting"] for k, v in nn_events.items()])
            )
            average_north = np.median(
                np.array([v["northing"] for k, v in nn_events.items()])
            )

            quiver_east.append(average_east)
            quiver_north.append(average_north)
            R, stress_axes = decompose_stress(stress_tensor)
            trend_s2, plunge_s2 = enu_to_trend_plunge(stress_axes["s2"])
            trend_east.append(stress_axis_scale * np.sin(np.pi * sh_max / 180.0))
            trend_north.append(stress_axis_scale * np.cos(np.pi * sh_max / 180.0))
            a_phi.append(simpson_aphi(stress_tensor))
            stress_ticks[i_id] = {
                "sh_max": sh_max,
                "sh_max_2_sigma": sh_max_error,
                "a_phi": a_phi[-1],
                "a_phi_2_sigma": a_phi_error,
                "average_location": {"east": average_east, "north": average_north},
                "grid_location": {"east": east, "north": north},
                "stress_tensor": stress_tensor.tolist(),
                "n_events": len(nn_events),
            }
            longitude, latitude = eastings, northings = pr.transform(
                latlon_proj, out_proj, average_east, average_north
            )
            longitude1, latitude1 = eastings, northings = pr.transform(
                latlon_proj,
                out_proj,
                average_east + trend_east[-1],
                average_north + trend_north[-1],
            )
            longitude2, latitude2 = eastings, northings = pr.transform(
                latlon_proj,
                out_proj,
                average_east - trend_east[-1],
                average_north - trend_north[-1],
            )
            f.write(
                f"{i_id}, {latitude:.7f},{longitude:.7f},"
                f"{sh_max:.1f},{sh_max_error:.1f},"
                f"{a_phi[-1]:.3f},{a_phi_error:.3f},{len(nn_events)},"
            )
            f.write(
                f"{latitude1:.7f},{longitude1:.7f},{latitude2:.7f},{longitude2:.7f}\n"
            )


f.close()

fig, ax = plt.subplots(figsize=[16, 12])
ax.set_aspect("equal")
hp_eastings, hp_northings = np.array(
    [(v["easting"], v["northing"]) for k, v in hp_events.items()]
).T
ax.plot(
    hp_eastings,
    hp_northings,
    ".",
    color="k",
    ms=1,
    markeredgecolor="k",
    zorder=-5,
)


for event in mt_events:
    beachball = beach(
        np.array(
            [
                mt_events[event]["strike"],
                mt_events[event]["dip"],
                mt_events[event]["rake"],
            ]
        ),
        xy=(mt_events[event]["easting"], mt_events[event]["northing"]),
        width=mt_size,
        facecolor="0.3",
        alpha=0.8,
        linewidth=0.25,
        zorder=3,
    )
    ax.add_collection(beachball)

ax.quiver(
    quiver_east,
    quiver_north,
    trend_east,
    trend_north,
    color=stress_map(np.array(a_phi) / 3),
    pivot="mid",
    width=0.003,
    zorder=5,
    headlength=0,
    headaxislength=0,
    headwidth=0,
    scale=1 / stress_axis_scale,
    scale_units="x",
    edgecolor="k",
    linewidth=0.5,
)

plot_counties = [
    "Pecos_County",
    "Loving_County",
    "Ward_County",
    "Winkler_County",
    "Andrews_County",
    "Ector_County",
    "Crane_County",
    "Midland_County",
    "Martin_County",
    "Howard_County",
    "Glasscock_County",
    "Gaines_County",
    "Dawson_County",
    "Borden_County",
    "Scurry_County",
    "Mitchell_County",
    "Sterling_County",
    "Tom_Green_County",
    "Reagan_County"
]

geojson_dir = Path(r"C:\Users\AdamBaig\GeoJSON")

nm_topojson_path = geojson_dir / "NM-35-new-mexico-counties.json"
stateline_topojson_path = geojson_dir / "us-albers.json"
tx_counties = json.loads((geojson_dir / "Texas_County_Boundaries_Detailed.geojson").read_text())
for county in tx_counties["features"]:
    long_lats = county["geometry"]["coordinates"][0][0]
    county_east, county_north = pr.transform(
        out_proj, latlon_proj, [p[0] for p in long_lats], [p[1] for p in long_lats]
    )
    ax.plot(county_east, county_north, lw=0.5, c="0.2", zorder=-10)

ax.set_facecolor("0.95")
ax.grid(True)
ax.xaxis.set_major_locator(MultipleLocator(10000))
ax.yaxis.set_major_locator(MultipleLocator(10000))
ax.set_yticklabels([])
ax.set_xticklabels([])
ax.tick_params(axis="both", which="both", color="w")
xmin = np.percentile(hp_eastings, 0.5)
xmax = np.percentile(hp_eastings, 99.5)
ymin = np.percentile(hp_northings, 0.5)
ymax = np.percentile(hp_northings, 99.5)
ax.set_xlim([xmin - 0.1 * (xmax - xmin), xmax + 0.1 * (xmax - xmin)])
ax.set_ylim([ymin - 0.1 * (ymax - ymin), ymax + 0.1 * (ymax - ymin)])
plot_topojson_arcs(ax, [nm_topojson_path], c="0.2", lw=1, zorder=-10)
plot_topojson_arcs(ax, [stateline_topojson_path], c="0.1", lw=2, zorder=-10)
fig.savefig(out_image, bbox_inches="tight")

fg_cb = plt.figure(figsize=[4, 1])
ax_cb = fg_cb.add_axes([0.1, 0.6, 0.8, 0.2])
colorbar = np.array([np.arange(0, 255), np.arange(0, 255)])
ax_cb.pcolor(colorbar, cmap=stress_map)
ax_cb.set_xticks([0, 85, 170, 255])
ax_cb.set_xticklabels([])
ax_cb.tick_params(which="minor", color="w")
ax_cb.set_xticks([42.5, 127.5, 212.5], minor=True)
ax_cb.set_xticklabels(["normal", "strike-slip", "thrust"], minor=True)
ax_cb.set_yticks([])
ax_cb.set_xlabel("Stress State")
fg_cb.savefig("a_phi_legend.png", transparent=True)
