import matplotlib.pyplot as plt

f1, a1 = plt.subplots()
(p_axis,) = a1.plot(
    0,
    0,
    "o",
    color="mediumorchid",
    markeredgecolor="0.2",
)
(t_axis,) = a1.plot(
    0,
    0,
    "o",
    color="springgreen",
    markeredgecolor="0.2",
)

a2.legend([p_axis, t_axis], ["P axes", "T axes"])
f2.savefig("pt_legend.png")

f2, a2 = plt.subplots()
sig1 = a2.plot(0, 0, "o", color="firebrick", markeredgecolor="0.2")
sig2 = a2.plot(0, 0, "o", color="forestgreen", markeredgecolor="0.2")
sig3 = a2.plot(0, 0, "o", color="royalblue", markeredgecolor="0.2")
