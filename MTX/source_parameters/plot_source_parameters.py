import json
from datetime import datetime

import matplotlib.dates as md
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm

from read_inputs import BASE_DIR

MIN_N = 20
N_DAYS = 20
TAB10 = cm.get_cmap('tab10')
sp_jsons = BASE_DIR.joinpath('source_param_jsons').glob('*.json')

source_parameters = {}
for sp_json in sp_jsons:
	event_id = sp_json.stem.removeprefix('sp_results_')
	source_parameters[event_id] = json.loads(sp_json.read_text())

m5_event = json.loads(BASE_DIR.joinpath('111505.json').read_text())

sp_succeeded = {k: v for k, v in source_parameters.items() if 'error' not in v}
sp_succeeded['00111505'] = {
	'mw': (m5_origin := m5_event['event']['origin'])['magnitudes'][0]['value'],
	'apparent_stress': m5_origin['apparentStress'] * 1e6,
	'stress_drop': m5_origin['stressDrop'] * 1e6,
	'origin time': m5_origin['time'],
	'depth': m5_origin['depth'] * 1000,
	'quality_score': m5_origin['sourceParameterQualityClass']
}

m5 = [v for v in sp_succeeded.values() if v['mw'] > 4.9][0]
sp_sorted = sorted(sp_succeeded.items(), key=lambda kv: kv[1]['origin time'])
for event in sp_succeeded.values():
	event['reformatted time'] = md.date2num(datetime.utcfromtimestamp(event['origin time']))
reformatted_times = md.date2num(
	[datetime.utcfromtimestamp(v['origin time']) for v in sp_succeeded.values()])
apparent_stress = [v['apparent_stress'] for v in sp_succeeded.values()]
stress_drop = [v['stress_drop'] for v in sp_succeeded.values()]
depths = [v['depth'] for v in sp_succeeded.values()]

fig, ax = plt.subplots(2, sharex=True)
ax[0].semilogy(reformatted_times, apparent_stress, '.', zorder=-4)
ax[1].semilogy(reformatted_times, stress_drop, '.', zorder=-4)
ax[1].set_ylim([7e3, 7e7])
ax[1].xaxis.set_major_formatter(md.DateFormatter("%b %d\n%Y"))
ax[0].semilogy(m5['reformatted time'], m5['apparent_stress'], '*', ms=10, color='gold', mec='0.2')
ax[1].semilogy(m5['reformatted time'], m5['stress_drop'], '*', ms=10, color='gold', mec='0.2')


def calc_median_parameter(key, n_days, min_n):
	median_param, times = [], []
	for params in sp_sorted:
		center_time = params[1]['reformatted time']
		windowed_params = [v[1] for v in sp_sorted if
		                   abs(v[1]['reformatted time'] - center_time) < n_days / 2]
		if len(windowed_params) >= min_n:
			median_param.append(np.median([v[key] for v in windowed_params]))
			times.append(center_time)
	return np.array(median_param), np.array(times)


med_as, times = calc_median_parameter('apparent_stress', N_DAYS, MIN_N)
med_sd, times = calc_median_parameter('stress_drop', N_DAYS, MIN_N)

median_as = np.median(apparent_stress)
median_sd = np.median(stress_drop)

ax[0].semilogy(times, med_as)
ax[0].semilogy([times[0], times[-1]], [median_as, median_as], 'k--', zorder=-2)
ax[1].semilogy([times[0], times[-1]], [median_sd, median_sd], 'k--', zorder=-2)
ax[1].semilogy(times, med_sd)
ax[0].set_ylabel('apparent stress (Pa)')
ax[1].set_ylabel('static stress drop (Pa)')
ax[0].set_title(f'smoothing window: {N_DAYS} days, minimum {MIN_N} events')
for a in ax:
	a.set_facecolor('0.96')

fig.savefig('stress_by_time.png')

scatter_kwargs = {'cmap': 'viridis_r',
                  'zorder': -4,
                  's': 8,
                  'lw': 0.25,
                  'edgecolor': '0.2',
                  'vmin': 5000,
                  'vmax': 7500}

fig_sc, ax_sc = plt.subplots(2, sharex=True)
scatter = ax_sc[0].scatter(reformatted_times, apparent_stress, c=depths, **scatter_kwargs)
ax_sc[1].scatter(reformatted_times, stress_drop, c=depths, **scatter_kwargs)
for a in ax_sc:
	a.set_yscale('log')
	a.set_facecolor('0.96')
ax_sc[1].set_ylim([7e3, 7e7])
ax_sc[1].xaxis.set_major_formatter(md.DateFormatter("%b %d\n%Y"))
for event in [v for v in sp_succeeded.values() if v['mw'] > 2]:
	ax_sc[0].semilogy(event['reformatted time'], event['apparent_stress'], '*', ms=12,
	                  color=scatter.to_rgba(event['depth']), mec='0.2')
	ax_sc[1].semilogy(event['reformatted time'], event['stress_drop'], '*', ms=12,
	                  color=scatter.to_rgba(event['depth']), mec='0.2')
cbar = fig_sc.colorbar(scatter, ax=ax_sc)
cbar.ax.invert_yaxis()
cbar.set_label('depth (m)')
ax_sc[0].semilogy(times, med_as, color=TAB10(0.15))
ax_sc[1].semilogy(times, med_sd, color=TAB10(0.15))
ax_sc[0].semilogy([times[0], times[-1]], [median_as, median_as], 'k--', zorder=-2)
ax_sc[1].semilogy([times[0], times[-1]], [median_sd, median_sd], 'k--', zorder=-2)

ax_sc[0].set_ylabel('apparent stress (Pa)')
ax_sc[1].set_ylabel('static stress drop (Pa)')
ax_sc[0].set_title(f'smoothing window: {N_DAYS} days, minimum {MIN_N} events')
fig_sc.savefig('stress_by_time_and_depth.png')

fig_mr, ax_mr = plt.subplots()

