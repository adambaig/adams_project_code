import json
from pathlib import Path

from nmxseis.interact.athena import AthenaClient
from nmxseis.model.nslc import NSL
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.velocity_model.vm_1d import VelocityModel1D, VMLayer1D
from obspy import UTCDateTime, read_inventory

from NocMeta.Meta import NOC_META

ATHENA_CONFIG = NOC_META["MTX"]
ATHENA_CLIENT = AthenaClient(
    rf'http://{ATHENA_CONFIG["athIP"]}', ATHENA_CONFIG["athApi"]
)

BASE_DIR = Path(__file__).parent
MTX_EPSG = 3081
REPROJECT = EPSGReprojector(MTX_EPSG)
INV = read_inventory(BASE_DIR.joinpath('MTX_Full.xml'))
SP_CONFIG = {
	"qualities": {
		"A": {"min_snr": 3, "min_samples_per_bin": 5, "f_smooth_min": 0.1},
		"B": {"min_snr": 3, "min_samples_per_bin": 3, "f_smooth_min": 0.5},
		"C": {"min_snr": 1.5, "min_samples_per_bin": 2, "f_smooth_min": 1},
	},
	"lowpass_nyq_backoff": 0.2,
	"min_d_hypo_m": 0,
	"max_d_hypo_m": 50000,
	"Q": {"P": 500, "S": 500},
	"kappa": 0,
	"f_smooth_max": 100,
	"f_smooth_n_bins": 41,
	"radius_model": "Madariaga",
	"spectrum_model": "Brune",
}

def get_station_enus():
	return REPROJECT.get_station_enus(INV, NSL.iter_inv(INV))


def get_velocity_model():
	with open(BASE_DIR.joinpath("velocity_model.json")) as f:
		velocity_model_json = json.load(f)
	layers = []
	for layer in velocity_model_json:
		layers.append(
			VMLayer1D(
				vp=layer["vp"],
				vs=layer["vs"],
				rho=layer["rho"],
				top=layer["top"] if "top" in layer else np.inf,
			)
		)
	return VelocityModel1D(layers)


def read_hp_locations(filename):
	f = open(filename)
	_head = f.readline()
	lines = f.readlines()
	f.close()
	events = {}
	for line in lines:
		lspl = line.split(",")
		id = lspl[0]
		events[id] = {
			"external_id": lspl[1].split()[0],
			"origin": EventOrigin(lat=float(lspl[3]), lon=float(lspl[4]),
			                      time=UTCDateTime(float(lspl[2])), depth_m=float(lspl[5]) * 1000),
			"magnitude": float(lspl[6]),
		}
	return events

def read_mts_from_query(mt_file):
    """
    reads output of mt_query.py
    """
    events = {}
    with open(mt_file) as f:
        lines = f.readlines()
    for line in lines:
        split_line = line.split(",")
        events[split_line[0]] = {
            "external id": split_line[1],
            "UTC time": UTCDateTime(float(split_line[2])),
            "latitude": float(split_line[3]),
            "longitude": float(split_line[4]),
            "depth_km": float(split_line[5]),
            "magnitude": float(split_line[6]),
            "strike": float(split_line[7]),
            "dip": float(split_line[8]),
            "rake": float(split_line[9]),
        }
    return events