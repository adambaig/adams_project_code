
from nmxseis.numerics.formulas import convert_radius_corner_brune, calculate_eshelby_stress_drop
import numpy as np

moment = 33424635239352788
p_corner = 0.8484176565278334
s_corner = 0.5567740870963906
vp = 5739.0
vs = 3279.4285714285716

rad_p = convert_radius_corner_brune(p_corner, vp)
rad_s = convert_radius_corner_brune(s_corner, vs)

rad = np.sqrt(rad_s*rad_p)

sd = calculate_eshelby_stress_drop(moment, rad)