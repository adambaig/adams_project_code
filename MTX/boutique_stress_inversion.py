import json

# noinspection PyUnresolvedReferences
import colorcet as cc
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from matplotlib.cm import get_cmap
import mplstereonet as mpls
import numpy as np
import pyproj as pr

from nmxseis.numerics.moment_tensor.stress_inversion import (
    translate_stress_tensor,
    determine_sh_max,
    simpson_aphi,
    decompose_stress,
)
from nmxseis.numerics.geodesy import EPSGReprojector
from nmxseis.numerics.geometry import enu_to_trend_plunge
from nmxseis.numerics.moment_tensor import (
    SDR,
    MTDecomp,
)
from source_parameters.read_inputs import read_hp_locations, read_mts_from_query

import ILSI

PI = np.pi
D2R = PI / 180.0
EPSG = 3081

majorLocator1 = MultipleLocator(10000)
latlon_proj = pr.Proj(init="epsg:3081")
out_proj = pr.Proj(init="epsg:4326")
# transformer = Transformer.from_crs("epsg:4326", "epsg:3081")
hp_events = read_hp_locations(
    r"C:\Users\adambaig\Project\MTX\hypoDD_summary\DD_with_MT_Feb_28_2023.csv"
)
mt_events = read_mts_from_query(
    r"MT_event_catalog\west_of_hwy_349.csv"
)


reproject = EPSGReprojector(EPSG)

search_radius = 10000  # in m
grid_spacing = 5000
min_neighbours = 20
stress_axis_scale = 100
mt_size = 3000
stress_map = get_cmap("cet_rainbow4_r")

stress_ticks = {}


def r_nearest_neighbours(grid_point, events, radius):
    radius_sq = radius * radius
    ref_east, ref_north = grid_point["east"], grid_point["north"]
    cull_events = {
        k: v
        for k, v in events.items()
        if (
            (v["easting"] - ref_east) ** 2 + (v["northing"] - ref_north) ** 2
            < radius_sq
        )
    }
    return cull_events


for event in hp_events.values():
    event |= {
        'easting': (enu := event['origin'].enu(reproject))[0],
        'northing': enu[1],
        'elevation': enu[2]
    }



for mt_event in mt_events:
    if mt_event in hp_events:
        mt_events[mt_event]["easting"] = hp_events[mt_event]["easting"]
        mt_events[mt_event]["northing"] = hp_events[mt_event]["northing"]
        mt_events[mt_event]["elevation"] = hp_events[mt_event]["elevation"]
    else:
        easting, northing = pr.transform(
            out_proj,
            latlon_proj,
            mt_events[mt_event]["longitude"],
            mt_events[mt_event]["latitude"],
        )
        mt_events[mt_event]["easting"] = easting
        mt_events[mt_event]["northing"] = northing
        mt_events[mt_event]["elevation"] = -1000 * mt_events[mt_event]["depth_km"]


for mt_event in mt_events.values():
    mt_event["sdr"] = SDR(
        strike=mt_event["strike"], dip=mt_event["dip"], rake=mt_event["rake"]
    )
    mt_event["dc mt"] = mt_event["sdr"].to_mt_dc()
    mt_event["mt decomp"] = MTDecomp.from_mt_mat(mt_event["dc mt"].mat)

eastings, northings = np.array(
    [(v["easting"], v["northing"]) for k, v in mt_events.items()]
).T
east_range = np.arange(
    min(eastings) - grid_spacing, max(eastings) + grid_spacing, grid_spacing
)
north_range = np.arange(
    min(northings) - grid_spacing, max(northings) + grid_spacing, grid_spacing
)
quiver_east, quiver_north, trend_north, trend_east, a_phi = [], [], [], [], []


def determine_SH_max_spread(stress_tensors, n_sigma=2):
    sh_max = determine_sh_max(np.sum(stress_tensors, axis=0))
    sh_max_iteration = []
    for stress_tensor in stress_tensors:
        sh_max_central = determine_sh_max(stress_tensor)
        possible_sh_maxes = np.array(
            [sh_max_central - 180.0, sh_max_central, sh_max_central + 180.0]
        )
        i_sh_max = np.argmin(abs(possible_sh_maxes - sh_max))
        sh_max_iteration.append(possible_sh_maxes[i_sh_max])
    return n_sigma * np.std(sh_max_iteration)


def determine_Aphi_spread(stress_tensors, n_sigma=2):
    a_phis = []
    for stress_tensor in stress_tensors:
        a_phis.append(simpson_aphi((stress_tensor)))
    return n_sigma * np.std(a_phis)


def angular_difference(tensor1, tensor2):
    for tensor in [tensor1, tensor2]:
        tensor /= np.linalg.norm(tensor)
    return 180 * np.arccos(np.tensordot(tensor1, tensor2)) / np.pi

i_id = 0
for east in east_range:
    for north in north_range:
        grid_point = {"east": east, "north": north}

        nn_events = r_nearest_neighbours(grid_point, mt_events, search_radius)
        if len(nn_events) >= min_neighbours:
            sufficient_nn_events = nn_events
            i_id += 1
            strikes, dips, rakes = np.array(
                [(v["strike"], v["dip"], v["rake"]) for v in nn_events.values()]
            ).T
            (
                stress_tensor_nwu,
                principal_stresses_comp_pos,
                principal_directions_nwu,
            ) = ILSI.ilsi.inversion_one_set(
                strikes,
                dips,
                rakes,
            )
            (
                boot_stress_tensors_nwu,
                boot_principal_stresses_comp_neg,
                boot_axes_nwu,
            ) = ILSI.ilsi.inversion_bootstrap(
                strikes,
                dips,
                rakes,
            )
            stress_tensor = translate_stress_tensor(stress_tensor_nwu)
            boot_stress_tensors = translate_stress_tensor(boot_stress_tensors_nwu)
            sh_max = determine_sh_max(stress_tensor)
            sh_maxes = determine_sh_max(boot_stress_tensors)
            sh_max_error = determine_SH_max_spread(boot_stress_tensors)
            a_phis = simpson_aphi(boot_stress_tensors)
            a_phi_error = np.std(2 * a_phis)
            median_east = np.median(
                np.array([v["easting"] for k, v in nn_events.items()])
            )
            median_north = np.median(
                np.array([v["northing"] for k, v in nn_events.items()])
            )
            average_east = np.median(
                np.array([v["easting"] for k, v in nn_events.items()])
            )
            average_north = np.median(
                np.array([v["northing"] for k, v in nn_events.items()])
            )

            quiver_east.append(average_east)
            quiver_north.append(average_north)
            R, stress_axes = decompose_stress(stress_tensor)
            trend_s2, plunge_s2 = enu_to_trend_plunge(stress_axes["s2"])
            trend_east.append(stress_axis_scale * np.sin(np.pi * sh_max / 180.0))
            trend_north.append(stress_axis_scale * np.cos(np.pi * sh_max / 180.0))
            a_phi.append(simpson_aphi(stress_tensor))

            fig_rhist, ax_rhist = plt.subplots()
            boot_r_values, boot_stress_axes = decompose_stress(boot_stress_tensors)
            a_phis = simpson_aphi(boot_stress_tensors)
            ax_rhist.hist(boot_r_values, np.arange(0, 1.01, 0.05))
            ylim = ax_rhist.get_ylim()
            ax_rhist.plot([R, R], ylim, color="0.1")
            ax_rhist.set_ylim(*ylim)
            fig_rhist.savefig(
                f"stress_vingettes//r_histogram_{str(i_id).zfill(3)}.png",
                bbox_inches="tight",
            )

            fig_aphi_hist, ax_aphi_hist = plt.subplots()
            ax_aphi_hist.hist(a_phis, np.arange(0, 3.01, 0.05))
            ylim = ax_aphi_hist.get_ylim()
            ax_aphi_hist.plot([a_phi[-1], a_phi[-1]], ylim, color="0.1")
            ax_aphi_hist.set_ylim(*ylim)

            ax_aphi_hist.set_xticks([0, 0.5, 1, 1.5, 2, 2.5, 3])
            ax_aphi_hist.set_xticklabels(["0", "normal", 1, "strike-slip", 2, "thrust", 3])
            ax_aphi_hist.set_xlabel("$A_{\phi}$ (Simpson, 1997)")
            fig_aphi_hist.savefig(
                f"stress_vingettes//aphi_histogram_{str(i_id).zfill(3)}.png",
                bbox_inches="tight",
                dpi=300,
            )

            fig, ax = mpls.subplots()

            ax.grid()

            for s1_axis, s2_axis, s3_axis in zip(
                boot_stress_axes["s1"], boot_stress_axes["s2"], boot_stress_axes["s3"]
            ):
                s1_trend, s1_plunge = enu_to_trend_plunge(s1_axis)
                s2_trend, s2_plunge = enu_to_trend_plunge(s2_axis)
                s3_trend, s3_plunge = enu_to_trend_plunge(s3_axis)
                ax.line(abs(s1_plunge), s1_trend, ".", color="firebrick", alpha=0.5)
                ax.line(abs(s2_plunge), s2_trend, ".", color="forestgreen", alpha=0.5)
                ax.line(abs(s3_plunge), s3_trend, ".", color="royalblue", alpha=0.5)

            s1_trend, s1_plunge = enu_to_trend_plunge(stress_axes["s1"])
            s2_trend, s2_plunge = enu_to_trend_plunge(stress_axes["s2"])
            s3_trend, s3_plunge = enu_to_trend_plunge(stress_axes["s3"])
            ax.line(
                abs(s1_plunge), s1_trend, "o", color="firebrick", markeredgecolor="0.2"
            )
            ax.line(
                abs(s2_plunge),
                s2_trend,
                "o",
                color="forestgreen",
                markeredgecolor="0.2",
            )
            ax.line(
                abs(s3_plunge), s3_trend, "o", color="royalblue", markeredgecolor="0.2"
            )
            ax._polar.set_position(ax.get_position())
            fig.savefig(
                f"stress_vingettes//stress_axes_{str(i_id).zfill(3)}.png",
                bbox_inches="tight",
            )

            fig_ang_scatter, ax_ang_scatter = mpls.subplots()
            ax_ang_scatter.line(
                [e["mt decomp"].p_plunge for e in sufficient_nn_events.values()],
                [e["mt decomp"].p_trend for e in sufficient_nn_events.values()],
                "o",
                color="mediumorchid",
                markeredgecolor="0.2",
            )
            ax_ang_scatter.line(
                [e["mt decomp"].t_plunge for e in sufficient_nn_events.values()],
                [e["mt decomp"].t_trend for e in sufficient_nn_events.values()],
                "o",
                color="springgreen",
                markeredgecolor="0.2",
            )
            ax_ang_scatter.grid()
            ax_ang_scatter._polar.set_position(ax.get_position())
            fig_ang_scatter.savefig(
                f"stress_vingettes//pt_scatter_plots_{str(i_id).zfill(3)}.png",
                bbox_inches="tight",
            )

            fig_map, ax_map = plt.subplots()
            ax_map.set_aspect("equal")
            ax_map.plot(
                [v["easting"] for v in mt_events.values()],
                [v["northing"] for v in mt_events.values()],
                ".",
                color="0.2",
                zorder=-1,
            )
            ax_map.plot(
                [v["easting"] for v in sufficient_nn_events.values()],
                [v["northing"] for v in sufficient_nn_events.values()],
                "o",
                color="orangered",
                markeredgecolor="0.1",
                zorder=12,
            )
            xlim = ax_map.get_xlim()
            ylim = ax_map.get_ylim()
            plot_counties = [
                "Pecos_County",
                "Loving_County",
                "Ward_County",
                "Winkler_County",
                "Andrews_County",
                "Ector_County",
                "Crane_County",
                "Midland_County",
                "Martin_County",
                "Howard_County",
                "Glasscock_County",
            ]
            geojson_dir = "C:\\Users\\adambaig\\tx.geojson\\counties\\individual\\"
            for county in plot_counties:
                county_json = json.load(open(geojson_dir + county + ".geojson"))
                long_lats = np.array(
                    county_json["features"][0]["geometry"]["coordinates"][0]
                )
                county_east, county_north = pr.transform(
                    out_proj, latlon_proj, long_lats[:, 0], long_lats[:, 1]
                )
                ax_map.plot(county_east, county_north, lw=0.5, c="0.2", zorder=-10)

            ax_map.set_facecolor("0.95")
            ax_map.grid(True)
            ax_map.xaxis.set_major_locator(MultipleLocator(10000))
            ax_map.yaxis.set_major_locator(MultipleLocator(10000))
            ax_map.set_yticklabels([])
            ax_map.set_xticklabels([])
            ax_map.tick_params(axis="both", which="both", color="w")
            ax_map.set_xlim(*xlim)
            ax_map.set_ylim(*ylim)
            fig_map.savefig(
                f"stress_vingettes//cluster_{str(i_id).zfill(3)}.png",
                bbox_inches="tight",
            )
            plt.close('all')
