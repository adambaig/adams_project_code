import os
import requests
import numpy as np
from obspy import UTCDateTime
import ast
import json

# inputs
host = "athena.mtx.nanometrics.net"
apikey = "23f76a2d-c954-4b29-81a5-062905328324"
start_time = "2020-01-01 00:00:00"
end_time = "2024-01-01 00:00:00"
latitudes = [31.90687, 32.13770]
longitudes = [-102.4515, -102.0069]
number_of_chunks = 2
output_path = r"C:\users\adambaig\Project\MTX\MT_event_catalog\Gardendale_clusters_mt.csv"
#######################################


directory = os.path.dirname(output_path)
if not os.path.isdir(directory):
    os.makedirs(directory)


graphql_url = "https://{}/graphql?apiKey={}".format(host, apikey)

start_time_str = f"startTime: {UTCDateTime(start_time).timestamp}," if start_time is not None else ""
end_time_str = f"endTime: {UTCDateTime(end_time).timestamp}," if end_time is not None else ""
lat_min_str = f"latitudeMinimum: {min(latitudes)}," if latitudes is not None else ""
lat_max_str = f"latitudeMaximum: {max(latitudes)}," if latitudes is not None else ""
lon_min_str = f"longitudeMinimum: {min(longitudes)}," if longitudes is not None else ""
lon_max_str = f"longitudeMaximum: {max(longitudes)}," if longitudes is not None else ""

nbody = {"data": {"eventList": {"size": 0, "events": []}}}

out_file = open(output_path, "w")

query = (
    f"""
    query {{
    eventList(
    {start_time_str}{end_time_str}{lat_min_str}{lat_max_str}{lon_min_str}{lon_max_str}
    magnitudeMinimum: 2,
    ) {{
      size
      totalSize
      events {{
        id
        externalId
        preferredOrigin{{
            id
            latitude
            longitude
            depth
            time
            preferredMagnitude {{
              value
              magnitudeType
            }}
            momentTensors{{
              id
        	  strike
              dip
              rake
              isPreferred
            }}
        }}
      }}
    }}
  }}
"""
)
r = requests.post(graphql_url, data={"query": query})

status = r.status_code
reason = r.reason
body = r.text

bodyt = ast.literal_eval(json.dumps(body))
nbody = json.loads(bodyt)
print("event count, ", nbody["data"]["eventList"]["size"])
for event in nbody["data"]["eventList"]["events"]:
    id = event["id"]
    external_id = event["externalId"].split()[0]
    event_time = event["preferredOrigin"]["time"]
    event_lat = event["preferredOrigin"]["latitude"]
    event_lon = event["preferredOrigin"]["longitude"]
    event_depth = event["preferredOrigin"]["depth"]
    event_mag = event["preferredOrigin"]["preferredMagnitude"]["value"]
    event_mts = event["preferredOrigin"]["momentTensors"]
    if len(event["preferredOrigin"]["momentTensors"]) > 1:
        i_preferred_mt = [i for i, mt in enumerate(event_mts) if mt['isPreferred']][0]
        strike = event["preferredOrigin"]["momentTensors"][i_preferred_mt]["strike"]
        dip = event["preferredOrigin"]["momentTensors"][i_preferred_mt]["dip"]
        rake = event["preferredOrigin"]["momentTensors"][i_preferred_mt]["rake"]
    elif len(event["preferredOrigin"]["momentTensors"]) == 1:
        strike = event["origins"][0]["momentTensors"][0]["strike"]
        dip = event["origins"][0]["momentTensors"][0]["dip"]
        rake = event["origins"][0]["momentTensors"][0]["rake"]
    else:
        print(f"failed on event {id}")
        continue
    out_file.write(
        f"{id},{external_id},{event_time:.3f},{event_lat:.7f},{event_lon:.7f},{event_depth:.4f},{event_mag:.2f}"
    )
    out_file.write(f",{strike:.4f},{dip:.4f},{rake:.4f}\n")
    #
    # except:
    #     print("failed", event["id"])
    #     continue


out_file.close()
