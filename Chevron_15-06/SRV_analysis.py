from copy import deepcopy
from datetime import datetime, timedelta
from matplotlib import cm, colors
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import os
import pytz
from scipy.ndimage import zoom
from scipy.interpolate import RectBivariateSpline

from pfi_qi.QI_analysis import (
    strain_grid,
    output_isosurface,
    event_density_grid,
)
from pfi_qi.engineering import sorted_stage_list
from read_inputs import (
    read_catalog,
    read_treatment,
    get_velocity_model,
    read_wells,
    read_strain,
    read_or_load_json,
    read_tops,
)
from plotting_stuff import gray_background_with_grid

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")
depth_interp_factor = 10
method = "event density"

velocity_model = get_velocity_model()
events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
wells = read_wells()
treatment = read_or_load_json(read_treatment, "treatment_data.json")
sorted_stages = read_or_load_json(
    sorted_stage_list, "sorted_stage_list.json", treatment
)
tops = read_tops()

# This block is dealing with the split stage.

split_stage = {
    k: v for k, v in events.items() if v["well"] == "1" and v["stage"] == "05"
}
for event_id, event in split_stage.items():
    if event["timestamp"] < utc.localize(datetime(2019, 12, 9, 9, 0)):
        event["stage"] = "05.0"
    else:
        event["stage"] = "05.5"
pump_wells = deepcopy(wells)
pump_wells["1"]["Stage 05.0"] = pump_wells["1"]["Stage 05"]
pump_wells["1"]["Stage 05.5"] = pump_wells["1"]["Stage 05"]
pump_wells["1"].pop("Stage 05")


events_no_is = {
    k: v
    for k, v in events.items()
    if v["treatment_code"].strip() not in ["IS", "PRE", "POST"]
    and v["magnitude"] > -1.25
}

ireton = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["H_V_Ireton"])
duvernay = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Duvernay"])
majeau = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Majeau_Lake"])
beaverhill = RectBivariateSpline(
    tops["e_grid"], tops["n_grid"], tops["Beaverhill_Lake"]
)


event_density_dir = "event_density//"
grid_spacing = 40
n_neighbours = 10
event_density, grid_points = event_density_grid(
    events_no_is,
    grid_spacing,
    n_neighbours,
    csv_out=event_density_dir + "all.csv",
)
non_zero_density = [v for v in np.ravel(event_density) if v > 0]
one_event_in_80_sphere = 3 / 4 / PI / 80 / 80 / 80
vmin = np.log10(one_event_in_80_sphere)
vmax = np.percentile(np.log10(non_zero_density), 99.9)
east_grid = np.unique(grid_points[:, 0])
north_grid = np.unique(grid_points[:, 1])
elevation_grid = np.unique(grid_points[:, 2])
n_e, n_n, n_z = len(east_grid), len(north_grid), len(elevation_grid)
z_grid_interp = zoom(elevation_grid, depth_interp_factor)
in_duvernay = np.zeros([n_e, n_n, depth_interp_factor * n_z])
for i_e, e in enumerate(east_grid):
    for i_n, n in enumerate(north_grid):
        duvernay_top = duvernay(e, n)
        duvernay_bottom = majeau(e, n)
        for i_z, z in enumerate(z_grid_interp):
            if z > duvernay_bottom and z < duvernay_top:
                in_duvernay[i_e, i_n, i_z] = 1


levels = np.linspace(vmin, vmax, 9)
color_scale = cm.ScalarMappable(
    norm=colors.Normalize(vmin=vmin, vmax=vmax), cmap="viridis_r"
)
fig, ax = plt.subplots(figsize=[16, 16])
ax.set_aspect("equal")
contour_colors = []
for level in levels:
    contour_colors.append(color_scale.to_rgba(level))
contours = ax.contourf(
    east_grid,
    north_grid,
    np.log10(event_density[:, :, 17]).T,
    vmin=vmin,
    levels=levels,
    vmax=vmax,
    colors=contour_colors,
    extend="max",
)
for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k")

ax = gray_background_with_grid(ax)
vertexes, volume_tags, volume_all = output_isosurface(
    event_density,
    10**vmin,
    grid_points,
    event_density_dir + "all_isosurfaces.csv",
)
cbaxes = fig.add_axes([0.2, 0.18, 0.6, 0.03])
cb = plt.colorbar(contours, cax=cbaxes, orientation="horizontal")
ticks = cb.get_ticks()
cb.set_ticklabels([f"{10**tick:.1e}".replace("-", "$-$") for tick in ticks])
cb.set_label("events per cubic meter")

x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()
fig.savefig(event_density_dir + "//figures//all_fig1.png", dpi=150, bbox_inches="tight")

g = open(event_density_dir + "cumulative_volume.csv", "w")
g.write(
    "unique stage id, well, stage, total volume, total differential, Duv volume, Duv differential\n"
)
g.write(",,,(m3),(m3),(m3),(m3)\n")
volume_old = 0
duv_volume_old = 0
for i_stage, stage in enumerate(sorted_stages):
    end_time = utc.localize(
        datetime.strptime(str(stage["end_time"]).split(".")[0], "%Y%m%d%H%M%S")
    )
    local_end_time = end_time.astimezone(timezone)
    cumulative_events = {
        k: v for k, v in events_no_is.items() if v["timestamp"] < end_time
    }
    out_tag = (
        f'after_{str(i_stage+1).zfill(3)}_Well{stage["well"]}_Stage{stage["stage"]}'
    )
    cumulative_event_density, _ = event_density_grid(
        cumulative_events,
        grid_points,
        n_neighbours,
        csv_out=event_density_dir + out_tag + ".csv",
    )
    fig, ax = plt.subplots(figsize=[16, 16])
    ax.set_aspect("equal")

    contour_colors = []
    for level in levels:
        contour_colors.append(color_scale.to_rgba(level))
    cumulative_depth_interpolate = zoom(
        cumulative_event_density, [1, 1, depth_interp_factor]
    )
    ax.contourf(
        east_grid,
        north_grid,
        np.log10(cumulative_event_density[:, :, 18]).T,
        vmin=vmin,
        levels=levels,
        vmax=vmax,
        colors=contour_colors,
        extend="max",
    )
    for well in pump_wells.values():
        ax.plot(well["easting"], well["northing"], "k")
    for perf, cluster in pump_wells[stage["well"]]["Stage " + stage["stage"]].items():
        cluster["easting"] = 0.5 * (cluster["top_east"] + cluster["bottom_east"])
        cluster["northing"] = 0.5 * (cluster["top_north"] + cluster["bottom_north"])
        cluster["elevation"] = 0.5 * (
            cluster["top_elevation"] + cluster["bottom_elevation"]
        )
    ax.plot(
        [
            v["easting"]
            for v in pump_wells[stage["well"]]["Stage " + stage["stage"]].values()
        ],
        [
            v["northing"]
            for v in pump_wells[stage["well"]]["Stage " + stage["stage"]].values()
        ],
        marker=(7, 1, 0),
        c="r",
        ms=16,
        markeredgecolor="k",
        linewidth=0.25,
        zorder=2,
    )
    for before_stage in sorted_stages[:i_stage]:
        ax.plot(
            [
                v["easting"]
                for v in pump_wells[before_stage["well"]][
                    "Stage " + before_stage["stage"]
                ].values()
            ],
            [
                v["northing"]
                for v in pump_wells[before_stage["well"]][
                    "Stage " + before_stage["stage"]
                ].values()
            ],
            marker=(7, 1, 0),
            c="0.7",
            ms=6,
            markeredgecolor="k",
            linewidth=0.25,
            zorder=2,
        )

    vertexes, volume_tags, volume_new = output_isosurface(
        cumulative_event_density,
        10**vmin,
        grid_points,
        event_density_dir + out_tag + "_isosurfaces.csv",
    )
    volume_new = (
        len(np.where(cumulative_event_density > 10**vmin)[0]) * grid_spacing**3
    )
    duv_volume_new = (
        len(np.where(cumulative_depth_interpolate * in_duvernay > 10**vmin)[0])
        * grid_spacing**3
        / depth_interp_factor
    )
    ax.text(
        0.6,
        0.9,
        f"Well {stage['well']}: Stage {stage['stage']}",
        fontsize=20,
        ha="left",
        transform=ax.transAxes,
    )
    ax.text(
        0.6,
        0.85,
        datetime.strftime(local_end_time, "%b %d, %Y, %H:%M:%S"),
        fontsize=20,
        ha="left",
        transform=ax.transAxes,
    )
    ax.text(
        0.6,
        0.8,
        f"Total Volume is {volume_new/10**9: .2f} X10$^9$ m$^3$",
        fontsize=20,
        ha="left",
        transform=ax.transAxes,
    )
    ax.text(
        0.6,
        0.75,
        f"Duvernay Volume is {duv_volume_new/10**9: .2f} X10$^9$ m$^3$",
        fontsize=20,
        ha="left",
        transform=ax.transAxes,
    )
    ax.set_xlim([x1, x2])
    ax.set_ylim([y1, y2])
    ax = gray_background_with_grid(ax)
    cbaxes = fig.add_axes([0.2, 0.18, 0.6, 0.03])
    cb = plt.colorbar(contours, cax=cbaxes, orientation="horizontal")
    ticks = cb.get_ticks()
    cb.set_ticklabels([f"{10**tick:.1e}".replace("-", "$-$") for tick in ticks])
    cb.ax.tick_params(labelsize=16, rotation=0)
    cb.set_label("events per cubic meter", fontsize=20)

    fig.savefig(
        event_density_dir + f"//figures//{out_tag}.png",
        dpi=150,
        bbox_inches="tight",
    )
    g.write(f"{i_stage+1},{stage['well']},{stage['stage']}")
    g.write(f",{volume_new:.4e}, {volume_new - volume_old: .4e},")
    g.write(f",{duv_volume_new:.4e}, {duv_volume_new - duv_volume_old: .4e}\n")
    volume_old = volume_new
    duv_volume_old = duv_volume_new
    plt.close(fig)
g.close()
