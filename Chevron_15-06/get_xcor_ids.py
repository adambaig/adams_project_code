import pytz
from datetime import datetime
from read_inputs import read_isxcor, read_catalog

utc = pytz.utc

is_xcor = read_isxcor()
events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)

len(events)
len(is_xcor)

for event in events.values():
    try:
        event["is_xcor"] = is_xcor[event["id"]]
    except:
        pass


event["timestamp"]

window_events = {
    k: v
    for k, v in events.items()
    if v["timestamp"] > utc.localize(datetime(2019, 12, 11))
    and v["timestamp"] < utc.localize(datetime(2019, 12, 13))
}

len(window_events)


len([v for v in window_events.values() if "is_xcor" in v])

is_xcor[event["id"][1:]]
events
is_xcor

g = open("xcor_events_in_window.csv", "w")
g.write("# id,date time, easting, northing, elevation, is_xcor\n")
for event in window_events.values():
    g.write(f"{event['id']},")
    g.write(datetime.strftime(event["timestamp"], "%y-%m-%d %H:%M:%S.%f,"))
    g.write(
        f"{event['easting']:.1f}, {event['northing']:.1f},"
        f"{event['elevation']:.1f}, {event['is_xcor']}\n"
    )
g.close()
