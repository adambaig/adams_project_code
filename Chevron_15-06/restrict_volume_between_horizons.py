import glob
import matplotlib.pyplot as plt
import numpy as np
import os
from scipy.ndimage import zoom
from scipy.interpolate import RectBivariateSpline
from read_inputs import read_strain, read_tops

PI = np.pi
# strain_flat,grid_points = read_strain('strain_complete//fields//all_wells.csv')

tops = read_tops()
depth_interp_factor = 10

ireton = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["H_V_Ireton"])
duvernay = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Duvernay"])
majeau = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Majeau_Lake"])
beaverhill = RectBivariateSpline(
    tops["e_grid"], tops["n_grid"], tops["Beaverhill_Lake"]
)
strain_flat, grid_points = read_strain("event_density//fields//all.csv")
e_grid = np.unique(grid_points[:, 0])
n_grid = np.unique(grid_points[:, 1])
z_grid = np.unique(grid_points[:, 2])
n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)
strain = np.reshape(strain_flat, [n_e, n_n, n_z])
strain_depth_interp = zoom(strain, [1, 1, depth_interp_factor])
z_grid_interp = zoom(z_grid, depth_interp_factor)
new_grid = np.zeros([depth_interp_factor * n_e * n_n * n_z, 3])
in_duvernay = np.zeros([n_e, n_n, depth_interp_factor * n_z])

for i_e, e in enumerate(e_grid):
    for i_n, n in enumerate(n_grid):
        duvernay_top = duvernay(e, n)
        duvernay_bottom = majeau(e, n)
        for i_z, z in enumerate(z_grid_interp):
            if z > duvernay_bottom and z < duvernay_top:
                in_duvernay[i_e, i_n, i_z] = 1

directory = {
    "strain": "strain_complete//fields//",
    "density": "event_density//fields//",
}
vmin = {"strain": 10 ** (-8.8), "density": 3 / 4 / PI / 80 / 80 / 80}
cubic_grid_size = 40 * 40 * 40 / depth_interp_factor

f = open("well_and_combo_volumes", "w")
f.write("dataset,method,total volume, duvernay volume\n")
for field in ["strain", "density"]:
    for field_file in [
        *glob.glob(f"{directory[field]}well_?.csv"),
        *glob.glob(f"{directory[field]}combo_??.csv"),
    ]:
        strain_flat, grid_points = read_strain(field_file)
        dataset = os.path.split(field_file)[1].split(".")[0].replace("_", " ")
        strain = np.reshape(strain_flat, [n_e, n_n, n_z])
        strain_depth_interp = zoom(strain, [1, 1, depth_interp_factor])
        f.write(f"{dataset},{field},")
        total_vol = (
            len(np.where(strain_depth_interp > vmin[field])[0]) * cubic_grid_size
        )
        duvernay_vol = (
            len(np.where(in_duvernay * strain_depth_interp > vmin[field])[0])
            * cubic_grid_size
        )
        f.write(f"{total_vol:.4e},{duvernay_vol:.4e}\n")
f.close()
