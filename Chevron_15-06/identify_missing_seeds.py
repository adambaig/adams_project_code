from datetime import datetime, timedelta
import glob
import numpy as np
import os

from read_inputs import read_catalog

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)

missing_ids = [
    os.path.basename(d)
    for d in glob.glob("C:\\Users\\adambaig\\Project\\Chevron_15-06\\empty\\*")
]

datetimes = [
    event_id for event_id, event in events.items() if event["id"] in missing_ids
]

start_interval = datetime(2019, 12, 10, 0, 0, 36)
start_interval + timedelta(0, 600)

file_starts = [start_interval + timedelta(0, i * 600) for i in range(4 * 144)]
file_ends = [start_interval + timedelta(0, 599.998 + i * 600) for i in range(4 * 144)]


timestamp

seednames = []
for datetime_code in datetimes:
    seed_ranges = zip(file_starts, file_ends)
    timestamp = datetime.strptime(datetime_code, "%Y%m%d%H%M%S.%f")
    for i, (start, end) in enumerate(seed_ranges):
        if timestamp > start and timestamp < end:
            start_code = datetime.strftime(start, "%Y%m%d.%H%M%S.%f")
            end_code = datetime.strftime(end, "%Y%m%d.%H%M%S.%f")
            seednames.append(f"{start_code}_{end_code}.seed")


unique_seeds = np.unique(seednames)
for seed in unique_seeds:
    print(seed)
