from datetime import datetime
import json
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
from obspy.imaging.mopad_wrapper import beach
import random

from read_inputs import read_wells, read_catalog
from plotting_stuff import gray_background_with_grid

events = read_catalog(
    "Catalogs/Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
wells = read_wells()
tab10 = cm.get_cmap("tab10")
well_color = {
    "1": tab10(0.05),
    "2": tab10(0.15),
    "3": tab10(0.25),
    "4": tab10(0.35),
    "5": tab10(0.45),
    "6": tab10(0.55),
    "A": tab10(0.65),
    "B": tab10(0.75),
    "C": tab10(0.85),
    "D": tab10(0.95),
    "-1": "k",
}

mt_events = {k: v for k, v in events.items() if v["mt confidence"] > 0.95}

shuffle_order = list(enumerate(mt_events))
random.shuffle(shuffle_order)
_, shuffled_events = zip(*shuffle_order)


for event in mt_events.values():
    if event["treatment_code"] == "PTEST":
        event["color"] = well_color["C"]
    elif event["treatment_code"] == "IS":
        event["color"] = well_color["B"]
    else:
        event["color"] = well_color[event["well"]]


fig, ax = plt.subplots(figsize=[16, 16])
ax.set_aspect("equal")
for well in [v for k, v in wells.items() if k in ["1", "2", "3", "4", "5", "6"]]:
    ax.plot(well["easting"], well["northing"], "k")

for event_id in shuffled_events:
    event = events[event_id]
    beach_ball = beach(
        (event["strike"], event["dip"], event["rake"]),
        xy=(event["easting"], event["northing"]),
        width=50,
        facecolor=event["color"],
        linewidth=0.25,
    )
    ax.add_collection(beach_ball)

gray_background_with_grid(ax)
fig.savefig("high_quality_beachballs_no_parents.png", bbox_inches="tight")


fig_leg, ax_leg = plt.subplots()
patches = []
for ii in ["1", "2", "3", "4", "5", "6", "-1", "C", "B"]:
    p = ax.plot(0, 0, "o", color=well_color[ii])
    patches.append(p[0])
ax_leg.legend(
    patches,
    [
        "Well 1",
        "Well 2",
        "Well 3",
        "Well 4",
        "Well 5",
        "Well 6",
        "Induced Feature",
        "Nov 27 Pressure Test",
        "Other Induced Seismicity",
    ],
)
ax_leg.axis("off")
fig_leg.savefig("figures/legend.png", bbox_inches="tight")
