import matplotlib.pyplot as plt
import numpy as np
from read_inputs import read_catalog

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
list(events.values())[0]
r_values = [v["dc R"] for v in events.values()]
r_bins = np.linspace(0, 1, 51)
fig, ax = plt.subplots()


ax.hist(r_values, bins=r_bins, edgecolor="0.2")
ax.set_xlabel("Pearson R")
ax.set_ylabel("count")


fig.savefig("overall_R_for_events.png")
