# import matplotlib
#
# matplotlib.use("Qt5agg")
from copy import deepcopy
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import pytz

from pfi_qi.QI_analysis import (
    parent_child,
    rt_diffusivity,
    fracture_dimensions,
    strain_grid,
    output_isosurface,
    rotate_all_events_to_well_coords,
)
from pfi_qi.engineering import (
    sorted_stage_list,
    breaktimes_by_stage,
    find_perf_center,
    calc_well_trend,
    make_unique_stage_list,
)
from pfi_qi.rotations import rotate_from_cardinal
from sms_moment_tensor.MT_math import decompose_MT, clvd_iso_dc, mt_to_sdr

from read_inputs import (
    read_catalog,
    get_velocity_model,
    read_wells,
    read_treatment,
    read_or_load_json,
    read_diverter_drops,
    read_parent_stages_2016,
    classify_treatment,
    classify_events_by_treatment,
    write_catalog,
)
from plotting_stuff import (
    rt_with_treatment,
    plot_rotated_stage,
    plot_stage,
    add_dimension_arrows_abs_coords,
    gray_background_with_grid,
)

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")

# read in all well and treatment da
treatment = read_or_load_json(read_treatment, "treatment_data.json")
sorted_stages = read_or_load_json(
    sorted_stage_list, "sorted_stage_list.json", treatment
)
break_times = read_or_load_json(
    breaktimes_by_stage, "break_times.json", sorted_stages, treatment
)
velocity_model = get_velocity_model()
events = read_catalog("Catalogs//Chevron_15_06_Apr30_2020_updatedEventDescriptions.csv")
wells = read_wells()
diverter_drops = read_diverter_drops()

magnitude_of_completeness = -1.25


# This block is dealing with the split stage.

split_stage = {
    k: v for k, v in events.items() if v["well"] == "1" and v["stage"] == "05"
}
for event_id, event in split_stage.items():
    if event["timestamp"] < utc.localize(datetime(2019, 12, 9, 9, 0)):
        event["stage"] = "05.0"
    else:
        event["stage"] = "05.5"
pump_wells = deepcopy(wells)
pump_wells["1"]["Stage 05.0"] = pump_wells["1"]["Stage 05"]
pump_wells["1"]["Stage 05.5"] = pump_wells["1"]["Stage 05"]
pump_wells["1"].pop("Stage 05")

unique_stages = make_unique_stage_list(sorted_stages)
# find frac events (still may have some IS), calculate diffusivity
diverter_stages = np.unique([v["stage"] for v in diverter_drops.values()])
diverter_stage_events = {
    k: v
    for k, v in events.items()
    if v["well"] == "1" and v["stage"] in diverter_stages
}

diverter_events_complete = {
    k: v
    for k, v in diverter_stage_events.items()
    if v["treatment_code"] not in ["PRE", "POST", "IS"]
    and v["magnitude"] > magnitude_of_completeness
}

divert0 = {k: v for k, v in diverter_events_complete.items() if v["n_diverters"] == 0}
divert1 = {k: v for k, v in diverter_events_complete.items() if v["n_diverters"] == 1}
divert2 = {k: v for k, v in diverter_events_complete.items() if v["n_diverters"] == 2}

divert0_dimensions = fracture_dimensions(
    divert0,
    pump_wells,
    csv_out="divert0_dimensions.csv",
    unique_stages=unique_stages,
    return_arrows_too=True,
)
divert1_dimensions = fracture_dimensions(
    divert1,
    pump_wells,
    csv_out="divert1_dimensions.csv",
    unique_stages=unique_stages,
    return_arrows_too=True,
)
divert2_dimensions = fracture_dimensions(
    divert2,
    pump_wells,
    csv_out="divert2_dimensions.csv",
    unique_stages=unique_stages,
    return_arrows_too=True,
)

stage_events = {
    k: v for k, v in events.items() if v["well"] == "1" and v["stage"] == "6"
}


fig, ax = plt.subplots()
ax.set_aspect("equal")
ax.plot(
    [v["easting"] for v in stage_events.values()],
    [v["northing"] for v in stage_events.values()],
    "o",
)
perf_center = find_perf_center("25", wells["1"])
ax.plot(perf_center["easting"], perf_center["northing"], "ro")
for perf, cluster in wells["1"]["Stage 25"].items():
    cluster["easting"] = 0.5 * (cluster["top_east"] + cluster["bottom_east"])
    cluster["northing"] = 0.5 * (cluster["top_north"] + cluster["bottom_north"])
    cluster["elevation"] = 0.5 * (
        cluster["top_elevation"] + cluster["bottom_elevation"]
    )
ax.plot(
    [v["easting"] for v in wells["1"]["Stage 25"].values()],
    [v["northing"] for v in wells["1"]["Stage 25"].values()],
    marker=(7, 1, 0),
    c="y",
    ms=16,
    markeredgecolor="k",
    linewidth=0.25,
    zorder=2,
)
for stage in diverter_stages:
    n_diverter = 0
    old_events = {}
    total_drops = len([v for v in diverter_drops.values() if v["stage"] == stage])
    well_stage_identifier = f"Well 1: Stage {stage}"
    stage_events = {
        k: v for k, v in diverter_events_complete.items() if v["stage"] == stage
    }
    fig_junk = plot_stage(stage_events, pump_wells)
    ax_junk = fig_junk.axes[0]
    x1, x2 = ax_junk.get_xlim()
    y1, y2 = ax_junk.get_ylim()
    plt.close(fig_junk)
    for drops in range(total_drops + 1):
        stage_div_events = {
            k: v for k, v in stage_events.items() if v["n_diverters"] == n_diverter
        }
        if stage_div_events:
            dimension = fracture_dimensions(
                stage_div_events, pump_wells, return_arrows_too=True
            )
        for drops in range(total_drops + 1):
            if stage_div_events:
                fig_en = plot_stage(
                    stage_div_events,
                    pump_wells,
                    limits={"x1": x1, "x2": x2, "y1": y1, "y2": y2},
                )
                if dimension:
                    ax = add_dimension_arrows_abs_coords(
                        fig_en.axes[0], dimension[well_stage_identifier]["arrows"]
                    )
                if old_events:
                    fig_en.axes[0].plot(
                        [v["easting"] for v in old_events.values()],
                        [v["northing"] for v in old_events.values()],
                        "wo",
                        markeredgecolor="0.3",
                        zorder=-2,
                    )
                fig_en.savefig(
                    f"figures//Diverter_dimensions//en_w1_s{stage}_n{n_diverter}.png"
                )
                plt.close(fig_en)
        n_diverter += 1
        old_events.update(stage_div_events)
