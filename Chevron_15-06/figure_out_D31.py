import matplotlib

matplotlib.use("Qt5agg")

from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import numpy as np

from read_inputs import read_wells
from pfi_qi.engineering import find_perf_center

wellD = read_wells()["D"]


wellD


def serial_to_datetime(serial_time):
    return datetime(1970, 1, 1) + timedelta(0, float(serial_time))


def datetime_to_serial(datetime):
    return ()


lspl

well_md = wellD["measured_depth"]
well_easting = wellD["easting"]
well_northing = wellD["northing"]
well_elevation = wellD["elevation"]
np.interp(6252, well_md, well_easting)


wellD["Stage 01"]
f = open("wells//completion_perf_mds_from_2016.csv")
head = f.readline()
lines = f.readlines()
f.close()
stage = 1
stage_name = "Stage 01"
i_cluster = 1
test_perfs = {stage_name: {}}
for line in lines[16:191]:
    lspl = line.split(",")
    if lspl[4] == "TREATMENT":
        i_cluster = 1
        stage += 1
        stage_name = "Stage " + str(stage).zfill(2)
        test_perfs[stage_name] = {}
    else:
        cluster = f"cluster {i_cluster}"
        top_md = float(lspl[6])
        bottom_md = float(lspl[8])
        top_east = np.interp(top_md, well_md, well_easting)
        top_north = np.interp(top_md, well_md, well_northing)
        top_elevation = np.interp(top_md, well_md, well_elevation)
        bottom_east = np.interp(bottom_md, well_md, well_easting)
        bottom_north = np.interp(bottom_md, well_md, well_northing)
        bottom_elevation = np.interp(bottom_md, well_md, well_elevation)
        test_perfs[stage_name][cluster] = {
            "top_east": top_east,
            "bottom_east": bottom_east,
            "top_north": top_north,
            "bottom_north": bottom_north,
            "top_elevation": top_elevation,
            "bottom_elevation": bottom_elevation,
        }
        i_cluster += 1


g = open("wells//well_D_perf_locations.csv", "w")
g.write("#Well Name,Stage,Time Stamp,UTM X,UTM Y,Depth\n")


for stage in [k for k in wellD.keys() if "tage" in k]:
    perf_center = find_perf_center(stage.split()[1], wellD)
    ax.plot(perf_center["easting"], perf_center["northing"], marker=(7, 1, 0))

for stage in test_perfs.keys():
    perf_center = find_perf_center(stage.split()[1], test_perfs)
    ax.plot(perf_center["easting"], perf_center["northing"], "o")


for md in [6252, 6232.4, 6212.8, 6193.2, 6173.6]:
    int_east = np.interp(md, well_md, well_easting)
    int_north = np.interp(md, well_md, well_northing)
    int_depth = -np.interp(md, well_md, well_elevation)
    print(f"{int_east:.3f}, {int_north:.3f}, {int_depth:.3f}")


serial_to_datetime(1482185700)
