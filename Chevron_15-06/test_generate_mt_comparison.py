import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import re

from sms_moment_tensor.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import mt_to_sdr
from sms_ray_modelling.raytrace import isotropic_ray_trace
from obspy.imaging.mopad_wrapper import beach

from plotting_stuff import gray_background_with_grid
from read_inputs import read_catalog, read_stations

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
stations = read_stations()

f = open("mt-qc//refixed_catalog_of_first_motions.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
amp_qc = {}
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[1:]:
        amp_qc[lspl[0]] = {k: v for v, k in zip(lspl[1:], headsplit[1:])}

theoretical_polarities = {}
f = open("mt-qc\\theoretical_polarities_redux.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[:1]:
        theoretical_polarities[lspl[0]] = {
            k: float(v) for v, k in zip(lspl[1:], headsplit[1:])
        }


def getVelfromTTT(TTT):
    velocity_model = []
    top = -1 * TTT["originz"]
    d1 = TTT["dz"]
    vel = TTT["vel1D"]
    for vp in vel:
        vs = vp / 1.73205080757
        rho = 310 * (vp**0.25)
        curlayer = {"vp": vp, "top": top, "vs": vs, "rho": rho}
        top = top - d1
        velocity_model.append(curlayer)
    return velocity_model


good_amps
pickle_TTT = open("optimal-TTT.pkl", "rb")
TTT = pickle.load(pickle_TTT, encoding="latin1")
pickle_TTT.close()
velocity_model = getVelfromTTT(TTT)
theoretical_polarities[event_id]

sign_correct = {"True": 1, "False": -1}
for event_id, event in {
    event["id"]: event for event in events.values() if event["magnitude"] > 0.7
}.items():
    print(event_id)
    source = {"e": event["easting"], "n": event["northing"], "z": event["elevation"]}
    if (
        os.path.isfile(f"mt-qc//amplitude_picks//{event_id}.csv")
        and event_id in amp_qc
        and event_id in theoretical_polarities
    ):
        f = open(f"mt-qc//amplitude_picks//{event_id}.csv")
        amps = {k: float(v) for k, v in [line.split(",") for line in f.readlines()[1:]]}
        f.close()
        good_amps = {k: v for k, v in amp_qc[event_id].items() if "None" not in v}
        if len(good_amps) > 6:
            right_hand_side = np.zeros(len(good_amps))
            A_matrix = np.zeros([len(good_amps), 6])
            for i_station, (station, correct_polarity) in enumerate(good_amps.items()):
                p_raypath = isotropic_ray_trace(
                    source, stations[station], velocity_model, "P"
                )
                s_raypath = isotropic_ray_trace(
                    source, stations[station], velocity_model, "S"
                )
                cos_incoming = np.sqrt(
                    1.0
                    - (
                        p_raypath["hrz_slowness"]["e"] ** 2
                        + p_raypath["hrz_slowness"]["n"] ** 2
                    )
                    * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
                )
                right_hand_side[i_station] = (
                    back_project_amplitude(
                        p_raypath,
                        s_raypath,
                        sign_correct[good_amps[station]]
                        * abs(amps[station])
                        * theoretical_polarities[event_id][station],
                        "P",
                        Q=60,
                    )
                    / cos_incoming
                )
                A_matrix[i_station, :] = inversion_matrix_row(p_raypath, "P")
            mt_dictionary = solve_moment_tensor(A_matrix, right_hand_side)
            mt_dc = mt_dictionary["dc"]
            dc_mt_strike, dc_mt_dip, dc_mt_rake = mt_to_sdr(
                mt_dictionary["dc"], conjugate=False
            )
    fig_pl, ax_pl = plt.subplots(figsize=[9, 9])

    ax_pl.set_aspect("equal")
    for station_id, station in {
        k: v for k, v in stations.items() if bool(re.search("B...4", k))
    }.items():
        try:
            if (
                sign_correct[good_amps[station_id]]
                * theoretical_polarities[event_id][station_id]
                == 0
            ):
                ax_pl.plot(
                    station["e"], station["n"], "o", color="0.9", markeredgecolor="0.2"
                )
            elif (
                sign_correct[good_amps[station_id]]
                * theoretical_polarities[event_id][station_id]
                > 0
            ):
                ax_pl.plot(
                    station["e"],
                    station["n"],
                    "o",
                    color="firebrick",
                    markeredgecolor="0.2",
                )
            else:
                ax_pl.plot(
                    station["e"],
                    station["n"],
                    "o",
                    color="royalblue",
                    markeredgecolor="0.2",
                )
            ax_pl.text(station["e"], station["n"] - 270, station_id, ha="center")
        except:
            pass
    beachball = beach(
        (dc_mt_strike, dc_mt_dip, dc_mt_rake),
        xy=(event["easting"], event["northing"]),
        width=350,
        facecolor="firebrick",
        linewidth=0.25,
    )
    ax_pl.add_collection(beachball)
    ax_pl = gray_background_with_grid(ax_pl, grid_spacing=1000)
