from copy import deepcopy
from datetime import datetime, timedelta
from matplotlib import cm, colors
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import os
import pytz
from scipy.ndimage import zoom

from pfi_qi.QI_analysis import (
    strain_grid,
    output_isosurface,
)
from pfi_qi.engineering import sorted_stage_list
from read_inputs import (
    read_catalog,
    read_treatment,
    get_velocity_model,
    read_wells,
    read_strain,
    read_or_load_json,
)
from plotting_stuff import gray_background_with_grid

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")

method = "strain"

velocity_model = get_velocity_model()
events = read_catalog("Catalogs/events_with_treatment_tags_before_MT_flip.csv")
wells = read_wells()
treatment = read_or_load_json(read_treatment, "treatment_data.json")
sorted_stages = read_or_load_json(
    sorted_stage_list, "sorted_stage_list.json", treatment
)

# This block is dealing with the split stage.

split_stage = {
    k: v for k, v in events.items() if v["well"] == "1" and v["stage"] == "05"
}
for event_id, event in split_stage.items():
    if event["timestamp"] < utc.localize(datetime(2019, 12, 9, 9, 0)):
        event["stage"] = "05.0"
    else:
        event["stage"] = "05.5"
pump_wells = deepcopy(wells)
pump_wells["1"]["Stage 05.0"] = pump_wells["1"]["Stage 05"]
pump_wells["1"]["Stage 05.5"] = pump_wells["1"]["Stage 05"]
pump_wells["1"].pop("Stage 05")


events_no_is = {
    k: v
    for k, v in events.items()
    if v["treatment_code"].strip() not in ["IS", "PRE", "POST"]
}

strain_dir = "strain"

grid_spacing = 40
min_neighbours = 5
max_radius = 80
all_well_strain_file = f"{strain_dir}//all_wells.csv"
if os.path.exists(all_well_strain_file):
    strain_flat, grid_points = read_strain(all_well_strain_file)
else:
    strain, grid_points = strain_grid(
        events_no_is,
        grid_spacing,
        min_neighbours,
        max_radius,
        "strain//all_wells.csv",
        velocity_model,
    )
e_grid = np.unique(grid_points[:, 0])
n_grid = np.unique(grid_points[:, 1])
z_grid = np.unique(grid_points[:, 2])
n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)
if "strain_flat" in globals():
    strain = np.reshape(strain_flat, [n_e, n_n, n_z])

vertexes, volume_tags, volume = output_isosurface(
    strain, 1e-9, grid_points, f"{strain_dir}//isosurface.csv"
)


vmin = -9
vmax = np.percentile(np.log10(non_zero_strain), 99.9)
east_grid = np.unique(grid_points[:, 0])
north_grid = np.unique(grid_points[:, 1])
elevation_grid = np.unique(grid_points[:, 2])
levels = np.linspace(vmin, vmax, 9)
color_scale = cm.ScalarMappable(
    norm=colors.Normalize(vmin=vmin, vmax=vmax), cmap="inferno_r"
)
fig, ax = plt.subplots(figsize=[16, 16])
strain_interpolate = zoom(strain, 4)


ax.set_aspect("equal")
contour_colors = []
for level in levels:
    contour_colors.append(color_scale.to_rgba(level))

contours = ax.contourf(
    zoom(east_grid, 4),
    zoom(north_grid, 4),
    np.log10(strain_interpolate[:, :, 68]).T,
    vmin=vmin,
    levels=levels,
    vmax=vmax,
    colors=contour_colors,
    extend="max",
)
for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k")

ax = gray_background_with_grid(ax)
vertexes, volume_tags, volume_all = output_isosurface(
    strain,
    10**vmin,
    grid_points,
    strain_dir + "all_isosurfaces.csv",
)
cbaxes = fig.add_axes([0.2, 0.18, 0.6, 0.03])
cb = plt.colorbar(contours, cax=cbaxes, orientation="horizontal")
ticks = cb.get_ticks()
cb.set_ticklabels([f"{10**tick:.1e}".replace("-", "$-$") for tick in ticks])
cb.set_label("strain")

x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()
fig.savefig(strain_dir + "//figures//all_fig1.png", dpi=150)

g = open(strain_dir + "cumulative_volume.csv", "w")
g.write("unique stage id, well, stage, volume, differential\n")
g.write(",,,(m3),(m3)\n")
volume_old = 0
for i_stage, stage in enumerate(sorted_stages):
    end_time = utc.localize(
        datetime.strptime(str(stage["end_time"]).split(".")[0], "%Y%m%d%H%M%S")
    )
    local_end_time = end_time.astimezone(timezone)
    cumulative_events = {
        k: v for k, v in events_no_is.items() if v["timestamp"] < end_time
    }
    out_tag = f'after_{str(i_stage).zfill(3)}_Well{stage["well"]}_Stage{stage["stage"]}'
    cumulative_strain, _ = strain_grid(
        cumulative_events,
        grid_points,
        min_neighbours,
        max_radius,
        f"{strain_dir}//{out_tag}.csv",
        velocity_model,
    )
    fig, ax = plt.subplots(figsize=[16, 16])
    ax.set_aspect("equal")

    contour_colors = []
    for level in levels:
        contour_colors.append(color_scale.to_rgba(level))

    cumulative_strain_interp = zoom(cumulative_strain, 4)

    ax.contourf(
        zoom(east_grid, 4),
        zoom(north_grid, 4),
        np.log10(cumulative_strain_interp[:, :, 68]).T,
        vmin=vmin,
        levels=levels,
        vmax=vmax,
        colors=contour_colors,
        extend="max",
    )
    for well in pump_wells.values():
        ax.plot(well["easting"], well["northing"], "k")
    for perf, cluster in pump_wells[stage["well"]]["Stage " + stage["stage"]].items():
        cluster["easting"] = 0.5 * (cluster["top_east"] + cluster["bottom_east"])
        cluster["northing"] = 0.5 * (cluster["top_north"] + cluster["bottom_north"])
        cluster["elevation"] = 0.5 * (
            cluster["top_elevation"] + cluster["bottom_elevation"]
        )
    ax.plot(
        [
            v["easting"]
            for v in pump_wells[stage["well"]]["Stage " + stage["stage"]].values()
        ],
        [
            v["northing"]
            for v in pump_wells[stage["well"]]["Stage " + stage["stage"]].values()
        ],
        marker=(7, 1, 0),
        c="r",
        ms=16,
        markeredgecolor="k",
        linewidth=0.25,
        zorder=2,
    )
    for before_stage in sorted_stages[:i_stage]:
        ax.plot(
            [
                v["easting"]
                for v in pump_wells[before_stage["well"]][
                    "Stage " + before_stage["stage"]
                ].values()
            ],
            [
                v["northing"]
                for v in pump_wells[before_stage["well"]][
                    "Stage " + before_stage["stage"]
                ].values()
            ],
            marker=(7, 1, 0),
            c="0.7",
            ms=6,
            markeredgecolor="k",
            linewidth=0.25,
            zorder=2,
        )

    vertexes, volume_tags, volume_new = output_isosurface(
        cumulative_strain,
        10**vmin,
        grid_points,
        f"{strain_dir}//{out_tag}_isosurfaces.csv",
    )
    volume_new = len(np.where(cumulative_strain > 10**vmin)[0]) * grid_spacing**3

    ax.text(
        0.7,
        0.9,
        f"Well {stage['well']}: Stage {stage['stage']}",
        fontsize=20,
        ha="left",
        transform=ax.transAxes,
    )
    ax.text(
        0.7,
        0.85,
        datetime.strftime(local_end_time, "%b %d, %Y, %H:%M:%S"),
        fontsize=20,
        ha="left",
        transform=ax.transAxes,
    )
    ax.text(
        0.7,
        0.8,
        f"volume is {volume_new/10**9: .2f} X10$^9$ m$^3$",
        fontsize=20,
        ha="left",
        transform=ax.transAxes,
    )
    ax.set_xlim([x1, x2])
    ax.set_ylim([y1, y2])
    ax = gray_background_with_grid(ax)
    cbaxes = fig.add_axes([0.2, 0.18, 0.6, 0.03])
    cb = plt.colorbar(contours, cax=cbaxes, orientation="horizontal")
    ticks = cb.get_ticks()
    cb.set_ticklabels([f"{10**tick:.1e}".replace("-", "$-$") for tick in ticks])
    cb.set_label("strain")

    fig.savefig(strain_dir + f"//figures//{out_tag}.png", dpi=150)
    g.write(
        f"{i_stage+1}, {stage['well']}, {stage['stage']}, {volume_new:.4e}, {volume_new - volume_old: .4e}\n"
    )
    volume_old = volume_new
    plt.close(fig)
g.close()
