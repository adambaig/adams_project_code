import matplotlib.pyplot as plt
import numpy as np

from read_inputs import get_velocity_model, read_stations, read_wells


stations = read_stations()

source = {
    "e": np.average([v["e"] for v in stations.values()]),
    "n": np.average([v["n"] for v in stations.values()]),
    "z": -2465,
}


f = open("shifted_stations.csv", "w")
f.write("station,northing, easting, elevation\n")
for station_id, station in stations.items():
    f.write(f"{station_id},{station['e'] - source['e']:.1f},")
    f.write(f"{station['n'] - source['n']:.1f},{station['z']:.1f}\n")
f.close()
