from read_inputs import get_velocity_model

velocity_model = get_velocity_model()
ground_elevation = 980

velocity_model

model_crh = []
for layer in velocity_model:
    if "top" in layer:
        depth_km = (ground_elevation - layer["top"]) / 1000
    else:
        depth_km = 0
    model_crh.append([layer["vp"] / 1000, depth_km])


with open("model.crh", "w") as f:
    f.write("Adam's fantastic Chevron velocity model\n")
    for layer in model_crh:
        f.write(f"{layer[0]:.3f}  {layer[1]:.3f}\n")
