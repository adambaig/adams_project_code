import colorsys
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np

from read_inputs import read_wells, read_diverter_drops

wells = read_wells()
diverter_drops = read_diverter_drops()

well1 = wells['1']

stages, kgs = np.array([(float(v['stage']),v['amount_kg']) for v in diverter_drops.values()]).T
for i_stage,stage in enumerate(stages):
    if stage in stages[:i_stage]:
        stages[i_stage]+=0.5

f = open('diverter_statistical_significance.csv')
head = f.readline()
lines = f.readlines()
f.close()

stage_stat_sig = []
p_value = []
up_well_shift = []
for line in lines:
    lspl = line.split(',')
    if lspl[1]=="pre-diverter and diverter 1":
        stage_stat_sig.append(float(lspl[0])+0.25)
    elif lspl[1]=="diverter1 and diverter 2":
        stage_stat_sig.append(float(lspl[0])+0.5)
    elif lspl[1]=="pre-diverter and diverter 2":
        stage_stat_sig.append(float(lspl[0])+0.75)
    p_value.append(float(lspl[5]))
    up_well_shift.append(float(lspl[6]))

stage_stat_sig = np.array(stage_stat_sig)
p_value = np.array(p_value)
up_well_shift = np.array(up_well_shift)

fig,(ax1,ax2) = plt.subplots(2,figsize=[12,6],sharex=True)
ax1.bar(stages+0.05, kgs, width=0.4,align='edge',edgecolor='0.2')
ax1.set_xlim([0.5,30.5])
stage_box = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,18,19,20,21,22,23,24,25,26]
stage_ticks = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,18,19,20,21,22,23,24,25]
y1,y2 = ax1.get_ylim()
for stage in stage_box:
    ax1.plot([stage,stage],[y1,y2],'0.4',lw=0.5,)
ax1.set_ylim([y1,y2])
ax1.set_xticks(np.array(stage_ticks)+0.5)
ax1.set_xticklabels(stage_ticks)

for stg,pvl,upw in zip(stage_stat_sig, p_value, up_well_shift):
    if stg%1<0.4:
        color = 'firebrick'
    elif stg%1<0.6:
        color='royalblue'
    else:
        color = 'darkgoldenrod'
    alpha = 0.2 + 0.8*(1-pvl)
    ax2.arrow(stg,0,0,upw,alpha=alpha,fc=color,ec=color,lw=4)

for stage in stage_box:
    ax2.plot([stage,stage],[-200,100],'0.4',lw=0.5,)
ax2.plot([0,31],[0,0],'k',zorder=2)
ax2.set_xlim([0.5,30.5])
ax2.set_ylim([-200,100])
ax2.set_xticks(np.array(stage_ticks)+0.5)
ax2.set_xticklabels(stage_ticks)

ax2.set_xlabel('Stage on Well 1 with Diverter')
ax1.set_ylabel('Diverter Amount (kg)')
ax2.set_ylabel('Upwell Shift (m)')
fig.savefig('Diverter_Shifts.png')

def make_simple_gradient_cmap(start_color,end_color):
    red_1, green_1, blue_1 = mcolors.to_rgb(start_color)
    red_2, green_2, blue_2 = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, red_1, red_1), (1, red_2, red_2)],
        "green": [(0.0, green_1, green_1), (1, green_2, green_2)],
        "blue": [(0.0, blue_1, blue_1), (1, blue_2, blue_2)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)

color = 'firebrick'
color =

def adjust_lightness(color, amount=0.5):
    c = colorsys.rgb_to_hls(*mcolors.to_rgb(color))
    return colorsys.hls_to_rgb(c[0], max(0, min(1, amount * c[1])), c[2])

def make_custom_gradient_cmap(color):
    red, green, blue = mcolors.to_rgb(color)
    lighter_red,lighter_green, lighter_blue = adjust_lightness(color, amount=1.8)
    cdict = {
        "red": [(0.0, red, red), (1, lighter_red, lighter_red)],
        "green": [(0.0, green, green), (1,lighter_green, lighter_green)],
        "blue": [(0.0, blue, blue), (1, lighter_blue, lighter_blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


p_d1 = make_custom_gradient_cmap('firebrick')
d1_d2 = make_custom_gradient_cmap('royalblue')
p_d2 = make_custom_gradient_cmap('darkgoldenrod')

dummy_values = np.array([np.linspace(0,1,100), np.linspace(0,1,100)])

f2,a2 = plt.subplots(figsize=[0.3,2])
a2.pcolor([0,1],dummy_values[0], dummy_values.T, cmap=p_d1)
a2.pcolor([1,2],dummy_values[0],  dummy_values.T, cmap=d1_d2)
a2.pcolor([2,3],dummy_values[0], dummy_values.T, cmap=p_d2)
a2.set_ylabel('p-value')
a2.set_xticks([])


f2.savefig('p_value_legend.png',bbox_inches='tight')
