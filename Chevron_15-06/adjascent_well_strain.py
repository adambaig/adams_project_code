from copy import deepcopy
from datetime import datetime, timedelta
from matplotlib import cm, colors
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import os
import pytz
from scipy.ndimage import zoom

from pfi_qi.QI_analysis import event_density_grid, strain_grid, output_isosurface

from read_inputs import (
    read_catalog,
    get_velocity_model,
    read_wells,
    read_treatment,
    read_or_load_json,
    read_strain,
)
from plotting_stuff import (
    rt_with_treatment,
    plot_rotated_stage,
    plot_stage,
    add_dimension_arrows,
    gray_background_with_grid,
)

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")
method = "strain"

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
wells = read_wells()
velocity_model = get_velocity_model()
frac_events = {k: v for k, v in events.items() if v["stage"] != "-1"}
events_no_is = {k: v for k, v in frac_events.items() if v["treatment_code"] != "IS"}
complete_events = {k: v for k, v in frac_events.items() if v["magnitude"] >= -1.25}

strain_dir = "strain_complete//fields//"

grid_spacing = 40
min_neighbours = 5
max_radius = 80
all_well_strain_file = f"{strain_dir}all_wells.csv"
strain_flat, grid_points = read_strain(all_well_strain_file)
e_grid = np.unique(grid_points[:, 0])
n_grid = np.unique(grid_points[:, 1])
z_grid = np.unique(grid_points[:, 2])
n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)
strain = np.reshape(strain_flat, [n_e, n_n, n_z])


strain_by_well = {}
volume_by_well = {}
for well in ["1", "2", "3", "4", "5", "6"]:

    well_strain_file = f"{strain_dir}well_{well}.csv"
    well_events = {k: v for k, v in complete_events.items() if v["well"] == well}
    strain_by_well[well], _ = strain_grid(
        well_events,
        grid_points,
        min_neighbours,
        max_radius,
        well_strain_file,
        velocity_model,
    )
    vmin = -8.8
    volume_by_well[well] = (
        len(np.where(strain_by_well[well] > 10**vmin)[0]) * grid_spacing**3
    )
    vertexes, volume_tags, volume_new = output_isosurface(
        strain_by_well[well],
        10**vmin,
        grid_points,
        f"{strain_dir}well_{well}_isosurfaces.csv",
    )

well_combos = [["1", "2"], ["2", "3"], ["4", "5"]]
strain_by_combo = {}
volume_by_combo = {}
for well_combo in well_combos:
    well1, well2 = well_combo
    combo_events = well_events = {
        k: v
        for k, v in complete_events.items()
        if v["well"] == well1 or v["well"] == well2
    }

    combo_strain_file = f"{strain_dir}combo_{well1}{well2}.csv"
    strain_by_combo[well1 + well2], _ = strain_grid(
        combo_events,
        grid_points,
        min_neighbours,
        max_radius,
        combo_strain_file,
        velocity_model,
    )
    volume_by_combo[well1 + well2] = (
        len(np.where(strain_by_combo[well1 + well2] > 10**vmin)[0])
        * grid_spacing**3
    )

    vertexes, volume_tags, volume_new = output_isosurface(
        strain_by_combo[well1 + well2],
        10**vmin,
        grid_points,
        f"{strain_dir}combo_{well1}{well2}_isosurfaces.csv",
    )
