from copy import deepcopy
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import matplotlib.dates as md
import matplotlib.cm as cm
import numpy as np
import pytz

from pfi_qi.QI_analysis import (
    parent_child,
    rt_diffusivity,
    fracture_dimensions,
    strain_grid,
    output_isosurface,
    rotate_all_events_to_well_coords,
)
from pfi_qi.engineering import (
    sorted_stage_list,
    breaktimes_by_stage,
    find_perf_center,
    calc_well_trend,
    make_unique_stage_list,
)
from pfi_qi.rotations import rotate_from_cardinal
from sms_moment_tensor.MT_math import decompose_MT, clvd_iso_dc, mt_to_sdr

from read_inputs import (
    read_events,
    read_catalog,
    get_velocity_model,
    read_wells,
    read_treatment,
    read_or_load_json,
    read_diverter_drops,
    read_parent_stages_2016,
    classify_treatment,
    classify_events_by_treatment,
    write_catalog,
)
from plotting_stuff import (
    rt_with_treatment,
    plot_rotated_stage,
    plot_stage,
    add_dimension_arrows_abs_coords,
    gray_background_with_grid,
)

from bvalue_plot import plotFMD_from_dict

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")

# read in all well and treatment da
events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
tab20 = cm.get_cmap("tab20")
tab20.set_over("k")
wells = read_wells()


def add_tab20_index_to_events(events):
    for event in events.values():
        if event["treatment_code"] in ["PRE", "POST", "FL", "HP", "LP"]:
            event["tab20 index"] = (
                (int(event["well"]) - 1) / 10.0
                + 0.01
                + 0.05 * (int(event["stage"]) % 2)
            )
        elif event["treatment_code"] == "FEAT1":
            event["tab20 index"] = 1.91
        elif event["treatment_code"] == "FEAT2":
            event["tab20 index"] = 1.91
        elif event["treatment_code"] == "IS":
            event["tab20 index"] = 0.71
        else:
            event["tab20 index"] = 0.81
    return None


add_tab20_index_to_events(events)

water_loading_events = {
    k: v for k, v in events.items() if abs(v["tab20 index"] - 0.81) < 0.001
}

list(events.items())[0]

fig_plan, ax = plt.subplots(figsize=[16, 16])
ax.set_aspect("equal")
for well_id, well in wells.items():
    ax.plot(well["easting"], well["northing"], "k")
    end_of_well = {
        "easting": well["easting"][-1],
        "northing": well["northing"][-1],
    }
    if well_id in ["1", "2", "3", "4", "5", "A", "B"]:
        ax.text(
            end_of_well["easting"] - 50,
            end_of_well["northing"] + 50,
            well_id,
            ha="right",
            va="bottom",
            fontsize=16,
            zorder=10,
        )
    else:
        ax.text(
            end_of_well["easting"] + 50,
            end_of_well["northing"] - 50,
            well_id,
            ha="left",
            va="top",
            fontsize=16,
            zorder=10,
        )

ax.scatter(
    [event["easting"] for event in water_loading_events.values()],
    [event["northing"] for event in water_loading_events.values()],
    c=[event["tab20 index"] for event in water_loading_events.values()],
    marker="o",
    cmap="tab20",
    zorder=4,
    vmax=1,
    vmin=0,
    edgecolor="0.3",
    linewidth=0.25,
)

ax = gray_background_with_grid(ax)


fig = plotFMD_from_dict(water_loading_events, key="magnitude")
fig_plan.savefig("figures//water_laoding_plan_view.png")
fig.savefig("figures//FMD_waterloading.png")


f_hist, a_hist = plt.subplots()
a_hist.hist(
    [v["mt confidence"] for v in water_loading_events.values()],
    bins=np.arange(0, 1.001, 0.01),
)

water_conf = np.array([v["mt confidence"] for v in water_loading_events.values()])
