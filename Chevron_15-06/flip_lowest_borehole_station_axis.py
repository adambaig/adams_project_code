from obspy import read_inventory


inv = read_inventory(r"stations\CV_Full.xml")
for network in inv:
    for station in network:
        if station.code[-1] == "4":
            for channel in station:
                channel.dip = -90
        if station.code[-1] in ["1", "2", "3"]:
            for channel in station:
                channel.dip = 90


inv.write(
    r"stations\CV_Full_correct_orientation.xml", format="stationxml", validate=True
)
