from datetime import datetime, timedelta
import glob
import json
import numpy as np
from obspy import UTCDateTime
import os
import sys
import pytz
import time
from scipy.signal import medfilt

from nmxseis.numerics.moment_tensor import MomentTensor

sys.path.append(
    "C:\\Users\\adambaig\\python_modules\\sms_pfi-quantitative-interpretation\\src"
)
from pfi_qi.engineering import pick_breakdown_time

timezone = pytz.timezone("America/Edmonton")
utc = pytz.utc
PI = np.pi

root_dir = "C:\\Users\\adambaig\\Project\\Chevron_15-06\\"
well_lookup = {
    "100/16-32-061-21W5/0": "D",
    "102/16-32-061-21W5/0": "C",
    "100/14-33-061-21W5/0": "6",
    "102/16-11-062-22W5/0": "3",
    "103/11-12-062-22W5/0": "2",
    "102/14-12-062-22W5/0": "A",
    "104/14-12-062-22W5/0": "1",
    "103/15-12-062-22W5/0": "B",
    "104/01-13-062-22W5/0": "4",
    "106/01-13-062-22W5/0": "5",
}

reverse_well_lookup = {v: k for k, v in well_lookup.items()}


def read_or_load_json(read_function, json_file, *args):
    if os.path.isfile(json_file):
        with open(json_file, "r") as read_file:
            dictionary = json.load(read_file)
        return dictionary
    dictionary = read_function(*args)
    with open(json_file, "w") as write_file:
        json.dump(dictionary, write_file)
    return dictionary


def read_stations(station_file="stations//postStackStations.csv"):
    f = open(station_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        stations[lspl[0]] = {
            "e": float(lspl[1]),
            "n": float(lspl[2]),
            "z": float(lspl[3]),
        }
    return stations


def read_treatment():
    treatment_csvs = glob.glob(
        r"C:\Users\adambaig\Project\Chevron 15-6\From Client\Completions\*\Frac - Stage Data (CSV)\*.csv"
    )
    treatment_data = {}
    for csv in treatment_csvs:
        _, well, _, stage = (
            os.path.split(csv)[1].replace("-", " ").split(".csv")[0].split()
        )
        f = open(csv)
        head = f.readline()
        lines = f.readlines()
        f.close()
        head_split = np.array(head.split(","))
        i_pressure = np.where(np.array(head_split) == "WH Press (MPa)")[0][0]
        i_slurry = np.where(np.array(head_split) == "CMB SLR Rate (m³/min)")[0][0]
        i_proppant = np.where(np.array(head_split) == "Density at Perfs (kg/m³)")[0][0]
        for line in lines:
            lspl = line.split(",")
            dt = timezone.localize(datetime.strptime(lspl[0], "%Y-%m-%d %H:%M:%S"))
            utc_timestamp = datetime.strftime(dt.astimezone(utc), "%Y%m%d%H%M%S.000000")
            treatment_data[utc_timestamp] = {}
            treatment_data[utc_timestamp]["well"] = well
            treatment_data[utc_timestamp]["stage"] = stage
            treatment_data[utc_timestamp]["pressure"] = float(lspl[i_pressure])
            treatment_data[utc_timestamp]["slurry_rate"] = float(lspl[i_slurry])
            treatment_data[utc_timestamp]["proppant_conc"] = float(lspl[i_proppant])
    return treatment_data


def check_p_breakdown(string):
    if not string:
        return None
    return float(string)


def read_frac_report():
    base_dir = root_dir + "From Client\\"
    f = open(base_dir + "Reporting - Frac Data report data - 2020-01-14 16.44.30.csv")
    topline = f.readline()
    head = f.readline()
    lines = f.readlines()
    f.close()
    report = {}
    for line in lines:
        lspl = line.split(",")
        well = lspl[2]
        if well not in report:
            report[well] = {}
            report[well]["long name"] = lspl[1]
        stage_number = lspl[5]
        if lspl[2]:
            start_local = timezone.localize(
                datetime.strptime(lspl[3], "%Y-%m-%d %H:%M")
            )
            end_local = timezone.localize(datetime.strptime(lspl[4], "%Y-%m-%d %H:%M"))
            report[well]["Stage " + stage_number] = {
                "start": datetime.strftime(
                    start_local.astimezone(utc), "%Y%m%d%H%M%S.%f"
                ),
                "end": datetime.strftime(end_local.astimezone(utc), "%Y%m%d%H%M%S.%f"),
                "top (mOTH)": float(lspl[6]),
                "bottom (mOTH)": float(lspl[7]),
                "pumping duration (min)": float(lspl[8]),
                "post-treat ISIP (bars)": float(lspl[9]),
                "frac gradient (kPa/m)": float(lspl[10]),
                "P breakdown (bars)": check_p_breakdown(lspl[11]),
                "avg treatment pressure (bars)": float(lspl[12]),
                "max treatment pressure (bars)": float(lspl[13]),
                "avg treatment rate (m3/min)": float(lspl[14]),
                "max treatment rate (m3/min)": float(lspl[15]),
                "total clean volume (m3)": float(lspl[16]),
                "total proppant (kg)": float(lspl[17]),
                "notes": lspl[18],
            }
    return report


def in_stage(eventtime, report):
    for well, stages in report.items():
        for stage, data in stages.items():
            if "Stage" in stage:
                if data["start"] < eventtime and data["end"] > eventtime:
                    return report[well]["short name"], stage
    return None, None


def serial_to_datetime(serial_time):
    return datetime(1970, 1, 1) + timedelta(0, float(serial_time))


def read_events(filename="FinalCatalog_mt_mag_stageAssign2_reformat.csv"):
    event_dir = root_dir + "Catalogs\\"
    event_file = event_dir + filename
    events = {}
    # report = read_frac_report()

    f = open(event_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        timestamp = utc.localize(datetime.strptime(lspl[1], "%Y-%m-%d %H:%M:%S.%f"))
        timestamp_id = datetime.strftime(timestamp, "%Y%m%d%H%M%S.%f")
        events[timestamp_id] = {
            "id": lspl[0],
            "timestamp": timestamp,
            "aarons_timestamp": lspl[2],
            "easting": float(lspl[3]),
            "northing": float(lspl[4]),
            "elevation": -float(lspl[5]),
            "easting_err": float(lspl[6]),
            "northing_err": float(lspl[7]),
            "elevation_err": float(lspl[8]),
            "long_well_name": lspl[9],
            "well": lspl[10].split(".")[0],
            "stage": lspl[11].split(".")[0].zfill(2),
            "unique_well_stage": int(lspl[12].split(".")[0]),
            "snr": float(lspl[13]),
            "stack_amplitude": float(lspl[14]),
            "magnitude": float(lspl[15]),
            "gen MT": MomentTensor.from_vector([float(s) for s in lspl[16:22]]).mat,
            "gen R": float(lspl[22]),
            "gen CN": float(lspl[23]),
            "dc MT": MomentTensor.from_vector([float(s) for s in lspl[24:30]]).mat,
            "dc R": float(lspl[30]),
            "dc CN": float(lspl[31]),
            "mt confidence": float(lspl[32]),
        }
    return events


def read_missing_events(filename="missing_events_merged_final.csv"):
    event_dir = root_dir + "Catalogs\\"
    event_file = event_dir + filename
    events = {}
    # report = read_frac_report()

    f = open(event_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        timestamp = utc.localize(datetime.strptime(lspl[1], "%Y-%m-%d %H:%M:%S.%f"))
        timestamp_id = datetime.strftime(timestamp, "%Y%m%d%H%M%S.%f")
        events[timestamp_id] = {
            "id": lspl[0],
            "timestamp": timestamp,
            "easting": float(lspl[2]),
            "northing": float(lspl[3]),
            "elevation": -float(lspl[4]),
            "easting_err": float(lspl[15]),
            "northing_err": float(lspl[16]),
            "elevation_err": float(lspl[17]),
            "long_well_name": lspl[9],
            "well": lspl[10].split(".")[0],
            "stage": lspl[11].split(".")[0].zfill(2),
            "unique_well_stage": int(lspl[12].split(".")[0]),
            "snr": float(lspl[5]),
            "stack_amplitude": float(lspl[18]),
            "magnitude": float(lspl[15]),
            "gen MT": MomentTensor.from_vector([float(s) for s in lspl[19:25]]).mat,
            "gen R": float(lspl[25]),
            "gen CN": float(lspl[26]),
            "dc MT": MomentTensor.from_vector([float(s) for s in lspl[27:33]]).mat,
            "dc R": float(lspl[33]),
            "dc CN": float(lspl[34]),
            "mt confidence": float(lspl[35]),
            "magnitude": float(lspl[36]),
        }
    return events


def read_mts(filename="FullCat_MT_mag.csv"):
    event_dir = root_dir + "Catalogs\\"
    event_file = event_dir + filename
    events = {}
    # report = read_frac_report()

    f = open(event_file)
    head = f.readline()
    lines = f.readlines()
    f.close()

    for line in lines:
        lspl = line.split(",")
        timestamp = utc.localize(datetime.strptime(lspl[1], "%Y-%m-%d %H:%M:%S.%f"))
        timestamp_id = datetime.strftime(timestamp, "%Y%m%d%H%M%S.%f")
        events[timestamp_id] = {
            "id": lspl[0],
            "timestamp": timestamp,
            "easting": float(lspl[2]),
            "northing": float(lspl[3]),
            "elevation": -float(lspl[4]),
            "snr": float(lspl[5]),
            "sws_amplitude": float(lspl[6]),
            "stack_amplitude": float(lspl[7]),
            "gen MT": MomentTensor.from_vector([float(s) for s in lspl[8:14]]).mat,
            "gen R": float(lspl[14]),
            "gen CN": float(lspl[15]),
            "dc MT": MomentTensor.from_vector([float(s) for s in lspl[16:22]]).mat,
            "dc R": float(lspl[22]),
            "dc CN": float(lspl[23]),
            "mt confidence": float(lspl[24]),
            "magnitude": float(lspl[25]),
        }
    return events


def get_velocity_model():
    return [
        {"rho": 2180.0, "vp": 2446.0, "vs": 1069.0},
        {"rho": 2324.0, "vp": 3163.0, "vs": 1513.0, "top": 460.0},
        {"rho": 2421.0, "vp": 3726.0, "vs": 1862.0, "top": -20.0},
        {"rho": 2451.0, "vp": 3911.0, "vs": 2046.0, "top": -220.0},
        {"rho": 2495.0, "vp": 4201.0, "vs": 2301.0, "top": -1140.0},
        {"rho": 2734.0, "vp": 6052.0, "vs": 3303.0, "top": -1480.0},
        {"rho": 2686.0, "vp": 5637.0, "vs": 3027.0, "top": -1720.0},
        {"rho": 2749.0, "vp": 6186.0, "vs": 3275.0, "top": -1920.0},
        {"rho": 2494.0, "vp": 4190.0, "vs": 2288.0, "top": -2320.0},
        {"rho": 2614.0, "vp": 5063.0, "vs": 2687.0, "top": -2560.0},
    ]


def assign_stages():
    f = open(root_dir + "wells\\Chevron-perf_locations_2019_only.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    perfs = {}
    timestamp_last = 0
    for line in lines:
        lspl = line.split(",")
        well_name = lspl[0]
        if well_name not in perfs:
            perfs[well_name] = {}
            i_stage = 0
        timestamp = int(float(lspl[2]))
        if int(lspl[1]) == 0:
            if timestamp_last != timestamp:
                i_stage += 1
            stage = "Stage " + str(i_stage).zfill(2)
        else:
            stage = "Stage " + lspl[1].zfill(2)
        if stage not in perfs[well_name]:
            perfs[well_name][stage] = {}
            i_cluster = 1
        easting, northing, depth = [float(s) for s in lspl[3:6]]
        perfs[well_name][stage]["cluster " + str(i_cluster)] = {
            "timestamp": timestamp,
            "top_east": easting,
            "bottom_east": easting,
            "top_north": northing,
            "bottom_north": northing,
            "top_elevation": -depth,
            "bottom_elevation": -depth,
        }
        i_cluster += 1
        timestamp_last = timestamp
    f = open(root_dir + "wells\\Reporting - Frac Data report data - 2016 only.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        well = lspl[2].split()[1]
        if well not in perfs:
            perfs[well] = {}
        dt = datetime.strptime(lspl[3], "%Y-%m-%d %H:%M")
        timestamp = time.mktime(dt.timetuple())
        stage = "Stage " + lspl[5].zfill(2)
        top_md = float(lspl[6])
        bottom_md = float(lspl[7])
        perfs[well][stage] = {"Zone": {"top_md": top_md, "bottom_md": bottom_md}}
    return perfs


def read_wells():
    base_dir = root_dir + "wells\\"
    wells = {}
    perfs = assign_stages()
    for well_file in glob.glob(base_dir + "*.well"):
        f = open(well_file)
        head = f.readline()
        lines = f.readlines()
        f.close()
        nwell = len(lines)
        easting, northing, tvdss, md = (
            np.zeros(nwell),
            np.zeros(nwell),
            np.zeros(nwell),
            np.zeros(nwell),
        )
        ii = -1
        for line in lines:
            ii += 1
            lspl = line.split(",")
            md[ii] = float(lspl[3])
            northing[ii], easting[ii] = [float(s) for s in lspl[10:12]]
            tvdss[ii] = float(lspl[4])
        well = lspl[1]
        wells[well_lookup[well]] = {
            "measured_depth": md,
            "easting": easting,
            "northing": northing,
            "elevation": -tvdss,
        }
        if well_lookup[well] in ["1", "2", "3", "4", "5", "6"]:
            wells[well_lookup[well]].update(perfs[well])
    for well in ["A", "B", "C", "D"]:
        for stage in list(perfs[well].keys()):
            top_md = perfs[well][stage]["Zone"]["top_md"]
            bottom_md = perfs[well][stage]["Zone"]["bottom_md"]
            perfs[well][stage]["Zone"]["top_east"] = np.interp(
                top_md, wells[well]["measured_depth"], wells[well]["easting"]
            )
            perfs[well][stage]["Zone"]["bottom_east"] = np.interp(
                bottom_md,
                wells[well]["measured_depth"],
                wells[well]["easting"],
            )
            perfs[well][stage]["Zone"]["top_north"] = np.interp(
                top_md, wells[well]["measured_depth"], wells[well]["northing"]
            )
            perfs[well][stage]["Zone"]["bottom_north"] = np.interp(
                bottom_md,
                wells[well]["measured_depth"],
                wells[well]["northing"],
            )
            perfs[well][stage]["Zone"]["top_elevation"] = np.interp(
                top_md, wells[well]["measured_depth"], wells[well]["elevation"]
            )
            perfs[well][stage]["Zone"]["bottom_elevation"] = np.interp(
                bottom_md,
                wells[well]["measured_depth"],
                wells[well]["elevation"],
            )
        wells[well].update(perfs[well])
    return wells


def output_perfs():
    perfs = assign_stages()
    f = open(root_dir + "wells\\Chevron-perf_locations_shifting_well_5.csv")
    head = f.readline()
    f.close()
    g = open(
        root_dir + "Chevron-perf_locations_unique_stage.csv",
        "w",
    )
    g.write(head)
    for well, stages in perfs.items():
        for stage, clusters in stages.items():
            for cluster in clusters.values():
                g.write(f"{well},{stage.split()[1]},{cluster['timestamp']},")
                g.write(
                    f"{cluster['top easting']},{cluster['top northing']},"
                    + f"{-cluster['top elevation']}\n"
                )
    g.close()


def write_catalog(events, file_name, precision="for client"):
    f = open(file_name, "w")
    f.write("Location (Datum UTM NAD27 Zone 11N),,,,,,,,Stage,,,,,,,")
    f.write("Source and Image Parameters,,,,")
    f.write("General MT solutions,,,,,,,,,,,DC constrained MT solutions,,,,,,,,,")
    f.write(
        "DC Nodal Plane 1,,,DC Nodal Plane 2,,,P strain axis,, B strain axis,, T strain axis,\n"
    )
    f.write("event ID,T0,Easting,Northing,TVDss,")
    f.write(
        "Easting location uncertainty,Northing location uncertainty,Depth location uncertainty,"
    )
    f.write("Well Name,Well ID,Stage,Unique Well Stage ID,")
    f.write(
        "Pumping curve integer,Pumping interval description,Number of diverter drops,"
    )
    f.write("Stack Signal to Noise,Stack Amplitude,Moment Magnitude,Seismic Moment,")
    f.write("Mee,Mnn,Mzz,Men,Mez,Mnz,Pearson R,Condition Number,")
    f.write("DC Component,CLVD Component,ISO component,")
    f.write("Mee,Mnn,Mzz,Men,Mez,Mnz,Pearson R,Condition Number,Confidence")
    [f.write(",Strike,Dip,Rake") for ii in range(2)]
    [f.write(",Trend,Plunge") for ii in range(3)]
    f.write(",mt cluster\n")
    f.write(",YYYY-MM-DD HH:MM:SS.000,")
    [f.write("(m),") for ii in range(6)]
    f.write(",,,,,,,,,,")
    [f.write("(Nm),") for ii in range(7)]
    f.write(",,%,%,%")
    [f.write(",(Nm)") for ii in range(6)]
    f.write(",,,%")
    [f.write(",deg") for ii in range(12)]
    f.write("\n")
    for event_id, event in events.items():
        f.write(
            event["id"]
            + event["timestamp"]
            .astimezone(timezone)
            .strftime(",%Y-%m-%d %H:%M:%S.%f")[:-3]
        )
        if precision == "max":
            f.write(f",{event['easting']},{event['northing']},{-event['elevation']},")
            f.write(
                f"{event['easting_err']},{event['northing_err']},{event['elevation_err']},"
            )
            f.write(
                f"{event['long_well_name']},{event['well']},{event['stage']},{event['unique_well_stage']},"
            )
            f.write(
                f"{event['treatment_counter']},{event['treatment_code']},{event['n_diverters']},"
            )
            f.write(f"{event['snr']},{event['stack_amplitude']},")

            f.write(f"{event['magnitude']},{event['moment']},")
            [f.write(f"{m},") for m in MomentTensor.from_matrix(event["gen MT"]).vec]
            f.write(f"{event['gen R']},{event['gen CN']},")
            f.write(f"{event['dc']},{event['clvd']},{event['iso']},")
            [f.write(f"{m},") for m in MomentTensor.from_matrix(event["dc MT"]).vec]
            f.write(f"{event['dc R']},{event['dc CN']},{event['mt confidence']*100},")
            f.write(f"{event['strike']%360},{event['dip']},{event['rake']},")
            f.write(
                f"{event['aux_strike']%360},{event['aux_dip']},{event['aux_rake']},"
            )
            f.write(f"{event['p_trend']%360},{event['p_plunge']},")
            f.write(f"{event['b_trend']%360},{event['b_plunge']},")
            f.write(f"{event['t_trend']%360},{event['t_plunge']}")
            f.write(f",{event['mt cluster']}\n")
        else:
            f.write(
                f",{event['easting']:.1f},{event['northing']:.1f},{-event['elevation']:.1f},"
            )
            f.write(
                f"{event['easting_err']:.1f},{event['northing_err']:.1f},{event['elevation_err']:.1f},"
            )
            f.write(
                f"{event['long_well_name']},{event['well']},{event['stage']},{event['unique_well_stage']},"
            )
            f.write(
                f"{event['treatment_counter']},{event['treatment_code']},{event['n_diverters']},"
            )
            f.write(f"{event['snr']:.2e},{event['stack_amplitude']:.3e},")

            f.write(f"{event['magnitude']:.2f},{event['moment']:.3e},")
            [
                f.write(f"{m:.3e},")
                for m in MomentTensor.from_matrix(event["gen MT"]).vec
            ]
            f.write(f"{event['gen R']:.3f},{event['gen CN']:.1f},")
            f.write(f"{event['dc']:.1f},{event['clvd']:.1f},{event['iso']:.1f},")
            [f.write(f"{m:.3e},") for m in MomentTensor.from_matrix(event["dc MT"]).vec]
            f.write(
                f"{event['dc R']:.3f},{event['dc CN']:.1f},{event['mt confidence']*100:.1f},"
            )
            f.write(
                f"{event['strike']%360:.1f},{event['dip']:.1f},{event['rake']:.1f},"
            )
            f.write(
                f"{event['aux_strike']%360:.1f},{event['aux_dip']:.1f},{event['aux_rake']:.1f},"
            )
            f.write(f"{event['p_trend']%360:.1f},{event['p_plunge']:.1f},")
            f.write(f"{event['b_trend']%360:.1f},{event['b_plunge']:.1f},")
            f.write(f"{event['t_trend']%360:.1f},{event['t_plunge']:.1f}")
            f.write(f",{event['mt cluster']}\n")
    return None


def read_catalog(catalog_csv):
    f = open(catalog_csv)
    header = [f.readline() for i in range(3)]
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        timestamp = timezone.localize(
            datetime.strptime(lspl[1], "%Y-%m-%d %H:%M:%S.%f")
        ).astimezone(utc)
        timestamp_id = datetime.strftime(timestamp, "%Y%m%d%H%M%S.%f")
        events[timestamp_id] = {
            "id": lspl[0].strip(),
            "timestamp": timestamp,
            "easting": float(lspl[2]),
            "northing": float(lspl[3]),
            "elevation": -float(lspl[4]),
            "easting_err": float(lspl[5]),
            "northing_err": float(lspl[6]),
            "elevation_err": float(lspl[7]),
            "long_well_name": lspl[8].strip(),
            "well": lspl[9].split(".")[0].strip(),
            "stage": lspl[10].split(".")[0].strip().zfill(2),
            "unique_well_stage": int(lspl[11].split(".")[0]),
            "treatment_counter": int(lspl[12]),
            "treatment_code": lspl[13].strip(),
            "n_diverters": int(lspl[14]),
            "snr": float(lspl[15]),
            "stack_amplitude": float(lspl[16]),
            "magnitude": float(lspl[17]),
            "moment": float(lspl[18]),
            "gen MT": MomentTensor.from_vector(
                np.array([float(s) for s in lspl[19:25]])
            ).mat,
            "gen R": float(lspl[25]),
            "gen CN": float(lspl[26]),
            "dc": float(lspl[27]),
            "clvd": float(lspl[28]),
            "iso": float(lspl[29]),
            "dc MT": MomentTensor.from_vector(
                np.array([float(s) for s in lspl[30:36]])
            ).mat,
            "dc R": float(lspl[36]),
            "dc CN": float(lspl[37]),
            "mt confidence": float(lspl[38]) / 100.0,
            "strike": float(lspl[39]),
            "dip": float(lspl[40]),
            "rake": float(lspl[41]),
            "aux_strike": float(lspl[42]),
            "aux_dip": float(lspl[43]),
            "aux_rake": float(lspl[44]),
            "p_trend": float(lspl[45]),
            "p_plunge": float(lspl[46]),
            "b_trend": float(lspl[47]),
            "b_plunge": float(lspl[48]),
            "t_trend": float(lspl[49]),
            "t_plunge": float(lspl[50]),
            "mt cluster": int(lspl[51]),
        }
    return events


def classify_treatment(treatment_data_for_stage):
    from scipy.signal import medfilt
    from read_inputs import read_diverter_drops

    tzero = pick_breakdown_time(treatment_data_for_stage)
    proppant = np.array([v["proppant_conc"] for v in treatment_data_for_stage.values()])
    times = np.array(list(treatment_data_for_stage.keys()))
    i_zero = np.where(times == tzero)[0][0]
    med_proppant = medfilt(proppant, 111)
    zone = [
        {"counter": 0, "state": "PRE", "n_diverters": 0},
        {"time": tzero, "counter": 1, "state": "FL", "n_diverters": 0},
    ]
    threshold_1 = 5
    threshold_2 = 120
    check_thresh1 = med_proppant - threshold_1
    check_thresh2 = med_proppant - threshold_2
    counter = 1
    n_diverters = 0
    diverter_stage = False
    stage = np.unique([v["stage"] for v in treatment_data_for_stage.values()])[0]
    well = np.unique([v["well"] for v in treatment_data_for_stage.values()])[0]
    if well == "1":
        diverter_drops = read_diverter_drops()
        if stage in [v["stage"] for v in diverter_drops.values()]:
            drop_times = [k for k, v in diverter_drops.items() if v["stage"] == stage]
            diverter_stage = True

    for i_time, time in enumerate(times[i_zero::60]):
        ii = i_time * 60 + i_zero
        if (
            check_thresh1[ii] * check_thresh1[ii - 60] < 0
            or check_thresh2[ii] * check_thresh2[ii - 60] < 0
        ):
            counter += 1
            zone.append(zone_state(time, counter, n_diverters, med_proppant[ii]))
        if diverter_stage:
            if n_diverters < len(drop_times):
                if time > drop_times[n_diverters]:
                    n_diverters += 1
                    counter += 1
                    zone.append(
                        zone_state(time, counter, n_diverters, med_proppant[ii])
                    )
    zone[-1]["state"] = "POST"
    return zone


def classify_events_by_treatment(stage_events, stage_data, diffusivity=None):
    if len(stage_events) == 0:
        return None
    treatment_zones = classify_treatment(stage_data)
    for event_id, event in stage_events.items():
        zone = in_zone(treatment_zones, event_id)
        event["treatment_counter"] = zone["counter"]
        event["treatment_code"] = zone["state"]
        event["n_diverters"] = zone["n_diverters"]
    if diffusivity is not None:
        hrz_diff = diffusivity["horizontal"]
        t0 = diffusivity["t0"]
        for event in stage_events.values():
            hrz_diffusivity = (
                event["along trend"] ** 2 / (event["timestamp"] - t0).seconds / 4 / PI
            )
            if hrz_diffusivity > hrz_diff:
                event["treatment_code"] = "IS"
    return None


def in_zone(zones, timestamp):
    z_times = np.array(
        [datetime.strptime(z["time"], "%Y%m%d%H%M%S.%f") for z in zones[1:]]
    )
    i_zone = np.where(z_times < datetime.strptime(timestamp, "%Y%m%d%H%M%S.%f"))[0]
    if len(i_zone) == 0:
        return zones[0]
    return zones[i_zone[-1] + 1]


def zone_state(time, counter, n_diverters, med_proppant):
    if med_proppant > 120:
        return {
            "time": time,
            "counter": counter,
            "state": "HP",
            "n_diverters": n_diverters,
        }
    if med_proppant > 5:
        return {
            "time": time,
            "counter": counter,
            "state": "LP",
            "n_diverters": n_diverters,
        }

    return {
        "time": time,
        "counter": counter,
        "state": "FL",
        "n_diverters": n_diverters,
    }


def read_pfi_catalog(filepath):
    f = open(filepath)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0]
        events[event_id] = {
            "timestamp": UTCDateTime(lspl[1]),
            "easting": float(lspl[2]),
            "northing": float(lspl[3]),
            "elevation": -float(lspl[4]),
            "SNR": float(lspl[5]),
            "sws_amplitude": float(lspl[6]),
            "raw_amplitude": float(lspl[7]),
        }
    return events


def read_events_from_Do(filepath, ignore_qc_flag=False):
    f = open(filepath)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    i_Mw = [i for i, h in enumerate(head.split(",")) if h == "Mw"][0]
    i_keep = [i for i, h in enumerate(head.split(",")) if "keep" in h][0]
    for line in lines:
        lspl = line.split(",")
        id = lspl[0].split(".")[0]
        if lspl[-1][0] == "y" and not ignore_qc_flag:
            events[id] = {}
            events[id]["UTC"] = UTCDateTime(lspl[1])
            lat, lon = [float(s) for s in lspl[2:4]]
            events[id]["depth"] = 1000 * float(lspl[4])
            events[id]["Mw"] = float(lspl[i_Mw])
            if i_keep:
                events[id]["keep"] = lspl[i_keep][0]
            events[id]
        if ignore_qc_flag:
            events[id] = {}
            events[id]["UTC"] = UTCDateTime(lspl[1])
            lat, lon = [float(s) for s in lspl[2:4]]
            events[id]["depth"] = 1000 * float(lspl[4])
            if lspl[i_Mw]:
                events[id]["Mw"] = float(lspl[i_Mw])
            if i_keep:
                events[id]["keep"] = lspl[i_keep][0]
    return events


def read_ppv(filepath):
    f = open(filepath)
    head = f.readline()
    lines = f.readlines()
    f.close()

    pgv = {}
    for line in lines:
        lspl = line.split(",")
        event_id = lspl[0].split(".")[0]
        if event_id not in pgv:
            pgv[event_id] = {"timestamp": UTCDateTime(lspl[1])}
        channel = lspl[7]
        pgv[event_id][channel] = {
            "hypocentral distance": float(lspl[13]) * 1000.0,
            "azimuth": float(lspl[14]),
            "snr": float(lspl[15]),
            "pga": float(lspl[19]) / 100.0,
            "pgv": float(lspl[20]) / 100.0,
            "pgd": float(lspl[21]) / 100.0,
        }
    return pgv


def read_detectability_curve(filepath):
    f = open(filepath)
    lines = f.readlines()
    f.close()
    detectability_curve = {
        "model_quad": float(lines[0].split(",")[1]),
        "model_linear": float(lines[1].split(",")[1]),
        "threshold": float(lines[2].split(",")[1]),
    }
    return detectability_curve


def read_diverter_drops(
    filepath=r"C:\Users\adambaig\Project\Chevron_15-06\From Client\Completions\Well_1_Diverter_Drops.csv",
):
    f = open(filepath)
    head = f.readline()
    lines = f.readlines()
    f.close()
    diverter_drops = {}
    for line in lines:
        lspl = line.split(",")
        amount_kg = float(lspl[1])
        if amount_kg > 1:
            drop_time = timezone.localize(
                datetime.strptime(lspl[2], "%Y-%m-%d %H:%M\n")
            )
            stage = lspl[0].zfill(2)
            if stage == "05":
                stage = "05.0"
            utc_timestamp = datetime.strftime(
                drop_time.astimezone(utc), "%Y%m%d%H%M%S.000000"
            )
            diverter_drops[utc_timestamp] = {
                "stage": stage,
                "amount_kg": amount_kg,
            }
    return diverter_drops


def read_strain(strain_file, tensor=False):
    f = open(strain_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    n_lines = len(lines)
    grid_points = np.zeros([n_lines, 3])
    if not tensor:
        strain_flat = np.zeros(n_lines)
    else:
        strain_flat = np.zeros([n_lines, 6])
    for i_line, line in enumerate(lines):
        lspl = line.split(",")
        grid_points[i_line, :] = [float(s) for s in lspl[:3]]
        if not tensor:
            strain_flat[i_line] = float(lspl[3])
        else:
            strain_flat[i_line, :] = [float(s) for s in lspl[3:]]
    return strain_flat, grid_points


def get_short_well_name(long_well_name):
    return well_lookup[long_well_name]


def get_long_well_name(short_well_name):
    return reverse_well_lookup[short_well_name]


def read_parent_stages_2016():
    f = open(
        f"{root_dir}\\Wells\\Reporting - Frac Data report data - 2020-01-14 16.44.30.csv"
    )
    head = f.readline()
    lines = f.readlines()
    f.close()
    parent_stages = []
    for line in lines:
        lspl = line.split(",")
        if lspl[2] == "":
            continue
        well = lspl[2].split()[1]
        if well not in ["A", "B", "C", "D"]:
            continue
        stage = lspl[5].zfill(2)
        start_time = timezone.localize(datetime.strptime(lspl[3], "%Y-%m-%d %H:%M"))
        end_time = timezone.localize(datetime.strptime(lspl[4], "%Y-%m-%d %H:%M"))
        parent_stages.append(
            {
                "well": well,
                "stage": stage,
                "start_time": float(
                    datetime.strftime(start_time.astimezone(utc), "%Y%m%d%H%M%S")
                ),
                "end_time": float(
                    datetime.strftime(start_time.astimezone(utc), "%Y%m%d%H%M%S")
                ),
            }
        )
    return sorted(parent_stages, key=lambda i: float(i["start_time"]))


def read_frac_dimensions(frac_dimension_file):
    f = open(frac_dimension_file)
    header = [f.readline() for i in range(3)]
    lines = f.readlines()
    stage_dimensions = {}
    for line in lines:
        lspl = line.split(",")
        well = lspl[0]
        stage = lspl[1]
        # if float(stage) < 10:
        #     stage = "0" + stage
        well_stage_identifier = f"Well {well}: Stage {stage}"
        stage_dimensions[well_stage_identifier] = {
            "well": well,
            "stage": stage,
            "unique well stage id": int(lspl[2]),
            "n_events": lspl[3],
            "half-length": float(lspl[4]),
            "half-width": float(lspl[5]),
            "half-height": float(lspl[6]),
            "azimuth": float(lspl[7]),
            "centroid": {
                "along well": float(lspl[8]),
                "across well": float(lspl[9]),
                "elevation": float(lspl[10]),
            },
        }
    return stage_dimensions


def read_diffusivity(diffusivity_file):
    f = open(diffusivity_file)
    header = [f.readline() for i in range(2)]
    lines = f.readlines()
    diffusivity = {}
    for line in lines:
        lspl = line.split(",")
        well = lspl[0]
        stage = lspl[1]
        # if float(stage) < 10:
        #     stage = "0" + stage
        well_stage_identifier = f"Well {well}: Stage {stage}"
        diffusivity[well_stage_identifier] = {
            "well": well,
            "stage": stage,
            "unique well stage id": int(lspl[2]),
            "n_events": lspl[3],
            "initiation time": lspl[4],
            "horizontal": float(lspl[5]),
            "vertical": float(lspl[6]),
        }
    return diffusivity


def read_cumulative_volume(filename):
    f = open(filename)
    header = [f.readline() for i in range(2)]
    lines = f.readlines()
    f.close()
    volume = {}
    for line in lines:
        lspl = line.split(",")
        unique_well_stage = int(lspl[0])
        well = lspl[1]
        stage = lspl[2].zfill(2)
        if well == "1" and abs(float(stage) - 5) < 0.001:
            stage = "05.0"
        if well == "1" and abs(float(stage) - 5.5) < 0.001:
            stage = "05.5"
        well_stage_identifier = f"Well {well}: Stage {stage}"

        volume[well_stage_identifier] = {
            "unique_well_stage": unique_well_stage,
            "total volume": float(lspl[3]),
            "incremental volume": float(lspl[4]),
        }
    return volume


def read_tops():
    tops_dir = "From Client\\tops\\"
    tops = {}
    tops = {"e_grid": np.zeros(111), "n_grid": np.zeros(94)}
    for top_file in glob.glob(f"{tops_dir}*.dat"):
        f = open(top_file)
        header = [f.readline() for i in range(20)]
        lines = f.readlines()
        top = os.path.split(top_file)[1].split(".")[0]
        tops[top] = -2400 * np.ones([111, 94])
        for line in lines:
            lspl = line.split()
            i_row = int(lspl[3]) - 2
            i_col = int(lspl[4]) - 2
            tops[top][i_row, i_col] = float(lspl[2])

    for line in lines:
        lspl = line.split()
        i_row = int(lspl[3]) - 2
        i_col = int(lspl[4]) - 2
        tops["e_grid"][i_row] = float(lspl[0])
        tops["n_grid"][i_col] = float(lspl[1])
    return tops


def read_relocated_events(file_name):
    f = open(file_name)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        timestamp = utc.localize(datetime.strptime(lspl[1], "%Y-%m-%d %H:%M:%S.%f"))
        timestamp_id = datetime.strftime(timestamp, "%Y%m%d%H%M%S.%f")
        events[timestamp_id] = {
            "id": lspl[0],
            "easting": float(lspl[2]),
            "northing": float(lspl[3]),
            "depth_ss": float(lspl[4]),
            "timestamp": timestamp,
        }
    return events


def read_isxcor(
    file_name=os.sep.join(
        [
            "C:",
            "Users",
            "adambaig",
            "Project",
            "Chevron_15-06",
            "Catalogs",
            "goodLocs_isxcor_reference.csv",
        ]
    )
):
    f = open(file_name)
    head = f.readline()
    lines = f.readlines()
    f.close()
    is_xcor = {}
    for line in lines:
        lspl = line.split(",")
        id = lspl[0]
        is_xcor[id] = int(lspl[6])
    return is_xcor
