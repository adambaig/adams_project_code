from copy import deepcopy
from datetime import datetime, timedelta
from matplotlib import cm, colors
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import os
import pytz
from scipy.ndimage import zoom

from pfi_qi.QI_analysis import (
    strain_grid,
    output_isosurface,
)
from pfi_qi.engineering import sorted_stage_list
from read_inputs import (
    read_catalog,
    read_treatment,
    get_velocity_model,
    read_wells,
    read_strain,
    read_or_load_json,
)

from plotting_stuff import gray_background_with_grid
from sms_ray_modelling.anisotropy import (
    thomsen_params_to_stiffness_tensor,
    voigt_to_cjk,
    devoigt,
    voigt_vti_compliance,
)
from sms_moment_tensor.MT_math import decompose_MT

PI = np.pi


def moment_to_potency_tensor(compliance, moment_tensor):
    return np.tensordot(compliance, moment_tensor, axes=([2, 3], [0, 1]))


velocity_model = get_velocity_model()
events = read_catalog(
    "Catalogs/Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
wells = read_wells()
treatment = read_or_load_json(read_treatment, "treatment_data.json")
sorted_stages = read_or_load_json(
    sorted_stage_list, "sorted_stage_list.json", treatment
)
for layer in velocity_model:
    layer["delta"] = 0.015
    layer["epsilon"] = 0.014
    layer["stiffness"] = thomsen_params_to_stiffness_tensor(layer)
    # recalibrate c66 according to Yan et al. 2019
    c66 = (
        -0.826e9
        + 0.403 * layer["stiffness"][0, 0]
        - 0.304 * layer["stiffness"][2, 2]
        + 0.726 * layer["stiffness"][3, 3]
    )
    c12 = c11 - 2 * c66
    layer["stiffness"][0, 1] = c12
    layer["stiffness"][1, 0] = c12
    layer["stiffness"][5, 5] = c66
    layer["compliance"] = devoigt(voigt_vti_compliance(layer["stiffness"]))


tops = np.array([layer["top"] for layer in velocity_model[1:]])
for event in events.values():
    if event["elevation"] > tops[0]:
        s_ijkl = velocity_model[0]["compliance"]
    else:
        i_layer = np.where(tops > event["elevation"])[0][-1] + 1
        s_ijkl = velocity_model[i_layer]["compliance"]
    event["potency"] = moment_to_potency_tensor(s_ijkl, event["gen MT"])


def strain_grid(events, grid_object, k_events, max_distance, csv_out, velocity_model):
    """
    calculate a gridded strain field from potency tensors
    """
    easting, northing, elevation = np.array(
        [(v["easting"], v["northing"], v["elevation"]) for k, v in events.items()]
    ).T
    if np.ndim(grid_object) == 0:
        east_grid = np.arange(min(easting), max(easting), grid_object)
        north_grid = np.arange(min(northing), max(northing), grid_object)
        elevation_grid = np.arange(min(elevation), max(elevation), grid_object)
    else:
        east_grid = np.unique(grid_object[:, 0])
        north_grid = np.unique(grid_object[:, 1])
        elevation_grid = np.unique(grid_object[:, 2])
    indexes = list(events.keys())
    neighbor_tree = BallTree(np.vstack([easting, northing, elevation]).T)
    volume = 4 * PI * max_distance**3 / 3
    strain = np.zeros([len(east_grid), len(north_grid), len(elevation_grid), 3, 3])
    grid_points = []
    for i_east, east in enumerate(east_grid):
        for i_north, north in enumerate(north_grid):
            for i_elev, elevation in enumerate(elevation_grid):
                grid_points.append([east, north, elevation])
                ind = neighbor_tree.query_radius(
                    [[east, north, elevation]], r=max_distance
                )
                shear_mod = shear_modulus(elevation, velocity_model)
                if len(ind[0]) >= k_events:
                    strain[i_east, i_north, i_elev] = (
                        sum([events[indexes[i]]["potency"] for i in ind[0]])
                        / 2
                        / volume
                    )
    return strain, np.array(grid_points)


good_MT_events = {k: v for k, v in events.items() if v["mt confidence"] > 0.95}
grid_spacing = 40
n_neighbours = 5
max_distance = 120

strain_tensors, grid_points = strain_grid(
    good_MT_events,
    grid_spacing,
    n_neighbours,
    max_distance,
    "strain_tensors.csv",
    velocity_model,
    tensor=True,
)


e_grid = np.unique(grid_points[:, 0])
n_grid = np.unique(grid_points[:, 1])
z_grid = np.unique(grid_points[:, 2])
n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)
# DUVERNAY is around z_grid[11]
elevation_index = 11
p_trend, p_plunge, t_trend, t_plunge = (
    np.zeros([n_e, n_n]),
    np.zeros([n_e, n_n]),
    np.zeros([n_e, n_n]),
    np.zeros([n_e, n_n]),
)
p_east, p_north, t_east, t_north = [], [], [], []


quiver_grid = []
for i_e, e in enumerate(e_grid):
    for i_n, n in enumerate(n_grid):
        if np.linalg.norm(strain_tensors[i_e, i_n, elevation_index]) > 1e-16:
            strain_decomp = decompose_MT(strain_tensors[i_e, i_n, elevation_index])
            p_trend[i_e, i_n] = strain_decomp["p_trend"]
            p_plunge[i_e, i_n] = strain_decomp["p_plunge"]
            t_trend[i_e, i_n] = strain_decomp["t_trend"]
            t_plunge[i_e, i_n] = strain_decomp["t_plunge"]
            t_east.append(
                np.sin(PI * t_trend[i_e, i_n] / 180.0)
                * np.cos(PI * t_plunge[i_e, i_n] / 180.0)
            )
            t_north.append(
                np.cos(PI * t_trend[i_e, i_n] / 180.0)
                * np.cos(PI * t_plunge[i_e, i_n] / 180.0)
            )
            p_east.append(
                np.sin(PI * p_trend[i_e, i_n] / 180.0)
                * np.cos(PI * p_plunge[i_e, i_n] / 180.0)
            )
            p_north.append(
                np.cos(PI * p_trend[i_e, i_n] / 180.0)
                * np.cos(PI * p_plunge[i_e, i_n] / 180.0)
            )
            quiver_grid.append({"east": e, "north": n})


fig, (ax1, ax2) = plt.subplots(2, figsize=[36, 16])
ax1.set_aspect("equal")
ax1.quiver(
    np.array([v["east"] for v in quiver_grid]),
    np.array([v["north"] for v in quiver_grid]),
    t_east,
    t_north,
    pivot="mid",
    width=0.002,
    zorder=5,
    headlength=0,
    headaxislength=0,
    headwidth=0,
    scale=1 / 100,
    scale_units="x",
    color="royalblue",
)
ax2.set_aspect("equal")
ax2.quiver(
    np.array([v["east"] for v in quiver_grid]),
    np.array([v["north"] for v in quiver_grid]),
    p_east,
    p_north,
    pivot="mid",
    width=0.002,
    zorder=5,
    headlength=0,
    headaxislength=0,
    headwidth=0,
    scale=1 / 100,
    scale_units="x",
    color="firebrick",
)
x1, x2 = ax1.get_xlim()
y1, y2 = ax1.get_ylim()


for ax in ax1, ax2:
    for well in wells.values():
        ax.plot(well["easting"], well["northing"], "k", lw=3)
    ax.set_xlim([x1, x2])
    ax.set_ylim([y1, y2])
    gray_background_with_grid(ax)

ax1.set_title("tensional strain", fontsize=18)
ax2.set_title("compressional strain", fontsize=18)

fig.savefig("strain_axis_quiver_plot_stacked", bbox_inches="tight")
