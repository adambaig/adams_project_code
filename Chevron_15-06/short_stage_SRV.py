import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import colors
import numpy as np
from scipy.ndimage import zoom
from scipy.interpolate import RectBivariateSpline

from pfi_qi.QI_analysis import strain_grid

from plotting_stuff import gray_background_with_grid
from read_inputs import (
    read_catalog,
    read_wells,
    read_frac_report,
    read_diffusivity,
    read_frac_dimensions,
    read_cumulative_volume,
    read_strain,
    get_velocity_model,
    read_tops,
)

dimensions = read_frac_dimensions("frac_dimensions.csv")
diffusivity = read_diffusivity("rt_curve.csv")
strain = read_cumulative_volume("strain_complete//cumulative_volume.csv")
density = read_cumulative_volume("event_density//cumulative_volume.csv")
wells = read_wells()
tops = read_tops()
tab20 = cm.get_cmap("tab20")
events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
velocity_model = get_velocity_model()


def reshape_report(report):
    reshape = {}
    for well in ["1", "2", "3", "4", "5", "6"]:
        for stage in [k for k in report[f"Well {well}"].keys() if "tage" in k]:
            stage_number = stage.split()[1].zfill(2)
            well_stage_identifier = f"Well {well}: Stage {stage_number}"
            if well == 1 and stage_number == 5:
                reshape[well_stage_identifier + ".0"] = report["Well 1"]["Stage 5"]
                reshape[well_stage_identifier + ".5"] = report["Well 1"]["Stage 5"]
            else:
                reshape[well_stage_identifier] = report[f"Well {well}"][stage]
    reshape.pop("Well 1: Stage 05")
    return reshape


ireton = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["H_V_Ireton"])
duvernay = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Duvernay"])
majeau = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Majeau_Lake"])
beaverhill = RectBivariateSpline(
    tops["e_grid"], tops["n_grid"], tops["Beaverhill_Lake"]
)


def length(point1, point2):
    p1 = np.array([point1["easting"], point1["northing"], point1["elevation"]])
    p2 = np.array([point2["easting"], point2["northing"], point2["elevation"]])
    return np.linalg.norm(p1 - p2)


def measure_aggregate_stage_length(well, stage1, stage2):
    stage1_clusters = list(well[stage1].keys())
    stage2_clusters = list(well[stage2].keys())
    stage1_clusters.sort()
    stage2_clusters.sort()
    end1_bottom = {
        "easting": well[stage1][stage1_clusters[0]]["bottom_east"],
        "northing": well[stage1][stage1_clusters[0]]["bottom_north"],
        "elevation": well[stage1][stage1_clusters[0]]["bottom_elevation"],
    }
    end2_bottom = {
        "easting": well[stage2][stage1_clusters[0]]["bottom_east"],
        "northing": well[stage2][stage1_clusters[0]]["bottom_north"],
        "elevation": well[stage2][stage1_clusters[0]]["bottom_elevation"],
    }
    end1_top = {
        "easting": well[stage1][stage1_clusters[-1]]["top_east"],
        "northing": well[stage1][stage1_clusters[-1]]["top_north"],
        "elevation": well[stage1][stage1_clusters[-1]]["top_elevation"],
    }
    end2_top = {
        "easting": well[stage2][stage1_clusters[-1]]["top_east"],
        "northing": well[stage2][stage1_clusters[-1]]["top_north"],
        "elevation": well[stage2][stage1_clusters[-1]]["top_elevation"],
    }
    return max(length(end1_top, end2_bottom), length(end2_top, end1_bottom))


report = reshape_report(read_frac_report())

n_clusters = {}
for well_id, well in wells.items():
    if well_id in ["1", "2", "3", "4", "5", "6"]:
        stages = {k: v for k, v in well.items() if "tage" in k}.items()
        for stage_id, stage in stages:
            well_stage_identifier = f"Well {well_id}: {stage_id}"
            n_clusters[well_stage_identifier] = len(stage)

test_stages = [k for k, v in n_clusters.items() if v < 6 and k.split()[-1] != "01"]
normal_stages = [k for k, v in n_clusters.items() if v == 6 and k.split()[-1] != "01"]
test_stages.append("Well 1: Stage 30")
comparison_stages = [
    "Well 1: Stage 23",
    "Well 1: Stage 24",
    "Well 1: Stage 25",
    "Well 2: Stage 22",
    "Well 2: Stage 23",
    "Well 2: Stage 24",
    "Well 2: Stage 25",
    "Well 3: Stage 28",
    "Well 3: Stage 29",
    "Well 3: Stage 30",
    "Well 3: Stage 31",
]


strain_flat, grid_points = read_strain(f"strain_complete//fields//all_wells.csv")
east_grid = np.unique(grid_points[:, 0])
north_grid = np.unique(grid_points[:, 1])
non_zero_strain = [v for v in strain_flat if v > 0]

vmin = -8.8
vmax = np.percentile(np.log10(non_zero_strain), 99.9)
east_grid = np.unique(grid_points[:, 0])
north_grid = np.unique(grid_points[:, 1])
elevation_grid = np.unique(grid_points[:, 2])
levels = np.linspace(vmin, vmax, 9)
color_scale = cm.ScalarMappable(
    norm=colors.Normalize(vmin=vmin, vmax=vmax), cmap="inferno_r"
)
contour_colors = []
for level in levels:
    contour_colors.append(color_scale.to_rgba(level))

test_events = {
    k: v
    for k, v in events.items()
    if f"Well {v['well']}: Stage {v['stage']}" in test_stages
    and v["treatment_code"] not in ["IS", "PRE", "POST"]
}
comparison_events = {
    k: v
    for k, v in events.items()
    if f"Well {v['well']}: Stage {v['stage']}" in comparison_stages
    and v["treatment_code"] not in ["IS", "PRE", "POST"]
}
min_neighbours = 5
max_radius = 80
depth_interp_factor = 10
test_strain, grid_points = strain_grid(
    test_events,
    grid_points,
    min_neighbours,
    max_radius,
    "test_stages_strain.csv",
    velocity_model,
)
test_strain_interpolate = zoom(test_strain, 4)
test_depth_interpolate = zoom(test_strain, [1, 1, depth_interp_factor])

comp_strain, grid_points = strain_grid(
    comparison_events,
    grid_points,
    min_neighbours,
    max_radius,
    "comp_stages_strain.csv",
    velocity_model,
)
comp_strain_interpolate = zoom(comp_strain, 4)
comp_depth_interpolate = zoom(comp_strain, [1, 1, depth_interp_factor])

is_events = {k: v for k, v in events.items() if v["treatment_code"] == "FEAT2"}

e_grid = np.unique(grid_points[:, 0])
n_grid = np.unique(grid_points[:, 1])
z_grid = np.unique(grid_points[:, 2])
n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)
strain = np.reshape(strain_flat, [n_e, n_n, n_z])
strain_depth_interp = zoom(strain, [1, 1, depth_interp_factor])
z_grid_interp = zoom(z_grid, depth_interp_factor)
new_grid = np.zeros([depth_interp_factor * n_e * n_n * n_z, 3])
in_duvernay = np.zeros([n_e, n_n, depth_interp_factor * n_z])

for i_e, e in enumerate(e_grid):
    for i_n, n in enumerate(n_grid):
        duvernay_top = duvernay(e, n)
        duvernay_bottom = majeau(e, n)
        for i_z, z in enumerate(z_grid_interp):
            if z > duvernay_bottom and z < duvernay_top:
                in_duvernay[i_e, i_n, i_z] = 1


fig, ax = plt.subplots(figsize=[12, 12])
ax.set_aspect("equal")
for well_stage_identifier in [*test_stages, *comparison_stages]:
    _, well_, _, stage = well_stage_identifier.split()
    well = wells[well_[0]]
    for cluster in well[f"Stage {stage}"].values():
        stage_color = tab20((int(well_[0]) - 1) / 10.0 + 0.01 + 0.05 * (int(stage) % 2))
        ax.plot(
            0.5 * (cluster["top_east"] + cluster["bottom_east"]),
            0.5 * (cluster["top_north"] + cluster["bottom_north"]),
            marker=(7, 1, 0),
            c=stage_color,
            ms=7,
            linewidth=0.25,
            zorder=2,
        )
x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()

for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k", zorder=1)
ax.set_xlim([x1, x2])
ax.set_ylim([y1, y2])
gray_background_with_grid(ax, grid_spacing=50)


fig.savefig("test_and_comparison_stage_perfs.png", bbox_inches="tight")

figure_name = ["test_stage_srv.png", "comp_stage_srv.png"]
for i_field, strain_field in enumerate(
    [test_strain_interpolate, comp_strain_interpolate]
):
    fig_srv, ax_srv = plt.subplots(figsize=[12, 12])
    ax_srv.set_aspect("equal")
    for well_stage_identifier in np.hstack([test_stages, comparison_stages]):
        _, well_, _, stage = well_stage_identifier.split()
        well = wells[well_[0]]
        for cluster in well[f"Stage {stage}"].values():
            stage_color = tab20(
                (int(well_[0]) - 1) / 10.0 + 0.01 + 0.05 * (int(stage) % 2)
            )
            ax_srv.plot(
                0.5 * (cluster["top_east"] + cluster["bottom_east"]),
                0.5 * (cluster["top_north"] + cluster["bottom_north"]),
                marker=(7, 1, 0),
                c=stage_color,
                ms=7,
                linewidth=0.25,
                zorder=2,
            )
    contours = ax_srv.contourf(
        zoom(east_grid, 4),
        zoom(north_grid, 4),
        np.log10(strain_field[:, :, 68]).T,
        vmin=vmin,
        levels=levels,
        vmax=vmax,
        colors=contour_colors,
        extend="max",
    )
    # ax_srv.plot(
    #     [v["easting"] for v in is_events.values()],
    #     [v["northing"] for v in is_events.values()],
    #     "ko",
    #     mew=0.25,
    #     mec="0.3",
    # )
    for well in wells.values():
        ax_srv.plot(well["easting"], well["northing"], "k", zorder=1)
    ax_srv.set_xlim([x1, x2])
    ax_srv.set_ylim([y1, y2])
    gray_background_with_grid(ax_srv, grid_spacing=50)
    fig_srv.savefig(figure_name[i_field], bbox_inches="tight")

vmin = 10 ** (-8.8)
cubic_grid_size = 40 * 40 * 40 / depth_interp_factor
total_vol = len(np.where(comp_depth_interpolate > vmin)[0]) * cubic_grid_size
duvernay_vol = (
    len(np.where(in_duvernay * comp_depth_interpolate > vmin)[0]) * cubic_grid_size
)
print(f"{total_vol:.4e},{duvernay_vol:.4e}\n")
