import matplotlib

matplotlib.use("Qt5Agg")

import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import re

from sms_moment_tensor.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)
from sms_moment_tensor.plotting import plot_regression, plot_beachballs
from sms_ray_modelling.raytrace import isotropic_ray_trace

from read_inputs import read_catalog, read_stations, get_velocity_model


def getVelfromTTT(TTT):
    velocity_model = []
    top = -1 * TTT["originz"]
    d1 = TTT["dz"]
    vel = TTT["vel1D"]
    for vp in vel:
        vs = vp / 1.73205080757
        rho = 310 * (vp**0.25)
        curlayer = {"vp": vp, "top": top, "vs": vs, "rho": rho}
        top = top - d1
        velocity_model.append(curlayer)
    return velocity_model


events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
stations = read_stations()

f = open("mt-qc//refixed_catalog_of_first_motions.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
amp_qc = {}
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[1:]:
        amp_qc[lspl[0]] = {k: v for v, k in zip(lspl[1:], headsplit[1:])}

theoretical_polarities = {}
f = open("mt-qc\\theoretical_polarities_alancienne.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[:1]:
        theoretical_polarities[lspl[0]] = {
            k: float(v) for v, k in zip(lspl[1:], headsplit[1:])
        }


pickle_TTT = open("optimal-TTT.pkl", "rb")
TTT = pickle.load(pickle_TTT, encoding="latin1")
pickle_TTT.close()
velocity_model = getVelfromTTT(TTT)
# velocity_model = get_velocity_model()
# velocity_model[3]

# velocity_model_even_simpler = [
#     velocity_model[-2]
# ]
# velocity_model = velocity_model_even_simpler
sign_correct = {"True": 1, "False": -1}
hrz_cut = 2000
for event_id, event in {
    event["id"]: event for event in events.values() if event["magnitude"] > 0.5
}.items():
    hrz_distance = []
    source = {"e": event["easting"], "n": event["northing"], "z": event["elevation"]}
    if os.path.isfile(f"mt-qc//amplitude_picks//{event_id}.csv") and event_id in amp_qc:
        f = open(f"mt-qc//amplitude_picks//{event_id}.csv")
        amps = {k: float(v) for k, v in [line.split(",") for line in f.readlines()[1:]]}
        f.close()
        good_amps = {
            k: v
            for k, v in amp_qc[event_id].items()
            if "None" not in v
            and np.sqrt(
                (source["e"] - stations[k]["e"]) ** 2
                + (source["n"] - stations[k]["n"]) ** 2
            )
            < hrz_cut
        }

        if len(good_amps) > 6:
            right_hand_side = np.zeros(len(good_amps))
            A_matrix = np.zeros([len(good_amps), 6])
            for i_station, (station, correct_polarity) in enumerate(good_amps.items()):
                p_raypath = isotropic_ray_trace(
                    source, stations[station], velocity_model, "P"
                )
                s_raypath = isotropic_ray_trace(
                    source, stations[station], velocity_model, "S"
                )
                cos_incoming = np.sqrt(
                    1.0
                    - (
                        p_raypath["hrz_slowness"]["e"] ** 2
                        + p_raypath["hrz_slowness"]["n"] ** 2
                    )
                    * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
                )
                right_hand_side[i_station] = (
                    back_project_amplitude(
                        p_raypath,
                        s_raypath,
                        sign_correct[good_amps[station]]
                        * amps[station]
                        * theoretical_polarities[event_id][station],
                        "P",
                        Q=60,
                    )
                    / cos_incoming
                )
                A_matrix[i_station, :] = inversion_matrix_row(p_raypath, "P")
            mt_dictionary = solve_moment_tensor(A_matrix, right_hand_side)
            fig = plt.figure(figsize=[9, 9])
            NSLCs = list(k + "..P" for k in good_amps.keys())
            axDCRegression = fig.add_subplot(2, 2, 1)
            axDc = fig.add_subplot(2, 2, 3)
            axGeneralRegression = fig.add_subplot(2, 2, 2)
            axGen = fig.add_subplot(2, 2, 4)
            plot_regression(
                right_hand_side,
                mt_dictionary["dc"],
                A_matrix,
                axDCRegression,
                fig=fig,
                nslc_tags=NSLCs,
            )
            plot_regression(
                right_hand_side,
                mt_dictionary["general"],
                A_matrix,
                axGeneralRegression,
                fig=fig,
                nslc_tags=NSLCs,
            )
            plot_beachballs(mt_dictionary, axDc, axGen)
            fig.savefig(f"mt_qc_{event_id}_simple_vm.png")
        plt.hist(hrz_distance)
plt.show()
