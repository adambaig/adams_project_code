import matplotlib.pyplot as plt
from read_inputs import read_stations, read_wells

wells = read_wells()
stations = read_stations()


fig, ax = plt.subplots(figsize=[16, 16])
ax.set_aspect("equal")
for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k")


superstations = {k: v for k, v in stations.items() if k[0] == "S"}

bh_stations = {k: v for k, v in stations.items() if k[-1] == "4"}


for station_id, station in superstations.items():
    ax.plot(station["e"], station["n"], "h", c="0.2", ms=5, zorder=2)

for station_id, station in bh_stations.items():
    ax.plot(
        station["e"],
        station["n"],
        "o",
        c="firebrick",
        ms=10,
        markeredgecolor="k",
    )
    ax.text(station["e"], station["n"] - 250, station_id, ha="center", zorder=10)
