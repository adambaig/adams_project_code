# import matplotlib
#
# matplotlib.use("Qt5agg")
from copy import deepcopy
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import pytz

from pfi_qi.QI_analysis import (
    parent_child,
    rt_diffusivity,
    fracture_dimensions,
    strain_grid,
    output_isosurface,
    rotate_all_events_to_well_coords,
)
from pfi_qi.engineering import (
    sorted_stage_list,
    breaktimes_by_stage,
    find_perf_center,
    calc_well_trend,
    make_unique_stage_list,
)
from pfi_qi.rotations import rotate_from_cardinal
from sms_moment_tensor.MT_math import decompose_MT, clvd_iso_dc, mt_to_sdr

from read_inputs import (
    read_catalog,
    get_velocity_model,
    read_wells,
    read_treatment,
    read_or_load_json,
    read_diverter_drops,
    read_parent_stages_2016,
    classify_treatment,
    classify_events_by_treatment,
    write_catalog,
    read_frac_dimensions,
)
from plotting_stuff import (
    rt_with_treatment,
    plot_rotated_stage,
    plot_stage,
    add_dimension_arrows_abs_coords,
    gray_background_with_grid,
)

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")
percentile = 95
bottom_percentile = 100 - percentile
# read in all well and treatment da
treatment = read_or_load_json(read_treatment, "treatment_data.json")
sorted_stages = read_or_load_json(
    sorted_stage_list, "sorted_stage_list.json", treatment
)
break_times = read_or_load_json(
    breaktimes_by_stage, "break_times.json", sorted_stages, treatment
)
velocity_model = get_velocity_model()
events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
wells = read_wells()
break_times
magnitude_of_completeness = -1.25

# This block is dealing with the split stage.
split_stage = {
    k: v for k, v in events.items() if v["well"] == "1" and v["stage"] == "05"
}
for event_id, event in split_stage.items():
    if event["timestamp"] < utc.localize(datetime(2019, 12, 9, 9, 0)):
        event["stage"] = "05.0"
    else:
        event["stage"] = "05.5"
pump_wells = deepcopy(wells)
pump_wells["1"]["Stage 05.0"] = pump_wells["1"]["Stage 05"]
pump_wells["1"]["Stage 05.5"] = pump_wells["1"]["Stage 05"]
pump_wells["1"].pop("Stage 05")

unique_stages = make_unique_stage_list(sorted_stages)
# find frac events (still may have some IS), calculate diffusivity
if percentile == 100:
    frac_dimensions = read_frac_dimensions("frac_dimensions_min_to_max.csv")
else:
    frac_dimensions = read_frac_dimensions("frac_dimensions.csv")

FL_events = {k: v for k, v in events.items() if v["treatment_code"] == "FL"}
LP_events = {k: v for k, v in events.items() if v["treatment_code"] == "LP"}
HP_events = {k: v for k, v in events.items() if v["treatment_code"] == "HP"}


fig1, ([[ax1, ax2], [ax3, ax4]]) = plt.subplots(2, 2, sharex=True, sharey=True)
reference_vector = [0, 1]
ref_width_vector = [0, 1]
for i_stage, stage in enumerate(sorted_stages):
    stage_events = {
        k: v
        for k, v in events.items()
        if v["stage"] == stage["stage"]
        and v["well"] == stage["well"]
        and v["treatment_code"] in ["FL", "LP", "HP"]
        and v["magnitude"] > magnitude_of_completeness
    }
    well_stage_identifier = f'Well {stage["well"]}: Stage {stage["stage"]}'
    rotate_to_well = deepcopy(stage_events)
    if well_stage_identifier in frac_dimensions:
        stage_dimensions = frac_dimensions[well_stage_identifier]
        az_rad = stage_dimensions["azimuth"] * PI / 180
        perf_center = find_perf_center(stage["stage"], pump_wells[stage["well"]])
        stage_events = rotate_from_cardinal(stage_events, az_rad, perf_center)
        rotate_to_well = rotate_from_cardinal(
            rotate_to_well, calc_well_trend(pump_wells[stage["well"]]), perf_center
        )
        for event_id, event in stage_events.items():
            event["along well"] = rotate_to_well[event_id]["along trend"]
            event["across well"] = rotate_to_well[event_id]["perp to trend"]

        for subset in (stage_events,):
            if len(subset) > 1:
                along_trend = [v["along trend"] for v in subset.values()]
                perp_trend = [v["perp to trend"] for v in subset.values()]
                elevation = [v["elevation"] for v in subset.values()]
                along_well = [v["along well"] for v in subset.values()]
                across_well = [v["across well"] for v in subset.values()]
                arrows = {
                    "lengthwise": {
                        "along trend": np.percentile(
                            along_trend, [bottom_percentile, 50, percentile]
                        ),
                        "perp to trend": np.percentile(perp_trend, [50, 50, 50]),
                    },
                    "widthwise": {
                        "along trend": np.percentile(along_trend, [50, 50, 50]),
                        "perp to trend": np.percentile(
                            perp_trend, [bottom_percentile, 50, percentile]
                        ),
                    },
                }
                arrows = rotate_from_cardinal(
                    arrows,
                    az_rad,
                    perf_center,
                    reverse=True,
                )
                length_vector = np.array(
                    [arrows["lengthwise"]["easting"], arrows["lengthwise"]["northing"]]
                )
                width_vector = np.array(
                    [arrows["widthwise"]["easting"], arrows["widthwise"]["northing"]]
                )
                # unpack arrow vectors to be relative and outward from centroid
                length_vector_A = np.ravel(np.diff(length_vector[:, :2][:, ::-1]))
                length_vector_B = np.ravel(np.diff(length_vector[:, 1:]))
                width_vector_A = np.ravel(np.diff(width_vector[:, :2][:, ::-1]))
                width_vector_B = np.ravel(np.diff(width_vector[:, 1:]))

                if np.dot(reference_vector, length_vector_A) > 0:
                    half_length1 = np.linalg.norm(length_vector_A)
                    half_length2 = np.linalg.norm(length_vector_B)
                else:
                    half_length1 = np.linalg.norm(length_vector_B)
                    half_length2 = np.linalg.norm(length_vector_A)
                if np.dot(reference_vector, width_vector_A) > 0:
                    half_width1 = np.linalg.norm(width_vector_A)
                    half_width2 = np.linalg.norm(width_vector_B)
                else:
                    half_width1 = np.linalg.norm(width_vector_B)
                    half_width2 = np.linalg.norm(width_vector_A)

                ax1.plot(
                    (az_rad / D2R) % 180,
                    length_factor * np.diff(length_vector)[1][1],
                    ".",
                )
                ax2.plot(
                    (az_rad / D2R) % 180,
                    -length_factor * np.diff(length_vector)[1][0],
                    ".",
                )
                ax3.plot(
                    (az_rad / D2R) % 180,
                    width_factor * np.diff(width_vector)[1][1],
                    ".",
                )
                ax4.plot(
                    (az_rad / D2R) % 180,
                    -width_factor * np.diff(width_vector)[1][0],
                    ".",
                )
ax1.set_title("half length 1")
ax2.set_title("half length 2")
ax3.set_title("half width 1")
ax4.set_title("half width 2")
