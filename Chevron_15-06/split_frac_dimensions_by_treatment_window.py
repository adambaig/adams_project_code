# import matplotlib
#
# matplotlib.use("Qt5agg")
from copy import deepcopy
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import pytz

from pfi_qi.QI_analysis import (
    parent_child,
    rt_diffusivity,
    fracture_dimensions,
    strain_grid,
    output_isosurface,
    rotate_all_events_to_well_coords,
)
from pfi_qi.engineering import (
    sorted_stage_list,
    breaktimes_by_stage,
    find_perf_center,
    calc_well_trend,
    make_unique_stage_list,
)
from pfi_qi.rotations import rotate_from_cardinal
from sms_moment_tensor.MT_math import decompose_MT, clvd_iso_dc, mt_to_sdr

from read_inputs import (
    read_catalog,
    get_velocity_model,
    read_wells,
    read_treatment,
    read_or_load_json,
    read_diverter_drops,
    read_parent_stages_2016,
    classify_treatment,
    classify_events_by_treatment,
    write_catalog,
    read_frac_dimensions,
)
from plotting_stuff import (
    rt_with_treatment,
    plot_rotated_stage,
    plot_stage,
    add_dimension_arrows_abs_coords,
    gray_background_with_grid,
)

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")
percentile = 100
bottom_percentile = 100 - percentile
# read in all well and treatment da
treatment = read_or_load_json(read_treatment, "treatment_data.json")
sorted_stages = read_or_load_json(
    sorted_stage_list, "sorted_stage_list.json", treatment
)
break_times = read_or_load_json(
    breaktimes_by_stage, "break_times.json", sorted_stages, treatment
)
velocity_model = get_velocity_model()
events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
wells = read_wells()
break_times
magnitude_of_completeness = -1.25

# This block is dealing with the split stage.
split_stage = {
    k: v for k, v in events.items() if v["well"] == "1" and v["stage"] == "05"
}
for event_id, event in split_stage.items():
    if event["timestamp"] < utc.localize(datetime(2019, 12, 9, 9, 0)):
        event["stage"] = "05.0"
    else:
        event["stage"] = "05.5"
pump_wells = deepcopy(wells)
pump_wells["1"]["Stage 05.0"] = pump_wells["1"]["Stage 05"]
pump_wells["1"]["Stage 05.5"] = pump_wells["1"]["Stage 05"]
pump_wells["1"].pop("Stage 05")

unique_stages = make_unique_stage_list(sorted_stages)
# find frac events (still may have some IS), calculate diffusivity
if percentile == 100:
    frac_dimensions = read_frac_dimensions("frac_dimensions_min_to_max.csv")
else:
    frac_dimensions = read_frac_dimensions("frac_dimensions.csv")

FL_events = {k: v for k, v in events.items() if v["treatment_code"] == "FL"}
LP_events = {k: v for k, v in events.items() if v["treatment_code"] == "LP"}
HP_events = {k: v for k, v in events.items() if v["treatment_code"] == "HP"}


f = open("split_frac_dimensions_by_treatment_with_centroids_min_to_max.csv", "w")
f.write("Well and Stage,,,,Number of Intervals,,,")
for label in [
    "Complete",
    "Fluid Only",
    "Low Proppant",
    "High Proppant",
    "Fluid and Low Proppant",
    "All Proppant",
    "Fluid and High Proppant",
]:
    f.write(f"{label},,,,,,,,,,")
f.write("\n")
f.write("well,stage,unique id,azimuth,fluid,low proppant,high proppant")
[
    f.write(
        ",number of events,half-length1 (m),half-length2 (m),"
        "half-width1 (m),half-width2 (m),"
        "half-height1 (m),half-height2 (m),"
        "along well (m),across well (m),relative elevation(m)"
    )
    for i in range(7)
]
f.write("\n")
reference_vector = [0, 1]
ref_width_vector = [0, 1]
for i_stage, stage in enumerate(sorted_stages):
    stage_events = {
        k: v
        for k, v in events.items()
        if v["stage"] == stage["stage"]
        and v["well"] == stage["well"]
        and v["treatment_code"] in ["FL", "LP", "HP"]
        and v["magnitude"] > magnitude_of_completeness
    }
    well_stage_identifier = f'Well {stage["well"]}: Stage {stage["stage"]}'
    rotate_to_well = deepcopy(stage_events)
    if well_stage_identifier in frac_dimensions:
        stage_dimensions = frac_dimensions[well_stage_identifier]
        az_rad = stage_dimensions["azimuth"] * PI / 180
        perf_center = find_perf_center(stage["stage"], pump_wells[stage["well"]])
        stage_events = rotate_from_cardinal(stage_events, az_rad, perf_center)
        rotate_to_well = rotate_from_cardinal(
            rotate_to_well, calc_well_trend(pump_wells[stage["well"]]), perf_center
        )
        for event_id, event in stage_events.items():
            event["along well"] = rotate_to_well[event_id]["along trend"]
            event["across well"] = rotate_to_well[event_id]["perp to trend"]
        stage_FL = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "FL"
        }
        stage_LP = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "LP"
        }
        stage_HP = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "HP"
        }
        zones = classify_treatment(
            {
                k: v
                for k, v in treatment.items()
                if v["well"] == stage["well"] and v["stage"] == stage["stage"]
            }
        )
        n_fl = len([z for z in zones if z["state"] == "FL"])
        n_lp = len([z for z in zones if z["state"] == "LP"])
        n_hp = len([z for z in zones if z["state"] == "HP"])
        f.write(
            f"{stage['well']},{stage['stage']},{i_stage+1},{stage_dimensions['azimuth']:.1f}"
        )
        f.write(f",{n_fl},{n_lp},{n_hp}")
        # f.write(f",{len(stage_events)},{stage_dimensions['half-length']:.1f}")
        # f.write(
        #     f",{stage_dimensions['half-width']:.1f},{stage_dimensions['half-height']:.1f}"
        # )
        # elevation = [v["elevation"] for v in stage_events.values()]
        # along_well = [v["along well"] for v in stage_events.values()]
        # across_well = [v["across well"] for v in stage_events.values()]
        # f.write(f",{np.median(along_well):.1f}")
        # f.write(f",{np.median(across_well):.1f}")
        # f.write(f",{np.median(elevation) - perf_center['elevation']:.1f}")
        along_trend = [v["along trend"] for v in stage_events.values()]
        perp_trend = [v["perp to trend"] for v in stage_events.values()]
        arrows = {
            "lengthwise": {
                "along trend": np.percentile(
                    along_trend, [bottom_percentile, 50, percentile]
                ),
                "perp to trend": np.percentile(perp_trend, [50, 50, 50]),
            },
            "widthwise": {
                "along trend": np.percentile(along_trend, [50, 50, 50]),
                "perp to trend": np.percentile(
                    perp_trend, [bottom_percentile, 50, percentile]
                ),
            },
        }
        arrows = rotate_from_cardinal(
            arrows,
            az_rad,
            perf_center,
            reverse=True,
        )
        length_vector = np.array(
            [arrows["lengthwise"]["easting"], arrows["lengthwise"]["northing"]]
        )
        width_vector = np.array(
            [arrows["widthwise"]["easting"], arrows["widthwise"]["northing"]]
        )
        is_length_north = (length_vector[1][2] - length_vector[1][0]) > 0
        is_width_north = (width_vector[1][2] - width_vector[1][0]) > 0
        for subset in (
            stage_events,
            stage_FL,
            stage_LP,
            stage_HP,
            {**stage_FL, **stage_LP},
            {**stage_LP, **stage_HP},
            {**stage_FL, **stage_HP},
        ):
            f.write(f",{len(subset)}")
            if len(subset) > 1:
                along_trend = [v["along trend"] for v in subset.values()]
                perp_trend = [v["perp to trend"] for v in subset.values()]
                elevation = [v["elevation"] for v in subset.values()]
                along_well = [v["along well"] for v in subset.values()]
                across_well = [v["across well"] for v in subset.values()]
                arrows = {
                    "lengthwise": {
                        "along trend": np.percentile(
                            along_trend, [bottom_percentile, 50, percentile]
                        ),
                        "perp to trend": np.percentile(perp_trend, [50, 50, 50]),
                    },
                    "widthwise": {
                        "along trend": np.percentile(along_trend, [50, 50, 50]),
                        "perp to trend": np.percentile(
                            perp_trend, [bottom_percentile, 50, percentile]
                        ),
                    },
                }
                arrows = rotate_from_cardinal(
                    arrows,
                    az_rad,
                    perf_center,
                    reverse=True,
                )
                length_vector = np.array(
                    [arrows["lengthwise"]["easting"], arrows["lengthwise"]["northing"]]
                )
                width_vector = np.array(
                    [arrows["widthwise"]["easting"], arrows["widthwise"]["northing"]]
                )
                # unpack arrow vectors to be relative and outward from centroid
                length_vector_A = np.ravel(np.diff(length_vector[:, :2][:, ::-1]))
                length_vector_B = np.ravel(np.diff(length_vector[:, 1:]))
                width_vector_A = np.ravel(np.diff(width_vector[:, :2][:, ::-1]))
                width_vector_B = np.ravel(np.diff(width_vector[:, 1:]))

                if np.dot(reference_vector, length_vector_A) > 0:
                    half_length1 = np.linalg.norm(length_vector_A)
                    half_length2 = np.linalg.norm(length_vector_B)
                else:
                    half_length1 = np.linalg.norm(length_vector_B)
                    half_length2 = np.linalg.norm(length_vector_A)
                if np.dot(ref_width_vector, width_vector_A) > 0:
                    half_width1 = np.linalg.norm(width_vector_A)
                    half_width2 = np.linalg.norm(width_vector_B)
                else:
                    half_width1 = np.linalg.norm(width_vector_B)
                    half_width2 = np.linalg.norm(width_vector_A)

                half_height1 = np.percentile(elevation, percentile) - np.median(
                    elevation
                )
                half_height2 = np.median(elevation) - np.percentile(
                    elevation, bottom_percentile
                )
                f.write(
                    f",{half_length1:.1f},{half_length2:.1f}"
                    f",{half_width1:.1f},{half_width2:.1f}"
                    f",{half_height1:.1f},{half_height2:.1f}"
                    f",{np.median(along_well):.1f}"
                    f",{np.median(across_well):.1f}"
                    f",{np.median(elevation) - perf_center['elevation']:.1f}"
                )
            elif len(subset) == 1:
                along_well = [v["along well"] for v in subset.values()]
                across_well = [v["across well"] for v in subset.values()]
                elevation = [v["elevation"] for v in subset.values()]
                f.write(",0,0,0,0,0,0")
                f.write(f",{np.median(along_well):.1f}")
                f.write(f",{np.median(across_well):.1f}")
                f.write(f",{np.median(elevation) - perf_center['elevation']:.1f}")
            else:
                f.write(",0,0,0,0,0,0,0,0,0")
        f.write("\n")
f.close()
