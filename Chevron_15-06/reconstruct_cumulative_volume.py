import glob
import matplotlib.pyplot as plt
import numpy as np
import os
from read_inputs import read_strain

PI = np.pi
one_event_in_80_sphere = 3 / 4 / PI / 80 / 80 / 80
dir = {"strain": "strain_complete//", "density": "events_density"}
vmin = {"strain": -8.8, "density": np.log10(one_event_in_80_sphere)}
grid_spacing = 40
strain_dir = "strain_complete//"
strain_files = glob.glob(f"{strain_dir}fields//after_*csv")
g = open(f"{dir['strain']}cumulative_volume.csv", "w")
g.write("unique stage id, well, stage, volume, differential\n")
g.write(",,,(m3),(m3)\n")
volume_old = 0
for strain_file in strain_files:
    strain_flat, grid_points = read_strain(strain_file)
    volume_new = (
        len(np.where(strain_flat > 10 ** (vmin["strain"]))[0]) * grid_spacing**3
    )
    file_split = os.path.split(strain_file)[1].split("_")
    well_stage = int(file_split[1])
    well = file_split[2][4]
    stage = file_split[3][5:7]
    g.write(
        f"{well_stage},{well},{stage},{volume_new:.4e},{volume_new - volume_old: .4e}\n"
    )
    volume_old = volume_new

g.close()
