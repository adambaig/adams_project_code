import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np

from read_inputs import read_wells, read_events


def gray_background_with_grid(axis, grid_spacing=250):
    axis.set_facecolor("lightgrey")
    axis.set_aspect("equal")
    x1, x2 = axis.get_xlim()
    y1, y2 = axis.get_ylim()
    grid_spacing2 = 2 * grid_spacing
    x_minor_ticks = np.arange(
        np.floor(x1 / grid_spacing) * grid_spacing,
        np.ceil(x2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    x_major_ticks = np.arange(
        np.floor(x1 / grid_spacing2) * grid_spacing2,
        np.ceil(x2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    y_minor_ticks = np.arange(
        np.floor(y1 / grid_spacing) * grid_spacing,
        np.ceil(y2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    y_major_ticks = np.arange(
        np.floor(y1 / grid_spacing2) * grid_spacing2,
        np.ceil(y2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    axis.set_xticks(x_minor_ticks, minor=True)
    axis.set_yticks(y_minor_ticks, minor=True)
    axis.set_xticks(x_major_ticks, minor=False)
    axis.set_yticks(x_major_ticks, minor=False)
    axis.grid(which="both")
    axis.grid(which="minor", alpha=1)
    axis.set_xlim([x1, x2])
    axis.set_ylim([y1, y2])
    axis.set_yticklabels([])
    axis.set_xticklabels([])
    axis.tick_params(which="both", color=(0, 0, 0, 0))
    axis.set_axisbelow(True)
    return axis


tab20 = cm.get_cmap("tab20")
wells = read_wells()
fig, ax = plt.subplots(figsize=[16, 16])
ax.set_aspect("equal")
for well_id, well in wells.items():
    ax.plot(well["easting"], well["northing"], "k")
    end_of_well = {"easting": well["easting"][-1], "northing": well["northing"][-1]}
    if well_id in ["1", "2", "3", "4", "5", "A", "B"]:
        ax.text(
            end_of_well["easting"] - 50,
            end_of_well["northing"] + 50,
            well_id,
            ha="right",
            va="bottom",
            fontsize=16,
        )
    else:
        ax.text(
            end_of_well["easting"] + 50,
            end_of_well["northing"] - 50,
            well_id,
            ha="left",
            va="top",
            fontsize=16,
        )

    if well_id in ["1", "2", "3", "4", "5", "6"]:
        stages = [v for k, v in well.items() if "tage" in k]
        for i_stage, stage in enumerate(stages):
            stage_color = tab20((int(well_id) - 1) / 10.0 + 0.01 + 0.05 * (i_stage % 2))
            for perf, cluster in stage.items():
                cluster["easting"] = 0.5 * (
                    cluster["top_east"] + cluster["bottom_east"]
                )
                cluster["northing"] = 0.5 * (
                    cluster["top_north"] + cluster["bottom_north"]
                )
                cluster["elevation"] = 0.5 * (
                    cluster["top_elevation"] + cluster["bottom_elevation"]
                )
            ax.plot(
                [v["easting"] for v in stage.values()],
                [v["northing"] for v in stage.values()],
                marker=(7, 1, 0),
                c=stage_color,
                ms=3,
                linewidth=0.25,
                zorder=2,
            )
ax = gray_background_with_grid(ax)
fig.savefig("wells_and_perfs_plan_view.png", bbox_inches="tight")
