import glob
from matplotlib import cm, colors
import matplotlib.pyplot as plt
import numpy as np
import os
import pytz
from scipy.ndimage import zoom

from pfi_qi.QI_analysis import event_density_grid

from read_inputs import (
    read_catalog,
    get_velocity_model,
    read_wells,
    read_treatment,
    read_or_load_json,
    read_strain,
)
from plotting_stuff import (
    rt_with_treatment,
    plot_rotated_stage,
    plot_stage,
    add_dimension_arrows,
    gray_background_with_grid,
)


PI = np.pi
D2R = PI / 180.0

method = "strain"
wells = read_wells()


def read_field_into_prism(filename):
    field_flat, grid_points = read_strain(filename)
    e_grid = np.unique(grid_points[:, 0])
    n_grid = np.unique(grid_points[:, 1])
    z_grid = np.unique(grid_points[:, 2])
    n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)
    return np.reshape(field_flat, [n_e, n_n, n_z]), grid_points


srv_dir = "strain_complete//"
all_well_strain, grid_points = read_field_into_prism(
    f"{srv_dir}//fields//all_wells.csv"
)
non_zero_strain = [v for v in np.ravel(all_well_strain) if v > 0]
vmin = -8.8
vmax = np.percentile(np.log10(non_zero_strain), 99.9)
color_scale = cm.ScalarMappable(
    norm=colors.Normalize(vmin=vmin, vmax=vmax), cmap="inferno_r"
)
cb_label = "strain"

east_grid = np.unique(grid_points[:, 0])
north_grid = np.unique(grid_points[:, 1])
elevation_grid = np.unique(grid_points[:, 2])

levels = np.linspace(vmin, vmax, 9)


single_well = glob.glob(f"{srv_dir}//fields//well_?.csv")
combo_wells = glob.glob(f"{srv_dir}//fields//combo_??.csv")


for field_file in [*single_well, *combo_wells]:
    field, grid_points = read_field_into_prism(field_file)
    fig, ax = plt.subplots(figsize=[16, 16])
    ax.set_aspect("equal")
    out_name = os.path.split(field_file)[1].split(".")[0]
    contour_colors = []
    for level in levels:
        contour_colors.append(color_scale.to_rgba(level))
    if method == "event density":
        contours = ax.contourf(
            east_grid,
            north_grid,
            np.log10(field[:, :, 17]).T,
            vmin=vmin,
            levels=levels,
            vmax=vmax,
            colors=contour_colors,
            extend="max",
        )
    elif method == "strain":
        field_interp = zoom(field, 4)
        contours = ax.contourf(
            zoom(east_grid, 4),
            zoom(north_grid, 4),
            np.log10(field_interp[:, :, 68]).T,
            vmin=vmin,
            levels=levels,
            vmax=vmax,
            colors=contour_colors,
            extend="max",
        )
    for well in wells.values():
        ax.plot(well["easting"], well["northing"], "k")

    ax = gray_background_with_grid(ax)
    cbaxes = fig.add_axes([0.2, 0.18, 0.6, 0.03])
    cb = plt.colorbar(contours, cax=cbaxes, orientation="horizontal")
    ticks = cb.get_ticks()
    cb.set_ticklabels([f"{10**tick:.1e}".replace("-", "$-$") for tick in ticks])
    cb.set_label(cb_label, fontsize=32)
    fig.savefig(f"{srv_dir}//figures//{out_name}.png", bbox_inches="tight")
