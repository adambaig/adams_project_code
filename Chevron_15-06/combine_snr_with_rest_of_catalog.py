from read_inputs import read_events, read_catalog, write_catalog, get_long_well_name

snr_events = read_events(filename="FinalCatalog_mt_mag_stageAssign2_reformat.csv")

snr_ids = [v["id"] for v in snr_events.values()]
events = read_catalog("events_after_MT_flip.csv")
snr_by_id = {v["id"]: v["snr"] for v in snr_events.values()}

well_names = {
    "1": "CHEVRON 104 HZ WAHIGAN 14-12-62-22",
    "2": "CHEVRON 103 HZ WAHIGAN 11-12-62-22",
    "3": "CHEVRON 102 HZ WAHIGAN 16-11-62-22",
    "4": "CHEVRON 104 HZ WAHIGAN 1-13-62-22",
    "5": "CHEVRON 106 HZ WAHIGAN 1-13-62-22",
    "6": "CHEVRON HZ KAYBOBS 14-33-61-21",
}


for event in events.values():
    event["snr"] = snr_by_id[event["id"]]
    if event["well"].strip() != "-1":
        event["long_well_name"] = well_names[event["well"].strip()]

write_catalog(events, "Catalogs//Chevron_15_06_Apr30_2020.csv")
