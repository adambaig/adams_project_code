from glob import glob
import json
import os

import numpy as np
from obspy import UTCDateTime
import pyproj as pr
from read_inputs import read_catalog

water_loading_events = read_catalog("Catalogs//Ptest_events.csv")

top_20_water_loading_events = dict(
    sorted(water_loading_events.items(), key=lambda x: x[1]["magnitude"], reverse=True)[
        :20
    ]
)

top_20_ids = [v["id"] for v in water_loading_events.values()]
