import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
from obspy import UTCDateTime
import sys

sys.path.append("..")
from bvalue_plot import plotFMD_from_dict
from read_inputs import read_pfi_catalog, read_events_from_Do, read_ppv
from calibration_functions import predict_mag, fit_data

snr_cut = 3
min_stations = 3
root_dir = r"C:\Users\adambaig\Project\Artis 9-18\Magnitude Calibration"
pfi_catalog = {
    k: v
    for k, v in read_pfi_catalog(
        root_dir + r"\EventCatalog\relocCatalog_simple_stack_All_rerun2.csv"
    ).items()
    if v["elevation"] < -1000
}
specfit_catalog = read_events_from_Do(
    root_dir + r"\Magnitude\5_Spectral_Fitting\Tables\Do_final.csv"
)

for event_id, event in specfit_catalog.items():
    for pfi_id, pfi_event in pfi_catalog.items():
        if abs(event["UTC"] - pfi_event["timestamp"]) < 0.1:
            event["image amplitude"] = pfi_event["raw_amplitude"]
f1, a1 = plt.subplots()
a1.semilogy(
    [v["Mw"] for v in specfit_catalog.values() if "image amplitude" in v],
    [v["image amplitude"] for v in specfit_catalog.values() if "image amplitude" in v],
    "o",
)

x1, x2 = a1.get_xlim()
x_mags = np.linspace(x1, x2, 100)
model_quad = -0.2740683228859524  ### from feasibility modelling
model_linear = 2.534516496227919
model_yint_sample = []
for event in {
    k: v for k, v in specfit_catalog.items() if "image amplitude" in v
}.values():
    model_yint_sample.append(
        np.log10(event["image amplitude"])
        - model_quad * event["Mw"] ** 2
        - model_linear * event["Mw"]
    )
model_yint = np.percentile(model_yint_sample, 95)

slope, y_int = np.polyfit(
    [v["Mw"] for v in specfit_catalog.values() if "image amplitude" in v],
    np.log10(
        [
            v["image amplitude"]
            for v in specfit_catalog.values()
            if "image amplitude" in v
        ]
    ),
    1,
)


a1.semilogy(x_mags, 10 ** (x_mags * slope + y_int))

a1.semilogy(
    x_mags, 10 ** (x_mags * x_mags * model_quad + x_mags * model_linear + model_yint)
)
a1.set_xlabel("specfit magnitude")
a1.set_ylabel("image amplitude")


def mag_from_image_amp_old(event):
    return min(
        np.roots(
            [model_quad, model_linear, model_yint - np.log10(event["raw_amplitude"])]
        )
    )


def mag_from_image_amp(event):
    return min(np.roots([slope, y_int - np.log10(event["raw_amplitude"])]))


for event_id, event in pfi_catalog.items():
    if event_id in specfit_catalog:
        pfi_catalog[event_id]["mw"] = specfit_catalog[event_id]["Mw"]
    else:
        pfi_catalog[event_id]["mw"] = mag_from_image_amp_old(event)

plotFMD_from_dict(pfi_catalog)


pfi_catalog


fig, ax = plt.subplots()
ax.set_aspect("equal")

i_sort = np.argsort([v["mw"] for v in pfi_catalog.values()])

eastings = np.array([v["easting"] for v in pfi_catalog.values()])
northings = np.array([v["northing"] for v in pfi_catalog.values()])
mags = np.array([v["mw"] for v in pfi_catalog.values()])

ax.scatter(eastings[i_sort], northings[i_sort], c=mags[i_sort])

plt.show()
