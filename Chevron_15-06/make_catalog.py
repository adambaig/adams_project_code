import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import mplstereonet as mpls

from read_inputs import read_catalog, write_catalog

from plotting_stuff import plot_strike_dip_rake_rosettes
from sms_moment_tensor.MT_math import (
    mt_vector_to_matrix,
    decompose_MT,
    mt_to_sdr,
    clvd_iso_dc,
)

phase1_events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)


list(phase1_events.items())[0]

mti_events = {}

f = open("mt-qc//MT_comparison_with_general.csv")
head = [f.readline() for n in range(2)]
lines = f.readlines()
f.close()

events_qc = {}
for line in lines:
    lspl = line.split(",")
    event_id = lspl[0]
    events_qc[event_id] = {
        "dc MT": mt_vector_to_matrix([float(s) for s in lspl[1:7]]),
        "dc CN": float(lspl[7]),
        "dc R": float(lspl[8]),
        "n": int(lspl[9]),
        "gen MT": mt_vector_to_matrix([float(s) for s in lspl[10:16]]),
        "gen CN": float(lspl[16]),
        "gen R": float(lspl[17]),
    }
    decomp = decompose_MT(events_qc[event_id]["dc MT"])
    clvd, iso, dc = clvd_iso_dc(events_qc[event_id]["gen MT"])
    sdrs = mt_to_sdr(events_qc[event_id]["dc MT"])
    events_qc[event_id] = {
        **events_qc[event_id],
        **decomp,
        "clvd": clvd,
        "iso": iso,
        "dc": dc,
        "strike": sdrs[0],
        "dip": sdrs[1],
        "rake": sdrs[2],
        "aux strike": sdrs[3],
        "aux dip": sdrs[4],
        "aux rake": sdrs[5],
        "mt confidence": float(lspl[9]) / 100,
    }
    phase1_key = [k for k, v in phase1_events.items() if v["id"] == event_id][0]
    mti_events[phase1_key] = {**phase1_events[phase1_key], **events_qc[event_id]}


catalog = write_catalog(mti_events, "mt-qc//Chevron_15_06_MTI_events.csv")
