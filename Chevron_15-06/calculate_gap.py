import matplotlib.pyplot as plt
import matplotlib.path as mpltPath
import numpy as np
from scipy.ndimage import zoom
from scipy.interpolate import RectBivariateSpline

from plotting_stuff import gray_background_with_grid
from read_inputs import read_wells, read_strain, read_tops

wells = read_wells()
tops = read_tops()


PI = np.pi
one_event_in_80_sphere = 3 / 4 / PI / 80 / 80 / 80
depth_interp_factor = 10

ireton = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["H_V_Ireton"])
duvernay = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Duvernay"])
majeau = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Majeau_Lake"])
beaverhill = RectBivariateSpline(
    tops["e_grid"], tops["n_grid"], tops["Beaverhill_Lake"]
)

vmin = {"strain_complete": 10 ** (-8.8), "event_density": one_event_in_80_sphere}
well2 = pair[1]

f = open("between_well_gaps.csv", "w")
f.write("first well,second well,method,above threshold, below threshhold\n")

cubic_grid_size = 40 * 40 * 40 / depth_interp_factor

for directory in ["strain_complete", "event_density"]:
    for pair in [["1", "2"], ["2", "3"], ["4", "5"]]:
        well1 = pair[0]
        well2 = pair[1]
        perfs = [], []
        for i_well, well in enumerate([well1, well2]):
            stages = list([k for k in wells[well].keys() if "tage" in k])
            if well == "3":  # only count stage 6 and up for well 3
                stages = [s for s in stages if int(s.split()[1]) > 7]
            for stage in stages:
                clusters = {k: v for k, v in wells[well][stage].items()}
                perfs_xy = [
                    [
                        0.5 * (v["top_east"] + v["bottom_east"]),
                        0.5 * (v["top_north"] + v["bottom_north"]),
                    ]
                    for v in clusters.values()
                ]
                for perf_xy in perfs_xy:
                    perfs[i_well].append(perf_xy)

        between_well_fence = mpltPath.Path([*perfs[0], *perfs[1][::-1]])
        perfs = np.array(perfs)
        strain_flat, grid_points = read_strain(
            f"{directory}//fields//combo_{well1}{well2}.csv"
        )
        e_grid = np.unique(grid_points[:, 0])
        n_grid = np.unique(grid_points[:, 1])
        z_grid = np.unique(grid_points[:, 2])
        n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)
        strain = np.reshape(strain_flat, [n_e, n_n, n_z])
        strain_depth_interp = zoom(strain, [1, 1, depth_interp_factor])
        z_grid_interp = zoom(z_grid, depth_interp_factor)
        new_grid = np.zeros([depth_interp_factor * n_e * n_n * n_z, 3])
        in_gap = np.zeros([n_e, n_n, n_z_interp])
        for i_e, e in enumerate(e_grid):
            for i_n, n in enumerate(n_grid):
                duvernay_top = duvernay(e, n)
                duvernay_bottom = majeau(e, n)
                if between_well_fence.contains_point([e, n]):
                    for i_z, z in enumerate(z_grid_interp):
                        if z > duvernay_bottom and z < duvernay_top:
                            in_gap[i_e, i_n, i_z] = 1

        gap_volmue = np.sum(in_gap) * cubic_grid_size
        above_threshhold = (
            len(np.where(strain_depth_interp * in_gap > vmin[directory])[0])
            * cubic_grid_size
        )
        below_threshhold = gap_volmue - above_threshhold
        f.write(f"{well1},{well2},{directory},{above_threshhold}, {below_threshhold}\n")
        fig, ax = plt.subplots(figsize=[16, 16])

        # for well in wells.values():
        #     ax.plot(well['easting'], well['northing'],'k')
        #
        # for gp in grid_points_in_hull:
        #     i_e = np.where(abs(e_grid - gp[0])<1)[0]
        #     i_n = np.where(abs(n_grid - gp[1])<1)[0]
        #     i_z = np.where(abs(z_grid - gp[2])<1)[0]
        #     if strain[i_e, i_n, i_z]< 10**-8.8:
        #         ax.plot(gp[0],gp[1],'.', zorder=-1, color='orange',alpha=0.7)
        # gray_background_with_grid(ax)
f.close()
