from copy import deepcopy
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import ks_2samp
from pfi_qi.QI_analysis import rotate_all_events_to_well_coords
from read_inputs import (
    read_catalog,
    read_wells,
)


events = read_catalog("Catalogs//Chevron_15_06_May8_2020.csv")
wells = read_wells()

# This block is dealing with the split stage.

split_stage = {
    k: v for k, v in events.items() if v["well"] == "1" and v["stage"] == "05"
}
for event_id, event in split_stage.items():
    if event["timestamp"] < utc.localize(datetime(2019, 12, 9, 9, 0)):
        event["stage"] = "05.0"
    else:
        event["stage"] = "05.5"
pump_wells = deepcopy(wells)
pump_wells["1"]["Stage 05.0"] = pump_wells["1"]["Stage 05"]
pump_wells["1"]["Stage 05.5"] = pump_wells["1"]["Stage 05"]
pump_wells["1"].pop("Stage 05")


magnitude_of_completeness = -1.25

well1_events = {k: v for k, v in events.items() if v["well"] == "1"}
rotate_all_events_to_well_coords(well1_events, pump_wells)

# This block is dealing with the split stage.

split_stage = {
    k: v for k, v in events.items() if v["well"] == "1" and v["stage"] == "05"
}
for event_id, event in split_stage.items():
    if event["timestamp"] < utc.localize(datetime(2019, 12, 9, 9, 0)):
        event["stage"] = "05.0"
    else:
        event["stage"] = "05.5"
pump_wells = deepcopy(wells)
pump_wells["1"]["Stage 05.0"] = pump_wells["1"]["Stage 05"]
pump_wells["1"]["Stage 05.5"] = pump_wells["1"]["Stage 05"]
pump_wells["1"].pop("Stage 05")


# find frac events (still may have some IS), calculate diffusivity
diverter_stages = np.unique([v["stage"] for v in diverter_drops.values()])
diverter_stage_events = {
    k: v
    for k, v in events.items()
    if v["well"] == "1" and v["stage"] in diverter_stages
}

diverter_events_complete = {
    k: v
    for k, v in diverter_stage_events.items()
    if v["treatment_code"] not in ["PRE", "POST", "IS"]
    and v["magnitude"] > magnitude_of_completeness
}


ks_stat, p = ks_2samp(
    [v["perp to trend"] for v in d0_events.values()],
    [v["perp to trend"] for v in d2_events.values()],
)


np.median([v["perp to trend"] for v in d1_events.values()])

f = open("diverter_statistical_significance.csv", "w")
f.write("Stage, Intervals, first invterval n, seconds interval n,")
f.write("KS statistic, p-value, up-well shift (m)\n")
n_thresh = 10
for stage in diverter_stages:
    stage_events = {
        k: v
        for k, v in diverter_events_complete.items()
        if v["well"] == "1" and v["stage"] == stage
    }
    d0_events = {k: v for k, v in stage_events.items() if v["n_diverters"] == 0}
    d1_events = {k: v for k, v in stage_events.items() if v["n_diverters"] == 1}
    d2_events = {k: v for k, v in stage_events.items() if v["n_diverters"] == 2}
    n0, n1, n2 = len(d0_events), len(d1_events), len(d2_events)
    perp0 = [v["perp to trend"] for v in d0_events.values()]
    perp1 = [v["perp to trend"] for v in d1_events.values()]
    perp2 = [v["perp to trend"] for v in d2_events.values()]
    if n0 > n_thresh and n1 > n_thresh:
        ks_stat, p = ks_2samp(perp0, perp1)
        shift = np.median(perp1) - np.median(perp0)
        f.write(f"{stage},pre-diverter and diverter 1,")
        f.write(f"{n0}, {n1}, {ks_stat:.4f},{p:.4f}, {shift:.1f}\n")
    if n0 > n_thresh and n2 > n_thresh:
        ks_stat, p = ks_2samp(perp0, perp2)
        shift = np.median(perp2) - np.median(perp0)
        f.write(f"{stage},pre-diverter and diverter 2,")
        f.write(f"{n0}, {n2}, {ks_stat:.4f},{p:.4f}, {shift:.1f}\n")
    if n1 > n_thresh and n2 > n_thresh:
        ks_stat, p = ks_2samp(perp1, perp2)
        shift = np.median(perp2) - np.median(perp1)
        f.write(f"{stage},diverter1 and diverter 2,")
        f.write(f"{n1}, {n2}, {ks_stat:.4f},{p:.4f}, {shift:.1f}\n")
f.close()
