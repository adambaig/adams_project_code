import json
import matplotlib.pyplot as plt
import numpy as np
from sklearn.cluster import AgglomerativeClustering

from sms_moment_tensor.MT_math import (
    decompose_MT,
    trend_plunge_to_unit_vector,
    mt_to_sdr,
    clvd_iso_dc,
    mt_matrix_to_vector,
)

from read_inputs import read_missing_events, read_catalog, write_catalog

unflipped_catalog = read_catalog(
    "Catalogs//events_with_treatment_tags_before_MT_flip.csv"
)
flipped_catalog = read_catalog("Catalogs//Chevron_15_06_May8_2020.csv")
missing_events = read_missing_events()

for event_id, event in unflipped_catalog.items():
    event["snr"] = flipped_catalog[event_id]["snr"]


def norm_MT(mt):
    return mt / np.linalg.norm(mt)


def moment(event):
    return 10 ** (1.5 * event["magnitude"] + 9)


def scaled_mt(mt, moment_value):
    moment = 10 ** (1.5 * event["magnitude"] + 9)
    return np.sqrt(2) * mt * moment / np.linalg.norm(event["dc MT"])


for event in missing_events.values():
    strike, dip, rake, aux_strike, aux_dip, aux_rake = mt_to_sdr(event["dc MT"])
    decomp_flip = decompose_MT(event["dc MT"])
    clvd, iso, dc = clvd_iso_dc(event["gen MT"])
    event["p_trend"] = decomp_flip["p_trend"]
    event["b_trend"] = decomp_flip["b_trend"]
    event["t_trend"] = decomp_flip["t_trend"]
    event["p_plunge"] = decomp_flip["p_plunge"]
    event["b_plunge"] = decomp_flip["b_plunge"]
    event["t_plunge"] = decomp_flip["t_plunge"]
    event["strike"] = strike
    event["dip"] = dip
    event["rake"] = rake
    event["aux_strike"] = aux_strike
    event["aux_dip"] = aux_dip
    event["aux_rake"] = aux_rake
    event["clvd"] = clvd
    event["iso"] = iso
    event["dc"] = dc
    event["moment"] = moment(event)
    event["dc MT"] = scaled_mt(event["dc MT"], event["moment"])
    event["gen MT"] = scaled_mt(event["gen MT"], event["moment"])
    event["treatment_code"] = "XX"
    event["treatment_counter"] = 0
    event["n_diverters"] = 0
    event["mt cluster"] = 0


new_catalog = {**unflipped_catalog, **missing_events}

for id, event in unflipped_catalog.items():
    if "treatment_counter" not in event:
        write()


write_catalog(
    new_catalog,
    "Catalogs//unflipped_events_with_some_treatment_tags_needing_updating.csv",
)
