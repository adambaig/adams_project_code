import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import random

from plotting_stuff import gray_background_with_grid
from read_inputs import read_wells, read_catalog

tab20 = cm.get_cmap("tab20")


def add_tab20_index_to_events(events):
    for event in events.values():
        if event["treatment_code"] in ["PRE", "POST", "FL", "HP", "LP"]:
            event["tab20 index"] = (
                (int(event["well"]) - 1) / 10.0
                + 0.01
                + 0.05 * (int(event["stage"]) % 2)
            )
        elif event["treatment_code"] == "FEAT1":
            event["tab20 index"] = 1.91
        elif event["treatment_code"] == "FEAT2":
            event["tab20 index"] = 1.91
        elif event["treatment_code"] == "IS":
            event["tab20 index"] = 0.71
        else:
            event["tab20 index"] = 0.81
    return None


tab20.set_over("k")
wells = read_wells()
events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
add_tab20_index_to_events(events)

shuffle_order = list(enumerate(events))
random.shuffle(shuffle_order)
_, shuffled_events = zip(*shuffle_order)


fig, ax = plt.subplots(figsize=[16, 16])
ax.set_aspect("equal")
for well_id, well in wells.items():
    if well_id in ["1", "2", "3", "4", "5", "6"]:
        ax.plot(well["easting"], well["northing"], "k")
        end_of_well = {"easting": well["easting"][-1], "northing": well["northing"][-1]}
    # if well_id in ["1", "2", "3", "4", "5", "A", "B"]:
    #     ax.text(
    #         end_of_well["easting"] - 50,
    #         end_of_well["northing"] + 50,
    #         well_id,
    #         ha="right",
    #         va="bottom",
    #         fontsize=16,
    #         zorder=10,
    #     )
    # else:
    #     ax.text(
    #         end_of_well["easting"] + 50,
    #         end_of_well["northing"] - 50,
    #         well_id,
    #         ha="left",
    #         va="top",
    #         fontsize=16,
    #         zorder=10,
    #     )

ax.scatter(
    [events[event]["easting"] for event in shuffled_events],
    [events[event]["northing"] for event in shuffled_events],
    c=[events[event]["tab20 index"] for event in shuffled_events],
    marker=".",
    cmap="tab20",
    zorder=4,
    vmax=1,
    vmin=0,
    edgecolor="0.3",
    linewidth=0.25,
)

gray_background_with_grid(ax)
fig.savefig("events_plan_view_sanitized.png", bbox_inches="tight")
color_indexes = np.unique([v["tab20 index"] for v in events.values()])
color_indexes[-3] = 9.91
color_indexes[-1] = 0.71

fig_leg, ax_leg = plt.subplots(figsize=[16, 16])
patches = []
for color_index in color_indexes:
    (p,) = ax.plot(0, 0, ".", color=tab20(color_index), markeredgecolor="k", mew=0.25)
    patches.append(p)
ax_leg.legend(
    patches,
    [
        "Odd Stages",
        "Even Stages Well 1",
        "Odd Stages",
        "Even Stages Well 2",
        "Odd Stages",
        "Even Stages Well 3",
        "Odd Stages",
        "Even Stages Well 4",
        "Odd Stages",
        "Even Stages Well 5",
        "Odd Stages",
        "Even Stages Well 6",
        "induced feature",
        "Nov 27 pressure test",
        "other induced seismicity",
    ],
)
fig_leg.savefig("event_legend.png")
