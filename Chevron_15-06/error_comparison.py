import matplotlib.pyplot as plt
import numpy as np

from read_inputs import read_catalog


events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
magnitude_of_completeness = -1.25

list(events.values())[0]

well_6_hrz_error = [
    np.linalg.norm([v["easting_err"], v["northing_err"]])
    for v in events.values()
    if v["well"] == "6" and v["magnitude"] > magnitude_of_completeness
]
well_1to5_hrz_error = [
    np.linalg.norm([v["easting_err"], v["northing_err"]])
    for v in events.values()
    if v["well"] in ["1", "2", "3", "4", "5"]
    and v["magnitude"] > magnitude_of_completeness
]
well_6_vrt_error = [
    v["elevation_err"]
    for v in events.values()
    if v["well"] == "6" and v["magnitude"] > magnitude_of_completeness
]
well_1to5_vrt_error = [
    v["elevation_err"]
    for v in events.values()
    if v["well"] in ["1", "2", "3", "4", "5"]
    and v["magnitude"] > magnitude_of_completeness
]


bins = np.arange(0, 101)
fig, [[ax1, ax2], [ax3, ax4]] = plt.subplots(2, 2, figsize=[12, 8])

ax1.hist(well_1to5_hrz_error, bins=bins)
ax2.hist(well_1to5_vrt_error, bins=bins)
ax3.hist(well_6_hrz_error, bins=bins)
ax4.hist(well_6_vrt_error, bins=bins)
ax1.set_ylabel("wells 1 to 5")
ax3.set_ylabel("well 6")
ax1.set_title("horizontal errors")
ax2.set_title("vertical errors")
fig.savefig("error_comparison_north_vs_south.png", bbox_inches="tight")

from datetime import datetime
import pytz

utc = pytz.utc
window_events = {
    k: v
    for k, v in events.items()
    if v["timestamp"] < utc.localize(datetime(2019, 12, 12, 10, 0))
    and v["timestamp"] > utc.localize(datetime(2019, 12, 12, 0, 0))
}
magnitudes = [k for k, v in window_events.items() if v["magnitude"] > -0.8]

fig, ax = plt.subplots()
ax.set_aspect("equal")
ax.plot(
    [v["easting"] for v in window_events.values()],
    [v["northing"] for v in window_events.values()],
    "k.",
    zorder=2,
)
ax.plot(
    [v["easting"] for v in window_events.values() if v["magnitude"] > -0.8],
    [v["northing"] for v in window_events.values() if v["magnitude"] > -0.8],
    "ro",
    zorder=23,
)
[v["northing"] for v in window_events.values() if v["magnitude"] > -0.8]
