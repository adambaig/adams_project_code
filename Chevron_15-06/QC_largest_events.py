import matplotlib.pyplot as plt
import numpy as np
from obspy.imaging.mopad_wrapper import beach

from read_inputs import read_catalog
from sms_moment_tensor.MT_math import (
    mt_matrix_to_vector,
    mt_vector_to_matrix,
    decompose_MT,
)

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
largeM_events = {k: v for k, v in events.items() if v["magnitude"] > 0.5}

f = open("mt-qc//MT_comparison.csv")
head = [f.readline() for n in range(2)]
lines = f.readlines()
f.close()

events_qc, events_no_qc = {}, {}
for line in lines:
    lspl = line.split(",")
    if len(lspl) == 19:
        event_id = lspl[0]
        events_qc[event_id] = {
            "mt DC": mt_vector_to_matrix([float(s) for s in lspl[1:7]]),
            "cn": float(lspl[7]),
            "r": float(lspl[8]),
            "n": int(lspl[9]),
        }
        decomp = decompose_MT(events_qc[event_id]["mt DC"])
        events_qc[event_id] = {**events_qc[event_id], **decomp}
        events_no_qc[event_id] = {
            "mt DC": mt_vector_to_matrix([float(s) for s in lspl[10:16]]),
            "cn": float(lspl[16]),
            "r": float(lspl[17]),
            "n": int(lspl[18]),
        }
        decomp = decompose_MT(events_no_qc[event_id]["mt DC"])
        events_no_qc[event_id] = {**events_no_qc[event_id], **decomp}

fig, ax = plt.subplots()
ax.set_aspect("equal")
for i_event, event in enumerate(largeM_events.values()):
    beach_phase1 = beach(
        mt_matrix_to_vector(event["dc MT"]),
        xy=(1, 1 + i_event),
        width=0.8,
        facecolor="firebrick",
    )
    beach_qc_pol = beach(
        mt_matrix_to_vector(events_qc[event["id"]]["mt DC"]),
        xy=(2, 1 + i_event),
        width=0.8,
        facecolor="royalblue",
    )
    beach_no_qc = beach(
        mt_matrix_to_vector(events_no_qc[event["id"]]["mt DC"]),
        xy=(3, 1 + i_event),
        width=0.8,
        facecolor="forestgreen",
    )
    for bb in (beach_phase1, beach_qc_pol, beach_no_qc):
        ax.add_collection(bb)
    ax.text(4.1, 1.5 + i_event, f"{event['magnitude']:.2f}")

ax.set_xlim([0, 5])
ax.set_ylim([0, i_event + 2])

ax.axis(False)
