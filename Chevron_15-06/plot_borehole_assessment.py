import matplotlib.pyplot as plt
import numpy as np
import re

from read_inputs import read_wells, read_stations, read_catalog
from plotting_stuff import gray_background_with_grid

f = open("mt-qc//borehole_assessment.csv")
head = f.readline()
lines = f.readlines()
f.close()

assessment = {}
for line in lines:
    lspl = line.split(",")
    assessment[lspl[0].upper()] = {
        "consistent": lspl[1].lower(),
        "4 flipped": lspl[2].lower(),
        "4 lower Q": lspl[3].lower(),
        "4 phase shift": lspl[4].lower(),
        "notes": lspl[5].strip(),
    }

stations = read_stations()
wells = read_wells()
events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)


large_events = {k: v for k, v in events.items() if v["magnitude"] > 0.2}
boreholes = {
    (k[:-1] + "X"): v for k, v in stations.items() if bool(re.search("B...4", k))
}

fig1, ax1 = plt.subplots(figsize=[8, 8])
fig2, ax2 = plt.subplots(figsize=[8, 8])
fig3, ax3 = plt.subplots(figsize=[8, 8])
fig4, ax4 = plt.subplots(figsize=[8, 8])
fig5, ax5 = plt.subplots(figsize=[8, 8])
for ax in [ax1, ax2, ax3, ax4, ax5]:
    ax.set_aspect("equal")
    for well in wells.values():
        ax.plot(well["easting"], well["northing"], "0.2", zorder=3)

    for id, location in boreholes.items():
        ax.text(location["e"], location["n"] - 270, id, ha="center", zorder=7)
    for event in large_events.values():
        ax.plot(
            event["easting"], event["northing"], "y*", markeredgecolor="k", zorder=4
        )

i_note = 0
for id, location in boreholes.items():
    if assessment[id]["consistent"] == "yes":
        good = ax1.plot(
            location["e"],
            location["n"],
            "o",
            color="royalblue",
            markeredgecolor="k",
            zorder=5,
        )
    else:
        bad = ax1.plot(
            location["e"],
            location["n"],
            "o",
            color="firebrick",
            markeredgecolor="k",
            zorder=5,
        )

    if assessment[id]["4 flipped"] == "yes":
        flipped = ax2.plot(
            location["e"], location["n"], "o", color="firebrick", markeredgecolor="k"
        )
    else:
        unflipped = ax2.plot(
            location["e"], location["n"], "o", color="royalblue", markeredgecolor="k"
        )

    if assessment[id]["4 lower Q"] == "yes":
        poor_q = ax3.plot(
            location["e"], location["n"], "o", color="firebrick", markeredgecolor="k"
        )
    else:
        good_q = ax3.plot(
            location["e"], location["n"], "o", color="royalblue", markeredgecolor="k"
        )

    if assessment[id]["4 phase shift"] == "no":
        no_phase_delay = ax4.plot(
            location["e"], location["n"], "o", color="royalblue", markeredgecolor="k"
        )
    else:
        phase_delay = ax4.plot(
            location["e"], location["n"], "o", color="firebrick", markeredgecolor="k"
        )
    if assessment[id]["notes"].lower() != "no":
        i_note += 1
        ax5.plot(
            location["e"], location["n"], "o", color="r", markeredgecolor="k", ms=14
        )
        ax5.text(
            location["e"],
            location["n"],
            str(i_note),
            ha="center",
            va="center",
            color="w",
        )
        print(str(i_note), assessment[id]["notes"])
    else:
        ax5.plot(location["e"], location["n"], "o", color="w", markeredgecolor="k")

for ax in [ax1, ax2, ax3, ax4, ax5]:
    ax = gray_background_with_grid(ax, grid_spacing=1000)
ax1.set_title("across borehole consistency")
ax1.legend([good[0], bad[0]], ["consistent", "inconsistent"])
ax2.set_title("lowest sensor polarity")
ax2.legend([unflipped[0], flipped[0]], ["expected polarity", "reversed"])
ax3.set_title("lowest sensor quality")
ax3.legend([good_q[0], poor_q[0]], ["expected quality", "lower quality"])
ax4.set_title("lowest sensor phase shift")
ax4.legend(
    [no_phase_delay[0], phase_delay[0]], ["no phase shift", "observed delay/distortion"]
)
ax5.set_title("other issues")
fig1.savefig("borehole_consstency.png")
fig2.savefig("lowest_polarity.png")
fig3.savefig("lowest_quality.png")
fig4.savefig("phase_delay.png")
fig5.savefig("other_issues.png")

for event in large_events.values():
    print(event["id"], event["timestamp"], event["magnitude"])
