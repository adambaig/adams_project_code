import matplotlib.pyplot as plt
import numpy as np
import numpy.ma as ma

from pfi_qi.QI_analysis import mt_consisitency

from plotting_stuff import gray_background_with_grid
from read_inputs import read_catalog, read_wells, get_velocity_model

PI = np.pi

events = read_catalog(
    "Catalogs//Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
wells = read_wells()
velocity_model = get_velocity_model()

good_MT_events = {k: v for k, v in events.items() if v["mt confidence"] > 0.95}

grid_spacing = 40
n_neighbours = 5
max_distance = 120

consistency, grid_points = mt_consisitency(
    events, grid_points, n_neighbours, max_distance / 2
)


mask_consistency = ma.masked_less(consistency, 1e-7)
fc, ac = plt.subplots(figsize=[16, 16])

ac.set_aspect("equal")
ac.pcolor(e_grid, n_grid, mask_consistency[:, :, 11].T, vmin=0.3, vmax=0.8)
