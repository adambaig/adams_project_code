import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import RectBivariateSpline


from read_inputs import read_catalog, read_tops


events = read_catalog("Catalogs//Chevron_15_06_Apr30_2020_updatedEventDescriptions.csv")

tops = read_tops()

duvernay = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Duvernay"])
beaverhill = RectBivariateSpline(
    tops["e_grid"], tops["n_grid"], tops["Beaverhill_Lake"]
)
ireton = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["H_V_Ireton"])
majeau = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Majeau_Lake"])


def in_formation(event):
    e, n, z = event["easting"], event["northing"], event["elevation"]
    if z > ireton(e, n)[0][0]:
        return "ABOVE"
    if z > duvernay(e, n)[0][0]:
        return "IRETON"
    if z > majeau(e, n)[0, 0]:
        return "DUVERNAY"
    if z > beaverhill(e, n)[0][0]:
        return "MAJEAU"
    return "BEAVERHILL"


for event in events.values():
    event["formation"] = in_formation(event)


fig, ax = plt.subplots()
formation_colors = {
    "ABOVE": "darkgoldenrod",
    "IRETON": "firebrick",
    "DUVERNAY": "forestgreen",
    "MAJEAU": "royalblue",
    "BEAVERHILL": "mediumorchid",
}
for formation in formation_colors.keys():
    east = [v["easting"] for v in events.values() if v["formation"] == formation]
    north = [v["northing"] for v in events.values() if v["formation"] == formation]
    z = [v["elevation"] for v in events.values() if v["formation"] == formation]
    ax.plot(east, z, ".", color=formation_colors[formation])
