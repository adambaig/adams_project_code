# import matplotlib
#
# matplotlib.use("Qt5agg")

from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import pytz

from pfi_qi.QI_analysis import parent_child, rt_diffusivity, fracture_dimensions
from pfi_qi.engineering import (
    sorted_stage_list,
    breaktimes_by_stage,
    find_perf_center,
    calc_well_trend,
)
from pfi_qi.rotations import rotate_from_cardinal

from read_inputs import (
    read_events,
    get_velocity_model,
    read_wells,
    read_treatment,
    read_or_load_json,
    classify_treatment,
    read_diverter_drops,
)
from plotting_stuff import rt_with_treatment, plot_rotated_stage, plot_stage

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")

treatment = read_or_load_json(read_treatment, "treatment_data.json")
sorted_stages = read_or_load_json(
    sorted_stage_list, "sorted_stage_list.json", treatment
)
break_times = read_or_load_json(
    breaktimes_by_stage, "break_times.json", sorted_stages, treatment
)

well_names, stage_names = np.array(
    [
        s.split(":sep:")
        for s in np.unique(
            [v["well"] + ":sep:" + v["stage"] for v in treatment.values()]
        )
    ]
).T
diverter_drops = read_diverter_drops()
color = {"FL": "aquamarine", "LP": "fuchsia", "HP": "orange", "POST": "black"}

for well, stage in zip(well_names, stage_names):
    stage_data = {
        k: v for k, v in treatment.items() if v["well"] == well and v["stage"] == stage
    }
    utc_times = [
        utc.localize(datetime.strptime(t, "%Y%m%d%H%M%S.%f")) for t in stage_data.keys()
    ]
    divisions = classify_treatment(stage_data)
    slurry = np.array([v["slurry_rate"] for v in stage_data.values()]) / 20.0
    proppant = np.array([v["proppant_conc"] for v in stage_data.values()]) / 500.0
    pressure = np.array([v["pressure"] for v in stage_data.values()]) / 100.0
    fig = plt.figure(figsize=[12, 7])
    diverter = None
    if well == "1" and stage in [v["stage"] for v in diverter_drops.values()]:
        ax2 = fig.add_axes([0.1, 0.1, 0.6, 0.8])
        ax4 = fig.add_axes([0.8, 0.1, 0, 0.8])
        ax5 = fig.add_axes([0.9, 0.1, 0, 0.8])
        ax5.set_xticks([])
        ax5.set_yticks(np.linspace(0, 1, 8))
        ax5.set_yticklabels(np.linspace(0, 700, 8))
        ax5.yaxis.set_ticks_position("right")
        ax5.yaxis.set_label_position("right")
        ax5.set_ylabel("Diverter (kg)", color="forestgreen")
        diverter = {k: v for k, v in diverter_drops.items() if v["stage"] == stage}
    else:
        ax2 = fig.add_axes([0.1, 0.1, 0.7, 0.8])
        ax4 = fig.add_axes([0.9, 0.1, 0, 0.8])
    if diverter:
        amounts = np.array([v["amount_kg"] for v in diverter.values()]) / 700.0
        diverter_times = [
            utc.localize(datetime.strptime(t, "%Y%m%d%H%M%S.%f"))
            for t in diverter.keys()
        ]
        ax2.bar(diverter_times, amounts, width=2.0 / 1440, color="forestgreen")
    ax2.set_facecolor("lightgrey")
    ax2.plot(utc_times, proppant, c="darkgoldenrod", lw=2)
    ax2.plot(utc_times, pressure, c="firebrick", lw=2)
    ax2.set_ylabel("pressure (MPa)", color="firebrick")
    ax2.set_yticks(np.linspace(0, 1, 6))
    ax2.set_yticklabels(np.linspace(0, 100, 6))
    ax3 = ax2.twinx()
    ax3.plot(utc_times, slurry, c="royalblue", lw=2)
    ax3.set_ylabel("slurry rate (m$^3$/min)", color="royalblue")
    ax3.set_yticks(np.linspace(0, 1, 5))
    ax3.set_yticklabels(np.linspace(0, 20, 5))
    ax4.yaxis.set_ticks_position("right")
    ax4.yaxis.set_label_position("right")
    ax4.set_yticks(np.linspace(0, 1, 6))
    ax4.set_yticklabels(np.linspace(0, 500, 6))
    ax4.set_xticks([])
    ax4.set_ylabel("Proppant Concentration (kg/m$^3$)", color="darkgoldenrod")
    ax2.xaxis.set_major_formatter(md.DateFormatter("%b %d\n%H:%M", tz=timezone))
    ax2.set_xlabel("local time")
    ax2.set_title(f"Well {well}: Stage {stage}")
    for ax in ax2, ax3, ax4:
        ax.set_ylim([0, 1])
    x1, x2 = ax2.get_xlim()
    for zone in divisions:
        if "time" in zone:
            dt = utc.localize(datetime.strptime(zone["time"], "%Y%m%d%H%M%S.%f"))
            ax2.plot([dt, dt], [0, 1], color=color[zone["state"]], zorder=4, lw=4)
    fig.savefig(f"figures//classification check//treatment_w{well}_s{stage}.png")


for well, stage in zip(well_names, stage_names):
    stage_data = {
        k: v for k, v in treatment.items() if v["well"] == well and v["stage"] == stage
    }
    utc_times = [
        utc.localize(datetime.strptime(t, "%Y%m%d%H%M%S.%f")) for t in stage_data.keys()
    ]
    divisions = classify_treatment(stage_data)
    f = open("stage_divisions.csv", "w")
    f.write("well, stage, time, interval, tag, number of diverters\n")
