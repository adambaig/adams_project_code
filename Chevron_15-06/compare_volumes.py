
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import pickle

tab10 = cm.get_cmap("tab10")
volume = {'density': [], 'strain': []}
files = {
    "strain": 'strain_complete//cumulative_volume.csv',
    "density": 'event_density//cumulative_volume.csv'
}

for field, filename in files.items():
    f = open(filename)
    header = [f.readline() for i in range(2)]
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(',')
        volume[field].append(
        {
        "unique stage id": lspl[0],
        "well": lspl[1].strip(),
        "stage": lspl[2].strip(),
        "total volume": float(lspl[3]),
        "diff volume": float(lspl[4])
        }
        )

well_color = {
    "1": tab10(0.05),
    "2": tab10(0.15),
    "3": tab10(0.25),
    "4": tab10(0.35),
    "5": tab10(0.45),
    "6": tab10(0.55),
    "A": tab10(0.65),
    "B": tab10(0.75),
    "C": tab10(0.85),
    "D": tab10(0.95),
}
#



percent_strike_from_pickle = pickle.load(open('percentStrike.pkl', 'rb'))

for well_id,well in percent_strike_from_pickle.items():

fig, ax = plt.subplots(figsize=[8,8])

for well in ['1','2','3','4','5','6']:
    # a1.plot([v['total volume'] for v in volume['density']], [v['total volume'] for v in volume['strain']],'.')
    ax.plot([v['diff volume'] for v in volume['density'] if v['well']==well],
            [v['diff volume'] for v in volume['strain'] if v['well']==well],
            'o',
            markeredgecolor='k',
            color=well_color[well])
    f_hist, (a_hist1,a_hist2) = plt.subplots(2, figsize=[8,8], sharex=True)
    a_hist1.hist([v['diff volume']*10e-6 for v in volume['strain'] if v['well']==well],edgecolor='0.2',facecolor=well_color[well], bins=np.linspace(0,700,40))
    a_hist2.hist([v['diff volume']*10e-6 for v in volume['density'] if v['well']==well],edgecolor='0.2',facecolor=well_color[well], bins=np.linspace(0,700,40))
    a_hist1.set_ylabel('from strain')
    a_hist2.set_ylabel('from event density')
    a_hist2.set_xlabel('differential volume (x 10$^6$ m$^3$)')
    f_hist.savefig(f'diff_volume_well_{well}.png')
ax.set_ylabel('differential volume (x 10$^6$ m$^3$) from strain')
ax.set_xlabel('differential volume (x 10$^6$ m$^3$) from event density')
ax.legend([f'well {w+1}' for w in range(6)] )
fig.savefig('volume_xplot.png')
