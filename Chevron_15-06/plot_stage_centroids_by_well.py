from copy import deepcopy
from datetime import datetime
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
import pytz

from pfi_qi.QI_analysis import fracture_dimensions

from read_inputs import read_catalog, read_wells

from plotting_stuff import gray_background_with_grid

tab10 = cm.get_cmap("tab10")
utc = pytz.utc

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
wells = read_wells()

{k: v for k, v in events.items() if v["well"] == "3" and v["stage"] == "38"}

# This block is dealing with the split stage.

split_stage = {
    k: v for k, v in events.items() if v["well"] == "1" and v["stage"] == "05"
}
for event_id, event in split_stage.items():
    if event["timestamp"] < utc.localize(datetime(2019, 12, 9, 9, 0)):
        event["stage"] = "05.0"
    else:
        event["stage"] = "05.5"
pump_wells = deepcopy(wells)
pump_wells["1"]["Stage 05.0"] = pump_wells["1"]["Stage 05"]
pump_wells["1"]["Stage 05.5"] = pump_wells["1"]["Stage 05"]
pump_wells["1"].pop("Stage 05")
##

frac_events = {k: v for k, v in events.items() if v["stage"] != "-1"}
frac_dimensions = fracture_dimensions(frac_events, pump_wells)

for well in ["1", "2", "3", "4", "5", "6"]:
    for stage in [k for k, v in pump_wells[well].items() if "tage " in k]:
        for perf, cluster in {k: v for k, v in pump_wells[well][stage].items()}.items():
            cluster["easting"] = 0.5 * (cluster["top_east"] + cluster["bottom_east"])
            cluster["northing"] = 0.5 * (cluster["top_north"] + cluster["bottom_north"])
            cluster["elevation"] = 0.5 * (
                cluster["top_elevation"] + cluster["bottom_elevation"]
            )

well_color = {
    "1": tab10(0.05),
    "2": tab10(0.15),
    "3": tab10(0.25),
    "4": tab10(0.35),
    "5": tab10(0.45),
    "6": tab10(0.55),
    "A": tab10(0.65),
    "B": tab10(0.75),
    "C": tab10(0.85),
    "D": tab10(0.95),
}

for well in ["1", "2", "3", "4", "5", "6"]:
    stage_dimensions = {k: v for k, v in frac_dimensions.items() if f"Well {well}" in k}
    fig = plt.figure(figsize=[16, 12])
    ax = fig.add_axes([0.1, 0.1, 0.5, 0.8])
    ax.plot(
        pump_wells[well]["easting"],
        pump_wells[well]["northing"],
        lw=4,
        zorder=5,
        c="0.3",
    )
    stages = [k for k, v in pump_wells[well].items() if "tage " in k]
    [
        ax.plot(
            np.average([v["easting"] for v in pump_wells[well][stage].values()]),
            np.average([v["northing"] for v in pump_wells[well][stage].values()]),
            marker=(7, 1, 0),
            c=well_color[well],
            ms=13,
            markeredgecolor="k",
            linewidth=0.25,
            zorder=7,
        )
        for stage in stages
    ]
    for well_stage, dimension in stage_dimensions.items():
        ax.plot(
            dimension["centroid easting"],
            dimension["centroid northing"],
            marker="o",
            markeredgecolor="k",
            c=well_color[well],
            zorder=10,
        )
        stage_name = well_stage.split(": ")[1]
        perf_center = {
            "easting": np.average(
                [v["easting"] for v in pump_wells[well][stage_name].values()]
            ),
            "northing": np.average(
                [v["northing"] for v in pump_wells[well][stage_name].values()]
            ),
        }
        ax.plot(
            [dimension["centroid easting"], perf_center["easting"]],
            [dimension["centroid northing"], perf_center["northing"]],
            "k",
            zorder=2,
        )

    x1, x2 = ax.get_xlim()
    y1, y2 = ax.get_ylim()
    for well_id, other_well in wells.items():

        ax.plot(other_well["easting"], other_well["northing"], lw=3, c="0.3", zorder=1)
    ax.set_xlim([x1, x2])
    ax.set_ylim([y1, y2])
    gray_background_with_grid(ax)

    distance_bins = np.hstack(np.linspace(-200, 200, 11))
    a2 = fig.add_axes([0.65, 0.2, 0.3, 0.6])
    a2.hist(
        [v["centroid across well"] for v in stage_dimensions.values()],
        bins=distance_bins,
        facecolor=well_color[well],
        edgecolor="0.2",
    )
    a2.set_xlabel("distance across wells (m)")
    a2.set_ylabel("count")
    a2.set_facecolor("lightgrey")
    fig.savefig(f"centroids_well_{well}.png", bbox_inches="tight")
