from glob import glob
import json
import os

import mplstereonet as mpls
import numpy as np
from obspy import read_inventory
import pyproj as pr

from sms_ray_modelling.raytrace import isotropic_ray_trace

from nmxseis.model.phase import Phase

from nmxseis.numerics.ray_modeling import Raypath

from nmxseis.numerics.moment_tensor import MTDecomp

from nmxseis.numerics.moment_tensor.inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    MTSolution,
)

from nmxseis.numerics.velocity_model.one_dimensional import (
    VelocityModel1D,
    VMLayer1D,
)

from read_inputs import read_catalog, get_velocity_model


latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init="epsg:26711")
water_loading_events = read_catalog("Catalogs//Ptest_events.csv")
inv = read_inventory(r"stations\CV_Full_correct_orientation.xml")
stations = {}
latitudes, longitudes = [], []
for net in inv:
    for station in net:
        channel = inv.select(station=station.code)
        east, north = pr.transform(
            latlon_proj, out_proj, station.longitude, station.latitude
        )
        stations[f"{net.code}.{station.code}."] = {
            "z": station.elevation - channel[0][0][0].depth,
            "e": east,
            "n": north,
        }
event_json = glob(r"waterboarding\MT jsons\*.json")[4]
velocity_model = VelocityModel1D([VMLayer1D(**layer) for layer in get_velocity_model()])


def to_enu(dictionary):
    return np.array([dictionary["e"], dictionary["n"], dictionary["z"]])


for event_json in glob(r"waterboarding\MT jsons\*.json"):
    with open(event_json) as f:
        mt_attributes = json.load(f)

    bpFreqs = mt_attributes["bpFreqs"]
    referenceFreq = np.sqrt(bpFreqs[0] * bpFreqs[1])

    event_id = os.path.basename(event_json).split(".")[0]
    event = [v for v in water_loading_events.values() if v["id"] == event_id][0]
    source = {"e": event["easting"], "n": event["northing"], "z": event["elevation"]}

    velocity_model = VelocityModel1D(
        [VMLayer1D(**layer) for layer in mt_attributes["velDict"]]
    )
    ampDict = mt_attributes["ampDict"]
    pRaypaths, sRaypaths = {}, {}
    for station_id, station in stations.items():
        pRaypaths[station_id] = Raypath.isotropic_ray_trace(
            to_enu(source), to_enu(station), velocity_model, Phase.P
        )
        sRaypaths[station_id] = Raypath.isotropic_ray_trace(
            to_enu(source), to_enu(station), velocity_model, Phase.S
        )

    back_projected_amplitudes = {
        ".".join(
            [
                v["channelPath"]["networkCode"],
                v["channelPath"]["stationCode"],
                v["channelPath"]["locationCode"],
                f'RT{v["phase"]}',
            ]
        ): v["backProjectedAmplitude"]
        for v in mt_attributes["eveDict"]["event"]["origin"]["custom MT"]["Artemis DC"][
            "polarityPicks"
        ]
    }

    inversionMatrix = []
    for station_phase in back_projected_amplitudes:
        network, station, location, channel = station_phase.split(".")
        nsl = ".".join([network, station, ""])
        if channel[-1] == "P":
            inversionMatrix.append(inversion_matrix_row(pRaypaths[nsl], Phase.P))
        elif channel[-1] == "V":
            inversionMatrix.append(inversion_matrix_row(sRaypaths[nsl], Phase.SV))
        elif channel[-1] == "H":
            inversionMatrix.append(inversion_matrix_row(sRaypaths[nsl], Phase.SH))

    moment_tensor = MTSolution.solve(
        inversionMatrix, list(back_projected_amplitudes.values())
    )
    dc_mt = moment_tensor[1].mt
    sdr1_dc, sdr2_dc = dc_mt.to_sdr_pair_dc()

    strikes, dips, rakes, aux_strikes, aux_dips, aux_rakes = [], [], [], [], [], []
    normal_1, normal_2 = [], []
    p_trend, p_plunge, t_trend, t_plunge = [], [], [], []
    for i in range(len(inversionMatrix)):
        new_matrix = inversionMatrix.copy()
        new_back_projected_amplitudes = list(back_projected_amplitudes.values())
        new_back_projected_amplitudes.pop(i)
        new_matrix.pop(i)
        new_moment_tensor = MTSolution.solve(new_matrix, new_back_projected_amplitudes)
        new_mt_dc = new_moment_tensor[1].mt
        decomposition = new_mt_dc.decompose()
        sdr1, sdr2 = new_mt_dc.to_sdr_pair_dc()
        strikes.append(sdr1.strike)
        dips.append(sdr1.dip)
        rakes.append(sdr1.rake)
        aux_strikes.append(sdr2.strike)
        aux_dips.append(sdr2.dip)
        aux_rakes.append(sdr2.rake)
        p_trend.append(decomposition.p_trend)
        p_plunge.append(decomposition.p_plunge)
        t_trend.append(decomposition.t_trend)
        t_plunge.append(decomposition.t_plunge)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection="stereonet")
    ax.plane(strikes, dips, "0.2")
    ax.plane(aux_strikes, aux_dips, "0.2")
    ax.line(p_plunge, p_trend, "+", color="firebrick")
    ax.line(t_plunge, t_trend, "+", color="royalblue")
    ax.plane(sdr1_dc.strike, sdr1_dc.dip, color="0.5", linewidth=2, zorder=2)
    ax.plane(sdr2_dc.strike, sdr2_dc.dip, color="0.5", linewidth=2, zorder=2)
    ax.plane(sdr1_dc.strike, sdr1_dc.dip, color="orangered", linewidth=1, zorder=3)
    ax.plane(sdr2_dc.strike, sdr2_dc.dip, color="orangered", linewidth=1, zorder=3)
    ax.grid()

    fig.savefig(
        rf"waterboarding\jack_knife_tests\jackknife_{event_id}.png", bbox_inches="tight"
    )


fig_pt_legend, ax_pt_legend = plt.subplots()
p_symbol = ax.plot(0, 0, "+", color="firebrick")
t_symbol = ax.plot(0, 0, "+", color="royalblue")
ax_pt_legend.legend([p_symbol[0], t_symbol[0]], ["P axes", "T axes"])
fig_pt_legend.savefig("waterboarding\\pt_legend.png")
