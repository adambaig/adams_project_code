from glob import glob
import json
import os

import numpy as np
from obspy import UTCDateTime
import pyproj as pr
from read_inputs import read_catalog

water_loading_events = read_catalog("Catalogs//Ptest_events.csv")


def getChannelPath(channel):
    channel_split = channel.split(".")
    return {
        "locationCode": channel_split[2],
        "channelCode": "Z",
        "stationCode": channel_split[1],
        "networkCode": channel_split[0],
    }


def packageStationMagnitude(stationMagnitude):
    amplitude = stationMagnitude["amplitude"]
    return {
        "eventAzimuth": stationMagnitude["event_azimuth"],
        "eventDistance": stationMagnitude["event_distance"],
        "value": stationMagnitude["value"],
        "magnitudeType": stationMagnitude["magnitude_type"],
        "amplitude": {
            "time": amplitude["time"],
            "period": amplitude["period"],
            "value": amplitude["value"],
            "amplitudeType": amplitude["amplitude_type"],
            "channelPath": getChannelPath(stationMagnitude["amplitude"]["channel"]),
        },
    }


def packageMagnitude(magnitude):
    return {
        "value": magnitude["value"],
        "error": magnitude["error"],
        "magnitudeType": magnitude["magnitude_type"],
        "stationMagnitudes": [
            packageStationMagnitude(sm) for sm in magnitude["station_magnitudes"]
        ],
    }


def packageArrivals(pick_line):
    split_line = pick_line.split(",")
    return {
        "eventAzimuth": 361,
        "eventDistance": 0.1,
        "time": 0,
        "pick": {
            "phase": split_line[1],
            "time": float(split_line[2]),
            "channelPath": getChannelPath(split_line[0]),
        },
    }


def filterErrorAxes(event):
    outAxes = [
        {
            "dip": 0,
            "azimuth": 0,
            "length": event["northing_err"],
        },
        {
            "dip": 0,
            "azimuth": 90,
            "length": event["easting_err"],
        },
        {
            "dip": 90,
            "azimuth": 0,
            "length": event["elevation_err"],
        },
    ]
    return outAxes


latlon_proj = pr.Proj(init="epsg:4326")
out_proj = pr.Proj(init=f"epsg:26711")


for event in water_loading_events.values():
    eventId = event["id"]

    pick_file_matches = glob(
        os.path.join(
            r"C:\Users\adambaig\logikdata\CHX15-06\picks", rf"{eventId}*.picks"
        )
    )

    if len(pick_file_matches) > 0:
        pick_file = pick_file_matches[0]
        with open(pick_file) as f:
            pick_lines = f.readlines()
        longitude, latitude = pr.transform(
            out_proj, latlon_proj, event["easting"], event["northing"]
        )
        magnitude = {"value": event["magnitude"], "error": 0, "magnitudeType": "Mw"}
        depth_km = -event["elevation"] / 1000
        time = UTCDateTime(event["timestamp"]).timestamp
        analysisTypes = {"m": "MANUAL", "a": "AUTOMATIC"}
        origin = {
            "errorAxes": filterErrorAxes(event),
            "depthError": 0,
            "azimuthGap": 0,
            "analysisType": "MANUAL",
            "timeRms": 0,
            "referenceElevation": 0,
            "distanceMaximum": 0,
            "distanceMinimum": 0,
            "comment": "",
            "author": "",
            "latitude": latitude,
            "longitude": longitude,
            "depth": depth_km,
            "time": time,
            "magnitudes": magnitude,
            "arrivals": [packageArrivals(line) for line in pick_lines],
        }

        mainPath = r"C:\Users\adambaig\logikdata\CHX15-06"
        eventJsonOut = {"event": {"origin": origin}}
        locDir = mainPath + "/Located"
        if not os.path.isdir(locDir):
            os.mkdir(locDir)
        with open(locDir + "/" + eventId + ".json", "w") as f:
            json.dump(eventJsonOut, f)
