from datetime import datetime
from glob import glob
import json
import os

import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory
import pyproj as pr
import pytz
import mplstereonet as mpls

from mtPlots import plot_general_sdr_rosettes
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.moment_tensor.inversion import MTSolution, MTType
from nmxseis.numerics.moment_tensor.plotting import individual_bb_plot
from sms_moment_tensor.MT_math import mt_matrix_to_vector
from read_inputs import read_catalog, get_velocity_model

UTC = pytz.utc
timezone = pytz.timezone("America/Edmonton")
latlon_proj = pr.Proj("epsg:4326")
out_proj = pr.Proj("epsg:26711")
water_loading_events = read_catalog("Catalogs//Ptest_events.csv")
inv = read_inventory(r"stations\CV_Full_correct_orientation.xml")
stations = {}
latitudes, longitudes = [], []
for net in inv:
    for station in net:
        channel = inv.select(station=station.code)
        east, north = pr.transform(
            latlon_proj, out_proj, station.longitude, station.latitude
        )
        stations[f"{net.code}.{station.code}."] = {
            "z": station.elevation - channel[0][0][0].depth,
            "e": east,
            "n": north,
        }

qualities = {
    "Nov 27, 05:25:13.484": "A",
    "Nov 27, 05:27:34.664": "A",
    "Nov 27, 05:34:04.736": "B",
    "Nov 27, 05:42:03.252": "A",
    "Nov 27, 05:57:57.800": "B",
    "Nov 27, 05:59:36.536": "C",
    "Nov 27, 06:03:22.236": "A",
    "Nov 27, 06:05:12.564": "A",
    "Nov 27, 06:38:30.352": "B",
    "Nov 27, 07:05:19.420": "B",
    "Nov 27, 07:13:26.956": "B",
    "Nov 27, 08:10:06.632": "A",
    "Nov 27, 08:41:31.880": "C",
    "Nov 27, 10:39:12.156": "A",
    "Nov 27, 11:56:43.260": "A",
    "Nov 27, 13:05:14.224": "B",
    "Nov 27, 13:31:55.948": "A",
    "Nov 27, 14:55:23.388": "A",
    "Nov 27, 19:11:34.416": "B",
    "Nov 27, 20:41:48.052": "A",
}


def unit_mt_matrix(moment_tensor):
    return moment_tensor.mat / np.linalg.norm(moment_tensor.mat)


def timestamp_to_datetime_string(timestamp):
    return datetime.strftime(timestamp.astimezone(timezone), "%b %d, %H:%M:%S.%f")[:-3]


for event_json in glob(r"waterboarding\MT jsons\*.json"):
    with open(event_json) as f:
        mt_attributes = json.load(f)
    event_id = os.path.basename(event_json).split(".")[0]
    event = [v for v in water_loading_events.values() if v["id"] == event_id][0]
    with open(glob(rf"waterboarding\Final_Picks\{event_id}*.picks")[0]) as f:
        lines = f.readlines()
        num_obs = len(lines)

    old_moment_tensor = MomentTensor.from_matrix(
        np.array(event["dc MT"])
    ).general_to_dc()
    old_dc_solution = MTSolution(
        MTType.DOUBLE_COUPLE,
        old_moment_tensor,
        condition_number=event["dc CN"],
        pearson_r=event["dc R"],
        rank=6,
        num_obs=num_obs,
        singular_values=[0, 0, 0, 0, 0, 0],
    )

    old_confidence = event["mt confidence"] * 100
    new_moment_tensor_from_json = mt_attributes["eveDict"]["event"]["origin"][
        "custom MT"
    ]["Artemis DC"]
    new_moment_tensor_from_json_general = mt_attributes["eveDict"]["event"]["origin"][
        "custom MT"
    ]["Artemis General"]
    new_moment_tensor = MomentTensor.from_vector(
        np.array(
            [
                new_moment_tensor_from_json[cc]
                for cc in ["mee", "mnn", "mzz", "men", "mez", "mnz"]
            ]
        )
    )
    new_moment_tensor_general = MomentTensor.from_vector(
        np.array(
            [
                new_moment_tensor_from_json_general[cc]
                for cc in ["mee", "mnn", "mzz", "men", "mez", "mnz"]
            ]
        )
    )

    new_unit_mt = unit_mt_matrix(new_moment_tensor)
    old_unit_mt = unit_mt_matrix(old_moment_tensor)
    event["similarity_old_to_new"] = np.tensordot(new_unit_mt, old_unit_mt)
    event["new general solution"] = new_moment_tensor_from_json_general
    event["new dc solution"] = new_moment_tensor_from_json


with open("waterboarding//newly_inverted_top_20_waterloading_events.csv", "w") as f:
    for event_id, event in {
        k: v for k, v in water_loading_events.items() if "new dc solution" in v
    }.items():
        timestamp_str = timestamp_to_datetime_string(event["timestamp"])
        quality = qualities[timestamp_str]
        f.write(f"{event['id']},{timestamp_str}")
        f.write(
            f",{event['easting']:.1f},{event['northing']:.1f},{-event['elevation']:.1f},"
        )
        f.write(
            f"{event['easting_err']:.1f},{event['northing_err']:.1f},{event['elevation_err']:.1f},"
        )
        f.write(
            f"{event['long_well_name']},{event['well']},{event['stage']},{event['unique_well_stage']},"
        )
        f.write(
            f"{event['treatment_counter']},{event['treatment_code']},{event['n_diverters']},"
        )
        f.write(f"{event['snr']:.2e},{event['stack_amplitude']:.3e},")

        f.write(f"{event['magnitude']:.2f},{event['moment']:.3e},")
        [
            f.write(f"{event['new general solution'][cc]:.3e},")
            for cc in ["mee", "mnn", "mzz", "men", "mez", "mnz"]
        ]
        f.write(f"{event['new general solution']['pearsonR']:.3f},")
        f.write(f"{event['new general solution']['conditionNumber']:.1f},")
        f.write(f"{event['new general solution']['dc']:.1f},")
        f.write(f"{event['new general solution']['clvd']:.1f},")
        f.write(f"{event['new general solution']['iso']:.1f},")
        [
            f.write(f"{event['new dc solution'][cc]:.3e},")
            for cc in ["mee", "mnn", "mzz", "men", "mez", "mnz"]
        ]
        f.write(f"{event['new dc solution']['pearsonR']:.3f},")
        f.write(f"{event['new dc solution']['conditionNumber']:.1f},")
        f.write(f"{event['new dc solution']['strike']:.1f},")
        f.write(f"{event['new dc solution']['dip']:.1f},")
        f.write(f"{event['new dc solution']['rake']:.1f},")
        f.write(f"{event['new dc solution']['np2Strike']:.1f},")
        f.write(f"{event['new dc solution']['np2Dip']:.1f},")
        f.write(f"{event['new dc solution']['np2Rake']:.1f},")
        f.write(f"{event['new dc solution']['pAxisTrend']:.1f},")
        f.write(f"{event['new dc solution']['pAxisPlunge']:.1f},")
        f.write(f"{event['new dc solution']['bAxisTrend']:.1f},")
        f.write(f"{event['new dc solution']['bAxisPlunge']:.1f},")
        f.write(f"{event['new dc solution']['tAxisTrend']:.1f},")
        f.write(f"{event['new dc solution']['tAxisPlunge']:.1f},")
        f.write(f"{quality}, {event['similarity_old_to_new']:.3f},")
        [f.write(f"{m:.3e},") for m in mt_matrix_to_vector(event["gen MT"])]
        f.write(f"{event['gen R']:.3f},{event['gen CN']:.1f},")
        f.write(f"{event['dc']:.1f},{event['clvd']:.1f},{event['iso']:.1f},")
        [f.write(f"{m:.3e},") for m in mt_matrix_to_vector(event["dc MT"])]
        f.write(
            f"{event['dc R']:.3f},{event['dc CN']:.1f},{event['mt confidence']*100:.1f},"
        )
        f.write(f"{event['strike']%360:.1f},{event['dip']:.1f},{event['rake']:.1f},")
        f.write(
            f"{event['aux_strike']%360:.1f},{event['aux_dip']:.1f},{event['aux_rake']:.1f},"
        )
        f.write(f"{event['p_trend']%360:.1f},{event['p_plunge']:.1f},")
        f.write(f"{event['b_trend']%360:.1f},{event['b_plunge']:.1f},")
        f.write(f"{event['t_trend']%360:.1f},{event['t_plunge']:.1f}")
        f.write("\n")
