from datetime import datetime
from glob import glob
import json
import os

import matplotlib.pyplot as plt
import numpy as np
from obspy import read_inventory
import pyproj as pr
import pytz
import mplstereonet as mpls

from mtPlots import plot_general_sdr_rosettes
from nmxseis.numerics.moment_tensor import MomentTensor
from nmxseis.numerics.moment_tensor.inversion import MTSolution, MTType
from nmxseis.numerics.moment_tensor.plotting import individual_bb_plot

from read_inputs import read_catalog, get_velocity_model

UTC = pytz.utc
timezone = pytz.timezone("America/Edmonton")
latlon_proj = pr.Proj("epsg:4326")
out_proj = pr.Proj("epsg:26711")
water_loading_events = read_catalog("Catalogs//Ptest_events.csv")
inv = read_inventory(r"stations\CV_Full_correct_orientation.xml")
stations = {}
latitudes, longitudes = [], []
for net in inv:
    for station in net:
        channel = inv.select(station=station.code)
        east, north = pr.transform(
            latlon_proj, out_proj, station.longitude, station.latitude
        )
        stations[f"{net.code}.{station.code}."] = {
            "z": station.elevation - channel[0][0][0].depth,
            "e": east,
            "n": north,
        }

# velocity_model = VelocityModel1D([VMLayer1D(**layer) for layer in get_velocity_model()])


def unit_mt_matrix(moment_tensor):
    return moment_tensor.mat / np.linalg.norm(moment_tensor.mat)


def timestamp_to_datetime_string(timestamp):
    return datetime.strftime(timestamp.astimezone(timezone), "%b %d, %H:%M:%S.%f")[:-3]


for event_json in glob(r"waterboarding\MT jsons\*.json"):
    with open(event_json) as f:
        mt_attributes = json.load(f)
    event_id = os.path.basename(event_json).split(".")[0]
    event = [v for v in water_loading_events.values() if v["id"] == event_id][0]

    with open(glob(rf"waterboarding\Final_Picks\{event_id}*.picks")[0]) as f:
        lines = f.readlines()
        num_obs = len(lines)
    print(
        f'Mw{event["magnitude"]:.2f} {datetime.strftime(event["timestamp"].astimezone(timezone),"%b %d, %H:%M:%S.%f")[:-3]}'
    )
    old_moment_tensor = MomentTensor.from_matrix(
        np.array(event["dc MT"])
    ).general_to_dc()
    old_dc_solution = MTSolution(
        MTType.DOUBLE_COUPLE,
        old_moment_tensor,
        condition_number=event["dc CN"],
        pearson_r=event["dc R"],
        rank=6,
        num_obs=num_obs,
        singular_values=[0, 0, 0, 0, 0, 0],
    )

    old_confidence = event["mt confidence"] * 100
    new_moment_tensor_from_json = mt_attributes["eveDict"]["event"]["origin"][
        "custom MT"
    ]["Artemis DC"]
    new_moment_tensor = MomentTensor.from_vector(
        np.array(
            [
                new_moment_tensor_from_json[cc]
                for cc in ["mee", "mnn", "mzz", "men", "mez", "mnz"]
            ]
        )
    )
    new_dc_solution = MTSolution(
        MTType.DOUBLE_COUPLE,
        new_moment_tensor,
        condition_number=new_moment_tensor_from_json["conditionNumber"],
        pearson_r=new_moment_tensor_from_json["pearsonR"],
        rank=6,
        num_obs=new_moment_tensor_from_json["numTraces"],
        singular_values=[0, 0, 0, 0, 0, 0],
    )

    fig, ax = plt.subplots(1, 2, figsize=[8, 4])

    individual_bb_plot(
        old_dc_solution, f"Old\n{old_confidence:.0f}%", ax[0], color="lightsteelblue"
    )
    individual_bb_plot(new_dc_solution, "New", ax[1], color="steelblue")

    fig.savefig(
        f"waterboarding\old_vs_new\comparison_{event_id}.png", bbox_inches="tight"
    )
    new_unit_mt = unit_mt_matrix(new_moment_tensor)
    old_unit_mt = unit_mt_matrix(old_moment_tensor)
    event["similarity_old_to_new"] = np.tensordot(new_unit_mt, old_unit_mt)


qualities = {
    "Nov 27, 05:25:13.484": "A",
    "Nov 27, 05:27:34.664": "A",
    "Nov 27, 05:34:04.736": "B",
    "Nov 27, 05:42:03.252": "A",
    "Nov 27, 05:57:57.800": "B",
    "Nov 27, 05:59:36.536": "C",
    "Nov 27, 06:03:22.236": "A",
    "Nov 27, 06:05:12.564": "A",
    "Nov 27, 06:38:30.352": "B",
    "Nov 27, 07:05:19.420": "B",
    "Nov 27, 07:13:26.956": "B",
    "Nov 27, 08:10:06.632": "A",
    "Nov 27, 08:41:31.880": "C",
    "Nov 27, 10:39:12.156": "A",
    "Nov 27, 11:56:43.260": "A",
    "Nov 27, 13:05:14.224": "B",
    "Nov 27, 13:31:55.948": "A",
    "Nov 27, 14:55:23.388": "A",
    "Nov 27, 19:11:34.416": "B",
    "Nov 27, 20:41:48.052": "A",
}

color = {"A": "royalblue", "B": "darkgoldenrod", "C": "firebrick"}

fig, ax = plt.subplots()
patches = {}
similarity, mw = np.zeros(20), np.zeros(20)
for i_event, (datetime_string, quality) in enumerate(qualities.items()):
    similarity, mw = zip(
        *[
            (e["similarity_old_to_new"], e["magnitude"])
            for e in water_loading_events.values()
            if timestamp_to_datetime_string(e["timestamp"]) == datetime_string
        ]
    )
    p = ax.plot(mw, similarity, "o", color=color[quality], markeredgecolor="0.2")
    if quality not in patches:
        patches[quality] = p[0]

ax.set_xlabel("moment magnitude")
ax.set_ylabel("similarity between old and new mechanisms")
ax.plot([-1.2, -0.9], [0.8, 0.8], "--", color="0.3")
ax.plot([-1.2, -0.9], [-0.8, -0.8], "--", color="0.3")
ax.set_xlim([-1.2, -0.9])
ax.set_ylim([-1, 1])
ax.legend(patches.values(), ["stable", "less stable", "not stable"])
fig.savefig("waterboarding//quality_synopsis.png")

strikes, dips, rakes = [], [], []
p_trends, p_plunges = [], []
b_trends, b_plunges = [], []
t_trends, t_plunges = [], []
for event_json in glob(r"waterboarding\MT jsons\*.json"):
    with open(event_json) as f:
        mt_attributes = json.load(f)
    event_id = os.path.basename(event_json).split(".")[0]
    new_moment_tensor_from_json = mt_attributes["eveDict"]["event"]["origin"][
        "custom MT"
    ]["Artemis DC"]
    new_moment_tensor = MomentTensor.from_vector(
        np.array(
            [
                new_moment_tensor_from_json[cc]
                for cc in ["mee", "mnn", "mzz", "men", "mez", "mnz"]
            ]
        )
    )
    sdr1, sdr2 = new_moment_tensor.to_sdr_pair_dc()
    event = [v for v in water_loading_events.values() if v["id"] == event_id][0]
    if qualities[timestamp_to_datetime_string(event["timestamp"])] == "A":
        strikes.append(sdr1.strike)
        strikes.append(sdr2.strike)
        dips.append(sdr1.dip)
        dips.append(sdr2.dip)
        rakes.append(sdr1.rake)
        rakes.append(sdr2.rake)
        p_trends.append(new_moment_tensor.decompose().p_trend)
        p_plunges.append(new_moment_tensor.decompose().p_plunge)
        b_trends.append(new_moment_tensor.decompose().b_trend)
        b_plunges.append(new_moment_tensor.decompose().b_plunge)
        t_trends.append(new_moment_tensor.decompose().t_trend)
        t_plunges.append(new_moment_tensor.decompose().t_plunge)


fig_rosette = plot_general_sdr_rosettes(strikes, dips, rakes, color="royalblue")
fig_rosette.savefig("waterboarding//stable_waterloading.png")
len(strikes)

fig, ax = mpls.subplots(1, 3, figsize=[16, 5])
for a in ax:
    a.set_azimuth_ticks([])


ax[0].density_contourf(p_plunges, p_trends, measurement="lines")
ax[0].line(p_plunges, p_trends, "k.")
ax[1].density_contourf(b_plunges, b_trends, measurement="lines")
ax[1].line(b_plunges, b_trends, "k.")
ax[2].density_contourf(t_plunges, t_trends, measurement="lines")
ax[2].line(t_plunges, t_trends, "k.")
fig.savefig("waterboarding//pbt_axes.png")
