import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np

from read_inputs import read_wells

wells = read_wells()

wellheads = {
    k: {
        "easting": v["easting"][0],
        "northing": v["northing"][0],
        "elevation": v["elevation"][0],
    }
    for k, v in wells.items()
}


fig, ax = plt.subplots()
ax.set_aspect("equal")
for well, wh in wellheads.items():
    ax.plot(wh["easting"], wh["northing"], "o")
    ax.text(wh["easting"] - 2, wh["northing"] - 2, well)

presumptive5 = {
    "easting": 0.5 * (wellheads["4"]["easting"] + wellheads["6"]["easting"]),
    "northing": 0.5 * (wellheads["4"]["northing"] + wellheads["6"]["northing"]),
}
ax.plot(presumptive5["easting"], presumptive5["northing"], "wo", markeredgecolor="k")


ax.set_xlabel("easting (m)")
ax.set_ylabel("northing (m)")

plt.show()
