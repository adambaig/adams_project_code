from datetime import datetime
import json
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import os
import pytz
from read_inputs import read_treatment
from QI_analysis import pick_breakdown_time

utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")

if os.path.isfile("treatment_data.json"):
    with open("treatment_data.json", "r") as read_file:
        treatment = json.load(read_file)
else:
    treatment = read_treatment()
    with open("treatment_data.json", "w") as write_file:
        json.dump(treatment, write_file)

well_names, stage_names = np.array(
    [
        s.split(":sep:")
        for s in np.unique(
            [v["well"] + ":sep:" + v["stage"] for v in treatment.values()]
        )
    ]
).T

for well, stage in zip(well_names, stage_names):
    fig = plt.figure(figsize=[12, 7])
    ax = fig.add_axes([0.1, 0.1, 0.7, 0.8])
    stage_data = {
        k: v for k, v in treatment.items() if v["well"] == well and v["stage"] == stage
    }
    slurry = np.array([v["slurry_rate"] for v in stage_data.values()]) / 20.0
    proppant = np.array([v["proppant_conc"] for v in stage_data.values()]) / 500.0
    pressure = np.array([v["pressure"] for v in stage_data.values()]) / 100.0
    break_time = pick_breakdown_time(stage_data)
    utc_times = [
        utc.localize(datetime.strptime(t, "%Y%m%d%H%M%S.%f")) for t in stage_data.keys()
    ]
    ax.set_facecolor("lightgrey")

    ax.plot(utc_times, proppant, c="darkgoldenrod", lw=2)
    ax.plot(utc_times, pressure, c="firebrick", lw=2)
    # ax.plot([break_time.datetime, break_time.datetime], [0, 1], "k-")
    ax.set_ylabel("pressure (MPa)", color="firebrick")
    ax.set_yticks(np.linspace(0, 1, 6))
    ax.set_yticklabels(np.linspace(0, 100, 6))
    a2 = ax.twinx()
    a2.plot(utc_times, slurry, c="royalblue", lw=2)
    a2.set_ylabel("slurry rate (m$^3$/min)", color="royalblue")
    a2.set_ylim([0, 1])
    a2.set_yticks(np.linspace(0, 1, 5))
    a2.set_yticklabels(np.linspace(0, 20, 5))

    a4 = fig.add_axes([0.9, 0.1, 0, 0.8])
    a4.yaxis.set_ticks_position("right")
    a4.yaxis.set_label_position("right")
    a4.set_yticks(np.linspace(0, 1, 6))
    a4.set_yticklabels(np.linspace(0, 500, 6))
    a4.set_xticks([])
    a4.set_ylabel("Proppant Concentration (kg/m$^3$)", color="darkgoldenrod")
    ax.set_ylim([0, 1])
    ax.xaxis.set_major_formatter(md.DateFormatter("%b %d\n%H:%M", tz=timezone))
    ax.set_xlabel("local time")
    ax.set_title(f"Well {well} Stage {stage}")
    fig.savefig(f"treatment_data_plots\\Chevron_W{well}_st{stage}.png")
