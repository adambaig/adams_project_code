import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np

D2R = np.pi / 180

tab10 = cm.get_cmap("tab10")
well_color = {
    "1": tab10(0.05),  # blue-ish
    "2": tab10(0.15),  # orange
    "3": tab10(0.25),  # red
    "4": tab10(0.35),  #
    "5": tab10(0.45),
    "6": tab10(0.55),
    "A": tab10(0.65),
    "B": tab10(0.75),
    "C": tab10(0.85),
    "D": tab10(0.95),
}

frac_dimension_file = "frac_dimensions.csv"
f = open(frac_dimension_file)
header = [f.readline() for i in range(3)]
lines = f.readlines()


stage_dimensions = {}
for line in lines:
    lspl = line.split(",")
    well = lspl[0]
    stage = lspl[1]
    if float(stage) < 10:
        stage = "0" + stage
    well_stage_identifier = f"Well {well}: Stage {stage}"
    stage_dimensions[well_stage_identifier] = {
        "well": well,
        "stage": stage,
        "unique well stage id": int(lspl[2]),
        "n_events": lspl[3],
        "half-length": float(lspl[4]),
        "half-width": float(lspl[5]),
        "half-height": float(lspl[6]),
        "azimuth": float(lspl[7]),
        "centroid": {
            "along well": float(lspl[8]),
            "across well": float(lspl[9]),
            "elevation": float(lspl[10]),
        },
    }


fig, (ax_length, ax_width, ax_height) = plt.subplots(3, figsize=[10, 7], sharex=True)
for well in ["1", "2", "3", "4", "5", "6"]:
    stage, half_length, half_width, half_height, azimuth = np.array(
        [
            (
                float(d["stage"]),
                d["half-length"],
                d["half-width"],
                d["half-height"],
                d["azimuth"],
            )
            for d in stage_dimensions.values()
            if d["well"] == well
        ]
    ).T
    ax_length.plot(stage, half_length, color=well_color[well])
    ax_width.plot(stage, half_width, color=well_color[well])
    ax_height.plot(stage, half_height, color=well_color[well])


ax_length.set_ylabel("half-length (m)")
ax_width.set_ylabel("half-width (m)")
ax_height.set_ylabel("half-height (m)")
ax_height.set_xlabel("stage")

fig.savefig("stage_dimentsions.png")


fig_dim_hist, ax_dim_hist = plt.subplots(
    3, 6, figsize=[18, 9], sharex=True, sharey=True
)

for well in ["1", "2", "3", "4", "5", "6"]:
    half_length, half_width, half_height = np.array(
        [
            (
                d["half-length"],
                d["half-width"],
                d["half-height"],
            )
            for d in stage_dimensions.values()
            if d["well"] == well
        ]
    ).T
    bins = np.linspace(0, 400, 20)
    ax_dim_hist[0, int(well) - 1].hist(half_length, bins, facecolor=well_color[well])
    ax_dim_hist[1, int(well) - 1].hist(half_width, bins, facecolor=well_color[well])
    ax_dim_hist[2, int(well) - 1].hist(half_height, bins, facecolor=well_color[well])
    ax_dim_hist[2, int(well) - 1].set_xlabel("distance (m)")
ax_dim_hist[0, 0].set_ylabel("half-length (# of stages)")
ax_dim_hist[1, 0].set_ylabel("half-width (# of stages)")
ax_dim_hist[2, 0].set_ylabel("half-height (# of stages)")
fig_dim_hist.savefig("figures/stage_dimentsion_histograms.png")

fig_rosette = plt.figure(figsize=[12, 8])
for well in ["1", "2", "3", "4", "5", "6"]:
    subaxes = 230 + int(well)
    ax_rosette = fig_rosette.add_subplot(subaxes, projection="polar")
    azimuths_to_180 = np.array(
        [v["azimuth"] for v in stage_dimensions.values() if v["well"] == well]
    )
    azimuths = np.hstack([azimuths_to_180, azimuths_to_180 + 180])
    plotting_azimuths = (90 - azimuths) % 360
    bin_number = 37
    bins = np.linspace(0, 2 * np.pi, bin_number)
    n_az, _, _ = plt.hist(plotting_azimuths * D2R, bins, visible=False)
    width = 2 * np.pi / bin_number
    # bars = ax.bar(bins[:-1], n_rake, width=width, facecolor="royalblue", label='All Events')
    bars = ax_rosette.bar(bins[:-1], n_az, width=width, facecolor=well_color[well])
    lines, labels = plt.thetagrids(
        [0, 90, 180, 270],
        [
            "E",
            "N",
            "W",
            "S",
        ],
    )

fig_rosette.savefig("stage_azimuths.png")
