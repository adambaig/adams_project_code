import numpy as np

from sms_moment_tensor.MT_math import (
    clvd_iso_dc,
    mt_to_sdr,
    decompose_MT,
    mt_vector_to_matrix,
    mt_matrix_to_vector,
)

SQRT2 = np.sqrt(2)
csv = "Example Catalog - Sheet1.csv"
f = open(csv)
head1 = f.readline()
head2 = f.readline()
lines = f.readlines()
f.close()
events = {}
for line in lines:
    lspl = line.split(",")
    id = lspl[0]
    events[id] = {
        "time": lspl[1],
        "easting": float(lspl[2]),
        "northing": float(lspl[3]),
        "tvdss": float(lspl[4]),
        "location uncertainty x": float(lspl[5]),
        "location uncertainty y": float(lspl[6]),
        "location uncertainty z": float(lspl[7]),
        "stack SNR": float(lspl[8]),
        "stack amplitude": float(lspl[9]),
        "moment magnitude": float(lspl[10]),
        "seismic moment": float(lspl[11]),
        "general MT": mt_vector_to_matrix([float(s) for s in lspl[12:18]]),
        "general Pearson R": float(lspl[18]),
        "general condition number": float(lspl[19]),
        "dc MT": mt_vector_to_matrix([float(s) for s in lspl[20:26]]),
        "dc Pearson R": float(lspl[26]),
        "dc condition number": float(lspl[27]),
        "dc confidence": float(lspl[33]),
        "rest": lspl[34:],
    }

g = open("example_catalog_with_MTs.csv", "w")
g.write(head1)
g.write(head2)
for id, event in events.items():
    moment = 10 ** (1.5 * event["moment magnitude"] + 9)
    event["norm general"] = (
        event["general MT"] / np.linalg.norm(event["general MT"]) / SQRT2
    )
    event["norm dc"] = event["dc MT"] / np.linalg.norm(event["dc MT"]) / SQRT2
    clvd, iso, dc = clvd_iso_dc(event["norm general"])
    decomp = decompose_MT(event["norm dc"])
    g.write(id + "," + event["time"] + ",")
    g.write("%.0f,%.0f,%.0f," % (event["easting"], event["northing"], event["tvdss"]))
    g.write(
        "%.0f,%.0f,%.0f,"
        % (
            event["location uncertainty x"],
            event["location uncertainty y"],
            event["location uncertainty z"],
        )
    )
    g.write(
        "%.4e,%.4e,%.3f,%.4e,"
        % (
            event["stack SNR"],
            event["stack amplitude"],
            event["moment magnitude"],
            event["seismic moment"],
        )
    )
    for solution in ["general", "dc"]:
        [
            g.write("%.4e," % mij)
            for mij in mt_matrix_to_vector(moment * event["norm " + solution])
        ]
        g.write(
            "%.4f,%.4f,"
            % (event[solution + " Pearson R"], event[solution + " condition number"])
        )
    g.write("%.1f," % event["dc confidence"])
    g.write("%.1f,%.1f,%.1f," % (dc, clvd, iso))
    for parameter in list(mt_to_sdr(event["norm dc"])):
        print("%.1f," % np.mod(parameter, 360))
    for axis in ["p", "b", "t"]:
        for parameter in ["trend", "plunge"]:
            g.write("%.1f," % np.mod(decomp[axis + "_" + parameter], 360))
    g.write(",".join(event["rest"]))
g.close()
