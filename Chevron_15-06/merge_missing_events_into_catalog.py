import json
import matplotlib.pyplot as plt
import numpy as np
from sklearn.cluster import AgglomerativeClustering

from sms_moment_tensor.MT_math import (
    decompose_MT,
    trend_plunge_to_unit_vector,
    mt_to_sdr,
    clvd_iso_dc,
    mt_matrix_to_vector,
)

from read_inputs import read_missing_events, read_catalog, write_catalog

old_catalog = read_catalog("Catalogs//Chevron_15_06_May8_2020.csv")
missing_events = read_missing_events()

frac_events = {k: v for (k, v) in old_catalog.items() if v["mt confidence"] > 0.9}


def norm_MT(mt):
    return mt / np.linalg.norm(mt)


def moment(event):
    return 10 ** (1.5 * event["magnitude"] + 9)


def scaled_mt(mt, moment_value):
    moment = 10 ** (1.5 * event["magnitude"] + 9)
    return np.sqrt(2) * mt * moment / np.linalg.norm(event["dc MT"])


with open("mt_dot_product_pairs.json", "r") as read_file:
    dot_product_dict = json.load(read_file)
dot_product_pairs = np.array(dot_product_dict["dot_product_pairs"])

distance_mat = abs(1 - abs(dot_product_pairs))
n_clusters = 5
clustering = AgglomerativeClustering(
    n_clusters=n_clusters, affinity="precomputed", linkage="complete"
).fit(distance_mat)
labels = clustering.labels_

cluster_mts = [
    sum(
        mt["dc MT"] / np.linalg.norm(mt["dc MT"])
        for mt in frac_events.values()
        if mt["mt cluster"] == ii
    )
    for ii in range(n_clusters)
]

for event in missing_events.values():
    dot_products = np.zeros(n_clusters)
    for i_cluster in range(n_clusters):
        dot_products[i_cluster] = np.tensordot(
            norm_MT(event["dc MT"]), norm_MT(cluster_mts[i_cluster])
        )
    i_cluster_belongs = np.argmax(abs(dot_products))
    if dot_products[i_cluster_belongs] < 0:
        event["dc MT"] = -event["dc MT"]
        event["gen MT"] = -event["gen MT"]
    event["mt cluster"] = i_cluster_belongs

for event in missing_events.values():
    strike, dip, rake, aux_strike, aux_dip, aux_rake = mt_to_sdr(event["dc MT"])
    decomp_flip = decompose_MT(event["dc MT"])
    clvd, iso, dc = clvd_iso_dc(event["gen MT"])
    event["p_trend"] = decomp_flip["p_trend"]
    event["b_trend"] = decomp_flip["b_trend"]
    event["t_trend"] = decomp_flip["t_trend"]
    event["p_plunge"] = decomp_flip["p_plunge"]
    event["b_plunge"] = decomp_flip["b_plunge"]
    event["t_plunge"] = decomp_flip["t_plunge"]
    event["strike"] = strike
    event["dip"] = dip
    event["rake"] = rake
    event["aux_strike"] = aux_strike
    event["aux_dip"] = aux_dip
    event["aux_rake"] = aux_rake
    event["clvd"] = clvd
    event["iso"] = iso
    event["dc"] = dc
    event["moment"] = moment(event)
    event["dc MT"] = scaled_mt(event["dc MT"], event["moment"])
    event["gen MT"] = scaled_mt(event["gen MT"], event["moment"])
    event["treatment_code"] = "XX"
    event["treatment_counter"] = 0
    event["n_diverters"] = 0


new_catalog = {**old_catalog, **missing_events}

write_catalog(new_catalog, "events_with_missing_included.csv")
