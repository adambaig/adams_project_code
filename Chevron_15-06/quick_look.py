from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import pytz
from read_inputs import read_catalog, read_relocated_events

utc = pytz.utc

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
relocate_events = read_relocated_events("Phase2\\first_relocated_catalog.csv")
window_start = utc.localize(datetime(2019, 12, 12, 0, 0))
window_end = utc.localize(datetime(2019, 12, 12, 10, 0))
window_events = {
    k: v
    for k, v in events.items()
    if v["timestamp"] > window_start and v["timestamp"] < window_end
}
[v["timestamp"] for v in window_events.values()]

plt.hist(
    [
        v["elevation_err"]
        for v in window_events.values()
        if v["well"] in ["1", "2", "3", "4", "5"]
    ]
)

[k for k, v in window_events.items() if v["magnitude"] > -0.8]
[v["timestamp"] for k, v in relocate_events.items()]
t1 = list(events.values())[0]["timestamp"]
t2 = list(relocate_events.values())[0]["timestamp"]
time_tolerance = 0.5
ii = 0
for r_event in relocate_events.values():
    match_events = {
        k: v
        for k, v in window_events.items()
        if abs(v["timestamp"] - r_event["timestamp"]).total_seconds() < time_tolerance
    }
    if len(match_events) == 1:
        ii += 1
        print("unique event found")
    if len(match_events) > 1:
        print("more than one event found")
