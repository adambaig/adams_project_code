import os
import sys
import logging

import numpy as np

from PyQt5 import QtWidgets, QtCore, QtGui

from NocTools.PFI_MT_QC.ui.main import Ui_MainWindow


class MTQC(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MTQC, self).__init__(parent)
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)

        self.readPrevDir()

        self.defaultStyle = self.yesBut1.styleSheet()

        self.dirEdit.returnPressed.connect(
            lambda: self.loadImageFiles(self.dirEdit.text())
        )
        self.fileList.itemClicked.connect(
            lambda: self.displayImage(self.dirPath, self.fileList.currentItem().text())
        )

        QtWidgets.QShortcut(QtGui.QKeySequence("up"), self, self.yesButtonPressed)
        self.yesBut1.released.connect(self.yesButtonPressed)
        self.yesBut2.released.connect(self.yesButtonPressed)

        QtWidgets.QShortcut(QtGui.QKeySequence("down"), self, self.noButtonPressed)
        self.noBut1.released.connect(self.noButtonPressed)
        self.noBut2.released.connect(self.noButtonPressed)

        QtWidgets.QShortcut(QtGui.QKeySequence("right"), self, self.nextButtonPressed)
        self.nextBut.released.connect(self.nextButtonPressed)

        QtWidgets.QShortcut(QtGui.QKeySequence("left"), self, self.prevButtonPressed)
        self.prevBut.released.connect(self.prevButtonPressed)

        QtWidgets.QShortcut(QtGui.QKeySequence("delete"), self, self.clearButtonPressed)
        self.clearBut.released.connect(self.clearButtonPressed)

        QtWidgets.QShortcut(QtGui.QKeySequence("Ctrl+s"), self, self.saveButPressed)
        self.saveBut.released.connect(self.saveButPressed)

        QtWidgets.QShortcut(QtGui.QKeySequence("F1"), self, self.displayResults)

        self.yesBut1.setEnabled(False)
        self.yesBut2.setEnabled(False)
        self.noBut1.setEnabled(False)
        self.noBut2.setEnabled(False)

    def readPrevDir(self):
        userdata = self.getUserData()
        if "prevDir" in userdata.keys():
            prevDir = userdata["prevDir"]
            self.dirEdit.setText(prevDir)
            self.loadImageFiles(prevDir)

    def loadImageFiles(self, dirPath):
        if not os.path.isdir(dirPath):
            print("Directory does not exist")
            return
        self.fileList.clear()
        fileList = [f for f in os.listdir(dirPath) if f.endswith(".png")]
        self.results = {}
        for afile in fileList:
            self.fileList.addItem(afile)
            self.results[afile] = {"q1": None, "q2": None}
        self.dirPath = dirPath
        userdata = self.getUserData()
        userdata["prevDir"] = dirPath
        self.writeUserData(userdata)

    def displayImage(self, dirPath, imageFile):
        self.currentImage = imageFile
        fullPath = os.path.join(dirPath, imageFile)
        self.imageSpace.setPixmap(QtGui.QPixmap(fullPath))
        result = self.results[imageFile]
        self.yesBut1.setStyleSheet(self.defaultStyle)
        self.noBut1.setStyleSheet(self.defaultStyle)
        self.yesBut2.setStyleSheet(self.defaultStyle)
        self.noBut2.setStyleSheet(self.defaultStyle)
        if result["q1"] is None:
            self.yesBut1.setEnabled(True)
            self.noBut1.setEnabled(True)
            self.yesBut2.setEnabled(False)
            self.noBut2.setEnabled(False)
            return
        else:
            if result["q1"]:
                self.yesBut1.setStyleSheet("background-color: green")
                self.yesBut2.setEnabled(True)
                self.noBut2.setEnabled(True)
            else:
                self.noBut1.setStyleSheet("background-color: red")
                self.yesBut2.setEnabled(False)
                self.noBut2.setEnabled(False)
            self.yesBut1.setEnabled(False)
            self.noBut1.setEnabled(False)
        if result["q2"] is None:
            return
        else:
            if result["q2"]:
                self.yesBut2.setStyleSheet("background-color: green")
            else:
                self.noBut2.setStyleSheet("background-color: red")
            self.yesBut2.setEnabled(False)
            self.noBut2.setEnabled(False)

    def yesButtonPressed(self):
        result = self.results[self.currentImage]
        if result["q1"] is None:
            q = 1
        elif not result["q1"]:
            return
        elif result["q2"] is None:
            q = 2
        else:
            return
        if q == 1:
            self.results[self.currentImage]["q1"] = True
            self.yesBut1.setStyleSheet("background-color: green")
            self.yesBut1.setEnabled(False)
            self.noBut1.setEnabled(False)

            self.yesBut2.setEnabled(True)
            self.noBut2.setEnabled(True)
        if q == 2:
            self.results[self.currentImage]["q2"] = True
            self.yesBut2.setStyleSheet("background-color: green")
            self.yesBut2.setEnabled(False)
            self.noBut2.setEnabled(False)
        self.updateListColors()

    def noButtonPressed(self):
        result = self.results[self.currentImage]
        if result["q1"] is None:
            q = 1
        elif not result["q1"]:
            return
        elif result["q2"] is None:
            q = 2
        else:
            return
        if q == 1:
            self.results[self.currentImage]["q1"] = False
            self.noBut1.setStyleSheet("background-color: red")
            self.yesBut1.setEnabled(False)
            self.noBut1.setEnabled(False)
        if q == 2:
            self.results[self.currentImage]["q2"] = False
            self.noBut2.setStyleSheet("background-color: red")
            self.yesBut2.setEnabled(False)
            self.noBut2.setEnabled(False)
        self.updateListColors()

    def nextButtonPressed(self):
        itemIndex = self.fileList.currentRow()
        if itemIndex + 1 > self.fileList.count() - 1:
            return
        self.fileList.setCurrentRow(itemIndex + 1)
        self.currentImage = self.fileList.currentItem().text()
        self.displayImage(self.dirPath, self.currentImage)
        self.updateListColors()

    def prevButtonPressed(self):
        itemIndex = self.fileList.currentRow()
        if itemIndex - 1 < 0:
            return
        self.fileList.setCurrentRow(itemIndex - 1)
        self.currentImage = self.fileList.currentItem().text()
        self.displayImage(self.dirPath, self.currentImage)
        self.updateListColors()

    def clearButtonPressed(self):
        self.results[self.currentImage] = {"q1": None, "q2": None}
        self.displayImage(self.dirPath, self.currentImage)
        self.updateListColors()

    def displayResults(self):
        for key, val in self.results.items():
            print(key, val)

    def saveButPressed(self):
        results = self.results
        resultKeys = sorted(list(results.keys()))
        output = []
        for akey in resultKeys:
            output.append([akey, str(results[akey]["q1"]), str(results[akey]["q2"])])
        np.savetxt(
            os.path.join(self.dirPath, "results.csv"),
            np.array(output),
            delimiter=",",
            fmt="%s",
        )

    def updateListColors(self):
        for ii in range(self.fileList.count()):
            key = self.fileList.item(ii).text()
            q1 = self.results[key]["q1"]
            q2 = self.results[key]["q2"]
            if q1 is None and q2 is None:
                continue
            if not q1:
                self.fileList.item(ii).setBackground(QtGui.QColor("#43f536"))
                continue
            if q1 and q2 is None:
                self.fileList.item(ii).setBackground(QtGui.QColor("#3bbcf7"))
                continue
            if q1 and q2 is not None:
                self.fileList.item(ii).setBackground(QtGui.QColor("#43f536"))
                continue

    ## USER DATA ##
    def getUserData(self):
        userdata = {}
        if os.path.isfile("./bin/userdata"):
            data = np.genfromtxt("./bin/userdata", delimiter=",", dtype=str)
            if data.ndim == 1:
                data = data.reshape(1, 2)
            for dat in data:
                userdata[dat[0]] = dat[1]
        return userdata

    def writeUserData(self, userdata):
        writeArray = []
        for key in userdata.keys():
            writeArray.append([key, userdata[key]])
        np.savetxt("./bin/userdata", np.array(writeArray), delimiter=",", fmt="%s")


# Class for logging
class QtHandler(logging.Handler):
    def __init__(self):
        logging.Handler.__init__(self)

    def emit(self, record):
        record = self.format(record)
        if record:
            XStream.stdout().write("%s\n" % record)


class XStream(QtCore.QObject):
    _stdout = None
    _stderr = None
    setTextCursor = QtCore.pyqtSignal()
    messageWritten = QtCore.pyqtSignal(str)

    def flush(self):
        pass

    def fileno(self):
        return -1

    def write(self, msg):
        if not self.signalsBlocked():
            self.setTextCursor.emit()
            self.messageWritten.emit(str(msg))

    @staticmethod
    def stdout():
        if not XStream._stdout:
            XStream._stdout = XStream()
            sys.stdout = XStream._stdout
        return XStream._stdout

    @staticmethod
    def stderr():
        if not XStream._stderr:
            XStream._stderr = XStream()
            sys.stderr = XStream._stderr
        return XStream._stderr


def GuiExcepthook(aType, value, tback):
    # log the exception here
    ## FILL ##
    # then call the default handler
    sys.__excepthook__(aType, value, tback)


def main():
    # Allow errors to propogate without crashing
    sys.excepthook = GuiExcepthook
    # For sending stdout to the trace log
    logger = logging.getLogger(__name__)
    handler = QtHandler()
    handler.setFormatter(logging.Formatter("%(levelname)s: %(message)s"))
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    app = QtWidgets.QApplication(sys.argv)
    w = MTQC()
    w.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
