import os
import numpy as np
import matplotlib.pyplot as plt
from obspy.imaging.beachball import beach

from DataManagement.Summary import getDataInfo


baseDir = r"D:\Nanometrics\Chevron\15_06_PFI_20200206\MT_QC\ImageDirectories"
staFile = r"D:\Nanometrics\Chevron\15_06_PFI_20200206\stations\postStackStations.csv"
finalCatalog = r"D:\Nanometrics\Chevron\15_06_PFI_20200206\Catalogs\FinalCatalogBuilding\Part2\Chevron_15-06_May11_2020.csv"
outputDir = r"D:\Nanometrics\Chevron\15_06_PFI_20200206\MT_QC\plots"

if not os.path.isdir(outputDir):
    os.makedirs(outputDir)

finalCatData, headers = getDataInfo(finalCatalog)
finalCatData = finalCatData.astype(str)
stations = np.genfromtxt(staFile, delimiter=",", dtype=str)
subDirs = os.listdir(baseDir)
for subDir in subDirs:
    resultFile = os.path.join(baseDir, subDir, "results.csv")
    data = np.genfromtxt(resultFile, delimiter=",", dtype=str)

    minX, maxX = np.min(stations[:, 1].astype(float)), np.max(
        stations[:, 1].astype(float)
    )
    minY, maxY = np.min(stations[:, 2].astype(float)), np.max(
        stations[:, 2].astype(float)
    )
    width = max([maxX - minX, maxY - minY])

    output = []
    for entry in data:
        dataStation = entry[0].replace(".png", "")
        ind = np.where(stations[:, 0] == dataStation)[0]
        if len(ind) > 1:
            print("wrong")
            quit()
        if entry[1] == "True" and entry[2] == "True":
            color = "g"
        elif entry[1] == "True" and entry[2] == "False":
            color = "r"
        elif entry[1] == "False":
            color = "k"
        output.append([dataStation, stations[ind[0], 1], stations[ind[0], 2], color])

    eventId = os.path.basename(os.path.dirname(resultFile))
    ind = np.where(finalCatData[:, headers.index("Event ID")] == eventId)[0][0]
    mt = np.array(
        [
            finalCatData[ind, headers.index("Strike 1")],
            finalCatData[ind, headers.index("Dip 1")],
            finalCatData[ind, headers.index("Rake 1")],
        ]
    ).astype(float)
    x, y = float(finalCatData[ind, headers.index("Easting")]), float(
        finalCatData[ind, headers.index("Northing")]
    )
    bb = beach(
        mt, xy=(x, y), width=width, facecolor="w", alpha=1.0, linewidth=1, nofill=True
    )
    Mw = finalCatData[ind, headers.index("Moment Magnitude")]
    snr = finalCatData[ind, headers.index("Stack Signal to Noise")]
    conf = finalCatData[ind, headers.index("Confidence")]

    plt.figure(figsize=(8, 8))
    # plt.gca().add_collection(bb)
    plt.plot(x, y, color="orange", marker="*", markersize=10)
    for entry in output:
        plt.plot(float(entry[1]), float(entry[2]), entry[3] + "o")
        plt.text(float(entry[1]), float(entry[2]), entry[0], fontsize=8)
    output = np.array(output)
    good = np.where(output[:, 3] == "g")[0]
    bad = np.where(output[:, 3] == "r")[0]
    cantSee = np.where(output[:, 3] == "k")[0]
    length = len(output)
    plt.title(
        "Event: "
        + eventId
        + "\nMw: "
        + Mw
        + "  SNR: "
        + str(float(snr))
        + "  MT Confidence: "
        + conf
        + "\nGood: "
        + str(round((len(good) / length) * 100, 2))
        + "%  Bad: "
        + str(round((len(bad) / length) * 100, 2))
        + "%"
    )
    plt.grid()
    plt.xlabel("Easting")
    plt.ylabel("Northing")
    plt.xlim(minX - 1000, maxX + 1000)
    plt.ylim(minY - 1000, maxY + 1000)
    plt.gca().set_aspect("equal")
    plt.savefig(os.path.join(outputDir, eventId + ".png"))
    plt.close()
