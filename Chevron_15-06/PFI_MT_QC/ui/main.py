# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\AaronBooterbaugh\Desktop\NOC_GUIs\LogiklystPSM\oilandgas_operations\src\NocTools\PFI_MT_QC\ui\main.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1029, 804)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.centralwidget.sizePolicy().hasHeightForWidth()
        )
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.q1Label = QtWidgets.QLabel(self.centralwidget)
        self.q1Label.setObjectName("q1Label")
        self.gridLayout.addWidget(self.q1Label, 0, 0, 1, 1)
        self.clearBut = QtWidgets.QPushButton(self.centralwidget)
        self.clearBut.setObjectName("clearBut")
        self.gridLayout.addWidget(self.clearBut, 0, 5, 1, 1)
        self.dirEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.dirEdit.setObjectName("dirEdit")
        self.gridLayout.addWidget(self.dirEdit, 2, 0, 1, 1)
        self.yesBut2 = QtWidgets.QPushButton(self.centralwidget)
        self.yesBut2.setObjectName("yesBut2")
        self.gridLayout.addWidget(self.yesBut2, 1, 1, 1, 1)
        self.q2Label = QtWidgets.QLabel(self.centralwidget)
        self.q2Label.setObjectName("q2Label")
        self.gridLayout.addWidget(self.q2Label, 1, 0, 1, 1)
        self.noBut2 = QtWidgets.QPushButton(self.centralwidget)
        self.noBut2.setObjectName("noBut2")
        self.gridLayout.addWidget(self.noBut2, 1, 2, 1, 1)
        self.fileList = QtWidgets.QListWidget(self.centralwidget)
        self.fileList.setObjectName("fileList")
        self.gridLayout.addWidget(self.fileList, 3, 0, 1, 1)
        self.yesBut1 = QtWidgets.QPushButton(self.centralwidget)
        self.yesBut1.setObjectName("yesBut1")
        self.gridLayout.addWidget(self.yesBut1, 0, 1, 1, 1)
        self.saveBut = QtWidgets.QPushButton(self.centralwidget)
        self.saveBut.setObjectName("saveBut")
        self.gridLayout.addWidget(self.saveBut, 0, 6, 1, 1)
        self.noBut1 = QtWidgets.QPushButton(self.centralwidget)
        self.noBut1.setObjectName("noBut1")
        self.gridLayout.addWidget(self.noBut1, 0, 2, 1, 1)
        self.nextBut = QtWidgets.QPushButton(self.centralwidget)
        self.nextBut.setObjectName("nextBut")
        self.gridLayout.addWidget(self.nextBut, 0, 4, 1, 1)
        self.imageSpace = QtWidgets.QLabel(self.centralwidget)
        self.imageSpace.setText("")
        self.imageSpace.setObjectName("imageSpace")
        self.gridLayout.addWidget(self.imageSpace, 2, 1, 3, 6)
        self.prevBut = QtWidgets.QPushButton(self.centralwidget)
        self.prevBut.setObjectName("prevBut")
        self.gridLayout.addWidget(self.prevBut, 0, 3, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.q1Label.setText(
            _translate("MainWindow", "Is the first break clearly visible?")
        )
        self.clearBut.setText(_translate("MainWindow", "Clear Current Image Results"))
        self.yesBut2.setText(_translate("MainWindow", "Yes (up)"))
        self.q2Label.setText(
            _translate(
                "MainWindow", "Does the visible first break match the synthetic?"
            )
        )
        self.noBut2.setText(_translate("MainWindow", "No (down)"))
        self.yesBut1.setText(_translate("MainWindow", "Yes (up)"))
        self.saveBut.setText(_translate("MainWindow", "Save Results"))
        self.noBut1.setText(_translate("MainWindow", "No (down)"))
        self.nextBut.setText(_translate("MainWindow", "Next (forward)"))
        self.prevBut.setText(_translate("MainWindow", "Previous (back)"))
