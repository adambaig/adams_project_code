# import matplotlib
#
# matplotlib.use("Qt5Agg")

import json
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
import pytz
from read_inputs import (
    read_events,
    get_velocity_model,
    read_wells,
    read_treatment,
)

from plot_stuff import gray_background_with_grid
from QI_analysis import (
    rotate_from_cardinal,
    calc_well_trend,
    pca_plan_view_trend,
    find_perf_center,
)

wells = read_wells(shift_5=True)
events = read_events()
wellhead_center = {
    "easting": np.average([well["easting"][0] for well in wells.values()]),
    "northing": np.average([well["northing"][0] for well in wells.values()]),
    "elevation": np.average([well["elevation"][0] for well in wells.values()]),
}


def rotate_perf_clusters(clusters, angle_rad, center):
    R = np.array(
        [
            [np.cos(angle_rad), -np.sin(angle_rad)],
            [np.sin(angle_rad), np.cos(angle_rad)],
        ]
    )
    for cluster in clusters.values():
        relative_easting = (
            0.5 * (cluster["top_east"] + cluster["bottom_east"]) - center["easting"]
        )
        relative_northing = (
            0.5 * (cluster["top_north"] + cluster["bottom_north"]) - center["northing"]
        )
        perp, along = R @ [relative_easting, relative_northing]
        cluster["along trend"] = along
        cluster["perp to trend"] = perp
    return clusters


tab10 = cm.get_cmap("tab10")

for well_id, well in {
    k: v for k, v in wells.items() if k in ["1", "2", "3", "4", "5", "6"]
}.items():
    well_trend = calc_well_trend(well)
    well_events = {k: v for k, v in events.items() if v["well"] == well_id}
    for stage in np.unique([v["stage"] for v in well_events.values()]):
        stage_events = {k: v for k, v in well_events.items() if v["stage"] == stage}
        perf_center = find_perf_center(stage.zfill(2), well)
        rotate_from_cardinal(stage_events, well_trend, perf_center)
        rotate_from_cardinal(wells, well_trend, perf_center)
        rotated_perfs = rotate_perf_clusters(
            well[f"Stage {stage.zfill(2)}"], well_trend, perf_center
        )
        mod_stage = np.mod(int(stage), 10)
        stage_color = tab10((mod_stage + 0.1) / 10)
        fig, ax = plt.subplots(figsize=[16, 16])
        ax.set_aspect("equal")
        ax.plot(
            [v["perp to trend"] for v in stage_events.values()],
            [v["along trend"] for v in stage_events.values()],
            "o",
            c=stage_color,
            markeredgecolor="k",
            linewidth=0.25,
            zorder=3,
            ms=10,
            alpha=0.8,
        )
        ax.plot(
            [v["perp to trend"] for v in rotated_perfs.values()],
            [v["along trend"] for v in rotated_perfs.values()],
            marker=(7, 1, 0),
            c=stage_color,
            ms=16,
            markeredgecolor="k",
            linewidth=0.25,
            zorder=2,
        )
        x1, x2 = ax.get_xlim()
        y1, y2 = ax.get_ylim()
        ax.plot(
            wells[well_id]["perp to trend"],
            wells[well_id]["along trend"],
            lw=4,
            c="0.7",
            zorder=0,
        )
        ax.plot(
            wells[well_id]["perp to trend"],
            wells[well_id]["along trend"],
            lw=5,
            c="0.3",
            zorder=-1,
        )
        for well2 in wells.values():
            ax.plot(
                well2["perp to trend"], well2["along trend"], lw=2, c="0.8", zorder=-2
            )
            ax.plot(
                well2["perp to trend"], well2["along trend"], lw=2.5, c="0.4", zorder=-3
            )
        ax.set_xlim([x1, x2])
        ax.set_ylim([y1, y2])
        ax = gray_background_with_grid(ax, grid_spacing=50)
        ax.set_title(f"Well {well_id} Stage {int(stage)}")

        fig.savefig(f"figures//events_well_{well_id}_stage_{stage.zfill(2)}.png")
        plt.close(fig)
