# -*- coding: utf-8 -*-
"""
Script to build the StationXML files for the Chevron PFI project.

This project has a slightly complicated instrument arrangement
consisting of downhole and surface geophones.  From Ben:

    Geophone sensitivities are:

        Downhole uses GS-ones, these have a sensitivity of 78.7 V/m/s.
        Surface uses GS-32CT in a string of 6 connected in series: 118.2 V/m/s (each individual phone is 19.7)

Created on Mon Apr 20 10:51:34 2020

@author: IanPritchard
"""

import os

import numpy as np
import matplotlib.pyplot as plt
import obspy
from obspy.signal.invsim import paz_to_freq_resp, corn_freq_2_paz
from obspy.core.inventory.response import Response
from obspy.core import UTCDateTime
from obspy.core.inventory import Inventory, Network, Station, Channel, Site
import pandas as pd
import pyproj

#%% Build poles and zeros
### Downhole sensors have different sensitivity than surface, but same PAZ
downholePaz = corn_freq_2_paz(
    10.0, damp=0.7
)  # this generates the poles + zeros below, but not the sensitivity
surfacePaz = corn_freq_2_paz(10.0, damp=0.7)  # both are 10 Hz sensors with 70% damping

downholePaz["sensitivity"] = 78700.0  # take GeoSpace value, multiply x1000 for mV --> V
surfacePaz[
    "sensitivity"
] = 118118.6  # take GeoSpace value, multiply by 6 (for 6x1 arrangement), then x1000 for mV --> V

poles = downholePaz["poles"]
zeros = downholePaz["zeros"]

downhole_scale_fac = downholePaz["sensitivity"]
surface_scale_fac = surfacePaz["sensitivity"]

sRate = 250.0

#%% Build Response Object

respDict = {}
stage_gain_frequency = 1.0
input_units = "M/S"
output_units = "VOLTS"
normalization_frequency = 10.0
pz_transfer_function_type = "LAPLACE (RADIANS/SECOND)"
normalization_factor = 1.0

for instrument, scale_fac in zip(
    ["surface", "downhole"], [surface_scale_fac, downhole_scale_fac]
):
    respDict[instrument] = Response.from_paz(
        zeros=zeros,
        poles=poles,
        stage_gain=scale_fac,
        stage_gain_frequency=10.0,
        input_units=input_units,
        output_units=output_units,
    )


for resp in respDict.values():
    resp.plot(1.0, sampling_rate=sRate, plot_degrees=True)


#%% Build XMLs
# EPSG: 26711, NAD27 / UTM zone 11N

wgs84 = pyproj.CRS("EPSG:4326")
localCRS = pyproj.CRS("EPSG:26711")

stnListFile = r"C:\Users\adambaig\Project\Chevron_15-06\stations\postStackStations.csv"
stnDf = pd.read_csv(stnListFile)

xmlOutDir = r"C:\Users\adambaig\Project\Chevron_15-06\stations"

netCode = "CV"
locCode = "VC"
chanCode = "DLZ"

stnLocs = {}
for ind, row in stnDf.iterrows():
    stnCode = row["#id"]
    NSLC = ".".join([netCode, stnCode, locCode, chanCode])
    x = row.Easting
    y = row.Northing
    z = -1.0 * row.Z  # Elevations in stnListFile are -'ve going up
    lat, lon, elev = pyproj.transform(localCRS, wgs84, x, y, z)
    stnLocs[NSLC] = [lat, lon, elev]


invDict = {"Full": None, "Light": None}

for key in invDict:

    newInv = Inventory(networks=[], source="Nanometrics")
    net = Network(
        code="CV",
        stations=[],
        description="Spectraseis Network",
        start_date=UTCDateTime(2016, 3, 1),
    )

    for nslc in stnLocs:

        nslcSplit = nslc.split(".")

        lon = stnLocs[nslc][1]
        lat = stnLocs[nslc][0]
        elev = stnLocs[nslc][2]

        sta = Station(
            code=str(nslcSplit[1]),
            latitude=lat,
            longitude=lon,
            elevation=elev,
            creation_date=UTCDateTime(2020, 1, 1),
            site=Site(name=str(nslcSplit[1])),
        )

        az = 0.0
        if nslcSplit[1][-1] == "4":
            dip = 90
        else:
            dip = -90.0

        chan = Channel(
            chanCode,
            location_code=locCode,
            latitude=lat,
            longitude=lon,
            elevation=elev,
            depth=0.0,
            azimuth=az,
            dip=dip,
            sample_rate=sRate,
        )

        if key == "Full":

            if nslcSplit[1][0] == "S":

                chan.response = respDict["surface"]

            else:

                chan.response = respDict["downhole"]

        sta.channels.append(chan)

        net.stations.append(sta)

    newInv.networks.append(net)

    invDict[key] = newInv


for xmlType in invDict:

    invDict[xmlType].write(
        os.path.join(xmlOutDir, "CV_" + xmlType + ".xml"), format="STATIONXML"
    )
