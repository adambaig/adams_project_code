import matplotlib.pyplot as plt
import numpy as np
from obspy import UTCDateTime
from obspy.imaging.mopad_wrapper import beach
import os
import pickle
import re
from six import iteritems
import time

from ExternalPrograms.FocMec.FocMecSupport import runFocMec
from General.FileTransfer import createDir
from General.Projections import calcAziViaLonLat

from sms_moment_tensor.moment_tensor_inversion import (
    inversion_matrix_row,
    back_project_amplitude,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import average_MTs, mt_to_sdr
from sms_moment_tensor.plotting import (
    plot_regression,
    plot_beachballs,
    plot_fault_planes_from_Focmec,
    plot_beachball_from_sdr,
)

from sms_ray_modelling.raytrace import isotropic_ray_trace, takeoff_angle_Focmec

from read_inputs import read_catalog, read_stations, get_velocity_model

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
stations = read_stations()

R2D = 180 / np.pi


def formFocMecInputFile(mainDir, sourceTag, eveID, ampDict, pTakeoff, aziDict):
    inputPath = "%s/%s/FocMec/%s.inp" % (mainDir, sourceTag, eveID)
    createDir(os.path.dirname(inputPath))
    # The [positive (peak), negative (trough)] sense codes, relative to channel orientation
    senseLookup = ["C", "D"]
    with open(inputPath, "w") as aFile:
        # Give header to file
        aFile.write("eventID:%s, runAtTime:%s\n" % (eveID, UTCDateTime(time.time())))
        # Loop through polarity pick observations
        for nsl, amp in ampDict.items():
            if amp > 0:
                sense = senseLookup[0]
            elif amp < 0:
                sense = senseLookup[1]
            else:
                continue
            takeOff = pTakeoff[nsl]
            staTag = nsl[:4]
            aFile.write(
                "{:4}{:8.2f}{:8.2f}{:1}\n".format(staTag, aziDict[nsl], takeOff, sense)
            )
    return inputPath


f = open("mt-qc//refixed_catalog_of_first_motions.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
amp_qc = {}
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[1:]:
        amp_qc[lspl[0]] = {k: v for v, k in zip(lspl[1:], headsplit[1:])}

theoretical_polarities = {}
f = open("mt-qc\\theoretical_polarities_alancienne.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[:1]:
        theoretical_polarities[lspl[0]] = {
            k: float(v) for v, k in zip(lspl[1:], headsplit[1:])
        }


absolute_polarities = {}
f = open("mt-qc//Adam_polarity_picks_transpose_superstation.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
polarity_map = {"u": -1, "d": 1, "0": 0}
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[:1]:
        absolute_polarities[lspl[0]] = {
            k: polarity_map[v.strip()] for v, k in zip(lspl[1:], headsplit[1:])
        }


def getVelfromTTT(TTT):
    velocity_model = []
    top = -1 * TTT["originz"]
    d1 = TTT["dz"]
    vel = TTT["vel1D"]
    for vp in vel:
        vs = vp / 1.73205080757
        rho = 310 * (vp**0.25)
        curlayer = {"vp": vp, "top": top, "vs": vs, "rho": rho}
        top = top - d1
        velocity_model.append(curlayer)
    return velocity_model


pickle_TTT = open("optimal-TTT.pkl", "rb")
TTT = pickle.load(pickle_TTT, encoding="latin1")
pickle_TTT.close()
velocity_model = getVelfromTTT(TTT)
velocity_model = get_velocity_model()

for event_id, event in {
    event["id"]: event for event in events.values() if event["magnitude"] > 0.2
}.items():
    source = {"e": event["easting"], "n": event["northing"], "z": event["elevation"]}

    f = open(f"mt-qc//pickAmps//{event_id}.csv")
    amps = {
        line.split(",")[0]: float(line.split(",")[1])
        for line in f.readlines()[1:]
        if line.split(",")[0][0] == "S"
    }
    f.close()

    adjusted_amps = {
        station: abs(amps[station]) * absolute_polarities[event_id][station]
        for station in amps.keys()
    }
    if len(amps) > 6:
        right_hand_side = []
        A_matrix = []
        p_raypaths, s_raypaths = {}, {}
        for i_station, (station, amp) in enumerate(adjusted_amps.items()):
            if amp != 0:
                p_raypaths[station] = isotropic_ray_trace(
                    source, stations[station], velocity_model, "P"
                )
                s_raypaths[station] = isotropic_ray_trace(
                    source, stations[station], velocity_model, "S"
                )
                cos_incoming = np.sqrt(
                    1.0
                    - (
                        p_raypaths[station]["hrz_slowness"]["e"] ** 2
                        + p_raypaths[station]["hrz_slowness"]["n"] ** 2
                    )
                    * p_raypaths[station]["velocity_model_chunk"][-1]["v"] ** 2
                )
                right_hand_side.append(
                    back_project_amplitude(
                        p_raypaths[station],
                        s_raypaths[station],
                        adjusted_amps[station],
                        "P",
                        Q=60,
                    )
                    / cos_incoming
                )
                A_matrix.append(inversion_matrix_row(p_raypaths[station], "P"))
        pTakeoff = {
            key: takeoff_angle_Focmec(rayPath) for key, rayPath in iteritems(p_raypaths)
        }
        sTakeoff = {
            key: takeoff_angle_Focmec(rayPath) for key, rayPath in iteritems(s_raypaths)
        }
        aziDict = {
            station: (
                R2D
                * np.arctan2(
                    stations[station]["e"] - event["easting"],
                    stations[station]["n"] - event["northing"],
                )
            )
            % 360
            for station in pTakeoff.keys()
        }

        mt_dictionary = solve_moment_tensor(A_matrix, right_hand_side)
        mt_dc = mt_dictionary["dc"]
        mt_dictionary = solve_moment_tensor(A_matrix, right_hand_side)
        fig = plt.figure(figsize=[16, 9])
        NSLCs = list(k + "..P" for k in pTakeoff.keys())
        axDCRegression = fig.add_subplot(2, 3, 1)
        axDc = fig.add_subplot(2, 3, 4)
        axGeneralRegression = fig.add_subplot(2, 3, 2)
        axGen = fig.add_subplot(2, 3, 5)
        axFocMecAll = fig.add_subplot(2, 3, 3, projection="stereonet")
        axFocMecSelect = fig.add_subplot(2, 3, 6)
        focmec_input = formFocMecInputFile(
            "mt-qc", ".", event_id, adjusted_amps, pTakeoff, aziDict
        )
        sdr_list, focmec_output = runFocMec(focmec_input, maxPolErr=18)
        plot_regression(
            right_hand_side,
            mt_dictionary["dc"],
            A_matrix,
            axDCRegression,
            fig=fig,
            nslc_tags=NSLCs,
        )
        plot_regression(
            right_hand_side,
            mt_dictionary["general"],
            A_matrix,
            axGeneralRegression,
            fig=fig,
            nslc_tags=NSLCs,
        )
        plot_beachballs(mt_dictionary, axDc, axGen)
        plot_fault_planes_from_Focmec(focmec_input, focmec_output, axFocMecAll, fig)
        fps_mt = average_MTs(sdr_list)
        (
            fps_strike1,
            fps_dip1,
            fps_rake1,
            fps_strike2,
            fps_dip2,
            fps_rake2,
        ) = mt_to_sdr(fps_mt)
        plot_beachball_from_sdr(
            (fps_strike1 + 360) % 360.0, fps_dip1, fps_rake1, axFocMecSelect, fig=fig
        )

        fig.text(0.5, 0.95, event_id, ha="center")
        fig.savefig(f"mt-qc//qc_plots_{event_id}_superstation.png")

        f_phase1, a_phase1 = plt.subplots()
        a_phase1.set_aspect("equal")
        phase1_bb = beach(
            (event["strike"], event["dip"], event["rake"]),
            width=1.8,
            xy=(1, 1),
            facecolor="indigo",
        )
        a_phase1.add_collection(phase1_bb)
        a_phase1.set_xlim([0, 2])
        a_phase1.set_ylim([0, 2])
        a_phase1.axis("off")
        f_phase1.savefig(f"mt-qc//phase1_bb_{event_id}.png")
