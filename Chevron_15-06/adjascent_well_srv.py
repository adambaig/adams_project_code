from copy import deepcopy
from datetime import datetime, timedelta
from matplotlib import cm, colors
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import os
import pytz
from scipy.ndimage import zoom

from pfi_qi.QI_analysis import event_density_grid, output_isosurface

from read_inputs import (
    read_catalog,
    get_velocity_model,
    read_wells,
    read_treatment,
    read_or_load_json,
    read_strain,
)
from plotting_stuff import (
    rt_with_treatment,
    plot_rotated_stage,
    plot_stage,
    add_dimension_arrows,
    gray_background_with_grid,
)

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")
method = "event density"
events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
wells = read_wells()
frac_events = {k: v for k, v in events.items() if v["stage"] != "-1"}
events_no_is = {k: v for k, v in frac_events.items() if v["treatment_code"] != "IS"}
complete_events = {k: v for k, v in frac_events.items() if v["magnitude"] >= -1.25}

event_density_dir = "event_density//fields//"


grid_spacing = 40
min_neighbours = 5
max_radius = 80
all_well_strain_file = f"{event_density_dir}all.csv"
if os.path.exists(all_well_strain_file):
    density_flat, grid_points = read_strain(all_well_strain_file)
e_grid = np.unique(grid_points[:, 0])
n_grid = np.unique(grid_points[:, 1])
z_grid = np.unique(grid_points[:, 2])
n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)
density = np.reshape(density_flat, [n_e, n_n, n_z])

density_by_well = {}
volume_by_well = {}
for well in ["1", "2", "3", "4", "5", "6"]:
    n_neighbours = 10
    well_density_file = f"{event_density_dir}well_{well}.csv"
    well_events = {k: v for k, v in events_no_is.items() if v["well"] == well}
    density_by_well[well], _ = event_density_grid(
        well_events,
        grid_points,
        n_neighbours,
        well_density_file,
    )
    one_event_in_80_sphere = 3 / 4 / PI / 80 / 80 / 80
    vmin = np.log10(one_event_in_80_sphere)
    volume_by_well[well] = (
        len(np.where(density_by_well[well] > 10**vmin)[0]) * grid_spacing**3
    )
    vertexes, volume_tags, volume_new = output_isosurface(
        density_by_well[well],
        10**vmin,
        grid_points,
        f"{event_density_dir}well_{well}_isosurfaces.csv",
    )

well_combos = [["1", "2"], ["2", "3"], ["4", "5"]]
density_by_combo = {}
volume_by_combo = {}
for well_combo in well_combos:
    well1, well2 = well_combo
    combo_events = well_events = {
        k: v
        for k, v in events_no_is.items()
        if v["well"] == well1 or v["well"] == well2
    }

    combo_density_file = f"{event_density_dir}combo_{well1}{well2}.csv"
    density_by_combo[well1 + well2], _ = event_density_grid(
        combo_events,
        grid_points,
        n_neighbours,
        combo_density_file,
    )
    volume_by_combo[well1 + well2] = (
        len(np.where(density_by_combo[well1 + well2] > 10**vmin)[0])
        * grid_spacing**3
    )
    vertexes, volume_tags, volume_new = output_isosurface(
        density_by_combo[well1 + well2],
        10**vmin,
        grid_points,
        f"{event_density_dir}combo_{well1}{well2}_isosurfaces.csv",
    )
