import matplotlib.pyplot as plt
import numpy as np
import pickle

from read_inputs import (
    read_catalog,
    read_wells,
    read_frac_report,
    read_diffusivity,
    read_frac_dimensions,
    read_cumulative_volume,
)

from plotting_stuff import gray_background_with_grid

dimensions = read_frac_dimensions("frac_dimensions.csv")
diffusivity = read_diffusivity("rt_curve.csv")
strain = read_cumulative_volume("strain_complete//cumulative_volume.csv")
density = read_cumulative_volume("event_density//cumulative_volume.csv")
wells = read_wells()
events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)


def reshape_report(report):
    reshape = {}
    for well in ["1", "2", "3", "4", "5", "6"]:
        for stage in [k for k in report[f"Well {well}"].keys() if "tage" in k]:
            stage_number = stage.split()[1].zfill(2)
            well_stage_identifier = f"Well {well}: Stage {stage_number}"
            if well == 1 and stage_number == 5:
                reshape[well_stage_identifier + ".0"] = report["Well 1"]["Stage 5"]
                reshape[well_stage_identifier + ".5"] = report["Well 1"]["Stage 5"]
            else:
                reshape[well_stage_identifier] = report[f"Well {well}"][stage]
    reshape.pop("Well 1: Stage 05")
    return reshape


report = reshape_report(read_frac_report())

with open("percentStrike.pkl", "rb") as pickle_file:
    percent_strike_from_pickle = pickle.load(pickle_file)

n_clusters = {}
for well_id, well in wells.items():
    if well_id in ["1", "2", "3", "4", "5", "6"]:
        stages = {k: v for k, v in well.items() if "tage" in k}.items()
        for stage_id, stage in stages:
            well_stage_identifier = f"Well {well_id}: {stage_id}"
            n_clusters[well_stage_identifier] = len(stage)

test_stages = [k for k, v in n_clusters.items() if v < 6 and k.split()[-1] != "01"]
normal_stages = [k for k, v in n_clusters.items() if v == 6 and k.split()[-1] != "01"]

inc_volume = {k: v["incremental volume"] for k, v in strain.items() if k in test_stages}
isip = {k: v["post-treat ISIP (bars)"] for k, v in report.items() if k in test_stages}

i_well1 = [i for i, stage in enumerate(test_stages) if "Well 1" in stage]
i_well2 = [i for i, stage in enumerate(test_stages) if "Well 2" in stage]
i_well3 = [i for i, stage in enumerate(test_stages) if "Well 3" in stage]
fig, ax = plt.subplots()
ax.plot(inc_volume[i_well1], isip[i_well1], "o", color="firebrick", markeredgecolor="k")
ax.plot(
    inc_volume[i_well2], isip[i_well2], "o", color="forestgreen", markeredgecolor="k"
)
ax.plot(inc_volume[i_well3], isip[i_well3], "o", color="royalblue", markeredgecolor="k")

fig2, ax2 = plt.subplots()
inc_volume = {
    k: v["incremental volume"] for k, v in strain.items() if k in normal_stages
}
isip.pop("Well 1: Stage 05")
isip = {k: v["post-treat ISIP (bars)"] for k, v in report.items() if k in normal_stages}

i_well1 = [i for i, stage in enumerate(normal_stages) if "Well 1" in stage]
i_well2 = [i for i, stage in enumerate(normal_stages) if "Well 2" in stage]
i_well3 = [i for i, stage in enumerate(normal_stages) if "Well 3" in stage]
i_well4 = [i for i, stage in enumerate(normal_stages) if "Well 4" in stage]
i_well5 = [i for i, stage in enumerate(normal_stages) if "Well 5" in stage]
ax2.plot(
    inc_volume[i_well1], isip[i_well1], "o", color="firebrick", markeredgecolor="k"
)
ax2.plot(
    inc_volume[i_well2], isip[i_well2], "o", color="forestgreen", markeredgecolor="k"
)
ax2.plot(
    inc_volume[i_well3], isip[i_well3], "o", color="royalblue", markeredgecolor="k"
)
ax2.set_ylim([450, 600])

fig3, ax3 = plt.subplots()
ax3.plot(
    inc_volume[i_well4], isip[i_well4], "o", color="darkgoldenrod", markeredgecolor="k"
)
ax3.plot(
    inc_volume[i_well5], isip[i_well5], "o", color="mediumorchid", markeredgecolor="k"
)
isip = {k: v for k, v in report.items()}

f = open("perf_centers.csv")
head = f.readline()
lines = f.readlines()
f.close()
lines[-1]
perf_centers = {}
for line in lines:
    lspl = line.split(",")
    well_stage_identifier = f"Well {lspl[1]}: Stage {lspl[2].zfill(2)}"
    perf_centers[well_stage_identifier] = {
        "easting": float(lspl[3]),
        "northing": float(lspl[4]),
        "elevation": float(lspl[5]),
    }

for identifier, well_stage in perf_centers.items():
    if identifier[:-2] == "Well 1: Stage 05":
        continue
    well_stage["isip"] = report[identifier]["post-treat ISIP (bars)"]
    if identifier in diffusivity:
        well_stage["diffusivity"] = diffusivity[identifier]["horizontal"]
    well = identifier[5]
    stage = identifier.split()[-1]
    ps_well = f"well-{well}"
    ps_stage = f"stage-{stage}.0"
    if ps_stage in percent_strike[ps_well]:
        well_stage["percent strike-slip"] = percent_strike[ps_well][ps_stage][
            "percentSS"
        ]
        well_stage["number MT"] = percent_strike[ps_well][ps_stage]["nevents"]


fig, ax = plt.subplots(figsize=[8, 8])
ax.set_aspect("equal")
for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k")

colors = ax.scatter(
    [v["easting"] for v in perf_centers.values() if "diffusivity" in v],
    [v["northing"] for v in perf_centers.values() if "diffusivity" in v],
    marker="o",
    c=[v["diffusivity"] for v in perf_centers.values() if "diffusivity" in v],
)

cbaxes = fig.add_axes([0.2, 0.18, 0.6, 0.03])
cb = plt.colorbar(colors, cax=cbaxes, orientation="horizontal")
cb.set_label("Post treatment ISIP (bars)")
ax = gray_background_with_grid(ax)
fig.savefig("isip.png")


f, a = plt.subplots(figsize=[12, 12])
a.plot(
    [
        v["isip"]
        for v in perf_centers.values()
        if "percent strike-slip" in v and v["number MT"] > 30
    ],
    [
        v["percent strike-slip"]
        for v in perf_centers.values()
        if "percent strike-slip" in v and v["number MT"] > 30
    ],
    "o",
)

list(events.values())[11110]

n_events = {}
for wellstage, isip_value in isip.items():
    well = wellstage.split(":")[0].split()[-1]
    stage = wellstage.split()[-1]
    n_events[wellstage] = len(
        [
            k
            for k, v in events.items()
            if v["well"] == well and v["stage"] == stage and v["magnitude"] > -1.25
        ]
    )


fig_isip_nevents, ax = plt.subplots()
ax.set_facecolor("0.9")
ax.plot(
    [v for v in isip.values()],
    [v for v in n_events.values()],
    "o",
    color="turquoise",
    markeredgecolor="0.2",
)
ax.set_xlim([450, 590])
ax.set_xlabel("ISIP (bar)")
ax.set_ylabel("number of events above Mc")
fig_isip_nevents.savefig("isip_n_events.png")
