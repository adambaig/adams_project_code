from datetime import datetime

import glob
import numpy as np
from obspy import UTCDateTime
import pytz
import re

from MT.moment_tensor import extractStream, loadPickFile, getStationsFromTTT
from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.raytrace import isotropic_ray_trace

from read_inputs import read_catalog, get_velocity_model, read_stations

datadir = "/data/mount_doom/home/aaronb/projects/Chevron_Phase1_MT_QC"

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")
datadir = "/data/mount_doom/home/benw/projects/Chevron_PFI_15-06/data/seed/raw"

Q = {"P": 60, "S": 60}
velocity_model = get_velocity_model()
events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
delta = 0.004
preTime = 1.0
postTime = 1.0
ampWindow = 0.05
polWindow = 0.01
TTTfile = "/data/mount_doom/work/projects/Chevron_PFI_15-06/velocity/test_variable2/update/optimal-TTT.pkl"
pklfile = open(TTTfile, "rb")
TTT = pickle.load(pklfile)

time_series = np.arange(0, 250 * delta, delta)
stations = read_stations()
starttime = utc.localize(datetime(2019, 12, 19, 12, 0, 36))
endtime = utc.localize(datetime(2019, 12, 19, 13, 0, 36))
events_window = {
    k: v
    for k, v in events.items()
    if v["timestamp"] > starttime and v["timestamp"] < endtime
}
bottom_stations = {k: v for k, v in stations.items() if re.match("B...4", k)}
for event in events_window.values():
    source = {
        "e": event["easting"],
        "n": event["northing"],
        "z": event["elevation"],
        "stress_drop": 3e5,
        "moment_magnitude": event["magnitude"],
        "moment_tensor": event["gen MT"],
    }
    event_window_start = UTCDateTime(event["timestamp"])
    event_window_end = event_window_start + 5
    st = extractStream(datadir, event_window_start, event_window_end)
    pickfile = glob.glob(pick_dir + "//" + event[id] + "*.picks")
    for station in bottom_stations.values():
        p_raypath = isotropic_ray_trace(source, station, velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, station, velocity_model, "S")
        waveform = get_response(
            p_raypath, s_raypath, source, station, Q, time_series, 0
        )
