from datetime import datetime, timedelta
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import pytz

from pfi_qi.engineering import calc_well_trend, find_perf_center
from pfi_qi.rotations import rotate_perf_clusters, rotate_from_cardinal
from generalPlots import gray_background_with_grid

from sms_moment_tensor.MT_math import mt_to_sdr

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")
tab10 = cm.get_cmap("tab10")
symbol = {"FL": "^", "LP": "s", "HP": "o", "PRE": ".", "POST": ".", "IS": "o"}


def rt_with_treatment(
    stage_events,
    stage_data,
    diffusivity,
    t0,
    title=None,
    y_axis="along trend",
    diverter=None,
):
    utc_times = [
        utc.localize(datetime.strptime(t, "%Y%m%d%H%M%S.%f")) for t in stage_data.keys()
    ]
    slurry = np.array([v["slurry_rate"] for v in stage_data.values()]) / 20.0
    proppant = np.array([v["proppant_conc"] for v in stage_data.values()]) / 500.0
    pressure = np.array([v["pressure"] for v in stage_data.values()]) / 100.0
    fig = plt.figure(figsize=[12, 12])
    if diverter is not None:
        ax1 = fig.add_axes([0.1, 0.52, 0.6, 0.38])
        ax2 = fig.add_axes([0.1, 0.1, 0.6, 0.38])
        ax4 = fig.add_axes([0.8, 0.1, 0, 0.38])
        ax5 = fig.add_axes([0.9, 0.1, 0, 0.38])
        ax5.set_xticks([])
        ax5.set_yticks(np.linspace(0, 1, 8))
        ax5.set_yticklabels(np.linspace(0, 700, 8))
        ax5.yaxis.set_ticks_position("right")
        ax5.yaxis.set_label_position("right")
        ax5.set_ylabel("Diverter (kg)", color="forestgreen")
    else:
        ax1 = fig.add_axes([0.1, 0.52, 0.7, 0.38])
        ax2 = fig.add_axes([0.1, 0.1, 0.7, 0.38])
        ax4 = fig.add_axes([0.9, 0.1, 0, 0.38])
    stage = np.unique([v["stage"] for v in stage_events.values()])[0]
    mod_stage = int(stage[-1])
    stage_color = tab10((mod_stage + 0.1) / 10)
    if "treatment_code" in list(stage_events.values())[0]:
        fluid_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "FL"
        }
        lp_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "LP"
        }
        hp_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "HP"
        }
        pre_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "PRE"
        }
        post_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "POST"
        }
        is_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "IS"
        }

        for events in (
            fluid_events,
            lp_events,
            hp_events,
            pre_events,
            post_events,
        ):
            if len(events) == 0:
                continue
            treatment_code = list(events.values())[0]["treatment_code"]
            ax1.plot(
                [v["timestamp"] for v in events.values()],
                [abs(v[y_axis]) for v in events.values()],
                symbol[treatment_code],
                c=stage_color,
                markeredgecolor="k",
                linewidth=0.25,
                zorder=3,
                ms=10,
                alpha=0.8,
            )
        if len(is_events) > 0:
            ax1.plot(
                [v["timestamp"] for v in is_events.values()],
                [abs(v[y_axis]) for v in is_events.values()],
                "o",
                c="w",
                markeredgecolor="k",
                linewidth=0.25,
                zorder=3,
                ms=10,
                alpha=0.8,
            )
    else:
        ax1.plot(
            [v["timestamp"] for v in stage_events.values()],
            [abs(v[y_axis]) for v in stage_events.values()],
            "o",
            c=stage_color,
            markeredgecolor="k",
            linewidth=0.25,
            zorder=3,
            ms=10,
            alpha=0.8,
        )
    ax1.set_xticklabels([])
    for ax in ax1, ax2:
        ax.set_facecolor("lightgrey")
    envelope_times = np.arange(
        t0,
        max([v["timestamp"] for v in stage_events.values()]),
        dtype="datetime64[s]",
    )
    if diffusivity:
        ax1.plot(
            envelope_times,
            np.sqrt(
                4
                * np.pi
                * diffusivity
                * np.array(
                    [
                        np.timedelta64(t - np.datetime64(t0), "s")
                        .item()
                        .total_seconds()
                        for t in envelope_times
                    ]
                )
            ),
            linestyle="--",
            lw="2",
            c="0.1",
        )
    if diverter is not None:
        amounts = np.array([v["amount_kg"] for v in diverter.values()]) / 700.0
        diverter_times = [
            utc.localize(datetime.strptime(t, "%Y%m%d%H%M%S.%f"))
            for t in diverter.keys()
        ]
        ax2.bar(diverter_times, amounts, width=2.0 / 1440, color="forestgreen")
    ax2.plot(utc_times, proppant, c="darkgoldenrod", lw=2)
    ax2.plot(utc_times, pressure, c="firebrick", lw=2)
    ax2.set_ylabel("pressure (MPa)", color="firebrick")
    ax2.set_yticks(np.linspace(0, 1, 6))
    ax2.set_yticklabels(np.linspace(0, 100, 6))
    ax3 = ax.twinx()
    ax3.plot(utc_times, slurry, c="royalblue", lw=2)
    ax3.set_ylabel("slurry rate (m$^3$/min)", color="royalblue")
    ax3.set_yticks(np.linspace(0, 1, 5))
    ax3.set_yticklabels(np.linspace(0, 20, 5))
    ax4.yaxis.set_ticks_position("right")
    ax4.yaxis.set_label_position("right")
    ax4.set_yticks(np.linspace(0, 1, 6))
    ax4.set_yticklabels(np.linspace(0, 500, 6))
    ax4.set_xticks([])
    ax4.set_ylabel("Proppant Concentration (kg/m$^3$)", color="darkgoldenrod")
    ax2.xaxis.set_major_formatter(md.DateFormatter("%b %d\n%H:%M", tz=timezone))
    ax2.set_xlabel("local time")
    if title is not None:
        ax1.set_title(title)
    for ax in ax2, ax3, ax4:
        ax.set_ylim([0, 1])
    x1, x2 = ax2.get_xlim()
    x3, x4 = ax1.get_xlim()
    for ax in ax1, ax2:
        ax.set_xlim([x1, max(x2, x4)])
    y1, y2 = ax1.get_ylim()
    ax1.set_ylim([0, y2])
    ax1.set_ylabel(f"{y_axis} distance (m)")
    return fig


def plot_stage(stage_events, wells, title=None, grid_spacing=50, limits=None):
    stage = np.unique([v["stage"] for v in stage_events.values()])[0]
    well_id = np.unique([v["well"] for v in stage_events.values()])[0]
    well_trend = calc_well_trend(wells[well_id])
    mod_stage = int(stage[-1])
    stage_color = tab10((mod_stage + 0.1) / 10)
    fig, ax = plt.subplots(figsize=[12, 12])
    ax.set_aspect("equal")
    for perf, cluster in wells[well_id]["Stage " + stage].items():
        cluster["easting"] = 0.5 * (cluster["top_east"] + cluster["bottom_east"])
        cluster["northing"] = 0.5 * (cluster["top_north"] + cluster["bottom_north"])
        cluster["elevation"] = 0.5 * (
            cluster["top_elevation"] + cluster["bottom_elevation"]
        )
    if "treatment_code" in list(stage_events.values())[0]:
        fluid_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "FL"
        }
        lp_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "LP"
        }
        hp_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "HP"
        }
        pre_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "PRE"
        }
        post_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "POST"
        }
        is_events = {
            k: v for k, v in stage_events.items() if v["treatment_code"] == "IS"
        }

        for events in (fluid_events, lp_events, hp_events, post_events):
            if len(events) == 0:
                continue
            treatment_code = list(events.values())[0]["treatment_code"]
            ax.plot(
                [v["easting"] for v in events.values()],
                [v["northing"] for v in events.values()],
                symbol[treatment_code],
                c=stage_color,
                markeredgecolor="k",
                linewidth=0.25,
                zorder=3,
                ms=10,
                alpha=0.8,
            )
        for events in (pre_events, is_events):
            if len(events) == 0:
                continue
            treatment_code = list(events.values())[0]["treatment_code"]
            ax.plot(
                [v["easting"] for v in is_events.values()],
                [v["northing"] for v in is_events.values()],
                symbol[treatment_code],
                c="w",
                markeredgecolor="k",
                linewidth=0.25,
                zorder=3,
                ms=10,
                alpha=0.8,
            )
    else:
        ax.plot(
            [v["easting"] for v in stage_events.values()],
            [v["northing"] for v in stage_events.values()],
            "o",
            c=stage_color,
            markeredgecolor="k",
            linewidth=0.25,
            zorder=3,
            ms=10,
            alpha=0.8,
        )
    ax.plot(
        [v["easting"] for v in wells[well_id]["Stage " + stage.zfill(2)].values()],
        [v["northing"] for v in wells[well_id]["Stage " + stage.zfill(2)].values()],
        marker=(7, 1, 0),
        c=stage_color,
        ms=16,
        markeredgecolor="k",
        linewidth=0.25,
        zorder=2,
    )
    if limits is not None:
        ax.set_xlim(limits["x1"], limits["x2"])
        ax.set_ylim(limits["y1"], limits["y2"])
    x1, x2 = ax.get_xlim()
    y1, y2 = ax.get_ylim()
    ax.plot(
        wells[well_id]["easting"],
        wells[well_id]["northing"],
        lw=4,
        c="0.7",
        zorder=0,
    )
    ax.plot(
        wells[well_id]["easting"],
        wells[well_id]["northing"],
        lw=5,
        c="0.3",
        zorder=-1,
    )
    for well2 in wells.values():
        ax.plot(well2["easting"], well2["northing"], lw=2, c="0.8", zorder=-2)
        ax.plot(well2["easting"], well2["northing"], lw=2.5, c="0.4", zorder=-3)
    ax.set_xlim([x1, x2])
    ax.set_ylim([y1, y2])
    ax = gray_background_with_grid(ax, grid_spacing=50)
    ax.set_title(title)
    return fig


def plot_rotated_stage(stage_events, wells, title=None, grid_spacing=50):
    stage = np.unique([v["stage"] for v in stage_events.values()])[0]
    well_id = np.unique([v["well"] for v in stage_events.values()])[0]

    well_trend = calc_well_trend(wells[well_id])
    perf_center = find_perf_center(stage.zfill(2), wells[well_id])
    rotate_from_cardinal(stage_events, well_trend, perf_center)
    rotate_from_cardinal(wells, well_trend, perf_center)
    rotated_perfs = rotate_perf_clusters(
        wells[well_id][f"Stage {stage}"], well_trend, perf_center
    )
    mod_stage = int(stage[-1])
    stage_color = tab10((mod_stage + 0.1) / 10)
    fig, ax = plt.subplots(figsize=[12, 12])
    ax.set_aspect("equal")
    ax.plot(
        [v["perp to trend"] for v in stage_events.values()],
        [v["along trend"] for v in stage_events.values()],
        "o",
        c=stage_color,
        markeredgecolor="k",
        linewidth=0.25,
        zorder=3,
        ms=10,
        alpha=0.8,
    )
    ax.plot(
        [v["perp to trend"] for v in rotated_perfs.values()],
        [v["along trend"] for v in rotated_perfs.values()],
        marker=(7, 1, 0),
        c=stage_color,
        ms=16,
        markeredgecolor="k",
        linewidth=0.25,
        zorder=2,
    )
    x1, x2 = ax.get_xlim()
    y1, y2 = ax.get_ylim()
    ax.plot(
        wells[well_id]["perp to trend"],
        wells[well_id]["along trend"],
        lw=4,
        c="0.7",
        zorder=0,
    )
    ax.plot(
        wells[well_id]["perp to trend"],
        wells[well_id]["along trend"],
        lw=5,
        c="0.3",
        zorder=-1,
    )
    for well2 in wells.values():
        ax.plot(
            well2["perp to trend"],
            well2["along trend"],
            lw=2,
            c="0.8",
            zorder=-2,
        )
        ax.plot(
            well2["perp to trend"],
            well2["along trend"],
            lw=2.5,
            c="0.4",
            zorder=-3,
        )
    ax.set_xlim([x1, x2])
    ax.set_ylim([y1, y2])
    ax = gray_background_with_grid(ax, grid_spacing=50)
    ax.set_title(title)
    return fig


def add_dimension_arrows(axis, arrows):
    axis.arrow(
        arrows["lengthwise"]["easting"][0],
        arrows["lengthwise"]["northing"][0],
        np.diff(arrow["lengthwise"]["easting"])[0],
        np.diff(arrow["lengthwise"]["northing"])[0],
        zorder=27,
        lw=3,
        color="k",
    )
    axis.arrow(
        arrows["lengthwise"]["easting"][0],
        arrows["lengthwise"]["northing"][0],
        np.diff(arrow["lengthwise"]["easting"])[0],
        np.diff(arrow["lengthwise"]["northing"])[0],
        zorder=28,
        lw=1,
        color="w",
    )
    axis.arrow(
        arrows["widthwise"]["easting"][0],
        arrows["widthwise"]["northing"][0],
        np.diff(arrow["widthwise"]["easting"])[0],
        np.diff(arrow["widthwise"]["northing"])[0],
        zorder=27,
        lw=3,
        color="k",
    )
    axis.arrow(
        arrows["widthwise"]["easting"][0],
        arrows["widthwise"]["northing"][0],
        np.diff(arrow["widthwise"]["easting"])[0],
        np.diff(arrow["widthwise"]["northing"])[0],
        zorder=28,
        lw=1,
        color="w",
    )
    return axis


def add_dimension_arrows_abs_coords(axis, arrows):
    axis.arrow(
        arrows["lengthwise"]["easting"][0],
        arrows["lengthwise"]["northing"][0],
        np.diff(arrows["lengthwise"]["easting"])[0],
        np.diff(arrows["lengthwise"]["northing"])[0],
        zorder=27,
        lw=3,
        color="k",
    )
    axis.arrow(
        arrows["lengthwise"]["easting"][0],
        arrows["lengthwise"]["northing"][0],
        np.diff(arrows["lengthwise"]["easting"])[0],
        np.diff(arrows["lengthwise"]["northing"])[0],
        zorder=28,
        lw=1,
        color="w",
    )
    axis.arrow(
        arrows["widthwise"]["easting"][0],
        arrows["widthwise"]["northing"][0],
        np.diff(arrows["widthwise"]["easting"])[0],
        np.diff(arrows["widthwise"]["northing"])[0],
        zorder=27,
        lw=3,
        color="k",
    )
    axis.arrow(
        arrows["widthwise"]["easting"][0],
        arrows["widthwise"]["northing"][0],
        np.diff(arrows["widthwise"]["easting"])[0],
        np.diff(arrows["widthwise"]["northing"])[0],
        zorder=28,
        lw=1,
        color="w",
    )
    return axis


def plot_strike_dip_rake_rosettes(
    events, mt_key="mt DC", bin_degrees=15, color="steelblue"
):
    n_events = 2 * len(events)
    strikes, dips, rakes = (
        np.zeros(n_events),
        np.zeros(n_events),
        np.zeros(n_events),
    )
    for i_event, event in enumerate(events.values()):
        (
            strikes[2 * i_event],
            dips[2 * i_event],
            rakes[2 * i_event],
            strikes[2 * i_event + 1],
            dips[2 * i_event + 1],
            rakes[2 * i_event + 1],
        ) = mt_to_sdr(event[mt_key])
    strikes_180 = strikes % 180
    strikes_360 = strikes_180 + 180
    mirrored_strikes = np.hstack([strikes_180, strikes_360])
    fig = plt.figure(figsize=[16, 5])
    ax_strike = fig.add_axes([0.05, 0.1, 0.38, 0.8], projection="polar")
    ax_dip = fig.add_axes([0.44, 0.1, 0.12, 0.8], projection="polar")
    ax_rake = fig.add_axes([0.57, 0.1, 0.38, 0.8], projection="polar")
    bin_degrees = 5
    strike_bins = np.arange(0, 360.0001, bin_degrees)
    n_strike, _ = np.histogram(mirrored_strikes, strike_bins)
    ax_strike.bar(
        D2R * (strike_bins[:-1] + bin_degrees / 2),
        n_strike,
        width=D2R * bin_degrees,
        color=color,
    )
    ax_strike.set_theta_zero_location("N")
    ax_strike.set_theta_direction(-1)
    ax_strike.set_thetagrids(
        np.arange(0, 360, 45), ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
    )

    dip_bins = np.arange(0, 90.0001, bin_degrees)
    n_dip, _ = np.histogram(dips, dip_bins)
    ax_dip.bar(
        D2R * (dip_bins[:-1] + bin_degrees / 2),
        n_dip,
        width=D2R * bin_degrees,
        color=color,
    )
    ax_dip.set_theta_direction(-1)
    ax_dip.set_thetamax(90)

    rake_bins = np.arange(-180, 180.0001, bin_degrees)
    n_rake, _ = np.histogram(rakes, rake_bins)
    ax_rake.bar(
        D2R * (rake_bins[:-1] + bin_degrees / 2),
        n_rake,
        width=D2R * bin_degrees,
        color=color,
    )
    ax_rake.set_thetagrids(np.arange(0, 360, 90), ["LL", "TH", "RL", "NR"])
    return fig
