from datetime import datetime
from glob import glob
import os
import pytz

UTC = pytz.utc


def make_seedfile_dictionary(directory_path, extension="seed"):
    seed_files = glob(os.path.join(directory_path, "*." + extension))
    if len(seed_files) == 0:
        raise Exception("No ." + extension + " files in directory_path")
    seedfile_dictionary = {}
    for seed_file in seed_files:
        starttime, endtime = (
            os.path.basename(seed_file).split("." + extension)[0].split("_")
        )
        seedfile_dictionary[seed_file] = {
            "start time": UTC.localize(
                datetime.strptime(starttime, "%Y%m%d.%H%M%S.%f")
            ),
            "end time": UTC.localize(datetime.strptime(endtime, "%Y%m%d.%H%M%S.%f")),
        }
    return seedfile_dictionary


def find_seedfile(datetime_sought, directory_path=None, seedfile_dictionary=None):
    if directory_path is None and seedfile_dictionary is None:
        raise Exception(
            "One of directory_path and seedfile_dictionary must be specified"
        )
        return None
    if seedfile_dictionary is None:
        seedfile_dictionary = make_seedfile_dictionary(directory_path)
    for seed_file, range in seedfile_dictionary.items():
        if (
            datetime_sought > range["start time"]
            and datetime_sought < range["end time"]
        ):
            return seed_file
    raise Exception("no seed file found corresponding to datetime_sought")
    return None
