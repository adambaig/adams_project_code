import glob
import matplotlib.pyplot as plt
import numpy as np
import os
from read_inputs import read_wells, read_tops, read_catalog

from plotting_stuff import gray_background_with_grid
events = read_catalog('Catalogs//Chevron_15_06_Apr30_2020_updatedEventDescriptions.csv')


dat_files = glob.glob('From Client\\tops\\*.dat')
fig, ax = plt.subplots(2,2, sharex=True, sharey=True, figsize=[12,12])
for i_dat,dat_file in enumerate(dat_files):
    i = i_dat//2
    j = i_dat%2
    ax[i,j].set_aspect('equal')

    ax[i,j].plot([event['easting'] for event in events.values()],
                [event['northing'] for event in events.values()],'k.')

    f = open(dat_file)
    header = [f.readline() for i in range(20)]
    lines = f.readlines()
    f.close()
    duvernay = []
    for line in lines:
        lspl = line.split()
        duvernay.append({'e': float(lspl[0]), 'n': float(lspl[1]), 'z': float(lspl[2])})

    for point in duvernay:
        ax[i,j].plot(point['e'],point['n'],'b.',zorder=-1)
    ax[i,j].set_title(os.path.split(dat_file)[1].split('.')[0])
    gray_background_with_grid(ax[i,j])
fig.savefig('tops_check.png')

|plt.plot(tops['n_grid'][:-1])

from pfi_qi.engineering import find_perf_center
from scipy.interpolate import RectBivariateSpline
test = find_perf_center('23',wells['2'])
tops['Duvernay'].shape
top = RectBivariateSpline(tops['e_grid'],tops['n_grid'][:-1],tops['Duvernay'])

def top(x,y,layer)


duv_top(test['easting'],test['northing'])
test
tops = read_tops()

test = wells['1']['Stage']
