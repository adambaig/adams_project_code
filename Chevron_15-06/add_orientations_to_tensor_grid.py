import numpy as np

from sms_moment_tensor.MT_math import decompose_MT, mt_vector_to_matrix

eps = 1e-16
f = open("strain_tensors.csv")
head = f.readline()
g = open("strain_tensors_and_orientations.csv", "w")
g.write(head.replace("\n", ",p trend,p plunge,t trend,t plunge\n"))
lines = f.readlines()
f.close()
for line in lines:
    strain = mt_vector_to_matrix([float(s) for s in line.split(",")[-6:]])
    g.write(line[:-1])
    if np.linalg.norm(strain) > eps:
        decomp = decompose_MT(strain)
        g.write(f",{decomp['p_trend']:.1f},{decomp['p_plunge']:.1f}")
        g.write(f",{decomp['t_trend']:.1f},{decomp['t_plunge']:.1f}\n")
    else:
        g.write(",-,-,-,-\n")

g.close()
