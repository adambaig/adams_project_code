import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np

from nmxseis.numerics.moment_tensor import MomentTensor

from read_inputs import read_catalog

events = read_catalog("events_after_MT_flip.csv")

list(events.values())[0]

good_events = {k: v for k, v in events.items() if v["mt confidence"] > 0.95}
bad_events = {k: v for k, v in events.items() if v["mt confidence"] <= 0.95}

good_dc_mts = [MomentTensor(np.array(v["dc MT"])) for v in good_events.values()]
poor_dc_mts = [MomentTensor(np.array(v["dc MT"])) for v in bad_events.values()]

test = good_dc_mts[0]


decomp = test.decompose()

fig_good, [p_ax, b_ax, t_ax] = mpls.subplots(1, 3, figsize=[15, 5])
for label, ax in zip(["p", "b", "t"], [p_ax, b_ax, t_ax]):
    ax.grid()
    ax.set_azimuth_ticks([])
    ax.density_contourf(
        [v[f"{label}_plunge"] for v in good_events.values()],
        [v[f"{label}_trend"] for v in good_events.values()],
        measurement="lines",
        alpha=0.67,
        cmap="magma_r",
    )

fig_bad, [p_ax, b_ax, t_ax] = mpls.subplots(1, 3, figsize=[15, 5])
for label, ax in zip(["p", "b", "t"], [p_ax, b_ax, t_ax]):
    ax.grid()
    ax.set_azimuth_ticks([])
    ax.density_contourf(
        [v[f"{label}_plunge"] for v in bad_events.values()],
        [v[f"{label}_trend"] for v in bad_events.values()],
        measurement="lines",
        alpha=0.67,
        cmap="magma_r",
    )

before_events = read_catalog("Catalogs//events_with_treatment_tags_before_MT_flip.csv")


fig_before, [p_ax, b_ax, t_ax] = mpls.subplots(1, 3, figsize=[15, 5])
for label, ax in zip(["p", "b", "t"], [p_ax, b_ax, t_ax]):
    ax.grid()
    ax.set_azimuth_ticks([])
    ax.density_contourf(
        [v[f"{label}_plunge"] for v in before_events.values()],
        [v[f"{label}_trend"] for v in before_events.values()],
        measurement="lines",
        alpha=0.67,
        cmap="magma_r",
    )


fig_good.savefig("good_pbt.png")
fig_bad.savefig("poor_pbt.png")
fig_before.savefig("before_pbt.png")
