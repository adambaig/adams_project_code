# import matplotlib
#
# matplotlib.use("Qt5agg")
from copy import deepcopy
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import pytz

from pfi_qi.QI_analysis import (
    parent_child,
    rt_diffusivity,
    fracture_dimensions,
    strain_grid,
    output_isosurface,
    rotate_all_events_to_well_coords,
)
from pfi_qi.engineering import (
    sorted_stage_list,
    breaktimes_by_stage,
    find_perf_center,
    calc_well_trend,
    make_unique_stage_list,
)
from pfi_qi.rotations import rotate_from_cardinal
from sms_moment_tensor.MT_math import decompose_MT, clvd_iso_dc, mt_to_sdr

from read_inputs import (
    read_events,
    read_catalog,
    get_velocity_model,
    read_wells,
    read_treatment,
    read_or_load_json,
    read_diverter_drops,
    read_parent_stages_2016,
    classify_treatment,
    classify_events_by_treatment,
    write_catalog,
)
from plotting_stuff import (
    rt_with_treatment,
    plot_rotated_stage,
    plot_stage,
    add_dimension_arrows_abs_coords,
)

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")

# read in all well and treatment data
treatment = read_or_load_json(read_treatment, "treatment_data.json")


len(treatment)
timestamp, b = list(treatment.items())[-1]

datetime.strptime(a, "%Y%m%d%H%M%S.%f")

wells = ["1", "2", "3", "4", "5", "6"]
well = "1"
for well in wells:
    f = open(f"treatment_data_well_{well}.csv", "w")
    f.write(
        "Mountain time,stage,pressure (MPa),slurry rate (m³/min),proppant concentration(kg/m³)\n"
    )
    well_treatment = {k: v for k, v in treatment.items() if v["well"] == well}
    for timestamp, data in well_treatment.items():
        transform_time_format = datetime.strftime(
            utc.localize(datetime.strptime(timestamp, "%Y%m%d%H%M%S.%f")).astimezone(
                timezone
            ),
            "%Y-%m-%d %H:%M:%S",
        )
        f.write(
            f"{transform_time_format},{data['stage']},{data['pressure']:.1f},"
            f"{data['slurry_rate']:.1f},{data['proppant_conc']:.1f}\n"
        )
    f.close()
