from MT.moment_tensor import getAmp, loadPickFile, getStationsFromTTT
from obspy import read
import pickle
import numpy as np
import matplotlib.pyplot as plt

# big event
# st=read('/data/miount_doom/home/benw/Chevron_mt_QC/event-18132057476-raw.mseed')
# pickFile='/data/mount_doom/home/benw/projects/Chevron_PFI_15-06/EventReview/Final_Picks/18132057476_20191209.132057.636000.picks'
# st_for=read('/data/mount_doom/home/benw/Chevron_mt_QC/testdir/synthetic2020-05-06T14-22-32.489894Z.mseed')
# small event
# st=read('/data/mount_doom/home/benw/Chevron_mt_QC/event-18051337064.mseed')
# pickFile='/data/mount_doom/home/benw/projects/Chevron_PFI_15-06/EventReview/Final_Picks/18051337064_20191209.051337.244000.picks'
# st_for=read('/data/mount_doom/home/benw/Chevron_mt_QC/syn2-small.mseed')
# HP event
st = read("/data/mount_doom/home/benw/Chevron_mt_QC/event-19233822148-raw.mseed")
pickFile = "/data/mount_doom/home/benw/projects/Chevron_PFI_15-06/EventReview/Final_Picks/19233822148_20191210.233822.316000.picks"
st_for = read("/data/mount_doom/home/benw/Chevron_mt_QC/syn3.mseed")

st.resample(250)

picks = loadPickFile(pickFile)
pickTimes = picks["picks"][:, 2]


TTTfile = "/data/mount_doom/work/projects/Chevron_PFI_15-06/velocity/test_variable2/update/optimal-TTT.pkl"
pklfile = open(TTTfile, "rb")
TTT = pickle.load(pklfile)


preTime = 1.0
postTime = 1.0
ampWindow = 0.05
polWindow = 0.01

startTime = st[0].stats.starttime.timestamp
delta = st[0].stats.delta
# print(startTime,delta)
# exit()
stations = getStationsFromTTT(TTT)

# st_for=read('/data/mount_doom/home/benw/Chevron_mt_QC/testdir/synthetic2020-05-06T14-22-32.489894Z.mseed')
# st_for=read('/data/mount_doom/home/benw/Chevron_mt_QC/syn2.mseed')
st_for.resample(250)
for_stations = []
for tr in st_for:
    for_stations.append(tr.stats.station)


t = np.arange(0, 250 * delta, delta)
# print(t)
# print(len(t))
# exit()
correct = 0
for jj, (pickTime, station) in enumerate(zip(pickTimes, stations)):
    if station["name"][0] == "B" and station["name"][4] == "4":
        #       try:
        tr_for = st_for.copy().select(station=station["name"])
        tr = st.copy().select(station=station["name"])[0]
        #               tr.data=tr.data*-1
        #                       print(tr)
        #                       print(tr_for)
        #                       print('#########')
        amp_sig = -1 * getAmp(tr, pickTime, ampWindow, polWindow, 0.0)
        ind = int(np.round((float(pickTime) - startTime) / delta))
        ind1 = np.argmax(np.abs(tr_for[0].data[ind - 125 : ind + 125])) + ind - 125
        signFor = np.sign(tr_for[0].data[ind1])
        #               if np.sign(amp_sig)==signFor:
        #                       correct=correct+1
        print(station["name"], ind, ind1)
        for_dat = tr_for[0].data[ind1 - 125 : ind1 + 125]
        plt.plot(t + (ind - 125) * delta, for_dat / np.max(np.abs(for_dat)), "r--")
        dat = -1 * tr.data[ind - 125 : ind + 125]
        #               print(dat)
        plt.plot(t + (ind - 125) * delta, dat / np.max(np.abs(dat)), "k")
        plt.xlabel("Time since T0 (s)")
        plt.ylabel("Normalized amplitude")
        if amp_sig < 0:
            plt.vlines(ind * delta, -1, 0, colors="m", linewidth=5)
        elif amp_sig > 0:
            plt.vlines(ind * delta, 0, 1, colors="m", linewidth=5)
        plt.savefig(str(station["name"]))
        plt.close()

# print (correct,'/',len(for_stations))
#       except:
#               print(station['name'],"nope")
#               continue
