from copy import deepcopy
from datetime import datetime, timedelta
from matplotlib import cm, colors
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import os
import pytz
from scipy.ndimage import zoom
from scipy.interpolate import RegularGridInterpolator

from pfi_qi.QI_analysis import (
    strain_grid,
    output_isosurface,
)
from pfi_qi.engineering import sorted_stage_list
from read_inputs import (
    read_catalog,
    read_treatment,
    get_velocity_model,
    read_wells,
    read_strain,
    read_or_load_json,
)
from plotting_stuff import gray_background_with_grid
from sms_ray_modelling.anisotropy import (
    thomsen_params_to_stiffness_tensor,
    voigt_to_cjk,
    devoigt,
    voigt_vti_compliance,
)

tab10 = cm.get_cmap("tab10")
wells = read_wells()
strain, grid_points = read_strain("strain_tensors_from_potency.csv", tensor=True)

easts = np.unique(grid_points[:, 0])
norths = np.unique(grid_points[:, 1])
elevs = np.unique(grid_points[:, 2])

n_east, n_north, n_elev = len(easts), len(norths), len(elevs)

s11_interp = RegularGridInterpolator(
    (easts, norths, elevs), strain[:, 0].reshape(n_east, n_north, n_elev)
)
s22_interp = RegularGridInterpolator(
    (easts, norths, elevs), strain[:, 1].reshape(n_east, n_north, n_elev)
)
s33_interp = RegularGridInterpolator(
    (easts, norths, elevs), strain[:, 2].reshape(n_east, n_north, n_elev)
)
s12_interp = RegularGridInterpolator(
    (easts, norths, elevs), strain[:, 3].reshape(n_east, n_north, n_elev)
)
s13_interp = RegularGridInterpolator(
    (easts, norths, elevs), strain[:, 4].reshape(n_east, n_north, n_elev)
)
s23_interp = RegularGridInterpolator(
    (easts, norths, elevs), strain[:, 5].reshape(n_east, n_north, n_elev)
)

strain_das = {}

for well_id, well in wells.items():
    easting = well["easting"]
    northing = well["northing"]
    elevation = well["elevation"]
    md = well["measured_depth"]
    strain_das[well_id] = np.zeros(len(md))
    for i, (e, n, z) in enumerate(zip(easting[1:], northing[1:], elevation[1:])):
        if i < len(md):  # use penultimate rotation matrix for last point
            vector = np.array(
                [
                    easting[i + 1] - easting[i - 1],
                    northing[i + 1] - northing[i - 1],
                    elevation[i + 1] - elevation[i - 1],
                ]
            )
            unit_vector = vector / np.sqrt(sum([v * v for v in vector]))
            across = np.array([vector[1], -vector[0], 0]) / np.sqrt(
                sum([v * v for v in vector[:2]])
            )
            up_down = np.cross(unit_vector, across)
            rotation_matrix = np.vstack([unit_vector, across, up_down]).T
        try:
            s11 = s11_interp((e, n, z))
            s22 = s22_interp((e, n, z))
            s33 = s33_interp((e, n, z))
            s12 = s12_interp((e, n, z))
            s13 = s13_interp((e, n, z))
            s23 = s13_interp((e, n, z))
            strain_tensor = np.array(
                [[s11, s12, s13], [s12, s22, s23], [s13, s23, s33]]
            )
            rotated_strain = rotation_matrix @ strain_tensor @ rotation_matrix.T
            strain_das[well_id][i + 1] = rotated_strain[0, 0]
        except:
            strain_das[well_id][i + 1] = 0

fig, (ax1, ax2) = plt.subplots(2, sharex=True)

for well_id in ["1", "2", "3"]:
    ax1.plot(
        wells[well_id]["measured_depth"] - 3000,
        1e9 * strain_das[well_id],
        color=tab10(int(well_id) / 10 - 0.01),
    )
for well_id in ["4", "5"]:
    ax2.plot(
        wells[well_id]["measured_depth"] - 3000,
        1e9 * strain_das[well_id],
        color=tab10(int(well_id) / 10 - 0.01),
    )
x1, x2 = ax1.get_xlim()
ax1.set_xlim(0, x2)
ax1.set_ylabel("nanostrain")
ax1.text(0.1, 0.8, "SE wells", transform=ax1.transAxes)
x1, x2 = ax2.get_xlim()
ax2.set_xlim(0, x2)
ax2.set_xlabel("relative measured depth (m)")
ax2.set_ylabel("nanostrain")
ax2.text(0.1, 0.8, "NW wells", transform=ax2.transAxes)
fig.savefig("synethized_das.png")
