import matplotlib.pyplot as plt
import numpy as np

from pfi_qi.QI_analysis import strain_grid
from sms_moment_tensor.MT_math import decompose_MT

from plotting_stuff import gray_background_with_grid
from read_inputs import read_catalog, read_wells, get_velocity_model

PI = np.pi

events = read_catalog(
    "Catalogs//Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
wells = read_wells()
velocity_model = get_velocity_model()

good_MT_events = {k: v for k, v in events.items() if v["mt confidence"] > 0.95}

grid_spacing = 40
n_neighbours = 5
max_distance = 120

strain_tensors, grid_points = strain_grid(
    good_MT_events,
    grid_spacing,
    n_neighbours,
    max_distance,
    "strain_tensors.csv",
    velocity_model,
    tensor=True,
)


e_grid = np.unique(grid_points[:, 0])
n_grid = np.unique(grid_points[:, 1])
z_grid = np.unique(grid_points[:, 2])
n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)
# DUVERNAY is around z_grid[11]
elevation_index = 11
p_trend, p_plunge, t_trend, t_plunge = (
    np.zeros([n_e, n_n]),
    np.zeros([n_e, n_n]),
    np.zeros([n_e, n_n]),
    np.zeros([n_e, n_n]),
)
p_east, p_north, t_east, t_north = [], [], [], []


quiver_grid = []
for i_e, e in enumerate(e_grid):
    for i_n, n in enumerate(n_grid):
        if np.linalg.norm(strain_tensors[i_e, i_n, elevation_index]) > 1e-16:
            strain_decomp = decompose_MT(strain_tensors[i_e, i_n, elevation_index])
            p_trend[i_e, i_n] = strain_decomp["p_trend"]
            p_plunge[i_e, i_n] = strain_decomp["p_plunge"]
            t_trend[i_e, i_n] = strain_decomp["t_trend"]
            t_plunge[i_e, i_n] = strain_decomp["t_plunge"]
            t_east.append(
                np.sin(PI * t_trend[i_e, i_n] / 180.0)
                * np.cos(PI * t_plunge[i_e, i_n] / 180.0)
            )
            t_north.append(
                np.cos(PI * t_trend[i_e, i_n] / 180.0)
                * np.cos(PI * t_plunge[i_e, i_n] / 180.0)
            )
            p_east.append(
                np.sin(PI * p_trend[i_e, i_n] / 180.0)
                * np.cos(PI * p_plunge[i_e, i_n] / 180.0)
            )
            p_north.append(
                np.cos(PI * p_trend[i_e, i_n] / 180.0)
                * np.cos(PI * p_plunge[i_e, i_n] / 180.0)
            )
            quiver_grid.append({"east": e, "north": n})


fig, (ax1, ax2) = plt.subplots(2, figsize=[36, 16])
ax1.set_aspect("equal")
ax1.quiver(
    np.array([v["east"] for v in quiver_grid]),
    np.array([v["north"] for v in quiver_grid]),
    t_east,
    t_north,
    pivot="mid",
    width=0.002,
    zorder=5,
    headlength=0,
    headaxislength=0,
    headwidth=0,
    scale=1 / 100,
    scale_units="x",
    color="royalblue",
)
ax2.set_aspect("equal")
ax2.quiver(
    np.array([v["east"] for v in quiver_grid]),
    np.array([v["north"] for v in quiver_grid]),
    p_east,
    p_north,
    pivot="mid",
    width=0.002,
    zorder=5,
    headlength=0,
    headaxislength=0,
    headwidth=0,
    scale=1 / 100,
    scale_units="x",
    color="firebrick",
)
x1, x2 = ax1.get_xlim()
y1, y2 = ax1.get_ylim()


for ax in ax1, ax2:
    for well in [v for k, v in wells.items() if k in ["1", "2", "3", "4", "5", "6"]]:
        ax.plot(well["easting"], well["northing"], "k", lw=3)
    ax.set_xlim([x1, x2])
    ax.set_ylim([y1, y2])
    gray_background_with_grid(ax)

ax1.set_title("tensional strain", fontsize=18)
ax2.set_title("compressional strain", fontsize=18)

fig.savefig("strain_axis_quiver_plot_stacked", bbox_inches="tight")
