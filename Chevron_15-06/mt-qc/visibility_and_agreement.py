import matplotlib.pyplot as plt
import numpy as np

from sms_moment_tensor.MT_math import mt_vector_to_matrix, decompose_MT
from read_inputs import read_catalog

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
f = open("mt-qc//fixed_catalog_of_first_motions.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
amp_qc = {}
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[1:]:
        amp_qc[lspl[0]] = {k: v for v, k in zip(lspl[1:], headsplit[1:])}


f = open("mt-qc//borehole_assessment.csv")
head = f.readline()
lines = f.readlines()
f.close()
correction = {}
for line in lines:
    lspl = line.split(",")
    correction[lspl[0][:-1] + "4"] = int(lspl[-1])


f = open("mt-qc//MT_comparison____.csv")
head = [f.readline() for n in range(2)]
lines = f.readlines()
f.close()

events_qc = {}
for line in lines:
    lspl = line.split(",")
    if len(lspl) == 19:
        event_id = lspl[0]
        events_qc[event_id] = {
            "mt DC": mt_vector_to_matrix([float(s) for s in lspl[1:7]]),
            "cn": float(lspl[7]),
            "r": float(lspl[8]),
            "n": int(lspl[9]),
            "magnitude": [
                v["magnitude"] for v in events.values() if v["id"] == event_id
            ][0],
        }
        decomp = decompose_MT(events_qc[event_id]["mt DC"])
        events_qc[event_id] = {**events_qc[event_id], **decomp}

for event_id, amps in amp_qc.items():
    mag = [v["magnitude"] for v in events.values() if v["id"] == event_id][0]
    n_true = len([v for k, v in amps.items() if v == "True" and correction[k] == 1])
    n_false = len([v for k, v in amps.items() if v == "False" and correction[k] == 1])
    n_none = len([v for k, v in amps.items() if v == "None" and correction[k] == 1])
    if event_id in events_qc:
        r_value = events_qc[event_id]["r"]
        cn = events_qc[event_id]["cn"]
    else:
        r_value = -999.25
        cn = -999.25
    amp_qc[event_id] = {
        **amp_qc[event_id],
        "magnitude": mag,
        "n_true": n_true,
        "n_false": n_false,
        "n_none": n_none,
        "r": r_value,
        "cn": cn,
    }


mbins = np.arange(-0.8, 0.8, 0.1)
bin = -0.5
visibility, tf_ratio = [], []
m_bin_mid = []
n_all_true, n_all_false = [], []
r_values, cns = [], []
for bin in mbins:
    m_events = {
        k: v
        for k, v in amp_qc.items()
        if v["magnitude"] > bin and v["magnitude"] < bin + 0.1
    }
    if len(m_events) > 0:
        visibility.append(
            100 * np.array([35 - v["n_none"] for v in m_events.values()]) / 35
        )
        tf_ratio.append(
            [
                np.log10(v["n_true"] / v["n_false"])
                for v in m_events.values()
                if v["n_false"] > 0 and v["n_true"] > 0
            ]
        )
        m_bin_mid.append(bin + 0.05)
        r_values.append([v["r"] for v in m_events.values() if v["r"] > 0])
        cns.append([v["cn"] for v in m_events.values() if v["cn"] > 0])
        print(
            {
                k: (v["n_true"], v["n_false"])
                for k, v in m_events.items()
                if v["n_true"] * v["n_false"] == 0
            }
        )


fig1, ax1 = plt.subplots()
ax1.set_facecolor("0.9")
parts = ax1.violinplot(visibility, m_bin_mid, widths=0.1, showmedians=True)
for pc in parts["bodies"]:
    pc.set_facecolor("gold")
    pc.set_edgecolor("0.3")
    pc.set_alpha(1)
parts["cbars"].set_color("0.3")
parts["cmaxes"].set_color("0.3")
parts["cmins"].set_color("0.3")
parts["cmedians"].set_color("0.3")

ax1.set_xlabel("magnitude")
ax1.set_ylabel("percentage of clear picks")
fig1.savefig("mt-qc//picking_visibility.png")


fig2, ax2 = plt.subplots()
ax2.set_facecolor("0.9")
parts = ax2.violinplot(tf_ratio, m_bin_mid, widths=0.1, showmedians=True)
for pc in parts["bodies"]:
    pc.set_facecolor("orangered")
    pc.set_edgecolor("0.3")
    pc.set_alpha(0.7)
parts["cbars"].set_color("0.3")
parts["cmaxes"].set_color("0.3")
parts["cmins"].set_color("0.3")
parts["cmedians"].set_color("0.3")
l3 = np.log10(3)
ax2.set_yticks([-1, -l3, 0, l3, 1])
ax2.set_yticklabels(["10X F", "3X F", "T:F = 1:1", "3X T", "10X T"])
ax2.set_xlabel("magnitude")
x1, x2 = ax2.get_xlim()
ax2.plot([x1, x2], [0, 0], ":", zorder=-2, color="0.1")
ax2.set_xlim([x1, x2])

fig2.savefig("mt-qc//tf_ratio.png")


fig3, ax3 = plt.subplots()
ax3.set_facecolor("0.9")
parts = ax3.violinplot(r_values, m_bin_mid, widths=0.1, showmedians=True)
for pc in parts["bodies"]:
    pc.set_facecolor("turquoise")
    pc.set_edgecolor("0.3")
    pc.set_alpha(0.7)
parts["cbars"].set_color("0.3")
parts["cmaxes"].set_color("0.3")
parts["cmins"].set_color("0.3")
parts["cmedians"].set_color("0.3")

ax3.set_xlabel("r values")

fig3.savefig("mt-qc//r_values.png")


fig4, ax4 = plt.subplots()
ax4.set_facecolor("0.9")
parts = ax4.violinplot(cns, m_bin_mid, widths=0.1, showmedians=True)
for pc in parts["bodies"]:
    pc.set_facecolor("pink")
    pc.set_edgecolor("0.3")
    pc.set_alpha(0.7)
parts["cbars"].set_color("0.3")
parts["cmaxes"].set_color("0.3")
parts["cmins"].set_color("0.3")
parts["cmedians"].set_color("0.3")

ax4.set_xlabel("condition_numbers")

fig3.savefig("mt-qc//condition_numbers.png")
