import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import mplstereonet as mpls

from read_inputs import read_catalog, write_catalog

from plotting_stuff import plot_strike_dip_rake_rosettes
from sms_moment_tensor.MT_math import mt_vector_to_matrix, decompose_MT, mt_to_sdr, clvd_iso_dc

phase1_events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)

f = open("mt-qc//MT_comparison____.csv")
head = [f.readline() for n in range(2)]
lines = f.readlines()
f.close()

events_qc= {}
for line in lines:
    lspl = line.split(",")
    if len(lspl) == 19:
        event_id = lspl[0]
        events_qc[event_id] = {
            "mt DC": mt_vector_to_matrix([float(s) for s in lspl[1:7]]),
            "cn": float(lspl[7]),
            "r": float(lspl[8]),
            "n": int(lspl[9]),
            "magnitude": [v["magnitude"] for v in phase1_events.values() if v["id"] == event_id][0],
        }
        decomp = decompose_MT(events_qc[event_id]["mt DC"])
        nodal_planes = mt_to_sdr(events_qc[event_id]["mt DC"])
        non_dc_decomp =
        phase1_event
        events_qc[event_id] = {**events_qc[event_id],
                        "strike": nodal_planes[0]%360,
                        "dip": nodal_planes[1],
                        "rake": nodal_planes[2],
                        "aux_strike": nodal_planes[3],
                        "aux_dip": nodal_planes[4],
                        "aux_rake": nodal_planes[5]}

        events_qc[event_id] = {**events_qc[event_id], **decomp}

nodal_planes = mt_to_sdr(events_qc[event_id]["mt DC"])
non_dc_decomp =
phase1_event
events_qc[event_id] = {**events_qc[event_id],
                "strike": nodal_planes[0]%360,
                "dip": nodal_planes[1],
                "rake": nodal_planes[2],
                "aux_strike": nodal_planes[3],
                "aux_dip": nodal_planes[4],
                "aux_rake": nodal_planes[5]}
