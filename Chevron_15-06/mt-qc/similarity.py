import matplotlib.pyplot as plt
import numpy as np

from sms_moment_tensor.MT_math import mt_vector_to_matrix
from read_inputs import read_catalog

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)

{v["id"]: v["magnitude"] for k, v in events.items() if v["magnitude"] > 0.5}


f = open("mt-qc//catalog_of_first_motions.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
amp_qc = {}
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[1:]:
        amp_qc[lspl[0]] = {k: v for v, k in zip(lspl[1:], headsplit[1:])}

f = open("mt-qc//MT_comparison.csv")
head = [f.readline() for n in range(2)]
lines = f.readlines()
f.close()
events_qc = {}
for line in lines:
    lspl = line.split(",")
    if len(lspl) == 19:
        event_id = lspl[0]
        events_qc[event_id] = {
            "mt DC": mt_vector_to_matrix([float(s) for s in lspl[1:7]]),
            "cn": float(lspl[7]),
            "r": float(lspl[8]),
            "n": int(lspl[9]),
        }

events_qc["37034555880"]

for event_id, amps in amp_qc.items():
    if event_id in events_qc:
        mag = [v["magnitude"] for v in events.values() if v["id"] == event_id][0]
        n_true = len([v for v in amps.values() if v == "True"])
        n_false = len([v for v in amps.values() if v == "False"])
        n_none = len([v for v in amps.values() if v == "None"])
        phase_1_mechanism = np.array(
            [v["dc MT"] for v in events.values() if v["id"] == event_id][0]
        )
        phase_1_mechanism /= np.linalg.norm(phase_1_mechanism)
        event_mt_qc = events_qc[event_id]["mt DC"]
        event_mt_qc /= np.linalg.norm(event_mt_qc)
        amp_qc[event_id] = {
            **amp_qc[event_id],
            "magnitude": mag,
            "n_true": n_true,
            "n_false": n_false,
            "n_none": n_none,
            "similarity": np.tensordot(phase_1_mechanism, event_mt_qc),
            "confidence": [
                v["mt confidence"] for v in events.values() if v["id"] == event_id
            ][0],
            "r": events_qc[event_id]["r"],
        }

mbins = np.arange(-0.8, 0.8, 0.1)
similarity, abs_similarity, flips = [], [], []
m_bin_mid = []
for bin in mbins:
    m_events = {
        k: v
        for k, v in amp_qc.items()
        if "magnitude" in v and v["magnitude"] > bin and v["magnitude"] < bin + 0.1
    }
    if len(m_events) > 0:
        similarities = np.array(
            [
                v["similarity"]
                for v in m_events.values()
                if "similarity" in v and v["r"] > 0.6
            ]
        )
        if len(similarities) > 0:
            similarity.append(similarities)
            abs_similarity.append(abs(similarities))
            flips.append(np.sign(similarities))
            m_bin_mid.append(bin + 0.05)

fig1, ax1 = plt.subplots()
ax1.set_facecolor("0.9")
parts = ax1.violinplot(similarity, m_bin_mid, widths=0.1, showmedians=True)
for pc in parts["bodies"]:
    pc.set_facecolor("limegreen")
    pc.set_edgecolor("0.3")
    pc.set_alpha(1)
parts["cbars"].set_color("0.3")
parts["cmaxes"].set_color("0.3")
parts["cmins"].set_color("0.3")
parts["cmedians"].set_color("0.3")

ax1.set_xlabel("magnitude")
ax1.set_ylabel("before-after similarity")
fig1.savefig("mt-qc//overall_similarity_high_conf.png")


fig2, ax2 = plt.subplots()
ax2.set_facecolor("0.9")
parts = ax2.violinplot(abs_similarity, m_bin_mid, widths=0.1, showmedians=True)
for pc in parts["bodies"]:
    pc.set_facecolor("deepskyblue")
    pc.set_edgecolor("0.3")
    pc.set_alpha(1)
parts["cbars"].set_color("0.3")
parts["cmaxes"].set_color("0.3")
parts["cmins"].set_color("0.3")
parts["cmedians"].set_color("0.3")

ax2.set_xlabel("magnitude")
ax2.set_ylabel("before-after |similarity|")
fig2.savefig("mt-qc//overall_abs_similarity.png")


#
#
# fig1, ax1 = plt.subplots()
# ax1.set_facecolor("0.9")
# parts = ax1.violinplot(abs_similarity, m_bin_mid, widths=0.1, showmedians=True)
# for pc in parts["bodies"]:
#     pc.set_facecolor("limegreen")
#     pc.set_edgecolor("0.3")
#     pc.set_alpha(1)
# parts["cbars"].set_color("0.3")
# parts["cmaxes"].set_color("0.3")
# parts["cmins"].set_color("0.3")
# parts["cmedians"].set_color("0.3")
#
# ax1.set_xlabel("magnitude")
# ax1.set_ylabel("before-after similarity")
# fig1.savefig("mt-qc//overall_similarity.png")
