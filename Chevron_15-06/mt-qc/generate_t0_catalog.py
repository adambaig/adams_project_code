# import matplotlib
#
# matplotlib.use("Qt5agg")
from copy import deepcopy
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import pytz

from read_inputs import (
    read_events,
    read_catalog,
)


PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)

mt_events = {k: v for k, v in events.items() if v["magnitude"] > -0.8}


f = open("mt_events_with_t0.csv", "w")
f.write("#id,t0,easting (m),northing (m),elevation (m)\n")
for event in mt_events.values():
    f.write(
        f'{event["id"]},{datetime.strftime(event["timestamp"],"%Y-%m-%dT%H:%M:%S.%fZ")},'
        f'{event["easting"]},{event["northing"]},{event["elevation"]}\n'
    )

f.close()
