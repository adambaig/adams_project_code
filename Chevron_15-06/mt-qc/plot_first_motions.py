import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import re
from obspy.imaging.mopad_wrapper import beach

from sms_moment_tensor.MT_math import mt_matrix_to_vector


from plotting_stuff import gray_background_with_grid
from read_inputs import read_catalog, read_stations

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate"
    "_reassignStages_updatedDescription_fancyHeader.csv"
)
stations = read_stations()

events = {v["id"]: v for k, v in events.items() if v["magnitude"] > 0.2}


f = open("mt-qc//catalog_of_first_motions.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
amp_qc = {}
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[1:]:
        amp_qc[lspl[0]] = {k: v for v, k in zip(lspl[1:], headsplit[1:])}
theoretical_polarities = {}
f = open("mt-qc\\theoretical_polarities.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[:1]:
        theoretical_polarities[lspl[0]] = {
            k: float(v) for v, k in zip(lspl[1:], headsplit[1:])
        }

for event in events.values():
    fig, ax = plt.subplots(figsize=[9, 9])
    ax.set_aspect("equal")
    for station_id, station in {
        k: v for k, v in stations.items() if bool(re.search("B...4", k))
    }.items():
        if amp_qc[event["id"]][station_id] == "True":
            manual_polarity = theoretical_polarities[event["id"]][station_id]
        elif amp_qc[event["id"]][station_id] == "False":
            manual_polarity = -theoretical_polarities[event["id"]][station_id]
        else:
            manual_polarity = 0
        if manual_polarity == 0:
            ax.plot(station["e"], station["n"], "o", color="0.9", markeredgecolor="0.2")
        elif manual_polarity > 0:
            ax.plot(
                station["e"],
                station["n"],
                "o",
                color="firebrick",
                markeredgecolor="0.2",
            )
        else:
            ax.plot(
                station["e"],
                station["n"],
                "o",
                color="royalblue",
                markeredgecolor="0.2",
            )
        ax.text(station["e"], station["n"] - 270, station_id, ha="center")
        beachball = beach(
            (event["strike"], event["dip"], event["rake"]),
            xy=(event["easting"], event["northing"]),
            width=350,
            facecolor="firebrick",
            linewidth=0.25,
        )
        ax.add_collection(beachball)
    ax = gray_background_with_grid(ax, grid_spacing=1000)
    fig.savefig(f'mt-qc//first_motion_{event["id"]}.png')
