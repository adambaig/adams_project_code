import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import mplstereonet as mpls

from read_inputs import read_catalog, write_catalog

from plotting_stuff import plot_strike_dip_rake_rosettes
from sms_moment_tensor.MT_math import (
    mt_vector_to_matrix,
    decompose_MT,
    mt_to_sdr,
    unit_vector_to_trend_plunge,
    sdr_to_mt,
    conjugate_sdr,
    strike_dip_to_normal,
)
from sms_moment_tensor.stress_inversions import (
    iterate_Micheal_stress_inversion,
    determine_SH_max,
    decompose_stress,
    Simpson_Aphi,
    resolve_shear_and_normal_stress,
    most_unstable_sdr,
)

PI = np.pi

phase1_events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)

unflipped_events = read_catalog("Catalogs\\unflipped_events_with_treatment_tags.csv")


f = open("mt-qc//MT_comparison____.csv")
head = [f.readline() for n in range(2)]
lines = f.readlines()
f.close()

events_qc = {}
for line in lines:
    lspl = line.split(",")
    if len(lspl) == 19:
        event_id = lspl[0]
        events_qc[event_id] = {
            "mt DC": mt_vector_to_matrix([float(s) for s in lspl[1:7]]),
            "cn": float(lspl[7]),
            "r": float(lspl[8]),
            "n": int(lspl[9]),
            "magnitude": [
                v["magnitude"] for v in phase1_events.values() if v["id"] == event_id
            ][0],
        }
        decomp = decompose_MT(events_qc[event_id]["mt DC"])
        nodal_planes = mt_to_sdr(events_qc[event_id]["mt DC"])
        events_qc[event_id] = {
            **events_qc[event_id],
            **decomp,
            "strike": nodal_planes[0] % 360,
            "dip": nodal_planes[1],
            "rake": nodal_planes[2],
            "aux_strike": nodal_planes[3],
            "aux_dip": nodal_planes[4],
            "aux_rake": nodal_planes[5],
        }

stress_tensor_mti, iterations_mti = iterate_Micheal_stress_inversion(
    {k: v for k, v in events_qc.items() if v["r"] > 0.7 and v["cn"] < 5},
    1000,
    output_iterations=True,
)

stress_tensor_umti, iterations_umti = iterate_Micheal_stress_inversion(
    {k: v for k, v in phase1_events.items() if v["mt confidence"] > 0.95},
    1000,
    output_iterations=True,
)

stress_tensor_unflipped, iterations_unflipped = iterate_Micheal_stress_inversion(
    {k: v for k, v in unflipped_events.items() if v["mt confidence"] > 0.95},
    1000,
    output_iterations=True,
)

stress = {
    "MTI": {
        "tensor": stress_tensor_mti,
        "iterations": iterations_mti,
        "events": events_qc,
    },
    "UMTI": {
        "tensor": stress_tensor_umti,
        "iterations": iterations_umti,
        "events": phase1_events,
    },
    "unflipped": {
        "tensor": stress_tensor_unflipped,
        "iterations": iterations_unflipped,
        "events": unflipped_events,
    },
}


axis_color = {"s1": "firebrick", "s2": "forestgreen", "s3": "royalblue"}
r_bins = np.linspace(0, 1, 101)

circle_x, circle_y = [], []
for i_rad in np.arange(0, PI + PI / 100, PI / 100):
    circle_x.append(np.cos(i_rad))
    circle_y.append(np.sin(i_rad))
circle_x = np.array(circle_x)
circle_y = np.array(circle_y)

for method, stresses in stress.items():
    fig, ax = mpls.subplots()
    f_hist, a_hist = plt.subplots()
    r_iterations, axes_iteration, sh_max_iteration = [], [], []
    for iteration in stresses["iterations"]:
        R, stress_axes = decompose_stress(iteration)
        r_iterations.append(R)
        axes_iteration.append(stress_axes)
        for axis, vector in stress_axes.items():
            trend, plunge = unit_vector_to_trend_plunge(vector)
            ax.line(
                plunge,
                trend,
                ".",
                alpha=0.02,
                color=axis_color[axis],
                zorder=3,
            )
        sh_max_iteration.append(determine_SH_max(iteration))
    ax.grid()
    fig.savefig(f"mt-qc//{method}_stress_axes.png")
    a_hist.hist(
        r_iterations,
        r_bins,
        facecolor="seagreen",
        edgecolor="0.1",
        zorder=2,
    )
    y1, y_max = a_hist.get_ylim()
    a_hist.plot([R, R], [0, y_max], "k")
    a_hist.set_xlim([0, 1])
    a_hist.set_ylim([0, y_max])
    a_hist.set_xlabel("stress ratio, $R = 1 - \phi$")
    a_hist.set_ylabel("count")
    f_hist.savefig(f"mt-qc//{method}_R_histogram.png")
    taus, sigmas = np.zeros(len(stresses["events"])), np.zeros(len(stresses["events"]))
    taus_stable, sigmas_stable = np.zeros(len(stresses["events"])), np.zeros(
        len(stresses["events"])
    )
    for i_event, event in enumerate(stresses["events"].values()):
        dc_mt = sdr_to_mt(event["strike"], event["dip"], event["rake"])
        sdr_unstable = most_unstable_sdr(dc_mt, stresses["tensor"])
        sdr_stable = conjugate_sdr(*sdr_unstable)
        event["true strike"] = sdr_unstable[0]
        event["true dip"] = sdr_unstable[1]
        event["true rake"] = sdr_unstable[2]
        normal_vector = strike_dip_to_normal(sdr_unstable[0], sdr_unstable[1])
        normal_vector_stable = strike_dip_to_normal(sdr_stable[0], sdr_stable[1])
        sigmas[i_event], taus[i_event] = resolve_shear_and_normal_stress(
            stresses["tensor"], normal_vector
        )
        (
            sigmas_stable[i_event],
            taus_stable[i_event],
        ) = resolve_shear_and_normal_stress(stresses["tensor"], normal_vector_stable)
    f_mc, a_mc = plt.subplots(figsize=[8, 4])
    a_mc.set_aspect("equal")
    a_mc.plot(circle_x, circle_y, "0.3", zorder=10)
    a_mc.plot(R * circle_x + 1 - R, R * circle_y, "0.3", zorder=10)
    a_mc.plot((1 - R) * circle_x - R, (1 - R) * circle_y, "0.3", zorder=10)
    a_mc.plot(sigmas, taus, ".", color="indigo", alpha=0.2)
    # a_mc.plot(sigmas_stable, taus_stable, ".", color="lavender", markeredgecolor='k')
    a_mc.arrow(-1.1, 0, 2.2, 0, head_width=0.06, head_length=0.1, fc="k", ec="k")
    a_mc.arrow(-1.1, 0, 0, 1.1, head_width=0.06, head_length=0.1, fc="k", ec="k")
    a_mc.text(-1.1, 1.22, "$\\tau$", ha="center")
    a_mc.text(1.21, 0.0, "${\\sigma}$", va="center")
    a_mc.text(-0.98, -0.1, "$\\sigma_3$")
    a_mc.text(-0.98, -0.1, "$\\sigma_3$")
    a_mc.text(1 - 2 * R + 0.03, -0.1, "$\\sigma_2$")
    a_mc.text(1.02, -0.1, "$\\sigma_1$")
    a_mc.axis("off")
    a_mc.set_xlim(-1.3, 1.3)
    a_mc.set_ylim(-0.15, 1.3)
    f_mc.savefig(f"mt-qc//{method}_mohr_coulomb.png")


determine_SH_max(stress["MTI"]["tensor"])
determine_SH_max(stress["UMTI"]["tensor"])
determine_SH_max(stress["unflipped"]["tensor"])
