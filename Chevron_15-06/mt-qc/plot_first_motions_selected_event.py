import re

import matplotlib.pyplot as plt
import numpy as np
import sys

from obspy.imaging.mopad_wrapper import beach

sys.path.append(r"C:\Users\adambaig\python_modules\sms_pfi_plotting\src")


from generalPlots import gray_background_with_grid
from read_inputs import read_catalog, read_stations

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate"
    "_reassignStages_updatedDescription_fancyHeader.csv"
)
stations = read_stations()

event = [v for v in events.values() if v["id"] == "17213221118"][0]


f = open("mt-qc//catalog_of_first_motions.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
amp_qc = {}
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[1:]:
        amp_qc[lspl[0]] = {k: v for v, k in zip(lspl[1:], headsplit[1:])}
theoretical_polarities = {}
f = open("mt-qc\\theoretical_polarities_check.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[:1]:
        theoretical_polarities[lspl[0]] = {
            k: float(v) for v, k in zip(lspl[1:], headsplit[1:])
        }


fig, ax = plt.subplots(figsize=[9, 9])
fig_th, ax_th = plt.subplots(figsize=[9, 9])

ax.set_aspect("equal")
for station_id, station in {
    k: v for k, v in stations.items() if bool(re.search("B...4", k))
}.items():
    if amp_qc[event["id"]][station_id] == "True":
        manual_polarity = theoretical_polarities[event["id"]][station_id]
    elif amp_qc[event["id"]][station_id] == "False":
        manual_polarity = -theoretical_polarities[event["id"]][station_id]
    else:
        manual_polarity = 0
    if manual_polarity == 0:
        ax.plot(station["e"], station["n"], "o", color="0.9", markeredgecolor="0.2")
    elif manual_polarity > 0:
        ax.plot(
            station["e"], station["n"], "o", color="firebrick", markeredgecolor="0.2"
        )
    else:
        ax.plot(
            station["e"], station["n"], "o", color="royalblue", markeredgecolor="0.2"
        )
    ax.text(station["e"], station["n"] - 270, station_id, ha="center")
    beachball = beach(
        (event["strike"], event["dip"], event["rake"]),
        xy=(event["easting"], event["northing"]),
        width=350,
        facecolor="firebrick",
        linewidth=0.25,
    )
    ax.add_collection(beachball)

for station_id, station in {
    k: v for k, v in stations.items() if bool(re.search("B...4", k))
}.items():

    if theoretical_polarities[event["id"]][station_id] == 0:
        ax_th.plot(station["e"], station["n"], "s", color="0.9", markeredgecolor="0.2")
    elif theoretical_polarities[event["id"]][station_id] > 0:
        ax_th.plot(
            station["e"], station["n"], "s", color="firebrick", markeredgecolor="0.2"
        )
    else:
        ax_th.plot(
            station["e"], station["n"], "s", color="royalblue", markeredgecolor="0.2"
        )
    # ax_th.text(station["e"], station["n"] - 270, station_id, ha="center")
    beachball = beach(
        (event["strike"], event["dip"], event["rake"]),
        xy=(event["easting"], event["northing"]),
        width=350,
        facecolor="firebrick",
        linewidth=0.25,
    )
    ax_th.add_collection(beachball)


for a in [ax, ax_th]:
    x = gray_background_with_grid(a, grid_spacing=1000)
# fig.savefig(f'mt-qc//first_motion_{event["id"]}.png')
