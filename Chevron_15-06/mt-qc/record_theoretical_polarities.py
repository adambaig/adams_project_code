from datetime import datetime
import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import UTCDateTime
import pickle
import pytz
import re
import os

from sms_moment_tensor.moment_tensor_inversion import inversion_matrix_row
from sms_moment_tensor.MT_math import mt_matrix_to_vector
from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.raytrace import isotropic_ray_trace

from read_inputs import read_catalog, get_velocity_model, read_stations


def getVelfromTTT(TTT):
    velocity_model = []
    top = -1 * TTT["originz"]
    d1 = TTT["dz"]
    vel = TTT["vel1D"]
    for vp in vel:
        vs = vp / 1.73205080757
        rho = 310 * (vp**0.25)
        curlayer = {"vp": vp, "top": top, "vs": vs, "rho": rho}
        top = top - d1
        velocity_model.append(curlayer)
    return velocity_model


PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")

Q = {"P": 60, "S": 60}
velocity_model = get_velocity_model()
pickle_TTT = open("optimal-TTT.pkl", "rb")
TTT = pickle.load(pickle_TTT, encoding="latin1")
pickle_TTT.close()
velocity_model = getVelfromTTT(TTT)
velocity_model = get_velocity_model()
catalog_file = "Catalogs/Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
events = read_catalog(catalog_file)
stations = read_stations()

bottom_stations = {k: v for k, v in stations.items() if re.match("B...4", k)}
# g = open("theoretical_polarities.csv", "w")
# g.write("event id")
# [g.write("," + station_name) for station_name in bottom_stations.keys()]
# g.write("\n")


sample_rate = 0.001
time_series = np.arange(0, 4, sample_rate)
selected_events = {
    k: v for k, v in events.items() if v["magnitude"] < 1 and v["magnitude"] > 0.7
}
# selected_events = {k: v for k, v in events.items() if v["id"] == "16094919048"}

for event in selected_events.values():
    print(event["id"])
    source = {
        "e": event["easting"],
        "n": event["northing"],
        "z": event["elevation"],
        "stress_drop": 3e5,
        "moment_magnitude": event["magnitude"],
        "moment_tensor": event["gen MT"],
    }
    # g.write(event["id"])
    mt_vector = np.matrix(mt_matrix_to_vector(source["moment_tensor"])).T
    for station_name, station in bottom_stations.items():
        p_raypath = isotropic_ray_trace(source, station, velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, station, velocity_model, "S")
        waveform = get_response(
            p_raypath, s_raypath, source, station, Q, time_series, 0
        )
        p_tt = p_raypath["traveltime"]
        i_p = int(p_tt / sample_rate)
        i_max = i_p + np.argmax(abs(waveform["z"][i_p - 10 : i_p + 10])) - 10
        polarity = np.sign(waveform["z"][i_max])

        fig, ax = plt.subplots(figsize=[6, 2])
        ax.plot(time_series, waveform["z"])
        ax.plot(time_series[i_max], waveform["z"][i_max], "ro")
        ax.set_ylabel(station_name)


#         g.write("," + str(np.squeeze(np.array(polarity))))
#     g.write("\n")
# g.close()
