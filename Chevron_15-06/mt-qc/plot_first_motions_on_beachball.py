import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
import os
import pickle
import re
from obspy.imaging.mopad_wrapper import beach


from sms_moment_tensor.MT_math import mt_matrix_to_vector, conjugate_sdr
from sms_ray_modelling.raytrace import isotropic_ray_trace


from plotting_stuff import gray_background_with_grid
from read_inputs import read_catalog, read_stations

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate"
    "_reassignStages_updatedDescription_fancyHeader.csv"
)
stations = read_stations()


def getVelfromTTT(TTT):
    velocity_model = []
    top = -1 * TTT["originz"]
    d1 = TTT["dz"]
    vel = TTT["vel1D"]
    for vp in vel:
        vs = vp / 1.73205080757
        rho = 310 * (vp**0.25)
        curlayer = {"vp": vp, "top": top, "vs": vs, "rho": rho}
        top = top - d1
        velocity_model.append(curlayer)
    return velocity_model


event = [v for v in events.values() if v["id"] == "16094919048"][0]
source = {
    "e": event["easting"],
    "n": event["northing"],
    "z": event["elevation"],
    "stress_drop": 3e5,
    "moment_magnitude": event["magnitude"],
    "moment_tensor": event["gen MT"],
}
# velocity_model = get_velocity_model()
pickle_TTT = open("optimal-TTT.pkl", "rb")
TTT = pickle.load(pickle_TTT, encoding="latin1")
pickle_TTT.close()
velocity_model = getVelfromTTT(TTT)
f = open("mt-qc//catalog_of_first_motions.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
amp_qc = {}
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[1:]:
        amp_qc[lspl[0]] = {k: v for v, k in zip(lspl[1:], headsplit[1:])}
theoretical_polarities = {}
f = open("mt-qc\\theoretical_polarities_check.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[:1]:
        theoretical_polarities[lspl[0]] = {
            k: float(v) for v, k in zip(lspl[1:], headsplit[1:])
        }


fig, ax = mpls.subplots()
fig_th, ax_th = mpls.subplots()
strike, dip, rake = event["strike"], event["dip"], event["rake"]
aux_strike, aux_dip, aux_rake = conjugate_sdr(strike, dip, rake)

for station_id, station in {
    k: v for k, v in stations.items() if bool(re.search("B...4", k))
}.items():
    if amp_qc[event["id"]][station_id] == "True":
        manual_polarity = theoretical_polarities[event["id"]][station_id]
    elif amp_qc[event["id"]][station_id] == "False":
        manual_polarity = -theoretical_polarities[event["id"]][station_id]
    else:
        manual_polarity = 0
    p_raypath = isotropic_ray_trace(source, station, velocity_model, "P")
    plunge = (
        180
        * np.arccos(
            np.sqrt(
                p_raypath["hrz_slowness"]["e"] ** 2
                + p_raypath["hrz_slowness"]["n"] ** 2
            )
            * p_raypath["velocity_model_chunk"][0]["v"]
        )
        / np.pi
    )
    trend = (
        180
        * np.arctan2(
            p_raypath["hrz_slowness"]["e"],
            p_raypath["hrz_slowness"]["n"],
        )
        / np.pi
    )

    if manual_polarity == 0:
        ax.line(plunge, trend, "o", color="0.9", markeredgecolor="0.2")
    elif manual_polarity > 0:
        ax.line(plunge, trend, "o", color="firebrick", markeredgecolor="0.2")
    else:
        ax.line(plunge, trend, "o", color="royalblue", markeredgecolor="0.2")
        # ax.plane()
    # ax.text(station["e"], station["n"] - 270, station_id, ha="center")
    # beachball = beach(
    #     (event["strike"], event["dip"], event["rake"]),
    #     xy=(event["easting"], event["northing"]),
    #     width=350,
    #     facecolor="firebrick",
    #     linewidth=0.25,
    # )
    # ax.add_collection(beachball)

for station_id, station in {
    k: v for k, v in stations.items() if bool(re.search("B...4", k))
}.items():
    p_raypath = isotropic_ray_trace(source, station, velocity_model, "P")
    plunge = (
        180
        * np.arccos(
            np.sqrt(
                p_raypath["hrz_slowness"]["e"] ** 2
                + p_raypath["hrz_slowness"]["n"] ** 2
            )
            * p_raypath["velocity_model_chunk"][0]["v"]
        )
        / np.pi
    )
    trend = (
        180
        * np.arctan2(
            p_raypath["hrz_slowness"]["e"],
            p_raypath["hrz_slowness"]["n"],
        )
        / np.pi
    )
    if theoretical_polarities[event["id"]][station_id] == 0:
        ind = ax_th.line(plunge, trend, "o", color="0.9", markeredgecolor="0.2")
    elif theoretical_polarities[event["id"]][station_id] > 0:
        up = ax_th.line(plunge, trend, "o", color="firebrick", markeredgecolor="0.2")
    else:
        down = ax_th.line(plunge, trend, "o", color="royalblue", markeredgecolor="0.2")


f, ax_leg = plt.subplots()

ind = ax_th.line(plunge, trend, "o", color="0.9", markeredgecolor="0.2")
ax_leg.legend([up[0], down[0], ind[0]], ["up", "down", "indeterminate"])
f.savefig("first_motion_legend.png")


for a in [ax, ax_th]:
    x = gray_background_with_grid(a, grid_spacing=1000)
# fig.savefig(f'mt-qc//first_motion_{event["id"]}.png')
