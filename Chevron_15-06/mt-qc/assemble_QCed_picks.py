import glob
import os

QC_directory = "c://"
os.path.isdir(QC_directory)

QC_directory = r"\\192.168.238.150\data3\O&G\Projects\P00061 Chevron 15-06 PFI MT QC\ImageDirectories\complete"
os.path.isdir(QC_directory)


event_csvs = glob.glob(f"{QC_directory}//*//results.csv")

g = open("catalog_of_first_motions.csv", "w")
f = open(event_csvs[0])
lines = f.readlines()
f.close()
g.write("event,")
g.write(",".join([line.split(",")[0].split(".")[0] for line in lines]))
g.write("\n")
event = event_csvs[0]
event.split(os.sep)[-2]
for event in event_csvs:
    f = open(event)
    lines = f.readlines()
    f.close()
    g.write(event.split(os.sep)[-2] + ",")
    g.write(",".join([line.split(",")[-1].split()[0] for line in lines]))
    g.write("\n")

g.close()
