import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import mplstereonet as mpls

from read_inputs import read_catalog

from plotting_stuff import plot_strike_dip_rake_rosettes
from sms_moment_tensor.MT_math import mt_vector_to_matrix, decompose_MT

phase1_events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)

f = open("mt-qc//MT_comparison____.csv")
head = [f.readline() for n in range(2)]
lines = f.readlines()
f.close()

r_cutoff = 0.7
m_cutoff = -1
cn_cutoff = 5
events_qc, events_no_qc = {}, {}
for line in lines:
    lspl = line.split(",")
    if len(lspl) == 19:
        event_id = lspl[0]
        events_qc[event_id] = {
            "mt DC": mt_vector_to_matrix([float(s) for s in lspl[1:7]]),
            "cn": float(lspl[7]),
            "r": float(lspl[8]),
            "n": int(lspl[9]),
            "magnitude": [
                v["magnitude"] for v in phase1_events.values() if v["id"] == event_id
            ][0],
        }
        decomp = decompose_MT(events_qc[event_id]["mt DC"])
        events_qc[event_id] = {**events_qc[event_id], **decomp}
        events_no_qc[event_id] = {
            "mt DC": mt_vector_to_matrix([float(s) for s in lspl[10:16]]),
            "cn": float(lspl[16]),
            "r": float(lspl[17]),
            "n": int(lspl[18]),
        }
        decomp = decompose_MT(events_no_qc[event_id]["mt DC"])
        events_no_qc[event_id] = {**events_no_qc[event_id], **decomp}
        phase1_event_key = {
            v["id"]: v for v in phase1_events.values() if v["id"] == event_id
        }
        events_no_qc[event_id] = [
            v for v in phase1_events.values() if v["id"] == event_id
        ][0]


def make_simple_gradient_from_white_cmap(end_color):
    red, green, blue = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, 1.0, 1.0), (1, red, red)],
        "green": [(0.0, 1.0, 1.0), (1, green, green)],
        "blue": [(0.0, 1.0, 1.0), (1, blue, blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


events_no_qc[event_id]

rbins = np.arange(0, 1, 0.02)

fig_r, ax_r = plt.subplots()
plt.hist([v["r"] for v in events_qc.values()], bins=rbins, color="olive", edgecolor="k")
ax_r.set_xlabel("Pearson R")
ax_r.set_ylabel("count")
fig_r.savefig("QC_ed_polariries.png")

len(
    {
        k: v
        for k, v in events_qc.items()
        if v["r"] > 0.7 and v["magnitude"] > m_cutoff and v["cn"] < 5
    },
)
fig_sdr_qc = plot_strike_dip_rake_rosettes(
    {
        k: v
        for k, v in events_qc.items()
        if v["r"] > r_cutoff and v["magnitude"] > m_cutoff and v["cn"] < cn_cutoff
    },
    color="olive",
)
fig_sdr_no_qc = plot_strike_dip_rake_rosettes(
    {
        k: v
        for k, v in events_no_qc.items()
        if v["mt confidence"] > 0.95
        # if events_qc[k]["r"] > r_cutoff and events_qc[k]["magnitude"] > m_cutoff and events_qc[k]["cn"] < cn_cutoff
    },
    mt_key="dc MT",
)

# fig_qc.savefig('mt-qc//all_events_qc.png')
fig_sdr_qc.savefig("mt-qc//events_qc.png")
fig_sdr_no_qc.savefig("mt-qc//events_no_qc.png")
fig_pbt_qc, (ax_p, ax_b, ax_t) = mpls.subplots(1, 3, figsize=[16, 5])
ax_p.density_contourf(
    [
        v["p_plunge"]
        for v in events_qc.values()
        if v["r"] > r_cutoff and v["magnitude"] > m_cutoff and v["cn"] < cn_cutoff
    ],
    [
        v["p_trend"]
        for v in events_qc.values()
        if v["r"] > r_cutoff and v["magnitude"] > m_cutoff and v["cn"] < cn_cutoff
    ],
    measurement="lines",
    cmap=make_simple_gradient_from_white_cmap("olive"),
)
ax_b.density_contourf(
    [
        v["b_plunge"]
        for v in events_qc.values()
        if v["r"] > r_cutoff and v["magnitude"] > m_cutoff and v["cn"] < cn_cutoff
    ],
    [
        v["b_trend"]
        for v in events_qc.values()
        if v["r"] > r_cutoff and v["magnitude"] > m_cutoff and v["cn"] < cn_cutoff
    ],
    measurement="lines",
    cmap=make_simple_gradient_from_white_cmap("olive"),
)
ax_t.density_contourf(
    [
        v["t_plunge"]
        for v in events_qc.values()
        if v["r"] > r_cutoff and v["magnitude"] > m_cutoff and v["cn"] < cn_cutoff
    ],
    [
        v["t_trend"]
        for v in events_qc.values()
        if v["r"] > r_cutoff and v["magnitude"] > m_cutoff and v["cn"] < cn_cutoff
    ],
    measurement="lines",
    cmap=make_simple_gradient_from_white_cmap("olive"),
)
ax_p.line(
    [
        v["p_plunge"]
        for v in events_qc.values()
        if v["r"] > r_cutoff and v["magnitude"] > m_cutoff and v["cn"] < cn_cutoff
    ],
    [
        v["p_trend"]
        for v in events_qc.values()
        if v["r"] > r_cutoff and v["magnitude"] > m_cutoff and v["cn"] < cn_cutoff
    ],
    "k.",
    alpha=0.4,
)
ax_b.line(
    [
        v["b_plunge"]
        for v in events_qc.values()
        if v["r"] > r_cutoff and v["magnitude"] > m_cutoff and v["cn"] < cn_cutoff
    ],
    [
        v["b_trend"]
        for v in events_qc.values()
        if v["r"] > r_cutoff and v["magnitude"] > m_cutoff and v["cn"] < cn_cutoff
    ],
    "k.",
    alpha=0.4,
)
ax_t.line(
    [
        v["t_plunge"]
        for v in events_qc.values()
        if v["r"] > r_cutoff and v["magnitude"] > m_cutoff and v["cn"] < cn_cutoff
    ],
    [
        v["t_trend"]
        for v in events_qc.values()
        if v["r"] > r_cutoff and v["magnitude"] > m_cutoff and v["cn"] < cn_cutoff
    ],
    "k.",
    alpha=0.4,
)
fig_pbt_qc.savefig("mt-qc//strain_qc.png")
fig_pbt_no_qc, (ax_p, ax_b, ax_t) = mpls.subplots(1, 3, figsize=[16, 5])
ax_p.density_contourf(
    [
        v["p_plunge"]
        for v in events_no_qc.values()
        if v["mt confidence"] > 0.95
        # if v["dc R"] > r_cutoff and v["magnitude"] > m_cutoff and v["dc CN"] < cn_cutoff
    ],
    [
        v["p_trend"]
        for v in events_no_qc.values()
        if v["mt confidence"] > 0.95
        # if v["dc R"] > r_cutoff and v["magnitude"] > m_cutoff and v["dc CN"] < cn_cutoff
    ],
    measurement="lines",
    cmap=make_simple_gradient_from_white_cmap("steelblue"),
)
ax_b.density_contourf(
    [
        v["b_plunge"]
        for v in events_no_qc.values()
        if v["mt confidence"] > 0.95
        # if v["dc R"] > r_cutoff and v["magnitude"] > m_cutoff and v["dc CN"] < cn_cutoff
    ],
    [
        v["b_trend"]
        for v in events_no_qc.values()
        if v["mt confidence"] > 0.95
        # if v["dc R"] > r_cutoff and v["magnitude"] > m_cutoff and v["dc CN"] < cn_cutoff
    ],
    measurement="lines",
    cmap=make_simple_gradient_from_white_cmap("steelblue"),
)
ax_t.density_contourf(
    [
        v["t_plunge"]
        for v in events_no_qc.values()
        if v["mt confidence"] > 0.95
        # if v["dc R"] > r_cutoff and v["magnitude"] > m_cutoff and v["dc CN"] < cn_cutoff
    ],
    [
        v["t_trend"]
        for v in events_no_qc.values()
        if v["mt confidence"] > 0.95
        # if v["dc R"] > r_cutoff and v["magnitude"] > m_cutoff and v["dc CN"] < cn_cutoff
    ],
    measurement="lines",
    cmap=make_simple_gradient_from_white_cmap("steelblue"),
)
ax_p.line(
    [
        v["p_plunge"]
        for v in events_no_qc.values()
        if v["mt confidence"] > 0.95
        # if v["dc R"] > r_cutoff and v["magnitude"] > m_cutoff and v["dc CN"] < cn_cutoff
    ],
    [
        v["p_trend"]
        for v in events_no_qc.values()
        if v["mt confidence"] > 0.95
        # if v["dc R"] > r_cutoff and v["magnitude"] > m_cutoff and v["dc CN"] < cn_cutoff
    ],
    "k.",
    alpha=0.4,
)
ax_b.line(
    [
        v["b_plunge"]
        for v in events_no_qc.values()
        if v["mt confidence"] > 0.95
        # if v["dc R"] > r_cutoff and v["magnitude"] > m_cutoff and v["dc CN"] < cn_cutoff
    ],
    [
        v["b_trend"]
        for v in events_no_qc.values()
        if v["mt confidence"] > 0.95
        # if v["dc R"] > r_cutoff and v["magnitude"] > m_cutoff and v["dc CN"] < cn_cutoff
    ],
    "k.",
    alpha=0.4,
)
ax_t.line(
    [
        v["t_plunge"]
        for v in events_no_qc.values()
        if v["mt confidence"] > 0.95
        # if v["dc R"] > r_cutoff and v["magnitude"] > m_cutoff and v["dc CN"] < cn_cutoff
    ],
    [
        v["t_trend"]
        for v in events_no_qc.values()
        if v["mt confidence"] > 0.95
        # if v["dc R"] > r_cutoff and v["magnitude"] > m_cutoff and v["dc CN"] < cn_cutoff
    ],
    "k.",
    alpha=0.4,
)
fig_pbt_no_qc.savefig("mt-qc//strain_no_qc.png")
