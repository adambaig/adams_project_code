from datetime import datetime
import glob
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import pytz
import os

from read_new_catalogs import (
    read_event_catalog,
    read_noise_catalog,
    read_matches,
)

import sys

sys.path.append("C:\\Users\\adambaig\\Project\\Chevron_15-06\\")

from read_inputs import read_catalog
from scipy.stats import linregress

tab10 = cm.get_cmap("tab10")

utc = pytz.utc
starttime = utc.localize(datetime(2019, 12, 11, 0, 0))
endtime = utc.localize(datetime(2019, 12, 13, 0, 0))
final_catalog = (
    "C:\\Users\\adambaig\\Project\\Chevron_15-06\\Catalogs\\"
    "Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
refernce_events = {
    k: v
    for k, v in read_catalog(final_catalog).items()
    if v["timestamp"] > starttime and v["timestamp"] < endtime
}


event_catalogs = glob.glob("MTs//*//relocCatalog*.csv")
paired_catalogs = glob.glob("paired_catalogs/*.csv")
event_times = {}
events = {}
for catalog in event_catalogs:
    dataset = catalog.split("\\")[1].lower()
    events[dataset] = read_event_catalog(catalog)
    event_times[dataset] = {
        k: utc.localize(v["datetime"]) for k, v in events[dataset].items()
    }

matched_ref_id = {}
for catalog in paired_catalogs:
    dataset = os.path.basename(catalog).split("_concat")[0]
    matched_ref_id[dataset] = read_matches(catalog)


def matched_event(id_key):
    return [v for v in refernce_events.values() if v["id"] == str(id_key)][0]


model_quad = -2.213766340e-01  ### from feasibility modelling
model_linear = 2.642608670e00
y_ints = {
    "borehole_all": -3.627676827824499,
    "borehole_deepest_1": -3.682730919082789,
    "borehole_deepest_2": -3.816847254999418,
    "borehole_deepest_3": -3.6090188736626057,
    "surface": -3.6852762135109245,
}


def mag_from_image_amp_old(event, dataset):
    return min(
        np.roots(
            [
                model_quad,
                model_linear,
                y_ints[dataset] - np.log10(event["simple_stack_amp"]),
            ]
        )
    )


magnitude_bins = np.arange(-1.7, 0, 0.1)
fig, ax = plt.subplots(2, 3, sharex=True, figsize=[16, 8])
patches, labels = [], []
for i_dataset, (tag, dataset) in enumerate(matched_ref_id.items()):
    i_row = (i_dataset + 1) // 3
    i_col = (i_dataset + 1) % 3
    orig_mag = [matched_event(k)["magnitude"] for k in dataset.keys()]
    amplitudes = [events[tag][k]["simple_stack_amp"] for k in dataset.values()]

    magnitudes = [
        min(np.roots([model_quad, model_linear, y_ints[tag] - np.log10(a)]))
        for a in amplitudes
    ]

    # ax.set_title(tag.replace('_',' '))
    ax[i_row, i_col].plot(orig_mag, magnitudes, ".", color=tab10(i_dataset / 10 + 0.01))
    ax[i_row, i_col].set_title(tag.replace("_", " "))
    ax[i_row, i_col].set_ylabel("subset catalog magnitude")
    ax[i_row, i_col].set_xlabel("full catalog magnitude")
    line_params = linregress(orig_mag, magnitudes)
    print(line_params.slope)
ax[0, 0].legend(patches, labels)


fig.savefig("mag_check.png", bbox_inches="tight")
#
#
# quad = -6.409776621e-03
# linear = 2.760859301e00
#
# fig_syn, ax_syn = plt.subplots()
# calibration_curve = np.zeros(len(magnitude_bins))
# for i_bin, bin in enumerate(magnitude_bins):
#     calibration_curve[i_bin] = quad * bin * bin + linear * bin
# ax_syn.semilogy(magnitude_bins, 10 ** (calibration_curve), "orangered", lw=2)
# ax_syn.set_xlabel("magnitude")
# ax_syn.set_ylabel("relative amplitude")
# ax_syn.set_title("Synthetic Calibration Curve")
# fig_syn.savefig("calibration_curve.png", bbox_inches="tight", transparent=True)
