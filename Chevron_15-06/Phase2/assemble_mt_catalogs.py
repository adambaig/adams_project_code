from datetime import datetime
import glob
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib import cm
import mplstereonet as mpls
import pytz
import os

from read_new_catalogs import (
    read_event_catalog,
    read_noise_catalog,
    read_matches,
)

from sms_moment_tensor.MT_math import decompose_MT

import sys

sys.path.append("C:\\Users\\adambaig\\Project\\Chevron_15-06\\")

from read_inputs import read_catalog, read_wells
from plotting_stuff import (
    gray_background_with_grid,
    plot_strike_dip_rake_rosettes,
)

subset_colors = {
    "surface": "#1f77b4",
    "borehole_deepest_1": "#ff7f0e",
    "borehole_deepest_2": "#2ca02c",
    "borehole_deepest_3": "#d62728",
    "borehole_all": "#9467bd",
    "borehole_level_1": "#8c564b",
    "borehole_level_2": "#e377c2",
    "borehole_level_3": "#bcbd22",
    "borehole_level_4": "#ff7f0e",  # same as borehole_deepest_1
    "reference": "#17becf",
}


tab10 = cm.get_cmap("tab10")

utc = pytz.utc
starttime = utc.localize(datetime(2019, 12, 11, 0, 0))
endtime = utc.localize(datetime(2019, 12, 13, 0, 0))
final_catalog = (
    "C:\\Users\\adambaig\\Project\\Chevron_15-06\\Catalogs\\"
    "Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
refernce_events = {
    k: v
    for k, v in read_catalog(final_catalog).items()
    if v["timestamp"] > starttime and v["timestamp"] < endtime
}

wells = read_wells()
event_catalogs = glob.glob("MTs//*//flipped_catalog.csv")
paired_catalogs = glob.glob("paired_catalogs/*.csv")
event_times = {}
events = {}
for catalog in event_catalogs:
    dataset = catalog.split("\\")[1].lower()
    events[dataset] = read_event_catalog(catalog, conf=True)
    event_times[dataset] = {
        k: utc.localize(v["datetime"]) for k, v in events[dataset].items()
    }

matched_ref_id = {}
for catalog in paired_catalogs:
    dataset = os.path.basename(catalog).split("_concat")[0]
    matched_ref_id[dataset] = read_matches(catalog)


def matched_event(id_key):
    return [v for v in refernce_events.values() if v["id"] == str(id_key)][0]


def similarity(mt1, mt2):
    return np.tensordot(mt1, mt2) / np.linalg.norm(mt1) / np.linalg.norm(mt2)


def make_simple_gradient_from_white_cmap(end_color):
    red, green, blue = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, 1.0, 1.0), (1, red, red)],
        "green": [(0.0, 1.0, 1.0), (1, green, green)],
        "blue": [(0.0, 1.0, 1.0), (1, blue, blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


event_datasets = {}
for event in refernce_events.values():
    id_number = int(event["id"])
    event_datasets[id_number] = {}
    event_datasets[id_number]["reference"] = matched_event(id_number)
    for dataset in events.keys():
        if id_number in matched_ref_id[dataset]:
            event_datasets[id_number][dataset] = events[dataset][
                matched_ref_id[dataset][id_number]
            ]

# for event in event_datasets.values():
#     ref_mt = event["reference"]["dc MT"]
#     subsets = [k for k in event.keys() if k != "reference"]
#     for dataset in subsets:
#         if np.tensordot(ref_mt, event[dataset]["dc MT"]) < 0:
#             event[dataset]["flipped"] = True
#             event[dataset]["flip MT"] = -event[dataset]["dc MT"]
#         else:
#             event[dataset]["flipped"] = False
#             event[dataset]["flip MT"] = event[dataset]["dc MT"]
#         event[dataset]["reference similarity"] = similarity(
#             ref_mt, event[dataset]["flip MT"]
#         )

for tag, dataset in events.items():
    mts = [v["dc MT"] for v in dataset.values() if v["mt confidence"] >= 0.95]
    n_mt = len(mts)
    p_trend, p_plunge = np.zeros(n_mt), np.zeros(n_mt)
    b_trend, b_plunge = np.zeros(n_mt), np.zeros(n_mt)
    t_trend, t_plunge = np.zeros(n_mt), np.zeros(n_mt)
    for i_mt, mt in enumerate(mts):
        decomp = decompose_MT(mt)
        p_trend[i_mt] = decomp["p_trend"]
        p_plunge[i_mt] = decomp["p_plunge"]
        b_trend[i_mt] = decomp["b_trend"]
        b_plunge[i_mt] = decomp["b_plunge"]
        t_trend[i_mt] = decomp["t_trend"]
        t_plunge[i_mt] = decomp["t_plunge"]
    f_pbt, (a_p_axes, a_b_axes, a_t_axes) = mpls.subplots(1, 3, figsize=[16, 5])
    a_p_axes.line(p_plunge, p_trend, "k.")
    a_b_axes.line(b_plunge, b_trend, "k.")
    a_t_axes.line(t_plunge, t_trend, "k.")
    a_p_axes.density_contourf(
        p_plunge,
        p_trend,
        measurement="lines",
        cmap=make_simple_gradient_from_white_cmap(subset_colors[tag]),
        zorder=2,
    )
    a_b_axes.density_contourf(
        b_plunge,
        b_trend,
        measurement="lines",
        cmap=make_simple_gradient_from_white_cmap(subset_colors[tag]),
        zorder=2,
    )
    a_t_axes.density_contourf(
        t_plunge,
        t_trend,
        measurement="lines",
        cmap=make_simple_gradient_from_white_cmap(subset_colors[tag]),
        zorder=2,
    )
    f_pbt.savefig(f"MTs//figures//pbt_{tag}.png")

    f_sdr = plot_strike_dip_rake_rosettes(
        {k: v for k, v in dataset.items() if v["mt confidence"] >= 0.95},
        mt_key="dc MT",
        color=subset_colors[tag],
    )
    f_sdr.savefig(f"MTs//figures//sdr_{tag}.png")
    f_map, a_map = plt.subplots(figsize=[10, 10])
    a_map.set_aspect("equal")
    for event in dataset.values():
        a_map.plot(event["easting"], event["northing"], ".", color="0.6", zorder=1)
    for event in [v for v in dataset.values() if v["mt confidence"] > 0.95]:
        a_map.plot(
            event["easting"],
            event["northing"],
            ".",
            color=subset_colors[tag],
            markeredgecolor="k",
            zorder=3,
            lw=0.25,
            ms=16,
        )
    x1, x2 = a_map.get_xlim()
    y1, y2 = a_map.get_ylim()

    for well in wells.values():
        a_map.plot(well["easting"], well["northing"], color="0.07", zorder=-1)
    a_map.set_xlim(x1, x2)
    a_map.set_ylim(y1, y2)
    a_map = gray_background_with_grid(a_map)
    f_map.savefig(f"MTs//figures//map_{tag}.png", bbox_inches="tight")

    # reverse_id_lookup = {v: k for k, v in matched_ref_id[tag].items()}
    # ref_ids = [reverse_id_lookup[k] for k in dataset.keys()]
    # ref_confidence = [matched_event(r)["mt confidence"] for r in ref_ids]
    # min_conf = [
    #     min(conf1, conf2)
    #     for conf1, conf2 in zip(
    #         ref_confidence, [v["mt confidence"] for v in dataset.values()]
    #     )
    # ]
    # f_smlr, a_smlr = plt.subplots()
    # a_smlr.plot(
    #     [v["reference similarity"] for v in dataset.values()], min_conf, "."
    # )
    # a_smlr.set_xlabel("similarity")
    # a_smlr.set_ylabel("minimum confidence of pair")
    # f_smlr.savefig(f"MTs//figures//reference_similarity_{tag}.png")


mts = [v["dc MT"] for v in refernce_events.values() if v["mt confidence"] >= 0.95]
n_mt = len(mts)
p_trend, p_plunge = np.zeros(n_mt), np.zeros(n_mt)
b_trend, b_plunge = np.zeros(n_mt), np.zeros(n_mt)
t_trend, t_plunge = np.zeros(n_mt), np.zeros(n_mt)
for i_mt, mt in enumerate(mts):
    decomp = decompose_MT(mt)
    p_trend[i_mt] = decomp["p_trend"]
    p_plunge[i_mt] = decomp["p_plunge"]
    b_trend[i_mt] = decomp["b_trend"]
    b_plunge[i_mt] = decomp["b_plunge"]
    t_trend[i_mt] = decomp["t_trend"]
    t_plunge[i_mt] = decomp["t_plunge"]
f_pbt, (a_p_axes, a_b_axes, a_t_axes) = mpls.subplots(1, 3, figsize=[16, 5])
a_p_axes.line(p_plunge, p_trend, "k.")
a_b_axes.line(b_plunge, b_trend, "k.")
a_t_axes.line(t_plunge, t_trend, "k.")
a_p_axes.density_contourf(
    p_plunge,
    p_trend,
    measurement="lines",
    cmap=make_simple_gradient_from_white_cmap(subset_colors["reference"]),
    zorder=2,
)
a_b_axes.density_contourf(
    b_plunge,
    b_trend,
    measurement="lines",
    cmap=make_simple_gradient_from_white_cmap(subset_colors["reference"]),
    zorder=2,
)
a_t_axes.density_contourf(
    t_plunge,
    t_trend,
    measurement="lines",
    cmap=make_simple_gradient_from_white_cmap(subset_colors["reference"]),
    zorder=2,
)
f_pbt.savefig(f"MTs//figures//pbt_reference.png")

f_sdr = plot_strike_dip_rake_rosettes(
    {k: v for k, v in refernce_events.items() if v["mt confidence"] >= 0.95},
    mt_key="dc MT",
    color=subset_colors["reference"],
)
f_sdr.savefig(f"MTs//figures//sdr_reference.png")

f_map, a_map = plt.subplots(figsize=[10, 10])
a_map.set_aspect("equal")
for event in refernce_events.values():
    a_map.plot(event["easting"], event["northing"], ".", color="0.6", zorder=1)
for event in [v for v in refernce_events.values() if v["mt confidence"] > 0.95]:
    a_map.plot(
        event["easting"],
        event["northing"],
        ".",
        color=subset_colors["reference"],
        markeredgecolor="k",
        zorder=3,
        lw=0.25,
        ms=16,
    )
x1, x2 = a_map.get_xlim()
y1, y2 = a_map.get_ylim()

for well in wells.values():
    a_map.plot(well["easting"], well["northing"], color="0.07", zorder=-1)
a_map.set_xlim(x1, x2)
a_map.set_ylim(y1, y2)
a_map = gray_background_with_grid(a_map)
f_map.savefig(f"MTs//figures//map_reference.png", bbox_inches="tight")
