from datetime import datetime
import glob
import json
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
from obspy.imaging.mopad_wrapper import beach
import os
import pytz
from sklearn.cluster import AgglomerativeClustering

import sys

sys.path.append("..")
from read_inputs import read_events, read_wells, read_catalog, write_catalog
from read_new_catalogs import read_event_catalog, write_event_catalog

from sms_moment_tensor.MT_math import (
    decompose_MT,
    trend_plunge_to_unit_vector,
    mt_to_sdr,
    clvd_iso_dc,
    mt_matrix_to_vector,
)
from plotting_stuff import gray_background_with_grid

from pfi_qi.QI_analysis import moment, scaled_mt

utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")


def norm_MT(mt):
    return mt / np.linalg.norm(mt)


def moment(event):
    return 10 ** (1.5 * event["magnitude"] + 9)


def scaled_mt(mt, moment_value):
    moment = 10 ** (1.5 * event["magnitude"] + 9)
    return np.sqrt(2) * mt * moment / np.linalg.norm(event["dc MT"])


def make_simple_gradient_from_white_cmap(end_color):
    red, green, blue = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, 1.0, 1.0), (1, red, red)],
        "green": [(0.0, 1.0, 1.0), (1, green, green)],
        "blue": [(0.0, 1.0, 1.0), (1, blue, blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


dataset = "surface"
events = read_event_catalog(
    glob.glob(f"MTs//{dataset}//relocCatalog*.csv")[0], conf=True
)
out_catalog = f"MTs//{dataset}//flipped_catalog.csv"
good_events = {k: v for (k, v) in events.items() if v["mt confidence"] > 0.9}
n_mt = len(good_events)
pair_json = f"mt_dot_product_pairs_{dataset}.json"
if os.path.exists(pair_json):
    with open(pair_json, "r") as read_file:
        dot_product_dict = json.load(read_file)
    dot_product_pairs = np.array(dot_product_dict["dot_product_pairs"])
else:
    dot_product_pairs = np.zeros([n_mt, n_mt])
    for i1, (id1, mt1) in enumerate(good_events.items()):
        for i2, (id2, mt2) in enumerate(good_events.items()):
            dc1_norm = mt1["dc MT"] / np.linalg.norm(mt1["dc MT"])
            dc2_norm = mt2["dc MT"] / np.linalg.norm(mt2["dc MT"])
            dot_product_pairs[i1, i2] = np.tensordot(dc1_norm, dc2_norm)
    dot_product_dict = {"dot_product_pairs": dot_product_pairs.tolist()}
    with open(pair_json, "w") as write_file:
        json.dump(dot_product_dict, write_file)


distance_mat = abs(1 - abs(dot_product_pairs))
n_clusters = 8
clustering = AgglomerativeClustering(
    n_clusters=n_clusters, affinity="precomputed", linkage="complete"
).fit(distance_mat)
labels = clustering.labels_
for i_mt, (id, mt) in enumerate(good_events.items()):
    mt["mt cluster"] = labels[i_mt]
colors = [
    "firebrick",
    "forestgreen",
    "darkgoldenrod",
    "steelblue",
    "indigo",
    "darkorange",
    "turquoise",
    "black",
]

flip = [1, -1, -1, -1, -1, 1, -1, 1]
for i_cluster in range(n_clusters):
    mt_cluster = {k: v for k, v in good_events.items() if v["mt cluster"] == i_cluster}

    custom_cmap = make_simple_gradient_from_white_cmap(colors[i_cluster])
    mt_avg = flip[i_cluster] * sum(
        [v["dc MT"] / np.linalg.norm(v["dc MT"]) for k, v in mt_cluster.items()]
    )

    p_trend, b_trend, t_trend = [], [], []
    p_plunge, b_plunge, t_plunge = [], [], []

    for id, mt in mt_cluster.items():
        if np.tensordot(mt_avg, mt["dc MT"]) < 0:
            mt["dc MT"] = -mt["dc MT"]
            mt["general MT"] = -mt["general MT"]
        decomp = decompose_MT(mt["dc MT"])
        p_trend.append(decomp["p_trend"])
        b_trend.append(decomp["b_trend"])
        t_trend.append(decomp["t_trend"])
        p_plunge.append(decomp["p_plunge"])
        b_plunge.append(decomp["b_plunge"])
        t_plunge.append(decomp["t_plunge"])
    f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = a3[0].density_contourf(
        p_plunge, p_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_b = a3[1].density_contourf(
        b_plunge, b_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_t = a3[2].density_contourf(
        t_plunge, t_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )

low_q_events = {k: v for (k, v) in events.items() if v["mt confidence"] <= 0.9}

cluster_mts = [
    sum(
        mt["dc MT"] / np.linalg.norm(mt["dc MT"])
        for mt in good_events.values()
        if mt["mt cluster"] == ii
    )
    for ii in range(n_clusters)
]

for event in low_q_events.values():
    dot_products = np.zeros(n_clusters)
    for i_cluster in range(n_clusters):
        dot_products[i_cluster] = np.tensordot(
            norm_MT(event["dc MT"]), norm_MT(cluster_mts[i_cluster])
        )
    i_cluster_belongs = np.argmax(abs(dot_products))
    if dot_products[i_cluster_belongs] < 0:
        event["dc MT"] = -event["dc MT"]
        event["general MT"] = -event["general MT"]
    event["mt cluster"] = i_cluster_belongs

if False:
    catalog = write_event_catalog(events, out_catalog)
