from datetime import datetime
import glob
import matplotlib.pyplot as plt
import numpy as np
import os
import pytz

utc = pytz.utc

import sys

sys.path.append("..")

from read_inputs import read_events_from_Do

subset_colors = {
    "surface": "#1f77b4",
    "borehole_deepest_1": "#ff7f0e",
    "borehole_deepest_2": "#2ca02c",
    "borehole_deepest_3": "#d62728",
    "borehole_all": "#9467bd",
    "borehole_level_1": "#8c564b",
    "borehole_level_2": "#e377c2",
    "borehole_level_3": "#bcbd22",
    "borehole_level_4": "#ff7f0e",  # same as borehole_deepest_1
    "reference": "#17becf",
    "all": "#ffffff",
}


def lighten_color(color, amount=0.5):
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys

    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])


baseline_spec_fit_out_file = (
    r"C:\Users\adambaig\Project\Chevron_15-06"
    r"\5_Spectral_Fitting\b1_-1.0\Tables\Do_Final_fixing_time.csv"
)

subset_specfit_data_files = glob.glob("SpecFit//Results//[!5]*//Tables//Do_Final.csv")
all_events = read_events_from_Do(baseline_spec_fit_out_file, ignore_qc_flag=True)


baseline_data = {
    k: v
    for k, v in all_events.items()
    if v["UTC"] > datetime(2019, 12, 11, 0, 0)
    and v["UTC"] < datetime(2019, 12, 13, 0, 0)
}

colors = {
    "yy": "indigo",
    "yn": "#FF4444",
    "ny": "#4444FF",
    "nn": "white",
}
zorder = {
    "yy": 9,
    "yn": 8,
    "ny": 7,
    "nn": 6,
}

for file in subset_specfit_data_files:
    subset_specfit = read_events_from_Do(file, ignore_qc_flag=True)
    dataset = os.path.split(file)[0].split(os.sep)[-2]
    data_label = dataset.replace("_", " ")
    Mw = np.array([v["Mw"] for v in subset_specfit.values() if "Mw" in v])
    base_Mw = np.array(
        [baseline_data[k]["Mw"] for k, v in subset_specfit.items() if "Mw" in v]
    )
    qc_flags_both = [
        baseline_data[k]["keep"] + v["keep"]
        for k, v in subset_specfit.items()
        if "Mw" in v
    ]
    fig, ax = plt.subplots()
    ax.set_facecolor(
        lighten_color(
            subset_colors[dataset],
            amount=0.1,
        )
    )
    ax.set_aspect("equal")
    for double_flag in ["yy", "yn", "ny", "nn"]:
        i_flag = [i for i, flag in enumerate(qc_flags_both) if flag == double_flag]
        ax.plot(
            base_Mw[i_flag],
            Mw[i_flag],
            "o",
            color=colors[double_flag],
            markeredgecolor="k",
            zorder=zorder[double_flag],
        )
    # x1, x2 = ax.get_xlim()
    # y1, y2 = ax.get_ylim()

    i_yy = [i for i, flag in enumerate(qc_flags_both) if flag == "yy"]
    mw_shift = np.median(Mw[i_yy] - base_Mw[i_yy])
    ax.text(-0.9, 0.1, f"median shift\n{mw_shift:.2f} units".replace("-", "$-$"))
    min_xy = -0.95
    max_xy = 0.25
    ax.plot([min_xy, max_xy], [min_xy, max_xy], ":", color="0.1", zorder=-1)
    ax.set_xlabel("reference Mw")
    ax.set_ylabel(f"{data_label} Mw")
    ax.set_xlim(min_xy, max_xy)
    ax.set_ylim(min_xy, max_xy)
    fig.savefig(f"SpecFit//Mw_comparison_{dataset}.png", bbox_inches="tight")


f_legend, a_legend = plt.subplots(figsize=[12, 12])
for double_flag in ["yy", "yn", "ny", "nn"]:
    (p,) = a_legend.plot(
        0,
        0,
        "o",
        color=colors[double_flag],
        markeredgecolor="k",
        zorder=zorder[double_flag],
    )

a_legend.legend(
    [
        "both passed QC",
        "reference passed QC only",
        "subset passed QC only",
        "both failed QC",
    ],
    fontsize=24,
)
f_legend.savefig("SpecFit//legend.png")

subset_colors = {
    "surface": "#1f77b4",
    "borehole_deepest_1": "#ff7f0e",
    "borehole_deepest_2": "#2ca02c",
    "borehole_deepest_3": "#d62728",
    "borehole_all": "#9467bd",
    "borehole_level_1": "#8c564b",
    "borehole_level_2": "#e377c2",
    "borehole_level_3": "#bcbd22",
    "borehole_level_4": "#ff7f0e",  # same as borehole_deepest_1
    "reference": "#17becf",
}
