from datetime import datetime, timedelta
import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import UTCDateTime
import sys

from read_new_catalogs import (
    read_event_catalog,
    read_noise_catalog,
    read_matches,
)


sys.path.append("C:\\Users\\adambaig\\Project\\Chevron_15-06\\")
from bvalue_plot import plotFMD_from_dict
from read_inputs import read_catalog, read_events_from_Do

# from calibration_functions import predict_mag, fit_data


subset_colors = {
    "surface": "#1f77b4",
    "borehole_deepest_1": "#ff7f0e",
    "borehole_deepest_2": "#2ca02c",
    "borehole_deepest_3": "#d62728",
    "borehole_all": "#9467bd",
    "borehole_level_1": "#8c564b",
    "borehole_level_2": "#e377c2",
    "borehole_level_3": "#bcbd22",
    "borehole_level_4": "#ff7f0e",  # same as borehole_deepest_1
    "reference": "#17becf",
}

root_dir = r"C:\Users\adambaig\Project\Chevron_15-06\Phase2\SpecFit\Results"
event_catalogs = glob.glob("MTs//*//flipped_catalog.csv")
specfit_catalogs = glob.glob(f"{root_dir}//[!5]*//Tables//Do_Final.csv")

y_ints = {}

for pfi_catalog_file, specfit_catalog_file in zip(event_catalogs, specfit_catalogs):

    dataset = pfi_catalog_file.split("\\")[1]
    dataset_label = dataset.replace("_", " ")
    if dataset == "surface":
        factor = 118.2**2
    else:
        factor = 78.7**2
    specfit_catalog = read_events_from_Do(specfit_catalog_file)
    pfi_catalog = read_event_catalog(pfi_catalog_file)

    for event_id, event in specfit_catalog.items():
        specfit_event_time = datetime.strptime(
            str(event["UTC"]), "%Y-%m-%dT%H:%M:%S.%fZ"
        )
        for pfi_id, pfi_event in pfi_catalog.items():
            if abs((specfit_event_time - pfi_event["datetime"]).total_seconds()) < 0.1:
                event["image amplitude"] = pfi_event["simple_stack_amp"]
    f1, a1 = plt.subplots()
    a1.semilogy(
        [v["Mw"] for v in specfit_catalog.values() if "image amplitude" in v],
        [
            v["image amplitude"]
            for v in specfit_catalog.values()
            if "image amplitude" in v
        ],
        "o",
        color=subset_colors[dataset],
    )

    x1, x2 = a1.get_xlim()
    x_mags = np.linspace(x1, x2, 100)
    model_quad = -2.213766340e-01  ### from feasibility modelling
    model_linear = 2.642608670e00
    model_yint_sample = []
    for event in {
        k: v for k, v in specfit_catalog.items() if "image amplitude" in v
    }.values():
        model_yint_sample.append(
            np.log10(event["image amplitude"])
            - model_quad * event["Mw"] ** 2
            - model_linear * event["Mw"]
        )
    model_yint = np.percentile(model_yint_sample, 50)
    y_ints[dataset] = model_yint
    print(f"{dataset}: {model_yint}")

    # a1.semilogy(x_mags, 10 ** (x_mags * slope + y_int))

    a1.semilogy(
        x_mags,
        10 ** (x_mags * x_mags * model_quad + x_mags * model_linear + model_yint),
        color="0.4",
    )
    a1.set_xlabel("specfit magnitude")
    a1.set_ylabel("image amplitude")
    f1.savefig(f"Magnitude Calibration//figures//fit_{dataset}.png")

    def mag_from_image_amp_old(event):
        return min(
            np.roots(
                [
                    model_quad,
                    model_linear,
                    model_yint - np.log10(event["simple_stack_amp"]),
                ]
            )
        )

    def mag_from_image_amp(event):
        return min(np.roots([slope, y_int - np.log10(event["simple_stack_amp"])]))

    for event_id, event in pfi_catalog.items():
        if event_id in specfit_catalog:
            pfi_catalog[event_id]["mw"] = specfit_catalog[event_id]["Mw"]
        else:
            pfi_catalog[event_id]["mw"] = mag_from_image_amp_old(event)

    fig_fmd = plotFMD_from_dict(pfi_catalog, color=subset_colors[dataset])
    fig_fmd.savefig(f"Magnitude Calibration//figures//FMD_{dataset}.png")

# fig, ax = plt.subplots()
# ax.set_aspect("equal")
#
# i_sort = np.argsort([v["mw"] for v in pfi_catalog.values()])
#
# eastings = np.array([v["easting"] for v in pfi_catalog.values()])
# northings = np.array([v["northing"] for v in pfi_catalog.values()])
# mags = np.array([v["mw"] for v in pfi_catalog.values()])
#
# ax.scatter(eastings[i_sort], northings[i_sort], c=mags[i_sort])
#
# plt.show()
y_ints
