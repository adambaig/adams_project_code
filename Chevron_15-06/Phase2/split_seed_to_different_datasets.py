import glob
from obspy import read
import os

baseline_dir = "full_seeds//"
deepest_one_dir = "deepest_one_seeds"
deepest_two_dir = "deepest_two_seeds"
deepest_three_dir = "deepest_three_seeds"
all_borehole_dir = "all_borehole_seeds"
surface_dir = "surface_seeds"

for seed in glob.glob(f"{baseline_dir}*.seeds"):
    st = read(seedfile)
    root = os.basename(seedfile).split(".")[0]
    st.select(station="B*4").write(f"{deepest_one_dir}{root}.mseed")
    st.select(station="B*[3,4]").write(f"{deepest_two_dir}{root}.mseed")
    st.select(station="B*[2,3,4]").write(f"{deepest_three_dir}{root}.mseed")
    st.select(station="B*").write(f"{all_borehole_dir}{root}.mseed")
    st.select(station="S*").write(f"{surface_dir}{root}.mseed")
