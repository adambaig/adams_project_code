from datetime import datetime
import glob
import matplotlib.pyplot as plt
import numpy as np
import os
import pytz
import sys

from read_new_catalogs import (
    read_event_catalog,
    read_matches,
    write_double_catalog,
    read_trigger_type,
    read_uncertainties,
)

sys.path.append("..")
from read_inputs import read_catalog, read_isxcor

utc = pytz.utc
starttime = utc.localize(datetime(2019, 12, 11, 0, 0))
endtime = utc.localize(datetime(2019, 12, 13, 0, 0))

event_catalogs = glob.glob(f"MTs{os.sep}*{os.sep}flipped_catalog.csv")
reference_events = {
    k: v
    for k, v in read_catalog(
        "C:\\Users\\adambaig\\Project\\Chevron_15-06\\Catalogs\\"
        "Chevron_15-06_May10_2020_MwUpdate_reassignStages"
        "_updatedDescription_fancyHeader.csv"
    ).items()
    if v["timestamp"] > starttime and v["timestamp"] < endtime
}

is_xcor = read_isxcor()
trigger_type = ["imaging_playback", "template_matching"]
for event in reference_events.values():
    event["trigger type"] = trigger_type[is_xcor[str(event["id"])]]

model_quad = -2.213766340e-01  ### from feasibility modelling
model_linear = 2.642608670e00
model_yint = {
    "borehole_all": -3.627676827824499,
    "borehole_deepest_1": -3.682730919082789,
    "borehole_deepest_2": -3.816847254999418,
    "borehole_deepest_3": -3.6090188736626057,
    "surface": -3.6852762135109245,
}

paired_catalogs = glob.glob("paired_catalogs/*.csv")
matched_ref_id = {}
for catalog in paired_catalogs:
    dataset = os.path.basename(catalog).split("_concat")[0]
    matched_ref_id[dataset] = read_matches(catalog)


def matched_event(id_key):
    return [v for v in reference_events.values() if v["id"] == str(id_key)][0]


def mag_from_image_amp_old(event, dataset):
    return min(
        np.roots(
            [
                model_quad,
                model_linear,
                model_yint[dataset] - np.log10(event["simple_stack_amp"]),
            ]
        )
    )


def mag_to_moment(Mw):
    return 10 ** (1.5 * Mw + 9)


def rescale_MTs(event):
    event["general MT"] = (
        event["moment"] * event["general MT"] / np.linalg.norm(event["general MT"])
    )
    event["dc MT"] = event["moment"] * event["dc MT"] / np.linalg.norm(event["dc MT"])
    return None


for file in event_catalogs:
    subset = read_event_catalog(file, conf=True)
    dataset = os.path.split(file)[0].split(os.sep)[-1]
    trigger_type_catalog = read_trigger_type(
        os.sep.join(["Final Catalogs", "triggers", dataset + ".csv"])
    )
    uncertainty = read_uncertainties(
        os.sep.join(["uncertainty", "data", dataset + ".csv"])
    )
    for id, event in subset.items():
        event["id"] = str(id)
        event["trigger type"] = trigger_type_catalog[id]
        event["moment magnitude"] = mag_from_image_amp_old(event, dataset)
        event["moment"] = mag_to_moment(event["moment magnitude"])
        event["id_ref"] = [k for k, v in matched_ref_id[dataset].items() if v == id][0]
        event["easting_err"] = uncertainty[id]["easting err"]
        event["northing_err"] = uncertainty[id]["northing err"]
        event["elevation_err"] = uncertainty[id]["elevation err"]
        event["snr"] = event["filter_amp"]
        rescale_MTs(event)
    out_catalog = write_double_catalog(f"{dataset}.csv", subset, reference_events)
