from datetime import datetime
import glob
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import mplstereonet as mpls

from sms_moment_tensor.MT_math import decompose_MT

from read_new_catalogs import (
    read_event_catalog,
    read_noise_catalog,
    read_matches,
)


subset_colors = {
    "surface": "#1f77b4",
    "borehole_deepest_1": "#ff7f0e",
    "borehole_deepest_2": "#2ca02c",
    "borehole_deepest_3": "#d62728",
    "borehole_all": "#9467bd",
    "borehole_level_1": "#8c564b",
    "borehole_level_2": "#e377c2",
    "borehole_level_3": "#bcbd22",
    "borehole_level_4": "#ff7f0e",  # same as borehole_deepest_1
    "reference": "#17becf",
}


event_catalogs = glob.glob("MTs//*//relocCatalog*.csv")
events = {}
for catalog in event_catalogs:
    dataset = catalog.split("\\")[1].lower()
    events[dataset] = read_event_catalog(catalog)

noise_catalogs = glob.glob("MTs//*//relocNoise*.csv")
noise = {}
for catalog in noise_catalogs:
    dataset = catalog.split("\\")[1].lower()
    noise[dataset] = read_noise_catalog(catalog)

for dataset in events.keys():
    r_values_noise = [v["dc R"] for v in noise[dataset].values()]
    r_values_events = [v["dc R"] for v in events[dataset].values()]
    r_bins = np.linspace(0, 1, 101)
    fig, ax = plt.subplots()
    ax.hist(r_values_noise, bins=r_bins, alpha=0.5, color="0.5")
    ax.hist(
        r_values_events,
        bins=r_bins,
        alpha=0.5,
        color=subset_colors[dataset],
        zorder=1,
    )
    ax.set_title(dataset.replace("_", " "))
    noise_90 = np.percentile(r_values_noise, 95)
    y1, y2 = ax.get_ylim()
    ax.plot([noise_90, noise_90], [y1, y2], "0.2")
    n_good = len(np.where(r_values_events > noise_90)[0])
    ax.text(0.7, 0.8 * y2, f"{n_good} events\nabove 95%\nconfidence")

    cdf = np.zeros(1001)
    for ii in range(1, 1000):
        cdf[ii] = np.percentile(r_values_noise, ii / 10)
    cdf[-1] = 1
    ax1 = ax.twinx()
    ax1.plot(cdf, np.linspace(0, 1, 1001), "--", c="0.1")
    ax.set_ylim(y1, y2)
    ax.set_ylabel("numbers of events")
    ax.set_xlabel("R value")
    ax1.set_ylabel("cumulative probability")
    ax1.set_ylim(0, 1.02)
    for event in events[dataset].values():
        event["mt confidence"] = np.interp(event["dc R"], cdf, np.linspace(0, 1, 1001))
    fig.savefig(f"MTs\\figures\\confidence_{dataset}.png", bbox_inches="tight")

    fig2, ax2 = plt.subplots()
    ax2.hist(
        [v["mt confidence"] for v in events[dataset].values()],
        bins=r_bins,
        color=subset_colors[dataset],
    )

    ax2.set_xlabel("fractional probability")
    ax2.set_ylabel("number of events")
    ax2.set_xlim(0, 1)
    ax2.set_title(dataset.replace("_", " "))
    fig2.savefig(f"MTs\\figures\\probability_{dataset}.png", bbox_inches="tight")

if False:
    for catalog in event_catalogs:
        dataset = catalog.split("\\")[1].lower()
        f = open(catalog)
        head = f.readline()
        lines = f.readlines()
        f.close()
        g = open(catalog, "w")
        g.write(head.replace("\n", ",mt confidence\n"))
        for line in lines:
            lspl = line.split(",")
            event_id = int(lspl[0])
            g.write(
                line.replace(
                    "\n",
                    f',{events[dataset][event_id]["mt confidence"]:.7f}\n',
                )
            )
        g.close()
