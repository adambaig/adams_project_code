from datetime import datetime
import glob
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import pytz
import os

from read_new_catalogs import (
    read_event_catalog,
    read_noise_catalog,
    read_matches,
)

import sys

sys.path.append("C:\\Users\\adambaig\\Project\\Chevron_15-06\\")

from read_inputs import read_catalog
from scipy.stats import linregress

tab10 = cm.get_cmap("tab10")

subset_colors = {
    "surface": "#1f77b4",
    "borehole_deepest_1": "#ff7f0e",
    "borehole_deepest_2": "#2ca02c",
    "borehole_deepest_3": "#d62728",
    "borehole_all": "#9467bd",
    "borehole_level_1": "#8c564b",
    "borehole_level_2": "#e377c2",
    "borehole_level_3": "#bcbd22",
    "borehole_level_4": "#ff7f0e",  # same as borehole_deepest_1
    "reference": "#17becf",
}

utc = pytz.utc
starttime = utc.localize(datetime(2019, 12, 11, 0, 0))
endtime = utc.localize(datetime(2019, 12, 13, 0, 0))
final_catalog = (
    "C:\\Users\\adambaig\\Project\\Chevron_15-06\\Catalogs\\"
    "Chevron_15-06_May10_2020_MwUpdate_reassignStages"
    "_updatedDescription_fancyHeader.csv"
)
refernce_events = {
    k: v
    for k, v in read_catalog(final_catalog).items()
    if v["timestamp"] > starttime and v["timestamp"] < endtime
}
event_catalogs = glob.glob("MTs//*//relocCatalog*.csv")
paired_catalogs = glob.glob("paired_catalogs/*.csv")
event_times = {}
events = {}
for catalog in event_catalogs:
    dataset = catalog.split("\\")[1].lower()
    events[dataset] = read_event_catalog(catalog)
    event_times[dataset] = {
        k: utc.localize(v["datetime"]) for k, v in events[dataset].items()
    }
matched_ref_id = {}
for catalog in paired_catalogs:
    dataset = os.path.basename(catalog).split("_concat")[0]
    matched_ref_id[dataset] = read_matches(catalog)


def matched_event(id_key):
    return [v for v in refernce_events.values() if v["id"] == str(id_key)][0]


magnitude_bins = np.arange(-1.7, 0, 0.1)
fig, ax = plt.subplots(2, 3, sharex=True, figsize=[16, 8])
patches, labels = [], []
factor = {
    "surface": np.sqrt(30 / 111),
    "borehole_deepest_1": np.sqrt(30 / 111),
    "borehole_deepest_2": np.sqrt(60 / 111),
    "borehole_deepest_3": np.sqrt(90 / 111),
    "borehole_all": np.sqrt(120 / 111),
}
for i_dataset, (tag, dataset) in enumerate(matched_ref_id.items()):
    i_row = (i_dataset + 1) // 3
    i_col = (i_dataset + 1) % 3
    orig_amp = [matched_event(k)["stack_amplitude"] for k in dataset.keys()]
    if tag == "surface":
        amplitudes = [
            events[tag][k]["simple_stack_amp"] / 118.2 / 118.2 for k in dataset.values()
        ]
    else:
        amplitudes = [
            events[tag][k]["simple_stack_amp"] / 78.7 / 78.7 for k in dataset.values()
        ]
    # n_in_bin = np.zeros(len(magnitude_bins))
    # for i_bin, bin in enumerate(magnitude_bins):
    #     n_in_bin[i_bin] = len([m for m in magnitudes if m > bin])

    # (p,) = ax[0, 0].semilogy(magnitude_bins, n_in_bin, ".")
    # ax[0, 0].set_xlabel("full catalog magnitude")
    # ax[0, 0].set_ylabel("number of events above M")
    # labels.append(tag.replace("_", " "))
    # patches.append(p)
    # ax.set_title(tag.replace('_',' '))
    ax[i_row, i_col].loglog(orig_amp, amplitudes, ".", color=subset_colors[tag])
    ax[i_row, i_col].set_title(tag.replace("_", " "))
    ax[i_row, i_col].set_ylabel("subset catalog amplitude")
    ax[i_row, i_col].set_xlabel("full catalog raw amplitude")
    ax[i_row, i_col].plot([1e-12, 1e-6], [1e-12, 1e-6], ":", zorder=-1)
    ax[i_row, i_col].set_xlim(1e-12, 1e-6)
    ax[i_row, i_col].set_ylim(1e-12, 1e-6)
    line_params = linregress(np.log10(orig_amp), np.log10(amplitudes))
    print(line_params.slope)
ax[0, 0].legend(patches, labels)


fig.savefig("amp_check.png", bbox_inches="tight")


fig1, a1 = plt.subplots()
snr = [events[tag][k]["filter_amp"] for k in dataset.values()]
a1.scatter(orig_amp, amplitudes, c=np.log10(snr))
a1.set_xscale("log")
a1.set_yscale("log")

#
#
# quad = -6.409776621e-03
# linear = 2.760859301e00
#
# fig_syn, ax_syn = plt.subplots()
# calibration_curve = np.zeros(len(magnitude_bins))
# for i_bin, bin in enumerate(magnitude_bins):
#     calibration_curve[i_bin] = quad * bin * bin + linear * bin
# ax_syn.semilogy(magnitude_bins, 10 ** (calibration_curve), "orangered", lw=2)
# ax_syn.set_xlabel("magnitude")
# ax_syn.set_ylabel("relative amplitude")
# ax_syn.set_title("Synthetic Calibration Curve")
# fig_syn.savefig("calibration_curve.png", bbox_inches="tight", transparent=True)
