f = open("colocated_stations.csv")
head = f.readline()
lines = f.readlines()
f.close()

head_switch = "#id,northing,easting,elevation\n"


stations = {}
for line in lines:
    lspl = line.split(",")
    station_id = lspl[0]
    stations[station_id] = {
        "easting": float(lspl[1]),
        "northing": float(lspl[2]),
        "elevation": float(lspl[3]),
    }


all_surface = {k: v for k, v in stations.items() if k[0] == "S"}
all_borehole = {k: v for k, v in stations.items() if k[0] == "B"}
deepest_one = {k: v for k, v in stations.items() if k[0] == "B" and k[-1] == "4"}
deepest_two = {k: v for k, v in stations.items() if k[0] == "B" and k[-1] in ["3", "4"]}
deepest_three = {k: v for k, v in stations.items() if k[0] == "B" and k[-1] != "1"}

all_surface["name"] = "all_surface"
all_borehole["name"] = "all_borehole"
deepest_one["name"] = "deepest_one"
deepest_two["name"] = "deepest_two"
deepest_three["name"] = "deepest_three"

for subset in [all_surface, all_borehole, deepest_one, deepest_two, deepest_three]:
    f = open(f'{subset["name"]}.csv', "w")
    f.write(head_switch)
    for station_id, station in subset.items():
        if station_id != "name":
            f.write(
                f'{station_id},{station["northing"]:.1f},'
                f'{station["easting"]:.1f},{station["elevation"]:.1f}\n'
            )
    f.close()
