from datetime import datetime
import pytz
from sms_moment_tensor.MT_math import (
    mt_vector_to_matrix,
    mt_matrix_to_vector,
    mt_to_sdr,
    decompose_MT,
    clvd_iso_dc,
)


timezone = pytz.timezone("America/Edmonton")
utc = pytz.utc


def read_event_catalog(catalog_file, conf=False):
    f = open(catalog_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        event_id = int(lspl[0])
        events[event_id] = {
            "datetime": datetime.strptime(lspl[1], "%Y-%m-%d %H:%M:%S.%f"),
            "easting": float(lspl[2]),
            "northing": float(lspl[3]),
            "depth_ss": float(lspl[4]),
            "filter_amp": float(lspl[5]),
            "raw_amp": float(lspl[6]),
            "simple_stack_amp": float(lspl[7]),
            "general MT": mt_vector_to_matrix([float(s) for s in lspl[8:14]]),
            "general R": float(lspl[14]),
            "general CN": float(lspl[15]),
            "dc MT": mt_vector_to_matrix([float(s) for s in lspl[16:22]]),
            "dc R": float(lspl[22]),
            "dc CN": float(lspl[23]),
        }
        if conf:
            events[event_id]["mt confidence"] = float(lspl[24])
    return events


def read_noise_catalog(catalog_file):
    f = open(catalog_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    noise_samples = {}
    for line in lines:
        lspl = line.split(",")
        noise_sample_id = int(lspl[0])
        noise_samples[noise_sample_id] = {
            "datetime": datetime.strptime(lspl[1], "%Y-%m-%d %H:%M:%S.%f"),
            "easting": float(lspl[2]),
            "northing": float(lspl[3]),
            "depth_ss": float(lspl[4]),
            "general MT": mt_vector_to_matrix([float(s) for s in lspl[5:11]]),
            "general R": float(lspl[11]),
            "general CN": float(lspl[12]),
            "dc MT": mt_vector_to_matrix([float(s) for s in lspl[13:19]]),
            "dc R": float(lspl[19]),
            "dc CN": float(lspl[20]),
        }
    return noise_samples


def read_matches(paired_catalog):
    f = open(paired_catalog)
    head = f.readline()
    lines = f.readlines()
    f.close()
    ids = {}
    for line in lines:
        lspl = line.split(",")
        ids[int(lspl[0])] = int(lspl[9])
    return ids


def write_event_catalog(events, catalog_file):
    f = open(catalog_file, "w")
    f.write(
        "\# ID,TO,UTMX,UTMY,DepthSS,FilterAmp,RawAmp,RelocAmp,"
        "gen11,gen22,gen33,gen12,gen13,gen23,gen_r,gen_cn,"
        "dc11,dc22,dc33,dc12,dc13,dc23,dc_r,dc_cn,mt confidence\n"
    )
    for event_id, event in events.items():
        f.write(
            f"{event_id},"
            f"{datetime.strftime(event['datetime'],'%Y-%m-%d %H:%M:%S.%f,')}"
        )
        f.write(
            f"{event['easting']:.1f},{event['northing']:.1f},"
            f"{event['depth_ss']:.1f},{event['filter_amp']:.7e},"
            f"{event['raw_amp']:.7e},{event['simple_stack_amp']:.7e},"
        )
        f.write(",".join([str(m) for m in mt_matrix_to_vector(event["general MT"])]))
        f.write(f",{event['general R']:.4f},{event['general CN']:.3f},")
        f.write(",".join([str(m) for m in mt_matrix_to_vector(event["dc MT"])]))
        f.write(f",{event['dc R']:.4f},{event['dc CN']:.3f},")
        f.write(f"{event['mt confidence']}\n")
    f.close()
    return catalog_file


def read_matched_catalog(file):
    f = open(file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    events = {}
    for line in lines:
        lspl = line.split(",")
        id = int(lspl[0])
        events[id] = {
            "datetime": datetime.strptime(lspl[1], "%Y-%m-%d %H:%M:%S.%f"),
            "easting": float(lspl[2]),
            "northing": float(lspl[3]),
            "depth_ss": float(lspl[4]),
            "trigger_type": lspl[5],
            "id_ref": int(lspl[6]),
            "datetime_ref": datetime.strptime(lspl[7], "%Y-%m-%d %H:%M:%S.%f"),
            "easting_ref": float(lspl[8]),
            "northing_ref": float(lspl[9]),
            "depth_ss_ref": float(lspl[10]),
            "mw_ref": float(lspl[11]),
            "stage_ref": lspl[12],
            "snr_ref": float(lspl[13]),
        }
    return events


def read_trigger_type(file_name):
    f = open(file_name)
    head = f.readline()
    lines = f.readlines()
    f.close()
    trigger_types = {}
    for line in lines:
        lspl = line.split(",")
        trigger_types[int(lspl[0])] = lspl[5].strip()
    return trigger_types


def read_uncertainties(file_name):
    f = open(file_name)
    head = f.readline()
    lines = f.readlines()
    f.close()
    errors = {}
    for line in lines:
        lspl = line.split(",")
        id = int(lspl[0])
        errors[id] = {
            "easting err": float(lspl[11]),
            "northing err": float(lspl[12]),
            "elevation err": float(lspl[13]),
        }
    return errors


def write_double_catalog(file_name, events, reference_events):
    f = open(file_name, "w")
    f.write("Subset Event")
    [f.write(",") for ii in range(52)]
    f.write("Reference Event\n")
    f.write(",Stage,,,,,,,Location (Datum UTM NAD27 Zone 11N),,,,,,,,")
    f.write("Source and Image Parameters,,,,")
    f.write("General MT Solutions,,,,,,,,,,,DC-Constrained MT Solutions,,,,,,,,,")
    f.write(
        "DC Nodal Plane 1,,,DC Nodal Plane 2,,,P strain axis,, B strain axis,, T strain axis,,"
    )
    f.write("Location (Datum UTM NAD27 Zone 11N),,,,,,,,,")
    f.write("Source and Image Parameters,,,,")
    f.write("General MT Solutions,,,,,,,,,,,DC-Constrained MT Solutions,,,,,,,,,")
    f.write(
        "DC Nodal Plane 1,,,DC Nodal Plane 2,,,P strain axis,, B strain axis,, T strain axis,\n"
    )
    f.write("Event ID,")
    f.write("Well Name,Well ID,Stage,Unique Well Stage ID,")
    f.write(
        "Pumping Curve Integer,Pumping Interval Description,Number of Diverter Drops,"
    )

    f.write("T0,Trigger Method,Easting,Northing,TVDss,")
    f.write(
        "Easting Location Uncertainty,Northing Location Uncertainty,Depth Location Uncertainty,"
    )

    f.write("Stack Signal to Noise,Stack Amplitude,Moment Magnitude,Seismic Moment,")
    f.write("Mee,Mnn,Mzz,Men,Mez,Mnz,Pearson R,Condition Number,")
    f.write("DC Component,CLVD Component,ISO component,")
    f.write("Mee,Mnn,Mzz,Men,Mez,Mnz,Pearson R,Condition Number,Confidence")
    [f.write(",Strike,Dip,Rake") for ii in range(2)]
    [f.write(",Trend,Plunge") for ii in range(3)]
    f.write(",Event ID,T0,Trigger Method,Easting,Northing,TVDss,")
    f.write(
        "Easting Location Uncertainty,Northing Location Uncertainty,Depth Location Uncertainty,"
    )
    f.write("Stack Signal to Noise,Stack Amplitude,Moment Magnitude,Seismic Moment,")
    f.write("Mee,Mnn,Mzz,Men,Mez,Mnz,Pearson R,Condition Number,")
    f.write("DC Component,CLVD Component,ISO component,")
    f.write("Mee,Mnn,Mzz,Men,Mez,Mnz,Pearson R,Condition Number,Confidence")
    [f.write(",Strike,Dip,Rake") for ii in range(2)]
    [f.write(",Trend,Plunge") for ii in range(3)]
    f.write("\n")
    f.write(",,,,,,,")
    f.write(",YYYY-MM-DD HH:MM:SS.000,,")
    [f.write("(m),") for ii in range(6)]
    f.write(",,,")
    [f.write("(Nm),") for ii in range(7)]
    f.write(",,%,%,%")
    [f.write(",(Nm)") for ii in range(6)]
    f.write(",,,%")
    [f.write(",deg") for ii in range(12)]
    f.write(",,YYYY-MM-DD HH:MM:SS.000,,")
    [f.write("(m),") for ii in range(6)]
    f.write(",,,")
    [f.write("(Nm),") for ii in range(7)]
    f.write(",,%,%,%")
    [f.write(",(Nm)") for ii in range(6)]
    f.write(",,,%")
    [f.write(",deg") for ii in range(12)]
    f.write("\n")

    def matched_event(id_key):
        return [v for v in reference_events.values() if v["id"] == str(id_key)][0]

    for event in events.values():
        strike, dip, rake, aux_strike, aux_dip, aux_rake = mt_to_sdr(event["dc MT"])
        decomp = decompose_MT(event["dc MT"])
        clvd, iso, dc = clvd_iso_dc(event["general MT"])
        match = matched_event(event["id_ref"])
        f.write(event["id"])
        f.write(
            f",{match['long_well_name']},{match['well']},{match['stage']},{match['unique_well_stage']},"
        )
        f.write(
            f"{match['treatment_counter']},{match['treatment_code']},{match['n_diverters']},"
        )
        f.write(
            event["datetime"].astimezone(timezone).strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]
        )
        f.write(f',{event["trigger type"].replace("_", " ")}')
        f.write(
            f",{event['easting']:.1f},{event['northing']:.1f},{event['depth_ss']:.1f},"
        )
        f.write(
            f"{event['easting_err']:.1f},{event['northing_err']:.1f},{event['elevation_err']:.1f},"
        )
        f.write(f"{event['snr']:.2e},{event['simple_stack_amp']:.3e},")

        f.write(f"{event['moment magnitude']:.2f},{event['moment']:.3e},")
        [f.write(f"{m:.3e},") for m in mt_matrix_to_vector(event["general MT"])]
        f.write(f"{event['general R']:.3f},{event['general CN']:.1f},")
        f.write(f"{dc:.1f},{clvd:.1f},{iso:.1f},")
        [f.write(f"{m:.3e},") for m in mt_matrix_to_vector(event["dc MT"])]
        f.write(
            f"{event['dc R']:.3f},{event['dc CN']:.1f},{event['mt confidence']*100:.1f},"
        )
        f.write(f"{strike%360:.1f},{dip:.1f},{rake:.1f},")
        f.write(f"{aux_strike%360:.1f},{aux_dip:.1f},{aux_rake:.1f},")
        f.write(f"{decomp['p_trend']%360:.1f},{decomp['p_plunge']:.1f},")
        f.write(f"{decomp['b_trend']%360:.1f},{decomp['b_plunge']:.1f},")
        f.write(f"{decomp['t_trend']%360:.1f},{decomp['t_plunge']:.1f},")
        f.write(f"{match['id']}")
        f.write(
            match["timestamp"]
            .astimezone(timezone)
            .strftime(",%Y-%m-%d %H:%M:%S.%f")[:-3]
        )
        f.write(f',{match["trigger type"].replace("_", " ")}')
        f.write(
            f",{match['easting']:.1f},{match['northing']:.1f},{-match['elevation']:.1f},"
        )
        f.write(
            f"{match['easting_err']:.1f},{match['northing_err']:.1f},{match['elevation_err']:.1f},"
        )

        f.write(f"{match['snr']:.2e},{match['stack_amplitude']:.3e},")

        f.write(f"{match['magnitude']:.2f},{match['moment']:.3e},")
        [f.write(f"{m:.3e},") for m in mt_matrix_to_vector(match["gen MT"])]
        f.write(f"{match['gen R']:.3f},{match['gen CN']:.1f},")
        f.write(f"{match['dc']:.1f},{match['clvd']:.1f},{match['iso']:.1f},")
        [f.write(f"{m:.3e},") for m in mt_matrix_to_vector(match["dc MT"])]
        f.write(
            f"{match['dc R']:.3f},{match['dc CN']:.1f},{match['mt confidence']*100:.1f},"
        )
        f.write(f"{match['strike']%360:.1f},{match['dip']:.1f},{match['rake']:.1f},")
        f.write(
            f"{match['aux_strike']%360:.1f},{match['aux_dip']:.1f},{match['aux_rake']:.1f},"
        )
        f.write(f"{match['p_trend']%360:.1f},{match['p_plunge']:.1f},")
        f.write(f"{match['b_trend']%360:.1f},{match['b_plunge']:.1f},")
        f.write(f"{match['t_trend']%360:.1f},{match['t_plunge']:.1f}")
        f.write("\n")
    return file_name
