import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
from sklearn.cluster import AgglomerativeClustering

from read_inputs import read_mts

from sms_moment_tensor.MT_math import (
    decompose_MT,
    trend_plunge_to_unit_vector,
    mt_to_sdr,
    clvd_iso_dc,
)

mt_catalog = read_mts()

ref_direction = 60
ref_unit_vector = [
    np.sin(ref_direction * np.pi / 180),
    np.cos(ref_direction * np.pi / 180),
]

ref_unit_vector2 = trend_plunge_to_unit_vector(305, 30)
p_unit = trend_plunge_to_unit_vector(decomp["p_trend"], decomp["p_plunge"])
ref_unit_vector2
np.dot(p_unit, ref_unit_vector2)
for event_id, event in mt_catalog.items():
    decomp = decompose_MT(event["dc MT"])
    p_vector = trend_plunge_to_unit_vector(decomp["p_trend"], decomp["p_plunge"])
    p_2vector = p_vector[:2] / np.linalg.norm(p_vector[:2])
    if decomp["b_plunge"] > 45:  # strike slip
        event["cluster"] = 0
        if np.abs(np.dot(p_2vector, ref_unit_vector)) > np.sqrt(
            0.5
        ):  # flip if p_trend in NE-SW direction
            event["DC_flip"] = event["dc MT"]
            event["gen_flip"] = event["gen MT"]
        else:
            event["DC_flip"] = -event["dc MT"]
            event["gen_flip"] = -event["gen MT"]
    else:
        event["cluster"] = 1
        p_unit = trend_plunge_to_unit_vector(decomp["p_trend"], decomp["p_plunge"])
        if np.dot(p_unit, ref_unit_vector2) > 0:
            event["DC_flip"] = -event["dc MT"]
            event["gen_flip"] = -event["gen MT"]
        else:
            event["DC_flip"] = event["dc MT"]
            event["gen_flip"] = event["gen MT"]
        flip_decomp = decompose_MT(event["DC_flip"])
        p_vector = trend_plunge_to_unit_vector(
            flip_decomp["p_trend"], flip_decomp["p_plunge"]
        )
    strike, dip, rake, aux_strike, aux_dip, aux_rake = mt_to_sdr(event["DC_flip"])
    decomp_flip = decompose_MT(event["DC_flip"])
    # decomp_flip = decompose_MT(event["dc MT"])
    clvd, iso, dc = clvd_iso_dc(event["gen_flip"])
    event["p_trend"] = decomp_flip["p_trend"]
    event["b_trend"] = decomp_flip["b_trend"]
    event["t_trend"] = decomp_flip["t_trend"]
    event["p_plunge"] = decomp_flip["p_plunge"]
    event["b_plunge"] = decomp_flip["b_plunge"]
    event["t_plunge"] = decomp_flip["t_plunge"]
    event["strike"] = strike
    event["dip"] = dip
    event["rake"] = rake
    event["aux_strike"] = aux_strike
    event["aux_dip"] = aux_dip
    event["aux_rake"] = aux_rake
    event["clvd"] = clvd
    event["iso"] = iso
    event["dc"] = dc


for i_cluster in range(2):
    p_plunge, p_trend = np.array(
        [
            (v["p_plunge"], v["p_trend"])
            for k, v in mt_catalog.items()
            if v["cluster"] < 10 and v["mt confidence"] > 0.9
        ]
    ).T
    b_plunge, b_trend = np.array(
        [
            (v["b_plunge"], v["b_trend"])
            for k, v in mt_catalog.items()
            if v["cluster"] < 10 and v["mt confidence"] > 0.9
        ]
    ).T
    t_plunge, t_trend = np.array(
        [
            (v["t_plunge"], v["t_trend"])
            for k, v in mt_catalog.items()
            if v["cluster"] < 10 and v["mt confidence"] > 0.9
        ]
    ).T

    fig, ax = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = ax[0].density_contourf(
        p_plunge, p_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    ax[0].line(p_plunge, p_trend, "k.")
    cax_b = ax[1].density_contourf(
        b_plunge, b_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    ax[1].line(b_plunge, b_trend, "k.")
    cax_t = ax[2].density_contourf(
        t_plunge, t_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    ax[2].line(t_plunge, t_trend, "k.")
    fig.savefig(f"mt_after_flip_all.png")
