import matplotlib.pyplot as plt
import mplstereonet as mpls

from sms_moment_tensor.MT_math import decompose_MT

from read_inputs import read_catalog

events = read_catalog("Catalogs//Chevron_15_06_Apr30_2020.csv")

p_trend = [v["p_trend"] for v in events.values() if v["mt confidence"] > 0.9]
p_plunge = [v["p_plunge"] for v in events.values() if v["mt confidence"] > 0.9]
b_trend = [v["b_trend"] for v in events.values() if v["mt confidence"] > 0.9]
b_plunge = [v["b_plunge"] for v in events.values() if v["mt confidence"] > 0.9]
t_trend = [v["t_trend"] for v in events.values() if v["mt confidence"] > 0.9]
t_plunge = [v["t_plunge"] for v in events.values() if v["mt confidence"] > 0.9]

events
fig, (ax1, ax2, ax3) = mpls.subplots(1, 3, figsize=[10, 3])
ax1.density_contourf(p_plunge, p_trend, measurement="lines")
ax2.density_contourf(b_plunge, b_trend, measurement="lines")
ax3.density_contourf(t_plunge, t_trend, measurement="lines")
len(t_trend)
