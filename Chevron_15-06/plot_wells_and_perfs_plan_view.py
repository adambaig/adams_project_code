import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np

from plotting_stuff import gray_background_with_grid
from read_inputs import read_wells, read_events

wells = read_wells()
fig, ax = plt.subplots(figsize=[16, 16])
ax.set_aspect("equal")
for well_id, well in wells.items():
    ax.plot(well["easting"], well["northing"], "k")
    end_of_well = {"easting": well["easting"][-1], "northing": well["northing"][-1]}
    if well_id in ["1", "2", "3", "4", "5", "A", "B"]:
        ax.text(
            end_of_well["easting"] - 50,
            end_of_well["northing"] + 50,
            well_id,
            ha="right",
            va="bottom",
            fontsize=16,
        )
    else:
        ax.text(
            end_of_well["easting"] + 50,
            end_of_well["northing"] - 50,
            well_id,
            ha="left",
            va="top",
            fontsize=16,
        )

    if well_id in ["1", "2", "3", "4", "5", "6"]:
        stages = [v for k, v in well.items() if "tage" in k]
        for i_stage, stage in enumerate(stages):
            stage_color = tab20((int(well_id) - 1) / 10.0 + 0.01 + 0.05 * (i_stage % 2))
            for perf, cluster in stage.items():
                cluster["easting"] = 0.5 * (
                    cluster["top_east"] + cluster["bottom_east"]
                )
                cluster["northing"] = 0.5 * (
                    cluster["top_north"] + cluster["bottom_north"]
                )
                cluster["elevation"] = 0.5 * (
                    cluster["top_elevation"] + cluster["bottom_elevation"]
                )
            ax.plot(
                [v["easting"] for v in stage.values()],
                [v["northing"] for v in stage.values()],
                marker=(7, 1, 0),
                c=stage_color,
                ms=3,
                linewidth=0.25,
                zorder=2,
            )
gray_background_with_grid(ax)
fig.savefig("wells_and_perfs_plan_view.png", bbox_inches="tight")
