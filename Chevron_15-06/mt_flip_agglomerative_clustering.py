from datetime import datetime
import json
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import mplstereonet as mpls
import pytz

import numpy as np
from obspy.imaging.mopad_wrapper import beach
import os
from sklearn.cluster import AgglomerativeClustering

from read_inputs import read_events, read_wells, read_catalog, write_catalog

from sms_moment_tensor.MT_math import (
    decompose_MT,
    trend_plunge_to_unit_vector,
    mt_to_sdr,
    clvd_iso_dc,
    mt_matrix_to_vector,
)
from plotting_stuff import gray_background_with_grid

from pfi_qi.QI_analysis import moment, scaled_mt

utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")


def norm_MT(mt):
    return mt / np.linalg.norm(mt)


def moment(event):
    return 10 ** (1.5 * event["magnitude"] + 9)


def scaled_mt(mt, moment_value):
    moment = 10 ** (1.5 * event["magnitude"] + 9)
    return np.sqrt(2) * mt * moment / np.linalg.norm(event["dc MT"])


def make_simple_gradient_from_white_cmap(end_color):
    red, green, blue = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, 1.0, 1.0), (1, red, red)],
        "green": [(0.0, 1.0, 1.0), (1, green, green)],
        "blue": [(0.0, 1.0, 1.0), (1, blue, blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


mt_catalog = read_catalog("Catalogs//events_with_treatment_tags_before_MT_flip.csv")

frac_events = {k: v for (k, v) in mt_catalog.items() if v["mt confidence"] > 0.95}
n_mt = len(frac_events)
if os.path.exists("mt_dot_product_pairs.json"):
    with open("mt_dot_product_pairs.json", "r") as read_file:
        dot_product_dict = json.load(read_file)
    dot_product_pairs = np.array(dot_product_dict["dot_product_pairs"])
else:
    dot_product_pairs = np.zeros([n_mt, n_mt])
    for i1, (id1, mt1) in enumerate(frac_events.items()):
        for i2, (id2, mt2) in enumerate(frac_events.items()):
            dc1_norm = mt1["dc MT"] / np.linalg.norm(mt1["dc MT"])
            dc2_norm = mt2["dc MT"] / np.linalg.norm(mt2["dc MT"])
            dot_product_pairs[i1, i2] = np.tensordot(dc1_norm, dc2_norm)
    dot_product_dict = {"dot_product_pairs": dot_product_pairs.tolist()}
    with open("mt_dot_product_pairs.json", "w") as write_file:
        json.dump(dot_product_dict, write_file)

distance_mat = abs(1 - abs(dot_product_pairs))
n_clusters = 4
clustering = AgglomerativeClustering(
    n_clusters=n_clusters, affinity="precomputed", linkage="complete"
).fit(distance_mat)
labels = clustering.labels_
for i_mt, (id, mt) in enumerate(frac_events.items()):
    mt["mt cluster"] = labels[i_mt]
colors = [
    "firebrick",
    "forestgreen",
    "darkgoldenrod",
    "steelblue",
    "indigo",
    "darkorange",
    "turquoise",
    "black",
]


f1, a1 = mpls.subplots(1, 3, figsize=[16, 5])
cax_p = a1[0].density_contourf(
    [v["p_plunge"] for v in frac_events.values()],
    [v["p_trend"] for v in frac_events.values()],
    measurement="lines",
)
cax_b = a1[1].density_contourf(
    [v["b_plunge"] for v in frac_events.values()],
    [v["b_trend"] for v in frac_events.values()],
    measurement="lines",
)
cax_t = a1[2].density_contourf(
    [v["t_plunge"] for v in frac_events.values()],
    [v["t_trend"] for v in frac_events.values()],
    measurement="lines",
)

f1.savefig("unflipped_axes.png")

mt_catalog

flip = [1, 1, 1, 1, 1, 1, 1, 1]
for i_cluster in range(n_clusters):
    mt_cluster = {k: v for k, v in frac_events.items() if v["mt cluster"] == i_cluster}

    custom_cmap = make_simple_gradient_from_white_cmap(colors[i_cluster])
    mt_avg = flip[i_cluster] * sum(
        [v["dc MT"] / np.linalg.norm(v["dc MT"]) for k, v in mt_cluster.items()]
    )

    p_trend, b_trend, t_trend = [], [], []
    p_plunge, b_plunge, t_plunge = [], [], []

    for id, mt in mt_cluster.items():
        if np.tensordot(mt_avg, mt["dc MT"]) < 0:
            mt["dc MT"] = -mt["dc MT"]
            mt["gen MT"] = -mt["gen MT"]
        decomp = decompose_MT(mt["dc MT"])
        p_trend.append(decomp["p_trend"])
        b_trend.append(decomp["b_trend"])
        t_trend.append(decomp["t_trend"])
        p_plunge.append(decomp["p_plunge"])
        b_plunge.append(decomp["b_plunge"])
        t_plunge.append(decomp["t_plunge"])
    f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = a3[0].density_contourf(
        p_plunge, p_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_b = a3[1].density_contourf(
        b_plunge, b_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    cax_t = a3[2].density_contourf(
        t_plunge, t_trend, measurement="lines", alpha=0.5, cmap=custom_cmap
    )
    f3.savefig(f"agglomerative_clustering_{i_cluster}.png")

low_q_events = {k: v for (k, v) in mt_catalog.items() if v["mt confidence"] <= 0.9}

cluster_mts = [
    sum(
        mt["dc MT"] / np.linalg.norm(mt["dc MT"])
        for mt in frac_events.values()
        if mt["mt cluster"] == ii
    )
    for ii in range(n_clusters)
]


for event in low_q_events.values():
    dot_products = np.zeros(n_clusters)
    for i_cluster in range(n_clusters):
        dot_products[i_cluster] = np.tensordot(
            norm_MT(event["dc MT"]), norm_MT(cluster_mts[i_cluster])
        )
    i_cluster_belongs = np.argmax(abs(dot_products))
    if dot_products[i_cluster_belongs] < 0:
        event["dc MT"] = -event["dc MT"]
        event["gen MT"] = -event["gen MT"]
    event["mt cluster"] = i_cluster_belongs


len([v["mt cluster"] for v in mt_catalog.values() if v["mt cluster"] < 0])

wells = read_wells()

fig, ax = plt.subplots(figsize=[12, 12])
ax.set_aspect("equal")


for i_cluster in range(n_clusters):
    ax.plot(
        [v["easting"] for v in frac_events.values() if v["mt cluster"] == i_cluster],
        [v["northing"] for v in frac_events.values() if v["mt cluster"] == i_cluster],
        ".",
        color=colors[i_cluster],
        markeredgecolor="k",
        markeredgewidth=0.25,
    )

for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k")

x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()

for i_cluster in range(n_clusters):
    strike, dip, rake = mt_to_sdr(cluster_mts[i_cluster], conjugate=False)
    beach_ball = beach(
        (strike % 360, dip, rake),
        xy=(x1 + (i_cluster + 0.5) * (x2 - x1) / 12, y1 + 0.05 * (y2 - y1)),
        width=400,
        facecolor=colors[i_cluster],
        linewidth=0.25,
    )
    ax.add_collection(beach_ball)
gray_background_with_grid(ax)

fig.savefig("events_by_mt_cluster.png", bbox_inches="tight")


for event in mt_catalog.values():
    strike, dip, rake, aux_strike, aux_dip, aux_rake = mt_to_sdr(event["dc MT"])
    decomp_flip = decompose_MT(event["dc MT"])
    clvd, iso, dc = clvd_iso_dc(event["gen MT"])
    event["p_trend"] = decomp_flip["p_trend"]
    event["b_trend"] = decomp_flip["b_trend"]
    event["t_trend"] = decomp_flip["t_trend"]
    event["p_plunge"] = decomp_flip["p_plunge"]
    event["b_plunge"] = decomp_flip["b_plunge"]
    event["t_plunge"] = decomp_flip["t_plunge"]
    event["strike"] = strike
    event["dip"] = dip
    event["rake"] = rake
    event["aux_strike"] = aux_strike
    event["aux_dip"] = aux_dip
    event["aux_rake"] = aux_rake
    event["clvd"] = clvd
    event["iso"] = iso
    event["dc"] = dc


# for event in mt_catalog.values():
#     event["mt confidence"] /= 100

write_catalog(mt_catalog, "events_after_MT_flip.csv")
