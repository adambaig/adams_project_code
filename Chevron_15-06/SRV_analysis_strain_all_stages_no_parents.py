from copy import deepcopy
from datetime import datetime, timedelta
from matplotlib import cm, colors
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import os
import pytz
from scipy.ndimage import zoom
from scipy.interpolate import RectBivariateSpline

from pfi_qi.QI_analysis import (
    strain_grid,
    output_isosurface,
)
from pfi_qi.engineering import sorted_stage_list
from read_inputs import (
    read_catalog,
    read_treatment,
    get_velocity_model,
    read_wells,
    read_strain,
    read_or_load_json,
    read_tops,
)
from plotting_stuff import gray_background_with_grid

PI = np.pi
D2R = PI / 180.0
utc = pytz.utc
timezone = pytz.timezone("America/Edmonton")

method = "strain"

velocity_model = get_velocity_model()
events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
wells = read_wells()
treatment = read_or_load_json(read_treatment, "treatment_data.json")
sorted_stages = read_or_load_json(
    sorted_stage_list, "sorted_stage_list.json", treatment
)
tops = read_tops()
# This block is dealing with the split stage.

split_stage = {
    k: v for k, v in events.items() if v["well"] == "1" and v["stage"] == "05"
}
for event_id, event in split_stage.items():
    if event["timestamp"] < utc.localize(datetime(2019, 12, 9, 9, 0)):
        event["stage"] = "05.0"
    else:
        event["stage"] = "05.5"
pump_wells = deepcopy(wells)
pump_wells["1"]["Stage 05.0"] = pump_wells["1"]["Stage 05"]
pump_wells["1"]["Stage 05.5"] = pump_wells["1"]["Stage 05"]
pump_wells["1"].pop("Stage 05")


events_no_is = {
    k: v
    for k, v in events.items()
    if v["treatment_code"].strip() not in ["IS", "PRE", "POST"]
    and v["magnitude"] > -1.25
}
# len({k: v for k, v in events.items() if v["well"] == "2" and v["stage"] == "19"})
strain_dir = "strain_complete"


ireton = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["H_V_Ireton"])
duvernay = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Duvernay"])
majeau = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Majeau_Lake"])
beaverhill = RectBivariateSpline(
    tops["e_grid"], tops["n_grid"], tops["Beaverhill_Lake"]
)


grid_spacing = 40
min_neighbours = 5
max_radius = 80
all_well_strain_file = f"{strain_dir}//all_wells_complete.csv"
if os.path.exists(all_well_strain_file):
    strain_flat, grid_points = read_strain(all_well_strain_file)
else:
    strain, grid_points = strain_grid(
        events_no_is,
        grid_spacing,
        min_neighbours,
        max_radius,
        f"{strain_dir}//all_wells.csv",
        velocity_model,
    )
e_grid = np.unique(grid_points[:, 0])
n_grid = np.unique(grid_points[:, 1])
z_grid = np.unique(grid_points[:, 2])
n_e, n_n, n_z = len(e_grid), len(n_grid), len(z_grid)
if "strain_flat" in globals():
    strain = np.reshape(strain_flat, [n_e, n_n, n_z])

vertexes, volume_tags, volume = output_isosurface(
    strain, 1e-9, grid_points, f"{strain_dir}//isosurface.csv"
)

non_zero_strain = [v for v in np.ravel(strain) if v > 0]
# plt.hist(np.log10(non_zero_strain), bins=np.linspace(-10, -5, 50))

vmin = -8.8
vmax = np.percentile(np.log10(non_zero_strain), 99.9)
east_grid = np.unique(grid_points[:, 0])
north_grid = np.unique(grid_points[:, 1])
elevation_grid = np.unique(grid_points[:, 2])
levels = np.linspace(vmin, vmax, 9)
color_scale = cm.ScalarMappable(
    norm=colors.Normalize(vmin=vmin, vmax=vmax), cmap="inferno_r"
)
strain_interpolate = zoom(strain, 4)
depth_interp_factor = 10
strain_depth_interpolate = zoom(strain, [1, 1, depth_interp_factor])
z_grid_interp = zoom(z_grid, depth_interp_factor)
in_duvernay = np.zeros([n_e, n_n, depth_interp_factor * n_z])
for i_e, e in enumerate(east_grid):
    for i_n, n in enumerate(north_grid):
        duvernay_top = duvernay(e, n)
        duvernay_bottom = majeau(e, n)
        for i_z, z in enumerate(z_grid_interp):
            if z > duvernay_bottom and z < duvernay_top:
                in_duvernay[i_e, i_n, i_z] = 1

fig, ax = plt.subplots(figsize=[16, 16])

ax.set_aspect("equal")
contour_colors = []
for level in levels:
    contour_colors.append(color_scale.to_rgba(level))

contours = ax.contourf(
    zoom(east_grid, 4),
    zoom(north_grid, 4),
    np.log10(strain_interpolate[:, :, 68]).T,
    vmin=vmin,
    levels=levels,
    vmax=vmax,
    colors=contour_colors,
    extend="max",
)
for well in [v for k, v in wells.items() if k in ["1", "2", "3", "4", "5", "6"]]:
    ax.plot(well["easting"], well["northing"], "k")

ax = gray_background_with_grid(ax)
# vertexes, volume_tags, volume_all = output_isosurface(
#     strain, 10 ** vmin, grid_points, strain_dir + "all_isosurfaces.csv",
# )
cbaxes = fig.add_axes([0.2, 0.18, 0.6, 0.03])
cb = plt.colorbar(contours, cax=cbaxes, orientation="horizontal")
ticks = cb.get_ticks()
cb.set_ticklabels([f"{10**tick:.1e}".replace("-", "$-$") for tick in ticks])
cb.ax.tick_params(labelsize=16, rotation=0)
cb.set_label("strain", fontsize=20)

x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()
fig.savefig(
    strain_dir + "//figures//all_fig1_no_parents.png", dpi=150, bbox_inches="tight"
)
