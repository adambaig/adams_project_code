import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as mcolors
import numpy as np
import scipy.stats as st
from scipy.interpolate import RectBivariateSpline

from pfi_qi.rotations import rotate_from_cardinal

from plotting_stuff import gray_background_with_grid
from read_inputs import read_wells, read_catalog, read_tops

angle = 135
angle_rad = angle * np.pi / 180
wells = read_wells()
events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate_reassignStages_updatedDescription_fancyHeader.csv"
)
wellhead = {"easting": wells["1"]["easting"][0], "northing": wells["1"]["northing"][0]}
events = rotate_from_cardinal(events, angle_rad, wellhead)
wells = rotate_from_cardinal(wells, angle_rad, wellhead)
list(events.values())[0]

tops = read_tops()
tab10 = cm.get_cmap("tab10")
duvernay = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Duvernay"])
beaverhill = RectBivariateSpline(
    tops["e_grid"], tops["n_grid"], tops["Beaverhill_Lake"]
)
ireton = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["H_V_Ireton"])
majeau = RectBivariateSpline(tops["e_grid"], tops["n_grid"], tops["Majeau_Lake"])


def make_simple_gradient_from_white_cmap(end_color):
    red, green, blue = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, 1.0, 1.0), (1, red, red)],
        "green": [(0.0, 1.0, 1.0), (1, green, green)],
        "blue": [(0.0, 1.0, 1.0), (1, blue, blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


def in_formation(event):
    e, n, z = event["easting"], event["northing"], event["elevation"]
    if z > ireton(e, n)[0][0]:
        return "ABOVE"
    if z > duvernay(e, n)[0][0]:
        return "IRETON"
    if z > majeau(e, n)[0, 0]:
        return "DUVERNAY"
    if z > beaverhill(e, n)[0][0]:
        return "MAJEAU"
    return "BEAVERHILL"


transects = {
    "1": {
        "along trend": -500 * np.ones(1601),
        "perp to trend": np.linspace(-1600, 1600, 1601),
    },
    "2": {
        "along trend": -1000 * np.ones(1601),
        "perp to trend": np.linspace(-1600, 1600, 1601),
    },
    "3": {
        "along trend": -1500 * np.ones(1601),
        "perp to trend": np.linspace(-1600, 1600, 1601),
    },
    "4": {
        "along trend": -2000 * np.ones(1601),
        "perp to trend": np.linspace(-1600, 1600, 1601),
    },
}


rotate_from_cardinal(transects, angle_rad, wellhead, reverse=True)

for event in events.values():
    event["formation"] = in_formation(event)


fig_hist, ax_hist = plt.subplots(figsize=[8, 6])
hist_color = {"1": tab10(0.05), "4": tab10(0.35)}
for event_well in ["1", "4"]:
    well_events = {
        k: v
        for k, v in events.items()
        if v["well"] == event_well and v["treatment_code"] not in ["IS", "PRE", "POST"]
        # and v["formation"] in ["DUVERNAY", "IRETON", "MAJEAU"]
    }

    fig, ax = plt.subplots(figsize=[13, 13])
    ax.set_aspect("equal")
    ax.plot(
        [v["perp to trend"] for v in well_events.values()],
        [v["elevation"] for v in well_events.values()],
        "k.",
        alpha=0.3,
    )
    perp_to_well = [v["perp to trend"] for v in well_events.values()]
    elevations = [v["elevation"] for v in well_events.values()]
    x1, x2 = ax.get_xlim()
    y1, y2 = ax.get_ylim()

    for layer_top in [ireton, duvernay, majeau, beaverhill]:
        for line in transects:
            transect_top = layer_top(
                np.array(transects[line]["easting"]),
                np.array(transects[line]["northing"]),
                grid=False,
            )
            ax.plot(
                transects[line]["perp to trend"],
                transect_top,
                "0.5",
                alpha=0.5,
                zorder=10,
            )

    xx, yy = np.mgrid[x1:x2:200j, y1:y2:200j]
    positions = np.vstack([xx.ravel(), yy.ravel()])
    values_A = np.vstack([perp_to_well, elevations])
    kernel_A = st.gaussian_kde(values_A)
    contour_A = np.reshape(kernel_A(positions).T, xx.shape)
    positions = np.vstack([xx.ravel(), yy.ravel()])
    # values_B = np.vstack([rel_east_B, rel_north_B])
    # kernel_B = st.gaussian_kde(values_B)
    # contour_B = np.reshape(kernel_B(positions).T, xx.shape)

    cf_set_A = ax.contourf(
        xx,
        yy,
        contour_A,
        cmap=make_simple_gradient_from_white_cmap(hist_color[event_well]),
        zorder=-10,
        alpha=0.9,
    )
    north_wells = ["1", "2", "3", "4", "5", "A", "B"]
    for well_id, well in {k: v for k, v in wells.items() if k in north_wells}.items():
        ax.plot(well["perp to trend"], well["elevation"], "k", zorder=11)
        end_of_well = well["perp to trend"][-1]
        if x1 < end_of_well and x2 > end_of_well:
            ax.text(
                well["perp to trend"][-1],
                well["elevation"][-1] - 25,
                well_id,
                color="k",
                zorder=12,
                ha="center",
                va="top",
                fontsize=16,
            )

    ax.set_xlim([x1, x2])
    ax.set_ylim([-2600, -2300])
    gray_background_with_grid(ax, grid_spacing=50)
    fig.savefig(f"well_{event_well}_gunbarrel.png", bbox_inches="tight")
    ax_hist.hist(
        [v["perp to trend"] for v in well_events.values()],
        bins=np.linspace(-800, 800, 100),
        facecolor=hist_color[event_well],
        alpha=0.5,
    )


y1, y2 = ax_hist.get_ylim()
for well_id, well in {k: v for k, v in wells.items() if k in north_wells}.items():
    end_of_well = well["perp to trend"][-1]
    ax_hist.plot([end_of_well, end_of_well], [y1, y2], "k")
    ax_hist.text(end_of_well + 10, 0.9 * y2, well_id)
ax_hist.set_ylim([y1, y2])
ax_hist.set_xlabel("distance across wells (m)")
ax_hist.set_ylabel("number of events")
fig_hist.savefig("event_trends_across_wells.png")
