import matplotlib.pyplot as plt
import numpy as np

from obspy import Stream, Trace, UTCDateTime
from obspy.core.trace import Stats

from read_inputs import get_velocity_model, read_stations, read_wells

from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.raytrace import isotropic_ray_trace

from sms_moment_tensor.MT_math import mt_vector_to_matrix, mt_matrix_to_vector

default_noise = 3.0e-8
sws_factor = 3
n_nodes = 13
dt = 0.002
superstation_factor = sws_factor * np.sqrt(n_nodes)
borehole_factor = 3
Q = {"P": 70, "S": 50}
stations = read_stations(shifted=True)
wells = read_wells()
velocity_model = get_velocity_model()
wells
source = {
    "e": 0,
    "n": 0,
    "z": -2465,
    "stress_drop": 1.0e6,
    "moment_tensor": mt_vector_to_matrix([1, -1, 0, 0, 0, 0]),
}
time_series = np.arange(0, 8, dt)
m11, m22, m33, m12, m13, m23 = mt_matrix_to_vector(source["moment_tensor"])
mag_range = np.arange(-3, 0.01, 0.1)


def createTraceStats(network, n1, o1, d1, component):
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


noise_level = {
    "S": default_noise / superstation_factor,
    "B": default_noise / borehole_factor,
}
outdir = "mixed_synthetics//"

g = open(outdir + "//synthetic_catalog.csv", "w")
g.write("date,time,x,y,z,magnitude,stress drop,m11,m22,m33,m12,m13,m23\n")

for mag in mag_range:
    st = Stream()
    seedtime = UTCDateTime.now()
    waveforms = []
    source["moment_magnitude"] = mag
    for station_id, station in stations.items():
        p_raypath = isotropic_ray_trace(source, station, velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, station, velocity_model, "S")
        waveforms.append(
            get_response(
                p_raypath,
                s_raypath,
                source,
                station,
                Q,
                time_series,
                noise_level[station_id[0]],
            )
        )
        channel_obj = {
            "component": "Z",
            "channel": "CPZ",
            "station": station_id,
            "network": "CV",
            "location": "00",
        }
        xStats = createTraceStats("CV", len(time_series), seedtime, dt, channel_obj)
        tr = Trace(data=waveforms[-1]["z"], header=xStats)
        st += tr
    outfile = (outdir + "//synthetic" + str(seedtime) + ".mseed").replace(":", "-")

    st.write(outfile)
    dat, tim = str(seedtime).split("T")
    g.write(dat + "," + tim[:-1] + ",")
    g.write("%.1f,%.1f,%.1f," % (source["e"], source["n"], source["z"]))
    g.write("%.3f,%.4e," % (source["moment_magnitude"], source["stress_drop"]))
    g.write("%.3f,%.3f,%.3f,%.3f,%.3f,%.3f\n" % (m11, m22, m33, m12, m13, m23))
g.close()
