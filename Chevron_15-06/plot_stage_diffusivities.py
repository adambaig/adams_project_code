import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np

D2R = np.pi / 180

tab10 = cm.get_cmap("tab10")
well_color = {
    "1": tab10(0.05),  # blue-ish
    "2": tab10(0.15),  # orange
    "3": tab10(0.25),  # green
    "4": tab10(0.35),  # red
    "5": tab10(0.45),  # purple
    "6": tab10(0.55),  # brown
    "A": tab10(0.65),
    "B": tab10(0.75),
    "C": tab10(0.85),
    "D": tab10(0.95),
}

diffusivity_file = "rt_curve.csv"
f = open(diffusivity_file)
header = [f.readline() for i in range(2)]
lines = f.readlines()


diffusivity = {}
for line in lines:
    lspl = line.split(",")
    well = lspl[0]
    stage = lspl[1]
    if float(stage) < 10:
        stage = "0" + stage
    well_stage_identifier = f"Well {well}: Stage {stage}"
    diffusivity[well_stage_identifier] = {
        "well": well,
        "stage": stage,
        "unique well stage id": int(lspl[2]),
        "n_events": lspl[3],
        "initiation time": lspl[4],
        "horizontal": float(lspl[5]),
        "vertical": float(lspl[6]),
    }


fig, (ax_hrz, ax_vrt) = plt.subplots(2, figsize=[10, 7], sharex=True)
for well in ["1", "2", "3", "4", "5", "6"]:
    stage, hrz, vrt = np.array(
        [
            (
                float(d["stage"]),
                d["horizontal"],
                d["vertical"],
            )
            for d in diffusivity.values()
            if d["well"] == well
        ]
    ).T
    ax_hrz.plot(stage, hrz, color=well_color[well])
    ax_vrt.plot(stage, vrt, color=well_color[well])


ax_hrz.set_ylabel("horizontal (m$^2$/s)")
ax_vrt.set_ylabel("vertical (m$^2$/s)")
ax_vrt.set_xlabel("stage")

fig.savefig("stage_diffusivities.png")


fig_dim_hist, ax_dim_hist = plt.subplots(
    2, 6, figsize=[18, 9], sharex=True, sharey=True
)

for well in ["1", "2", "3", "4", "5", "6"]:
    stage, hrz, vrt = np.array(
        [
            (
                float(d["stage"]),
                d["horizontal"],
                d["vertical"],
            )
            for d in diffusivity.values()
            if d["well"] == well
        ]
    ).T
    bins = np.linspace(0, 4, 20)
    ax_dim_hist[0, int(well) - 1].hist(hrz, bins, facecolor=well_color[well])
    ax_dim_hist[1, int(well) - 1].hist(vrt, bins, facecolor=well_color[well])
    ax_dim_hist[1, int(well) - 1].set_xlabel("diffusivity (m$^2$/s)")
ax_dim_hist[0, 0].set_ylabel("horizontal (# of stages)")
ax_dim_hist[1, 0].set_ylabel("vertical (# of stages)")
fig_dim_hist.savefig("figures/stage_diffusivity_histograms.png")
