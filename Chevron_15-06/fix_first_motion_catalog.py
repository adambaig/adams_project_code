f = open("mt-qc//borehole_assessment.csv")
head = f.readline()
lines = f.readlines()
f.close()
correction = {}
for line in lines:
    lspl = line.split(",")
    correction[lspl[0][:-1] + "4"] = int(lspl[-1])

f = open("mt-qc//catalog_of_first_motions.csv")
headsplit = f.readline().replace("\n", "").split(",")
lines = f.readlines()
f.close()
amp_qc = {}
for line in lines:
    lspl = line.replace("\n", "").split(",")
    for station in headsplit[1:]:
        amp_qc[lspl[0]] = {k: v for v, k in zip(lspl[1:], headsplit[1:])}

fix_qc = {}
mapping = {"True": 1, "False": -1, "None": 0}
reverse_mapping = {v: k for k, v in mapping.items()}
for event, stations in amp_qc.items():
    fix_qc[event] = {}
    for station, can_see in stations.items():
        fix_qc[event][station] = reverse_mapping[
            correction[station] * mapping[amp_qc[event][station]]
        ]


amp_qc["16094919048"]


# g = open("mt-qc//refixed_catalog_of_first_motions.csv", "w")
# g.write(",".join(headsplit) + "\n")
# for event, stations in fix_qc.items():
#     g.write(event + ",")
#     g.write(",".join([v for v in stations.values()]))
#     g.write("\n")
# g.close()
