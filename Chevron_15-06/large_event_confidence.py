from read_inputs import read_catalog

ids = {
    "16094919048",
    "17213221118",
    "18113650824",
    "18121129692",
    "18132057476",
    "37034555880",
}

events = read_catalog(
    "Catalogs\\Chevron_15-06_May10_2020_MwUpdate"
    "_reassignStages_updatedDescription_fancyHeader.csv"
)

large_events = {k: v for k, v in events.items() if v["id"] in ids}

for event in large_events.values():
    print(f'{event["id"]}: {event["mt confidence"]}')
