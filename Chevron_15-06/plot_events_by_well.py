import matplotlib

matplotlib.use("Qt5Agg")

import json
import matplotlib.pyplot as plt
import numpy as np
from read_inputs import (
    read_events,
    get_velocity_model,
    read_wells,
    read_treatment,
)

from plot_stuff import gray_background_with_grid
from QI_analysis import rotate_from_cardinal, calc_well_trend


wells = read_wells(shift_5=True)
events = read_events()

for well_id, well in {
    k: v for k, v in wells.items() if k in ["1", "2", "3", "4", "5", "6"]
}.items():
    fig, ax = plt.subplots(figsize=[10, 10])
    ax.set_facecolor("lightgrey")
    ax.set_aspect("equal")
    ax.plot(well["easting"], well["northing"], lw=4, c="0.7", zorder=0)
    ax.plot(well["easting"], well["northing"], lw=5, c="0.3", zorder=-1)
    for well2 in wells.values():
        ax.plot(well2["easting"], well2["northing"], lw=2, c="0.8", zorder=-2)
        ax.plot(well2["easting"], well2["northing"], lw=2.5, c="0.4", zorder=-3)
    well_events = {k: v for k, v in events.items() if v["well"] == well_id}
    mod_stage = np.mod([int(v["stage"]) for v in well_events.values()], 10)
    ax.scatter(
        [v["easting"] for v in well_events.values()],
        [v["northing"] for v in well_events.values()],
        c=mod_stage,
        cmap="tab10",
        vmin=-0.1,
        vmax=9.9,
        edgecolor="k",
        linewidth=0.25,
        zorder=3,
        alpha=0.7,
    )
    ax = gray_background_with_grid(ax)
    ax.set_title(f"Well {well_id}")
    fig.savefig(f"figures//events_well_{well_id}.png", bbox_inches="tight")

plt.show()
