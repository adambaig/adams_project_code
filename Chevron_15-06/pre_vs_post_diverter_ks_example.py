from copy import deepcopy
from datetime import datetime

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import pytz
from scipy.stats import ks_2samp
from pfi_qi.QI_analysis import rotate_all_events_to_well_coords
from read_inputs import read_catalog, read_wells, read_diverter_drops


events = read_catalog("Catalogs//Chevron_15_06_May8_2020.csv")
wells = read_wells()
tab10 = cm.get_cmap("tab10")
# This block is dealing with the split stage.
utc = pytz.utc

magnitude_of_completeness = -1.25

well1_events = {k: v for k, v in events.items() if v["well"] == "1"}

diverter_drops = read_diverter_drops()

# This block is dealing with the split stage.

split_stage = {
    k: v for k, v in events.items() if v["well"] == "1" and v["stage"] == "05"
}
for event_id, event in split_stage.items():
    if event["timestamp"] < utc.localize(datetime(2019, 12, 9, 9, 0)):
        event["stage"] = "05.0"
    else:
        event["stage"] = "05.5"
pump_wells = deepcopy(wells)
pump_wells["1"]["Stage 05.0"] = pump_wells["1"]["Stage 05"]
pump_wells["1"]["Stage 05.5"] = pump_wells["1"]["Stage 05"]
pump_wells["1"].pop("Stage 05")

rotate_all_events_to_well_coords(well1_events, pump_wells)
# find frac events (still may have some IS), calculate diffusivity
diverter_stages = np.unique([v["stage"] for v in diverter_drops.values()])
diverter_stage_events = {
    k: v
    for k, v in events.items()
    if v["well"] == "1" and v["stage"] in diverter_stages
}

diverter_events_complete = {
    k: v
    for k, v in diverter_stage_events.items()
    if v["treatment_code"] not in ["PRE", "POST", "IS"]
    and v["magnitude"] > magnitude_of_completeness
}


stage_events = {
    k: v
    for k, v in diverter_events_complete.items()
    if v["well"] == "1" and v["stage"] == "20"
}
d0_events = {k: v for k, v in stage_events.items() if v["n_diverters"] == 0}
d1_events = {k: v for k, v in stage_events.items() if v["n_diverters"] == 1}


perp0 = [v["perp to trend"] for v in d0_events.values()]
perp1 = [v["perp to trend"] for v in d1_events.values()]


ks_stat, p = ks_2samp(perp0, perp1)
shift = np.median(perp1) - np.median(perp0)

bins = np.linspace(-200, 200, 21)
fig, (ax1, ax2) = plt.subplots(2, figsize=[5, 8], sharex=True, sharey=True)
ax1.hist(perp0, bins, facecolor=tab10(0.05), edgecolor="0.1")
ax2.hist(perp1, bins, facecolor=tab10(0.05), edgecolor="0.1")


perfs = np.array([-37.5, -22.5, -7.5, 7.5, 22.5, 37.5])
for ax in ax1, ax2:
    ax.plot(
        perfs,
        0.5 * np.ones(6),
        marker=(7, 1, 0),
        color="orangered",
        markeredgecolor="k",
        zorder=10,
        linestyle="None",
    )
    ax.set_ylabel("number of events")

ax2.set_xlabel("along-well distance (m)")
fig.savefig("diverter_histogram.png", bbox_inches="tight")
