import numpy as np
from nmxseis.numerics.geodesy import EPSGReprojector
from obspy import read, read_inventory, UTCDateTime


from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.model.seismic_event import EventOrigin
from nmxseis.util import supporting_gz

from read_inputs import read_velocity_model, get_arrivals_from_picks

velocity_model = read_velocity_model()
reproject = EPSGReprojector('2154')
inventory = supporting_gz(read_inventory)("Cerville.xml.gz")
waveform_stream = read("20080405.084145.783856_20080405.084149.030000.seed")
event = EventOrigin(lon=6.321144338311351,lat=48.67833518924822,depth_m=-11.94518994634995,time =UTCDateTime(2008,4,5,8,41,46,732294))


arrivals = get_arrivals_from_picks("00003064_20080405.084146.783856.picks")
event_id = "3064"


nsl_to_plot = NSL(net="GI", sta="C03", loc="")
waveform_stream.select(station=nsl_to_plot.sta).plot()
waveform_stream

trace_to_plot = (
    waveform_stream.select(station=nsl_to_plot.sta, channel="*Z")
    .copy()
    .remove_response(inventory=inventory)
    .filter("highpass", freq=5, corners=1, zerophase=True)[0]
)
trace_to_plot.plot()

x = trace_to_plot.copy()
x

arrivals[nsl_to_plot]

len(trace_to_plot)

import matplotlib.pyplot as plt
from numpy.fft import fft, fftfreq

i_start = 1250
i_end = 3750
fig = plt.figure(figsize=[8, 10])
ax_trace = fig.add_axes([0.1, 0.8, 0.8, 0.15])
ax_spectra = fig.add_axes([0.1, 0.1, 0.8, 0.6])
ax_trace.plot(
    trace_to_plot.times()[i_start:i_end] + trace_to_plot.stats.starttime.timestamp,
    trace_to_plot.data[i_start:i_end],
    "k",
)
sig_win = [
    arrivals[nsl_to_plot][Phase.P] - 0.01,
    arrivals[nsl_to_plot][Phase.P] + 0.235,
]
nse_win = [
    arrivals[nsl_to_plot][Phase.P] - 0.255,
    arrivals[nsl_to_plot][Phase.P] - 0.01,
]
ylim = ax_trace.get_ylim()
sig_window = plt.Rectangle(
    xy=(sig_win[0].timestamp, ylim[0]),
    width=np.diff(sig_win),
    height=np.diff(ylim),
    facecolor="lightblue",
    alpha=0.4,
)
nse_window = plt.Rectangle(
    xy=(nse_win[0].timestamp, ylim[0]),
    width=np.diff(nse_win),
    height=np.diff(ylim),
    facecolor="0.1",
    alpha=0.4,
)
ax_trace.add_patch(sig_window)
ax_trace.add_patch(nse_window)
ax_trace.set_xlim(
    [
        trace_to_plot.times()[i_start] + trace_to_plot.stats.starttime.timestamp,
        trace_to_plot.times()[i_end] + trace_to_plot.stats.starttime.timestamp,
    ]
)
ax_trace.plot(np.tile(arrivals[nsl_to_plot][Phase.P], 2), ylim)
ax_trace.set_ylim(*ylim)


signal = [
    tr
    for tr, time in zip(
        trace_to_plot.data,
        trace_to_plot.times() + trace_to_plot.stats.starttime.timestamp,
    )
    if time > sig_win[0] and time < sig_win[1]
]
noise = [
    tr
    for tr, time in zip(
        trace_to_plot.data,
        trace_to_plot.times() + trace_to_plot.stats.starttime.timestamp,
    )
    if time > nse_win[0] and time < nse_win[1]
]

len(signal)
len(noise)

freqs = fftfreq(len(signal), trace_to_plot.stats.delta)
noise_spectra = fft(noise)
signal_spectra = fft(signal)
len(freqs)
nf2 = len(freqs) // 2 - 1

ax_spectra.loglog(
    freqs[:nf2], abs(signal_spectra[:nf2]) / 2 / np.pi / freqs[:nf2], "steelblue"
)
ax_spectra.loglog(
    freqs[:nf2], abs(noise_spectra[:nf2]) / 2 / np.pi / freqs[:nf2], "0.2"
)
ax_spectra.set_xlabel("frequency (Hz)")
ax_spectra.set_ylabel("displacement spectra (m$\cdot$s)")
ax_spectra.grid("on")
xticks = ax_trace.get_xticks()
ax_trace.set_xticklabels(
    [str(UTCDateTime(t)).split("T")[1][:-4] for t in xticks], rotation=30
)
ax_trace.set_xlabel("UTC time")
ax_trace.set_yticklabels("")

fig.savefig("spectra_example2.png")
