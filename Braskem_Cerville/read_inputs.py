from collections import defaultdict
from obspy import UTCDateTime

from nmxseis.model.nslc import NSL
from nmxseis.model.phase import Phase
from nmxseis.numerics.formulas import gardners_relation
from nmxseis.numerics.velocity_model.vm_1d import VelocityModel1D, VMLayer1D


def read_catalog(filename="2008_Amin_reviewed.csv"):
    with open(filename) as f:
        _head = f.readline()
        lines = f.readlines()
    events = {}
    for line in lines:
        split_line = line.split(",")
        event_id = split_line[0]
        events[event_id] = {
            "lon": float(split_line[1]),
            "lat": float(split_line[2]),
            "depth": float(split_line[3]),
            "mag": float(split_line[4]),
            "origin time": float(split_line[5]),
            "utc": UTCDateTime(split_line[6]),
        }
    return events


def read_velocity_model(filename="Cerville_SmoothedVelocityProfile.csv"):
    with open(filename) as f:
        _head = f.readline()
        lines = f.readlines()
    layers = []
    for i_line, line in enumerate(lines):
        split_line = line.split(",")
        tvdss, vp, vs = [float(s) for s in split_line]
        if i_line == 0:
            layers.append(VMLayer1D(vp=vp, vs=vs, rho=gardners_relation(vp).tolist()))
        else:
            layers.append(VMLayer1D(top=-tvdss, vp=vp, vs=vs, rho=gardners_relation(vp).tolist()))
    return VelocityModel1D(layers)


def get_arrivals_from_picks(pick_file):
    with open(pick_file) as f:
        lines = f.readlines()
    arrivals = defaultdict(dict)
    for line in lines:
        nsl_str, phase, time_str = line.split(",")
        if phase in ["P", "S"]:
            arrivals[NSL.parse(nsl_str)][Phase(phase)] = UTCDateTime(float(time_str))
    return arrivals
