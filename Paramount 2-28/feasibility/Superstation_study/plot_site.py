import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt

from read_inputs import read_dsa, read_wells, read_15_stations

dsa_stations = read_dsa()
wells = read_wells()

proposed_stations = read_15_stations()

fig, ax = plt.subplots()
ax.set_aspect("equal")
xav, yav = 0, 0

for well in wells:
    xwell = wells[well]["easting"]
    ywell = wells[well]["northing"]
    ilateral = np.where(wells[well]["tvdss"] < -2260)
    xav += np.average(xwell[ilateral])
    yav += np.average(ywell[ilateral])
    ax.plot(xwell, ywell, "firebrick", lw=3, zorder=-10)

xav /= 5
yav /= 5

for station in proposed_stations:
    ax.plot(
        proposed_stations[station]["easting"],
        proposed_stations[station]["northing"],
        "h",
        color="forestgreen",
        markeredgecolor="k",
        ms=10,
        zorder=2,
    )
x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()

for station in dsa_stations:
    ax.plot(
        dsa_stations[station]["easting"],
        dsa_stations[station]["northing"],
        "v",
        color="darkgoldenrod",
        markeredgecolor="k",
        ms=10,
        zorder=3,
    )

ax.set_xlim([x1, x2])
ax.set_ylim([y1, y2])
ax.set_xlabel("easting (m)")
ax.set_ylabel("northing (m)")
plt.show()

f2, a2 = plt.subplots()

for station in proposed_stations:
    (ps,) = a2.plot(
        proposed_stations[station]["easting"],
        proposed_stations[station]["northing"],
        "h",
        color="forestgreen",
        markeredgecolor="k",
        ms=10,
        zorder=2,
    )

for station in dsa_stations:
    (ds,) = a2.plot(
        dsa_stations[station]["easting"],
        dsa_stations[station]["northing"],
        "v",
        color="darkgoldenrod",
        markeredgecolor="k",
        ms=10,
        zorder=3,
    )
    a2.text(
        dsa_stations[station]["easting"] + 100,
        dsa_stations[station]["northing"] + 100,
        station,
    )
a2.legend([ps, ds], ["13-node superstation", "DSA station"])
plt.show()
