import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (
    MultipleLocator,
    FormatStrFormatter,
    AutoMinorLocator,
    LogLocator,
)

import glob


def sqa(x):
    return np.squeeze(np.array(x))


image_stats = {}
noise_sample = []
ncolor = {
    "7 stations": "firebrick",
    "13 stations": "royalblue",
    "19 stations": "forestgreen",
}

ms = {"7 stations": 5, "13 stations": 7, "19 stations": 9}

factor = {
    "7 stations": np.sqrt(7.0 / 19.0),
    "13 stations": np.sqrt(13.0 / 19.0),
    "19 stations": 1.0,
}


xMajorLocator = MultipleLocator(0.5)
xMinorLocator = MultipleLocator(0.1)
yMajorLocator = LogLocator(10)
yMinorLocator = LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9])

# fig,ax= plt.subplots(5,2)
ii = -1
for position in ["center", "toe"]:
    ii += 1
    image_stats[position] = {}
    jj = -1
    for nstations in ["7 stations", "13 stations", "19 stations"]:
        jj += 1
        nstat = nstations.split(" ")[0]
        csv = "high_noise_catalog_15_by_" + nstat + "_" + position + ".csv"
        image_stats[position][nstations] = []
        f = open(csv)
        head = f.readline()
        lines = f.readlines()
        f.close()
        for line in lines:
            lspl = line.split(",")
            true_loc = np.array([float(s) for s in lspl[2:5]])
            magnitude, stress_drop = [float(s) for s in lspl[5:7]]
            m11, m22, m33, m12, m13, m23 = [float(s) for s in lspl[7:13]]
            amplitude = float(lspl[13])
            image_loc = np.array([float(s) for s in lspl[14:17]])
            image_stats[position][nstations].append(
                {
                    "magnitude": magnitude,
                    "amplitude": amplitude,
                    "location shift": true_loc - image_loc,
                }
            )
            if magnitude < -1.7:
                noise_sample.append(np.log10(factor[nstations] * amplitude))
        mag = [v["magnitude"] for v in image_stats[position][nstations]]
        amp = [v["amplitude"] for v in image_stats[position][nstations]]

noise_sample = np.array(noise_sample)

threshold = 10 ** (np.average(noise_sample) + 2 * np.std(noise_sample))

f2, a2 = plt.subplots(2, figsize=[16, 10])
ii = -1
stacks = []
patches = []
M_detect = {}
for nstations in ["7 stations", "13 stations", "19 stations"]:
    jj = -1
    ii += 1
    M_detect[nstations] = {}
    for position in ["center", "toe"]:
        M_detect[nstations][position] = {}
        jj += 1
        M_detect[nstations][position] = {}
        mag = np.array([v["magnitude"] for v in image_stats[position][nstations]])
        amp = np.array([v["amplitude"] for v in image_stats[position][nstations]])
        (p,) = a2[jj].semilogy(
            mag, factor[nstations] * amp, marker="h", color=ncolor[nstations]
        )
    patches.append(p)
    incoherent_stacks = []
    jj = -1
    for position in ["center", "toe"]:
        jj += 1
        mag = np.array([v["magnitude"] for v in image_stats[position][nstations]])
        amp = np.array([v["amplitude"] for v in image_stats[position][nstations]])
        idetectable = np.where(factor[nstations] * amp > threshold)[0]
        ilarge = np.where(mag > -0.5)[0]
        large_amps = np.matrix(np.log10(factor[nstations] * amp[ilarge]))
        Amatrix = np.matrix(
            np.vstack([mag[ilarge] * mag[ilarge], mag[ilarge], np.ones(len(ilarge))])
        ).T
        quad, slope, yint = [
            sqa(s)
            for s in np.linalg.inv(Amatrix.T * Amatrix) * Amatrix.T * large_amps.T
        ]
        detectability_line = 10 ** (mag * mag * quad + mag * slope + yint)
        M_detect[nstations][position] = (
            (-slope + np.sqrt(slope * slope - 4 * quad * (yint - np.log10(threshold))))
            / 2
            / quad
        )
        a2[jj].plot(mag, detectability_line, "k:", lw=1)
        a2[jj].plot([-3, 3], [threshold, threshold], "k-.", lw=2)
        a2[jj].set_xlim([-2.6, -0])
        a2[jj].set_ylim([0.02, 10])
        a2[jj].grid(True, which="both")
        a2[jj].yaxis.set_major_locator(yMajorLocator)
        a2[jj].yaxis.set_minor_locator(yMinorLocator)
        a2[jj].xaxis.set_major_locator(xMajorLocator)
        a2[jj].xaxis.set_minor_locator(xMinorLocator)
        a2[jj].text(-2.1, 1.1 * threshold, "2$\sigma$ detectability threshold")
a2[0].set_title("center of pad")
a2[1].set_title("toe of well")
a2[1].set_xlabel("magnitude")
a2[0].set_ylabel("amplitude")
a2[1].set_ylabel("amplitude")
a2[1].legend(patches, ["7 nodes", "13 nodes", "19 nodes"])
a2[0].legend(patches, ["7 nodes", "13 nodes", "19 nodes"])

plt.show()

np.log10(2 * np.sqrt(42) / 3)

M_detect
