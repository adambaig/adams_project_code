import glob
import numpy as np

import utm


def read_wells():
    dr = "C:\\Users\\adambaig\\Project\\Paramount 2-28\\"
    wells = {}
    for well_file in glob.glob(dr + "DS_04*"):
        ext = well_file.split(".")[-1]
        well = well_file.split("~")[1]
        wells[well] = {}

        f = open(well_file)
        for ii in range(24):
            dumhead = f.readline()
        head = f.readline()
        lines = f.readlines()
        f.close()
        nlines = len(lines)
        wells[well]["tvdss"] = np.zeros(nlines)
        wells[well]["easting"] = np.zeros(nlines)
        wells[well]["northing"] = np.zeros(nlines)
        ii = -1
        for line in lines:
            ii += 1
            if ext == "csv":
                lspl = line.split(",")
            elif ext == "txt":
                lspl = line.split()
            wells[well]["tvdss"][ii] = float(lspl[4])
            wells[well]["easting"][ii] = float(lspl[9])
            wells[well]["northing"][ii] = float(lspl[10])
    return wells


def read_dsa():
    dr = "C:\\Users\\adambaig\\Project\\Paramount 2-28\\"
    dsa_file = dr + "DSA_05MArch2019.csv"
    stations = {}
    f = open(dsa_file)
    head = f.readline()
    lines = f.readlines()
    nlines = len(lines)
    f.close()
    ii = -1
    for line in lines:
        lspl = line.split(",")
        station = lspl[0] + "." + lspl[1]
        stations[station] = {}
        (
            stations[station]["easting"],
            stations[station]["northing"],
            d1,
            d2,
        ) = utm.from_latlon(float(lspl[2]), float(lspl[3]))
        stations[station]["elevation"] = float(lspl[4])
    return stations


def read_stations(station_file):
    stations = {}
    f = open(station_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        stations[lspl[0]] = {}
        stations[lspl[0]]["northing"] = float(lspl[1])
        stations[lspl[0]]["easting"] = float(lspl[2])
        stations[lspl[0]]["tvdss"] = float(lspl[3])
    return stations


def read_15_stations(writeout=True):
    stations = {}
    f = open("stations_15.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        stations[lspl[0]] = {}
        (
            stations[lspl[0]]["easting"],
            stations[lspl[0]]["northing"],
            d1,
            d2,
        ) = utm.from_latlon(float(lspl[1]), float(lspl[2]))
        stations[lspl[0]]["tvdss"] = -855
    if writeout:
        g = open("Paramount_stations_15.csv", "w")
        g.write("station, northing (m), easting (m), tvdss (m)\n")
        for station in stations:
            g.write(
                station
                + ",%.1f,%.1f,%.1f\n"
                % (
                    stations[station]["northing"],
                    stations[station]["easting"],
                    stations[station]["tvdss"],
                )
            )
        g.close()
    return stations


def place_stations(centerx, centery, nx, ny, writeout=False):
    target_depth = 3150
    lateral_length = 3000
    pad_width = 1400
    xwide = target_depth + pad_width / 2
    ywide = target_depth + lateral_length / 2
    easting_ctr, northing_ctr = 504043.3404684826, 6027716.559513638
    xrange = np.linspace(easting_ctr - xwide, easting_ctr + xwide, 12)
    yrange = np.linspace(northing_ctr - ywide, northing_ctr + ywide, 15)
    stations = []
    jj = -1
    for ix in xrange:
        for iy in yrange:
            jj += 1
            stations.append({"name": str(jj), "x": ix, "y": iy, "z": -855.7})
    if writeout:
        g = open("Paramount_stations_180.csv", "w")
        g.write("station, northing (m), easting (m), tvdss (m)\n")
        for station in stations:
            g.write(
                station["name"]
                + ",%.1f,%.1f,%.1f\n" % (station["y"], station["x"], station["z"])
            )
        g.close()
    return stations


def read_velocity_model():
    kb = 855.7
    dr = "O:\\O&G\\SalesSupport\\20190305_Paramount_2-28\\MC\\"
    f = open(dr + "DSA.mdl")
    lines = f.readlines()
    f.close()
    layer_colors = [""]
    velocity_model = []
    ii = -1
    for line in lines:
        ii += 1
        lspl = line.split()
        vp = float(lspl[0])
        vs = float(lspl[1])
        rho = 310 * vp ** (0.25)
        top = 1000 * float(lspl[2]) - kb
        if ii == 0:
            velocity_model.append(
                {"rho": rho, "vp": vp, "vs": vs, "color": layer_colors[ii]}
            )
        else:
            velocity_model.append(
                {"top": top, "rho": rho, "vp": vp, "vs": vs, "color": layer_colors[ii]}
            )
