import numpy as np
import glob

from obspy import Stream, Trace, UTCDateTime
from obspy.core.trace import Stats
import utm
from ray_modelling.waveform_model import get_response, add_noise
from ray_modelling.raytrace import isotropic_ray_trace

from get_inputs import read_velocity_model

dt = 0.002 # sample rate
time_series = np.arange(0, 5.00, dt)
noise_level = 3.e-8/np.sqrt(42.)
easting_ctr,northing_ctr = 504043.3404684826, 6027716.559513638
depth = 2280.
n_iterations = 20

for stationsCSV in ['./Paramount_stations_15.csv' ]:
    for location_string in ['toe','center']:
        if location_string == 'toe':
            xshift = 500
            yshift = 1600
        elif location_string =='center':
            xshift = 0
            yshift = 0
        def sqa(x):
            return(np.squeeze(np.array(x)))


        stations = []
        f = open(stationsCSV)
        stationlines = f.readlines()
        f.close()
        for line in stationlines:
            lspl = line.split(',')
            stations.append({
        		"x": float(lspl[2]),
        		"y": float(lspl[1]),
        		"z": float(lspl[3]),
        		"name": lspl[0]
        		})
		nstations = str(len(stations))

        velocity_model = read_velocity_model()

        def createTraceStats(network,n1,o1,d1,component):
            stat=Stats()
            stat.starttime=UTCDateTime(o1)
            stat.npts=n1
            stat.delta=d1
            stat.sampling_rate=(1/d1)
            stat.component=component['component']
            stat.channel=component['channel']
            stat.station=component['station']
            stat.network=network
            stat.location=component['location']
            return stat

        Q = {"P": 60, "S": 60}

        source = {
                "x": easting_ctr+xshift,
                "y": northing_ctr+yshift,
                "z": depth,
                "moment_magnitude": -2.5,
                "stress_drop": 3e5,  # static stress drop
                "moment_tensor": np.matrix([[0,1,0],[1,0,0],[0,0,0]])
                  }
        m11,m12,m13,d1,m22,m23,d2,d3,m33 = sqa(source["moment_tensor"].reshape(9,))
        mt_6 = [m11,m22,m33,m12,m13,m23]
        source["moment_tensor"] = (source["moment_tensor"] /
                                     np.linalg.norm(source["moment_tensor"]))
        g = open('test_catalog_'+nstations+'_'+location_string+'.csv','w')
        g.write('date,time,x,y,z,magnitude,stress drop,m11,m22,m33,m12,m13,m23\n')

        for mag in np.arange(-2.5,1.01,0.1):
            for ii in range(len(stations)):
                pRaypath = isotropic_ray_trace(source, stations[ii], velocity_model, "P")
                sRaypath = isotropic_ray_trace(source, stations[ii], velocity_model, "S")
                base_waveforms.append(get_response(pRaypath, sRaypath, source,
                                          stations[ii], Q, time_series, 0))
			for i_iteration in range(n_iterations):
	            base_waveforms = []
	            source["moment_magnitude"] = mag

	            waveforms = []
	            top, bottom = -1200, 3500
	            time_series = np.arange(0, 5.00, dt)
	            st = Stream()
	            for ii in range(len(stations)):
	                waveforms.append({})
	                for comp in ['n','e','d']:
	                    waveforms[-1][comp] = add_noise(time_series, noise_level) + base_waveforms[ii][comp]
	            for ii in range(len(waveforms)):
	                seedtime = UTCDateTime.now()
	                channel_obj = {
	                                'component': 'Z',
	                                'channel': 'CPZ',
	                                'station': stations[ii]['name'],
	                                'network': 'XX',
	                                'location': '00'
	                                }
	                xStats = createTraceStats('TC',len(time_series),seedtime,dt,channel_obj)
	                tr = Trace(data=1e8*waveforms[ii]['d'], header=xStats) # 1e8 is the scaling factor deduced from the SEG2 data
	                st += tr
	            outfile  = ('seeds/synthetic'+str(seedtime)+'.mseed').replace(':','-')
	            st.write(outfile)
	            dat,tim = str(seedtime).split('T')
	            g.write(dat+','+tim[:-1]+',')
	            g.write('%.1f,%.1f,%.1f,' % (source['x'], source['y'],source['z']))
	            g.write('%.3f,%.4e,' % (source['moment_magnitude'], source['stress_drop']))
	            g.write('%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,' % (m11,m22,m33,m12,m13,m23))
    g.close()
