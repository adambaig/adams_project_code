import matplotlib

matplotlib.use("Qt5Agg")

import matplotlib.pyplot as plt
import numpy as np
import sys

sys.path.append("..")

from read_inputs import read_wells, read_stations


def gray_background_with_grid(axis, grid_spacing=250):
    axis.set_facecolor("lightgrey")
    axis.set_aspect("equal")
    x1, x2 = axis.get_xlim()
    y1, y2 = axis.get_ylim()
    grid_spacing2 = 2 * grid_spacing
    x_minor_ticks = np.arange(
        np.floor(x1 / grid_spacing) * grid_spacing,
        np.ceil(x2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    x_major_ticks = np.arange(
        np.floor(x1 / grid_spacing2) * grid_spacing2,
        np.ceil(x2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    y_minor_ticks = np.arange(
        np.floor(y1 / grid_spacing) * grid_spacing,
        np.ceil(y2 / grid_spacing) * grid_spacing,
        grid_spacing,
    )
    y_major_ticks = np.arange(
        np.floor(y1 / grid_spacing2) * grid_spacing2,
        np.ceil(y2 / grid_spacing2) * grid_spacing2,
        grid_spacing2,
    )
    axis.set_xticks(x_minor_ticks, minor=True)
    axis.set_yticks(y_minor_ticks, minor=True)
    axis.set_xticks(x_major_ticks, minor=False)
    axis.set_yticks(x_major_ticks, minor=False)

    axis.grid(which="both")
    axis.grid(which="minor", alpha=1)
    axis.set_xlim([x1, x2])
    axis.set_ylim([y1, y2])
    axis.set_yticklabels([])
    axis.set_xticklabels([])
    axis.tick_params(which="both", color=(0, 0, 0, 0))
    axis.set_axisbelow(True)

    return axis


wells = read_wells()
stations = read_stations()

f = open("archive//noise_comparison_apr_22.csv")
head = f.readline()
lines = f.readlines()
f.close()
semblance_stack = np.zeros([15, 288])
simple_stack = np.zeros([15, 288])
individual = np.zeros([15, 7, 288])
for i_line, line in enumerate(lines):
    lspl = line.split(",")
    n_count = 2
    for i_station in range(15):
        individual[i_station, :, i_line] = np.array(
            [float(s) for s in lspl[n_count : n_count + 7]]
        )
        simple_stack[i_station, i_line] = float(lspl[n_count + 7])
        semblance_stack[i_station, i_line] = float(lspl[n_count + 8])
        n_count += 9


east_station = [v["e"] for v in stations.values()]
north_station = [v["n"] for v in stations.values()]

np.median(simple_stack, axis=1)
np.median(semblance_stack, axis=1)


fig, (a1, a2, a3) = plt.subplots(1, 3, figsize=[16, 5])
for ax in [a1, a2, a3]:

    ax.set_aspect("equal")

    for well in wells.values():
        ax.plot(well["easting"], well["northing"], "k", zorder=9)


a1.scatter(
    east_station,
    north_station,
    c=np.log10(np.median(simple_stack, axis=1)),
    zorder=10,
    edgecolor="0.2",
)
a1.set_title("median simple stack noise")

a2.scatter(
    east_station,
    north_station,
    c=np.log10(np.median(semblance_stack, axis=1)),
    zorder=10,
    edgecolor="0.2",
)
a2.set_title("median SWS noise")

a3.scatter(
    east_station,
    north_station,
    c=np.log10(np.median(semblance_stack / simple_stack, axis=1)),
    zorder=10,
    edgecolor="0.2",
)
a3.set_title("median ratio sws/simple")


for ax in [a1, a2, a3]:

    ax = gray_background_with_grid(ax, grid_spacing=500)

plt.show()
