import numpy as np
import matplotlib.pyplot as plt
from obspy import read
from obspy.signal import filter
import glob
from scipy.fftpack import fft, ifft
from datetime import datetime, timedelta

dr = "/data/mount_doom/home/adamb/projects/paramount_2-28/data/seed/"

seeds = glob.glob(dr + "*.seed")
seeds.sort()
st = read(seeds[0])
day1 = datetime.strptime(seeds[0].split("/")[-1].split(".")[0], "%Y%m%d")
dayn = datetime.strptime(seeds[-1].split("/")[-1].split(".")[0], "%Y%m%d")
day_delta = timedelta(days=1)
df = st[0].stats.sampling_rate

for i_day in range((dayn - day1 + day_delta).days):
    current_day = day1 + day_delta * i_day
    daystr = datetime.strftime(current_day, "%Y%m%d")
    print(daystr)
    daily_noise = np.zeros([105, 288])
    day_seeds = glob.glob(dr + daystr + "*")
    for seed in day_seeds:
        timestr = seed.split(".")[1]
        hour = int(timestr[:2])
        minute = int(timestr[2:4])
        jj = 12 * hour + minute // 5
        st = read(seed)
        for ii, trace in enumerate(st):
            signal = trace.data / 1000 / 59.0
            daily_noise[ii, jj] = np.std(filter.bandpass(signal, 20, 60, df))

    fig, ax = plt.subplots(figsize=[16, 12])
    noise_plot = ax.pcolor(
        np.log10(daily_noise), vmin=-8, vmax=-5, cmap="nipy_spectral"
    )
    ax.set_yticks(range(0, 106, 7))

    ax.set_yticklabels("")
    ax.set_yticks(np.arange(3.5, 110, 7), minor=True)
    ax.set_yticklabels(range(1, 16), minor=True)
    ax.set_xticks(np.arange(0, 289, 24))
    ax.set_xticklabels(
        [
            "12 am",
            "2 am",
            "4 am",
            "6 am",
            "8 am",
            "10 am",
            "12 pm",
            "2 pm",
            "4 pm",
            "6 pm",
            "8 pm",
            "10 pm",
            "12 am",
        ]
    )
    ax.set_ylim([105, 0])
    ax.set_xlim([0, 288])
    ax.set_xlabel("UTC Time")
    ax.set_ylabel("superstation")
    ax.set_title(datetime.strftime(current_day, "%b %d, %Y"))
    ax.grid(which="major", axis="y")
    cb = fig.colorbar(noise_plot, extend="both")
    cb.set_label("RMS level between 20 and 60 Hz(m/s)")
    l3 = np.log10(3)
    cb.set_ticks([-8, -8 + l3, -7, -7 + l3, -6, -6 + l3, -5])
    cb.set_ticklabels(
        [
            "10$^{-8}$",
            "3X10$^{-8}$",
            "10$^{-7}$",
            "3X10$^{-7}$",
            "10$^{-6}$",
            "3X10$^{-6}$",
            "10$^{-5}$",
        ]
    )
    fig.savefig("rms_noise_" + daystr + ".png")
