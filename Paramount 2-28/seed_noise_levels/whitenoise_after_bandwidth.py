import matplotlib

matplotlib.use("Qt5agg")

from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np
from obspy.signal import filter

from sms_ray_modelling.waveform_model import add_noise

viridis = cm.get_cmap("viridis")

time_series = np.linspace(0, 5, 1251)
noise = add_noise(time_series, 1)

df = 1 / time_series[1]
freq = np.fft.fftfreq(1251, 1 / df)

fig, ax = plt.subplots()
corners = [5, 15, 25, 35, 45]
for i_low_corner, low_corner in enumerate(corners[:-1]):
    for high_corner in corners[i_low_corner + 1 :]:
        rms_filt_noise = np.sqrt(
            np.average(filter.bandpass(noise, low_corner, high_corner, df) ** 2)
        )
        ax.loglog(low_corner, high_corner, "o", color=viridis(rms_filt_noise))
        print(low_corner, high_corner, rms_filt_noise)

plt.show()
