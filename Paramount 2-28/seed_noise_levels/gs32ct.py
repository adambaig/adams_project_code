import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft, ifft
from obspy import read

from obspy.signal.invsim import corn_freq_2_paz

paz_10hz = corn_freq_2_paz(10.0, damp=np.sqrt(0.5))

respfile = "GS32CT.csv"
st = read("20190420.235500.000000_20190421.000000.000000.seed")
# print (st)
N = len(st[0].data)
T = st[0].stats.delta
df = 1.0 / T

xf = np.linspace(0.0, 1.0 / (T), N)
dataFFT = fft(
    st[0].data / 1000 / 10**1.8
)  # convert from mV to V and account for 36 dB gain
# Geospace says gain has been backed out already but numbers work out if we include it


my_data = np.genfromtxt(respfile, delimiter=",", skip_header=1)

freq = my_data[:, 0]
sens = my_data[:, 2]
phase = my_data[:, 3]


interpSens = np.interp(xf, freq, sens)
interpPhase = np.interp(xf, freq, phase)

iFFTresp = []
for s, p in zip(interpSens, interpPhase):
    tmpReal = s * np.cos(p * np.pi / 180.0)
    tmpImag = s * np.sin(p * np.pi / 180.0)
    tmp = np.complex(tmpReal, tmpImag)
    iFFTresp.append(tmp)
    # iFFTresp.append(tmp*1000*10**1.8)

iFFTsignal = []
for d, r in zip(dataFFT, iFFTresp):
    iFFTsignal.append(d / r)

signal = ifft(iFFTsignal)
FiltKwargsEnv = {
    "type": "bandpass",
    "freqmin": 1,
    "freqmax": 100,
    "zerophase": True,
    "corners": 4,
}

st[0].data = signal
st[0].filter(**FiltKwargsEnv)
plt.plot(st[0].data)
# print (np.std(st[0].data))
plt.show()
exit()


# # print len(signal),len(st[0].data)
# # plt.plot(10*np.log(fft(st[0].data)*fft(st[0].data)))
# # plt.psd(signal,Fs=250,scale_by_freq=False)
# # plt.psd(st[0].data,Fs=250,scale_by_freq=False)
# # plt.show()
#
# # plt.psd(signal,Fs=250,scale_by_freq=False)
# # # plt.psd(st[0].data,Fs=250,scale_by_freq=False)
# # plt.show()
#
# # print iFFTsignal
#
# # plt.plot(freq,sens)
# # # plt.plot(xf,interpSens)
# # plt.show()
#
# paz_sts2 = {'poles': [-43.98-43.98j, 44.87-44.87j],
#          'zeros': [0j, 0j],
#          'gain': 10**1.8,
#          'sensitivity': 59.0593}
# # print paz_sts2
# # exit()
# st[0].data=st[0].data/1000.
# st[0].filter(**FiltKwargsEnv)
# # print np.min(st[0].data),np.max(st[0].data)
# st[0].data=st[0].data
# plt.plot(st[0].data)
# st[0].simulate(paz_remove=paz_sts2,remove_sensitivity=True)
# st[0].filter(**FiltKwargsEnv)
# # print np.min(st[0].data),np.max(st[0].data)
# plt.plot(st[0].data)
# plt.show()
# print(np.std(st[0].data))
