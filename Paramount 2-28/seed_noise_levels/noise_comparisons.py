from datetime import datetime, timedelta
import glob
import matplotlib.pyplot as plt
import numpy as np
from obspy import read
from obspy.signal import filter
from scipy.fftpack import fft, ifft

eps = 1e-18
raw_dr = "/data/mount_doom/home/adamb/projects/paramount_2-28/data/seed/"
prc_dr = "/data/mount_doom/home/adamb/projects/paramount_2-28/data/seed_proc/20190424*"
raw_seeds = glob.glob(raw_dr + "*.seed")
prc_seeds = glob.glob(prc_dr + "*.seed")
raw_seeds.sort()
prc_seeds.sort()
st = read(prc_seeds[0])
df = st[0].stats.sampling_rate


f = open("noise_comparison_apr_22.csv", "w")
f.write("starttime,endtime")
for trace in st:
    station_name = trace.stats.station + "." + trace.stats.station
    for ii in [str(s).zfill(2) for s in range(7)]:
        f.write("," + station_name + "." + ii)
    f.write("," + station_name + ".simple_stack," + station_name + ".sws_stack")
f.write("\n")
for raw_seed, prc_seed in zip(raw_seeds, prc_seeds):
    prc_st = read(prc_seed)
    raw_st = read(raw_seed)
    raw_noise = np.zeros([15, 7])
    prc_noise = np.zeros(15)
    stk_noise = np.zeros(15)
    for prc_trace in prc_st:
        signal = prc_trace.data / 1000 / 59.0
        i_station = int(prc_trace.stats.station) - 1
        prc_noise[i_station] = np.std(filter.bandpass(signal, 20, 60, df))
        n_nonzero = 0
        for raw_trace in raw_st.select(station=prc_trace.stats.station):
            i_location = int(raw_trace.stats.location) - 1
            raw_signal.append(raw_trace.data / 1000 / 59.0)
            raw_noise[i_station, i_location] = np.std(
                filter.bandpass(raw_signal[-1], 20, 60, df)
            )
            if raw_noise[i_station, i_location] > eps:
                n_nonzero += 1

        stk_signal = np.sum(np.array(raw_signal), axis=0) / n_nonzero
        stk_noise[i_station] = np.std(filter.bandpass(stk_signal, 20, 60, df))
    f.write(raw_seed[0].stats.starttime + "," + raw_seed[0].stats.endtime)
    for i_station in range(15):
        for i_location in range(7):
            f.write(",%.8e" % raw_noise[i_station, i_location])
        f.write(",%.8e" % stk_noise[i_station, i_location])
        f.write(",%.8e" % prc_noise[i_station, i_location])
    f.write("\n")
f.close()
