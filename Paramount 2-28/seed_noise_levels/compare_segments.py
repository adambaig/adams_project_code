import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
from obspy import read
from scipy.signal import detrend

station = 5

stream = "20190422.000500.000000_20190422.001000.000000.seed"

raw_stream = read("noise//seed//" + stream)
prc_stream = read("noise//seed_proc//" + stream)
prc_stream_2 = read("noise//seed_proc_2//" + stream)
print(prc_stream)
p_times = prc_stream[0].times()
p2_times = prc_stream_2[0].times()
r_times = raw_stream[0].times()
stn_stream = raw_stream.select(station="05")

plt.plot(p_times, prc_stream[5].data)
plt.plot(p2_times, 1e9 * prc_stream_2[5].data)

# plt.plot(r_times,detrend(stn_stream[0]),zorder=4,alpha=0.5)
plt.show()
