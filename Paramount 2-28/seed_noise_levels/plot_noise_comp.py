import matplotlib

matplotlib.use("Qt5agg")
import matplotlib.pyplot as plt
import numpy as np

f = open("archive//noise_comparison_apr_22.csv")
head = f.readline()
lines = f.readlines()
f.close()

semblance_stack = np.zeros([15, 288])
simple_stack = np.zeros([15, 288])
individual = np.zeros([15, 7, 288])
for i_line, line in enumerate(lines):
    lspl = line.split(",")
    n_count = 2
    for i_station in range(15):
        individual[i_station, :, i_line] = np.array(
            [float(s) for s in lspl[n_count : n_count + 7]]
        )
        simple_stack[i_station, i_line] = float(lspl[n_count + 7])
        semblance_stack[i_station, i_line] = float(lspl[n_count + 8])
        n_count += 9

l3 = np.log10(3)
fig, (ax1, ax2) = plt.subplots(2, sharex=True)
color_plot = ax1.pcolor(np.log10(simple_stack), vmin=-10, vmax=-6, cmap="nipy_spectral")
ax2.pcolor(np.log10(semblance_stack), vmin=-10, vmax=-6, cmap="nipy_spectral")
fig.subplots_adjust(right=0.85)
cbar_ax = fig.add_axes([0.9, 0.1, 0.02, 0.8])
cb = fig.colorbar(color_plot, cax=cbar_ax)
cb.set_label("noise level (m/s)")
cb.set_ticks([-10, -10 + l3, -9, -9 + l3, -8, -8 + l3, -7, -7 + l3, -6])
cb.set_ticklabels(
    [
        "${1 \cdot 10^{-10}}$",
        "$3 \cdot 10^{-9}$",
        "$1 \cdot 10^{-9}$",
        "$3 \cdot 10^{-8}$",
        "$1 \cdot 10^{-8}$",
        "$3 \cdot 10^{-7}$",
        "$1 \cdot 10^{-7}$",
        "$3 \cdot 10^{-6}$",
        "$1 \cdot 10^{-6}$",
    ]
)
for ax in [ax1, ax2]:
    ax.set_yticks(range(0, 16))
    ax.set_yticklabels("")
    ax.set_yticks(np.arange(0.5, 15, 1), minor=True)
    ax.set_yticklabels(range(1, 16), minor=True)
    ax.set_xticks(np.arange(0, 289, 24))
    ax.set_ylim([15, 0])
    ax.set_xlim([0, 288])
    ax.set_ylabel("superstation")
    ax.tick_params(which="minor", color="#00000000")
ax2.set_xticklabels(
    [
        "12 am",
        "2 am",
        "4 am",
        "6 am",
        "8 am",
        "10 am",
        "12 pm",
        "2 pm",
        "4 pm",
        "6 pm",
        "8 pm",
        "10 pm",
        "12 am",
    ]
)
ax2.set_xlabel("UTC Time")
ax1.set_title("Simple Stack")
ax2.set_title("Semblance-Weighted Stack")
f2, a2 = plt.subplots()
a2.hist(
    np.log10(np.ndarray.flatten(simple_stack / semblance_stack)),
    bins=np.arange(0, 3, 0.025),
)
l5 = np.log10(np.sqrt(10))
xticks = [0, l5, 1, 1 + l5, 2, 2 + l5, 3]
xticklabels = ["1", "5", "10", "15", "20", "25", "30"]
a2.set_xticks(xticks)
a2.set_xticklabels(xticklabels)
a2.set_xlabel("noise reduction factor (dB)")
a2.set_ylabel("count")
plt.show()
