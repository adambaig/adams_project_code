import matplotlib.pyplot as plt
import numpy as np
from obspy import UTCDateTime
from pathlib import Path
from matplotlib import colors
import matplotlib.dates as mdates
import sys
import datetime
import pytz


def initPFIPlots(
    catalog,
    wellpaths,
    stageInfo=None,
    idCol=0,
    tCol=1,
    xlocCol=2,
    ylocCol=3,
    zlocCol=4,
    wellNameCol=24,
    wellIdCol=25,
    stageCol=26,
    stageIDCol=27,
    magCol=6,
    genMTCol=8,
    genRCol=14,
    dcMTCol=16,
    dcRCol=22,
    confCol=28,
    xerrcol=29,
    yerrcol=30,
    zerrcol=31,
    colorby="Time",
    cmap="jet",
    edgecolor="k",
    edgeWidth=0.25,
    majorticks=500,
    wantLabels=False,
    labelsz=13,
    outcolor="w",
    depthexag=1.0,
    wellcolor="k",
    stagecolor="k",
    stagesym="+",
    alpha=1,
    eventsz=15,
    bkgroundcol="lightgrey",
    sizeByMag=True,
):

    eventLocs = np.genfromtxt(catalog, delimiter=",", dtype="U32")

    inputs = getdata(
        eventLocs,
        idCol=idCol,
        tCol=tCol,
        xlocCol=xlocCol,
        ylocCol=ylocCol,
        zlocCol=zlocCol,
        wellNameCol=wellNameCol,
        wellIdCol=wellIdCol,
        stageCol=stageCol,
        stageIDCol=stageIDCol,
        magCol=magCol,
        genMTCol=8,
        xerrcol=xerrcol,
        yerrcol=yerrcol,
        zerrcol=zerrcol,
    )

    t0s = []
    for t0 in inputs["t0s"]:
        t0s.append(UTCDateTime(t0).timestamp)
    t0s = np.array(t0s)
    plotparams = {}
    plotparams["xlocs"] = inputs["xlocs"]
    plotparams["ylocs"] = inputs["ylocs"]
    plotparams["zlocs"] = inputs["zlocs"]
    plotparams["xerrs"] = inputs["xerrs"]
    plotparams["yerrs"] = inputs["yerrs"]
    plotparams["zerrs"] = inputs["zerrs"]
    plotparams["ids"] = inputs["ids"]
    plotparams["t0s"] = t0s
    plotparams["wellnames"] = inputs["wellnames"]
    plotparams["wellIds"] = inputs["wellIds"]
    plotparams["stages"] = inputs["stages"]
    plotparams["stageIds"] = inputs["stageIds"]
    plotparams["mags"] = inputs["mags"]
    plotparams["genMTs"] = inputs["genMTs"]
    plotparams["genRs"] = inputs["genRs"]
    plotparams["dcMTs"] = inputs["dcMTs"]
    plotparams["dcRs"] = inputs["dcRs"]
    plotparams["confVals"] = inputs["conf"]
    plotparams["exaggeration"] = depthexag

    uniqueWells = np.unique(sorted(inputs["wellnames"]))

    if stageInfo is not None:
        minstage, maxstage = getStageMinMax(stageInfo)

    mint = mdates.date2num(makedateTime(min(t0s)))
    maxt = mdates.date2num(makedateTime(max(t0s)))

    colorlab = [
        "tab:blue",
        "tab:orange",
        "tab:green",
        "tab:red",
        "tab:purple",
        "tab:brown",
        "tab:pink",
        "tab:gray",
        "tab:olive",
        "tab:cyan",
    ]
    if colorby == "Stage":
        c = inputs["stageIds"]
        mincol = minstage
        maxcol = maxstage
    elif colorby == "Well":
        cmap = colors.ListedColormap(["w"] + colorlab[0 : len(uniqueWells) - 1])
        c = []
        for wellname in plotparams["wellnames"]:
            if wellname == "-1":
                c.append(0)
            else:
                c.append(np.where(uniqueWells == wellname)[0][0] % 10)
        mincol = -0.5
        maxcol = len(uniqueWells) - 0.5
    else:
        c = t0s
        mincol = mint
        maxcol = maxt

    eventszArr = []

    if sizeByMag:
        eventszArr = np.interp(
            inputs["mags"],
            (inputs["mags"].min(), inputs["mags"].max()),
            (eventsz, eventsz * 10),
        )
        eventszArr = np.array(eventszArr)
    else:
        for ii in range(len(inputs["mags"])):
            eventszArr.append(eventsz)
        eventszArr = np.array(eventszArr)
    if edgecolor is not None:
        edges = []
        edgeWs = []
        for ii in range(len(inputs["mags"])):
            edges.append(edgecolor)
            edgeWs.append(edgeWidth)
        edges = np.array(edges)
        edgeWs = np.array(edgeWs)
    else:
        edges = None
        edgeWs = None

    plotparams["c"] = np.array(c)
    plotparams["colvals"] = [mincol, maxcol]

    if colorby == "Stage":
        cbarlabel = "Stage index"
        timeticks = False
    elif colorby == "Well":
        cbarlabel = "Well"
        timeticks = False
    else:
        colorby = "Time"
        cbarlabel = "Event time"
        timeticks = True

    plotparams["colorby"] = colorby
    plotparams["cbarlabel"] = cbarlabel
    plotparams["timeticks"] = timeticks

    minX, maxX, minY, maxY, minZ, maxZ = getMinMax(
        wellpaths, inputs["xlocs"], inputs["ylocs"], inputs["zlocs"]
    )
    (
        minor_ticksX,
        major_ticksX,
        minor_ticksY,
        major_ticksY,
        minor_ticksZ,
        major_ticksZ,
    ) = getTicks(minX, maxX, minY, maxY, minZ, maxZ, majorticks)

    plotparams["mX"] = [minX, maxX]
    plotparams["mY"] = [minY, maxY]
    plotparams["mZ"] = [minZ, maxZ]

    xper = (maxX - minX) / ((maxX - minX) + (maxY - minY))
    yper = (maxY - minY) / ((maxX - minX) + (maxY - minY))
    zper = depthexag * (maxZ - minZ) / (depthexag * (maxZ - minZ) + (maxY - minY))

    xplotsize = (maxX - minX) + (maxY - minY)
    yplotsize = depthexag * (maxZ - minZ) + (maxY - minY)
    if xplotsize > yplotsize:
        yplotsize = int(yplotsize * 98 / xplotsize)
        xplotsize = 98
    else:
        xplotsize = xplotsize * 98 / yplotsize
        yplotsize = 98

    plotparams["ptsX"] = int(round(xplotsize * xper))
    plotparams["ptsY"] = int(round(xplotsize * yper))
    plotparams["ptsZ"] = int(round(yplotsize * zper))
    # print(plotparams['ptsX'], plotparams['ptsY'], plotparams['ptsZ'])
    # exit()

    plotparams["ticsX"] = [minor_ticksX, major_ticksX]
    plotparams["ticsY"] = [minor_ticksY, major_ticksY]
    plotparams["ticsZ"] = [minor_ticksZ, major_ticksZ]

    plotparams["wellpaths"] = wellpaths
    plotparams["stageInfo"] = stageInfo
    plotparams["cmap"] = cmap
    plotparams["majorticks"] = majorticks
    plotparams["wantLabels"] = wantLabels
    plotparams["labelsz"] = labelsz
    plotparams["outcolor"] = outcolor
    plotparams["alpha"] = alpha
    plotparams["eventsz"] = eventszArr
    plotparams["bkgroundcol"] = bkgroundcol
    plotparams["wellcolor"] = wellcolor
    plotparams["stagecolor"] = stagecolor
    plotparams["stagesym"] = stagesym
    plotparams["edgecolor"] = edges
    plotparams["edgeWidth"] = edgeWs

    return plotparams


def getdata(
    eventLocs,
    idCol=0,
    tCol=1,
    xlocCol=2,
    ylocCol=3,
    zlocCol=4,
    wellNameCol=24,
    wellIdCol=25,
    stageCol=26,
    stageIDCol=27,
    magCol=6,
    genMTCol=8,
    genRCol=14,
    dcMTCol=16,
    dcRCol=22,
    confCol=28,
    xerrcol=29,
    yerrcol=30,
    zerrcol=31,
):

    inputs = {}
    inputs["xlocs"] = np.array(eventLocs[:, xlocCol]).astype(float)
    inputs["ylocs"] = np.array(eventLocs[:, ylocCol]).astype(float)
    inputs["zlocs"] = np.array(eventLocs[:, zlocCol]).astype(float)
    inputs["xerrs"] = np.array(eventLocs[:, xerrcol]).astype(float)
    inputs["yerrs"] = np.array(eventLocs[:, yerrcol]).astype(float)
    inputs["zerrs"] = np.array(eventLocs[:, zerrcol]).astype(float)

    inputs["ids"] = np.array(eventLocs[:, idCol]).astype(int)
    inputs["t0s"] = np.array(eventLocs[:, tCol]).astype(str)
    inputs["mags"] = np.array(eventLocs[:, magCol]).astype(float)

    inputs["wellnames"] = np.array(eventLocs[:, wellNameCol]).astype(str)
    inputs["wellIds"] = np.array(eventLocs[:, wellIdCol]).astype(int)
    inputs["stages"] = np.array(eventLocs[:, stageCol]).astype(int)
    inputs["stageIds"] = np.array(eventLocs[:, stageIDCol]).astype(int)

    inputs["genMTs"] = np.array(eventLocs[:, genMTCol : genMTCol + 6]).astype(float)
    inputs["dcMTs"] = np.array(eventLocs[:, dcMTCol : dcMTCol + 6]).astype(float)
    inputs["genRs"] = np.array(eventLocs[:, genRCol]).astype(float)
    inputs["dcRs"] = np.array(eventLocs[:, dcRCol]).astype(float)
    inputs["conf"] = np.array(eventLocs[:, confCol]).astype(float)
    return inputs


def getStageMinMax(stageInfo):
    minstage = np.min(stageInfo["stageid"])
    maxstage = np.max(stageInfo["stageid"])

    return minstage, maxstage


def getMinMax(wellpaths, xlocs, ylocs, zlocs):
    minxwell = 9999999999999999999
    maxxwell = -9999999999999999999
    minywell = 9999999999999999999
    maxywell = -9999999999999999999
    maxzwell = -9999999999999999999
    for key in wellpaths:
        if key != "-1":
            if min(wellpaths[key][:, 1]) < minxwell:
                minxwell = min(wellpaths[key][:, 1])
            if max(wellpaths[key][:, 1]) > maxxwell:
                maxxwell = max(wellpaths[key][:, 1])
            if min(wellpaths[key][:, 2]) < minywell:
                minywell = min(wellpaths[key][:, 2])
            if max(wellpaths[key][:, 2]) > maxywell:
                maxywell = max(wellpaths[key][:, 2])
            if max(wellpaths[key][:, 0]) > maxzwell:
                maxzwell = max(wellpaths[key][:, 0])

    minX = min(min(xlocs), minxwell)
    maxX = max(max(xlocs), maxxwell)
    minY = min(min(ylocs), minywell)
    maxY = max(max(ylocs), maxywell)
    minZ = min(zlocs)
    maxZ = max(max(zlocs), maxzwell)

    return minX, maxX, minY, maxY, minZ, maxZ


def getTicks(minX, maxX, minY, maxY, minZ, maxZ, majorticks):
    minorticks = majorticks / 2

    minor_ticksX = np.arange(
        int(minX / minorticks) * minorticks - majorticks,
        int(maxX / minorticks) * minorticks + majorticks,
        minorticks,
    )
    major_ticksX = np.arange(
        int(minX / majorticks) * majorticks - majorticks * 2,
        int(maxX / majorticks) * majorticks + majorticks * 2,
        majorticks,
    )

    minor_ticksY = np.arange(
        int(minY / minorticks) * minorticks - majorticks,
        int(maxY / minorticks) * minorticks + majorticks,
        minorticks,
    )
    major_ticksY = np.arange(
        int(minY / majorticks) * majorticks - majorticks * 2,
        int(maxY / majorticks) * majorticks + majorticks * 2,
        majorticks,
    )

    minor_ticksZ = np.arange(
        int(minZ / minorticks) * minorticks - majorticks,
        int(maxZ / minorticks) * minorticks + majorticks,
        minorticks,
    )
    major_ticksZ = np.arange(
        int(minZ / majorticks) * majorticks - majorticks * 2,
        int(maxZ / majorticks) * majorticks + majorticks * 2,
        majorticks,
    )

    return (
        minor_ticksX,
        major_ticksX,
        minor_ticksY,
        major_ticksY,
        minor_ticksZ,
        major_ticksZ,
    )


def plotStations(
    stations,
    wellpaths=None,
    plotSuperStation="-1",
    outputDir="./",
    wellcol="k",
    stationMarker="H",
    stationColor="b",
    majorticks=2000,
    bkgroundcol="lightgrey",
):
    plotSuperStation = str(plotSuperStation)
    Path(outputDir).mkdir(parents=True, exist_ok=True)
    minX, maxX, minY, maxY, minZ, maxZ = getMinMax(
        wellpaths, stations["X"], stations["Y"], stations["Z"]
    )
    minor_ticksX, major_ticksX, minor_ticksY, major_ticksY, __, __ = getTicks(
        minX, maxX, minY, maxY, minZ, maxZ, majorticks
    )
    fig, ax = plt.subplots()
    unique_stats = np.unique(stations["stations"])
    if wellpaths != []:
        for key in wellpaths:
            if key != "-1":
                ax.plot(wellpaths[key][:, 1], wellpaths[key][:, 2], wellcol, alpha=1)
    for us in unique_stats:
        uInds = np.where(stations["stations"] == us)
        meanX = np.mean(stations["X"][uInds])
        meanY = np.mean(stations["Y"][uInds])
        ax.scatter(meanX, meanY, marker=stationMarker, c=stationColor, alpha=1)
        ax.annotate(us, (meanX, meanY))
    setaxparams(
        ax,
        bkgroundcol,
        majorticks,
        minor_ticksX,
        major_ticksX,
        minor_ticksY,
        major_ticksY,
        minX,
        maxX,
        minY,
        maxY,
    )
    plt.xlabel("UTMX")
    plt.ylabel("UTMY")
    ax.set_aspect("equal")
    fig.savefig(outputDir + "/stationPlot.png", bbox_inches="tight")
    plt.close(fig)

    if plotSuperStation != "-1":
        fig, ax = plt.subplots()
        unique_stats = np.unique(stations["stations"])
        uInds = np.where(stations["stations"] == plotSuperStation)
        for x, y, name in zip(
            stations["X"][uInds], stations["Y"][uInds], stations["locations"][uInds]
        ):
            ax.scatter(
                x - np.mean(stations["X"][uInds]),
                y - np.mean(stations["Y"][uInds]),
                marker="v",
                c=stationColor,
                alpha=1,
            )
            ax.annotate(
                name,
                (x - np.mean(stations["X"][uInds]), y - np.mean(stations["Y"][uInds])),
            )

    minX = np.min(stations["X"][uInds] - np.mean(stations["X"][uInds]))
    maxX = np.max(stations["X"][uInds] - np.mean(stations["X"][uInds]))
    minY = np.min(stations["Y"][uInds] - np.mean(stations["Y"][uInds]))
    maxY = np.max(stations["Y"][uInds] - np.mean(stations["Y"][uInds]))

    minor_ticksX, major_ticksX, minor_ticksY, major_ticksY, __, __ = getTicks(
        minX, maxX, minY, maxY, minY, maxY, 10.0
    )
    setaxparams(
        ax,
        bkgroundcol,
        10,
        minor_ticksX,
        major_ticksX,
        minor_ticksY,
        major_ticksY,
        minX,
        maxX,
        minY,
        maxY,
    )
    plt.xlabel("Easting (m)")
    plt.ylabel("Northing (m)")
    ax.set_aspect("equal")
    fig.savefig(outputDir + "/superStationPlot.png", bbox_inches="tight")
    plt.close(fig)


def plotFM(par, magStep=0.2, minMag=None, maxMag=None, outputDir="./", bkgroundcol="w"):
    Path(outputDir).mkdir(parents=True, exist_ok=True)
    fig, ax = plt.subplots()
    # mags = np.log(par['mags']*1000)
    if minMag is None:
        minMag = np.round(np.min(mags), 1)
    if maxMag is None:
        maxMag = np.round(np.max(mags), 1)

    bins = np.arange(minMag, maxMag, magStep)
    magCount, magBin, __ = plt.hist(
        mags, bins=bins, color="b", alpha=0.6, edgecolor="k"
    )
    cumCount = np.cumsum(magCount[::-1])[::-1]
    mc = magBin[np.argmax(magCount)] + magStep * 0.5
    centerMags = magBin[:-1] + 0.5 * magStep
    bValHighMag = [
        centerMags[i] for i in range(len(magCount))[::-1] if magCount[i] >= 1
    ]
    if not bValHighMag:
        print("There are no bins with more than " + str(0) + " events")
        sys.exit()
    elif bValHighMag:
        bValHighMag = bValHighMag[0]
    plt.scatter(centerMags, cumCount, c="r", marker="o", edgecolors="k")
    bArgs = np.where((centerMags <= bValHighMag) & (centerMags >= mc))[0]
    bMags = centerMags[bArgs]
    bCumCounts = cumCount[bArgs]
    logBCumCount = np.log10(bCumCounts)
    # Determine and plot GR relationship
    A = np.vstack([bMags, np.ones(len(bMags))]).T
    b, a = np.linalg.lstsq(A, logBCumCount, rcond=None)[0]
    plt.plot(bMags, 10 ** (bMags * b + a), "k-")
    # Add labels, set limits
    plt.text(
        0.65,
        0.75,
        "No. events="
        + str(len(mags))
        + "\nMc: "
        + "{:.2f}".format(mc)
        + "\nb: "
        + "{:.2f}".format(-1 * b),
        transform=plt.gca().transAxes,
    )
    plt.yscale("log")
    plt.ylabel("Event Count")  # , fontsize=16)
    plt.xlim(minMag - 0.5, maxMag + 0.5)
    plt.xlabel("Mw")
    ax.set_facecolor(bkgroundcol)
    fig.savefig(outputDir + "/b-value.png", bbox_inches="tight")
    plt.close(fig)


def setaxparams(
    ax,
    bkgroundcol,
    majorticks,
    minor_ticks1,
    major_ticks1,
    minor_ticks2,
    major_ticks2,
    min1,
    max1,
    min2,
    max2,
):
    print(minor_ticks2)
    ax.set_facecolor(bkgroundcol)
    ax.set_xticks(minor_ticks1, minor=True)
    ax.set_yticks(minor_ticks2, minor=True)
    ax.set_xticks(major_ticks1, minor=False)
    ax.set_yticks(major_ticks2, minor=False)
    ax.grid(which="both")
    ax.grid(which="minor", alpha=1)
    ax.set_ylim([min2 - majorticks, max2 + majorticks])
    ax.set_xlim([min1 - majorticks, max1 + majorticks])
    ax.set_axisbelow(True)


def setFig(
    ax,
    plotname,
    wantLabels,
    mincol,
    maxcol,
    labelsz,
    label1,
    label2,
    flipY=False,
    wantaxis=[True, True],
):
    if wantLabels:
        if wantaxis[0]:
            ax.set_xlabel(label1, fontsize=labelsz)
        else:
            ax.set_xticklabels([])
        if wantaxis[1]:
            ax.set_ylabel(label2, fontsize=labelsz)
        else:
            ax.set_yticklabels([])
    else:
        ax.set_yticklabels([])
        ax.set_xticklabels([])
    if flipY:
        ax.invert_yaxis()
    plotname.set_clim(mincol, maxcol)


def windowParDict(par, key, value, comparison="=="):
    outpar = par.copy()
    indices = None
    if comparison == "==":
        indices = np.where(par[key] == value)[0]
    if comparison == ">":
        indices = np.where(par[key] > value)[0]
    if comparison == "<":
        indices = np.where(par[key] < value)[0]
    if comparison == "<=":
        indices = np.where(par[key] <= value)[0]
    if comparison == ">=":
        indices = np.where(par[key] >= value)[0]
    if comparison == "!=":
        indices = np.where(par[key] != value)[0]

    if indices is not None:
        for keyI in par:
            if isinstance(par[keyI], np.ndarray):
                outpar[keyI] = par[keyI][indices]
    return outpar


def makedateTime(t):
    d = UTCDateTime(t).datetime
    year = int(d.strftime("%Y"))
    mon = int(d.strftime("%m"))
    day = int(d.strftime("%d"))
    hour = int(d.strftime("%H"))
    minute = int(d.strftime("%M"))
    sec = int(d.strftime("%S"))
    msec = int(d.strftime("%f"))
    d2 = datetime.datetime(year, mon, day, hour, minute, sec, msec, pytz.UTC)
    return d2


def plotWellsXY(par, ax):
    for key in par["wellpaths"]:
        if key != "-1":
            ax.plot(
                par["wellpaths"][key][:, 1],
                par["wellpaths"][key][:, 2],
                par["wellcolor"],
                alpha=1,
                zorder=1,
            )


def plotWellsXZ(par, ax):
    for key in par["wellpaths"]:
        if key != "-1":
            ax.plot(
                par["wellpaths"][key][:, 1],
                par["wellpaths"][key][:, 0],
                par["wellcolor"],
                alpha=1,
                zorder=1,
            )


def plotWellsYZ(par, ax):
    for key in par["wellpaths"]:
        if key != "-1":
            ax.plot(
                par["wellpaths"][key][:, 2],
                par["wellpaths"][key][:, 0],
                par["wellcolor"],
                alpha=1,
                zorder=1,
            )


def plotStageTicksXY(par, ax):
    ax.scatter(
        par["stageInfo"]["xloc"],
        par["stageInfo"]["yloc"],
        marker=par["stagesym"],
        c=par["stagecolor"],
        alpha=1,
        zorder=10,
    )


def plotStageTicksXZ(par, ax):
    ax.scatter(
        par["stageInfo"]["yloc"],
        par["stageInfo"]["zloc"],
        marker=par["stagesym"],
        c=par["stagecolor"],
        alpha=1,
        zorder=10,
    )


def plotStageTicksYZ(par, ax):
    ax.scatter(
        par["stageInfo"]["yloc"],
        par["stageInfo"]["zloc"],
        marker=par["stagesym"],
        c=par["stagecolor"],
        alpha=1,
        zorder=10,
    )
