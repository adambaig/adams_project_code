import matplotlib.pyplot as plt
import numpy as np
import sys
from sms_moment_tensor.MT_math import (
    decompose_MT,
    mt_vector_to_matrix,
    mt_matrix_to_vector,
    mt_to_sdr,
)
from .generalPlots import setaxparams, setFig, windowParDict, makedateTime
from .generalPlots import plotWellsXY, plotStageTicksXY
from pathlib import Path
import matplotlib.dates as mdates
from scipy.ndimage import zoom


def plotStrain(
    par,
    conf_thresh=0.9,
    reservoir_thickness=200,
    min_events=1,
    calcGrid=50,
    outputDir="./",
    outname="strain.png",
    title="",
    singleColor=True,
):
    if "\n" not in title:
        title = title + "\n"
    Path(outputDir).mkdir(parents=True, exist_ok=True)
    PI = np.pi
    east_grid = np.arange(par["mX"][0], par["mX"][1], calcGrid)
    north_grid = np.arange(par["mY"][0], par["mY"][1], calcGrid)
    params = windowParDict(par, "confVals", conf_thresh, comparison=">")
    moment_tensors = params["dcMTs"]
    mags = params["mags"]
    xlocs = params["xlocs"]
    ylocs = params["ylocs"]

    fig, ax = plt.subplots(1, 2, sharey=True, figsize=(15, 15))
    grid_points = []
    easts = []
    norths = []
    t_trend_east, t_trend_north, p_trend_east, p_trend_north = [], [], [], []
    for east in east_grid:
        for north in north_grid:
            grid_mts = r_nearest_neighbour_indices(
                {"east": east, "north": north}, xlocs, ylocs, 2 * calcGrid
            )
            volume = 4 * PI * calcGrid * calcGrid * reservoir_thickness
            if len(grid_mts) >= min_events:
                strain = 0.0
                for mt, mag in zip(moment_tensors[grid_mts], mags[grid_mts]):
                    strain = strain + (scaled_mt(mt, mag)) / volume / 2.0
                easts.append(east)
                norths.append(north)
                decomp = decompose_MT(strain)
                t_trend, t_plunge = decomp["t_trend"], decomp["t_plunge"]
                p_trend, p_plunge = decomp["p_trend"], decomp["p_plunge"]
                t_trend_east.append(
                    np.sin(PI * t_trend / 180.0) * np.cos(PI * t_plunge / 180.0)
                )
                t_trend_north.append(
                    np.cos(PI * t_trend / 180.0) * np.cos(PI * t_plunge / 180.0)
                )
                p_trend_east.append(
                    np.sin(PI * p_trend / 180.0) * np.cos(PI * p_plunge / 180.0)
                )
                p_trend_north.append(
                    np.cos(PI * p_trend / 180.0) * np.cos(PI * p_plunge / 180.0)
                )

    plotWellsXY(par, ax[0])
    plotWellsXY(par, ax[1])
    plotStageTicksXY(par, ax[0])
    plotStageTicksXY(par, ax[1])

    c1 = []
    if par["timeticks"]:
        for c in par["c"]:
            t = mdates.date2num(makedateTime(c))
            c1.append(t)
    else:
        for c in par["c"]:
            c1.append(c)

    color1 = "royalblue"
    color2 = "firebrick"
    # if singleColor:
    #     color1 = "royalblue"
    #     color2 = "firebrick"
    # else:
    #     color1 = np.array(c1)
    #     color2 = np.array(c1)
    ax[0].set_aspect("equal")
    ax[0].quiver(
        np.array(easts),
        np.array(norths),
        t_trend_east,
        t_trend_north,
        pivot="mid",
        width=0.005,
        zorder=5,
        headlength=0,
        headaxislength=0,
        headwidth=0,
        scale=1 / 100,
        scale_units="x",
        color=color1,
    )

    ax[1].set_aspect("equal")
    quiv = ax[1].quiver(
        np.array(easts),
        np.array(norths),
        p_trend_east,
        p_trend_north,
        pivot="mid",
        width=0.005,
        zorder=5,
        headlength=0,
        headaxislength=0,
        headwidth=0,
        scale=1 / 100,
        scale_units="x",
        linewidth=0.5,
        color=color2,
    )
    setaxparams(
        ax[0],
        par["bkgroundcol"],
        par["majorticks"],
        par["ticsX"][0],
        par["ticsX"][1],
        par["ticsY"][0],
        par["ticsY"][1],
        par["mX"][0],
        par["mX"][1],
        par["mY"][0],
        par["mY"][1],
    )
    setFig(
        ax[0],
        quiv,
        par["wantLabels"],
        par["colvals"][0],
        par["colvals"][1],
        par["labelsz"],
        label1="UTM easting",
        label2="UTM northing",
        flipY=False,
        wantaxis=[True, True],
    )
    setaxparams(
        ax[1],
        par["bkgroundcol"],
        par["majorticks"],
        par["ticsX"][0],
        par["ticsX"][1],
        par["ticsY"][0],
        par["ticsY"][1],
        par["mX"][0],
        par["mX"][1],
        par["mY"][0],
        par["mY"][1],
    )
    setFig(
        ax[1],
        quiv,
        par["wantLabels"],
        par["colvals"][0],
        par["colvals"][1],
        par["labelsz"],
        label1="UTM easting",
        label2="",
        flipY=False,
        wantaxis=[True, True],
    )
    fig.subplots_adjust(wspace=0.05, hspace=0.0)
    ax[0].set_title("Tensional strain\n" + title, fontsize=par["labelsz"] * 1.1)
    ax[1].set_title("Compressional strain\n" + title, fontsize=par["labelsz"] * 1.1)
    fig.savefig(outputDir + "/" + outname, bbox_inches="tight")
    plt.close(fig)


def plotStrainByWell(
    par,
    conf_thresh=0.9,
    reservoir_thickness=200,
    min_events=1,
    calcGrid=50,
    outputDir="./",
):
    Path(outputDir).mkdir(parents=True, exist_ok=True)
    for ii, well in enumerate(par["wellpaths"]):
        print(well)
        params = windowParDict(par, "wellnames", well, comparison="==")
        params["stageInfo"] = windowParDict(
            params["stageInfo"], "wellname", well, comparison="=="
        )
        if well == "-1":
            well = "Outside pump times"
        outname = "strain-well-" + well.replace("/", ".") + ".png"
        plotStrain(
            params,
            conf_thresh=conf_thresh,
            reservoir_thickness=reservoir_thickness,
            min_events=min_events,
            calcGrid=calcGrid,
            outputDir=outputDir,
            outname=outname,
            title=well,
        )


def plotStrainByStage(
    par,
    conf_thresh=0.9,
    reservoir_thickness=200,
    min_events=1,
    calcGrid=50,
    outputDir="./",
):
    Path(outputDir).mkdir(parents=True, exist_ok=True)
    for ii, stage in enumerate(par["stageInfo"]["stageid"]):
        well = par["stageInfo"]["wellname"][ii]
        wellstage = par["stageInfo"]["wellstage"][ii]
        nstages = len(par["stageInfo"]["stageid"])
        instageInds = np.where(par["stageIds"] == stage)[0]
        if len(instageInds) > 0:
            print(
                "Plotting well "
                + str(well)
                + " stage "
                + str(wellstage)
                + " ("
                + str(ii)
                + "/"
                + str(nstages)
                + ")"
            )
            params = windowParDict(par, "stageIds", stage, comparison="==")
            params["stageInfo"] = windowParDict(
                params["stageInfo"], "stageid", stage, comparison="=="
            )
            title = str(well) + "\nStage " + str(wellstage)
            outname = (
                "well-"
                + str(well).replace("/", ".")
                + "-stage-"
                + str(wellstage)
                + ".png"
            )
            plotStrain(
                params,
                conf_thresh=conf_thresh,
                reservoir_thickness=reservoir_thickness,
                min_events=min_events,
                calcGrid=calcGrid,
                outputDir=outputDir,
                outname=outname,
                title=title,
            )
        else:
            print(
                "No events, skipping well "
                + str(well)
                + " stage "
                + str(wellstage)
                + " ("
                + str(ii)
                + "/"
                + str(nstages)
                + ")"
            )


def plotSRV(
    par,
    Mc=None,
    outputDir="./",
    title="",
    reservoir_thickness=250,
    min_events=10,
    calcGrid=40,
):
    if Mc is None:
        print("Mc must be defined")
        sys.exit()
    if "\n" not in title:
        title = title + "\n"
    Path(outputDir).mkdir(parents=True, exist_ok=True)
    PI = np.pi
    params = windowParDict(par, "mags", Mc, comparison=">")
    moment_tensors = params["dcMTs"]
    mags = params["mags"]
    xlocs = params["xlocs"]
    ylocs = params["ylocs"]
    zlocs = params["zlocs"]

    (
        t_trends,
        p_trends,
        b_trends,
        t_plunges,
        p_plunges,
        b_plunges,
        strikes,
        dips,
        rakes,
    ) = ([], [], [], [], [], [], [], [], [])
    for mt in moment_tensors:
        strike, dip, rake = mt_to_sdr(mt, conjugate=False)
        decomp = decompose_MT(mt)
        p_trends.append(decomp["p_trend"])
        b_trends.append(decomp["b_trend"])
        t_trends.append(decomp["t_trend"])
        p_plunges.append(decomp["p_plunge"])
        b_plunges.append(decomp["b_plunge"])
        t_plunges.append(decomp["t_plunge"])
        strikes.append(strike)
        dips.append(dip)
        rakes.append(rake)

    east_grid = np.arange(par["mX"][0], par["mX"][1], calcGrid)
    north_grid = np.arange(par["mY"][0], par["mY"][1], calcGrid)
    depth_grid = np.arange(par["mZ"][0], par["mZ"][1], calcGrid)

    strain_mag = np.zeros([len(east_grid), len(north_grid), len(depth_grid)])
    easts, norths, depths, strains = (
        [],
        [],
        [],
        [],
    )
    strain_mag = np.zeros([len(east_grid), len(north_grid), len(depth_grid)])
    for i_east, east in enumerate(east_grid):
        for i_north, north in enumerate(north_grid):
            for i_depth, depth in enumerate(depth_grid):
                grid_mts = r3_nearest_neighbours_indices(
                    {"east": east, "north": north, "depth": depth},
                    xlocs,
                    ylocs,
                    zlocs,
                    2 * calcGrid,
                )
                volume = 4 * PI * calcGrid * calcGrid * calcGrid / 3
                if len(grid_mts) >= min_events:
                    strain = 0.0
                    for mt, mag in zip(moment_tensors[grid_mts], mags[grid_mts]):
                        strain = strain + (scaled_mt(mt, mag)) / volume / 2.0 / 1.5e9
                    easts.append(east)
                    norths.append(north)
                    depths.append(depth)
                    strains.append(strain)
                    # if (np.isnan(np.log10(np.linalg.norm(strain)))) or (np.log10(np.linalg.norm(strain)) <= 0):
                    #     strain_mag[i_east, i_north, i_depth] = 0
                    # else:
                    # print(np.linalg.norm(strain))
                    strain_mag[i_east, i_north, i_depth] = np.linalg.norm(strain)
                else:
                    easts.append(east)
                    norths.append(north)
                    depths.append(depth)
                    strains.append(0.0)
                    strain_mag[i_east, i_north, i_depth] = 0

    strains_full = np.squeeze(
        strain_mag[:, :, 13].reshape([(i_east + 1) * (i_north + 1), 1])
    )
    bins = np.log10(np.array(strains_full[np.where(strains_full > 0)[0]]))
    strain_mag = zoom(strain_mag, 4)
    minstrain = np.min(bins)
    maxstrain = np.max(bins) + abs(np.max(bins) * 0.01)
    print(minstrain, maxstrain)
    # exit()
    # minstrain =  # bins[2]  # min(bins)
    # maxstrain =  # bins[-1]  # max(bins)
    levels = np.arange(minstrain, maxstrain, (maxstrain - minstrain) / 7)
    # print(levels)
    # plt.hist(
    #     np.log10(np.array(strains_full[np.where(strains_full > 0)[0]])))  # , bins=np.arange(-10, -6, 0.1)
    # # )
    # plt.show()

    ne4, nn4, nd4 = strain_mag.shape
    l2, l5 = np.log10(2), np.log10(5)
    # levels = [-6, -6 + l2, -6 + l5, -5, -5 + l2, -5 + l5, -4]
    fig, ax = plt.subplots()
    ax.set_aspect("equal")
    ax.set_facecolor("0.95")

    #    strain_mag = strain_mag / np.max(strain_mag[:])
    for ii in range(ne4):
        for jj in range(nn4):
            if strain_mag[ii, jj, 52] < 0.0:
                strain_mag[ii, jj, 52] = 0.0
            print(strain_mag[ii, jj, 52])
            # print(strain_mag[ii, jj, 52])
            # print(np.log10(strain_mag[ii, jj, 52]))
            # if np.isnan(np.log10(strain_mag[ii, jj, 52])):
            #     strain_mag[ii, jj, 52] = 0.
    # print(np.log10(strain_mag[ii, jj, 52]).T)

    east_grid4 = np.linspace(east_grid[0], east_grid[0] + ne4 * calcGrid / 4, ne4)
    north_grid4 = np.linspace(north_grid[0], north_grid[0] + nn4 * calcGrid / 4, nn4)
    depth_grid4 = np.linspace(depth_grid[0], depth_grid[0] + nd4 * calcGrid / 4, nd4)
    contour_plot = ax.contourf(
        east_grid4,
        north_grid4,
        np.log10(strain_mag[:, :, 52]).T,
        zorder=16,
        alpha=0.5,
        vmin=minstrain,
        vmax=maxstrain,
        levels=levels,
        extend="max",
        cmap="inferno_r",
    )

    cbar = plt.colorbar(contour_plot)
    # cbar.set_spacing('proportional')
    plotWellsXY(par, ax)
    plotStageTicksXY(par, ax)
    # setaxparams(ax, par['bkgroundcol'], par['majorticks'], par['ticsX'][0], par['ticsX'][1],
    #             par['ticsY'], par['ticsY'][1], par['mX'][0], par['mX'][1], par['mY'][0], par['mY'][1])
    setFig(
        ax,
        contour_plot,
        par["wantLabels"],
        par["colvals"][0],
        par["colvals"][1],
        par["labelsz"],
        label1="UTM easting",
        label2="UTM northing",
        flipY=False,
        wantaxis=[True, True],
    )
    plt.show()


def r_nearest_neighbour_indices(grid_point, xlocs, ylocs, radius):
    radius_sq = radius * radius
    ref_east, ref_north = grid_point["east"], grid_point["north"]
    inds = np.where(((xlocs - ref_east) ** 2 + (ylocs - ref_north) ** 2) < radius_sq)[0]
    return inds


def r3_nearest_neighbours_indices(grid_point, xlocs, ylocs, zlocs, radius):
    radius_sq = radius * radius
    ref_east, ref_north, ref_depth = (
        grid_point["east"],
        grid_point["north"],
        grid_point["depth"],
    )
    inds = np.where(
        ((xlocs - ref_east) ** 2 + (ylocs - ref_north) ** 2 + (zlocs - ref_depth) ** 2)
        < radius_sq
    )[0]
    return inds


def scaled_mt(mt, mag):
    moment = 10 ** (1.5 * mag + 9)
    mt_mat = mt_vector_to_matrix(mt)
    scaled_mt = mt_mat * moment / np.linalg.norm(mt_mat)
    return mt_matrix_to_vector(scaled_mt)
