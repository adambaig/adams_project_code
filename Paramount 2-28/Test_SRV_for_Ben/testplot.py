from Functions.generalPlots import initPFIPlots, plotStations, plotFM

# from locationPlots import movieAllEvents, plotEvents, plotEventsByWell, plotEventsByStage
# from histogramPlots import plotAllEventHistogram, plotWellEventHistogram, plotHistogramWithPumping, plotUncertainties
# from mtPlots import makeLune, makeMTConfplot, strainQuiverPlot, plotPBTaxes, plotRakes, plotBeachBalls
# from plot3D import well3D
# from dataPlots import plotNoise, plotSuperstationTraces
from Functions.analysisPlots import (
    plotStrain,
    plotStrainByWell,
    plotStrainByStage,
    plotSRV,
)
from obspy import UTCDateTime
import numpy as np
import os
import matplotlib.pyplot as plt
import pyproj

wellDir = r"C:\Users\adambaig\Project\Paramount 2-28\Test_SRV_for_Ben\wells"
wellFiles = [os.path.join(wellDir, f) for f in os.listdir(wellDir)]
wellinfo = {}
wellinfo["-1"] = []
for wellFile in wellFiles:
    wellName = (os.path.basename(wellFile).split("_")[0].replace("-", "/", 1)) + "/00"
    wellData = np.genfromtxt(wellFile, delimiter=",", dtype=float)
    wellinfo[wellName] = wellData  # in Z,X,Y order
    meanwellX = np.mean(wellData[:, 1])
    meanwellY = np.mean(wellData[:, 2])
    meanwellZ = np.max(wellData[:, 0])


stageFile = r"C:\Users\adambaig\Project\Paramount 2-28\Test_SRV_for_Ben\stage_info_epsg26911_wStageId.csv"
stages = np.genfromtxt(stageFile, delimiter=",", dtype=str)
stageinfo = {}
stageX = np.array(stages[:, 2]).astype(float)
stageY = np.array(stages[:, 3]).astype(float)
stageZ = np.array(stages[:, 4]).astype(float)
stageWell = np.array(stages[:, 0]).astype(str)
wellStage = np.array(stages[:, 1]).astype(int)
stageid = np.array(stages[:, 7]).astype(int)
stageinfo["stageid"] = stageid
stageinfo["xloc"] = stageX
stageinfo["yloc"] = stageY
stageinfo["zloc"] = stageZ
stageinfo["wellname"] = stageWell
stageinfo["wellstage"] = wellStage


pumpdata = r"C:\Users\adambaig\Project\Paramount 2-28\Test_SRV_for_Ben\pumpdata\AllWells_ConcatPump.csv"
pump = np.genfromtxt(pumpdata, delimiter=",", dtype=str)
pumpInfo = {}
times = []
p = []
for i, t in enumerate(np.array(pump[:, 0])):
    times.append(UTCDateTime(t).timestamp)

times = np.array(times)
sortInd = np.argsort(times)
for i in range(len(sortInd)):
    p.append(i)

pumpInfo["Date"] = times[sortInd]
pumpInfo["Well"] = np.array(pump[:, 1])[sortInd].astype(str)
pumpInfo["Pressure"] = np.array(pump[:, 6])[sortInd].astype(float)
pumpInfo["Proppant"] = np.array(pump[:, 18])[sortInd].astype(float)
pumpInfo["Slurry"] = np.array(pump[:, 12])[sortInd].astype(float)


#################################################################################################
catalog = r"C:\Users\adambaig\Project\Paramount 2-28\Test_SRV_for_Ben\relocCatalog_stalta_wStageId.csv"
colorby = "Time"
wantLabels = True
cmap = "jet"
stageCol = 26
doAll = False
depthexag = 2
XYonly = False
doAll = False
useLocal = False
plotparams = initPFIPlots(
    catalog,
    wellinfo,
    stageInfo=stageinfo,
    depthexag=depthexag,
    colorby=colorby,
    wantLabels=wantLabels,
    cmap=cmap,
    stageCol=stageCol,
)

startTime = UTCDateTime("2019-04-22").timestamp
endTime = UTCDateTime("2019-04-23").timestamp

################################################################################################################
# Plot all Events
# plotEvents(plotparams, outputDir='./testnew', XYonly=XYonly,
#            dateTickFormat='%Y/%m/%d\n%H:%M:%S', outname='all.png', title='All Events')
#
# # Plot all Events for each well
# wellStage = True  # set to color by individual well stage rather than stage id
# plotEventsByWell(plotparams, outputDir='./testnew', XYonly=XYonly,
#                  dateTickFormat='%Y/%m/%d\n%H:%M:%S', wellStage=wellStage)

# Plot all events for each stage
# stageTime = True
# plotEventsByStage(plotparams, outputDir='./testnew', XYonly=XYonly,
#                   dateTickFormat = '%Y/%m/%d\n%H:%M:%S', wellStage = wellStage, stageTime = stageTime)

# movieAllEvents(plotparams, outputDir='./testMovie', XYonly=XYonly, timeStep=3600, dateTickFormat='%Y/%m/%d\n %H:%M:00',
#                startTime=UTCDateTime('2019-04-22').timestamp, fps=5., dimEarly=False)

# #########################################################################
# plotAllEventHistogram(plotparams, outputDir='./HistogramTest')
# plotWellEventHistogram(plotparams, outputDir='./HistogramTest')
# plotHistogramWithPumping(plotparams, pumpInfo, outputDir='./HistogramTest', startTime=startTime, endTime=endTime)
# ###########################################################################################################
# stationFile = '/home/benjaminwitten/Testing/pfiplot/paramount_station_locations_utm11N.csv'
# stats = np.genfromtxt(stationFile, delimiter=',', dtype=str)
# stat_locs = stats[:, 0]
# stations = {}
# stat = []
# locs = []
# for stat_loc in stat_locs:
#     stat.append(stat_loc.split('_')[0])
#     locs.append(stat_loc.split('_')[1])
#     # print(stat, loc)
# stations['stations'] = np.array(stat)
# stations['locations'] = np.array(locs)
# stations['X'] = np.array(stats[:, 1]).astype(float)
# stations['Y'] = np.array(stats[:, 2]).astype(float)
# stations['Z'] = np.array(stats[:, 3]).astype(float)
#
# plotStations(stations, wellpaths=wellinfo, plotSuperStation=10, outputDir='./Stations')
# #######################################################################################################
#
# plotFM(plotparams, outputDir='./bValTest')


############################################################################################################
# makeLune(plotparams, outputDir='./luneTest')
# noiseCat = '/home/benjaminwitten/Desktop/paramount_data/relocNoiseMTs_stalta.csv'
# makeMTConfplot(plotparams, noiseCat, outputDir='testNoiseConf')
# strainQuiverPlot(plotparams, outputDir='pvect', axis='b')
# plotPBTaxes(plotparams, confThresh=0.72, outputDir='./pbt')
# plotRakes(plotparams, outputDir='./rakes', confThresh=0.72, doAll=False)
# plotBeachBalls(plotparams, minMag=0.00001, outputDir='beachballs')


############################################
# horizonfiles = ['/home/benjaminwitten/Testing/pfiplot/basement_CANA.csv',
#                 '/home/benjaminwitten/Testing/pfiplot/basement_CANA.csv']
# horizonnames = ['horizon1', 'horizon2']
# horizoncolors = ['r', 'b']
#
# Proj1 = pyproj.Proj(init='epsg:32614')
#
# horizons = {}
# for ii, f in enumerate(horizonfiles):
#     xdata = []
#     ydata = []
#     hdata = np.genfromtxt(f, delimiter=',', dtype=str)
#     lons = np.array(hdata[:, 0]).astype(float)
#     lats = np.array(hdata[:, 1]).astype(float)
#     depth = -1.*np.array(hdata[:, 2]).astype(float)
#     for lon, lat in zip(lons, lats):
#         xx, yy = Proj1(lon, lat, inverse=False)
#         xdata.append(xx)
#         ydata.append(yy)
#     xdata = (np.array(xdata)-np.median(xdata))/200+meanwellX+1000*ii
#     ydata = (np.array(ydata)-np.median(ydata))/200+meanwellY+1000*ii
#     zdata = (depth-np.median(depth))/5+meanwellZ-300*ii
#     dat = np.array([xdata, ydata, zdata])
#     horizons[horizonnames[ii]] = {}
#     horizons[horizonnames[ii]]['data'] = dat
#     horizons[horizonnames[ii]]['color'] = horizoncolors[ii]
#
# well3D(plotparams, horizons=horizons, trueAspect=True)


##############################################################################
# plotUncertainties(plotparams, outputDir='uncer')

##############################################################################
# rawDir = '/home/benjaminwitten/Desktop/paramount_data/rawData/'
# sembDir = '/home/benjaminwitten/Desktop/paramount_data/data/'
# # plotNoise(seedDir=rawDir, outputDir='noisePlots', tag='all')
# startTime = UTCDateTime('2019-04-22 00:02:35')
# endTime = UTCDateTime('2019-04-22 00:02:38')
# plotSuperstationTraces(rawDir, sembDir, startTime, endTime, superstation='All', outputDir='supertrace')

##############################################################################
# plotStrain(plotparams, outputDir='strain', singleColor=False)
# plotStrainByWell(plotparams, outputDir='strain')
# plotStrainByStage(plotparams, outputDir='strain')

##############################################################################
plotSRV(plotparams, Mc=0.000005, outputDir="srv")
