import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import sys

sys.path.append("..")
from read_inputs import read_events, read_pick_snr
from bvalue_plot import plotFMD_from_dict

from scipy.odr import Model, Data, ODR


def sqa(x):
    return np.squeeze(np.array(x))


def linear_fit(B, x):
    return B[0] * x + B[1]


def quadratic_fit(B, x):
    return B[0] * x * x * x * x + B[1] * x + B[2]


def cubic_fit(B, x):
    return B[0] * x * x * x + B[1] * x * x + B[2] * x + B[3]


def quartic_fit(B, x):
    return B[0] * x * x * x * x + B[1] * x * x * x + B[2] * x * x + B[3] * x + B[4]


def linear_exp_fit(B, x):
    return B[0] * x + B[1] + B[2] * np.exp(B[3] * (x - B[4]))


dr = "2019-06-16T15-53//"
m_range_low = -1
m_range_high = 1.5
model = "linear"
f = open(dr + "synthetic_catalog_relocated.csv")
lines = f.readlines()
f.close()

nl = len(lines)
vs_source = 4287.03325037125 / np.sqrt(3)
mags, amps, stress_drop = np.zeros(nl), np.zeros(nl), np.zeros(nl)

snr = read_pick_snr()


for ii, line in enumerate(lines):
    lspl = line.split(",")
    mags[ii] = float(lspl[5])
    amps[ii] = float(lspl[13])
    stress_drop[ii] = float(lspl[6])

f = open("calibration_ppv_mags.csv")
head = f.readline()
ppv_mags = {}
for line in f.readlines():
    lspl = line.split(",")
    ppv_mags[lspl[0]] = {}
    ppv_mags[lspl[0]]["M"] = float(lspl[1])
    ppv_mags[lspl[0]]["stack amp"] = float(lspl[2])
f.close()

fig, ax = plt.subplots()
scatter_sd = ax.scatter(
    mags, amps, c=np.log10(stress_drop), edgecolor="k", cmap="magma", vmin=4, vmax=7
)

ax.set_yscale("log")
ax.set_ylim([1e-5, 1e4])
ax.set_xlabel("moment magntiude")
ax.set_ylabel("amplitude")
cb = fig.colorbar(scatter_sd)
cb.set_label("stress drop (Pa)")
cb.set_ticks([4, 5, 6, 7])
cb.set_ticklabels(["10$^4$", "10$^5$", "10$^6$", "10$^7$"])
moment = 10 ** (1.5 * mags + 9.05)
f2, a2 = plt.subplots()
corner_freq_p = 0.32 * vs_source * np.cbrt(16.0 * stress_drop / (7.0 * moment))

scatter_cf = a2.scatter(
    mags, amps, c=corner_freq_p, edgecolor="k", cmap="viridis", vmin=20, vmax=60
)
events = read_events()

amps_calib = np.array(
    [
        v["amp"]
        for k, v in events.items()
        if ((v["spec_fit_Mw"] > -8) and ("stage" in v))
    ]
)
mags_calib = np.array(
    [v["Mw"] for k, v in events.items() if ((v["spec_fit_Mw"] > -8) and ("stage" in v))]
)
amps_uncalib = np.array(
    [
        v["amp"]
        for k, v in events.items()
        if ((v["spec_fit_Mw"] < -8) and ("stage" in v))
    ]
)
ax.semilogy(mags_calib, amps_calib, "s", color="seagreen", markeredgecolor="k")
ax.semilogy(
    [v["M"] for k, v in ppv_mags.items()],
    [v["stack amp"] for k, v in ppv_mags.items()],
    ".",
    color="seagreen",
    markeredgecolor="k",
)
a2.semilogy(mags_calib, amps_calib, "s", color="firebrick", markeredgecolor="k")

all_amps = np.array([v["amp"] for k, v in events.items()])

i_interval = np.where(
    (mags < m_range_high) & (mags > m_range_low) & (stress_drop < 1.0e5)
)[0]

# A = np.matrix(np.vstack([mags[i_interval], np.ones(len(i_interval))])).T
# slope, y_int = sqa(
#     np.linalg.inv(A.T * A) * A.T * np.matrix(np.log10(amps[i_interval])).T
# )
# mags_extrapolated = (
#     np.log10(
#         np.array(
#             [v["amp"] for k, v in events.items() if ((v["Mw"] < -8) and ("stage" in v))]
#         )
#     )
#     - y_int
# ) / slope


# quadratic_model = Model(quadratic_fit)
# odr_fit_data = Data(np.log10(amps[i_interval]), mags[i_interval])
# quadratic_odr = ODR(odr_fit_data, quadratic_model, beta0=[0, 0, 0])
# quadratic_output = quadratic_odr.run()
# quad_amp_calib, linear_amp_calib, yint_amp_calib = quadratic_output.beta

linexp_model = Model(linear_exp_fit)
odr_fit_data = Data(np.log10(amps[i_interval]), mags[i_interval])
linexp_odr = ODR(odr_fit_data, linexp_model, beta0=[0, 0, 0, 0, 0])
linexp_output = linexp_odr.run()
le_slope, le_yint, le_etemp, le_exp, log_offset = linexp_output.beta


linear_model = Model(linear_fit)
odr_fit_data = Data(np.log10(amps[i_interval]), mags[i_interval])
linear_odr = ODR(odr_fit_data, linear_model, beta0=[0, 0])
linear_output = linear_odr.run()
linear_amp_calib, yint_amp_calib = linear_output.beta


def extrapolate_mag(amp, fit="quad"):
    log_amp = np.log10(amp)
    if fit == "quad":

        return (
            quad_amp_calib * log_amp * log_amp
            + linear_amp_calib * log_amp
            + yint_amp_calib
        )
    if fit == "cubic":
        return (
            cubic_amp_calib * log_amp * log_amp * log_amp
            + quad_amp_calib * log_amp * log_amp
            + linear_amp_calib * log_amp
            + yint_amp_calib
        )
    if fit == "quartic":
        return (
            quart_amp_calib * log_amp * log_amp * log_amp * log_amp
            + cubic_amp_calib * log_amp * log_amp * log_amp
            + quad_amp_calib * log_amp * log_amp
            + linear_amp_calib * log_amp
            + yint_amp_calib
        )
    if fit == "linexp":
        return (
            le_slope * log_amp
            + le_yint
            + le_etemp * np.exp(le_exp * (log_amp - log_offset))
        )
    if fit == "linear":
        return linear_amp_calib * log_amp + yint_amp_calib


test_amps = np.array(10 ** np.arange(-5, 5.1, 0.1))

ax.semilogy(extrapolate_mag(test_amps, fit=model), test_amps, "0.5", lw=2)
a2.semilogy(extrapolate_mag(test_amps, fit=model), test_amps, "0.5", lw=2)


# ax.semilogy(mags_extrapolated, amps_uncalib, ".", color="seagreen")
# a2.semilogy(mags_extrapolated, amps_uncalib, ".", color="firebrick")
#
# for event in events:
#     if events[event]["Mw"] > 8:
#         events[event]["Mw_all"] = events[event]["Mw"]
#     else:
#         events[event]["Mw_all"] = (
#             np.log10(np.array(events[event]["amp"])) - y_int
#         ) / slope

f3, a3 = plt.subplots()
a3.set_aspect("equal")
mag_match = extrapolate_mag(
    np.array(
        [
            v["amp"]
            for k, v in events.items()
            if ((v["spec_fit_Mw"] > -8) and ("stage" in v))
        ]
    ),
    fit="linear",
)

match_events = {
    k: v for k, v in events.items() if ((v["spec_fit_Mw"] > -8) and ("stage" in v))
}
# linear_model = Model(linear_fit)
# odr_fit_data = Data(mags_calib, mag_match)
# linear_odr = ODR(odr_fit_data, linear_model, beta0=[0, 0])
# linear_output = linear_odr.run()
# slope_odr, yint_odr = linear_output.beta

for event in events:
    if "stage" in events[event]:
        if events[event]["spec_fit_Mw"] < -8:
            events[event]["mag_recalib"] = extrapolate_mag(
                events[event]["amp"], fit=model
            )

            if event in snr:
                events[event]["median_snr"] = np.median(
                    [v for k, v in snr[event].items()]
                )
            else:
                events[event]["median_snr"] = 0
        else:
            events[event]["mag_recalib"] = events[event]["spec_fit_Mw"]


for event in events:
    if event in snr:
        events[event]["median_snr"] = np.median([v for k, v in snr[event].items()])
    else:
        events[event]["median_snr"] = 0

median_snr = np.array([v["median_snr"] for k, v in events.items()])
a3.plot(mag_match, mags_calib, "o", markeredgecolor="k", color="mediumorchid")
a3.plot([-0.1, 0.7], [-0.1, 0.7], "0.5")

a3.set_xlabel("magnitudes from synthetic calibration")
a3.set_ylabel("spectral fit amplitudes")


a2.set_yscale("log")
a2.set_ylim([1e-5, 1e4])
a2.set_xlabel("moment magntiude")
a2.set_ylabel("amplitude")
cb2 = f2.colorbar(scatter_cf, extend="both")
cb2.set_label("corner frequency (Hz)")
plotFMD_from_dict(
    {k: v for k, v in events.items() if "stage" in v},
    key="mag_recalib",
    manual_fit=True,
)
stage_events = {k: v for k, v in events.items() if "stage" in v}

mag_bins = np.arange(-1.5, 0.5, 0.1)
spec_fit_mags = np.array([v["Mw"] for k, v in events.items() if v["spec_fit_Mw"] > -8])
calib_mags = np.array(
    [v["mag_recalib"] for k, v in stage_events.items() if v["spec_fit_Mw"] < -8]
)

n_spec_fit_mags, bins, p = plt.hist(spec_fit_mags, bins=mag_bins, visible=False)
n_calib_mags, bins, p = plt.hist(calib_mags, bins=mag_bins, visible=False)


f6, a6 = plt.subplots()
p_spec_fit = a6.bar(
    mag_bins[:-1],
    n_spec_fit_mags,
    width=0.08,
    facecolor="seagreen",
    edgecolor="k",
    align="edge",
)
p_calib = a6.bar(
    mag_bins[:-1],
    n_calib_mags,
    bottom=n_spec_fit_mags,
    width=0.08,
    facecolor="mediumorchid",
    edgecolor="k",
    align="edge",
)
a6.set_yscale("log")
a6.set_ylim([0.5, 1000])
a6.legend([p_spec_fit, p_calib], ["Spectral Fit", "Calibrated"])
a6.set_ylabel("count")
a6.set_xlabel("moment magnitude")
a6.set_xticks(np.arange(-1.5, 0.6, 0.5))
a6.set_xticks(mag_bins, minor=True)
plt.show()
