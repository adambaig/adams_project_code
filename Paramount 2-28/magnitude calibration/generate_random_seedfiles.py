import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import glob
import os
from obspy import Stream, Trace, UTCDateTime
from obspy.core.trace import Stats

from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.raytrace import isotropic_ray_trace
from randomize_seismicity.moment_tensor import dc

import sys

sys.path.append("..")
from read_inputs import read_stations, read_velocity_model

plt.close("all")

dt = 0.004  # sample rate
base_noise = 1.0e-8
n_nodes = 7
noise_level = base_noise / np.sqrt(n_nodes)


velocity_model = read_velocity_model(mode="no_lvl")
stations = read_stations()


def sqa(x):
    return np.squeeze(np.array(x))


def createTraceStats(network, n1, o1, d1, component):
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


Q = {"P": 500.0, "S": 100.0}


nevents = 1000
outdir = str(UTCDateTime.now()).replace(":", "-")[:-11]
os.mkdir(outdir)
g = open(outdir + "//run_parameters.txt", "w")
g.write("station file: 228_stations_epsg26911.csv\n")
g.write("noise level (m/s): " + str(base_noise) + "\n")
g.write("nodes per superstation: " + str(n_nodes) + "\n")
g.close()

g = open(outdir + "//synthetic_catalog.csv", "w")
g.write("date,time,x,y,z,magnitude,stress drop,m11,m22,m33,m12,m13,m23\n")
for ievent in range(nevents):
    source = {
        "x": 504021.2862,
        "y": 6028175.741,
        "z": 2350.01,
        "moment_magnitude": -1.5 + np.random.rand() * 3.5,
        "stress_drop": 10 ** (np.random.rand() * 3 + 4),  # static stress drop
        "moment_tensor": dc(),
    }
    m11, m12, m13, d1, m22, m23, d2, d3, m33 = sqa(source["moment_tensor"].reshape(9))
    mt_6 = [m11, m22, m33, m12, m13, m23]
    source["moment_tensor"] = source["moment_tensor"] / np.linalg.norm(
        source["moment_tensor"]
    )
    top, bottom = -1000, 3500

    time_series = np.arange(0, 4.00, dt)
    waveforms = []
    max_waveform = 0
    st = Stream()
    for station in stations:
        p_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "P")
        s_raypath = isotropic_ray_trace(source, stations[station], velocity_model, "S")
        waveforms.append(
            get_response(
                p_raypath,
                s_raypath,
                source,
                stations[station],
                Q,
                time_series,
                noise_level,
            )
        )
        mwave = max(
            [
                max(abs(waveforms[-1]["n"])),
                max(abs(waveforms[-1]["e"])),
                max(abs(waveforms[-1]["d"])),
            ]
        )
        if max_waveform < mwave:
            max_waveform = mwave

    for ii, station in enumerate(stations):
        seedtime = UTCDateTime.now()
        channel_obj = {
            "component": "Z",
            "channel": "CPZ",
            "station": station,
            "network": "PM",
            "location": "00",
        }
        xStats = createTraceStats("CV", len(time_series), seedtime, dt, channel_obj)
        tr = Trace(
            data=waveforms[ii]["d"] * 59000.0, header=xStats
        )  # 59000 is the sensitivity in mV/m/s
        st += tr
    outfile = (outdir + "//synthetic" + str(seedtime) + ".mseed").replace(":", "-")

    st.write(outfile)
    dat, tim = str(seedtime).split("T")
    g.write(dat + "," + tim[:-1] + ",")
    g.write("%.1f,%.1f,%.1f," % (source["x"], source["y"], source["z"]))
    g.write("%.3f,%.4e," % (source["moment_magnitude"], source["stress_drop"]))
    g.write("%.3f,%.3f,%.3f,%.3f,%.3f,%.3f\n" % (m11, m22, m33, m12, m13, m23))
    print(ievent)

g.close()
