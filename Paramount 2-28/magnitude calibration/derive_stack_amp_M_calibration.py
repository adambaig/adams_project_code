import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import sys

sys.path.append("..")
from read_inputs import read_events, read_pick_snr
from bvalue_plot import plotFMD_from_dict

from scipy.odr import Model, Data, ODR

cutoff = 0.2


def sqa(x):
    return np.squeeze(np.array(x))


def linear_fit(B, x):
    return B[0] * x + B[1]


def quadratic_fit(B, x):
    return B[0] * x * x * x * x + B[1] * x + B[2]


def linear_exp_fit(B, x):
    return B[0] * x + B[1] + B[2] * np.exp(B[3] * (x - B[4]))


dr = "2019-06-16T15-53//"
m_range_low = -1.5
m_range_high = 0.8
snr_cut = 2.5
model = "linear"
f = open(dr + "synthetic_catalog_relocated.csv")
lines = f.readlines()
f.close()

nl = len(lines)
vs_source = 4287.03325037125 / np.sqrt(3)
mags, amps, stress_drop = np.zeros(nl), np.zeros(nl), np.zeros(nl)
events = read_events()
snr = read_pick_snr()

for event in events:
    if event in snr:
        events[event]["median_snr"] = np.median([v for k, v in snr[event].items()])
    else:
        events[event]["median_snr"] = 0

median_snr = np.array([v["median_snr"] for k, v in events.items()])

for ii, line in enumerate(lines):
    lspl = line.split(",")
    mags[ii] = float(lspl[5])
    amps[ii] = float(lspl[13])
    stress_drop[ii] = float(lspl[6])

f = open("calibration_ppv_mags.csv")
head = f.readline()
ppv_mags = {}
for line in f.readlines():
    lspl = line.split(",")
    ppv_mags[lspl[0]] = {}
    ppv_mags[lspl[0]]["M"] = float(lspl[1])
    ppv_mags[lspl[0]]["stack amp"] = float(lspl[2])
    if lspl[0] in snr:
        ppv_mags[lspl[0]]["median snr"] = np.median(
            [v for k, v in snr[lspl[0]].items()]
        )
    else:
        ppv_mags[lspl[0]]["median snr"] = -1

f.close()

fig, ax = plt.subplots()


events = read_events()

amps_calib = np.array(
    [
        v["amp"]
        for k, v in events.items()
        if ((v["spec_fit_Mw"] > -8) and ("stage" in v))
    ]
)
mags_calib = np.array(
    [
        v["spec_fit_Mw"]
        for k, v in events.items()
        if ((v["spec_fit_Mw"] > -8) and ("stage" in v))
    ]
)
amps_uncalib = np.array(
    [
        v["amp"]
        for k, v in events.items()
        if ((v["spec_fit_Mw"] < -8) and ("stage" in v))
    ]
)
p1 = ax.semilogy(
    mags_calib, amps_calib, "s", color="green", markeredgecolor="k", zorder=-12
)
p2 = ax.scatter(
    [v["M"] for k, v in ppv_mags.items() if v["median snr"] > snr_cut],
    [v["stack amp"] for k, v in ppv_mags.items() if v["median snr"] > snr_cut],
    c=np.log(
        np.array(
            [v["median snr"] for k, v in ppv_mags.items() if v["median snr"] > snr_cut]
        )
    ),
    marker=".",
    zorder=1,
    cmap="plasma_r"
    #    edgecolor="k",
)
cb = fig.colorbar(p2)
cb.set_label("log$_{10}$(median SNR)")
# ax.legend([p1[0], p2[0]], ["spectral fit", "PPV calibration"])
ax.set_xlabel("calibrated PPV moment magntiude")
ax.set_ylabel("stack amplitude")


all_amps = np.array([v["amp"] for k, v in events.items()])

i_interval = np.where(
    (mags < m_range_high) & (mags > m_range_low) & (stress_drop < 3.0e5)
)[0]


linear_model = Model(linear_fit)
odr_fit_data = Data(
    [
        np.log10(v["stack amp"])
        for k, v in ppv_mags.items()
        if v["median snr"] > snr_cut
    ],
    [v["M"] for k, v in ppv_mags.items() if v["median snr"] > snr_cut],
)
linear_odr = ODR(odr_fit_data, linear_model, beta0=[0, 0])
linear_output = linear_odr.run()
# linear_amp_calib, yint_amp_calib = linear_output.beta

linear_amp_calib = (0.4 + 0.933) / (np.log10(3.21) - np.log10(0.016))
yint_amp_calib = 0.1


def extrapolate_mag(amp, fit="linear"):
    log_amp = np.log10(amp)
    if fit == "quad":
        return (
            quad_amp_calib * log_amp * log_amp
            + linear_amp_calib * log_amp
            + yint_amp_calib
        )
    if fit == "linexp":
        return (
            le_slope * log_amp
            + le_yint
            + le_etemp * np.exp(le_exp * (log_amp - log_offset))
        )
    if fit == "linear":
        return linear_amp_calib * log_amp + yint_amp_calib


match_events = {
    k: v for k, v in events.items() if ((v["spec_fit_Mw"] > -8) and ("stage" in v))
}

for event in events:
    if "stage" in events[event]:
        if events[event]["Mw"] < -8:
            events[event]["mag_recalib"] = extrapolate_mag(
                events[event]["amp"], fit=model
            )

            if event in snr:
                events[event]["median_snr"] = np.median(
                    [v for k, v in snr[event].items()]
                )
            else:
                events[event]["median_snr"] = 0
        else:
            events[event]["mag_recalib"] = events[event]["spec_fit_Mw"]


stage_events = {k: v for k, v in events.items() if "stage" in v}

mag_bins = np.arange(-2, 0.7, 0.1)
spec_fit_mags = np.array([v["Mw"] for k, v in events.items() if v["spec_fit_Mw"] > -8])
calib_mags = np.array(
    [v["mag_recalib"] for k, v in stage_events.items() if v["spec_fit_Mw"] < -8]
)

test_amps = np.array(10 ** np.arange(-4, 2.1, 0.1))
n_spec_fit_mags, bins, p = plt.hist(spec_fit_mags, bins=mag_bins, visible=False)
n_calib_mags, bins, p = plt.hist(calib_mags, bins=mag_bins, visible=False)
ax.semilogy(extrapolate_mag(test_amps, fit=model), test_amps, "0.5", lw=2)
plotFMD_from_dict(
    {k: v for k, v in events.items() if "stage" in v},
    key="mag_recalib",
    manual_fit=False,
)
stage_events = {k: v for k, v in events.items() if "stage" in v}


n_spec_fit_mags, bins, p = plt.hist(spec_fit_mags, bins=mag_bins, visible=False)
n_calib_mags, bins, p = plt.hist(calib_mags, bins=mag_bins, visible=False)


f6, a6 = plt.subplots()
p_spec_fit = a6.bar(
    mag_bins[:-1],
    n_spec_fit_mags,
    width=0.08,
    facecolor="seagreen",
    edgecolor="k",
    align="edge",
)
p_calib = a6.bar(
    mag_bins[:-1],
    n_calib_mags,
    bottom=n_spec_fit_mags,
    width=0.08,
    facecolor="mediumorchid",
    edgecolor="k",
    align="edge",
)

n_tot = []
n_cumulative = 0
for mb in mag_bins:
    n_tot.append(
        len(np.where(spec_fit_mags > mb)[0]) + len(np.where(calib_mags > mb)[0])
    )
a6.plot(mag_bins, n_tot, "o", color="firebrick", markeredgecolor="k")
a6.plot([mag_bins[11], mag_bins[20]], [n_tot[11], n_tot[20]], "k", lw=2)

a6.set_yscale("log")
a6.set_ylim([0.5, 5000])
a6.legend([p_spec_fit, p_calib], ["Spectral Fit", "Calibrated"])
a6.set_ylabel("count")
a6.set_xlabel("moment magnitude")
a6.set_xticks(np.arange(-1.5, 0.6, 0.5))
a6.set_xticks(mag_bins[::2], minor=False)

plt.show()
f3, a3 = plt.subplots()
a3.set_aspect("equal")
mag_match = extrapolate_mag(
    np.array(
        [
            v["amp"]
            for k, v in events.items()
            if ((v["spec_fit_Mw"] > -8) and ("stage" in v))
        ]
    ),
    fit="linear",
)

match_events = {
    k: v for k, v in events.items() if ((v["spec_fit_Mw"] > -8) and ("stage" in v))
}
# linear_model = Model(linear_fit)
# odr_fit_data = Data(mags_calib, mag_match)
# linear_odr = ODR(odr_fit_data, linear_model, beta0=[0, 0])
# linear_output = linear_odr.run()
# slope_odr, yint_odr = linear_output.beta

for event in events:
    if "stage" in events[event]:
        if events[event]["spec_fit_Mw"] < -8:
            events[event]["mag_recalib"] = extrapolate_mag(
                events[event]["amp"], fit=model
            )

            if event in snr:
                events[event]["median_snr"] = np.median(
                    [v for k, v in snr[event].items()]
                )
            else:
                events[event]["median_snr"] = 0
        else:
            events[event]["mag_recalib"] = events[event]["spec_fit_Mw"]


a3.plot(mag_match, mags_calib, "o", markeredgecolor="k", color="mediumorchid")
a3.plot([-0.1, 0.7], [-0.1, 0.7], "0.5")

a3.set_xlabel("magnitudes from synthetic calibration")
a3.set_ylabel("spectral fit amplitudes")

plt.show()
