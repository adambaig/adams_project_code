import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt

import sys

sys.path.append("..")

from read_inputs import read_pgv


f = open(
    "O:\\O&G\\Projects\\P00016 (Paramount 2-28 PFI)\\Magnitude\\4_FAS_Analysis\\Tables\\FASDatTableV.csv"
)
head = f.readline()
lines = f.readlines()
f.close()

pgv_mark = {}
for line in lines:
    lspl = line.split(",")
    event = str(int(float(lspl[0])))
    if not (event in pgv_mark):
        pgv_mark[event] = {}
    station = lspl[7].split(".VC")[0]
    pgv_mark[event][station] = float(lspl[20])

pgv = read_pgv()

pgv_x, pgv_y = [], []
event_not_found = []
for event in pgv_mark:
    for station in pgv_mark[event]:
        try:
            pgv_y.append(pgv[event][station])
            pgv_x.append(pgv_mark[event][station])

        except:
            print(event + " " + station + " not found")
            event_not_found.append(event)

unique_not_found = np.unique(event_not_found)

len(pgv_x)
fig, ax = plt.subplots()
ax.loglog(pgv_x, np.array(pgv_y) * 100.0, ".")
x1, x2 = ax.get_xlim()
y1, y2 = ax.get_ylim()
a1 = min(x1, y1)
a2 = max(x2, y2)
ax.loglog([a1, a2], [a1, a2])
ax.set_xlabel("From Mark (in m/s)")
ax.set_ylabel("From Aaron")
plt.show()
