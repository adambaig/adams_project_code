import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt

import sys

sys.path.append("..")
from read_inputs import read_events, read_pgv, read_stations, read_pick_snr

events = read_events()
pgv = read_pgv()
snr = read_pick_snr()
stations = read_stations()

Mw_events = {k: v for k, v in events.items() if v["spec_fit_Mw"] > -8}
fit_type = "simple"
fit_type2 = "geom_and_anel"


def sqa(x):
    return np.squeeze(np.array(x))


def fit_data(ppvs, distances, mags, fit_type):

    ppvs = sqa(ppvs)
    distances = sqa(distances)
    mags = sqa(mags)
    n_rows = len(ppvs)
    if fit_type == "simple":
        # log(ppv + distance) = A + B * Mw
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags])).T
        rhs = np.matrix(np.log10(ppvs) + np.log10(distances))
    elif fit_type == "geom_only":
        # log(ppv) = A + B * Mw + C * log(distance)
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags, np.log10(distances)])).T
        rhs = np.matrix(np.log10(ppvs))
    elif fit_type == "include_anel":
        # log(ppv + distance) = A + B * Mw + C * distance
        Amatrix = np.matrix(np.vstack([np.ones(n_rows), mags, distances])).T
        rhs = np.matrix(np.log10(ppvs) + np.log10(distances))
    elif fit_type == "geom_and_anel":
        # log(ppv) = A + B * Mw + C * log(distance) + D*distance
        Amatrix = np.matrix(
            np.vstack([np.ones(n_rows), mags, np.log10(distances), distances])
        ).T
        rhs = np.matrix(np.log10(ppvs))
    return sqa(np.linalg.inv(Amatrix.T * Amatrix) * Amatrix.T * np.matrix(rhs).T)


def predict_mag(ppv, distance, fit, fit_type):
    if fit_type == "simple":
        predicted_mag = (np.log10(ppv) + np.log10(distance) - fit[0]) / fit[1]
    elif fit_type == "geom_only":
        predicted_mag = (np.log10(ppv) - fit[2] * np.log10(distance) - fit[0]) / fit[1]
    elif fit_type == "include_anel":
        predicted_mag = (
            np.log10(ppv) + np.log10(distance) - fit[2] * distance - fit[0]
        ) / fit[1]
    elif fit_type == "geom_and_anel":
        predicted_mag = (
            np.log10(ppv) - fit[2] * np.log10(distance) - fit[3] * distance - fit[0]
        ) / fit[1]
    return predicted_mag


def station_event_distance(station, event):
    return np.sqrt(
        (station["x"] - event["easting"]) ** 2
        + (station["y"] - event["northing"]) ** 2
        + (station["z"] - event["depth"]) ** 2
    )


def Mw_PPV_AB(pgv, d_hyp):
    return (np.log10(pgv) + 3.186 - np.log10(d_hyp) + 0.196 * d_hyp) / 0.946


snr_threshold = 3.0
d_hyp, mags, ppvs = [], [], []
for event in Mw_events:
    ground_motion = pgv[event]
    for station in ground_motion:
        snr_stat = station.split(".")[1] + "P"
        if event in snr:
            if snr_stat in snr[event]:
                if snr[event][snr_stat] > 0:
                    d_hyp.append(
                        station_event_distance(stations[station], events[event])
                    )
                    ppvs.append(ground_motion[station])
                    mags.append(events[event]["Mw"])

fit = fit_data(ppvs, d_hyp, mags, fit_type)

m_residual = []
for event in Mw_events:
    ground_motion = pgv[event]
    for station in ground_motion:
        snr_stat = station.split(".")[1] + "P"
        if event in snr:
            if snr[event][snr_stat] > 0:
                m_residual.append(
                    events[event]["Mw"]
                    - predict_mag(
                        pgv[event][station],
                        station_event_distance(stations[station], events[event]),
                        fit,
                        fit_type,
                    )
                )

fig, ax = plt.subplots(2, 2, figsize=[16, 16])
sc0 = ax[0, 0].scatter(d_hyp, ppvs, c=mags, marker="o", edgecolor="0.7")
ax[0, 0].set_yscale("log")
ax[0, 0].set_ylim(4e-8, 5e-6)
ax[0, 0].set_xlabel("distance (m)")
ax[0, 0].set_ylabel("ppv (m/s)")

sc1 = ax[0, 1].scatter(mags, ppvs, c=d_hyp, marker="o", edgecolor="0.7")
ax[0, 1].set_yscale("log")
ax[0, 1].set_ylim(4e-8, 5e-6)
ax[0, 1].set_xlabel("magnitude")
ax[0, 1].set_ylabel("ppv (m/s)")


ax[1, 0].scatter(d_hyp, m_residual, c=mags, marker="o", edgecolor="0.7")
ax[1, 0].set_xlabel("distance (m)")
ax[1, 0].set_ylabel("magnitude residual from simple model")
y1, y2 = ax[1, 0].get_ylim()
fit2 = fit_data(ppvs, d_hyp, mags, fit_type2)

m_residual2 = []
for event in Mw_events:
    ground_motion = pgv[event]
    for station in ground_motion:
        m_residual2.append(
            events[event]["Mw"]
            - Mw_PPV_AB(
                1e3 * pgv[event][station],
                station_event_distance(stations[station], events[event]) / 1000.0,
            )
            # - predict_mag(
            #     pgv[event][station],
            #     station_event_distance(stations[station], events[event]),
            #     fit2,
            #     fit_type2,
            # )
        )

len(d_hyp)


#
# ax[1, 1].scatter(d_hyp, m_residual2, c=mags, marker="o", edgecolor="0.7")
# ax[1, 1].set_xlabel("distance (m)")
# ax[1, 1].set_ylabel("geometrical spreading fit residual")
# ax[1, 1].set_ylim(y1, y2)
ppvs_all = []
m_predict = []
m_unique = []
stack_amp = []
snr_threshold = 1
for event in Mw_events:
    m_by_station = []
    for station in pgv[event]:
        ppvs_all.append(pgv[event][station])
        m_by_station.append(
            # predict_mag(
            #     pgv[event][station],
            #     station_event_distance(stations[station], events[event]),
            #     fit2,
            #     fit_type2,
            # )
            Mw_PPV_AB(
                1e3 * pgv[event][station],
                station_event_distance(stations[station], events[event]) / 1000.0,
            )
        )

    m_unique.append(np.median(m_by_station))
    for station in pgv[event]:
        m_predict.append(m_unique[-1])
        stack_amp.append(events[event]["amp"])
f.close()

cb0 = fig.colorbar(sc0, ax=ax[0, 0])
cb0.set_label("magnitude")

cb1 = fig.colorbar(sc1, ax=ax[0, 1])
cb1.set_label("distance (m)")
cb2 = fig.colorbar(sc0, ax=ax[1, 0])
cb2.set_label("magnitude")
cb3 = fig.colorbar(sc0, ax=ax[1, 1])
cb3.set_label("magnitude")


f2, a2 = plt.subplots(2)

a2[0].semilogx(ppvs_all, m_predict, "k.", alpha=0.02)
a2[0].set_xlabel("ppv (m/s)")
a2[0].set_ylabel("predicted magnitude")

a2[1].loglog(ppvs_all, stack_amp, "k.", alpha=0.02)
a2[1].set_xlabel("ppv (m/s)")
a2[1].set_ylabel("stack amplitude")

mag_bins = np.arange(-1.2, 1, 0.1)


n_bin = []
for bin in mag_bins[:-1]:
    n_bin.append(len(np.where(m_unique > bin)[0]))

distance_bins = np.arange(3000, 6600, 500)


distances = sqa(d_hyp)
m_residual = sqa(m_residual)
m_residual2 = sqa(m_residual2)
box_residuals, box_residuals2 = [], []
for ii, d_bin in enumerate(distance_bins[:-1]):
    d_bin2 = distance_bins[ii + 1]
    i_bin = np.where((distances > d_bin) & (distances < d_bin2))[0]
    box_residuals.append(m_residual[i_bin])
    box_residuals2.append(m_residual2[i_bin])
#
# f3, a3 = plt.subplots(2)
#
# a3[0].violinplot(box_residuals)
# a3[0].grid(which="both")
# a3[1].violinplot(box_residuals2)
# a3[1].grid(which="both")
plt.show()
