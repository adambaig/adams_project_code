import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import mplstereonet as mpls

D2R = np.pi / 180

f = open("MTI_delivered_Jun21.csv")
head = f.readline()
lines = f.readlines()
f.close()


def normal(strike, dip):
    st = D2R * strike
    dp = D2R * dip


strike1, dip1, rake1, strike2, dip2, rake2 = [], [], [], [], [], []
for line in lines:
    lspl = line.split(",")
    strike1.append(float(lspl[2]))
    dip1.append(float(lspl[3]))
    strike2.append(float(lspl[5]))
    dip2.append(float(lspl[6]))


strikes = np.hstack([strike1, strike1])
dips = np.hstack([dip1, dip2])
fig, ax = mpls.subplots(1)
ax.pole(strikes, dips, "k.")
ax.density_contourf(strikes, dips, measurement="poles", alpha=0.5, cmap="Reds")
ax.grid()
plt.show()
