import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
from numpy import pi, sin, cos
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import sys

sys.path.append("..")

from read_inputs import read_wells, read_events, read_stations

arbitrary_rotate = 0
minorLocator = MultipleLocator(500)
minorLocator2 = MultipleLocator(500)


events = read_events()
wells = read_wells()
stations = read_stations()


def lighten_color(color, amount=0.5):
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys

    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])


R = np.matrix(
    [
        [cos(arbitrary_rotate * pi / 180), -sin(arbitrary_rotate * pi / 180)],
        [sin(arbitrary_rotate * pi / 180), cos(arbitrary_rotate * pi / 180)],
    ]
)

reference = {
    "easting": np.average(np.array([v["easting"] for k, v in events.items()])),
    "northing": np.average(np.array([v["northing"] for k, v in events.items()])),
    "depth": 0,
}


def sqa(x):
    return np.squeeze(np.array(x))


def rotate(dictionary, reference, rotation_matrix):
    eastings = (
        np.array([v["easting"] for k, v in dictionary.items()]) - reference["easting"]
    )
    northings = (
        np.array([v["northing"] for k, v in dictionary.items()]) - reference["northing"]
    )
    if "tvdss" in dictionary.keys():
        depths = -(
            np.array([v["tvdss"] for k, v in dictionary.items()]) - reference["depth"]
        )
    else:
        depths = (
            np.array([v["depth"] for k, v in dictionary.items()]) - reference["depth"]
        )
    h1, h2 = [sqa(x) for x in rotation_matrix * np.matrix([eastings, northings])]
    return {"h1": h1, "h2": h2, "new_depth": depths}


def rotate_array(dictionary, reference, rotation_matrix):
    eastings = dictionary["easting"] - reference["easting"]
    northings = dictionary["northing"] - reference["northing"]
    if "tvdss" in dictionary.keys():
        depths = -(dictionary["tvdss"])
    else:
        depths = dictionary["depth"]
    h1, h2 = [sqa(x) for x in rotation_matrix * np.matrix([eastings, northings])]
    return {"h1": h1, "h2": h2, "new_depth": depths}


base_colors = {
    "103/13-33-0": "maroon",
    "103/14-33-0": "navy",
    "103/15-33-0": "darkolivegreen",
    "103/16-33-0": "darkgoldenrod",
    "104/15-33-0": "rebeccapurple",
}

events

for well in base_colors:

    eastings = (
        np.array([v["easting"] for k, v in events.items()]) - reference["easting"]
    )
    northings = (
        np.array([v["northing"] for k, v in events.items()]) - reference["northing"]
    )
    depths = np.array([v["depth"] for k, v in events.items()]) - reference["depth"]
    stages = np.array([v["stage"] for k, v in events.items()])
    ev_well = np.array([v["well"] for k, v in events.items()])
    np.unique(ev_well)

    for event in events:
        if events[event]["Mw"] > -8:
            mags.append(events[event]["Mw"])
        else:
            mags.append(0.58 * np.log10(events[event]["amp"]) + 0.1)
    mags = np.array(mags)
    isort = np.argsort(mags)
    # a1.scatter(eastings, northings, c=event_color, marker=".", edgecolor="k")
    f1, a1 = plt.subplots()
    a1.set_aspect("equal")

    f2, a2 = plt.subplots()
    a2.set_aspect("equal")

    for well in wells:
        a1.plot(
            wells[well]["easting"] - reference["easting"],
            wells[well]["northing"] - reference["northing"],
            "firebrick",
            lw=2,
            zorder=-1,
        )
        a1.plot(
            wells[well]["easting"] - reference["easting"],
            wells[well]["northing"] - reference["northing"],
            "0.7",
            lw=4,
            zorder=-2,
        )
        a2.plot(
            wells[well]["northing"] - reference["northing"],
            wells[well]["tvdss"],
            "firebrick",
            lw=2,
            zorder=-1,
        )
        a2.plot(
            wells[well]["northing"] - reference["northing"],
            wells[well]["tvdss"],
            "0.7",
            lw=4,
            zorder=-2,
        )

    f1, a1 = plt.subplots()
    a1.set_aspect("equal")

    f2, a2 = plt.subplots()
    a2.set_aspect("equal")

    for well in wells:
        a1.plot(
            wells[well]["easting"] - reference["easting"],
            wells[well]["northing"] - reference["northing"],
            "firebrick",
            lw=2,
            zorder=-1,
        )
        a1.plot(
            wells[well]["easting"] - reference["easting"],
            wells[well]["northing"] - reference["northing"],
            "0.7",
            lw=4,
            zorder=-2,
        )
    scatter = a1.scatter(
        eastings[isort],
        northings[isort],
        c=mags[isort],
        s=40 * (mags[isort] + 2),
        vmin=-1,
        vmax=0.5,
        marker=".",
        edgecolor="k",
    )
    a1.set_facecolor("0.95")
    a1.xaxis.set_minor_locator(minorLocator)
    a1.yaxis.set_minor_locator(minorLocator)
    a1.xaxis.set_ticks_position("none")
    a1.yaxis.set_ticks_position("none")
    a1.grid(True, which="both")
    a1.set_xticklabels([])
    a1.set_yticklabels([])
    cb1 = f1.colorbar(scatter)
    cb1.set_label("magnitude")
    f1.tight_layout()
plt.show()
