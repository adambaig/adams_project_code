import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
from numpy import pi, sin, cos
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import sys

sys.path.append("..")

from read_inputs import read_wells, read_events, read_stations

arbitrary_rotate = 43
minorLocator = MultipleLocator(500)
minorLocator2 = MultipleLocator(500)
minorLocator3 = MultipleLocator(1000)

events = read_events()
wells = read_wells()
stations = read_stations()


R = np.matrix(
    [
        [cos(arbitrary_rotate * pi / 180), -sin(arbitrary_rotate * pi / 180)],
        [sin(arbitrary_rotate * pi / 180), cos(arbitrary_rotate * pi / 180)],
    ]
)

reference = {
    "easting": np.average(np.array([v["easting"] for k, v in events.items()])),
    "northing": np.average(np.array([v["northing"] for k, v in events.items()])),
    "depth": 0,
}


def sqa(x):
    return np.squeeze(np.array(x))


def rotate(dictionary, reference, rotation_matrix):
    eastings = (
        np.array([v["easting"] for k, v in dictionary.items()]) - reference["easting"]
    )
    northings = (
        np.array([v["northing"] for k, v in dictionary.items()]) - reference["northing"]
    )
    if "tvdss" in dictionary.keys():
        depths = -(
            np.array([v["tvdss"] for k, v in dictionary.items()]) - reference["depth"]
        )
    else:
        depths = (
            np.array([v["depth"] for k, v in dictionary.items()]) - reference["depth"]
        )
    h1, h2 = [sqa(x) for x in rotation_matrix * np.matrix([eastings, northings])]
    return {"h1": h1, "h2": h2, "new_depth": depths}


def rotate_array(dictionary, reference, rotation_matrix):
    eastings = dictionary["easting"] - reference["easting"]
    northings = dictionary["northing"] - reference["northing"]
    if "tvdss" in dictionary.keys():
        depths = -(dictionary["tvdss"])
    else:
        depths = dictionary["depth"]
    h1, h2 = [sqa(x) for x in rotation_matrix * np.matrix([eastings, northings])]
    return {"h1": h1, "h2": h2, "new_depth": depths}


def rotate_stations(stations, reference, rotation_matrix):
    for station in stations:
        eastings = stations[station]["e"] - reference["easting"]
        northings = stations[station]["n"] - reference["northing"]
        h1, h2 = [sqa(x) for x in rotation_matrix * np.matrix([eastings, northings]).T]
        stations[station]["h1"] = h1
        stations[station]["h2"] = h2
    return stations


R_events = rotate(events, reference, R)


R_stations = rotate_stations(stations, reference, R)

f1, a1 = plt.subplots()
a1.set_aspect("equal")

f2, a2 = plt.subplots()
a2.set_aspect("equal")
f3, a3 = plt.subplots()
a3.set_aspect("equal")

for well in wells:
    R_well = rotate_array(wells[well], reference, R)
    a1.plot(R_well["h1"], R_well["h2"], "firebrick", lw=2, zorder=-1)
    a1.plot(R_well["h1"], R_well["h2"], "0.7", lw=4, zorder=-2)
    a2.plot(
        wells[well]["northing"],
        wells[well]["tvdss"],
        "firebrick",
        lw=2,
        zorder=-1,
    )
    a2.plot(wells[well]["northing"], wells[well]["tvdss"], "0.7", lw=4, zorder=-2)
    a3.plot(R_well["h1"], R_well["h2"], "firebrick", lw=2, zorder=-1)
    a3.plot(R_well["h1"], R_well["h2"], "0.7", lw=4, zorder=-2)

for station in stations:
    a3.plot(stations[station]["h1"], stations[station]["h2"], "kv")


mags = np.array([v["Mw"] for k, v in events.items()])
i_sort = np.argsort(mags)
scatter = a1.scatter(
    R_events["h1"][i_sort],
    R_events["h2"][i_sort],
    c=mags[i_sort],
    vmin=-1,
    vmax=0,
    edgecolor="k",
)
a1.set_facecolor("0.95")
a1.xaxis.set_minor_locator(minorLocator)
a1.yaxis.set_minor_locator(minorLocator)
a1.grid(True, which="both")
a1.set_xticklabels([])
a1.set_yticklabels([])
a1.tick_params(color="w")
cb = f1.colorbar(scatter, extend="both")
cb.set_label("moment magnitude")

a2.plot(
    [v["northing"] for k, v in events.items()],
    -np.array([v["depth"] for k, v in events.items()]),
    ".",
    color="royalblue",
    markeredgecolor="k",
)
a2.set_facecolor("0.95")
a2.xaxis.set_minor_locator(minorLocator2)
a2.yaxis.set_minor_locator(minorLocator2)
a2.grid(True, which="both")
a2.set_xticklabels([])
a2.set_yticklabels([])
a2.tick_params(color="w")


a3.set_facecolor("0.95")

a3.xaxis.set_minor_locator(minorLocator3)
a3.yaxis.set_minor_locator(minorLocator3)
a3.grid(True, which="both")
a3.set_xticklabels([])
a3.set_yticklabels([])
a3.tick_params(color="w")

plt.show()
