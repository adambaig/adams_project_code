from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import numpy as np
from obspy import read, UTCDateTime
from scipy.fftpack import fft, ifft, fftfreq
from scipy.signal import detrend
import sys

from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_moment_tensor.moment_tensor_inversion import inversion_matrix_row
from sms_moment_tensor.MT_math import mt_matrix_to_vector

sys.path.append("..")
from read_inputs import (
    read_wells,
    read_mts,
    read_stations,
    read_picks,
    read_velocity_model,
)


mt_catalog = (
    "C:\\Users\\adambaig\\Project\\Paramount 2-28\\MTI\\"
    "Paramount_catalog_MT_confidence_neg.csv"
)
stations = read_stations()
mts = read_mts(mt_catalog)
id0, mt0 = list(mts.items())[110]
seed_dir = "C:\\Users\\adambaig\\Project\\Paramount 2-28\mSeeds\\"
st = read(f"{seed_dir}{id0}.seed")
start_time = datetime.strptime(str(st[0].stats.starttime), "%Y-%m-%dT%H:%M:%S.%fZ")
picks = read_picks(id0)
t_zero = datetime.strptime(str(UTCDateTime(picks["t0"])), "%Y-%m-%dT%H:%M:%S.%fZ")
i_zero = int((t_zero - start_time).total_seconds() // (st[0].stats.delta))


times = [start_time + timedelta(0, s) for s in st[0].times()]
tsfreq = fftfreq(st[0].stats.npts, st[0].stats.delta)

fig, ax = plt.subplots(figsize=[8, 12])
for ii, station in enumerate(stations):
    tr = st.select(station=f"{station[3:]}")[0].detrend()
    p_pick = datetime.strptime(
        str(UTCDateTime(picks[station]["P"])), "%Y-%m-%dT%H:%M:%S.%fZ"
    )
    ax.plot(times[7200:7800], 1 + ii + 50 * tr.data[7200:7800], c="0.2")
    ax.plot([p_pick, p_pick], [ii + 0.4, ii + 1.6], "firebrick", lw=2)
# fig.savefig("example_trace.png")

source = {"e": mt0["easting"], "n": mt0["northing"], "z": -mt0["depth"]}
velocity_model = read_velocity_model(mode="Funkenstein")
dc_mt = mt0["dc mt"]

fig, ax = plt.subplots(figsize=[8, 12])
for ii, (station_id, station) in enumerate(stations.items()):
    raypath = isotropic_ray_trace(source, station, velocity_model, "P")
    radiation_pattern_coefficients = inversion_matrix_row(raypath, "P")
    flip = np.squeeze(
        np.array(np.sign(radiation_pattern_coefficients @ mt_matrix_to_vector(dc_mt)))
    )
    tr = st.select(station=f"{station_id[3:]}")[0].detrend()
    p_pick = datetime.strptime(
        str(UTCDateTime(picks[station_id]["P"])), "%Y-%m-%dT%H:%M:%S.%fZ"
    )
    ax.plot(times[7200:7800], 1 + ii + flip * 50 * tr.data[7200:7800], c="0.2")
    ax.plot([p_pick, p_pick], [ii + 0.4, ii + 1.6], "royalblue", lw=2)
# fig.savefig("flipped_trace.png")


# def inverse_Q(signal, raypath, Q):

n_stations = len(stations)
once_shifted_data = np.zeros([len(st), st[0].stats.npts])
p_rays = []
fig, ax = plt.subplots(figsize=[8, 12])
snr = np.zeros(len(st))
for ii, (station_id, station) in enumerate(stations.items()):
    raypath = isotropic_ray_trace(source, station, velocity_model, "P")
    p_rays.append(raypath)
    radiation_pattern_coefficients = inversion_matrix_row(raypath, "P")
    flip = np.squeeze(
        np.array(np.sign(radiation_pattern_coefficients @ mt_matrix_to_vector(dc_mt)))
    )
    tr = st.select(station=f"{station_id[3:]}")[0].detrend()
    once_shifted_data[ii, :] = np.real(
        fft(
            ifft(tr.data)
            * np.exp(-2.0j * np.pi * (picks[station_id]["P"] - picks["t0"]) * tsfreq)
        )
    )

    ax.plot(
        times[i_zero - 20 : i_zero + 180],
        1 + ii + flip * 50 * detrend(once_shifted_data)[ii, i_zero - 20 : i_zero + 180],
        c="0.2",
    )


# fig.savefig('once_shifted.png')
Amatrix_lags = np.zeros([(n_stations**2 - n_stations) // 2 + 1, n_stations])
lags = np.zeros((n_stations**2 - n_stations) // 2 + 1)
i_pair = -1
# determine relative shifts by x-correlation (Van Decar and Crosson, 1990)
for i_station in range(n_stations - 1):
    for j_station in range(i_station + 1, n_stations):
        i_pair += 1
        xcorr = np.correlate(
            once_shifted_data[i_station, i_zero - 10 : i_zero + 10],
            once_shifted_data[j_station, i_zero - 10 : i_zero + 10],
            mode="full",
        )
        lags[i_pair] = (np.argmax(xcorr)) / st[0].stats.sampling_rate
        Amatrix_lags[i_pair, i_station] = 1.0
        Amatrix_lags[i_pair, j_station] = -1.0
Amatrix_lags[-1, :] = np.ones(n_stations)

time_shifts = -Amatrix_lags.T @ lags.T / n_stations


fig, ax = plt.subplots(figsize=[8, 12])
twice_shifted_data = np.zeros([n_stations, st[0].stats.npts])
for i_station in range(n_stations):
    twice_shifted_data[i_station, :] = np.real(
        fft(
            ifft(once_shifted_data[i_station, :])
            * np.exp(2.0j * np.pi * (time_shifts[i_station]) * tsfreq)
        )
    )
    ax.plot(
        times[i_zero - 50 : i_zero + 180],
        1
        + i_station
        + flip
        * 50
        * detrend(twice_shifted_data)[i_station, i_zero - 50 : i_zero + 180],
        c="0.2",
    )


fig, ax = plt.subplots()
ax.plot(np.sum(once_shifted_data[:, i_zero - 20 : i_zero + 180], axis=0))
