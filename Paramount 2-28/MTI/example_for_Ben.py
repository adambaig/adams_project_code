stations = read_stations()

"""  stations is a nested dictionary like so:
{'PM.01': {'e': 504640.067832, 'n': 6027489.08112, 'z': 836.4014286},
 'PM.02': {'e': 504183.690811, 'n': 6029146.48802, 'z': 832.1814286},
 'PM.03': {'e': 504113.029386, 'n': 6031836.92149, 'z': 812.8585714},
 'PM.04': {'e': 503042.031389, 'n': 6030126.84045, 'z': 798.44},
 'PM.05': {'e': 500414.049378, 'n': 6030546.16751, 'z': 807.0071429},
 'PM.06': {'e': 502110.732629, 'n': 6028807.23585, 'z': 807.6542857},
 'PM.07': {'e': 500872.074753, 'n': 6027426.56434, 'z': 827.2171429},
 'PM.08': {'e': 502533.510066, 'n': 6026563.37647, 'z': 864.38},
 'PM.09': {'e': 503816.103725, 'n': 6025758.2065, 'z': 851.58},
 'PM.10': {'e': 505007.390955, 'n': 6024843.83167, 'z': 875.5142857},
 'PM.11': {'e': 505299.475535, 'n': 6025784.28237, 'z': 881.4185714},
 'PM.12': {'e': 506512.645286, 'n': 6026460.46617, 'z': 842.6257143},
 'PM.13': {'e': 505636.295769, 'n': 6027818.42707, 'z': 831.4985714},
 'PM.14': {'e': 506608.618433, 'n': 6029703.58369, 'z': 815.7071429},
 'PM.15': {'e': 505529.576573, 'n': 6030324.64446, 'z': 839.8214286}}
"""

velocity_model = read_velocity_model(mode="read")
""" velocity_model is an list of dictionaries
[{'vp': 3358.0274291654287,
  'vs': 1938.7580401748073,
  'rho': 2359.8410615626417},
 {'vp': 4185.545219483219,
  'vs': 2416.5256591739876,
  'rho': 2493.444485244485,
  'top': -742.7741935483873},
 {'vp': 5169.198441936441,
  'vs': 2984.4381119465984,
  'rho': 2628.5575806284633,
  'top': -1195.193548387097},
 {'vp': 4287.03325037125,
  'vs': 2475.1198011267174,
  'rho': 2508.423765887138,
  'top': -2184.4838709677424},
 {'vp': 5793.74017047817,
  'vs': 3345.01744704032,
  'rho': 2704.5900595984394,
  'top': -2437.8387096774195}]

"""

Q = {"P": 250, "S": 100}
source = {
    "moment_magnitude": 1,
    "moment_tensor": np.matrix([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]),
    "stress_drop": 3.0e5,
    "e": catalog[event]["easting"],
    "n": catalog[event]["northing"],
    "z": catalog[event]["elevation"],
}
time_series = np.arange(n_points) / sample_rate

"""
swap in the easting, northing, and elevation of the source into the catalog[event] properties
"""


p_raypath = isotropic_ray_trace(source, stations[id], velocity_model, "P")
s_raypath = isotropic_ray_trace(source, stations[id], velocity_model, "S")

"""
id is a key of the station dictionary (e.g. 'PM.13')
"""
waveform_3C = get_response(
    p_raypath, s_raypath, source, stations[id], Q, time_series, 0
)

"""
the last argument of waveform_3C of 0 makes the waveforms noise_free

waveform_3C is a dictionary with the keys "e" "n" "z" with arrays of waveforms
+ve is in the east,  north, and up directions, respectively
"""

returns
