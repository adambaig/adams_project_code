import matplotlib

matplotlib.use("Qt4agg")

import numpy as np
import matplotlib.pyplot as plt

f = open("shift_per_event_best58_location.csv")
head = f.readline()
lines = f.readlines()
f.close()

statics = {}
nstations = len(head.split(",")) - 1
stations = []
for station in head.split(",")[2:]:
    stations.append(station.split()[0])
    statics[station.split()[0]] = []

fig, ax = plt.subplots()

for line in lines:
    lspl = line.split(",")
    shifts = np.array([float(t) for t in lspl[2:]])
    for ii, station in enumerate(stations):
        statics[station].append(float(line.split(",")[ii + 2]))


med_stat = np.zeros(len(stations))
dev_stat = np.zeros(len(stations))
kurt_stat = np.zeros(len(stations))
for ii, station in enumerate(stations):
    statics[station] = np.array(statics[station])
    med_stat[ii] = np.median(
        [s for s in statics[station][np.where(statics[station] > -900)[0]]]
    )
    dev_stat[ii] = np.std(
        [s for s in statics[station][np.where(statics[station] > -900)[0]]]
    )


fliers = dict(marker=".")

ax.boxplot(
    [statics[station][np.where(statics[station] > -900)[0]] for station in stations],
    flierprops=fliers,
)
ax.set_ylabel("time residual (s)")


plt.show()

g = open("aggregate_statics.csv", "w")
g.write("station,median statics,25th percentile,75th percentile\n")
for station in stations:
    i_good = np.where(statics[station] > -900)[0]
    g.write(
        station
        + ",%.4e,%.4e,%.4e\n"
        % (
            np.median(statics[station][i_good]),
            np.percentile(statics[station][i_good], 25),
            np.percentile(statics[station][i_good], 75),
        )
    )
g.close()
