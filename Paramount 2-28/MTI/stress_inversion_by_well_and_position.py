import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
from sms_moment_tensor.MT_math import (
    decompose_MT,
    mt_to_sdr,
    unit_vector_to_trend_plunge,
    strike_dip_to_normal,
)
from sms_moment_tensor.stress_inversions import (
    iterate_Micheal_stress_inversion,
    decompose_stress,
    resolve_shear_and_normal_stress,
    most_unstable_sdr,
)
import sys

sys.path.append("..")
from read_inputs import read_wells, read_mts

grid_spacing = 50
reservoir_thickness = 200
min_events = 40
PI = np.pi

wells = read_wells()
east_center = 504000
north_center = 6027500
n_iterations = 1000


def r_error(r_iterations, R):
    variance = 0
    n_iterations = len(r_iterations)
    for r_iteration in r_iterations:
        variance += (r_iteration - R) * (r_iteration - R)
    return 2 * np.sqrt(variance / (n_iterations - 1))


def angular_error(vector_iterations, central_vector):
    n_iterations = len(vector_iterations)
    angles = np.zeros(n_iterations)
    for i_vector, vector in enumerate(vector_iterations):
        angles[i_vector] = abs(
            np.arccos(np.dot(np.squeeze(np.array(vector)), central_vector))
        )
    return 2 * 180 * sum(angles) / (n_iterations - 1) / PI


"""
r_error(r_iterations, R)
stress_axes["s1"]
angular_error([a["s1"] for a in axes_iteration], stress_axes["s1"])
angular_error()
R
r_iterations
"""

mt_catalog = (
    "C:\\Users\\adambaig\\Project\\Paramount 2-28\\MTI\\"
    + "Paramount_catalog_MT_confidence_neg_flip.csv"
)
conf_thresh = 0.9
r_bins = np.linspace(0, 1, 101)
moment_tensors = {
    k: v for k, v in read_mts(mt_catalog).items() if v["confidence"] > conf_thresh
}
for mt in moment_tensors:
    strike, dip, rake = mt_to_sdr(moment_tensors[mt]["dc mt"], conjugate=False)
    moment_tensors[mt]["strike"] = strike
    moment_tensors[mt]["dip"] = dip
    moment_tensors[mt]["rake"] = rake

mt_wells = np.unique(np.array([v["well"] for k, v in moment_tensors.items()]))
colors = ["firebrick", "forestgreen", "royalblue", "darkgoldenrod", "indigo"]
north_cut = {
    "103/13-33-0": 6028000,
    "103/14-33-0": 6028000,
    "103/15-33-0": 6027500,
    "103/16-33-0": 6027500,
    "104/15-33-0": 6027500,
}
wells.keys()
axis_color = {"s1": "firebrick", "s2": "forestgreen", "s3": "royalblue"}
fig, a = plt.subplots()
a.set_facecolor("0.95")
a.set_aspect("equal")
extent = ["all", "heel", "toe"]
f = open("stress_states.csv", "w")
f.write("well, data selection, n events,")
for i_axis in range(3):
    f.write(
        "sigma%i tr, sigma%i pl, sigma%i error, " % (i_axis + 1, i_axis + 1, i_axis + 1)
    )
f.write("R, R error\n")

circle_x, circle_y = [], []
for i_rad in np.arange(0, PI + PI / 100, PI / 100):
    circle_x.append(np.cos(i_rad))
    circle_y.append(np.sin(i_rad))
circle_x = np.array(circle_x)
circle_y = np.array(circle_y)

for i_well, well in enumerate(wells):
    well_events = {k: v for k, v in moment_tensors.items() if v["well"] == well}
    heel_events = {
        k: v for k, v in well_events.items() if v["northing"] < north_cut[well]
    }
    toe_events = {
        k: v for k, v in well_events.items() if v["northing"] > north_cut[well]
    }
    easting, northing = np.array(
        [(v["easting"], v["northing"]) for k, v in well_events.items()]
    ).T

    for i_subset, subset in enumerate([well_events, heel_events, toe_events]):
        best_stress_tensor, stress_tensors = iterate_Micheal_stress_inversion(
            subset, n_iterations, output_iterations=True
        )
        easting_subset, northing_subset = np.array(
            [(v["easting"], v["northing"]) for k, v in subset.items()]
        ).T

        f_well, a_well = plt.subplots()
        a_well.set_aspect("equal")
        for well_again in wells:
            a_well.plot(
                wells[well_again]["easting"] - east_center,
                wells[well_again]["northing"] - north_center,
                "0.8",
                lw=1,
                zorder=-5,
            )
            a_well.plot(
                wells[well_again]["easting"] - east_center,
                wells[well_again]["northing"] - north_center,
                "0.2",
                lw=3,
                zorder=-6,
            )
        a_well.plot(
            easting_subset - east_center,
            northing_subset - north_center,
            "o",
            color=colors[i_well],
            markeredgecolor="k",
        )
        f_well.tight_layout()
        f_well.savefig(
            "Stress Strain plots\\"
            + well.replace("/", "_")
            + "_"
            + extent[i_subset]
            + "_events.png"
        )

        fig, ax = mpls.subplots()
        f_hist, a_hist = plt.subplots()
        r_iterations, axes_iteration = [], []
        for i_iteration in range(n_iterations):
            R, stress_axes = decompose_stress(stress_tensors[i_iteration])
            r_iterations.append(R)
            axes_iteration.append(stress_axes)
            for axis, vector in stress_axes.items():
                trend, plunge = unit_vector_to_trend_plunge(vector)
                ax.line(
                    plunge,
                    trend,
                    ".",
                    alpha=0.05,
                    color=axis_color[axis],
                    zorder=3,
                )
        n_events = len(subset)
        R, stress_axes = decompose_stress(best_stress_tensor)
        taus, sigmas = np.zeros(n_events), np.zeros(n_events)
        for i_event, event in enumerate(subset):
            sdr_unstable = most_unstable_sdr(subset[event]["dc mt"], best_stress_tensor)
            moment_tensors[event]["true strike"] = sdr_unstable[0]
            moment_tensors[event]["true dip"] = sdr_unstable[1]
            moment_tensors[event]["true rake"] = sdr_unstable[2]
            normal_vector = strike_dip_to_normal(sdr_unstable[0], sdr_unstable[1])
            sigmas[i_event], taus[i_event] = resolve_shear_and_normal_stress(
                best_stress_tensor, normal_vector
            )
        a_hist.hist(
            r_iterations,
            r_bins,
            facecolor="seagreen",
            edgecolor="0.1",
            zorder=2,
        )
        y1, y_max = a_hist.get_ylim()
        a_hist.plot([R, R], [0, y_max], "k")
        a_hist.set_xlim([0, 1])
        a_hist.set_ylim([0, y_max])
        plot_title = well + ": " + extent[i_subset] + (", %i events") % len(subset)
        a_hist.set_xlabel("stress ratio, R")
        a_hist.set_ylabel("count")
        a_hist.set_title(plot_title)
        f_hist.savefig(
            "Stress Strain plots\\"
            + well.replace("/", "_")
            + "_"
            + extent[i_subset]
            + "_R.png"
        )
        f.write(well + "," + extent[i_subset] + "," + str(len(subset)))
        patches = []
        for axis, vector in stress_axes.items():
            trend, plunge = unit_vector_to_trend_plunge(vector)
            (patch,) = ax.line(
                plunge,
                trend,
                "o",
                color=axis_color[axis],
                zorder=4,
                markeredgecolor="k",
            )
            patches.append(patch)
            error = angular_error([a[axis] for a in axes_iteration], vector)
            f.write(",%.1f, %.1f, %.1f" % (trend, plunge, error))
        fig_legend, ax_legend = plt.subplots()
        ax_legend.legend(patches, ["$\sigma_1$", "$\sigma_2$", "$\sigma_3$"])
        f.write("%.3f,%.3f\n" % (R, r_error(r_iterations, R)))
        ax.set_title(plot_title.replace("events", "events\n\n"))
        fig.savefig(
            "Stress Strain plots\\"
            + well.replace("/", "_")
            + "_"
            + extent[i_subset]
            + "_axes.png"
        )
        f_mc, a_mc = plt.subplots(figsize=[8, 4])
        a_mc.set_aspect("equal")
        a_mc.plot(circle_x, circle_y, "0.3", zorder=10)
        a_mc.plot(R * circle_x + 1 - R, R * circle_y, "0.3", zorder=10)
        a_mc.plot((1 - R) * circle_x - R, (1 - R) * circle_y, "0.3", zorder=10)
        a_mc.plot(sigmas, taus, ".", color=colors[i_well])
        a_mc.arrow(-1.1, 0, 2.2, 0, head_width=0.06, head_length=0.1, fc="k", ec="k")
        a_mc.arrow(-1.1, 0, 0, 1.1, head_width=0.06, head_length=0.1, fc="k", ec="k")
        a_mc.text(-1.1, 1.22, "$\\tau$", ha="center")
        a_mc.text(1.21, 0.0, "${\\sigma}$", va="center")
        a_mc.text(-0.98, -0.1, "$\\sigma_3$")
        a_mc.text(-0.98, -0.1, "$\\sigma_3$")
        a_mc.text(1 - 2 * R + 0.03, -0.1, "$\\sigma_2$")
        a_mc.text(1.02, -0.1, "$\\sigma_1$")
        a_mc.axis("off")
        a_mc.set_xlim(-1.3, 1.3)
        a_mc.set_ylim(-0.15, 1.3)

        f_mc.savefig(
            "Stress Strain plots\\"
            + well.replace("/", "_")
            + "_"
            + extent[i_subset]
            + "_mc.png"
        )
        f_fp, a_fp = mpls.subplots(1)
        strikes = np.array([v["true strike"] for k, v in subset.items()])
        dips = np.array([v["true dip"] for k, v in subset.items()])
        rakes = np.mod(np.array([v["true rake"] for k, v in subset.items()]), 360)
        a_fp.pole(strikes, dips, ".", color=colors[i_well])
        a_fp.density_contourf(
            strikes,
            dips,
            measurement="poles",
            cmap="Greys",
            alpha=0.5,
            zorder=-1,
        )
        f_fp.savefig(
            "Stress Strain plots\\"
            + well.replace("/", "_")
            + "_"
            + extent[i_subset]
            + "_fp.png"
        )
        f_rr = plt.figure()
        a_rr = f_rr.add_subplot(111, projection="polar")
        bin_number = 31
        bins = np.linspace(0, 2 * np.pi, bin_number)
        n_rake, _, _ = plt.hist(rakes * np.pi / 180.0, bins, visible=False)
        width = 2 * np.pi / bin_number
        bars = a_rr.bar(bins[:-1], n_rake, width=width, facecolor=colors[i_well])
        lines, labels = plt.thetagrids(
            [0, 90, 180, 270],
            [
                "            Left-lateral\n            strike-slip",
                "Thrust",
                "Right-lateral            \nstrike-slip           ",
                "Normal",
            ],
        )
        a_rr.set_title("Rakes of both fault planes\n\n")
        f_rr.savefig(
            "Stress Strain plots\\"
            + well.replace("/", "_")
            + "_"
            + extent[i_subset]
            + "_rr.png"
        )
    plt.close("all")
    a.plot(
        wells[well]["easting"] - east_center,
        wells[well]["northing"] - north_center,
        "0.8",
        lw=1,
        zorder=-5,
    )
    a.plot(
        wells[well]["easting"] - east_center,
        wells[well]["northing"] - north_center,
        "0.2",
        lw=3,
        zorder=-6,
    )
    a.plot(
        easting - east_center,
        northing - north_center,
        "o",
        color=colors[i_well],
        markeredgecolor="k",
    )

a.set_xlabel("easting relative to %.0f (m)" % east_center)
a.set_ylabel("northing relative to %.0f (m)" % north_center)
f.close()
