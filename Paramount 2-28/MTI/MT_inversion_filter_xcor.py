import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np

from moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
import sys

sys.path.append("..")

from read_inputs import (
    read_velocity_model,
    read_stations,
    read_events_from_Do,
    read_waveform,
    read_picks,
    read_amp_picks,
)

catalog = read_events_from_Do()
stations = read_stations()
velocity_model = read_velocity_model(mode="read")

Q = {"P": 250, "S": 100}
explosion = {
    "moment_tensor": np.matrix([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]),
    "stress_drop": 3.0e5,
}
xcorr_time = np.arange(-2, 2.001, 0.004)
paz_10hz = corn_freq_2_paz(10.0, damp=np.sqrt(0.5))
snr_threshold = 1.5
bandpass_hf = 120
bandpass_lf = 2
sample_rate = 250
n_stations = len(stations)
plot_out = False


def sqa(x):
    return np.squeeze(np.array(x))


def createTraceStats(network, n1, o1, d1, component):
    # make a stats file for a seed trace
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


event, value = dict(catalog).popitem()

event = {k: v for k, v in catalog.items() if k == "471960"}

event = "471960"

i_event = 0

f_x = open("MT_output.csv", "w")
f_x.write(
    "event time,magnitude,n picks,m11,m12,m13,m22,m23,m33,dc11,"
    + "dc12,dc13,dc22,dc23,dc33,cn_gen,cn_dv,rsq_gen,rsq_dc\n"
)
for event in catalog:
    picks = read_picks(event)
    st = read_waveform(event, UTCDateTime(picks["t0"]), 2, raw=True)
    n_points = st[0].stats.npts
    if plot_out:
        fig0 = plt.figure(figsize=[16, 9])
        ax0 = fig0.add_axes([0.05, 0.1, 0.25, 0.8])
        ax1 = fig0.add_axes([0.35, 0.1, 0.25, 0.8])
        ax2 = fig0.add_axes([0.65, 0.1, 0.25, 0.8])
    dat = np.zeros([n_points, n_stations])
    norm_dat = np.zeros([n_points, n_stations])
    xcor_dat = np.zeros([n_points, n_stations])
    time_series = np.arange(n_points) / sample_rate
    tsfreq = fftfreq(len(time_series), time_series[1])
    winfreq = fftfreq(100, time_series[1])
    if picks is None:
        print("picks needed for event " + event)
    else:
        p_raypath = [object() for _ in range(n_stations)]
        s_raypath = [object() for _ in range(n_stations)]
        trace = st[0]
        jj = -1
        for trace in st:
            expl_channel_object = {
                "component": "Z",
                "channel": "CPZ",
                "station": "dummy",
                "network": "PM",
                "location": "00",
            }
            expl_stats = createTraceStats(
                "PM",
                trace.stats.npts,
                trace.stats.starttime,
                trace.stats.delta,
                expl_channel_object,
            )
            jj += 1
            id = trace.get_id().split(".VC.")[0]

            source = {
                "x": catalog[event]["easting"],
                "y": catalog[event]["northing"],
                "z": catalog[event]["depth"],
            }
            p_raypath[jj] = isotropic_ray_trace(
                source, stations[id], velocity_model, "P"
            )
            s_raypath[jj] = isotropic_ray_trace(
                source, stations[id], velocity_model, "S"
            )
            explosion["moment_magnitude"] = catalog[event]["Mw"]
            explosion["x"] = source["x"]
            explosion["y"] = source["y"]
            explosion["z"] = source["z"]

            explosive_response = np.real(
                ifft(
                    fft(
                        get_response(
                            p_raypath[jj],
                            s_raypath[jj],
                            explosion,
                            stations[id],
                            Q,
                            time_series,
                            0,
                        )["d"]
                    )
                    * np.exp(
                        2.0j * np.pi * (p_raypath[jj]["traveltime"] - 1.0) * tsfreq
                    )
                )
            )

            normalized_response = explosive_response / np.sqrt(
                np.correlate(explosive_response, explosive_response)
            )
            expl_trace = Trace(data=normalized_response, header=expl_stats)
            expl_trace.simulate(paz_simulate=paz_10hz)
            predicted_onset = picks[id]["P"]
            trace.detrend()
            dat[:, jj] = np.real(
                ifft(
                    fft(trace.data)
                    * np.exp(
                        2.0j
                        * np.pi
                        * (
                            predicted_onset
                            - UTCDateTime(catalog[event]["UTC"]).timestamp
                            - 1
                        )
                        * tsfreq
                    )
                )
            )

            # plt.plot(dat[:,jj]/max(abs(dat[:,jj])))
            # plt.plot(expl_trace.data)

            # disp_spec = np.array(
            #     10e-8
            #     * abs(fft(dat[500 : 500 + lwin, jj]))[:lwin2]
            #     / winfreq[:lwin2]
            #     / lwin
            #     / 2
            #     / np.pi
            # )
            # noise_spec = np.array(
            #     10e-8
            #     * abs(fft(dat[400 - lwin : 400, jj]))[:lwin2]
            #     / winfreq[:lwin2]
            #     / lwin
            #     / 2
            #     / np.pi
            # )
            # igood = np.where(disp_spec / noise_spec > snr_threshold)[0]
            xcor_dat[:, jj] = np.correlate(
                filter.bandpass(
                    dat[:, jj], bandpass_lf, bandpass_hf, sample_rate, zerophase=True
                ),
                filter.bandpass(
                    normalized_response,
                    bandpass_lf,
                    bandpass_hf,
                    sample_rate,
                    zerophase=True,
                ),
                "same",
            )
            if plot_out:
                ax0.plot(
                    time_series - 1, jj + 1 + dat[:, jj] / max(abs(dat[:, jj])), "0.2"
                )

                ax2.plot(
                    time_series - time_series[-1] / 2,
                    jj + 1 + xcor_dat[:, jj] / max(abs(xcor_dat[:, jj])),
                    "0.2",
                    zorder=-1,
                )
            norm_dat[:, jj] = xcor_dat[:, jj] / max(abs(xcor_dat[:, jj]))
        fig = plt.figure()
        ax1 = fig.add_axes([0.1, 0.4, 0.8, 0.55])
        ax2 = fig.add_axes([0.1, 0.1, 0.8, 0.25])
        ax1.pcolor(
            time_series, range(n_stations), norm_dat.T, vmax=1, vmin=-1, cmap="bwr"
        )
        ax1.set_xlim([0, 2])
        ax1.set_xticklabels("")
        envelopes = hilbert(norm_dat, axis=0)
        envelopes *= np.conj(envelopes)
        i_x_max = np.argmax(np.sum(np.real(envelopes), axis=-1))
        statics = np.zeros(n_stations)
        for i_station in range(n_stations):
            statics[istation] = time_series(
                np.argmax(np.real(envelopes[i_x_max - 20 : i_x_max + 20, i_station]))
                + 20
            )

        ax2.plot(time_series, np.sum(np.real(envelopes), axis=-1))
        ax2.set_xlim([0, 2])
        ax2.set_xlabel("time (s)")
        fig.savefig("xcor_images//" + event + ".png")

        """
        right_hand_side_xcor = []
        inversion_matrix_xcor = []
        signal_xcor, noise_xcor, snr_xcor, amp_xcor = (
            np.zeros(n_stations),
            np.zeros(n_stations),
            np.zeros(n_stations),
            np.zeros(n_stations),
        )
        if plot_out:
            ax0.set_xlim([-0.4, 0.4])
            ax1.set_xlim([-0.4, 0.4])
            ax2.set_xlim([-0.4, 0.4])
            ax1.set_title(
                event
                + (" Mw = %.1f" % catalog[event]["magnitude"]).replace("-", "$-$")
            )
            ax0.set_ylabel("raw data")
            ax2.set_ylabel("cross-correlated filtered data")
            ax0.set_xlabel("frequency")
            ax1.set_xlabel("shifted time (s)")
            ax2.set_xlabel("shifted time (s)")
        for jj in range(n_stations):
            n_amp_win = int(500 / bandpass_hf / 2)
            i_trace_max = (
                np.argmax(abs(xcor_dat[i_x_max - n_amp_win : i_x_max + n_amp_win, jj]))
                + i_x_max
                - n_amp_win
            )
            amp_xcor[jj] = xcor_dat[i_trace_max, jj]
            signal_xcor[jj] = np.std(
                xcor_dat[i_x_max - n_amp_win : i_x_max + n_amp_win, jj]
            )
            noise_xcor[jj] = np.std(xcor_dat[: 450 + 2 * n_amp_win, jj])
            snr_xcor[jj] = signal_xcor[jj] / noise_xcor[jj]
            if plot_out:
                ax2.text(-0.39, jj + 1.01, ("%.1f" % (snr_xcor[jj])), fontsize=6)
            if snr_xcor[jj] > snr_threshold:
                if plot_out:
                    if amp_xcor[jj] > 0:
                        ax2.plot(
                            time_series[i_trace_max] - time_series[-1] / 2,
                            jj + 1 + amp_xcor[jj] / max(abs(xcor_dat[:, jj])),
                            "o",
                            color="orangered",
                            markeredgecolor="k",
                            zorder=2,
                            alpha=0.3,
                        )
                        ax2.plot(
                            time_series[i_x_max - n_amp_win : i_x_max + n_amp_win]
                            - time_series[-1] / 2,
                            jj
                            + 1
                            + xcor_dat[i_x_max - n_amp_win : i_x_max + n_amp_win, jj]
                            / max(abs(xcor_dat[:, jj])),
                            color="orangered",
                            lw=2,
                            zorder=1,
                        )
                    else:
                        ax2.plot(
                            time_series[i_x_max] - time_series[-1] / 2,
                            jj + 1 + amp_xcor[jj] / max(abs(xcor_dat[:, jj])),
                            "o",
                            color="turquoise",
                            markeredgecolor="k",
                            zorder=2,
                            alpha=0.3,
                        )
                        ax2.plot(
                            time_series[i_x_max - n_amp_win : i_x_max + n_amp_win]
                            - time_series[-1] / 2,
                            jj
                            + 1
                            + xcor_dat[i_x_max - n_amp_win : i_x_max + n_amp_win, jj]
                            / max(abs(xcor_dat[:, jj])),
                            color="turquoise",
                            lw=2,
                            zorder=1,
                        )
                cos_incoming = np.sqrt(
                    1.0
                    - (
                        p_raypath[jj]["hrz_slowness"]["x"] ** 2
                        + p_raypath[jj]["hrz_slowness"]["y"] ** 2
                    )
                    * p_raypath[jj]["velocity_model_chunk"][-1]["v"] ** 2
                )
                right_hand_side_xcor.append(
                    back_project_amplitude(
                        p_raypath[jj], s_raypath[jj], amp_xcor[jj] / cos_incoming, "P"
                    )
                )
                inversion_matrix_xcor.append(inversion_matrix_row(p_raypath[jj], "P"))
        if sqa(right_hand_side_xcor).shape is ():
            n_measurements = 0
        else:
            n_measurements = len(sqa(right_hand_side_xcor))
        if n_measurements > 6:
            mt_gen_x, cn_gen_x, rsq_gen_x, mt_dc_x, cn_dev_x, rsq_dc_x = solve_moment_tensor(
                inversion_matrix_xcor, right_hand_side_xcor
            )
            f_x.write(
                event
                + ","
                + str(catalog[event]["magnitude"])
                + ","
                + str(n_measurements)
            )
            f_x.write(
                ",%.6e,%.6e,%.6e,%.6e,%.6e,%.6e"
                % (
                    mt_gen_x[0],
                    mt_gen_x[1],
                    mt_gen_x[2],
                    mt_gen_x[3],
                    mt_gen_x[4],
                    mt_gen_x[5],
                )
            )
            f_x.write(
                ",%.6e,%.6e,%.6e,%.6e,%.6e,%.6e"
                % (
                    mt_dc_x[0],
                    mt_dc_x[1],
                    mt_dc_x[2],
                    mt_dc_x[3],
                    mt_dc_x[4],
                    mt_dc_x[5],
                )
            )
            f_x.write(
                ",%.1f,%.1f,%.3f,%.3f\n" % (cn_gen_x, cn_dev_x, rsq_gen_x, rsq_dc_x)
            )
        else:
            f_x.write(event + "," + str(catalog[event]["magnitude"]) + "0")
            f_x.write(",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n")
        """
f_x.close()
if plot_out:
    plt.show()
