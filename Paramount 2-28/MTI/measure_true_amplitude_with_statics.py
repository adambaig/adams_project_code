import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
from obspy import read, UTCDateTime, Trace
from obspy.core.trace import Stats
from obspy.signal import filter
from obspy.signal.invsim import corn_freq_2_paz

from scipy.fftpack import fft, ifft, fftfreq
from scipy.signal import hilbert
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response


import sys

sys.path.append("..")

from read_inputs import (
    read_velocity_model,
    read_stations,
    read_events_from_Do,
    read_events,
    read_waveform,
    read_picks,
)

catalog = read_events()
catalog = read_events_from_Do()
# catalog = {k: v for k, v in read_events().items() if v["Mw"] > -8}
# catalog = {k: v for k, v in read_events().items() if v["amp"] > 0.02}
# catalog = {k: v for k, v in read_events().items() if v["Mw"] > -8}
# catalog = {k: v for k, v in read_events().items() if k in ["209572"]}
stations = read_stations()
velocity_model = read_velocity_model(mode="read")

Q = {"P": 500, "S": 100}
explosion = {
    "moment_tensor": np.matrix([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]),
    "stress_drop": 3.0e5,
}
xcorr_time = np.arange(-2, 2.001, 0.004)
paz_10hz = corn_freq_2_paz(10.0, damp=np.sqrt(0.5))
snr_threshold = 1.0
bandpass_hf = 120
bandpass_lf = 2
n_amp_win = 3
sample_rate = 250
n_stations = len(stations)
plot_out = True
epsilon = 1e-18


def sqa(x):
    return np.squeeze(np.array(x))


def createTraceStats(network, n1, o1, d1, component):
    # make a stats file for a seed trace
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


statics = {}
f = open("aggregate_statics.csv")
head = f.readline()
for line in f.readlines():
    lspl = line.split(",")
    #        statics[lspl[0]] = float(lspl[1])
    statics[lspl[0]] = {
        "median": float(lspl[1]),
        "25th": float(lspl[2]),
        "75th": float(lspl[3]),
    }
f.close()

for ii, event in enumerate({k: v for k, v in catalog.items()}):
    picks = read_picks(event)
    if picks is None:
        print("picks needed for event " + event)
    else:
        st = read_waveform(event, UTCDateTime(picks["t0"]), 5, raw=True)
        n_points = st[0].stats.npts
        if plot_out:
            fig0 = plt.figure(figsize=[16, 9])
            ax0 = fig0.add_axes([0.05, 0.35, 0.4, 0.6])
            ax2 = fig0.add_axes([0.55, 0.35, 0.4, 0.6])
            ax3 = fig0.add_axes([0.05, 0.05, 0.4, 0.25])
            ax4 = fig0.add_axes([0.55, 0.05, 0.4, 0.25])
        dat = np.zeros([n_points, n_stations])
        norm_dat = np.zeros([n_points, n_stations])
        xcor_dat = np.zeros([n_points, n_stations])
        time_series = np.arange(n_points) / sample_rate
        tsfreq = fftfreq(len(time_series), time_series[1])
        winfreq = fftfreq(100, time_series[1])
        pick_out = (
            "picks//"
            + picks["id"]
            + "_"
            + UTCDateTime.strftime(UTCDateTime(picks["t0"]), "%Y%m%d.%H%M%S.%f")
            + ".PICKS"
        )
        f_pick = open(pick_out, "w")
        p_raypath = [object() for _ in range(n_stations)]
        s_raypath = [object() for _ in range(n_stations)]
        n_raw_dat = np.zeros(dat.shape)
        for jj, trace in enumerate(st):
            id = trace.get_id().split(".VC.")[0]
            f_pick.write(id + (".VC,P,%.6f\n") % picks[id]["P"])
            source = {
                "x": catalog[event]["easting"],
                "y": catalog[event]["northing"],
                "z": catalog[event]["depth"],
            }

            predicted_onset = picks[id]["P"] + statics[id]["median"]
            trace.detrend()
            dat[:, jj] = np.real(
                ifft(
                    fft(filter.bandpass(trace.data, 20, 50, 250))
                    * np.exp(
                        2.0j
                        * np.pi
                        * (
                            predicted_onset
                            - UTCDateTime(catalog[event]["UTC"]).timestamp
                            - 1.0
                        )
                        * tsfreq
                    )
                )
            )
            n_raw_dat[:, jj] = dat[:, jj] / max(abs(dat[:, jj]))
        dat_envelopes = hilbert(n_raw_dat, axis=0)
        dat_envelopes *= np.conj(dat_envelopes)

        i_shift_max = np.argmax(np.sum(np.real(dat_envelopes[100:400]), axis=-1)) + 100

        mute = np.zeros(n_points)
        mute[i_shift_max - 3 * n_amp_win : i_shift_max] = np.ones(3 * n_amp_win)
        mute[i_shift_max - 5 * n_amp_win : i_shift_max - 3 * n_amp_win] = np.sin(
            np.pi * np.arange(2 * n_amp_win) / 4 / n_amp_win
        )
        mute[i_shift_max : i_shift_max + 2 * n_amp_win] = np.cos(
            np.pi * np.arange(2 * n_amp_win) / 4 / n_amp_win
        )
        windowed_data = np.zeros([6 * n_amp_win, n_stations])

        array_mute = np.tile(mute, [15, 1]).T
        mute_data = array_mute * dat
        for i_station in range(n_stations):
            windowed_data[:, i_station] = mute_data[
                i_shift_max - 3 * n_amp_win : i_shift_max + 3 * n_amp_win,
                i_station,
            ]
        Amatrix_lags = np.zeros([(n_stations**2 - n_stations) // 2 + 1, n_stations])
        lags = np.zeros((n_stations**2 - n_stations) // 2 + 1)
        i_pair = -1
        # determine relative shifts by x-correlation (Van Decar and Crosson, 1990)
        for i_station in range(n_stations - 1):
            for j_station in range(i_station + 1, n_stations):
                i_pair += 1
                xcorr = np.correlate(
                    filter.bandpass(windowed_data[:, i_station], 20, 60, 250),
                    filter.bandpass(windowed_data[:, j_station], 20, 60, 250),
                    mode="full",
                )
                lags[i_pair] = (np.argmax(abs(xcorr)) - 6 * n_amp_win) / sample_rate
                Amatrix_lags[i_pair, i_station] = 1.0
                Amatrix_lags[i_pair, j_station] = -1.0
        Amatrix_lags[-1, :] = np.ones(n_stations)
        Amatrix_lags = np.matrix(Amatrix_lags)
        lags = np.matrix(lags)
        time_shifts = -sqa(Amatrix_lags.T * lags.T / n_stations)

        shifted_dat = np.zeros([n_points, n_stations])
        for i_station in range(n_stations):
            shifted_dat[:, i_station] = np.real(
                fft(
                    ifft(dat[:, i_station])
                    * np.exp(2.0j * np.pi * (time_shifts[i_station]) * tsfreq)
                )
            )

        n_shift_dat = np.zeros(dat.shape)
        for i_station in range(len(stations)):
            n_shift_dat[:, i_station] = shifted_dat[:, i_station] / max(
                abs(shifted_dat[:, i_station])
            )
        shift_envelopes = hilbert(n_shift_dat, axis=0)
        shift_envelopes *= np.conj(shift_envelopes)
        i_shift_max = (
            np.argmax(np.sum(np.real(shift_envelopes[100:400]), axis=-1)) + 100
        )
        mute = np.zeros(n_points)
        mute[i_shift_max - 3 * n_amp_win : i_shift_max] = np.ones(3 * n_amp_win)
        mute[i_shift_max - 5 * n_amp_win : i_shift_max - 3 * n_amp_win] = np.sin(
            np.pi * np.arange(2 * n_amp_win) / 4 / n_amp_win
        )
        mute[i_shift_max : i_shift_max + 2 * n_amp_win] = np.cos(
            np.pi * np.arange(2 * n_amp_win) / 4 / n_amp_win
        )

        ref_trace = shifted_dat[:, 5] * mute
        ref_trace = ref_trace / np.sqrt(np.correlate(ref_trace, ref_trace))
        for jj, trace in enumerate(st):
            xcor_dat[:, jj] = np.correlate(
                filter.bandpass(
                    shifted_dat[:, jj],
                    bandpass_lf,
                    bandpass_hf,
                    sample_rate,
                    zerophase=True,
                ),
                filter.bandpass(
                    ref_trace,
                    bandpass_lf,
                    bandpass_hf,
                    sample_rate,
                    zerophase=True,
                ),
                "same",
            )
            if plot_out:
                ax0.plot(
                    time_series - 1,
                    jj + 1 + shifted_dat[:, jj] / max(abs(shifted_dat[:, jj])),
                    "0.2",
                )

                ax2.plot(
                    time_series - time_series[-1] / 2,
                    jj + 1 + xcor_dat[:, jj] / max(abs(xcor_dat[:, jj])),
                    "0.2",
                    zorder=-1,
                )
            norm_factor = max(abs(xcor_dat[:, jj]))
            if norm_factor > epsilon:
                norm_dat[:, jj] = xcor_dat[:, jj] / max(abs(xcor_dat[:, jj]))
        ax0.plot(
            time_series - 1,
            6 + ref_trace / max(abs(ref_trace)),
            "g",
            zorder=10,
            lw=3,
        )

        envelopes = hilbert(norm_dat, axis=0)
        envelopes *= np.conj(envelopes)
        i_x_max = np.argmax(np.sum(np.real(envelopes), axis=-1))
        amp, snr, i_max = (
            np.zeros(n_stations),
            np.zeros(n_stations),
            np.zeros(n_stations, dtype=int),
        )
        for i_station, station in enumerate(stations):
            if max(abs(envelopes[:, i_station])) > epsilon:
                i_max[i_station] = (
                    np.argmax(
                        abs(
                            xcor_dat[
                                i_x_max - n_amp_win : i_x_max + n_amp_win,
                                i_station,
                            ]
                        )
                    )
                    - n_amp_win
                    + i_x_max
                )
                #                amp[i_station] = xcor_dat[i_max[i_station], i_station]
                # snr[i_station] = abs(amp[i_station]) / np.sqrt(
                #     np.var(
                #         xcor_dat[
                #             i_max[i_station] - 100 : i_max[i_station] - 10,
                #             i_station,
                #         ]
                #     )
                # )
                recon_time = (
                    picks[id]["P"]
                    - UTCDateTime(catalog[event]["UTC"]).timestamp
                    + statics[id]["median"]
                    + (i_shift_max - 250) * time_series[1]
                    - time_shifts[i_id]
                    + time_series[i_max[i_station]]
                    - time_series[-1] / 2
                )
                i_recon_time = int(recon_time // time_series[1])
                amp[i_station] = dat[i_recon_time, i_station]
                snr[i_station] = abs(amp[i_station]) / np.sqrt(
                    np.var(dat[i_recon_time - 150 : i_recon_time - 50, i_station])
                )
                f_pick.write(
                    station
                    + (".VC,P_amp_pick,%.6f\n")
                    % (
                        picks[id]["P"]
                        + statics[id]["median"]
                        + time_series[i_shift_max - 250]
                        - time_shifts[i_station]
                        + time_series[i_max[i_station]]
                        - time_series[-1] / 2
                    )
                )
                f_pick.write(station + (".VC,P_amp,%.4e\n") % amp[i_station])
                f_pick.write(station + (".VC,P_amp_snr,%.4e\n") % snr[i_station])
            else:
                f_pick.write(station + (".VC,P_amp,%.4e\n") % 0)
                f_pick.write(station + (".VC,P_amp_snr,%.4e\n") % 0)

        if plot_out:
            ax3.plot()
            ax0.set_ylabel("raw data")
            ax2.set_ylabel("cross-correlated filtered data")
            ax0.set_xticklabels([])
            ax0.plot(
                [time_series[i_shift_max] - 1, time_series[i_shift_max] - 1],
                [0, 16],
                "r",
                zorder=-3,
            )
            ax0.plot(
                [
                    time_series[i_shift_max - n_amp_win] - 1,
                    time_series[i_shift_max - n_amp_win] - 1,
                ],
                [0, 16],
                "b",
                zorder=-3,
            )
            ax0.plot(
                [
                    time_series[i_shift_max + n_amp_win] - 1,
                    time_series[i_shift_max + n_amp_win] - 1,
                ],
                [0, 16],
                "b",
                zorder=-3,
            )
            ax2.plot(
                [
                    time_series[i_shift_max - n_amp_win] - time_series[-1] / 2,
                    time_series[i_shift_max - n_amp_win] - time_series[-1] / 2,
                ],
                [0, 16],
                "b",
                zorder=-3,
            )
            ax2.plot(
                [
                    time_series[i_shift_max + n_amp_win] - time_series[-1] / 2,
                    time_series[i_shift_max + n_amp_win] - time_series[-1] / 2,
                ],
                [0, 16],
                "b",
                zorder=-3,
            )

            ax2.plot(
                [time_series[i_x_max] - 1, time_series[i_x_max] - 1],
                [0, 16],
                "r",
                zorder=-3,
            )
            ax2.set_xticklabels([])

            ax3.plot(time_series - 1, np.sum(np.real(dat_envelopes), axis=-1))

            ax3.set_xlabel("shifted time (s)")
            ax4.set_xlabel("time (s)")
        for jj in range(n_stations):
            if plot_out:
                ax2.text(-0.19, jj + 1.02, ("%.1f" % (snr[jj])), fontsize=6)
            if snr[jj] > snr_threshold:
                if plot_out:
                    if amp[jj] > 0:
                        ax2.plot(
                            time_series[i_max[jj]] - time_series[-1] / 2,
                            jj + 1 + amp[jj] / max(abs(xcor_dat[:, jj])),
                            "o",
                            color="orangered",
                            markeredgecolor="k",
                            zorder=2,
                            alpha=0.3,
                        )
                        ax2.plot(
                            time_series[i_x_max - n_amp_win : i_x_max + n_amp_win]
                            - time_series[-1] / 2,
                            jj
                            + 1
                            + xcor_dat[i_x_max - n_amp_win : i_x_max + n_amp_win, jj]
                            / max(abs(xcor_dat[:, jj])),
                            color="orangered",
                            lw=2,
                            zorder=1,
                        )
                    else:
                        ax2.plot(
                            time_series[i_max[jj]] - time_series[-1] / 2,
                            jj + 1 + amp[jj] / max(abs(xcor_dat[:, jj])),
                            "o",
                            color="turquoise",
                            markeredgecolor="k",
                            zorder=2,
                            alpha=0.3,
                        )
                        ax2.plot(
                            time_series[i_x_max - n_amp_win : i_x_max + n_amp_win]
                            - time_series[-1] / 2,
                            jj
                            + 1
                            + xcor_dat[i_x_max - n_amp_win : i_x_max + n_amp_win, jj]
                            / max(abs(xcor_dat[:, jj])),
                            color="turquoise",
                            lw=2,
                            zorder=1,
                        )

        if plot_out:
            ax0.set_xlim([-0.5, 0.5])
            ax2.set_xlim([-0.5, 0.5])
            ax3.set_xlim([-0.5, 0.5])
            ax4.set_xlim([-0.5, 0.5])
            #        fig0.savefig("polarity_QC/polarity_" + event.zfill(8) + ".png")
            #    plt.close("all")
            rec_fig, rec_ax = plt.subplots()
            picks[id]["P"]
            picks[id]["P"] + statics[id]["median"] + time_shifts[-1]

            for i_id, id in enumerate(stations):
                recon_time = (
                    picks[id]["P"]
                    - UTCDateTime(catalog[event]["UTC"]).timestamp
                    + statics[id]["median"]
                    + (i_shift_max - 250) * time_series[1]
                    - time_shifts[i_id]
                    + time_series[i_max[i_id]]
                    - time_series[-1] / 2
                )

                trace = st.select(station=id.split(".")[1])[0]
                filter_data = filter.bandpass(trace.data, 20, 50, 250)
                rec_ax.plot(
                    time_series,
                    i_id + 1 + filter_data / max(abs(filter_data)),
                    "k",
                    lw=0.5,
                )

                rec_ax.plot(
                    [recon_time, recon_time],
                    [i_id + 0.0, i_id + 2],
                    "r",
                    lw=2,
                    zorder=2,
                    alpha=0.5,
                )

            rec_ax.set_xlim([0.8, 1.6])
            rec_fig.savefig("amp_pick_QC//amp_pick_" + event.zfill(8) + ".png")
            plt.close("all")

        f_pick.close()

plt.show()
