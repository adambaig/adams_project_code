import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
import sys

from sms_moment_tensor.MT_math import decompose_MT

sys.path.append("..")
from read_inputs import read_wells, read_mts


grid_spacing = 50
reservoir_thickness = 200
min_events = 1
PI = np.pi

wells = read_wells()
east_center = 504000
north_center = 6027500


def r_nearest_neighbours(grid_point, events, radius):
    radius_sq = radius * radius
    ref_east, ref_north = grid_point["east"], grid_point["north"]
    cull_events = {
        k: v
        for k, v in events.items()
        if (
            (v["easting"] - ref_east) ** 2 + (v["northing"] - ref_north) ** 2
            < radius_sq
        )
    }
    return cull_events


def scaled_mt(event):
    moment = 10 ** (1.5 * event["Mw"] + 9)
    return event["dc mt"] * moment / np.linalg.norm(event["dc mt"])


mt_catalog = "C:\\Users\\adambaig\\Project\\Paramount 2-28\\MTI\\Paramount_catalog_MT_confidence_neg_flip.csv"
conf_thresh = 0.9

moment_tensors = {
    k: v for k, v in read_mts(mt_catalog).items() if v["confidence"] > conf_thresh
}
wells = read_wells()

easting, northing = np.array(
    [(v["easting"], v["northing"]) for k, v in moment_tensors.items()]
).T

east_grid = np.arange(min(easting), max(easting), grid_spacing)
north_grid = np.arange(min(northing), max(northing), grid_spacing)
grid_points = []
for east in east_grid:
    for north in north_grid:
        grid_mts = r_nearest_neighbours(
            {"east": east, "north": north}, moment_tensors, 2 * grid_spacing
        )
        volume = 4 * PI * grid_spacing * grid_spacing * reservoir_thickness
        if len(grid_mts) >= min_events:
            strain = sum([scaled_mt(v) for k, v in grid_mts.items()]) / volume / 2.0
            grid_points.append({"east": east, "north": north, "strain": strain})
fig, ax = plt.subplots(1, 2, sharey=True)


t_trend_east, t_trend_north, p_trend_east, p_trend_north = [], [], [], []
for grid_point in grid_points:
    decomp = decompose_MT(grid_point["strain"])
    t_trend, t_plunge = decomp["t_trend"], decomp["t_plunge"]
    p_trend, p_plunge = decomp["p_trend"], decomp["p_plunge"]
    t_trend_east.append(np.sin(PI * t_trend / 180.0) * np.cos(PI * t_plunge / 180.0))
    t_trend_north.append(np.cos(PI * t_trend / 180.0) * np.cos(PI * t_plunge / 180.0))
    p_trend_east.append(np.sin(PI * p_trend / 180.0) * np.cos(PI * p_plunge / 180.0))
    p_trend_north.append(np.cos(PI * p_trend / 180.0) * np.cos(PI * p_plunge / 180.0))

ax[0].set_aspect("equal")
ax[0].quiver(
    np.array([v["east"] for v in grid_points]),
    np.array([v["north"] for v in grid_points]),
    t_trend_east,
    t_trend_north,
    pivot="mid",
    width=0.005,
    zorder=5,
    headlength=0,
    headaxislength=0,
    headwidth=0,
    scale=1 / 100,
    scale_units="x",
    color="royalblue",
)

ax[1].set_aspect("equal")
ax[1].quiver(
    np.array([v["east"] for v in grid_points]),
    np.array([v["north"] for v in grid_points]),
    p_trend_east,
    p_trend_north,
    pivot="mid",
    width=0.005,
    zorder=5,
    headlength=0,
    headaxislength=0,
    headwidth=0,
    scale=1 / 100,
    scale_units="x",
    linewidth=0.5,
    color="firebrick",
)
ax[0].set_title("tensional strain")
ax[1].set_title("compressional strain")
minor_ticks = np.arange(502500, 6030000, 250)
major_ticks = np.arange(502500, 6030000, 500)
for a in ax:
    for id, well in wells.items():
        a.plot(well["easting"], well["northing"], "0.8", lw=1, zorder=-5)
        a.plot(well["easting"], well["northing"], "0.2", lw=3, zorder=-6)
    # a.set_xlabel("easting relative to %.0f (m)" % east_center)
    a.set_facecolor("lightgrey")

    a.set_xticks(minor_ticks, minor=True)
    a.set_yticks(minor_ticks, minor=True)
    a.set_xticks(major_ticks, minor=False)
    a.set_yticks(major_ticks, minor=False)

    a.grid(which="both")
    a.grid(which="minor", alpha=1)
    a.set_ylim([6025993.0, 6029780.0])
    a.set_xlim([502897.0, 505123.0])
    a.set_aspect("equal")
    a.set_yticklabels([])
    a.set_xticklabels([])
    a.set_axisbelow(True)
    a.tick_params(which="both", color="w")
fig.savefig("strain_plot.png")
# ax[0].set_ylabel("northing relative to %.0f (m)" % north_center)
