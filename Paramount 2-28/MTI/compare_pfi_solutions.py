import matplotlib

matplotlib.use("Qt5agg")

import glob
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from obspy.imaging.mopad_wrapper import beach
import mplstereonet


from sms_moment_tensor.MT_math import (
    sdr_to_mt,
    mt_matrix_to_vector,
    mt_to_sdr,
    mt_vector_to_matrix,
)
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
)
from sms_ray_modelling.raytrace import isotropic_ray_trace
import sys

sys.path.append("..")

from read_inputs import (
    read_stations,
    read_velocity_model,
    read_events,
    read_polarities,
)

catalogs = [
    "moment_tensors_amplitude.csv",
    "moment_tensors_pfi_amps_based_high_SNR.csv",
    "moment_tensors_pfi_amps_based_all.csv",
    "mts_pure_pfi.csv",
]

pick_dirs = [
    "C:\\Users\\adambaig\\Project\\Paramount 2-28\\MTI\\picks\\archive\\",
    "C:\\Users\\adambaig\\Project\\Paramount 2-28\\MTI\\picks\\pfi\\",
    "C:\\Users\\adambaig\\Project\\Paramount 2-28\\MTI\\picks\\pfi\\allPicks\\",
    "C:\\Users\\adambaig\\Project\\Paramount 2-28\\MTI\\picks\\pfi\\allPicks\\",
]

bwr = cm.get_cmap("bwr")
velocity_model = read_velocity_model(mode="super_duper")
events = read_events()
stations = read_stations()
polarities = read_polarities()


def read_mt_catalog(csv):
    f = open(csv)
    head = f.readline()
    lines = f.readlines()
    f.close()
    catalog = {}
    if head.split(",")[-2] == "dip2":
        for line in lines:
            lspl = line.split(",")
            event_id = lspl[0]
            strike, dip, rake = [float(s) for s in lspl[2:5]]
            catalog[event_id] = mt_matrix_to_vector(sdr_to_mt(strike, dip, rake))
    else:
        for line in lines:
            lspl = line.split(",")
            event_id = lspl[0].split(".")[0]
            catalog[event_id] = [-float(s) for s in lspl[1:]]
    return catalog


def read_picks(pick_dir, event):
    pick_file = glob.glob(pick_dir + event.zfill(8) + "*.PICKS")[0]
    f = open(pick_file)
    picks = {}
    for line in f.readlines():
        lspl = line.split(",")
        station = lspl[0].split(".VC")[0]
        if station not in picks:
            picks[station] = {}
        picks[station][lspl[1]] = float(lspl[2])
    f.close()
    return picks


def sqa(x):
    return np.squeeze(np.array(x))


title = [
    "First motion",
    "optimal stack, high SNR",
    "optimal stack, all",
    "imaging",
]
mt_dict = []
for ii in range(4):
    mt_dict.append(read_mt_catalog(catalogs[ii]))
snr_cutoff = 10
for event in {k: v for k, v in mt_dict[3].items()}:
    fig = plt.figure(figsize=[12, 12])
    ax0 = plt.subplot2grid((3, 4), (0, 0), colspan=4)
    ax1 = plt.subplot2grid((3, 4), (1, 0), projection="stereonet")
    ax2 = plt.subplot2grid((3, 4), (1, 1), projection="stereonet")
    ax3 = plt.subplot2grid((3, 4), (1, 2), projection="stereonet")
    ax4 = plt.subplot2grid((3, 4), (1, 3), projection="stereonet")
    ax5 = plt.subplot2grid((3, 4), (2, 0))
    ax6 = plt.subplot2grid((3, 4), (2, 1))
    ax7 = plt.subplot2grid((3, 4), (2, 2))
    ax8 = plt.subplot2grid((3, 4), (2, 3))
    reg_ax = [ax5, ax6, ax7, ax8]
    ax0.set_aspect("equal")
    beachballs = []
    source = {
        "x": events[event]["easting"],
        "y": events[event]["northing"],
        "z": events[event]["depth"],
    }
    for ii, axi in enumerate([ax1, ax2, ax3, ax4]):
        right_hand_side = []
        A_matrix = []
        picks = read_picks(pick_dirs[ii], event)
        for station in picks:
            p_raypath = isotropic_ray_trace(
                source, stations[station], velocity_model, "P"
            )
            s_raypath = isotropic_ray_trace(
                source, stations[station], velocity_model, "S"
            )
            cos_incoming = np.sqrt(
                1.0
                - (
                    p_raypath["hrz_slowness"]["x"] ** 2
                    + p_raypath["hrz_slowness"]["y"] ** 2
                )
                * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
            )

            if picks[station]["P_amp_snr"] > snr_cutoff:
                if ii == 0:
                    polarity_correct = -polarities[event]
                else:
                    polarity_correct = -1
                right_hand_side.append(
                    back_project_amplitude(
                        p_raypath,
                        s_raypath,
                        polarity_correct * picks[station]["P_amp"] / cos_incoming,
                        "P",
                        Q=500,
                    )
                )
                A_matrix.append(inversion_matrix_row(p_raypath, "P"))
        max_abs_rhs = max(abs(np.array(right_hand_side)))
        jj = -1
        for i_station, station in enumerate(picks):
            p_raypath = isotropic_ray_trace(
                source, stations[station], velocity_model, "P"
            )
            plunge = (
                180
                * np.arccos(
                    np.sqrt(
                        p_raypath["hrz_slowness"]["x"] ** 2
                        + p_raypath["hrz_slowness"]["y"] ** 2
                    )
                    * p_raypath["velocity_model_chunk"][0]["v"]
                )
                / np.pi
            )
            trend = (
                180
                * np.arctan2(
                    p_raypath["hrz_slowness"]["x"],
                    p_raypath["hrz_slowness"]["y"],
                )
                / np.pi
            )

            if picks[station]["P_amp_snr"] > snr_cutoff:
                jj += 1
                axi.line(
                    plunge,
                    trend,
                    "o",
                    markeredgecolor="k",
                    color=bwr(0.5 * (right_hand_side[jj] + max_abs_rhs) / max_abs_rhs),
                )

        strike1, dip1, rake1, strike2, dip2, rake2 = mt_to_sdr(mt_dict[ii][event])
        axi.plane(strike1, dip1, "0.4")
        axi.plane(strike2, dip2, "0.4")
        axi.set_title(title[ii] + "\n\n")
        # reg_ax[ii].set_aspect("equal")
        rhs_comp = sqa(A_matrix * np.matrix(mt_dict[ii][event]).T)
        reg_ax[ii].plot(right_hand_side, rhs_comp, "k.")
        pearson_r = np.corrcoef(sqa(right_hand_side), sqa(rhs_comp))[0, 1]

        # x1, x2 = reg_ax[ii].get_xlim()
        # y1, y2 = reg_ax[ii].get_ylim()
        # xy_max = 1.05 * max([-x1, x2, -y1, y2])
        # reg_ax[ii].plot(
        #     [-xy_max, xy_max], [-xy_max, xy_max], "r--", zorder=-10
        # )
        reg_ax[ii].set_xlabel("R$ = %.3f$" % pearson_r)
        beachballs.append(
            beach(
                mt_dict[ii][event],
                xy=(1 + 2 * ii, 1),
                width=1.8,
                mopad_basis="NED",
                linewidth=0.5,
                facecolor="red",
            )
        )

        ax0.add_collection(beachballs[-1])

    ax0.set_xlim([0, 8])
    ax0.set_ylim([0, 2])
    ax0.axis("off")
    fig.text(0.50, 0.88, event, ha="center", fontsize=16)
    fig.savefig("PFI_MT_comparision//" + event + ".png")
# plt.show()
