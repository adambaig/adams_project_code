import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
from sklearn.cluster import AgglomerativeClustering

from sms_moment_tensor.MT_math import (
    decompose_MT,
    mt_to_sdr,
    mt_matrix_to_vector,
)

import sys

sys.path.append("..")

from read_inputs import read_wells, read_mts, read_reloc_mts

mt_catalog = (
    "C:\\Users\\adambaig\\Project\\Paramount 2-28\\MTI\\" + "relocCatalog_dec_11.csv"
)

moment_tensors = read_reloc_mts(mt_catalog)

mt_conf = {k: v for k, v in moment_tensors.items() if v["pearson r"] > 0.65}


mt_sum = np.zeros([3, 3])

for id, mt in mt_conf.items():
    mt_sum += mt["dc mt"]

for id, mt in mt_conf.items():
    if np.tensordot(mt_sum, mt["dc mt"]) < 1:
        mt_conf[id]["dc flip"] = -mt["dc mt"]
    else:
        mt_conf[id]["dc flip"] = mt["dc mt"]


n_mt = len(mt_conf)
dot_product_pairs = np.zeros([n_mt, n_mt])
for i1, (id1, mt1) in enumerate(mt_conf.items()):
    for i2, (id2, mt2) in enumerate(mt_conf.items()):
        dc1_norm = mt1["dc flip"] / np.linalg.norm(mt1["dc flip"])
        dc2_norm = mt2["dc flip"] / np.linalg.norm(mt2["dc flip"])
        dot_product_pairs[i1, i2] = np.tensordot(dc1_norm, dc2_norm)

distance_mat = abs(1 - abs(dot_product_pairs))

""  # Compute clustering
n_clusters = 3
clustering = AgglomerativeClustering(
    n_clusters=n_clusters, affinity="precomputed", linkage="complete"
).fit(distance_mat)
labels = clustering.labels_
for i_mt, (id, mt) in enumerate(mt_conf.items()):
    mt_conf[id]["label"] = labels[i_mt]

wells = read_wells()
f1, (a1, a2) = plt.subplots(1, 2)
a1.set_aspect("equal")
for wellname, well in wells.items():
    a1.plot(well["easting"], well["northing"], "0.8", zorder=3, lw=4)
    a1.plot(well["easting"], well["northing"], "0.3", zorder=3, lw=2)
patches = []
for i_cluster in range(n_clusters):
    mt_cluster = {k: v for k, v in mt_conf.items() if v["label"] == i_cluster}
    mt_avg = sum(
        [v["dc flip"] / np.linalg.norm(v["dc flip"]) for k, v in mt_cluster.items()]
    )
    p_trend, p_plunge, b_trend, b_plunge, t_trend, t_plunge = (
        [],
        [],
        [],
        [],
        [],
        [],
    )
    print("cluster " + str(i_cluster) + ": " + str(len(mt_cluster)))
    for id, mt in mt_cluster.items():
        if np.tensordot(mt_avg, mt["dc flip"]) > 0:
            mt_conf[id]["flip"] = 1
        else:
            mt_conf[id]["flip"] = -1
        flipped_dc = mt_conf[id]["flip"] * mt_conf[id]["dc flip"]
        decomp = decompose_MT(flipped_dc)
        p_trend.append(decomp["p_trend"])
        b_trend.append(decomp["b_trend"])
        t_trend.append(decomp["t_trend"])
        p_plunge.append(decomp["p_plunge"])
        b_plunge.append(decomp["b_plunge"])
        t_plunge.append(decomp["t_plunge"])
    f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = a3[0].density_contourf(
        p_plunge, p_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    cax_b = a3[1].density_contourf(
        b_plunge, b_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    cax_t = a3[2].density_contourf(
        t_plunge, t_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    # a3[0].line(p_plunge, p_trend, "k.")
    # a3[1].line(b_plunge, b_trend, "k.")
    # a3[2].line(t_plunge, t_trend, "k.")
    a3[0].set_title("P axes\n\n")
    a3[1].set_title("B axes\n\n")
    a3[2].set_title("T axes\n\n")
    a3[0].grid()
    a3[1].grid()
    a3[2].grid()
    a3[0].set_ylabel("cluster %i\n\n" % i_cluster)
    east, north = np.array(
        [(v["easting"], v["northing"]) for k, v in mt_cluster.items()]
    ).T
    (patch,) = a1.plot(east, north, ".", zorder=5)
    patches.append(patch)

a2.legend(patches, [("Cluster %i" % s) for s in range(n_clusters)], numpoints=1)

# g = open("flipped_mts.csv", "w")
# g.write("id, timestamp, easting, northing, elevation, Mw,amp,")
# g.write("error e, error n, error z,dc11, dc22, dc33, dc12, dc13, dc23,")
# g.write("r, well, stage, well_stage, confidence\n")
# for event_id, event in mt_conf.items():
#     if event["label"] in [0, 5, 6]:
#         dc11, dc22, dc33, dc12, dc13, dc23 = -event[
#             "flip"
#         ] * mt_matrix_to_vector(event["dc flip"])
#     else:
#         dc11, dc22, dc33, dc12, dc13, dc23 = event[
#             "flip"
#         ] * mt_matrix_to_vector(event["dc flip"])
#     g.write(event_id + "," + event["UTC"])
#     g.write(
#         ",%.3f,%.8e,%.1f, %.1f, %.1f"
#         % (
#             event["Mw"],
#             event["amp"],
#             event["easting"],
#             event["northing"],
#             -event["depth"],
#         )
#     )
#     g.write(
#         ",%.1f, %.1f, %.1f,"
#         % (event["error"]["x"], event["error"]["y"], event["error"]["z"])
#     )
#     g.write(6 * "%.7f," % (dc11, dc22, dc33, dc12, dc13, dc23))
#     g.write(str(event["pearson r"]))
#     g.write(
#         ","
#         + event["well"]
#         + ","
#         + event["stage"]
#         + ","
#         + str(event["well_stage"])
#     )
#     g.write(",%.3f\n" % (event["confidence"]))
# g.close()
plt.show()
