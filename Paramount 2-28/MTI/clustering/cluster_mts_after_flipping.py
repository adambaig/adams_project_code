import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
from sklearn.cluster import AgglomerativeClustering
from obspy.imaging.mopad_wrapper import beach
from sms_moment_tensor.MT_math import (
    decompose_MT,
    mt_to_sdr,
    mt_matrix_to_vector,
    general_to_DC,
)

import sys

sys.path.append("..")

from read_inputs import read_wells, read_mts

conf_thresh = 0.9
mt_catalog = "C:\\Users\\adambaig\\Project\\Paramount 2-28\\MTI\\Paramount_catalog_MT_confidence_neg_flip.csv"

moment_tensors = {
    k: v for k, v in read_mts(mt_catalog).items() if v["confidence"] > conf_thresh
}


fig, ax = mpls.subplots(3, 1, figsize=[16, 5])
ax[0]


n_mt = len(moment_tensors)
dot_product_pairs = np.zeros([n_mt, n_mt])
for i1, (id1, mt1) in enumerate(moment_tensors.items()):
    for i2, (id2, mt2) in enumerate(moment_tensors.items()):
        dc1_norm = mt1["dc mt"] / np.linalg.norm(mt1["dc mt"])
        dc2_norm = mt2["dc mt"] / np.linalg.norm(mt2["dc mt"])
        dot_product_pairs[i1, i2] = np.tensordot(dc1_norm, dc2_norm)

distance_mat = abs(1 - dot_product_pairs)


""  # Compute clustering
n_clusters = 3
clustering = AgglomerativeClustering(
    n_clusters=n_clusters, affinity="precomputed", linkage="complete"
).fit(distance_mat)
labels = clustering.labels_
for i_mt, (id, mt) in enumerate(moment_tensors.items()):
    moment_tensors[id]["label"] = labels[i_mt]
minor_ticks = np.arange(502500, 6030000, 250)
major_ticks = np.arange(502500, 6030000, 500)
wells = read_wells()
f4, a4 = plt.subplots(1, n_clusters, figsize=[16, 9], sharex=True, sharey=True)
colors = ["firebrick", "forestgreen", "royalblue", "darkgoldenrod", "indigo"]
f1, (a1, a2) = plt.subplots(1, 2)
a1.set_aspect("equal")
for wellname, well in wells.items():
    a1.plot(well["easting"], well["northing"], "0.8", zorder=3, lw=4)
    a1.plot(well["easting"], well["northing"], "0.3", zorder=3, lw=2)
    for a in a4:
        a.plot(well["easting"], well["northing"], "0.8", zorder=3, lw=4)
        a.plot(well["easting"], well["northing"], "0.3", zorder=3, lw=2)
        a.set_facecolor("lightgrey")
        a.set_xticks(minor_ticks, minor=True)
        a.set_yticks(minor_ticks, minor=True)
        a.set_xticks(major_ticks, minor=False)
        a.set_yticks(major_ticks, minor=False)

        a.grid(which="both")
        a.grid(which="minor", alpha=1)
        a.set_ylim([6025993.0, 6029780.0])
        a.set_xlim([502897.0, 505123.0])
        a.set_aspect("equal")
        a.set_yticklabels([])
        a.set_xticklabels([])
patches = []
for i_cluster in range(n_clusters):
    mt_cluster = {k: v for k, v in moment_tensors.items() if v["label"] == i_cluster}
    mt_avg = sum(
        [v["dc mt"] / np.linalg.norm(v["dc mt"]) for k, v in mt_cluster.items()]
    )
    p_trend, p_plunge, b_trend, b_plunge, t_trend, t_plunge, avg_mt = (
        [],
        [],
        [],
        [],
        [],
        [],
        np.zeros([3, 3]),
    )

    for id, mt in mt_cluster.items():
        decomp = decompose_MT(mt["dc mt"])
        p_trend.append(decomp["p_trend"])
        b_trend.append(decomp["b_trend"])
        t_trend.append(decomp["t_trend"])
        p_plunge.append(decomp["p_plunge"])
        b_plunge.append(decomp["b_plunge"])
        t_plunge.append(decomp["t_plunge"])
        avg_mt += mt["dc mt"]
    f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
    cax_p = a3[0].density_contourf(
        p_plunge, p_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    cax_b = a3[1].density_contourf(
        b_plunge, b_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )
    cax_t = a3[2].density_contourf(
        t_plunge, t_trend, measurement="lines", alpha=0.5, cmap="Greens"
    )

    # a3[0].line(p_plunge, p_trend, "k.")
    # a3[1].line(b_plunge, b_trend, "k.")
    # a3[2].line(t_plunge, t_trend, "k.")
    a3[0].set_title("P axes\n\n")
    a3[1].set_title("B axes\n\n")
    a3[2].set_title("T axes\n\n")
    a3[0].grid()
    a3[1].grid()
    a3[2].grid()
    a3[0].set_ylabel("cluster %i\n\n" % (i_cluster + 1))
    east, north = np.array(
        [(v["easting"], v["northing"]) for k, v in mt_cluster.items()]
    ).T
    (patch,) = a1.plot(east, north, ".", color=colors[i_cluster], zorder=5)
    patches.append(patch)
    a4[i_cluster].set_aspect("equal")
    a4[i_cluster].plot(east, north, ".", color=colors[i_cluster], zorder=5)
    a4[i_cluster].set_title(
        "cluster " + str(i_cluster + 1) + ": " + str(len(mt_cluster))
    )
    avg_dc = mt_matrix_to_vector(general_to_DC(avg_mt))

    f_bb, a_bb = plt.subplots()
    beachball = beach(
        avg_dc,
        xy=(0.5, 0.5),
        width=0.48,
        facecolor=colors[i_cluster],
        mopad_basis="XYZ",
        linewidth=0.25,
    )
    a_bb.set_aspect("equal")
    a_bb.add_collection(beachball)
    a_bb.set_axis_off()
a2.legend(
    patches,
    [("Cluster %i" % s) for s in range(1, n_clusters + 1)],
    numpoints=1,
)
