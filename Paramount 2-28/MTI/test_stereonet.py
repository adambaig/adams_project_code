import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import mplstereonet as mpls
from matplotlib import cm
from obspy.imaging.mopad_wrapper import beach


from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import (
    decompose_MT,
    pt_to_sdr,
    mt_matrix_to_vector,
    mt_to_sdr,
)
from sms_moment_tensor.plotting import (
    plot_regression,
    plot_beachballs,
    plot_radiation_pattern_on_map,
)
import sys

sys.path.append("..")

from read_inputs import (
    read_amp_picks,
    read_stations,
    read_velocity_model,
    read_events,
    read_polarities,
    read_old_MTs,
    read_events_from_Do,
)

p_trend, p_plunge, b_trend, b_plunge, t_trend, t_plunge = (
    [],
    [],
    [],
    [],
    [],
    [],
)

strikes, dips, rakes = [], [], []
polarities = read_polarities()
events = read_events()
stations = read_stations()
velocity_model = read_velocity_model(mode="read")
bwr = cm.get_cmap("bwr")
test_events = {k: v for k, v in read_events().items() if k == "471956"}
# test_events = {k: v for k, v in events.items() if k in read_events_from_Do()}
snr_cutoff = 1
dc_rsq_vec, dc_MT_vec, num_picks_vec, cn_vec = [], [], [], []
for event in test_events:
    amp_picks = read_amp_picks(event)
    inversion_matrix = []
    right_hand_side = []
    n_picks = 0
    source = {
        "e": events[event]["easting"],
        "n": events[event]["northing"],
        "z": -events[event]["depth"],
    }
    if not (amp_picks is None):
        max_abs_amp = 0
        for station in {k: v for k, v in amp_picks.items() if "PM" in k}:
            if amp_picks[station]["P_amp_snr"] > snr_cutoff:
                n_picks += 1
                p_raypath = isotropic_ray_trace(
                    source, stations[station], velocity_model, "P"
                )
                s_raypath = isotropic_ray_trace(
                    source, stations[station], velocity_model, "S"
                )
                cos_incoming = np.sqrt(
                    1.0
                    - (
                        p_raypath["hrz_slowness"]["e"] ** 2
                        + p_raypath["hrz_slowness"]["n"] ** 2
                    )
                    * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
                )
                inversion_matrix.append(inversion_matrix_row(p_raypath, "P"))
                # reverse sign on amplitude since up on the geophone triggers downward motion
                right_hand_side.append(
                    back_project_amplitude(
                        p_raypath,
                        s_raypath,
                        -  # polarities[event]
                        # *
                        amp_picks[station]["P_amp"] / cos_incoming,
                        "P",
                        Q=500,
                    )
                )
                if abs(amp_picks[station]["P_amp"]) > max_abs_amp:
                    max_abs_amp = abs(amp_picks[station]["P_amp"])

        if n_picks > 7:
            moment_tensor = solve_moment_tensor(inversion_matrix, right_hand_side)

            mt_dc = mt_matrix_to_vector(moment_tensor["dc"])
            dc_MT_vec.append(mt_dc)
            dc_rsq_vec.append(moment_tensor["rsq_dc"])
            num_picks_vec.append(n_picks)
            cn_vec.append(moment_tensor["condition_number_dev"])
            decomp = decompose_MT(mt_dc)
            max_abs_rhs = max(abs(np.array(right_hand_side)))
            f1, a1 = mpls.subplots(1)
            ii = -1
            for station in {k: v for k, v in amp_picks.items() if "PM" in k}:
                p_raypath = isotropic_ray_trace(
                    source, stations[station], velocity_model, "P"
                )
                plunge = (
                    180
                    * np.arccos(
                        np.sqrt(
                            p_raypath["hrz_slowness"]["e"] ** 2
                            + p_raypath["hrz_slowness"]["n"] ** 2
                        )
                        * p_raypath["velocity_model_chunk"][0]["v"]
                    )
                    / np.pi
                )
                trend = (
                    180
                    * np.arctan2(
                        p_raypath["hrz_slowness"]["e"],
                        p_raypath["hrz_slowness"]["n"],
                    )
                    / np.pi
                )
                if amp_picks[station]["P_amp_snr"] > snr_cutoff:
                    ii += 1
                    a1.line(
                        plunge,
                        trend,
                        "o",
                        markeredgecolor="k",
                        color=bwr(
                            0.5 * (right_hand_side[ii] + max_abs_rhs) / max_abs_rhs
                        ),
                    )
            strike1, dip1, rake1, strike2, dip2, rake2 = mt_to_sdr(mt_dc)
            if moment_tensor["rsq_dc"] > 0.3:
                p_trend.append(decomp["p_trend"])
                b_trend.append(decomp["b_trend"])
                t_trend.append(decomp["t_trend"])
                p_plunge.append(decomp["p_plunge"])
                b_plunge.append(decomp["b_plunge"])
                t_plunge.append(decomp["t_plunge"])
                strikes.append(strike1)
                strikes.append(strike2)
                dips.append(dip1)
                dips.append(dip2)
                rakes.append(rake1)
                rakes.append(rake2)
            a1.plane(strike1, dip1, "0.4")
            a1.plane(strike2, dip2, "0.4")
            f1.text(
                0.1,
                0.9,
                ("R$^{2}$ = %.2f" % moment_tensor["rsq_dc"]),
                ha="center",
            )
plt.show()
# nslc_tags = [
#     "PM.01..RTP",
#     "PM.01..RTV",
#     "PM.01..RTH",
#     "PM.02..RTP",
#     "PM.02..RTV",
#     "PM.02..RTH",
#     "PM.03..RTP",
#     "PM.03..RTV",
#     "PM.03..RTH",
#     "PM.04..RTP",
#     "PM.04..RTV",
#     "PM.04..RTH",
#     "PM.05..RTP",
#     "PM.05..RTV",
#     "PM.05..RTH",
# ]


# fig_reg, ax_reg = plt.subplots()
#
# plot_regression(
#     right_hand_side, mt_dc, inversion_matrix, nslc_tags, ax_reg, fig_reg
# )
#
#
# plot_beachballs(moment_tensor)
# nslcs = {}
# for station in stations:
#     nslcs[station + "..P"] = stations[station]
# event_loc = {}
# event_loc["e"] = events[event]["easting"]
# event_loc["n"] = events[event]["northing"]
#
# ax = plot_radiation_pattern_on_map(right_hand_side, nslcs, event_loc)
# fig = plt.figure()
# fig.axes.append(ax[0])
#
# plt.show()
