import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
from obspy import read, UTCDateTime, Trace
from obspy.core.trace import Stats
from obspy.signal import filter
from obspy.signal.invsim import corn_freq_2_paz

from scipy.fftpack import fft, ifft, fftfreq

from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.interface_scattering import (
    P_stack_transmission,
    SV_stack_transmission,
    SH_stack_transmission,
)
from moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
import sys

sys.path.append("..")

from read_inputs import (
    read_velocity_model,
    read_stations,
    read_events_from_Do,
    read_waveform,
    read_picks,
)

catalog = read_events_from_Do()
stations = read_stations()
velocity_model = read_velocity_model(mode="read")

Q = {"P": 60, "S": 60}
explosion = {
    "moment_tensor": np.matrix([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]),
    "stress_drop": 3.0e5,
}
xcorr_time = np.arange(-2, 2.001, 0.004)
paz_10hz = corn_freq_2_paz(10.0, damp=np.sqrt(0.5))
snr_threshold = 1.5
bandpass_hf = 120
bandpass_lf = 2
sample_rate = 250
statics = {}


def sqa(x):
    return np.squeeze(np.array(x))


def createTraceStats(network, n1, o1, d1, component):
    # make a stats file for a seed trace
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


f = open("shift_per_event_best73_location.csv", "w")

fig, ax = plt.subplots()

n_stations = len(stations)
for ii, event in enumerate(catalog):
    picks = read_picks(event)
    st = read_waveform(event, UTCDateTime(picks["t0"]), 2, raw=True)
    n_points = st[0].stats.npts
    time_series = np.arange(n_points) * st[0].stats.delta
    freq = fftfreq(len(time_series) * 2 - 1, time_series[1])
    tsfreq = fftfreq(len(time_series), time_series[1])
    jj = -1
    corr_array = np.zeros([2 * n_points - 1, n_stations])
    dat = np.zeros([n_points, n_stations])
    dat_align = np.zeros([n_points, n_stations])
    syn = np.zeros([n_points, n_stations])
    amp = -1.0e11 * np.ones(n_stations)
    snr = np.ones(n_stations)
    easting, northing = np.zeros(n_stations), np.zeros(n_stations)
    kk = 0
    rhs, Amat = [], []
    if picks is None:
        print("picks needed for event " + event)
    else:
        p_raypath = [object() for _ in range(n_stations)]
        s_raypath = [object() for _ in range(n_stations)]
        xcorr_lags = np.zeros(n_stations)
        if ii == 0:
            f.write("event time")
            for trace in st:
                id = trace.get_id().split(".VC.")[0]
                f.write("," + id)
                statics[id] = -999.25 * np.ones(len(catalog))
            f.write("\n")
        f.write(str(catalog[event]["UTC"]))
        for trace in st:
            jj += 1
            id = trace.get_id().split(".VC.")[0]
            source = {
                "x": catalog[event]["easting"],
                "y": catalog[event]["northing"],
                "z": catalog[event]["depth"],
            }
            p_raypath[jj] = isotropic_ray_trace(
                source, stations[id], velocity_model, "P"
            )
            s_raypath[jj] = isotropic_ray_trace(
                source, stations[id], velocity_model, "S"
            )

            explosion["moment_magnitude"] = catalog[event]["Mw"]
            explosion["x"] = source["x"]
            explosion["y"] = source["y"]
            explosion["z"] = source["z"]
            explosive_response = get_response(
                p_raypath[jj], s_raypath[jj], explosion, stations[id], Q, time_series, 0
            )
            normalized_response = explosive_response["d"] / np.sqrt(
                np.correlate(explosive_response["d"], explosive_response["d"])
            )
            expl_channel_object = {
                "component": "Z",
                "channel": "CPZ",
                "station": stations[id],
                "network": "CV",
                "location": "04",
            }
            expl_stats = createTraceStats(
                "CV",
                trace.stats.npts,
                trace.stats.starttime,
                trace.stats.delta,
                expl_channel_object,
            )
            expl_trace = Trace(data=normalized_response, header=expl_stats)
            expl_trace.simulate(paz_simulate=paz_10hz)
            trace = trace.detrend()
            xcorr = np.correlate(trace.data, expl_trace.data, mode="full")

            if id in picks:
                t_residual = (
                    picks[id]["P"]
                    - UTCDateTime(picks["t0"]).timestamp
                    - p_raypath[jj]["traveltime"]
                )
                pick_residual = picks[id]["P"] - UTCDateTime(picks["t0"]).timestamp
                syn[:, jj] = np.real(
                    ifft(
                        fft(
                            filter.bandpass(
                                expl_trace.data, bandpass_lf, bandpass_hf, sample_rate
                            )
                        )
                        * np.exp(2.0j * np.pi * (p_raypath[jj]["traveltime"]) * tsfreq)
                    )
                )

                dat[:, jj] = np.real(
                    ifft(
                        fft(
                            filter.bandpass(
                                trace.data, bandpass_lf, bandpass_hf, sample_rate
                            )
                        )
                        * np.exp(2.0j * np.pi * (picks[id]["P"] - picks["t0"]) * tsfreq)
                    )
                )
                xshift = np.real(
                    ifft(fft(xcorr) * np.exp(2.0j * np.pi * (t_residual) * freq))
                )

                xcorr_lags[jj] = xcorr_time[
                    np.argmax(abs(xshift[n_points - 100 : n_points + 100]))
                    + n_points
                    - 100
                ]
                f.write(",%.4e" % xcorr_lags[jj])
                statics[id][ii] = xcorr_lags[jj]
                corr_array[:, jj] = xshift / max(abs(xcorr))
                i_max = (
                    np.argsort(abs(corr_array[n_points - 20 : n_points + 20, jj]))[-1]
                    + n_points
                )
                amp[jj] = corr_array[i_max, jj]
                snr[jj] = abs(amp[jj]) / np.sqrt(
                    np.var(xcorr[n_points // 5 : 3 * n_points // 5])
                )
                if np.isnan(snr[jj]):
                    snr[jj] = 0.0
                if snr[jj] > snr_threshold:
                    kk += 1
                    rhs.append(
                        back_project_amplitude(
                            p_raypath[jj], s_raypath[jj], amp[jj], "P"
                        )
                    )
                    Amat.append(inversion_matrix_row(p_raypath[jj], "P"))
                dat_align[:, jj] = np.real(
                    ifft(
                        fft(
                            filter.bandpass(
                                trace.data, bandpass_lf, bandpass_hf, sample_rate
                            )
                        )
                        * np.exp(
                            2.0j
                            * np.pi
                            * (
                                picks[id]["P"]
                                - catalog[event]["UTC"].timestamp
                                + xcorr_lags[jj]
                            )
                            * tsfreq
                        )
                    )
                )
            else:
                f.write(",-999.25")
            # ax.plot(xcorr_lags, "k", alpha=0.2)
        f.write("\n")

plt.pcolor(corr_array)
plt.show()
