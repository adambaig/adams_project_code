import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
from obspy import read, UTCDateTime, Trace
from obspy.core.trace import Stats
from obspy.signal import filter
from obspy.signal.invsim import corn_freq_2_paz

from scipy.fftpack import fft, ifft, fftfreq
from scipy.signal import hilbert
from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_ray_modelling.waveform_model import get_response
from sms_ray_modelling.interface_scattering import (
    P_stack_transmission,
    SV_stack_transmission,
    SH_stack_transmission,
)
from moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
import sys

sys.path.append("..")

from read_inputs import (
    read_velocity_model,
    read_stations,
    read_events_from_Do,
    read_events,
    read_waveform,
    read_picks,
)


# catalog = read_events_from_Do()
catalog = {k: v for k, v in read_events().items() if v["Mw"] > -8}


stations = read_stations()
velocity_model = read_velocity_model(mode="read")

Q = {"P": 250, "S": 100}
explosion = {
    "moment_tensor": np.matrix([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]),
    "stress_drop": 3.0e5,
}
xcorr_time = np.arange(-2, 2.001, 0.004)
paz_10hz = corn_freq_2_paz(10.0, damp=np.sqrt(0.5))
snr_threshold = 1.5
bandpass_hf = 120
bandpass_lf = 2
sample_rate = 250
n_stations = len(stations)
plot_out = False


def sqa(x):
    return np.squeeze(np.array(x))


def createTraceStats(network, n1, o1, d1, component):
    # make a stats file for a seed trace
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


f = open("shift_per_event_best58_location.csv", "w")

for ii, event in enumerate(catalog):
    picks = read_picks(event)
    st = read_waveform(event, UTCDateTime(picks["t0"]), 2, raw=True)
    n_points = st[0].stats.npts
    if ii == 0:
        f.write("event id,event time")
        for trace in st:
            f.write("," + trace.get_id().split(".VC.")[0])
        f.write("\n")
    if plot_out:
        fig0 = plt.figure(figsize=[16, 9])
        ax0 = fig0.add_axes([0.05, 0.1, 0.25, 0.8])
        ax1 = fig0.add_axes([0.35, 0.1, 0.25, 0.8])
        ax2 = fig0.add_axes([0.65, 0.1, 0.25, 0.8])
    dat = np.zeros([n_points, n_stations])
    norm_dat = np.zeros([n_points, n_stations])
    xcor_dat = np.zeros([n_points, n_stations])
    time_series = np.arange(n_points) / sample_rate
    tsfreq = fftfreq(len(time_series), time_series[1])
    winfreq = fftfreq(100, time_series[1])
    if picks is None:
        print("picks needed for event " + event)
    else:
        p_raypath = [object() for _ in range(n_stations)]
        s_raypath = [object() for _ in range(n_stations)]
        trace = st[0]
        jj = -1
        f.write(event + "," + str(catalog[event]["UTC"]))
        for trace in st:
            expl_channel_object = {
                "component": "Z",
                "channel": "CPZ",
                "station": "dummy",
                "network": "PM",
                "location": "00",
            }
            expl_stats = createTraceStats(
                "PM",
                trace.stats.npts,
                trace.stats.starttime,
                trace.stats.delta,
                expl_channel_object,
            )
            jj += 1
            id = trace.get_id().split(".VC.")[0]

            source = {
                "x": catalog[event]["easting"],
                "y": catalog[event]["northing"],
                "z": catalog[event]["depth"],
            }
            p_raypath[jj] = isotropic_ray_trace(
                source, stations[id], velocity_model, "P"
            )
            s_raypath[jj] = isotropic_ray_trace(
                source, stations[id], velocity_model, "S"
            )
            explosion["moment_magnitude"] = catalog[event]["Mw"]
            explosion["x"] = source["x"]
            explosion["y"] = source["y"]
            explosion["z"] = source["z"]

            explosive_response = np.real(
                ifft(
                    fft(
                        get_response(
                            p_raypath[jj],
                            s_raypath[jj],
                            explosion,
                            stations[id],
                            Q,
                            time_series,
                            0,
                        )["d"]
                    )
                    * np.exp(
                        2.0j * np.pi * (p_raypath[jj]["traveltime"] - 1.0) * tsfreq
                    )
                )
            )

            normalized_response = explosive_response / np.sqrt(
                np.correlate(explosive_response, explosive_response)
            )
            expl_trace = Trace(data=normalized_response, header=expl_stats)
            expl_trace.simulate(paz_simulate=paz_10hz)
            predicted_onset = picks[id]["P"]
            trace.detrend()
            dat[:, jj] = np.real(
                ifft(
                    fft(trace.data)
                    * np.exp(
                        2.0j
                        * np.pi
                        * (
                            predicted_onset
                            - UTCDateTime(catalog[event]["UTC"]).timestamp
                            - 1
                        )
                        * tsfreq
                    )
                )
            )
            xcor_dat[:, jj] = np.correlate(
                filter.bandpass(
                    dat[:, jj], bandpass_lf, bandpass_hf, sample_rate, zerophase=True
                ),
                filter.bandpass(
                    normalized_response,
                    bandpass_lf,
                    bandpass_hf,
                    sample_rate,
                    zerophase=True,
                ),
                "same",
            )
            if plot_out:
                ax0.plot(
                    time_series - 1, jj + 1 + dat[:, jj] / max(abs(dat[:, jj])), "0.2"
                )

                ax2.plot(
                    time_series - time_series[-1] / 2,
                    jj + 1 + xcor_dat[:, jj] / max(abs(xcor_dat[:, jj])),
                    "0.2",
                    zorder=-1,
                )
            norm_dat[:, jj] = xcor_dat[:, jj] / max(abs(xcor_dat[:, jj]))
        fig = plt.figure()
        ax1 = fig.add_axes([0.1, 0.4, 0.8, 0.55])
        ax2 = fig.add_axes([0.1, 0.1, 0.8, 0.25])
        ax1.pcolor(
            time_series, range(n_stations), norm_dat.T, vmax=1, vmin=-1, cmap="bwr"
        )
        ax1.set_xlim([0, 2])
        ax1.set_xticklabels("")
        envelopes = hilbert(norm_dat, axis=0)
        envelopes *= np.conj(envelopes)
        i_x_max = np.argmax(np.sum(np.real(envelopes), axis=-1))
        statics, amp, snr = (
            np.zeros(n_stations),
            np.zeros(n_stations),
            np.zeros(n_stations),
        )
        for i_station in range(n_stations):
            i_max = (
                np.argmax(np.real(envelopes[i_x_max - 20 : i_x_max + 20, i_station]))
                - 20
                + i_x_max
            )
            statics[i_station] = time_series[i_max]
            amp[i_station] = xcor_dat[i_max, i_station]
            snr[i_station] = np.sqrt(
                np.var(xcor_dat[i_max - 100 : i_max - 10, i_station])
            )
            ax1.plot(
                [statics[i_station], statics[i_station]],
                [i_station, i_station + 1],
                "0.3",
            )
            f.write(",%.5f" % (statics[i_station] - time_series[i_x_max]))
        f.write("\n")
        ax2.plot(time_series, np.sum(np.real(envelopes), axis=-1))
        ax2.set_xlim([0, 2])
        ax2.set_xlabel("time (s)")

        fig.savefig("xcor_realigned//" + event + ".png")

if plot_out:
    plt.show()
