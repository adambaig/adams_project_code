import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import mplstereonet as mpls
import numpy as np
from sms_moment_tensor.MT_math import (
    decompose_MT,
    mt_to_sdr,
    mt_vector_to_matrix,
)
import sys

sys.path.append("..")

from read_inputs import read_wells, read_mts

mt_catalog = (
    "C:\\Users\\adambaig\\Project\\Paramount 2-28\\MTI\\"
    + "Paramount_catalog_MT_confidence_neg.csv"
)

moment_tensors = read_mts(mt_catalog)

p_trend, p_plunge, b_trend, b_plunge, t_trend, t_plunge = (
    [],
    [],
    [],
    [],
    [],
    [],
)


mt_wells = np.unique(np.array(v["well"] for k, v in moment_tensor.items()))

f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
for event_id, mt in {
    k: v for k, v in moment_tensors.items() if v["confidence"] > 0.999
}.items():
    decomp = decompose_MT(mt["dc mt"])
    p_trend.append(decomp["p_trend"])
    b_trend.append(decomp["b_trend"])
    t_trend.append(decomp["t_trend"])
    p_plunge.append(decomp["p_plunge"])
    b_plunge.append(decomp["b_plunge"])
    t_plunge.append(decomp["t_plunge"])

cax_p = a3[0].density_contourf(
    p_plunge, p_trend, measurement="lines", alpha=0.5, cmap="Greens"
)
cax_b = a3[1].density_contourf(
    b_plunge, b_trend, measurement="lines", alpha=0.5, cmap="Greens"
)
cax_t = a3[2].density_contourf(
    t_plunge, t_trend, measurement="lines", alpha=0.5, cmap="Greens"
)
cax_p = a3[0].line(p_plunge, p_trend, "k.")
cax_b = a3[1].line(b_plunge, b_trend, "k.")
cax_t = a3[2].line(t_plunge, t_trend, "k.")

# a3[0].line(p_plunge, p_trend, "k.")
# a3[1].line(b_plunge, b_trend, "k.")
# a3[2].line(t_plunge, t_trend, "k.")
a3[0].set_title("P axes\n\n")
a3[1].set_title("B axes\n\n")
a3[2].set_title("T axes\n\n")
a3[0].grid()
a3[1].grid()
a3[2].grid()


test = decompose_MT(moment_tensors["851197"]["dc mt"])

f, a = mpls.subplots()
a.line(test["p_plunge"], test["p_trend"], ".")
a.grid()
