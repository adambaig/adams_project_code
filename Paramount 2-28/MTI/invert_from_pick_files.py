import matplotlib

matplotlib.use("Qt5agg")

import numpy as np
import matplotlib.pyplot as plt
import mplstereonet as mpls
from matplotlib import cm
from obspy.imaging.mopad_wrapper import beach


from sms_ray_modelling.raytrace import isotropic_ray_trace
from sms_moment_tensor.moment_tensor_inversion import (
    back_project_amplitude,
    inversion_matrix_row,
    solve_moment_tensor,
)
from sms_moment_tensor.MT_math import (
    decompose_MT,
    pt_to_sdr,
    mt_matrix_to_vector,
)
from sms_moment_tensor.plotting import (
    plot_regression,
    plot_beachballs,
    plot_radiation_pattern_on_map,
)
import sys

sys.path.append("..")

from read_inputs import (
    read_amp_picks,
    read_stations,
    read_velocity_model,
    read_events,
    read_polarities,
    read_old_MTs,
)

p_trend, p_plunge, b_trend, b_plunge, t_trend, t_plunge = (
    [],
    [],
    [],
    [],
    [],
    [],
)

strikes, dips, rakes = [], [], []
polarities = read_polarities()
events = read_events()
stations = read_stations()
velocity_model = read_velocity_model(mode="read")
bwr = cm.get_cmap("bwr")
test_events = {k: v for k, v in read_events().items() if k == "448348"}
test_events = {k: v for k, v in events.items() if k in read_events()}
snr_cutoff = 1
fig, ax = mpls.subplots(1, 3, figsize=[16, 5])
dc_r_vec, dc_MT_vec, num_picks_vec, cn_vec = [], [], [], []
f_out = open("moment_tensors_15_reinvert.csv", "w")
f_out.write("event_id, UTCDateTime, strike1,dip1,rake1,strike2,dip2,rake2\n")
for event in test_events:
    amp_picks = read_amp_picks(event)
    inversion_matrix = []
    right_hand_side = []
    n_picks = 0
    source = {
        "e": events[event]["easting"],
        "n": events[event]["northing"],
        "z": -events[event]["depth"],
    }
    if not (amp_picks is None):
        max_abs_amp = 0
        for station in {k: v for k, v in amp_picks.items() if "PM" in k}:
            if amp_picks[station]["P_amp_snr"] > snr_cutoff:
                n_picks += 1
                p_raypath = isotropic_ray_trace(
                    source, stations[station], velocity_model, "P"
                )
                s_raypath = isotropic_ray_trace(
                    source, stations[station], velocity_model, "S"
                )
                cos_incoming = np.sqrt(
                    1.0
                    - (
                        p_raypath["hrz_slowness"]["e"] ** 2
                        + p_raypath["hrz_slowness"]["n"] ** 2
                    )
                    * p_raypath["velocity_model_chunk"][-1]["v"] ** 2
                )
                inversion_matrix.append(inversion_matrix_row(p_raypath, "P"))
                # reverse sign on amplitude since up on the geophone triggers downward motion
                right_hand_side.append(
                    back_project_amplitude(
                        p_raypath,
                        s_raypath,
                        -  # polarities[event]
                        # *
                        amp_picks[station]["P_amp"] / cos_incoming,
                        "P",
                        Q=500,
                    )
                )
                if abs(amp_picks[station]["P_amp"]) > max_abs_amp:
                    max_abs_amp = abs(amp_picks[station]["P_amp"])
        if n_picks > 7:
            moment_tensor = solve_moment_tensor(inversion_matrix, right_hand_side)
            mt_dc = mt_matrix_to_vector(moment_tensor["dc"])
            dc_MT_vec.append(mt_dc)
            dc_r_vec.append(moment_tensor["r_dc"])
            num_picks_vec.append(n_picks)
            cn_vec.append(moment_tensor["condition_number_dev"])
            decomp = decompose_MT(mt_dc)
            max_abs_rhs = max(abs(np.array(right_hand_side)))
            if moment_tensor["r_dc"] > 0.3:
                ax[0].line(decomp["p_plunge"], decomp["p_trend"], "k.")
                ax[1].line(decomp["b_plunge"], decomp["b_trend"], "k.")
                ax[2].line(decomp["t_plunge"], decomp["t_trend"], "k.")
            f1, a1 = mpls.subplots(1)
            ii = -1
            for station in {k: v for k, v in amp_picks.items() if "PM" in k}:
                p_raypath = isotropic_ray_trace(
                    source, stations[station], velocity_model, "P"
                )
                stations[station]["n"] - source["n"]
                plunge = (
                    180
                    * np.arccos(
                        np.sqrt(
                            p_raypath["hrz_slowness"]["e"] ** 2
                            + p_raypath["hrz_slowness"]["n"] ** 2
                        )
                        * p_raypath["velocity_model_chunk"][0]["v"]
                    )
                    / np.pi
                )
                trend = (
                    180
                    * np.arctan2(
                        -p_raypath["hrz_slowness"]["e"],
                        -p_raypath["hrz_slowness"]["n"],
                    )
                    / np.pi
                )
                if amp_picks[station]["P_amp_snr"] > snr_cutoff:
                    ii += 1
                    a1.line(
                        plunge,
                        trend,
                        "o",
                        markeredgecolor="k",
                        color=bwr(
                            0.5 * (right_hand_side[ii] + max_abs_rhs) / max_abs_rhs
                        ),
                    )
            strike1, dip1, rake1, strike2, dip2, rake2 = pt_to_sdr(
                decomp["p_trend"],
                decomp["p_plunge"],
                decomp["t_trend"],
                decomp["t_plunge"],
            )
            if moment_tensor["r_dc"] > 0.3:
                f_out.write(event + "," + events[event]["UTC"])
                f_out.write(
                    ",%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n"
                    % (
                        strike1,
                        dip1,
                        rake1,
                        strike2,
                        dip2,
                        rake2,
                        moment_tensor["r_dc"],
                    )
                )
                p_trend.append(decomp["p_trend"])
                b_trend.append(decomp["b_trend"])
                t_trend.append(decomp["t_trend"])
                p_plunge.append(decomp["p_plunge"])
                b_plunge.append(decomp["b_plunge"])
                t_plunge.append(decomp["t_plunge"])
                strikes.append(strike1)
                strikes.append(strike2)
                dips.append(dip1)
                dips.append(dip2)
                rakes.append(rake1)
                rakes.append(rake2)
            a1.plane(strike1, dip1, "0.4")
            a1.plane(strike2, dip2, "0.4")
            f1.text(
                0.1,
                0.9,
                ("R$^{2}$ = %.2f" % moment_tensor["r_dc"]),
                ha="center",
            )
            f1.savefig("beachball_QC//beachball" + event.zfill(8) + ".png")
            plt.show()
f_out.close()
ax[0].set_title("P axes\n\n")
ax[1].set_title("B axes\n\n")
ax[2].set_title("T axes\n\n")
f2, a2 = plt.subplots()
a2.scatter(num_picks_vec, dc_r_vec, c=cn_vec)

f3, a3 = mpls.subplots(1, 3, figsize=[16, 5])
cax_p = a3[0].density_contourf(
    p_plunge, p_trend, measurement="lines", alpha=0.5, cmap="Greens"
)
cax_b = a3[1].density_contourf(
    b_plunge, b_trend, measurement="lines", alpha=0.5, cmap="Greens"
)
cax_t = a3[2].density_contourf(
    t_plunge, t_trend, measurement="lines", alpha=0.5, cmap="Greens"
)
a3[0].line(p_plunge, p_trend, "k.")
a3[1].line(b_plunge, b_trend, "k.")
a3[2].line(t_plunge, t_trend, "k.")
a3[0].set_title("P axes\n\n")
a3[1].set_title("B axes\n\n")
a3[2].set_title("T axes\n\n")
a3[0].grid()
a3[1].grid()
a3[2].grid()

f4, a4 = mpls.subplots()
a4.density_contourf(strikes, dips, n_measurements="poles", alpha=0.5, cmap="Reds")
a4.pole(strikes, dips, "k.")
a4.set_title("poles of both fault planes\n\n")
a4.grid()
f5 = plt.figure()
a5 = f5.add_subplot(111, projection="polar")
rakes = np.mod(np.array(rakes), 360)
bin_number = 30
bins = np.linspace(0, 2 * np.pi, bin_number)
n_rake, _, _ = plt.hist(rakes * np.pi / 180.0, bins, visible=False)
width = 2 * np.pi / bin_number
bars = a5.bar(bins[:-1], n_rake, width=width, facecolor="royalblue")
lines, labels = plt.thetagrids(
    [0, 90, 180, 270],
    [
        "            Left-lateral\n            strike-slip",
        "Thrust",
        "Right-lateral            \nstrike-slip           ",
        "Normal",
    ],
)
a5.set_title("Rakes of both fault planes\n\n")

plt.show()
