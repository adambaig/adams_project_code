import numpy as np
from sms_moment_tensor.MT_math import fracture_planes_to_mt, mt_to_sdr, sdr_to_mt

infile = "moment_tensors_polarity.csv"
outfile = infile.replace(".csv", "_fixed.csv")

f = open(infile)
g = open(outfile, "w")
g.write(f.readline())
lines = f.readlines()
f.close()
n_events = len(lines)
in_strike1, out_strike1 = np.zeros(n_events), np.zeros(n_events)
in_dip1, out_dip1 = np.zeros(n_events), np.zeros(n_events)
in_rake1, out_rake1 = np.zeros(n_events), np.zeros(n_events)
in_strike2, out_strike2 = np.zeros(n_events), np.zeros(n_events)
in_dip2, out_dip2 = np.zeros(n_events), np.zeros(n_events)
in_rake2, out_rake2 = np.zeros(n_events), np.zeros(n_events)

for i_event, line in enumerate(lines):
    lspl = line.split(",")
    event_id, utc_dt = lspl[:2]
    in_strike1[i_event], in_dip1[i_event], in_rake1[i_event] = [
        float(s) for s in lspl[2:5]
    ]
    in_strike2[i_event], in_dip2[i_event], in_rake2[i_event], rsq = [
        float(s) for s in lspl[5:9]
    ]
    is_thrust = True
    if in_rake1[i_event] > 0:
        is_thrust = True
    mt = fracture_planes_to_mt(
        in_strike1[i_event],
        in_dip1[i_event],
        in_strike2[i_event],
        in_dip2[i_event],
        is_thrust,
    )
    (
        out_strike1[i_event],
        out_dip1[i_event],
        out_rake1[i_event],
        out_strike2[i_event],
        out_dip2[i_event],
        out_rake2[i_event],
    ) = mt_to_sdr(mt)


strike1, strike2, dip1, dip2 = in_strike1[0], in_strike2[0], in_dip1[0], in_dip2[0]
