# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.

Scripts to do stress inversions and projections of fault and MT stuff

"""

import numpy as np
from numpy import sin, cos, pi
import matplotlib.pyplot as plt
import mplstereonet as mpls

d2r = pi / 180.0
r2d = 1.0 / d2r


def sqa(x):
    return np.squeeze(np.array(x))


def unit_slip_vector(strike, dip, rake):
    # assume strike, dip, and rake are in degrees
    # NED coordinates a la Aki and Richards
    st = d2r * strike
    dp = d2r * dip
    rk = d2r * rake
    return [
        cos(rk) * cos(st) + cos(dp) * sin(rk) * sin(st),
        cos(rk) * sin(st) - cos(dp) * sin(rk) * cos(st),
        -sin(rk) * sin(dp),
    ]


def unit_plane_normal(strike, dip):
    # assume strike and dip are in degrees
    # NED coordinates a la Aki and Richards
    st = d2r * strike
    dp = d2r * dip
    return [-sin(dp) * sin(st), sin(dp) * cos(st), -cos(dp)]


def unit_vector_2_trend_plunge(u):
    if u[2] < 0:
        u = -u
    trend = np.arctan2(u[1], u[0]) * 180.0 / np.pi
    plunge = np.arctan(u[2] / np.sqrt(u[0] * u[0] + u[1] * u[1])) * 180.0 / np.pi
    return [trend, plunge]


def Michael_stress_inversion(strikes, dips, rakes):
    # invert for stress using Michael (1984) method
    # returns a 3X3 (relative) stress tensor with zero trace
    # something is introducing a -ve sign that needs to be compenstated for
    # doing this returns the correct stress for the Angelier (1979) dataset
    n_events = len(strikes)
    RHS = np.matrix(np.zeros(3 * n_events))
    inversion_matrix = np.matrix(np.zeros([3 * n_events, 5]))
    for i_event in range(n_events):
        n1, n2, n3 = unit_plane_normal(strikes[i_event], dips[i_event])
        s1, s2, s3 = unit_slip_vector(strikes[i_event], dips[i_event], rakes[i_event])
        i1 = 3 * i_event
        i2 = 3 * i_event + 1
        i3 = 3 * i_event + 2
        inversion_matrix[i1, 0] = n1 * (n2 * n2 + 2 * n3 * n3)
        inversion_matrix[i1, 1] = n2 * (1.0 - 2.0 * n1 * n1)
        inversion_matrix[i1, 2] = n3 * (1.0 - 2.0 * n1 * n1)
        inversion_matrix[i1, 3] = n1 * (-n2 * n2 + n3 * n3)
        inversion_matrix[i1, 4] = -2.0 * n1 * n2 * n3
        inversion_matrix[i2, 0] = n2 * (-n1 * n1 + n3 * n3)
        inversion_matrix[i2, 1] = n1 * (1.0 - 2.0 * n2 * n2)
        inversion_matrix[i2, 2] = -2.0 * n1 * n2 * n3
        inversion_matrix[i2, 3] = n2 * (n1 * n1 + 2.0 * n3 * n3)
        inversion_matrix[i2, 4] = n3 * (1.0 - 2.0 * n2 * n2)
        inversion_matrix[i3, 0] = n3 * (-2.0 * n1 * n1 - n2 * n2)
        inversion_matrix[i3, 1] = -2.0 * n1 * n2 * n3
        inversion_matrix[i3, 2] = n1 * (1.0 - 2.0 * n3 * n3)
        inversion_matrix[i3, 3] = n3 * (-n1 * n1 - 2.0 * n2 * n2)
        inversion_matrix[i3, 4] = n2 * (1.0 - 2.0 * n3 * n3)
        RHS[0, i1] = s1
        RHS[0, i2] = s2
        RHS[0, i3] = s3
    s11, s12, s13, s22, s23 = sqa(
        np.linalg.inv(inversion_matrix.T * inversion_matrix)
        * inversion_matrix.T
        * RHS.T
    )
    s33 = -s11 - s22  # set trace to zero
    return -1 * np.matrix([[s11, s12, s13], [s12, s22, s23], [s13, s23, s33]])


def stereonet_plot_stress_tensor(stress_tensor):
    eigvals, eigvecs = np.linalg.eig(stress_tensor)
    i_sort = np.argsort(eigvals)
    s3_axis = eigvecs[:, i_sort[0]]
    s2_axis = eigvecs[:, i_sort[1]]
    s1_axis = eigvecs[:, i_sort[2]]
    R = (eigvals[i_sort[2]] - eigvals[i_sort[1]]) / (
        eigvals[i_sort[2]] - eigvals[i_sort[0]]
    )

    fig = plt.figure()
    ax = fig.add_subplot(111, projection="stereonet")
    s1_trend, s1_plunge = unit_vector_2_trend_plunge(sqa(s1_axis))
    s2_trend, s2_plunge = unit_vector_2_trend_plunge(sqa(s2_axis))
    s3_trend, s3_plunge = unit_vector_2_trend_plunge(sqa(s3_axis))

    ax.line(s1_plunge, s1_trend, "o", color="firebrick")
    ax.line(s2_plunge, s2_trend, "o", color="forestgreen")
    ax.line(s3_plunge, s3_trend, "o", color="royalblue")
    ax.set_title("R=" + str(R))
    return fig
