import numpy as np
from numpy import pi, sin, cos
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import sys

sys.path.append("..")

from plotting import gray_background_with_grid
from read_inputs import read_wells, read_events, read_stations


wells = read_wells()
stations = read_stations()
events = read_events()

fig1, ax1 = plt.subplots(figsize=[12, 12])
ax1.set_aspect("equal")

for well in wells.values():
    ax1.plot(well["easting"], well["northing"], "0.2", lw=2)

ax1 = gray_background_with_grid(ax1)

fig1.savefig("wells.png", bbox_inches="tight")

fig2, ax2 = plt.subplots(figsize=[8, 8])
ax2.set_aspect("equal")

for well in wells.values():
    ax2.plot(well["easting"], well["northing"], "0.2", lw=2)

for station in stations.values():
    ax2.plot(
        station["e"],
        station["n"],
        "h",
        color="orangered",
        markeredgecolor="0.1",
        ms=12,
    )

ax2 = gray_background_with_grid(ax2)
fig2.savefig("wells_and_stations.png", bbox_inches="tight")


fig3, ax3 = plt.subplots(figsize=[8, 8])
ax3.set_aspect("equal")
for well in wells.values():
    ax3.plot(well["easting"], well["northing"], "0.2", lw=2)

mags, eastings, northings = np.array(
    [(v["Mw"], v["easting"], v["northing"]) for v in events.values()]
).T
i_plot = np.argsort(mags)
scatter = ax3.scatter(
    eastings[i_plot],
    northings[i_plot],
    c=mags[i_plot],
    vmin=-1.5,
    vmax=0.5,
    cmap="inferno",
    edgecolor="0.1",
    linewidth=0.25,
    s=20,
    zorder=39,
)

ax3 = gray_background_with_grid(ax3)
fig3.colorbar(scatter, ax=ax3, label="moment magnitude")
fig3.savefig("wells_and_events.png", bbox_inches="tight")
