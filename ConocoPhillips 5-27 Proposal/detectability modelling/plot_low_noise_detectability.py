import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (
    MultipleLocator,
    FormatStrFormatter,
    AutoMinorLocator,
    LogLocator,
)
import glob


def sqa(x):
    return np.squeeze(np.array(x))


drs = {"2019-03-21T15-35": "19 stations"}
"""
drs = {
    '2019-03-21T12-58': '19 stations',
    '2019-03-21T14-43': '13 stations',
    '2019-03-21T15-12': '7 stations'
}
"""
coeff = {
    "7 stations": np.sqrt(42),
    "13 stations": np.sqrt(78),
    "19 stations": np.sqrt(114),
}


xMajorLocator = MultipleLocator(0.5)
xMinorLocator = MultipleLocator(0.1)
yMajorLocator = LogLocator(10)
yMinorLocator = LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9])

positions = {"center"}

image_stats = {}
xMajorLocator = MultipleLocator(0.5)
xMinorLocator = MultipleLocator(0.1)
yMajorLocator = LogLocator(10)
yMinorLocator = LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9])

ms = {
    "7 stations": 5,
    "13 stations": 7,
    "19 stations": 9,
}

qcolor = {"toe": "forestgreen", "center": "firebrick"}


ii = -1
noise_samples = []

for dr in drs:
    ii += 1
    image_stats[drs[dr]] = {}
    for position in positions:
        csv = (
            dr
            + "//synthetic_catalog_"
            + position
            + "_static_shifted_and_corr_and_padded.csv"
        )
        f = open(csv)
        #    head = f.readline()
        lines = f.readlines()
        f.close()
        image_stats[drs[dr]][position] = {}
        for line in lines:
            lspl = line.split(",")
            true_loc = np.array([float(s) for s in lspl[2:5]])
            magnitude, stress_drop = [float(s) for s in lspl[5:7]]
            m11, m22, m33, m12, m13, m23 = [float(s) for s in lspl[7:13]]
            amplitude = float(lspl[13])
            image_loc = np.array([float(s) for s in lspl[14:17]])
            if not (magnitude in image_stats[drs[dr]][position]):
                image_stats[drs[dr]][position][magnitude] = {}
                image_stats[drs[dr]][position][magnitude]["samples"] = []
            image_stats[drs[dr]][position][magnitude]["samples"].append(
                {"amplitude": amplitude, "location shift": true_loc - image_loc}
            )
            if magnitude < -2:
                if amplitude > 0:
                    noise_samples.append(np.log10(coeff[drs[dr]] * amplitude))
                # below M-2 peak image amplitude is distributed log normally
                # assumption

threshold = 10 ** (np.average(noise_samples) + 1 * np.std(noise_samples))
# 2 sigma significant for detection threshold

f1, a1 = plt.subplots(3, 2, figsize=[16, 10])
f2, a2 = plt.subplots(figsize=[16, 10])
patches, labels = [], []
M_detect = {}
for dataset in image_stats.keys():
    M_detect[dataset] = {}
    for position in positions:
        bias = []
        spread = []
        average_amp = []
        mags = image_stats[dataset][position].keys()
        M_detect[dataset][position] = {}
        for mag in mags:
            nsamples = len(image_stats[dataset][position][mag]["samples"])
            shifts = [
                s["location shift"]
                for s in image_stats[dataset][position][mag]["samples"]
            ]
            amps = [
                s["amplitude"] for s in image_stats[dataset][position][mag]["samples"]
            ]
            bias.append(np.average(shifts, axis=0))
            spread.append(np.std(shifts, axis=0))
            average_amp.append(np.average(amps))
            image_stats[dataset][position][mag]["bias"] = bias[-1]
            image_stats[dataset][position][mag]["spread"] = spread[-1]
            image_stats[dataset][position][mag]["amplitude"] = average_amp[-1]
        mags = np.array([float(s) for s in mags])
        i_plot = np.where(np.array(average_amp) * coeff[dataset] > threshold)[0]
        x_spread = np.array(spread)[:, 0]
        x_bias = abs(np.array(bias)[:, 1])
        y_spread = np.array(spread)[:, 1]
        y_bias = abs(np.array(bias)[:, 1])
        z_spread = np.array(spread)[:, 2]
        z_bias = abs(np.array(bias)[:, 2])
        average_amp = np.array(average_amp)
        (p,) = a1[0, 0].plot(
            mags[i_plot],
            x_bias[i_plot],
            marker="h",
            markersize=ms[dataset],
            color=qcolor[position],
            markeredgecolor="k",
        )
        a1[0, 1].plot(
            mags[i_plot],
            x_spread[i_plot],
            marker="h",
            markersize=ms[dataset],
            color=qcolor[position],
            markeredgecolor="k",
        )
        a1[1, 0].plot(
            mags[i_plot],
            y_bias[i_plot],
            marker="h",
            markersize=ms[dataset],
            color=qcolor[position],
            markeredgecolor="k",
        )
        a1[1, 1].plot(
            mags[i_plot],
            y_spread[i_plot],
            marker="h",
            markersize=ms[dataset],
            color=qcolor[position],
            markeredgecolor="k",
        )
        a1[2, 0].plot(
            mags[i_plot],
            z_bias[i_plot],
            marker="h",
            markersize=ms[dataset],
            color=qcolor[position],
            markeredgecolor="k",
        )
        a1[2, 1].plot(
            mags[i_plot],
            z_spread[i_plot],
            marker="h",
            markersize=ms[dataset],
            color=qcolor[position],
            markeredgecolor="k",
        )
        patches.append(p)
        labels.append(dataset + ": " + position)
        a2.semilogy(
            mags,
            coeff[dataset] * average_amp,
            marker="h",
            markersize=ms[dataset],
            color=qcolor[position],
            markeredgecolor="k",
        )
        ilarge = np.where(coeff[dataset] * average_amp > threshold)[0][0:6]
        large_amps = np.matrix(np.log10(coeff[dataset] * average_amp[ilarge]))
        Amatrix = np.matrix(
            np.vstack([mags[ilarge] * mags[ilarge], mags[ilarge], np.ones(len(ilarge))])
        ).T
        quad, slope, yint = [
            sqa(s)
            for s in np.linalg.inv(Amatrix.T * Amatrix) * Amatrix.T * large_amps.T
        ]
        detectability_line = 10 ** (mags * mags * quad + mags * slope + yint)
        M_detect[dataset][position] = (
            (-slope + np.sqrt(slope * slope - 4 * quad * (yint - np.log10(threshold))))
            / 2
            / quad
        )
        a2.semilogy(mags, detectability_line, "k:", lw=1)

dum, y1 = a1[0, 0].get_ylim()
dum, y2 = a1[1, 0].get_ylim()
dum, y3 = a1[0, 1].get_ylim()
dum, y4 = a1[1, 1].get_ylim()
dum, y5 = a1[2, 0].get_ylim()
dum, y6 = a1[2, 1].get_ylim()
y_max = max([y1, y2, y3, y4, y5, y6])


a1[0, 1].legend(patches, labels)
a1[0, 0].set_title("bias")
a1[0, 1].set_title("spread")
a1[0, 0].set_ylabel("x horizontal distance (m)")
a1[1, 0].set_ylabel("y horizontal distance (m)")
a1[2, 0].set_ylabel("vertical distance (m)")
a1[0, 1].set_ylabel("x horizontal distance (m)")
a1[1, 1].set_ylabel("y horizontal distance (m)")
a1[2, 1].set_ylabel("vertical distance (m)")
a1[2, 0].set_xlabel("moment magnitude")
a1[2, 1].set_xlabel("moment magnitude")
a1[0, 0].set_ylim([-0.05 * y_max, y_max])
a1[1, 0].set_ylim([-0.05 * y_max, y_max])
a1[0, 1].set_ylim([-0.05 * y_max, y_max])
a1[1, 1].set_ylim([-0.05 * y_max, y_max])
a1[2, 0].set_ylim([-0.05 * y_max, y_max])
a1[2, 1].set_ylim([-0.05 * y_max, y_max])
a1[0, 0].grid(True)
a1[1, 0].grid(True)
a1[0, 1].grid(True)
a1[1, 1].grid(True)
a1[2, 0].grid(True)
a1[2, 1].grid(True)

a2.grid(True)
a2.set_xlabel("moment magnitude")
a2.set_ylabel("amplitude")
a2.legend(patches, labels)
a2.semilogy([-4, 4], [threshold, threshold], "-.", lw=2, zorder=-3, color="k")
a2.text(-2.38, 1.03 * threshold, "2$\sigma$ detection threshold")
a2.set_xlim([-2.55, -1.55])
a2.set_ylim([0.2, 10])
plt.show()

M_detect

20 * np.log10(2 * 3.1415 * 3e-8 * 10)
