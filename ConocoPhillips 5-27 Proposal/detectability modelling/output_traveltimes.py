import numpy as np
import glob
import os

from obspy.imaging.mopad_wrapper import beach
from obspy import Stream, Trace, UTCDateTime
from obspy.core.trace import Stats
import utm
import matplotlib.pyplot as plt
from ray_modelling.waveform_model import get_response, add_noise
from ray_modelling.raytrace import isotropic_ray_trace

from read_inputs import block_model

dt = 0.002  # sample rate
tshift = 0.0  # shift t0 in seconds
ishift = int(tshift / dt)  # shift in samples
time_series = np.arange(0, 5.00, dt)
noise_level = 3.0e-8 / np.sqrt(42.0)
easting_ctr, northing_ctr = 577783.6, 6278497.5
depth = 1210.0
plot_out = False
n_iterations = 20
# 1204-1211 Lower-Montney, 1122-1129 Mid-Montney, 1056-1060 Upper-Montney
freqmin, freqmax, resample = 20.0, 49.9, 100.0

for stationsCSV in ["./CoP_proposed_stations_out_with_ISM.csv"]:

    for location_string in ["center", "toe"]:
        if location_string == "toe":
            xshift = 0
            yshift = -1600
        elif location_string == "center":
            xshift = 0
            yshift = 0

        def sqa(x):
            return np.squeeze(np.array(x))

        stations = []
        f = open(stationsCSV)
        stationlines = f.readlines()
        f.close()
        for line in stationlines:
            lspl = line.split(",")
            stations.append(
                {
                    "x": float(lspl[2]),
                    "y": float(lspl[1]),
                    "z": float(lspl[3]),
                    "name": lspl[0],
                }
            )

        velocity_model = block_model()

        source = {
            "x": easting_ctr + xshift,
            "y": northing_ctr + yshift,
            "z": depth,
            "moment_magnitude": -2.5,
            "stress_drop": 3e5,  # static stress drop
            "moment_tensor": np.matrix([[0, 1, 0], [1, 0, 0], [0, 0, 0]]),
        }
        m11, m12, m13, d1, m22, m23, d2, d3, m33 = sqa(
            source["moment_tensor"].reshape(
                9,
            )
        )
        mt_6 = [m11, m22, m33, m12, m13, m23]
        source["moment_tensor"] = source["moment_tensor"] / np.linalg.norm(
            source["moment_tensor"]
        )
        g = open("output_traveltimes_" + location_string + "_with_ISM.csv", "w")
        g.write(
            "station name,station x (m), station y (m), station z (m)"
            + ",P traveltime (s),S traveltime (s)\n"
        )
        for station in stations:
            pRaypath = isotropic_ray_trace(source, station, velocity_model, "P")
            sRaypath = isotropic_ray_trace(source, station, velocity_model, "S")
            g.write(station["name"])
            g.write(",%.1f,%.1f,%.1f," % (station["x"], station["y"], station["z"]))
            g.write("%.7f,%.7f\n" % (pRaypath["traveltime"], sRaypath["traveltime"]))
g.close()
