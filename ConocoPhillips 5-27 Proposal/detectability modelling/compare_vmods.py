import numpy as np
import matplotlib.pyplot as plt

from read_inputs import read_velocity_model, block_model

finer_velocity_model = read_velocity_model("CPBB NLL velocity model.csv")
block_velocity_model = block_model()

finer_velocity_model

fig, ax = plt.subplots()

ax.plot(finer_velocity_model["vp"], finer_velocity_model["depth"] - 900)
ax.set_ylim([4000, -900])
previous_top = -900
block_vp = []
block_depth = [previous_top]
for layer in block_velocity_model:
    block_vp.append()
