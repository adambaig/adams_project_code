import numpy as np
import glob

from obspy import read, UTCDateTime
import utm
from DataHandling.dataProcessing import (processMseed,convertStreamToRSF, 
					mergeEvents, stationCSVtoRSF,
					convertNumpyToRSF)
from Velocity.velocity import createTTT_1D,create2Dfrom1D_vel,createRSFfromCSV_1D
from Imaging.imaging import relocate_Event_PS
from ray_modelling.raytrace import isotropic_ray_trace
import rsf.api as rsf
import m8r as sf

from read_inputs import block_model

dt = 0.002 # sample rate
time_series = np.arange(0, 5.00, dt)
easting_ctr,northing_ctr = 577783.6,6278497.5
depth = 1200.
# 1204-1211 Lower-Montney, 1122-1129 Mid-Montney, 1056-1060 Upper-Montney
freqmin,freqmax,resample = 20.,50.,125.
base_dir = '//data//mount_doom//home//adamb//ConocoPhillips_5-27//'
stationsCSV = base_dir + 'CoP_proposed_stations_out.csv'
outStations = base_dir + 'CoP_proposed_stations.rsf'
stationFile=stationCSVtoRSF(stationsCSV, outStations,elevation=False, sort=False)
dr = base_dir + 'PS_imaging//2019-04-09T14-48//'
velocity_model = block_model()
catalog = dr + '//synthetic_catalog_toe.csv'
velpcsv=base_dir + 'discretized_vp.csv'
velscsv=base_dir + 'discretized_vs.csv'
velp1Drsf='CoP_vp_1D.rsf'
velp2Drsf='CoP_vp_2D_new.rsf'
vels1Drsf='CoP_vs_1D.rsf'
vels2Drsf='CoP_vs_2D_new.rsf'
#sampling in depth (m)
input_delta_z=10
#depth (m), positive down
datum=-900
vCol=0
#maximum distance of output 2D model (m)
maxXYdist=10000
#sampling x,y in (m)
output_delta_xy=100
init_Vp_TTT='CoP_init_Vp_TTT.rsf'
init_Vs_TTT='CoP_init_Vs_TTT.rsf'
sigma_static = 0.005

#P wave
#create 1D rsf file from regularly sampled csv
velp1D=createRSFfromCSV_1D(velpcsv,velp1Drsf,input_delta_z,datum=datum,col=vCol,hasHeaders=True)
#Extrapolate 1D to 2D velocity profile
velp2D=create2Dfrom1D_vel(velp1Drsf,velp2Drsf,maxXYdist,output_delta_xy,smooth=True,rect=10)
#Create travelTime table from 2D profile, remove 2D because it is not needed or useful anymore
pTTT=createTTT_1D(velp2Drsf,init_Vp_TTT,outStations,clean=False)
input=rsf.Input(pTTT)
n1=input.int('n1')
n2=input.int('n2')
d1=input.int('d1')
d2=input.int('d2')
o1=input.int('o1')
o2=input.int('o2')
pTTT2 = np.zeros((n2,n1),dtype='float32')
input.read(pTTT2)

#S wave
#create 1D rsf file from regularly sampled csv
vels1D=createRSFfromCSV_1D(velscsv,vels1Drsf,input_delta_z,datum=datum,col=vCol,hasHeaders=True)
#Extrapolate 1D to 2D velocity profile
vels2D=create2Dfrom1D_vel(vels1Drsf,vels2Drsf,maxXYdist,output_delta_xy,smooth=True,rect=10)
#Create travelTime table from 2D profile, remove 2D because it is not needed or useful anymore
sTTT=createTTT_1D(vels2Drsf,init_Vs_TTT,outStations,clean=False)
input=rsf.Input(sTTT)
n1=input.int('n1')
n2=input.int('n2')
d1=input.int('d1')
d2=input.int('d2')
o1=input.int('o1')
o2=input.int('o2')
sTTT2 = np.zeros((n2,n1),dtype='float32')
input.read(sTTT2)

f = open(catalog)
head = f.readline()
lines = f.readlines()
f.close()

for location_string in ['toe','center']:
    if location_string == 'toe':
        xshift = 0.
        yshift = -1600.
        catalog = dr+'synthetic_catalog_toe.csv'
    elif location_string =='center':
        xshift = 0.
        yshift = 0.
        catalog = dr+'synthetic_catalog_center.csv'
    ox = easting_ctr+xshift
    oy = northing_ctr+yshift
    oz = depth
    f = open(catalog)
    head = f.readline()
    lines = f.readlines()
    f.close()

    f = open(stationsCSV)
    station_lines = f.readlines()
    f.close()
    ptt_diff,stt_diff = [],[]
    for station_line in station_lines:
        lspl = station_line.split(',')
        st_y,st_x,st_z = [float(s) for s in lspl[1:4]]
        station = {"x": st_x, "y": st_y, "z": st_z}
        source = {"x": ox, "y": oy, "z": oz}
        pRaypath = isotropic_ray_trace(source,station,velocity_model,"P")
        sRaypath = isotropic_ray_trace(source,station,velocity_model,"S")
        zInd = int((oz-st_z)/d1)
        xyDist = np.sqrt((ox-st_x)**2 + (oy-st_y)**2)
        xyInd = int(xyDist/d2)
        remX = xyDist/d2 - xyInd
        remZ = 0.
        c00=(pTTT2[xyInd  ][zInd  ]*(1.-remZ)+
             pTTT2[xyInd  ][zInd+1]*(  remZ))
        c01=(pTTT2[xyInd+1][zInd  ]*(1.-remZ)+
             pTTT2[xyInd+1][zInd+1]*(  remZ))
        p_traveltime = c00*(1.-remX) + c01*(remX)
        c00=(sTTT2[xyInd  ][zInd  ]*(1.-remZ)+
             sTTT2[xyInd  ][zInd+1]*(  remZ))
        c01=(sTTT2[xyInd+1][zInd  ]*(1.-remZ)+
             sTTT2[xyInd+1][zInd+1]*(  remZ))
        s_traveltime = c00*(1.-remX) + c01*(remX)
        ptt_diff.append(pRaypath['traveltime'] - p_traveltime)
        stt_diff.append(sRaypath['traveltime'] - s_traveltime)

    ptt_diff = np.array(ptt_diff)
    stt_diff = np.array(stt_diff)

    g = open(catalog.split('.')[0]+'_relocated.csv','w')
    for line in lines:
        lspl = line.split(',')
        x,y,z = [float(s) for s in lspl[2:5]]
        ox = x
        oy = y
        oz = z
        oo = [oz,oy,ox]
        nn = [50,50,50]
        dd = [4,4,4]
        dat,tim = lspl[0],lspl[1]
        seedfile = 'synthetic'+dat+'T'+tim.replace(':','-')+'Z.mseed'
        print(seedfile)
        st = read(dr+'//'+seedfile)
        nstations = len(st)/3
        process_stream=processMseed(st.select(component="Z"), 
				freqmin=freqmin, freqmax=freqmax, resample=resample, doRadRemove=True)
        process_stream_E=processMseed(st.select(component="E"), 
				freqmin=freqmin, freqmax=freqmax, resample=resample, doRadRemove=True)
        process_stream_N=processMseed(st.select(component="N"), 
				freqmin=freqmin, freqmax=freqmax, resample=resample, doRadRemove=True)
        image_file=convertStreamToRSF(process_stream,'image',pad=0)
        image_file_E=convertStreamToRSF(process_stream_E,'image',pad=0)
        image_file_N=convertStreamToRSF(process_stream_N,'image',pad=0)
        sdata=sf.add(scale="0.5,0.5")[image_file_E,image_file_N]
        p_image_statics=convertNumpyToRSF(ptt_diff+sigma_static*np.random.randn(nstations),'p_statics.rsf')
        s_image_statics=convertNumpyToRSF(stt_diff+sigma_static*np.random.randn(nstations),'s_statics.rsf')
	pad_image_file=sf.pad(beg1=125)[image_file]
	pad_sdata=sf.pad(beg1=125)[sdata]
        image_amp,rel_dp, rel_nr, rel_es, rel_t0 = relocate_Event_PS(pad_image_file,pad_sdata,
                    init_Vp_TTT,
                    init_Vs_TTT,
                    outStations,
                    nn,dd,oo,nthreads=4,
		    pstatics='p_statics.rsf',
		    sstatics='s_statics.rsf')
        g.write(line[:-2])
        g.write(',%.4e,%.1f,%.1f,%.1f\n' %(image_amp, rel_es, rel_nr, rel_dp))
    g.close()

