import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (
    MultipleLocator,
    FormatStrFormatter,
    AutoMinorLocator,
    LogLocator,
)
import glob


def sqa(x):
    return np.squeeze(np.array(x))


drs = {
    "2019-03-15T13-13": "7 stations",
    "2019-03-15T12-48": "13 stations",
    "2019-03-18T12-54": "19 stations",
}


coeff = {
    "7 stations": np.sqrt(42),
    "13 stations": np.sqrt(78),
    "19 stations": np.sqrt(114),
}


xMajorLocator = MultipleLocator(0.5)
xMinorLocator = MultipleLocator(0.1)
yMajorLocator = LogLocator(10)
yMinorLocator = LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9])

positions = {"center", "toe"}

image_stats = {}
xMajorLocator = MultipleLocator(0.5)
xMinorLocator = MultipleLocator(0.1)
yMajorLocator = LogLocator(10)
yMinorLocator = LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9])

ms = {
    "7 stations": 5,
    "13 stations": 7,
    "19 stations": 9,
}

qcolor = {"toe": "forestgreen", "center": "firebrick"}


ii = -1
noise_samples = []

for dr in drs:
    ii += 1
    image_stats[drs[dr]] = {}
    for position in positions:
        csv = dr + "//synthetic_catalog_" + position + "_static_shifted.csv"
        f = open(csv)
        #    head = f.readline()
        lines = f.readlines()
        f.close()
        image_stats[drs[dr]][position] = {}
        for line in lines:
            lspl = line.split(",")
            true_loc = np.array([float(s) for s in lspl[2:5]])
            magnitude, stress_drop = [float(s) for s in lspl[5:7]]
            m11, m22, m33, m12, m13, m23 = [float(s) for s in lspl[7:13]]
            amplitude = float(lspl[13])
            image_loc = np.array([float(s) for s in lspl[14:17]])
            if not (magnitude in image_stats[drs[dr]][position]):
                image_stats[drs[dr]][position][magnitude] = {}
                image_stats[drs[dr]][position][magnitude]["samples"] = []
            image_stats[drs[dr]][position][magnitude]["samples"].append(
                {"amplitude": amplitude, "location shift": true_loc - image_loc}
            )
            if magnitude < -2:
                if amplitude > 0:
                    noise_samples.append(np.log10(coeff[drs[dr]] * amplitude))
                # below M-2 peak image amplitude is distributed log normally
                # assumption

threshold = 10 ** (np.average(noise_samples) + 2 * np.std(noise_samples))
# 2 sigma significant for detection threshold

f1, a1 = plt.subplots(2, 2, figsize=[16, 10])
f2, a2 = plt.subplots(figsize=[16, 10])
patches, labels = [], []

for dataset in image_stats.keys():

    for position in positions:
        bias = []
        spread = []
        average_amp = []
        mags = image_stats[dataset][position].keys()
        for mag in mags:
            nsamples = len(image_stats[dataset][position][mag]["samples"])
            shifts = [
                s["location shift"]
                for s in image_stats[dataset][position][mag]["samples"]
            ]
            amps = [
                s["amplitude"] for s in image_stats[dataset][position][mag]["samples"]
            ]
            bias.append(np.average(shifts, axis=0))
            spread.append(np.std(shifts, axis=0))
            average_amp.append(np.average(amps))
            image_stats[dataset][position][mag]["bias"] = bias[-1]
            image_stats[dataset][position][mag]["spread"] = spread[-1]
            image_stats[dataset][position][mag]["amplitude"] = average_amp[-1]
        xy_spread = np.linalg.norm(np.array(spread)[:, :2], axis=1)
        xy_bias = np.linalg.norm(np.array(bias)[:, :2], axis=1)
        z_spread = np.array(spread)[:, 2]
        z_bias = abs(np.array(bias)[:, 2])
        (p,) = a1[0, 0].plot(
            mags,
            xy_bias,
            marker="h",
            markersize=ms[dataset],
            color=qcolor[position],
            markeredgecolor="k",
        )
        a1[0, 1].plot(
            mags,
            xy_spread,
            marker="h",
            markersize=ms[dataset],
            color=qcolor[position],
            markeredgecolor="k",
        )
        a1[1, 0].plot(
            mags,
            z_bias,
            marker="h",
            markersize=ms[dataset],
            color=qcolor[position],
            markeredgecolor="k",
        )
        a1[1, 1].plot(
            mags,
            z_spread,
            marker="h",
            markersize=ms[dataset],
            color=qcolor[position],
            markeredgecolor="k",
        )
        patches.append(p)
        labels.append(dataset + ": " + position)
        a2.semilogy(
            mags,
            coeff[dataset] * np.array(average_amp),
            marker="h",
            markersize=ms[dataset],
            color=qcolor[position],
            markeredgecolor="k",
        )


dum, y1 = a1[0, 0].get_ylim()
dum, y2 = a1[1, 0].get_ylim()
dum, y3 = a1[0, 1].get_ylim()
dum, y4 = a1[1, 1].get_ylim()
y_max = max([y1, y2, y3, y4])

a1[0, 1].legend(patches, labels)
a1[0, 0].set_title("bias")
a1[0, 1].set_title("spread")
a1[0, 0].set_ylabel("horizontal distance (m)")
a1[1, 0].set_ylabel("vertical distance (m)")
a1[1, 0].set_xlabel("moment magnitude")
a1[1, 1].set_xlabel("moment magnitude")
a1[0, 0].set_ylim([-0.05 * y_max, y_max])
a1[1, 0].set_ylim([-0.05 * y_max, y_max])
a1[0, 1].set_ylim([-0.05 * y_max, y_max])
a1[1, 1].set_ylim([-0.05 * y_max, y_max])
a1[0, 0].grid(True)
a1[1, 0].grid(True)
a1[0, 1].grid(True)
a1[1, 1].grid(True)

a2.grid(True)
a2.set_xlabel("moment magnitude")
a2.set_ylabel("amplitude")
a2.legend(patches, labels)
a2.semilogy([-4, 4], [threshold, threshold], "-.", lw=2, zorder=-3, color="k")
a2.text(-2.38, 1.03 * threshold, "2$\sigma$ detection threshold")
a2.set_xlim([-2.55, -1.45])
a2.set_ylim([0.2, 10])
plt.show()
