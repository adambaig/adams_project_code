import numpy as np
from ray_modelling.raytrace import isotropic_ray_trace

from read_inputs import block_model


source = {"x": 0.0, "y": 0.0, "z": 3000.0}

station = {"x": 120.0, "y": 10.0, "z": -700.0}

velocity_model = block_model()

pRaypath = isotropic_ray_trace(source, station, velocity_model, "P", test=True)

# print(pRaypath)
