import matplotlib

matplotlib.use("Qt5agg")
import numpy as np
import matplotlib.pyplot as plt
import utm
import glob
from obspy.imaging.mopad_wrapper import beach

dr = "C:\\Users\\adambaig\\Project\\ConocoPhillips 5-27 Proposal\\detectability modelling\\"


def sqa(x):
    return np.squeeze(np.array(x))


def read_stations():
    f = open(dr + "stations.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        name = lspl[0]
        stations[name] = {}
        stations[name]["easting"], stations[name]["northing"], d1, d2 = utm.from_latlon(
            float(lspl[1]), float(lspl[2])
        )
    return stations


def read_ism_station_locations():
    f = open(dr + "ISM_stations_4.csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        name = lspl[1]
        stations[name] = {}
        stations[name]["easting"], stations[name]["northing"], d1, d2 = utm.from_latlon(
            float(lspl[2]), float(lspl[3])
        )
        stations[name]["elevation"] = float(lspl[4])
    return stations


def read_wells():
    wells = {}
    csvs = glob.glob(dr + "CSV//*.csv")
    for csv in csvs:
        well = csv.split("\\")[-1].split(" P4V1 ")[0]
        f = open(csv)
        head = f.readline()
        null = f.readline()
        lines = f.readlines()
        nl = len(lines)
        f.close()
        wells[well] = {}
        east, north, tvdss, md = np.zeros(nl), np.zeros(nl), np.zeros(nl), np.zeros(nl)
        ii = -1
        for line in lines:
            ii += 1
            lspl = line.split(",")
            md[ii] = float(lspl[0])
            east[ii], north[ii], tvdss[ii] = [float(s) for s in lspl[8:11]]
        wells[well]["easting"] = east
        wells[well]["northing"] = north
        wells[well]["tvdss"] = tvdss
        wells[well]["md"] = md
    return wells


def plot_site():
    beach_toe = beach(
        [0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
        xy=(577783.6, 6278497.5 - 1600.0),
        zorder=0,
        width=500,
        mopad_basis="NED",
        linewidth=0.5,
    )
    beach_center = beach(
        [0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
        xy=(577783.6, 6278497.5),
        zorder=0,
        width=500,
        mopad_basis="NED",
        linewidth=0.5,
    )
    wells = read_wells()
    stations = read_stations()
    ism = read_ism_station_locations()
    fig, ax = plt.subplots()
    ax.set_aspect("equal")
    for station in stations:
        ax.plot(
            stations[station]["easting"], stations[station]["northing"], "kh", zorder=4
        )
        ax.text(
            stations[station]["easting"] + 10,
            stations[station]["northing"] + 10,
            station,
        )
    #    for ism_station in ism:
    #        ax.plot(ism[ism_station]['easting'],ism[ism_station]['northing'],
    #                    'v',color='sienna',zorder = 4)
    #        ax.text(ism[ism_station]['easting']+10,
    #                ism[ism_station]['northing']+10,ism_station)
    ii = -1
    for well in wells:
        if well[0] in ["C", "D", "E"]:
            z = 2
        else:
            z = -2
        ax.plot(
            wells[well]["easting"], wells[well]["northing"], color="0.2", lw=5, zorder=z
        )
        ax.plot(
            wells[well]["easting"],
            wells[well]["northing"],
            color="0.7",
            lw=3,
            zorder=z + 1,
        )
    ax.set_xlabel("easting (m)")
    ax.set_ylabel("northing (m)")
    ax.add_collection(beach_toe)
    ax.add_collection(beach_center)
    return fig


def export_stations():
    stations = read_stations()
    assumed_elev = -900.0
    f = open(dr + "stations_out.csv", "w")
    f.write("station, northing (m), easting (m), tvdss (m)\n")
    for station in stations:
        f.write(
            station
            + ",%.1f,%.1f,%.1f\n"
            % (
                stations[station]["northing"],
                stations[station]["easting"],
                assumed_elev,
            )
        )
    f.close()


def export_ism_stations():
    stations = read_ism_station_locations()
    f = open(dr + "ism_stations_out.csv", "w")
    f.write("station, northing (m), easting (m), tvdss (m)\n")
    for station in stations:
        f.write(
            station
            + ",%.1f,%.1f,%.1f\n"
            % (
                stations[station]["northing"],
                stations[station]["easting"],
                -stations[station]["elevation"],
            )
        )
    f.close()


def read_velocity_model(vm_file):
    f = open(vm_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    vm = {}
    nlines = len(lines)
    depth = np.zeros(nlines)
    vp = np.zeros(nlines)
    vs = np.zeros(nlines)
    ii = -1
    for line in lines:
        ii += 1
        lspl = line.split(",")
        depth[ii], vp[ii], vs[ii] = [float(s) for s in lspl[0:5:2]]
    vm["depth"] = depth
    vm["vs"] = vs
    vm["vp"] = vp
    return vm


def block_model():
    kb = 900.0
    layer_color = [
        "bisque",
        "lightsteelblue",
        "tan",
        "saddlebrown",
        "mediumseagreen",
        "indigo",
        "rosybrown",
        "darkslateblue",
        "lavender",
        "khaki",
        "darkgoldenrod",
        "olive",
    ]
    velocity_model = [
        {"vp": 2500.0, "vs": 1300.0},
        {"vp": 3000.0, "vs": 1450.0, "top": 100.0 - kb},
        {"vp": 3300.0, "vs": 1800.0, "top": 400.0 - kb},
        {"vp": 4750.0, "vs": 2900.0, "top": 1350.0 - kb},
        {"vp": 5800.0, "vs": 3100.0, "top": 1500.0 - kb},
        {"vp": 4900.0, "vs": 2900.0, "top": 1950.0 - kb},
        {"vp": 6000.0, "vs": 3200.0, "top": 2300.0 - kb},
        {"vp": 3350.0, "vs": 1600.0, "top": 3000.0 - kb},
        {"vp": 4800.0, "vs": 2400.0, "top": 3200.0 - kb},
        {"vp": 4100.0, "vs": 2150.0, "top": 3500.0 - kb},
        {"vp": 4950.0, "vs": 2550.0, "top": 3800.0 - kb},
        {"vp": 6750.0, "vs": 3625.0, "top": 4000.0 - kb},
    ]
    for ii in range(len(velocity_model)):
        velocity_model[ii]["rho"] = 310.0 * velocity_model[ii]["vp"] ** (0.25)
        velocity_model[ii]["color"] = layer_color[ii]
    return velocity_model


def output_discretized_vm():
    f = open("discretized_vm.csv", "w")
    f.write("velocity (m/s)\n")
    velocity_model = block_model()
    depth, md = -900, 0
    tops = np.array([layer["top"] for layer in velocity_model[1:]])
    while md < 4000:
        ilayer = np.where(tops < depth)[0]
        if len(ilayer) == 0:
            velocity = velocity_model[0]["vp"]
        else:
            ii = ilayer[-1] + 1
            velocity = velocity_model[ii]["vp"]
        f.write("%.0f\n" % velocity)
        depth += 10
        md += 10
    f.close()


plot_site()

plt.show()
