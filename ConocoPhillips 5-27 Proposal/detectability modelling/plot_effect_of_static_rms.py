import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (
    MultipleLocator,
    FormatStrFormatter,
    AutoMinorLocator,
    LogLocator,
)
import glob


def sqa(x):
    return np.squeeze(np.array(x))


drs = {
    "2019-04-10T18-05": "19 stations",
    #    "2019-04-10T18-06": "25 stations",
    #    "2019-04-10T18-07": "31 stations"
}
"""
drs = {
    "2019-04-10T14-17": "19 stations",
    "2019-04-10T14-47": "25 stations",
    "2019-04-10T14-33": "31 stations"
}
"""


coeff = {
    "31 stations": np.sqrt(186),
    "19 stations": np.sqrt(114),
    "25 stations": np.sqrt(150),
}


xMajorLocator = MultipleLocator(0.5)
xMinorLocator = MultipleLocator(0.1)
yMajorLocator = LogLocator(10)
yMinorLocator = LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9])

statics = {"1ms", "3ms", "5ms"}

image_stats = {}
xMajorLocator = MultipleLocator(0.5)
xMinorLocator = MultipleLocator(0.1)
yMajorLocator = LogLocator(10)
yMinorLocator = LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9])

ms = {"19 stations": 5, "25 stations": 7, "31 stations": 9}

qcolor = {"1ms": "forestgreen", "3ms": "royalblue", "5ms": "firebrick"}

ii = -1
noise_samples = []

for dr in drs:
    ii += 1
    image_stats[drs[dr]] = {}
    for static in statics:
        csv = dr + "//synthetic_catalog_center_" + static + "statics_relocated.csv"
        f = open(csv)
        #    head = f.readline()
        lines = f.readlines()
        f.close()
        image_stats[drs[dr]][static] = {}
        for line in lines:
            lspl = line.split(",")
            true_loc = np.array([float(s) for s in lspl[2:5]])
            magnitude, stress_drop = [float(s) for s in lspl[5:7]]
            m11, m22, m33, m12, m13, m23 = [float(s) for s in lspl[7:13]]
            amplitude = float(lspl[13])
            image_loc = np.array([float(s) for s in lspl[14:17]])
            if not (magnitude in image_stats[drs[dr]][static]):
                image_stats[drs[dr]][static][magnitude] = {}
                image_stats[drs[dr]][static][magnitude]["samples"] = []
            image_stats[drs[dr]][static][magnitude]["samples"].append(
                {"amplitude": amplitude, "location shift": true_loc - image_loc}
            )
            if magnitude < -2.4:
                if amplitude > 0:
                    noise_samples.append(np.log10(coeff[drs[dr]] * amplitude))
                # below M-2 peak image amplitude is distributed log normally
                # assumption

threshold = 10 ** (np.average(noise_samples) + 2 * np.std(noise_samples))
# 2 sigma significant for detection threshold

f0, a0 = plt.subplots()
f1, a1 = plt.subplots(3, 2, figsize=[16, 10])
patches, labels = [], []

for dataset in image_stats.keys():

    for static in statics:
        bias = []
        spread = []
        average_amp = []
        mags = image_stats[dataset][static].keys()
        for mag in mags:
            nsamples = len(image_stats[dataset][static][mag]["samples"])
            shifts = [
                s["location shift"]
                for s in image_stats[dataset][static][mag]["samples"]
            ]
            amps = [
                s["amplitude"] for s in image_stats[dataset][static][mag]["samples"]
            ]
            bias.append(np.average(shifts, axis=0))
            spread.append(np.std(shifts, axis=0))
            average_amp.append(np.average(amps))
            image_stats[dataset][static][mag]["bias"] = bias[-1]
            image_stats[dataset][static][mag]["spread"] = spread[-1]
            image_stats[dataset][static][mag]["amplitude"] = average_amp[-1]
        mags = np.array([float(s) for s in mags])
        a0.semilogy(mags, average_amp, color=qcolor[static], marker="h")
        i_plot = np.where(np.array(average_amp) * coeff[dataset] > threshold)[0]
        x_spread = np.array(spread)[:, 0]
        x_bias = abs(np.array(bias)[:, 1])
        y_spread = np.array(spread)[:, 1]
        y_bias = abs(np.array(bias)[:, 1])
        z_spread = np.array(spread)[:, 2]
        z_bias = abs(np.array(bias)[:, 2])
        (p,) = a1[0, 0].plot(
            mags[i_plot],
            x_bias[i_plot],
            marker="h",
            markersize=ms[dataset],
            color=qcolor[static],
            markeredgecolor="k",
        )
        a1[0, 1].plot(
            mags[i_plot],
            x_spread[i_plot],
            marker="h",
            markersize=ms[dataset],
            color=qcolor[static],
            markeredgecolor="k",
        )
        a1[1, 0].plot(
            mags[i_plot],
            y_bias[i_plot],
            marker="h",
            markersize=ms[dataset],
            color=qcolor[static],
            markeredgecolor="k",
        )
        a1[1, 1].plot(
            mags[i_plot],
            y_spread[i_plot],
            marker="h",
            markersize=ms[dataset],
            color=qcolor[static],
            markeredgecolor="k",
        )
        a1[2, 0].plot(
            mags[i_plot],
            z_bias[i_plot],
            marker="h",
            markersize=ms[dataset],
            color=qcolor[static],
            markeredgecolor="k",
        )
        a1[2, 1].plot(
            mags[i_plot],
            z_spread[i_plot],
            marker="h",
            markersize=ms[dataset],
            color=qcolor[static],
            markeredgecolor="k",
        )
        patches.append(p)
        labels.append(dataset + ": " + static)


dum, y1 = a1[0, 0].get_ylim()
dum, y2 = a1[1, 0].get_ylim()
dum, y3 = a1[0, 1].get_ylim()
dum, y4 = a1[1, 1].get_ylim()
dum, y5 = a1[2, 0].get_ylim()
dum, y6 = a1[2, 1].get_ylim()
y_max = max([y1, y2, y3, y4, y5, y6])

a1[0, 1].legend(patches, labels)
a1[0, 0].set_title("bias")
a1[0, 1].set_title("location accuracy")
a1[0, 0].set_ylabel("x horizontal distance (m)")
a1[1, 0].set_ylabel("y horizontal distance (m)")
a1[2, 0].set_ylabel("vertical distance (m)")
a1[0, 1].set_ylabel("x horizontal distance (m)")
a1[1, 1].set_ylabel("y horizontal distance (m)")
a1[2, 1].set_ylabel("vertical distance (m)")
a1[2, 0].set_xlabel("moment magnitude")
a1[2, 1].set_xlabel("moment magnitude")
a1[0, 0].set_ylim([-0.05 * y_max, y_max])
a1[1, 0].set_ylim([-0.05 * y_max, y_max])
a1[0, 1].set_ylim([-0.05 * y_max, y_max])
a1[1, 1].set_ylim([-0.05 * y_max, y_max])
a1[2, 0].set_ylim([-0.05 * y_max, y_max])
a1[2, 1].set_ylim([-0.05 * y_max, y_max])
a1[0, 0].grid(True)
a1[1, 0].grid(True)
a1[0, 1].grid(True)
a1[1, 1].grid(True)
a1[2, 0].grid(True)
a1[2, 1].grid(True)
plt.show()
