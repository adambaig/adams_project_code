import matplotlib

matplotlib.use("Qt5Agg")
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (
    MultipleLocator,
    FormatStrFormatter,
    AutoMinorLocator,
    LogLocator,
)
import glob


def sqa(x):
    return np.squeeze(np.array(x))


drs = {
    "2019-03-14T12-17": "7 stations: good stack",
    "2019-03-14T12-23": "7 stations: bad stack",
    "2019-03-14T12-29": "13 stations: good stack",
    "2019-03-14T12-36": "13 stations: bad stack",
}
coeff = {
    "2019-03-14T12-17": np.sqrt(42),
    "2019-03-14T12-23": np.sqrt(7),
    "2019-03-14T12-29": np.sqrt(78),
    "2019-03-14T12-36": np.sqrt(13),
}
image_stats = {}
xMajorLocator = MultipleLocator(0.5)
xMinorLocator = MultipleLocator(0.1)
yMajorLocator = LogLocator(10)
yMinorLocator = LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9])
ms = {"7 stations": 5, "13 stations": 7}
qcolor = {"good stack": "g", "bad stack": "r"}
f1, a1 = plt.subplots()
ii = -1
for dr in drs:
    ii += 1
    image_stats[drs[dr]] = {}
    csv = dr + "//synthetic_catalog_center_located.csv"
    f = open(csv)
    #    head = f.readline()
    lines = f.readlines()
    f.close()
    nstations, stackq = drs[dr].split(": ")
    tag = csv.split(".")[0].split("_")[-1]
    image_stats[drs[dr]] = []
    for line in lines:
        lspl = line.split(",")
        true_loc = np.array([float(s) for s in lspl[2:5]])
        magnitude, stress_drop = [float(s) for s in lspl[5:7]]
        m11, m22, m33, m12, m13, m23 = [float(s) for s in lspl[7:13]]
        amplitude = float(lspl[13])
        image_loc = np.array([float(s) for s in lspl[14:17]])
        image_stats[drs[dr]].append(
            {
                "magnitude": magnitude,
                "amplitude": amplitude,
                "location shift": true_loc - image_loc,
            }
        )
        mag = [v["magnitude"] for v in image_stats[drs[dr]]]
        amp = coeff[dr] * np.array([v["amplitude"] for v in image_stats[drs[dr]]])
        a1.semilogy(
            mag,
            amp,
            color=qcolor[stackq],
            marker="h",
            markeredgecolor="k",
            markersize=ms[nstations],
        )
plt.show()
