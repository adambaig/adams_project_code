import numpy as np
import glob

from obspy import read, UTCDateTime
import utm

from DataHandling.dataProcessing import (
    processMseed,
    convertMseedToRSF,
    mergeEvents,
    stationCSVtoRSF,
)
from Velocity.velocity import createTTT_1D, create2Dfrom1D_vel, createRSFfromCSV_1D
from Imaging.imaging import relocate_Event

dt = 0.002  # sample rate
tshift = 0.0  # shift t0 in seconds
ishift = int(tshift / dt)  # shift in samples
time_series = np.arange(0, 5.00, dt)
noise_level = 3.0e-8 / np.sqrt(78.0)
easting_ctr, northing_ctr = 577783.6, 6278497.5
depth = 1200.0
# 1204-1211 Lower-Montney, 1122-1129 Mid-Montney, 1056-1060 Upper-Montney
freqmin, freqmax, resample = 10.0, 49.9, 100.0

stationFile = stationCSVtoRSF(stationsCSV, outStations, elevation=False, sort=False)
dr = "2019-03-13T18-15"
catalog = "synthetic_catalog_center.csv"
velcsv = "discretized_vm.csv"
vel1Drsf = "CoP_vp_1D.rsf"
vel2Drsf = "CoP_vp_2D_new.rsf"
# sampling in depth (m)
input_delta_z = 10
# depth (m), positive down
datum = -900
vpCol = 0
# maximum distance of output 2D model (m)
maxXYdist = 10000
# sampling x,y in (m)
output_delta_xy = 100
init_Vp_TTT = "CoP_init_Vp_TTT.rsf"

# create 1D rsf file from regularly sampled csv
vel1D = createRSFfromCSV_1D(
    velcsv, vel1Drsf, input_delta_z, datum=datum, col=vpCol, hasHeaders=True
)
# Extrapolate 1D to 2D velocity profile
vel2D = create2Dfrom1D_vel(
    vel1Drsf, vel2Drsf, maxXYdist, output_delta_xy, smooth=True, rect=10
)
# Create travelTime table from 2D profile, remove 2D because it is not needed or useful anymore
TTT = createTTT_1D(vel2Drsf, init_Vp_TTT, outStations, clean=False)

f = open(dr + catalog)
head = f.readline()
lines = f.readlines()
f.close()

g = open(dr + catalog.split(".")[0] + "_located.csv")
for line in lines:
    lspl = line.split(",")
    x, y, z = [float(s) for s in lspl[2:5]]
    ox = x
    oy = y
    oz = z
    oo = [oz, oy, ox]
    nn = [50, 50, 50]
    dd = [40, 40, 40]
    dat, tim = lspl[1], lspl[2]
    seedfile = "synthetic" + dat + "T" + tim + "Z.mseed"
    st = read()
    process_stream = processMseed(
        st, freqmin=freqmin, freqmax=freqmax, resample=resample, doRadRemove=True
    )
    image_file = convertMseedToRSF(process_stream, "image", pad=0)
    image_amp, rel_dp, rel_nr, rel_es = relocate_Event(
        image_file, init_Vp_TTT, outStations, nn, dd, oo, nthreads=8
    )
    g.write(line.split("\n"))

    g.write("%.4e,%.1f,%.1f,%.1f\n" % (image_amp, rel_es, rel_nr, rel_dp))
    """
    print('x difference: %.3f' % (source['x'] - rel_es))
    print('y difference: %.3f' % (source['y'] - rel_nr))
    print('z difference: %.3f' % (source['z'] - rel_dp))
    print('image amplitude: %.3e' % (image_amp))
    """

g.close()
