import matplotlib

matplotlib.use("Qt5agg")

import matplotlib.pyplot as plt
import numpy as np
from obspy import read
from obspy.signal.filter import bandpass
from numpy.fft import fft, ifft, fftfreq
from sms_ray_modelling.raytrace import isotropic_ray_trace
from scipy.integrate import cumtrapz
from pfi_fs.configuration import geology

for directory_name in [
    "15_stations_7_center",
    "15_stations_13_center",
    "20_stations_13_center",
    "20_stations_19_center",
    "40_stations_13_center",
    "40_stations_19_center",
    "60_stations_13_center",
    "60_stations_19_center",
]:
    velocity_model = [{"vp": 4000.0, "vs": 2200.0, "rho": 310.0 * 6000.0 ** (0.25)}]

    stations = {}
    f = open("Artis_rough_stations_epsg3401_60.csv")
    head = f.readline()
    for line in f.readlines():
        lspl = line.split(",")
        stations[lspl[0]] = {
            "n": float(lspl[1]),
            "e": float(lspl[2]),
            "z": float(lspl[3]),
        }

    f.close()

    source = {
        "e": np.average([s["e"] for d, s in stations.items()]),
        "n": np.average([s["n"] for d, s in stations.items()]),
        "z": -1350,
    }

    catalog = directory_name + "//synthetic_catalog.csv"
    f = open(catalog)
    head = f.readline()
    lines = f.readlines()
    events = {}
    for line in lines:
        lspl = line.split(",")
        date, time = lspl[:2]
        id = date + "T" + time.replace(":", "-") + "Z"
        events[id] = {"magnitude": float(lspl[5])}
    f.close()

    magnitude_est, magnitude_true, rms_noise, abs_max = (
        np.zeros(len(events)),
        np.zeros(len(events)),
        np.zeros(len(events)),
        np.zeros(len(events)),
    )
    for ii, (event, parameters) in enumerate(events.items()):
        seedfile = directory_name + "//synthetic" + event + ".mseed"
        stream = read(seedfile)
        magnitude_est_ind = np.zeros(len(stations))
        for jj, trace in enumerate(stream):
            data = trace.data
            sampling_rate = trace.stats.sampling_rate
            #    print(np.sqrt(np.mean(trace.data[10:110] ** 2)) * np.sqrt(42) / 4e-8)
            station = str(jj + 1)
            # fig = plt.figure()
            # ax1 = fig.add_axes([0.1, 0.1, 0.8, 0.6])
            # ax2 = fig.add_axes([0.1, 0.75, 0.8, 0.15])
            p_raypath = isotropic_ray_trace(
                source, stations[station], velocity_model, "P"
            )
            s_raypath = isotropic_ray_trace(
                source, stations[station], velocity_model, "S"
            )
            i_p = int(p_raypath["traveltime"] * sampling_rate) - 2
            i_s = int(s_raypath["traveltime"] * sampling_rate) - 2
            i_s = i_p + 10
            freqs = fftfreq(i_s - i_p, 1 / sampling_rate)
            n_pos = int((i_s - i_p) / 2.0)
            disp = bandpass(
                cumtrapz(trace.data, dx=1 / sampling_rate),
                10,
                200,
                sampling_rate,
            )
            sv2 = sum(trace.data[i_p:i_s] ** 2)
            sd2 = sum(disp[i_p:i_s] ** 2)
            area = 2 * sd2 ** (0.75) / sv2 ** (0.25)
            moment_est = (
                area
                * 4
                * np.pi
                * velocity_model[0]["vp"] ** 3
                * velocity_model[0]["rho"]
                * p_raypath["geometrical_spreading"]
                / 0.52
                / 2
            )
            magnitude_est_ind[jj] = 2 * np.log10(moment_est) / 3 - 6
            magnitude_true[ii] = parameters["magnitude"]
            rms_noise[ii] = np.sqrt(np.mean(trace.data[:100] ** 2))
            abs_max[ii] = max(abs(trace.data))
        magnitude_est[ii] = np.median(magnitude_est_ind)
    fig, ax = plt.subplots(1, 3, figsize=[18, 6])
    # ax[0].plot(magnitude_true, magnitude_est)
    ax[0].set_xlabel("true moment magnitude")
    ax[0].set_ylabel("estimated moment magnitude")
    x1, x2 = ax[0].get_xlim()
    y1, y2 = ax[0].get_ylim()
    ax_min = min(x1, y1)
    ax_max = max(x2, y2)
    ax[0].set_xlim(ax_min, ax_max)
    ax[0].set_ylim(ax_min, ax_max)
    ax[1].semilogy(magnitude_true, abs_max)
    ax[1].set_ylabel("maximum amplitude across traces")
    ax[1].set_xlabel("true moment magnitude")
    ax[1].set_ylim([10**-8, 10**-5])
    ax[2].plot(magnitude_true, 10**9 * rms_noise)
    y1, y2 = ax[2].get_ylim()
    ax[2].set_ylim(0, y2)
    fig.text(0.5, 0.95, directory_name.replace("_", " "), ha="center")
    ax[2].set_xlabel("true moment magnitude")
    ax[2].set_ylabel("effective noise level (nm/s)")
    fig.savefig("seed_check//" + directory_name + ".png")
