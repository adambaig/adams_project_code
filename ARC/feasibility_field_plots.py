import matplotlib

matplotlib.use("Qt5Agg")

import glob
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (
    MultipleLocator,
    FormatStrFormatter,
    AutoMinorLocator,
    LogLocator,
)


# assumes input files are mulitple realizations of M0s


def sqa(x):
    return np.squeeze(np.array(x))


center_east, center_north = 460237.00, 6054180.00
dx, dy = 700, 1350

wells = {}
for well in glob.glob("Well_?.csv"):
    f = open(well)
    head = f.readline()
    lines = f.readlines()
    f.close()
    wells[well.split(".")[0][-1]] = {
        "easting": np.array([float(line.split(",")[0]) for line in lines])
        - center_east,
        "northing": np.array([float(line.split(",")[1]) for line in lines])
        - center_north,
    }

decoherence_correction = 0.05
fig, ax = plt.subplots(3, sharey=True, figsize=[9, 12])
f1, a1 = plt.subplots(3, sharey=True, figsize=[9, 12])
f2, a2 = plt.subplots(3, sharey=True, figsize=[9, 12])

mag_max = -1.35
mag_min = -1.75
for i_scenario, scenario in enumerate(["15_13", "30_13", "60_19"]):
    n_stations, n_nodes = scenario.split("_")
    csvs = glob.glob(
        "Detectability Map//"
        + n_stations
        + "_stations_"
        + n_nodes
        + "_?e?n//synthetic_catalog_*.csv"
    )

    image_stats = []
    location_error = {}
    location_bias = {}
    m_detect = {}
    quad = -0.17844677939633868
    slope = 2.657618431617095
    threshold = 2.2541953390018084e-15 / np.sqrt(float(n_nodes) / 13.0)

    for csv in csvs:
        position = csv.split("\\")[-2].split("_")[3]
        nnodes = csv.split("\\")[-2].split("_")[2]
        nstations = csv.split("\\")[-2].split("_")[0]
        f = open(csv)
        lines = f.readlines()
        f.close()
        for line in lines:
            lspl = line.split(",")
            true_loc = np.array([float(s) for s in lspl[2:5]])
            magnitude, stress_drop = [float(s) for s in lspl[5:7]]
            m11, m22, m33, m12, m13, m23 = [float(s) for s in lspl[7:13]]
            amplitude = float(lspl[13])
            image_loc = np.array([float(s) for s in lspl[14:17]])
            if abs(magnitude) < 1e-7:
                image_stats.append(
                    {
                        "position": position,
                        "nstations": nstations,
                        "nnodes": nnodes,
                        "magnitude": magnitude,
                        "amplitude": amplitude,
                        "location shift": true_loc - image_loc,
                    }
                )

        location_error[position] = {
            "vertical": np.std(
                np.array(
                    [
                        i_s["location shift"][2]
                        for i_s in image_stats
                        if i_s["position"] == position
                    ]
                )
            ),
            "horizontal": np.std(
                np.sqrt(
                    [
                        x**2 + y**2
                        for x, y in [
                            i_s["location shift"][:2]
                            for i_s in image_stats
                            if i_s["position"] == position
                        ]
                    ]
                )
            ),
        }
        location_bias[position] = {
            "vertical": np.median(
                np.array(
                    [
                        i_s["location shift"][2]
                        for i_s in image_stats
                        if i_s["position"] == position
                    ]
                )
            ),
            "horizontal": np.median(
                np.sqrt(
                    [
                        x**2 + y**2
                        for x, y in [
                            i_s["location shift"][:2]
                            for i_s in image_stats
                            if i_s["position"] == position
                        ]
                    ]
                )
            ),
        }
        avg_amplitude = np.average(
            np.array(
                [i_s["amplitude"] for i_s in image_stats if i_s["position"] == position]
            )
        )
        m_detect[position] = (
            -slope
            + np.sqrt(slope * slope - 4 * quad * (np.log10(avg_amplitude / threshold)))
        ) / 2.0 / quad + decoherence_correction
    n_rows = len(m_detect)
    A_matrix, rhs_detectability = np.zeros([n_rows, 5]), np.zeros(n_rows)
    for i_row, (position, mag) in enumerate(m_detect.items()):
        x = int(position[0]) * dx
        y = int(position[2]) * dy
        A_matrix[i_row, :] = [1, x, y, x * x, y * y]
        rhs_detectability[i_row] = mag

    rhs_vrt_error, rhs_hrz_error = np.zeros(n_rows), np.zeros(n_rows)
    for i_row, (position, error) in enumerate(location_error.items()):
        rhs_vrt_error[i_row] = error["vertical"]
        rhs_hrz_error[i_row] = error["horizontal"]

    detectability_coeffs = np.linalg.lstsq(A_matrix, rhs_detectability)[0]
    vertical_error_coeffs = np.linalg.lstsq(A_matrix, rhs_vrt_error)[0]
    horizontal_error_coeffs = np.linalg.lstsq(A_matrix, rhs_hrz_error)[0]

    def interp_field(x, y, coeffs):
        return (
            coeffs[0]
            + coeffs[1] * abs(x)
            + coeffs[2] * abs(y)
            + coeffs[3] * x * x
            + coeffs[4] * y * y
        )

    stations = {}
    f = open("ARC_random_grid_" + n_stations + ".csv")
    head = f.readline()
    lines = f.readlines()
    f.close()
    stations = {}
    for line in lines:
        lspl = line.split(",")
        id = lspl[0]
        stations[id] = {
            "easting": float(lspl[2]),
            "northing": float(lspl[1]),
            "elevation": float(lspl[3]),
        }

    ax[i_scenario].set_aspect("equal")
    y_pos = np.linspace(-1000, 1000, 101)
    x_pos = np.linspace(-2400, 2400, 241)
    detect_mag = np.zeros([len(x_pos), len(y_pos)])
    hrz_error = np.zeros([len(x_pos), len(y_pos)])
    vrt_error = np.zeros([len(x_pos), len(y_pos)])

    for i_x, x in enumerate(x_pos):
        for i_y, y in enumerate(y_pos):
            detect_mag[i_x, i_y] = interp_field(x, y, detectability_coeffs)
            hrz_error[i_x, i_y] = interp_field(x, y, horizontal_error_coeffs)
            vrt_error[i_x, i_y] = interp_field(x, y, vertical_error_coeffs)
    ax[i_scenario].pcolor(
        x_pos,
        y_pos,
        detect_mag.T,
        zorder=0,
        cmap="viridis",
        vmin=mag_min,
        vmax=mag_max,
    )
    a1[i_scenario].pcolor(
        x_pos, y_pos, hrz_error.T, zorder=0, cmap="inferno", vmin=0, vmax=30
    )
    a2[i_scenario].pcolor(
        x_pos, y_pos, vrt_error.T, zorder=0, cmap="inferno", vmin=0, vmax=30
    )
    for axis in [ax[i_scenario], a1[i_scenario], a2[i_scenario]]:
        for tag, well in wells.items():
            axis.plot(
                well["easting"],
                well["northing"],
                c="0.8",
                lw=4,
                zorder=3,
                alpha=0.5,
            )
            axis.plot(
                well["easting"],
                well["northing"],
                c="0.2",
                lw=2,
                zorder=4,
                alpha=0.5,
            )
        # for id, station in stations.items():
        #     axis.plot(
        #         station["easting"],
        #         station["northing"],
        #         "h",
        #         markeredgecolor="w",
        #         c="k",
        #         zorder=5,
        #     )
        axis.set_ylabel("northing (m)")
        # axis.set_title(n_stations + " " + n_nodes + "-node superstations")
    for axis in [ax[2], a1[2], a2[2]]:
        axis.set_xlabel("easting (m)")
    print(csv)
    print(
        "vertical error %.1f: "
        % np.median([e["vertical"] for k, e in location_error.items()])
    )
    print(
        "horizontal error %.1f: "
        % np.median([e["horizontal"] for k, e in location_error.items()])
    )
    print(
        "vertical bias %.1f: "
        % np.median([e["vertical"] for k, e in location_bias.items()])
    )
    print(
        "horizonal bias %.1f: "
        % np.median([e["horizontal"] for k, e in location_bias.items()])
    )
    print("detectable magnitude: %.2f " % m_detect["0e0n"])
magnitude_range = np.linspace(mag_min, mag_max, 100)
fig_cb_detect = plt.figure(figsize=[1, 8])
cb_detect = fig_cb_detect.add_axes([0.1, 0.1, 0.3, 0.8])
cb_detect.pcolor(
    [0, 1], magnitude_range, np.vstack([magnitude_range, magnitude_range]).T
)
cb_detect.yaxis.tick_right()
cb_detect.yaxis.set_label_position("right")
cb_detect.set_xticks([])
cb_detect.set_ylabel("magnitude of detectability")

# error_range = np.linspace(0, 30, 100)
# fig_cb_error = plt.figure(figsize=[8, 1])
# cb_error = fig_cb_error.add_axes([0.1, 0.5, 0.8, 0.4])
# cb_error.pcolor(
#     error_range, [0, 1], np.vstack([error_range, error_range]), cmap="inferno"
# )
# cb_error.set_yticks([])
# cb_error.set_xlabel("location error (m)")
