import matplotlib

matplotlib.use("Qt5agg")

import glob
import matplotlib.pyplot as plt
import numpy as np
from pyproj import Transformer

transformer = Transformer.from_crs("epsg:4326", "epsg:26911")

f = open("Wells.csv")
head = f.readline()
lines = f.readlines()
f.close()

line = lines[0]

for i_well, line in enumerate(lines):
    coordinates = line.split('"')[1].split()
    g = open("Well_" + str(i_well + 1) + ".csv", "w")
    g.write("easting (m), northing (m) \n")
    coordinate = coordinates[4]
    for coordinate in coordinates:
        lon, lat, elev = [float(s) for s in coordinate.split(",")]
        easting, northing = transformer.transform(lat, lon)
        g.write("%.2f, %.2f\n" % (easting, northing))
    g.close()


fig, ax = plt.subplots()
ax.set_aspect("equal")

wells = {}
for well_file in glob.glob("Well_?.csv"):
    f = open(well_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    well_id = well_file[5]
    eastings, northings = np.zeros(len(lines)), np.zeros(len(lines))
    for i_line, line in enumerate(lines):
        eastings[i_line], northings[i_line] = [float(s) for s in line.split(",")]
    wells[well_id] = {"eastings": eastings, "northings": northings}
    ax.plot(eastings, northings, color="firebrick", lw=2)
