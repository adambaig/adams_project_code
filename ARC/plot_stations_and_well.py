import matplotlib

matplotlib.use("Qt5agg")
import glob
import matplotlib.pyplot as plt
import numpy as np
from pyproj import Transformer

from pfi_fs.configuration import geology

transformer = Transformer.from_crs("epsg:4326", "epsg:26911")

center_east, center_north = 460237.00, 6054180.00

wells = {}
for well in glob.glob("Well_?.csv"):
    f = open(well)
    head = f.readline()
    lines = f.readlines()
    f.close()
    wells[well.split(".")[0][-1]] = {
        "easting": np.array([float(line.split(",")[0]) for line in lines])
        - center_east,
        "northing": np.array([float(line.split(",")[1]) for line in lines])
        - center_north,
    }

fig, ax = plt.subplots(2, 2, figsize=[16, 14], sharex=True, sharey=True)
for axis in [ax[0, 0], ax[0, 1], ax[1, 0], ax[1, 1]]:
    for tag, well in wells.items():
        axis.plot(
            well["easting"],
            well["northing"],
            c="0.8",
            lw=4,
            zorder=3,
            alpha=0.5,
        )
        axis.plot(
            well["easting"],
            well["northing"],
            c="0.2",
            lw=2,
            zorder=4,
            alpha=0.5,
        )
station_files = glob.glob("ARC_random_grid_??.csv")
for i_file, station_file in enumerate(station_files):
    i_row = i_file // 2
    i_col = i_file % 2
    f = open(station_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    n_lines = len(lines)
    east, north = np.zeros(n_lines), np.zeros(n_lines)
    for i_line, line in enumerate(lines):
        lspl = line.split(",")
        north[i_line], east[i_line] = [float(s) for s in lspl[1:3]]
    a = ax[i_row, i_col]
    a.set_aspect("equal")
    a.plot(east - center_east, north - center_north, "v", zorder=6)

f = open("ARC_pilot_grid.csv")
head = f.readline()
lines = f.readlines()
f.close()
n_lines = len(lines)
east, north = np.zeros(n_lines), np.zeros(n_lines)
for i_line, line in enumerate(lines):
    dum, lat, lon, elev = [float(s) for s in line.split(",")]
    east[i_line], north[i_line] = transformer.transform(lat, lon)
ax[1, 1].plot(east - center_east, north - center_north, "v", zorder=6)
ax[1, 1].set_aspect("equal")

ax[1, 0].set_xlabel("easting (m)")
ax[1, 1].set_xlabel("easting (m)")
ax[0, 0].set_ylabel("northing (m)")
ax[1, 0].set_ylabel("northing (m)")

Q, velocity_model = geology("geology.cfg")

tops = [880]
vp = [velocity_model[0]["vp"], velocity_model[0]["vp"]]
vs = [velocity_model[0]["vs"], velocity_model[0]["vs"]]

for layer in velocity_model[1:]:
    for ii in range(2):
        tops.append(layer["top"])
        vp.append(layer["vp"])
        vs.append(layer["vs"])
tops.append(layer["top"] - 1000)
v_fig, v_ax = plt.subplots()
v_ax.plot(vp, tops, "firebrick")
v_ax.plot(vs, tops, "royalblue")
v_ax.plot([0, 6000], [-1320, -1320], "darkgoldenrod", lw=3)
v_ax.text(500, -1270, "target depth\n2200 m TVD")
v_ax.plot([0, 6000], [880, 880], "k")
v_ax.set_ylim([-1600, 1000])
v_ax.set_xlim([0, 6000])
v_ax.legend(["$v_p$", "$v_s$"])
v_ax.set_xlabel("velocity (m/s)")
v_ax.set_ylabel("elevation above sea level (m)")
