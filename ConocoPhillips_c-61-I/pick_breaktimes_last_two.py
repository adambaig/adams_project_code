import matplotlib.pyplot as plt
import json
from datetime import datetime
from read_inputs import read_wells
from pfi_qi.engineering import breaktimes_by_stage, sorted_stage_list
import matplotlib

matplotlib.use("Qt5agg")


with open("treatment_data.json") as f:
    treatment_data = json.load(f)

# sorted_stages = sorted_stage_list(treatment_data)

with open("sorted_stages.json") as f:
    sorted_stages = json.load(f)
    # json.dump(sorted_stages,f)

wells = read_wells()

plug_and_perf_stages = [stage for stage in sorted_stages if stage["well"] == "C-A061-I"]
plug_and_perf_breaktimes = breaktimes_by_stage(plug_and_perf_stages, treatment_data)
sliding_sleeve_stages = [
    stage for stage in sorted_stages if stage["well"] != "C-A061-I"
]


sliding_sleeve_breaktimes = {}
for ss_stage in sliding_sleeve_stages:
    if ss_stage["well"] == "C-C061-I" and ss_stage["stage"] in ["3", "4"]:
        data_for_stage = {
            k: v
            for k, v in treatment_data.items()
            if v["well"] == ss_stage["well"] and v["stage"] == ss_stage["stage"]
        }
        if ss_stage["well"] not in sliding_sleeve_breaktimes:
            sliding_sleeve_breaktimes[ss_stage["well"]] = {}

        proppant = [
            v["downhole_proppant_concentration"] / 100 for v in data_for_stage.values()
        ]
        slurry_rate = [v["slurry_rate"] / 20 for v in data_for_stage.values()]
        pressure = [v["pressure"] / 40 for v in data_for_stage.values()]
        times = [datetime.strptime(k, "%Y%m%d%H%M%S.%f") for k in data_for_stage]
        fig, ax = plt.subplots()
        ax.plot(times, proppant)
        ax.plot(times, slurry_rate)
        ax.plot(times, pressure)
        approx_break = fig.ginput()
        approx_time = datetime.utcfromtimestamp(approx_break[0][0] * 86400)
        data_after_pick = {
            k: v
            for k, v in data_for_stage.items()
            if datetime.strptime(k, "%Y%m%d%H%M%S.%f") > approx_time
        }
        slurry_rate_at_pick = list(data_after_pick.values())[0]["slurry_rate"]
        slurry_rate_after_pick = [
            k
            for k, v in data_after_pick.items()
            if v["slurry_rate"] > 1.2 * slurry_rate_at_pick
        ]
        if len(slurry_rate_after_pick) > 0:
            sliding_sleeve_breaktimes[ss_stage["well"]][
                ss_stage["stage"]
            ] = slurry_rate_after_pick[0]
        plt.close(fig)

# breaktimes = {**plug_and_perf_breaktimes, **sliding_sleeve_breaktimes}

with open("breaktimes.json") as f:
    breaktimes = json.load(f)


breaktimes["C-C061-I"] = {
    **breaktimes["C-C061-I"],
    **sliding_sleeve_breaktimes["C-C061-I"],
}

with open("breaktimes.json", "w") as f:
    json.dump(breaktimes, f)
