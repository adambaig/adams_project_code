from read_inputs import read_wells, read_final_catalog, read_stations
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

matplotlib.use("Qt5agg")


events = read_final_catalog()
wells = read_wells()
stations = read_stations()

{k: v for k, v in events.items() if v["well id"] == "C-C061-I" and v["stage"] == "4"}

fig, ax = plt.subplots(figsize=[16, 6])
ax.set_aspect("equal")

for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k")

for station in stations.values():
    ax.plot(station["easting"], station["northing"], "bh")

new_stations = fig.ginput(n=15)


with open("fake_new_stations.csv", "w") as f:
    f.write("station, easting (m), northing (m), elevation (m)\n")
    for i_station, station in enumerate(new_stations):
        f.write(f"{31+i_station},{station[0]:.1f}, {station[1]:.1f}, 800\n")

# for well_id, well in wells.items():
#     with open(f'wells//{well_id}.txt','w') as f:
#         f.write('easting (m),northing (m), elev (m)\n')
#         for e,n,z in zip(well['easting'], well['northing'], well['elevation']):
#             f.write(f"{e:.1f},{n:.1f},{z:.1f}\n")
#
