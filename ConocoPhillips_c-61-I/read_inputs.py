import pickle
import matplotlib.pyplot as plt
import json
from datetime import datetime
import glob
import os
import pytz

import numpy as np
from sms_moment_tensor.MT_math import mt_vector_to_matrix


UTC = pytz.utc
TIMEZONE = pytz.timezone("America/Edmonton")


def read_wells():
    wells = {}
    well_files = glob.glob("wells//200*.txt")
    for well_file in well_files:
        with open(well_file) as f:
            _head = [f.readline() for i in range(17)]
            lines = f.readlines()
        well_name = os.path.basename(well_file).split("_")[1].split(".")[0]
        md, easting, northing, elevation = np.array(
            [[float(line.split()[i]) for i in range(4)] for line in lines]
        ).T
        wells[well_name] = {
            "md": md,
            "easting": easting,
            "northing": northing,
            "elevation": elevation,
        }
    with open("wells//C061I_Perf_Sleeve_Depths_2.csv") as f:
        _head = f.readline()
        perf_lines = f.readlines()
    for line in perf_lines:
        lspl = line.split(",")
        well = lspl[0].split()[-1].split("/")[0]
        stage = lspl[5]
        cluster = lspl[6]
        top_md = float(lspl[7])
        bottom_md = float(lspl[8])
        if well in wells:
            cluster_info = {
                "top_east": np.interp(
                    top_md, wells[well]["md"], wells[well]["easting"]
                ),
                "top_north": np.interp(
                    top_md, wells[well]["md"], wells[well]["northing"]
                ),
                "top_elevation": np.interp(
                    top_md, wells[well]["md"], wells[well]["elevation"]
                ),
                "bottom_east": np.interp(
                    bottom_md, wells[well]["md"], wells[well]["easting"]
                ),
                "bottom_north": np.interp(
                    bottom_md, wells[well]["md"], wells[well]["northing"]
                ),
                "bottom_elevation": np.interp(
                    bottom_md, wells[well]["md"], wells[well]["elevation"]
                ),
            }
            stage_name = f"Stage {stage}"
            if stage_name not in wells[well]:
                wells[well][f"Stage {stage}"] = {}
            wells[well][f"Stage {stage}"][f"Cluster {cluster}"] = cluster_info
    return wells


def read_treatment():
    with open("completion info\MONTNEY_DC_WV_FRAC_SENSORDATA_Seconds.csv") as f:
        _head = f.readline()
        lines = f.readlines()
    treatment_data = {}
    for line in lines:
        lspl = line.split(",")
        timestamp = TIMEZONE.localize(datetime.strptime(lspl[1], "%Y-%m-%d %H:%M:%S"))
        utc_timestamp = datetime.strftime(
            timestamp.astimezone(UTC), "%Y%m%d%H%M%S.000000"
        )
        treatment_data[utc_timestamp] = {
            "well": lspl[2].split()[-1].split("/")[0],
            "stage": lspl[3],
            "pressure": float(lspl[4]),
            "slurry_rate": float(lspl[6]),
            "downhole_proppant_concentration": float(lspl[8]),
        }
    return treatment_data


def getVelfromTTT(TTT):
    velocity_model = []
    top = -1 * TTT["originz"]
    d1 = TTT["dz"]
    vel = TTT["vel1D"]
    for vp in vel:
        vs = vp / 1.73205080757
        rho = 310 * (vp**0.25)
        curlayer = {"vp": vp, "top": top, "vs": vs, "rho": rho}
        top = top - d1
        velocity_model.append(curlayer)
    return velocity_model


def smooth_regularly_sampled_velocity_model(velocity_model, smoothing_distance=500):
    smoothed_velocity_model = []
    initial_dz = abs(velocity_model[0]["top"] - velocity_model[1]["top"])
    n_smoothing_layers = int(smoothing_distance / initial_dz)
    i_layer = n_smoothing_layers // 2
    for i_layer, layer in enumerate(
        velocity_model[n_smoothing_layers // 2 : -n_smoothing_layers // 2]
    ):
        layers_to_smooth = velocity_model[i_layer : i_layer + n_smoothing_layers]
        average_vp = np.average([v["vp"] for v in layers_to_smooth])
        average_vs = np.average([v["vs"] for v in layers_to_smooth])
        average_rho = np.average([v["rho"] for v in layers_to_smooth])
        smoothed_velocity_model.append(
            {
                "top": layer["top"],
                "vp": float(average_vp),
                "vs": float(average_vs),
                "rho": float(average_rho),
            }
        )
    return smoothed_velocity_model


def read_final_catalog(filename=r"Catalogs\COP_C61-I_final_catalog.csv"):
    events = {}
    with open(filename) as f:
        _head = f.readline()
        lines = f.readlines()
    for line in lines:
        split_line = line.split(",")
        event_id = split_line[0]
        time_str, microseconds_str = split_line[1].split(".")
        if len(microseconds_str) < 6:
            microseconds_str.ljust(6, "0")
        events[event_id] = {
            "UTC Date Time": UTC.localize(
                datetime.strptime(
                    time_str + "." + microseconds_str, "%Y-%m-%d %H:%M:%S.%f"
                )
            ),
            # "serial time": float(split_line[2]),
            "easting": float(split_line[3]),
            "northing": float(split_line[4]),
            "elevation": -float(split_line[5]),
            "log SNR": float(split_line[6]),
            "easting uncertainty": float(split_line[7]),
            "northing uncertainty": float(split_line[8]),
            "elevation uncertainty": float(split_line[9]),
            "Mw": float(split_line[10]),
            "corner frequency": float(split_line[11]),
            "moment": float(split_line[12]),
            "well name": split_line[13],
            "well id": split_line[14],
            "stage": split_line[15],
            "stage id": int(split_line[16]),
            "horizontal distance": float(split_line[17]),
            "total distance": float(split_line[18]),
            "elapsed time": float(split_line[19]),
            "is flipped": split_line[20] == "TRUE",
            "general MT": mt_vector_to_matrix([float(v) for v in split_line[21:27]]),
            "general MT R": float(split_line[27]),
            "general MT CN": float(split_line[28]),
            "clvd": float(split_line[29]),
            "iso": float(split_line[30]),
            "dc": float(split_line[31]),
            "DC MT": mt_vector_to_matrix([float(v) for v in split_line[32:38]]),
            "DC MT R": float(split_line[38]),
            "DC MT CN": float(split_line[39]),
            "MT confidence": float(split_line[40]),
            "note": split_line[53],
            "event type": split_line[54].strip(),
        }
    return events


def read_stations():
    with open("Superstations.csv") as f:
        _head = f.readline()
        lines = f.readlines()
    stations = {}
    for line in lines:
        split_line = line.split(",")
        stations[split_line[0]] = {
            "easting": float(split_line[1]),
            "northing": float(split_line[2]),
            "elevation": float(split_line[3]),
        }
    return stations
