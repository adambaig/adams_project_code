import glob

import matplotlib.pyplot as plt
import numpy as np
import pyproj as pr


latlon_proj = pr.Proj(init="epsg:4236")
out_proj = pr.Proj(init="epsg:26710")


east_center, north_center = 591282.461, 6286338.056

wells = {}
for well_file in glob.glob("*latlon.csv"):
    f = open(well_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    well = well_file.split("_latlon")[0]
    n_lines = len(lines)
    lat, lon, elev = np.zeros(n_lines), np.zeros(n_lines), np.zeros(n_lines)
    f = open(f"well_{well}_en.csv", "w")
    f.write("easting (m), northing (m), elev (m)\n")
    for i_line, line in enumerate(lines):
        lon[i_line], lat[i_line], elev[i_line] = [float(s) for s in line.split(",")]
    easting, northing = pr.transform(latlon_proj, out_proj, lon, lat)
    for i_line, line in enumerate(lines):
        f.write(f"{easting[i_line]:.3f}, {northing[i_line]:.3f}, {elev[i_line]:.3f}\n")
    wells[well] = {"easting": easting, "northing": northing, "elevation": elev}

    f.close()

station_files = glob.glob("Stations//*_latlon.csv")

for station_file in station_files:
    f = open(station_file)
    head = f.readline()
    lines = f.readlines()
    f.close()
    n_lines = len(lines)
    g = open(station_file.replace("_latlon.", "."), "w")
    g.write("station,easting (m), northing (m), elev (m)\n")
    easting, northing = np.zeros(n_lines), np.zeros(n_lines)
    lat, lon, elev = np.zeros(n_lines), np.zeros(n_lines), np.zeros(n_lines)
    for i_station, line in enumerate(lines):
        lon[i_station], lat[i_station], elev[i_station] = [
            float(s) for s in line.split(",")[1:]
        ]
    easting, northing = pr.transform(latlon_proj, out_proj, lon, lat)
    stations = {}
    for i_station, line in enumerate(lines):
        station = line.split(",")[0]
        stations[station] = {
            "easting": easting[i_station],
            "northing": northing[i_station],
            "elevation": elev[i_station],
        }
        g.write(
            f"{station},{stations[station]['easting']:.3f}, {stations[station]['northing']:.3f}, {stations[station]['elevation']:.3f}\n"
        )
    g.close()
    f.close()

fig, ax = plt.subplots()
ax.set_aspect("equal")
[ax.plot(well["easting"], well["northing"], "0.25", lw=2) for well in wells.values()]
[
    ax.plot(
        station["easting"],
        station["northing"],
        "h",
        color="forestgreen",
        markeredgecolor="k",
    )
    for station in stations.values()
]
plt.show()
