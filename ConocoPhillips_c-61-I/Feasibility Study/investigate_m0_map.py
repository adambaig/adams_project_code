import glob
import importlib
import logging
import matplotlib.pyplot as plt
import numpy as np
import os
from progress.bar import IncrementalBar
from shutil import copy2, move
import sys


from pfi_fs.simulate_events import generate_1Csuperstation_waveforms
from pfi_fs.configuration import read_synthetic_catalog


total_geophones = 6*13*20
spacing = 400
f = open('Detectability_Map//detectability_parameters.csv')
quad,slope,threshold = [float(line.split(',')[1]) for line in f.readlines()]
f.close()


def calc_detectability_magnitude(amp):
    return (-slope + np.sqrt(slope * slope - 4 * quad * (np.log10(amp / threshold)))) / 2.0 / quad

def interp_field_no_linear(x, y, coeffs):
    return coeffs[0] + coeffs[1] * x * x + coeffs[2] * y * y

map_directories = glob.glob('Detectability_Map//central*')

M0_events = {}
for point in map_directories:
    M0_events[point.split('_')[-1]] = list(read_synthetic_catalog(point, relocated=True).values())


amps,i_x,i_y = np.array([(v[0]['image_amplitude'],int(v[0]['e']/400),int(v[0]['n']/400)) for v in M0_events.values()]).T

m_detect = np.zeros([4,12])
n_rows = 48
A_matrix, rhs_detectability = np.zeros([n_rows, 3]), np.zeros(n_rows)


for i_row,(ii,jj,amp) in enumerate(zip(i_x,i_y,amps)):
    m_detect[int(ii),int(jj)] = calc_detectability_magnitude(total_geophones*amp)
    A_matrix[i_row,:] = [1,ii*spacing**2,jj*spacing**2]
    rhs_detectability[i_row] = calc_detectability_magnitude(total_geophones*amp)


detectability_coeffs = np.linalg.lstsq(A_matrix, rhs_detectability)[0]

interp_field_no_linear(0,0,detectability_coeffs)


fig,ax= plt.subplots()
ax.set_aspect('equal')
ax.pcolor(m_detect.T)

plt.plot(m_detect[0,:])
plt.
