from generalPlots import gray_background_with_grid
from read_inputs import read_wells
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np

TAB10 = cm.get_cmap("tab10")


def get_tab10_color(stage_name):
    stage_int = int(stage_name[-1])
    return TAB10(((stage_int - 0.1) / 10) % 1)


wells = read_wells()

fig, ax = plt.subplots(figsize=[20, 20])
ax.set_aspect("equal")
for well in wells.values():
    ax.plot(well["easting"], well["northing"], "k")

    for stage, clusters in {k: v for k, v in well.items() if "Stage" in k}.items():
        color = get_tab10_color(stage)
        for cluster_name, cluster in clusters.items():
            ax.plot(
                [cluster["top easting"], cluster["bottom easting"]],
                [cluster["top northing"], cluster["bottom northing"]],
                color=color,
                lw=10,
                zorder=4,
            )

gray_background_with_grid(ax)
