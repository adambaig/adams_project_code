from collections import OrderedDict
from datetime import datetime
from itertools import accumulate
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.dates import DateFormatter
from matplotlib.colors import Normalize
import operator

# from pfi_qi.rotations import rotate_from_cardinal

from read_inputs import read_final_catalog


events = read_final_catalog(
    r"Catalogs\COP_C61-I_final_catalog_wStagesNoInterp_updateMw.csv"
)

all_frac_events = {
    k: v
    for k, v in events.items()
    if v["event type"] == "Frac Event" and v["well id"] != "-1"
}

tab10 = cm.get_cmap("tab10")

fig, ax = plt.subplots()

magbins = np.arange(-2.4, 1.5, 0.1)

ax.hist(
    [v["Mw"] for v in all_frac_events.values()],
    bins=magbins,
    edgecolor="0.8",
    facecolor=tab10(0.05),
)

ax.set_yscale("log")
ax.set_ylim([0.7, 6000])
ax.set_xlabel("moment magnitude")
ax.set_ylabel("Number of events")
ax.set_title("C-061-I magnitude distribution")
fig.savefig("C61I_mags.png")
len([v["Mw"] for v in all_frac_events.values() if v["Mw"] > -0.5])
16 / (159)
