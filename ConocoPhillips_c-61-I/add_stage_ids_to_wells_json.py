import json

from read_inputs import read_final_catalog


events = read_final_catalog()
list(events.values())[0]

with open("wells.json") as f:
    wells = json.load(f)


stages = {}
for event in events.values():
    if event["stage id"] not in stages:
        stages[event["stage id"]] = {"well": event["well id"], "stage": event["stage"]}

# with open('stage_mapping.json','w') as f:
#     json.dump(stages, f)
#
with open("stage_mapping.json") as f:
    stage_id_mapping = json.load(f)
i = 0
for well_id, well in wells.items():
    for stage in well:
        if "Stage" in stage:
            i += 1
            stage_number = stage.split()[1]
            stage_id = [
                k
                for k, v in stage_id_mapping.items()
                if v["well"] == well_id and v["stage"] == stage_number
            ]
            # print(well_id, stage, stage_id)
            well[stage]["stage id"] = int(stage_id[0])


with open("wells_with_stage_id.json", "w") as f:
    json.dump(wells, f)
