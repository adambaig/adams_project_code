import json
import matplotlib.pyplot as plt
import numpy as np

with open("noise_floor_300_slices.json") as f:
    noise_floor = json.load(f)


noise_values = []

for datetime, station_noise in noise_floor.items():
    for station, noise in station_noise.items():
        noise_values.append(noise)


noise_bins = np.arange(0, 1e-6, 1e-8)
noise_bins
plt.hist(noise_values, bins=noise_bins)


np.percentile(noise_values, 80)
