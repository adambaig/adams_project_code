from obspy.io.segy.core import _read_segy
import matplotlib.pyplot as plt
import numpy as np

from obspy import read
from segypy import segypy

header = segypy.getSegyHeader("00016926.sgy")

times = st[0].times()
for tr in st:
    fig, ax = plt.subplots()
    ax.plot(times, tr.data / 19 / 16)


st = _read_segy("00016926.sgy", unpack_trace_headers=True)

list(st[100].stats.segy.trace_header.items())


fig, ax = plt.subplots(figsize=[16, 16])
ax.set_aspect("equal")
for tr in st[:57]:
    easting = tr.stats.segy.trace_header["group_coordinate_x"]
    northing = tr.stats.segy.trace_header["group_coordinate_y"]
    ax.plot(easting, northing, "kh")
