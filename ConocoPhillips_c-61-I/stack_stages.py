from datetime import datetime
import json
import pytz

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as mcolors
from obspy import UTCDateTime
import scipy.stats as st

from pfi_qi.QI_analysis import calculate_dimensions, fracture_dimensions
from pfi_qi.engineering import calc_well_trend, find_perf_center
from pfi_qi.rotations import rotate_from_cardinal

from generalPlots import gray_background_with_grid
from read_inputs import read_final_catalog, read_wells
from pfi_qi.engineering import calc_well_trend, find_perf_center
from pfi_qi.rotations import pca_plan_view_trend

UTC = pytz.utc


events = read_final_catalog(
    r"Catalogs\COP_C61-I_final_catalog_wStagesNoInterp_updateMw.csv"
)
wells = read_wells()
angle = calc_well_trend(wells["C-D061-I"])

well_names, stage_names = np.array(
    [
        s.split(":sep:")
        for s in np.unique(
            [v["well id"] + ":sep:" + v["stage"] for v in events.values()]
        )
    ]
).T

recentered_events = {}


for well, stage in zip(well_names, stage_names):
    if well != "-1":
        stage_events = {
            k: v
            for k, v in events.items()
            if v["stage"] == stage and v["well id"] == well
        }

        perf_center = find_perf_center(stage, wells[well])
        recentered_events = {
            **recentered_events,
            **rotate_from_cardinal(stage_events, 0, perf_center),
        }


fig, ax = plt.subplots()

ax.set_aspect("equal")
# ax.plot(
#     [v['easting'] for v in recentered_events.values() if v['well id'] == 'C-A061-I'],
#     [v['northing'] for v in recentered_events.values() if v['well id'] == 'C-A061-I'],
#
#     '.'
# )
51 / (49 + 55 + 55)

northings_A = [
    v["along trend"] for v in recentered_events.values() if v["well id"] == "C-A061-I"
]
eastings_A = [
    v["perp to trend"] for v in recentered_events.values() if v["well id"] == "C-A061-I"
]
ax.plot(eastings_A, northings_A, ".")
pca_plan_view_trend(eastings_A, northings_A) * 180.0 / 3.14159

northings_C = [
    v["along trend"] for v in recentered_events.values() if v["well id"] == "C-C061-I"
]
eastings_C = [
    v["perp to trend"] for v in recentered_events.values() if v["well id"] == "C-C061-I"
]
ax.plot(eastings_C, northings_C, ".")
pca_plan_view_trend(eastings_C, northings_C) * 180.0 / 3.14159


len(northings_A)

center = {
    "easting": wells["C-D061-I"]["easting"][0],
    "northing": wells["C-D061-I"]["northing"][0],
}
wells = rotate_from_cardinal(wells, angle, center)
tab10 = cm.get_cmap("tab10")
well_colors = {
    "C-A061-I": "firebrick",
    "C-C061-I": "steelblue",
    "C-D061-I": "darkslateblue",
}

with open("breaktimes.json") as f:
    breaktimes = json.load(f)


def make_simple_gradient_from_white_cmap(end_color):
    red, green, blue = mcolors.to_rgb(end_color)
    cdict = {
        "red": [(0.0, 1.0, 1.0), (1, red, red)],
        "green": [(0.0, 1.0, 1.0), (1, green, green)],
        "blue": [(0.0, 1.0, 1.0), (1, blue, blue)],
    }
    return mcolors.LinearSegmentedColormap("custom", cdict, 256)


find_perf_center("32", wells[well])

list(events.values())[199]

stage_events = {
    event_id: event
    for event_id, event in events.items()
    if event["stage"] == "11" and event["well name"] == "C-A061-I"
}


def add_stage_reference_for_xyt(breaktimes, events):
    for well, stages in breaktimes.items():
        well_angle = calc_well_trend(wells[well])
        for stage, breaktime in stages.items():
            stage_events = {
                event_id: event
                for event_id, event in events.items()
                if event["stage"] == stage and event["well id"] == well
            }
            if len(stage_events) > 0:
                perf_center = find_perf_center(stage, wells[well])
                rotate_from_cardinal(stage_events, well_angle, perf_center)
                break_datetime = UTC.localize(
                    datetime.strptime(breaktime, "%Y%m%d%H%M%S.%f")
                )
                for event in stage_events.values():
                    event["seconds in stage"] = (
                        event["UTC Date Time"] - break_datetime
                    ).days * 86400 + (event["UTC Date Time"] - break_datetime).seconds


events = rotate_from_cardinal(events, angle, center)


frac_events = {
    event_id: event
    for event_id, event in events.items()
    if event["event type"] == "Frac Event"
}
add_stage_reference_for_xyt(breaktimes, stage_events)


for well in wells:
    well_events = {
        event_id: event
        for event_id, event in frac_events.items()
        if event["well id"] == well
        and "seconds in stage" in event
        and event["seconds in stage"] > 0
    }

    fig, ax = plt.subplots(figsize=[5, 5])
    sign_flip = 1 if well == "C-A061-I" else -1
    max_time = 10800 if well == "C-A061-I" else 3600
    for event in well_events.values():
        event["r"] = sign_flip * event["perp to trend"]
    ax.plot(
        [event["r"] for event in well_events.values()],
        [event["seconds in stage"] for event in well_events.values()],
        ".",
        color="0.2",
        zorder=3,
    )
    xx, yy = np.mgrid[-500:500:200j, 0:max_time:200j]
    positions = np.vstack([xx.ravel(), yy.ravel()])
    values_A = np.vstack(
        [
            np.array([v["r"] for v in well_events.values()]),
            [v["seconds in stage"] for v in well_events.values()],
        ]
    )
    kernel_A = st.gaussian_kde(values_A)
    contour_A = np.reshape(kernel_A(positions).T, xx.shape)
    positions = np.vstack([xx.ravel(), yy.ravel()])
    cf_set_A = ax.contourf(
        xx,
        yy,
        contour_A,
        cmap=make_simple_gradient_from_white_cmap(well_colors[well]),
        zorder=-10,
        alpha=0.9,
    )
    ax.set_title(well)
    y_ticks = np.linspace(0, max_time, 7)
    ax.set_yticks(y_ticks)
    diff_sq_neg = [
        v["r"] ** 2 / v["seconds in stage"] for v in well_events.values() if v["r"] > 0
    ]
    diff_sq_pos = [
        v["r"] ** 2 / v["seconds in stage"] for v in well_events.values() if v["r"] <= 0
    ]
    diff_neg = np.sqrt(np.percentile((diff_sq_neg), 95))
    diff_pos = np.sqrt(np.percentile((diff_sq_pos), 95))
    ax.plot(np.sqrt(np.arange(10800)) * diff_pos, np.arange(0, 10800), "k:")
    ax.plot(-np.sqrt(np.arange(10800)) * diff_neg, np.arange(0, 10800), "k:")
    ax.text(480, 0.9 * max_time, f"D = {diff_pos:.1f} m$^2$/s", ha="right")
    ax.text(-480, 0.9 * max_time, f"D = {diff_neg:.1f} m$^2$/s", ha="left")
    ax.set_xlim([-500, 500])
    ax.set_ylim([max_time, 0])
    ax.set_yticklabels(np.linspace(0, max_time / 60, 7))
    ax.set_ylabel("elapsed time in stage (mins)")
    ax.set_xlabel("distance from well (m)")
    fig.savefig(f"r_vs_t_contour_plot_{well}.png")
