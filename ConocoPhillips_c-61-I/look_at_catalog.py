import matplotlib.pyplot as plt
import numpy as np
from obspy.imaging.beachball import beach

from generalPlots import gray_background_with_grid
from mtPlots import plot_strike_dip_rake_rosettes, plot_lune_from_events_dictionary
from sms_moment_tensor.MT_math import mt_to_sdr

from read_inputs import read_wells, read_final_catalog, read_stations


MT_CONFIDENCE_THRESHOLD = 0.95
MT_WIDTH = 300
events = read_final_catalog()
wells = read_wells()
stations = read_stations()

event_types = np.unique([v['event type'] for v in events.values()])
frac_colors = {
    'Frac Event': 'firebrick',
    'Lower NE Fault Event': 'forestgreen',
    'Lower NW Fault Event': 'mediumorchid',
    'Upper NW Feature Event': 'darkgoldenrod'
    'Off Zone': '0.5',
}
well_colors = {
    'C-A061-I': 'firebrick',
    'C-C061-I': 'steelblue',
    'C-D061-I': 'darkslateblue',
    '-1': '0.2'
}

tab10 = cm.get_cmap('tab10')


patch = {}
fig_legend, ax_legend = plt.subplots()
for label, color in well_colors.items():
    if label != '-1':
        patch[label], = ax_legend.plot(0, 0, '.', color=color)
for label, color in frac_colors.items():
    if label != "Frac Event":
        patch[label], = ax_legend.plot(0, 0, '.', color=color)

ax_legend.legend(patch.values(), ['Frac Event', 'NE Fault', 'Lower NW Fault'])

for event in events.values():
    if event['event type'] == 'Frac Event' and event['well id'] == '-1':
        event['event type'] = 'Off Zone'
good_mt_events = {k: v for k, v in events.items() if v['MT confidence'] > MT_CONFIDENCE_THRESHOLD}


fig_lune_types, ax = plt.subplots(1, 4, figsize=[12, 8])
for i_type, event_type in enumerate(event_types[:4]):
    subset_mts = {k: v for k, v in good_mt_events.items() if v['event type'] == event_type}
    fig_rosette = plot_strike_dip_rake_rosettes(
        subset_mts, mt_key='DC MT', color=frac_colors[event_type], bin_degrees=5)
    plot_lune_from_events_dictionary(ax[i_type], subset_mts, color=frac_colors[event_type])
    fig_rosette.savefig(f"rosette_{event_type.replace(' ','_')}.png")
    print(event_type, len(subset_mts))

fig_lune_types.savefig('lunes_by_type.png')


fig_lune_well, ax = plt.subplots(1, 4, figsize=[12, 8])
frac_mts = {k: v for k, v in good_mt_events.items() if v['event type'] == 'Frac Event'}
for i_well, well in enumerate(['C-A061-I', 'C-C061-I', 'C-D061-I', '-1']):
    subset_mts = {k: v for k, v in frac_mts.items() if v['well id'] == well}
    fig_rosette = plot_strike_dip_rake_rosettes(subset_mts, mt_key='DC MT', color=well_colors[well])
    plot_lune_from_events_dictionary(ax[i_well], subset_mts, color=well_colors[well])
    fig_rosette.savefig(f"rosette_{well.replace(' ','_')}.png")
    print(well, len(subset_mts))
fig_lune_well.savefig('lunes_by_well')

easting_1, easting_2 = 590500, 593500
northing_1, northing_2 = 6283500, 6291000
northing_2 - northing_1
easting_2 - easting_1
elevation_1, elevation_2 = -1600, -600
fig_onpad, ax_onpad = plt.subplots(figsize=[12, 12])
fig_offpad, ax_offpad = plt.subplots(figsize=[12, 12])
fig_map = plt.

plt.hist([event['elevation'] for event in events.values()])

fig_map = plt.figure(figsize=[12, 12])
ax_map =


for ax in [ax_onpad, ax_offpad, ax_map]:
    ax.set_aspect('equal')
    for well in wells.values():
        ax.plot(well['easting'], well['northing'], 'k', zorder=-3)
for event in events.values():
    if event['event type'] == 'Frac Event':
        ax_onpad.plot(event['easting'], event['northing'], '.',
                      color=well_colors[event['well id']], zorder=1)
        ax_offpad.plot(event['easting'], event['northing'], '.',
                       color=well_colors[event['well id']], zorder=1)
        ax_map.plot(event['easting'], event['northing'], '.',
                    color=well_colors[event['well id']], zorder=1)
        if event['MT confidence'] > MT_CONFIDENCE_THRESHOLD:
            strike, dip, rake = mt_to_sdr(event['DC MT'], conjugate=False)
            beachball = beach(
                np.array([strike, dip, rake]),
                xy=(event["easting"], event["northing"]),
                width=MT_WIDTH,
                facecolor=well_colors[event['well id']],
                alpha=0.8,
                linewidth=0.25,
                zorder=5,
            )
            ax_onpad.add_collection(beachball)
    else:
        ax_onpad.plot(event['easting'], event['northing'], '.',
                      color=frac_colors[event['event type']], zorder=1)
        ax_offpad.plot(event['easting'], event['northing'], '.',
                       color=frac_colors[event['event type']], zorder=1)
        ax_map.plot(event['easting'], event['northing'], '.',
                    color=frac_colors[event['event type']], zorder=1)
        if event['MT confidence'] > MT_CONFIDENCE_THRESHOLD:
            strike, dip, rake = mt_to_sdr(event['DC MT'], conjugate=False)
            beachball = beach(
                np.array([strike, dip, rake]),
                xy=(event["easting"], event["northing"]),
                width=MT_WIDTH,
                facecolor=frac_colors[event['event type']],
                alpha=0.8,
                linewidth=0.25,
                zorder=3,
            )
            ax_offpad.add_collection(beachball)
for ax in [ax_onpad, ax_offpad, ax_map]:
    ax.set_xlim(easting_1, easting_2)
    ax.set_ylim(northing_1, northing_2)
    gray_background_with_grid(ax)

fig_onpad.savefig('beachballs_by_type_and_well_onpad.png', bbox_inches='tight')
fig_offpad.savefig('beachballs_by_type_and_well_offpad.png', bbox_inches='tight')
fig_map.savefig('events_by_type.png', bbox_inches='tight')


for
