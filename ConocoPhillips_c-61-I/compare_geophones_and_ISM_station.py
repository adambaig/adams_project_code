import glob
from datetime import datetime
import matplotlib.pyplot as plt
from obspy.io.segy.core import _read_segy
from obspy import read, read_inventory, UTCDateTime
import pyproj as pr

import pytz

from read_inputs import read_wells, read_final_catalog
from generalPlots import gray_background_with_grid

UTC = pytz.utc

inv = read_inventory(r"Seeds\FromAthena\CP_ESA_Full.xml")
seed = read(r"Seeds\FromAthena\event_2021-07-31T15_45_11.740Z.seed")
st = seed.remove_response(inv)

wells = read_wells()

ism35 = inv.select(station="ISM35")

out_proj = pr.Proj("epsg:26710")
latlon_proj = pr.Proj(init="epsg:4326")
ism35_latitude = ism35[0][0].latitude
ism35_longitude = ism35[0][0].longitude

ism35_easting, ism35_northing = pr.transform(
    latlon_proj, out_proj, ism35_longitude, ism35_latitude
)


events = read_final_catalog()

filtered_ISM_station_data = (
    st.select(station="ISM35", channel="??Z")
    .filter("bandpass", freqmin=20, freqmax=60)
    .trim(
        starttime=UTCDateTime(2021, 7, 31, 15, 45),
        endtime=UTCDateTime(2021, 7, 31, 15, 46),
    )[0]
    .data
)
times = (
    st.select(station="ISM35", channel="??Z")
    .trim(
        starttime=UTCDateTime(2021, 7, 31, 15, 45),
        endtime=UTCDateTime(2021, 7, 31, 15, 46),
    )[0]
    .times()
)


sgy = _read_segy(r"SEGYS\00013586.sgy", unpack_header=True)
sample_sgy_data = sgy[18].filter("bandpass", freqmin=20, freqmax=60).data
sgy_times = sgy[18].times()

list(events.items())[0]
seed = read(r"Seeds\RawSeeds\20210731.154500.000000_20210731.154600.002000.seed")

ref_event = {
    k: v
    for k, v in events.items()
    if v["UTC Date Time"] > UTC.localize(datetime(2021, 7, 31, 15, 44))
    and v["UTC Date Time"] < UTC.localize(datetime(2021, 7, 31, 15, 46))
}

stations = {}
with open("CoP_C61-l_Stations.csv") as f:
    _head = f.readline()
    lines = f.readlines()
    for line in lines:
        split_line = line.split(",")
        stations[split_line[0]] = {
            "easting": float(split_line[1]),
            "northing": float(split_line[2]),
            "elevation": float(split_line[3]),
        }


st_130 = seed.select(station="B130")
len(st_130[0].times())

fig, ax = plt.subplots(figsize=[16, 4])
ax.plot(times, filtered_ISM_station_data, color="firebrick", alpha=0.7)
ax.plot(
    st_130[0].times(),
    st_130[0].filter("bandpass", freqmin=20, freqmax=60).data / 19.69 / 3,
    color="royalblue",
    alpha=0.7,
)
ax.set_xlim([10, 15])


f2, a2 = plt.subplots(figsize=[16, 4])
a2.plot(
    times[1000:2000], filtered_ISM_station_data[1000:2000], color="firebrick", alpha=0.7
)
a2.plot(
    st_130[0].times()[2500:5000],
    st_130[0].filter("bandpass", freqmin=20, freqmax=60).data[2500:5000] / 19.69 / 3,
    color="royalblue",
    alpha=0.7,
)

a2.set_xlim([5, 10])

for a in [ax, a2]:
    a.set_xlabel("time (s)")
    a.set_ylabel("amplitude (m/s)")

fig.savefig("total_event.png")
f2.savefig("pre-event_noise.png")

fig_map, ax_map = plt.subplots(figsize=[12, 12])
ax_map.set_aspect("equal")
for well in wells.values():
    ax_map.plot(well["easting"], well["northing"], "k")

for station in stations.values():
    ax_map.plot(station["easting"], station["northing"], "h", color="royalblue")

ax_map.plot(ism35_easting, ism35_northing, "s", color="firebrick")

ax_map.plot(
    events["4229154512095"]["easting"],
    events["4229154512095"]["northing"],
    "o",
    color="orangered",
    markeredgecolor="k",
)

gray_background_with_grid(ax_map)

fig_map.savefig("map_with_ISM35.png")
