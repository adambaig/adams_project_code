import numpy as np
import glob
import os

from obspy.imaging.mopad_wrapper import beach
from obspy import Stream, Trace, UTCDateTime
from obspy.core.trace import Stats
import utm
import matplotlib.pyplot as plt
from ray_modelling.waveform_model import get_response, add_noise
from ray_modelling.raytrace import isotropic_ray_trace

from get_inputs import get_velocity_model

dt = 0.002  # sample rate
tshift = 0.0  # shift t0 in seconds
ishift = int(tshift / dt)  # shift in samples
time_series = np.arange(0, 5.00, dt)
base_noise = 3.0e-8
n_nodes = 13
noise_level = base_noise / np.sqrt(n_nodes * 6)
easting_ctr, northing_ctr = 0, 0
depth = 2100.0  # "depth" is total vertical depth below sea level
plot_out = False  # make output plots for waveform sections
n_iterations = 20  # number of iterations for each magnitude
stress_drop = 3.0e5  # 0.3MPa
moment_tensor = np.matrix([[0.0, 1.0, 0.0], [1.0, 0.0, 0.0], [0.0, 0.0, 0.0]])
magnitude_range = np.arange(-2.5, 1.01, 0.1)
Q = {"P": 60, "S": 60}
velocity_model = get_velocity_model()


def sqa(x):
    # helper function for removing dimensions from numpy arrays or matrices
    return np.squeeze(np.array(x))


def createTraceStats(network, n1, o1, d1, component):
    # make a stats file for a seed trace
    stat = Stats()
    stat.starttime = UTCDateTime(o1)
    stat.npts = n1
    stat.delta = d1
    stat.sampling_rate = 1 / d1
    stat.component = component["component"]
    stat.channel = component["channel"]
    stat.station = component["station"]
    stat.network = network
    stat.location = component["location"]
    return stat


for lat in ["1500", "2500"]:
    for stationsCSV in [
        "./Tec_petrol_stations_30_" + lat + "_lat.csv",
        "./Tec_petrol_stations_56_" + lat + "_lat.csv",
        "./Tec_petrol_stations_90_" + lat + "_lat.csv",
    ]:
        # output seed files are dumped into a directors timestamped to minute accuracy
        outdir = str(UTCDateTime.now()).replace(":", "-")[:-11]
        os.mkdir(outdir)
        if plot_out:
            os.mkdir(outdir + "//pngs")
        g = open(outdir + "//run_parameters.txt", "w")
        g.write("lateral length: " + lat + "\n")
        g.write("station file: " + stationsCSV + "\n")
        g.write("noise level (m/s): " + str(base_noise) + "\n")
        g.write("nodes per superstation: " + str(n_nodes) + "\n")
        g.close()
        for location_string in ["center", "toe_" + lat]:
            if location_string == "toe_2500":
                xshift = 225
                yshift = 1250
            elif location_string == "toe_1500":
                xshift = 225
                yshift = 750
            elif location_string == "center":
                xshift = 0
                yshift = 0
            stations = []
            f = open(stationsCSV)
            head = f.readline()
            stationlines = f.readlines()
            f.close()
            for line in stationlines:
                lspl = line.split(",")
                stations.append(
                    {
                        "x": float(lspl[2]),
                        "y": float(lspl[1]),
                        "z": float(lspl[3]),
                        "name": lspl[0],
                    }
                )
            source = {
                "x": easting_ctr + xshift,
                "y": northing_ctr + yshift,
                "z": depth,
                "stress_drop": stress_drop,  # static stress drop
                "moment_tensor": moment_tensor,
            }
            m11, m12, m13, d1, m22, m23, d2, d3, m33 = sqa(
                source["moment_tensor"].reshape(
                    9,
                )
            )
            mt_6 = [m11, m22, m33, m12, m13, m23]
            source["moment_tensor"] = source["moment_tensor"] / np.linalg.norm(
                source["moment_tensor"]
            )
            g = open(outdir + "//synthetic_catalog_" + location_string + ".csv", "w")
            g.write("date,time,x,y,z,magnitude,stress drop,m11,m22,m33,m12,m13,m23\n")
            for mag in magnitude_range:
                base_waveforms = []
                source["moment_magnitude"] = mag
                for ii in range(len(stations)):
                    pRaypath = isotropic_ray_trace(
                        source, stations[ii], velocity_model, "P"
                    )
                    sRaypath = isotropic_ray_trace(
                        source, stations[ii], velocity_model, "S"
                    )
                    base_waveforms.append(
                        get_response(
                            pRaypath, sRaypath, source, stations[ii], Q, time_series, 0
                        )
                    )
                for i_iteration in range(n_iterations):
                    waveforms = []
                    st = Stream()
                    max_waveform = 0
                    for ii in range(len(stations)):
                        waveforms.append({})
                        for comp in ["n", "e", "d"]:
                            temp_waveform = (
                                add_noise(time_series, noise_level)
                                + base_waveforms[ii][comp]
                            )
                            waveforms[-1][comp] = np.hstack(
                                [temp_waveform[-ishift:], temp_waveform[:-ishift]]
                            )
                        if plot_out:
                            maxtemp = max(
                                [
                                    max(abs(waveforms[-1]["n"])),
                                    max(abs(waveforms[-1]["e"])),
                                    max(abs(waveforms[-1]["d"])),
                                ]
                            )
                            if maxtemp > max_waveform:
                                max_waveform = maxtemp
                    if plot_out:
                        fig, ax = plt.subplots()
                        for ii in range(len(waveforms)):
                            ax.plot(
                                0.8 * waveforms[ii]["n"] / max_waveform + ii + 0.9,
                                time_series,
                                color="steelblue",
                                alpha=0.8,
                            )
                            ax.plot(
                                0.8 * waveforms[ii]["e"] / max_waveform + ii + 1.0,
                                time_series,
                                color="firebrick",
                                alpha=0.8,
                            )
                            ax.plot(
                                0.8 * waveforms[ii]["d"] / max_waveform + ii + 1.1,
                                time_series,
                                color="darkgoldenrod",
                                alpha=0.8,
                            )

                        ax.set_ylabel("time (s)")
                        ax.set_xlabel("station")
                        ax.set_xticks(range(1, len(stations) + 1))
                        ax.set_xlim([0, len(waveforms) + 1])
                        ax.set_ylim([time_series[-1], time_series[0]])
                        fig.tight_layout()
                    for ii in range(len(waveforms)):
                        seedtime = UTCDateTime.now()
                        channel_obj = {
                            "component": "Z",
                            "channel": "CPZ",
                            "station": stations[ii]["name"],
                            "network": "CV",
                            "location": "00",
                        }
                        xStats = createTraceStats(
                            "TC", len(time_series), seedtime, dt, channel_obj
                        )
                        tr = Trace(
                            data=1e8 * waveforms[ii]["d"], header=xStats
                        )  # 1e8 is the factor deduced from the SEG2 data
                        st += tr

                    seed_out = "synthetic" + str(seedtime).replace(":", "-")
                    outfile = outdir + "//" + seed_out + ".mseed"
                    st.write(outfile)
                    dat, tim = str(seedtime).split("T")

                    g.write(dat + "," + tim[:-1] + ",")
                    g.write("%.1f,%.1f,%.1f," % (source["x"], source["y"], source["z"]))
                    g.write(
                        "%.3f,%.4e,"
                        % (source["moment_magnitude"], source["stress_drop"])
                    )
                    g.write(
                        "%.3f,%.3f,%.3f,%.3f,%.3f,%.3f\n"
                        % (m11, m22, m33, m12, m13, m23)
                    )
                    if plot_out:
                        fig.savefig(outdir + "//pngs//" + seed_out + ".png")

g.close()
