import glob

infiles = glob.glob("*center*0_relocated.csv")
infile = infiles[0]
for infile in infiles:
    f = open(infile)
    head = f.readline()
    lines = f.readlines()
    f.close()

    g = open(infile, "w")
    g.write(head)
    for line in lines:
        g.write(line[:95] + "," + line[95:99] + line[100:])
    g.close()
