import numpy as np
import matplotlib.pyplot as plt

from get_inputs import get_velocity_model, read_stations
from obspy.imaging.mopad_wrapper import beach
from matplotlib.ticker import MultipleLocator

minorLocator1 = MultipleLocator(500)
minorLocator2 = MultipleLocator(500)
minorLocator3 = MultipleLocator(500)
minorLocator4 = MultipleLocator(500)

for lat_length in [1500, 2500]:
    beach_toe = beach(
        [0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
        xy=(225, lat_length / 2),
        zorder=2,
        width=500,
        mopad_basis="NED",
        linewidth=0.5,
    )
    beach_center = beach(
        [0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
        xy=(0, 0),
        zorder=2,
        width=500,
        mopad_basis="NED",
        linewidth=0.5,
    )
    beach_toe_depth = beach(
        [0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
        xy=(lat_length / 2, -2100),
        zorder=2,
        width=500,
        mopad_basis="NED",
        linewidth=0.5,
    )
    beach_center_depth = beach(
        [0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
        xy=(0, -2100),
        zorder=2,
        width=500,
        mopad_basis="NED",
        linewidth=0.5,
    )
    for num_stations in ["30", "56", "90"]:
        stations = read_stations(
            "Tec_petrol_stations_" + num_stations + "_" + str(lat_length) + "_lat.csv"
        )

        f1, a1 = plt.subplots()
        a1.set_aspect("equal")
        # plan view
        a1.plot(
            [0, -225, -225],
            [-lat_length / 2, -lat_length / 2, lat_length / 2],
            "firebrick",
            lw=3,
            zorder=0,
        )
        a1.plot(
            [0, -225, -225],
            [-lat_length / 2, -lat_length / 2, lat_length / 2],
            "0.8",
            lw=1,
            alpha=0.5,
            zorder=1,
        )
        a1.plot(
            [0, 0, 0],
            [-lat_length / 2, -lat_length / 2, lat_length / 2],
            "firebrick",
            lw=3,
            zorder=0,
        )
        a1.plot(
            [0, 0, 0],
            [-lat_length / 2, -lat_length / 2, lat_length / 2],
            "0.8",
            lw=1,
            alpha=0.5,
            zorder=1,
        )
        a1.plot(
            [0, 225, 225],
            [-lat_length / 2, -lat_length / 2, lat_length / 2],
            "firebrick",
            lw=3,
            zorder=0,
        )
        a1.plot(
            [0, 225, 225],
            [-lat_length / 2, -lat_length / 2, lat_length / 2],
            "0.8",
            lw=1,
            alpha=0.5,
            zorder=1,
        )
        a1.set_facecolor("0.9")
        for station in stations:
            a1.plot(
                stations[station]["x"],
                stations[station]["y"],
                "h",
                color="forestgreen",
                markeredgecolor="k",
                ms=5,
                zorder=4,
            )
        a1.add_collection(beach_toe)
        a1.add_collection(beach_center)
        a1.xaxis.set_minor_locator(minorLocator1)
        a1.yaxis.set_minor_locator(minorLocator2)
        a1.set_xticklabels([])
        a1.set_yticklabels([])
        a1.grid(True, which="both")
        a1.tick_params(axis="both", which="both", color=(0, 0, 0, 0))

        f2, a2 = plt.subplots()
        a2.set_aspect("equal")
        a2.plot(
            [-lat_length / 2, -lat_length / 2, lat_length / 2],
            [984, -2100, -2100],
            "firebrick",
            lw=3,
            zorder=0,
        )
        for station in stations:
            a2.plot(
                stations[station]["y"],
                -stations[station]["z"],
                "h",
                color="forestgreen",
                markeredgecolor="k",
                ms=5,
                zorder=4,
            )
        a2.add_collection(beach_toe_depth)
        a2.add_collection(beach_center_depth)
        x1, x2 = a2.get_xlim()
        a2.plot([x1, x2], [984, 984], "k", lw=2)
        a2.set_xlim([x1, x2])
        a2.xaxis.set_minor_locator(minorLocator3)
        a2.yaxis.set_minor_locator(minorLocator4)
        a2.set_xticklabels([])
        a2.set_yticklabels([])
        a2.grid(True, which="both")
        a2.tick_params(axis="x", which="both", color=(0, 0, 0, 0))
        a2.tick_params(axis="y", which="both", color=(0, 0, 0, 0))
        a2.set_facecolor("0.9")
        f1.savefig(
            "plots//plan_view_" + num_stations + "_" + str(lat_length) + "png",
            bbox_inches="tight",
        )
        f2.savefig(
            "plots//depth_view_" + num_stations + "_" + str(lat_length) + "png",
            bbox_inches="tight",
        )
        f1.clear()
        f2.clear()

f1.clear()
