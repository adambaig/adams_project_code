import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (
    MultipleLocator,
    FormatStrFormatter,
    AutoMinorLocator,
    LogLocator,
)

import glob

matplotlib.rcParams.update({"font.size": 16})


def sqa(x):
    return np.squeeze(np.array(x))


csvs = glob.glob("synthetic_catalog_*0_st_relocated.csv")

ncolor = {"30": "firebrick", "56": "royalblue", "90": "forestgreen"}

factor = {"30": 1, "56": 1, "90": 1}

marker = {"toe": "s", "center": "o"}

noise_sample = []
xMajorLocator = MultipleLocator(0.5)
xMinorLocator = MultipleLocator(0.1)
yMajorLocator = LogLocator(10)
yMinorLocator = LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9])
image_stats = {}
M_detect = {}
for csv in csvs:
    position = csv.split("_")[3]
    nstations = csv.split("_")[2]
    lateral_length = csv.split("_")[4]
    if not (lateral_length in image_stats):
        image_stats[lateral_length] = {}
        M_detect[lateral_length] = {}
    if not (position in image_stats[lateral_length]):
        image_stats[lateral_length][position] = {}
        M_detect[lateral_length][position] = {}
    if not (nstations in image_stats[lateral_length][position]):
        image_stats[lateral_length][position][nstations] = {}
        M_detect[lateral_length][position][nstations] = {}
    f = open(csv)
    head = f.readline()
    lines = f.readlines()
    f.close()
    for line in lines:
        lspl = line.split(",")
        true_loc = np.array([float(s) for s in lspl[2:5]])
        magnitude, stress_drop = [float(s) for s in lspl[5:7]]
        m11, m22, m33, m12, m13, m23 = [float(s) for s in lspl[7:13]]
        amplitude = float(lspl[13])
        image_loc = np.array([float(s) for s in lspl[14:17]])
        if not (magnitude in image_stats[lateral_length][position][nstations]):
            image_stats[lateral_length][position][nstations][magnitude] = {}
            image_stats[lateral_length][position][nstations][magnitude]["samples"] = []
        image_stats[lateral_length][position][nstations][magnitude]["samples"].append(
            {"amplitude": amplitude, "location shift": true_loc - image_loc}
        )
        if magnitude < -2:
            noise_sample.append(factor[nstations] * amplitude)

threshold = np.average(noise_sample) + 3 * np.std(noise_sample)

patches, labels = [], []
for lateral_length in ["1500", "2500"]:
    f1, a1 = plt.subplots(3, 2, figsize=[16, 10])
    f2, a2 = plt.subplots(figsize=[16, 10])
    patches, labels = [], []
    for position in ["toe", "center"]:
        for nstations in ["30", "56", "90"]:
            bias = []
            spread = []
            average_amp = []
            mags = image_stats[lateral_length][position][nstations].keys()
            for mag in mags:
                nsamples = len(
                    image_stats[lateral_length][position][nstations][mag]["samples"]
                )
                shifts = [
                    s["location shift"]
                    for s in image_stats[lateral_length][position][nstations][mag][
                        "samples"
                    ]
                ]
                amps = [
                    s["amplitude"]
                    for s in image_stats[lateral_length][position][nstations][mag][
                        "samples"
                    ]
                ]
                bias.append(np.average(shifts, axis=0))
                spread.append(np.std(shifts, axis=0))
                average_amp.append(np.average(amps))
                image_stats[lateral_length][position][nstations][mag]["bias"] = bias[-1]
                image_stats[lateral_length][position][nstations][mag][
                    "spread"
                ] = spread[-1]
                image_stats[lateral_length][position][nstations][mag][
                    "amplitude"
                ] = average_amp[-1]
            mags = np.array([float(s) for s in mags])
            i_plot = np.where(np.array(average_amp) * factor[nstations] > threshold)[0]
            x_spread = np.array(spread)[:, 0]
            x_bias = abs(np.array(bias)[:, 1])
            y_spread = np.array(spread)[:, 1]
            y_bias = abs(np.array(bias)[:, 1])
            z_spread = np.array(spread)[:, 2]
            z_bias = abs(np.array(bias)[:, 2])
            average_amp = np.array(average_amp)
            (p,) = a1[0, 0].plot(
                mags[i_plot],
                x_bias[i_plot],
                marker=marker[position],
                color=ncolor[nstations],
                markeredgecolor="k",
            )
            a1[0, 1].plot(
                mags[i_plot],
                x_spread[i_plot],
                marker=marker[position],
                color=ncolor[nstations],
                markeredgecolor="k",
            )
            a1[1, 0].plot(
                mags[i_plot],
                y_bias[i_plot],
                marker=marker[position],
                color=ncolor[nstations],
                markeredgecolor="k",
            )
            a1[1, 1].plot(
                mags[i_plot],
                y_spread[i_plot],
                marker=marker[position],
                color=ncolor[nstations],
                markeredgecolor="k",
            )
            a1[2, 0].plot(
                mags[i_plot],
                z_bias[i_plot],
                marker=marker[position],
                color=ncolor[nstations],
                markeredgecolor="k",
            )
            a1[2, 1].plot(
                mags[i_plot],
                z_spread[i_plot],
                marker=marker[position],
                color=ncolor[nstations],
                markeredgecolor="k",
            )
            patches.append(p)
            labels.append(nstations + " superstations: " + position)
            a2.semilogy(
                mags,
                factor[nstations] * average_amp,
                marker=marker[position],
                color=ncolor[nstations],
                markeredgecolor="k",
            )
            ilarge = np.where(factor[nstations] * average_amp > threshold)[0][2:10]
            large_amps = np.matrix(np.log10(factor[nstations] * average_amp[ilarge]))
            Amatrix = np.matrix(
                np.vstack(
                    [mags[ilarge] * mags[ilarge], mags[ilarge], np.ones(len(ilarge))]
                )
            ).T
            quad, slope, yint = [
                sqa(s)
                for s in np.linalg.inv(Amatrix.T * Amatrix) * Amatrix.T * large_amps.T
            ]
            detectability_line = 10 ** (mags * mags * quad + mags * slope + yint)
            """
            M_detect[lateral_length][position][nstations] = (
                (
                    -slope
                    + np.sqrt(slope * slope - 4 * quad * (yint - np.log10(threshold)))
                )
                / 2
                / quad
            )
            """
            M_detect[lateral_length][position][nstations] = np.interp(
                threshold, factor[nstations] * average_amp, mags
            )
            # a2.semilogy(mags, detectability_line, "k:", lw=1)
    dum, y1 = a1[0, 0].get_ylim()
    dum, y2 = a1[1, 0].get_ylim()
    dum, y3 = a1[0, 1].get_ylim()
    dum, y4 = a1[1, 1].get_ylim()
    dum, y5 = a1[2, 0].get_ylim()
    dum, y6 = a1[2, 1].get_ylim()
    y_max = max([y1, y2, y3, y4, y5, y6])

    a1[0, 1].legend(patches, labels)
    a1[0, 0].set_title("bias")
    a1[0, 1].set_title("spread")
    a1[0, 0].set_ylabel("x horizontal distance (m)")
    a1[1, 0].set_ylabel("y horizontal distance (m)")
    a1[2, 0].set_ylabel("vertical distance (m)")
    a1[0, 1].set_ylabel("x horizontal distance (m)")
    a1[1, 1].set_ylabel("y horizontal distance (m)")
    a1[2, 1].set_ylabel("vertical distance (m)")
    a1[2, 0].set_xlabel("moment magnitude")
    a1[2, 1].set_xlabel("moment magnitude")
    a1[0, 0].set_ylim([-0.05 * y_max, y_max])
    a1[1, 0].set_ylim([-0.05 * y_max, y_max])
    a1[0, 1].set_ylim([-0.05 * y_max, y_max])
    a1[1, 1].set_ylim([-0.05 * y_max, y_max])
    a1[2, 0].set_ylim([-0.05 * y_max, y_max])
    a1[2, 1].set_ylim([-0.05 * y_max, y_max])
    a1[0, 0].grid(True)
    a1[1, 0].grid(True)
    a1[0, 1].grid(True)
    a1[1, 1].grid(True)
    a1[2, 0].grid(True)
    a1[2, 1].grid(True)

    a2.grid(True)
    a2.set_xlabel("moment magnitude")
    a2.set_ylabel("amplitude")
    a2.legend(patches, labels)
    a2.semilogy([-4, 4], [threshold, threshold], "-.", lw=2, zorder=-3, color="k")
    a2.text(-2.38, 1.03 * threshold, "2$\sigma$ detection threshold")
    a2.set_xlim([-2.55, -1.05])
    a2.set_ylim([0.02, 10])
    a2.set_title(lateral_length + "m Lateral")
plt.show()

M_detect
