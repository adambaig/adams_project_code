import numpy as np
import glob

from obspy import read, UTCDateTime
import utm

from DataHandling.dataProcessing import (processMseed,convertStreamToRSF, 
					mergeEvents, stationCSVtoRSF,
					convertNumpyToRSF)
from Velocity.velocity import createTTT_1D,create2Dfrom1D_vel,createRSFfromCSV_1D
from Imaging.imaging import relocate_Event
from ray_modelling.raytrace import isotropic_ray_trace
import rsf.api as rsf

from get_inputs import get_velocity_model

dt = 0.002 # sample rate
time_series = np.arange(0, 5.00, dt)
easting_ctr,northing_ctr = 0,0
depth = 2100.
datum=-984
sigma_static = 0.005
freqmin,freqmax,resample = 20.,50.,125.
outStations='Tec_petrol_stations.rsf'
velocity_model = get_velocity_model()

seed_dir= {
	"30_1500": "2019-04-04T19-04",
	"56_1500": "2019-04-04T19-15",
	"90_1500": "2019-04-04T19-33",
	"30_2500": "2019-04-04T20-02",
	"56_2500": "2019-04-04T20-13",
	"90_2500": "2019-04-04T20-31"
}
base_dir = '//data//mount_doom//home//adamb//TecPetrol//'
def sqa(x):
    return(np.squeeze(np.array(x)))

for stationsCSV in [ base_dir+'Tec_petrol_stations_30_1500_lat.csv',
                     base_dir+'Tec_petrol_stations_56_1500_lat.csv',
                     base_dir+'Tec_petrol_stations_90_1500_lat.csv',
                     base_dir+'Tec_petrol_stations_30_2500_lat.csv',
                     base_dir+'Tec_petrol_stations_56_2500_lat.csv',
                     base_dir+'Tec_petrol_stations_90_2500_lat.csv']:
    nstations = stationsCSV.split('_')[-3]
    lat_length = stationsCSV.split('.')[-2].split('_')[-2]
    outStations = 'Tec_petrol_stations_'+nstations+'_'+lat_length+'.rsf'
    stationFile=stationCSVtoRSF(stationsCSV, outStations,elevation=False, 
					sort=False)
    file_dir = base_dir+seed_dir[nstations+'_'+lat_length]+'//'
    velcsv= base_dir+'discretized_vm.csv'
    vel1Drsf= base_dir+'TecPetrol_vp_1D.rsf'
    vel2Drsf= base_dir+'TecPetrol_vp_2D.rsf'
    #sampling in depth (m)
    input_delta_z=10
    #depth (m), positive down
    vpCol=0
    #maximum distance of output 2D model (m)
    maxXYdist=10000
    #sampling x,y in (m)
    output_delta_xy=100
    init_Vp_TTT= base_dir+'TecPetrol_init_Vp_TTT_'+nstations+'.rsf'
    #create 1D rsf file from regularly sampled csv
    vel1D=createRSFfromCSV_1D(velcsv,vel1Drsf,input_delta_z,datum=datum,col=vpCol,hasHeaders=True)
    #Extrapolate 1D to 2D velocity profile
    vel2D=create2Dfrom1D_vel(vel1Drsf,vel2Drsf,maxXYdist,output_delta_xy)
    #Create travelTime table from 2D profile, remove 2D because it is not needed or useful anymore
    TTT=createTTT_1D(vel2Drsf,init_Vp_TTT,outStations,clean=True)
    input=rsf.Input(TTT)
    n1=input.int('n1')
    n2=input.int('n2')
    d1=input.int('d1')
    d2=input.int('d2')
    o1=input.int('o1')
    o2=input.int('o2')
    TTT2_Judgement_Day = np.zeros((n2,n1),dtype='float32')
    input.read(TTT2_Judgement_Day)
    for location_string in ['toe','center']:
        if location_string == 'toe':
            xshift = 225
	    yshift = float(lat_length)/2
            catalog = file_dir+'synthetic_catalog_toe_'+lat_length+'.csv'
        elif location_string =='center':
            xshift = 0
            yshift = 0
            catalog = file_dir+'synthetic_catalog_center.csv'
        ox = easting_ctr+xshift
        oy = northing_ctr+yshift
        oz = depth
        oo = [oz,oy,ox]
        nn = [50,50,50]
        dd = [4,4,4]
        f = open(catalog)
        head = f.readline()
        lines = f.readlines()
        f.close()

        f = open(stationsCSV)
        station_lines = f.readlines()
        f.close()
        tt_diff = []
	for station_line in station_lines:
            lspl = station_line.split(',')
	    st_x,st_y,st_z = [float(s) for s in lspl[1:4]]
	    station = {"x": st_x, "y": st_y, "z": st_z}
	    source = {"x": ox, "y": oy, "z": oz}
	    pRaypath = isotropic_ray_trace(source,station,velocity_model,"P")
            zInd = int((oz-st_z)/d1)
            xyDist = np.sqrt((ox-st_x)**2 + (oy-st_y)**2)
            xyInd = int(xyDist/d2)
            remX = xyDist/d2 - xyInd
            remZ = 0.
            c00=(TTT2_Judgement_Day[xyInd  ][zInd  ]*(1.-remZ)+
		 TTT2_Judgement_Day[xyInd  ][zInd+1]*(  remZ))
            c01=(TTT2_Judgement_Day[xyInd+1][zInd  ]*(1.-remZ)+
		 TTT2_Judgement_Day[xyInd+1][zInd+1]*(  remZ))
	    travelTime = c00*(1.-remX) + c01*(remX)
            tt_diff.append(pRaypath['traveltime'] - travelTime)

        tt_diff = np.array(tt_diff)
        g = open(file_dir+'synthetic_catalog_'+nstations+'_'+
			location_string+'_'+lat_length+'_st_relocated.csv','w')
        g.write('date,time,x,y,z,magnitude,stress drop,m11,m22,m33,m12,m13,m23\n')
        for line in lines:
            lspl = line.split(',')
            x,y,z = [float(s) for s in lspl[2:5]]
            ox = x
            oy = y
            oz = z
            oo = [oz,oy,ox]
            nn = [50,50,50]
            dd = [4,4,4]
            dat,tim = lspl[0],lspl[1]
            seedfile = 'synthetic'+dat+'T'+tim.replace(':','-')+'Z.mseed'
            print(seedfile)
            st = read(file_dir+seedfile)
            process_stream=processMseed(st, freqmin=freqmin, freqmax=freqmax, resample=resample, doRadRemove=True)
            image_file=convertStreamToRSF(process_stream,'image',pad=0)
            image_statics=convertNumpyToRSF(tt_diff+sigma_static*np.random.randn(int(nstations)),'statics.rsf')

            image_amp,rel_dp, rel_nr, rel_es, rel_t0 = relocate_Event(image_file,
                                init_Vp_TTT,
                                outStations,
                                nn,dd,oo,nthreads=4,statics='statics.rsf')
            g.write(line[:-2]+',')	
            g.write('%.4e,%.1f,%.1f,%.1f\n' %(image_amp, rel_es, rel_nr, rel_dp))
#            print('x difference: %.3f' % (source['x'] - rel_es))
#            print('y difference: %.3f' % (source['y'] - rel_nr))
#            print('z difference: %.3f' % (source['z'] - rel_dp))
#            print('image amplitude: %.3e' % (image_amp))

    g.close()
