import matplotlib

matplotlib.use("Qt5Agg")

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (
    MultipleLocator,
    FormatStrFormatter,
    AutoMinorLocator,
    LogLocator,
)

import glob


def sqa(x):
    return np.squeeze(np.array(x))


drs = ["30 stations", "56 stations", "90 stations", "120 stations", "180 stations"]
noisedrs = ["3e-8", "1e-7", "3e-7"]
image_stats = {}

ncolor = {
    "30 stations": "firebrick",
    "56 stations": "royalblue",
    "90 stations": "forestgreen",
    "120 stations": "darkgoldenrod",
    "180 stations": "indigo",
}

marker = {"toe": "s", "center": "o"}

xMajorLocator = MultipleLocator(0.5)
xMinorLocator = MultipleLocator(0.1)
yMajorLocator = LogLocator(10)
yMinorLocator = LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9])

fig, ax = plt.subplots(5, 2)
ii = -1
for dr in drs:
    ii += 1
    image_stats[dr] = {}
    for noisedr in noisedrs:
        csvs = glob.glob(dr + "//" + noisedr + "//*.csv")
        image_stats[dr][noisedr] = {}
        jj = -1
        for csv in csvs:
            jj += 1
            f = open(csv)
            head = f.readline()
            lines = f.readlines()
            f.close()
            tag = csv.split(".")[0].split("_")[-1]
            image_stats[dr][noisedr][tag] = []
            for line in lines:
                lspl = line.split(",")
                true_loc = np.array([float(s) for s in lspl[2:5]])
                magnitude, stress_drop = [float(s) for s in lspl[5:7]]
                m11, m22, m33, m12, m13, m23 = [float(s) for s in lspl[7:13]]
                amplitude = float(lspl[13])
                image_loc = np.array([float(s) for s in lspl[14:17]])
                image_stats[dr][noisedr][tag].append(
                    {
                        "magnitude": magnitude,
                        "amplitude": amplitude,
                        "location shift": true_loc - image_loc,
                    }
                )
            mag = [v["magnitude"] for v in image_stats[dr][noisedr][tag]]
            amp = [v["amplitude"] for v in image_stats[dr][noisedr][tag]]
            ax[ii, jj].semilogy(mag, amp, marker=marker[tag], color=ncolor[dr])
            ax[ii, jj].set_xlim([-2.6, -0])
            ax[ii, jj].set_ylim([0.01, 100])
            ax[ii, jj].grid(True, which="both")
            ax[ii, jj].yaxis.set_major_locator(yMajorLocator)
            ax[ii, jj].yaxis.set_minor_locator(yMinorLocator)
            ax[ii, jj].xaxis.set_major_locator(xMajorLocator)
            ax[ii, jj].xaxis.set_minor_locator(xMinorLocator)
ax[0, 0].set_title("toe")
ax[0, 1].set_title("center")
ax[0, 0].set_ylabel("amplitude")
ax[1, 0].set_ylabel("amplitude")
ax[2, 0].set_ylabel("amplitude")
ax[2, 0].set_xlabel("magnitude")
ax[2, 1].set_xlabel("magnitude")
ax[0, 0].set_xticklabels([])
ax[1, 0].set_xticklabels([])
ax[0, 1].set_xticklabels([])
ax[1, 1].set_xticklabels([])
ax[0, 1].set_yticklabels([])
ax[1, 1].set_yticklabels([])
ax[2, 1].set_yticklabels([])
factor = [
    np.sqrt(30.0 / 90.0),
    np.sqrt(56.0 / 90.0),
    1.0,
    np.sqrt(120 / 90),
    np.sqrt(180.0 / 90),
]
f2, a2 = plt.subplots(3, 2)
ii = -1
incoherent_stacks = []
stacks = []
M_detect = {}
for noisedr in noisedrs:
    jj = -1
    ii += 1
    M_detect[noisedr] = {}
    for tag in ["center", "toe"]:
        M_detect[noisedr][tag] = {}
        jj += 1
        kk = -1
        for dr in drs:
            M_detect[noisedr][tag][dr] = {}
            kk += 1
            mag = np.array([v["magnitude"] for v in image_stats[dr][noisedr][tag]])
            amp = np.array([v["amplitude"] for v in image_stats[dr][noisedr][tag]])
            a2[ii, jj].semilogy(
                mag, factor[kk] * amp, marker=marker[tag], color=ncolor[dr]
            )
            ilow = np.where(mag < -1.7)[0]
            incoherent_stacks.append(factor[kk] * amp[ilow])
    stacks.append(np.array(incoherent_stacks).flatten())
    incoherent_stacks = []
    threshold = np.average(stacks[-1]) + 3 * np.std(stacks[-1])
    jj = -1
    for tag in ["center", "toe"]:
        kk = -1
        jj += 1
        for dr in drs:
            kk += 1
            mag = np.array([v["magnitude"] for v in image_stats[dr][noisedr][tag]])
            amp = np.array([v["amplitude"] for v in image_stats[dr][noisedr][tag]])
            idetectable = np.where(factor[kk] * amp > threshold)[0]
            ilarge = np.where(mag > -0.0)[0]
            large_amps = np.matrix(np.log10(factor[kk] * amp[ilarge]))
            Amatrix = np.matrix(
                np.vstack(
                    [mag[ilarge] * mag[ilarge], mag[ilarge], np.ones(len(ilarge))]
                )
            ).T
            quad, slope, yint = [
                sqa(s)
                for s in np.linalg.inv(Amatrix.T * Amatrix) * Amatrix.T * large_amps.T
            ]
            detectability_line = 10 ** (mag * mag * quad + mag * slope + yint)
            M_detect[noisedr][tag][dr] = (
                (
                    -slope
                    + np.sqrt(slope * slope - 4 * quad * (yint - np.log10(threshold)))
                )
                / 2
                / quad
            )
            a2[ii, jj].plot(mag, detectability_line, "k:", lw=1)
            a2[ii, jj].set_xlim([-2.6, 1])
            a2[ii, jj].set_ylim([0.01, 1000])
            a2[ii, jj].grid(True, which="both")
            a2[ii, jj].yaxis.set_major_locator(yMajorLocator)
            a2[ii, jj].yaxis.set_minor_locator(yMinorLocator)
            a2[ii, jj].xaxis.set_major_locator(xMajorLocator)
            a2[ii, jj].xaxis.set_minor_locator(xMinorLocator)
        a2[ii, jj].legend(drs)
a2[0, 0].set_title("center of pad")
a2[0, 1].set_title("toe of well")
a2[2, 0].set_xlabel("magnitude")
a2[2, 1].set_xlabel("magnitude")
a2[0, 0].set_ylabel("amplitude")
a2[1, 0].set_ylabel("amplitude")
a2[2, 0].set_ylabel("amplitude")

n_color = {"3e-7": "red", "1e-7": "orange", "3e-8": "yellow"}

f3, a3 = plt.subplots()
for noise_level in M_detect:
    n_level = float(noise_level)
    for position in M_detect[noise_level]:
        for n_stations in M_detect[noise_level][position]:
            mag_detect = M_detect[noise_level][position][n_stations]
            n_stat = int(n_stations.split()[0])
            a3.plot(
                n_stat,
                mag_detect,
                marker[position],
                color=n_color[noise_level],
                markeredgecolor="k",
            )

a3.grid(True)
a3.set_xlabel("number of superstations")
a3.set_ylabel("detectable magnitude")

plt.show()
