import matplotlib

matplotlib.use("Qt5Agg")
import numpy as np
import matplotlib.pyplot as plt

import utm


def place_stations(writeout=False):
    lat_ctr, lon_ctr = -37.445049, -69.164669
    target_depth = 3500
    lateral_length = 1300
    pad_width = 200
    nstations = 120
    xwide = target_depth + pad_width / 2
    ywide = target_depth + lateral_length / 2

    easting_ctr, northing_ctr, d1, d2 = utm.from_latlon(lat_ctr, lon_ctr)

    xrange = np.linspace(easting_ctr - xwide, easting_ctr + xwide, 12)
    yrange = np.linspace(northing_ctr - ywide, northing_ctr + ywide, 15)

    stations = []
    jj = -1
    for ix in xrange:
        for iy in yrange:
            jj += 1
            stations.append({"name": str(jj), "x": ix, "y": iy, "z": -1000})
    if writeout:
        g = open("Tec_petrol_stations_180.csv", "w")
        g.write("station, northing (m), easting (m), tvdss (m)\n")
        for station in stations:
            g.write(
                station["name"]
                + ",%.1f,%.1f,%.1f\n" % (station["y"], station["x"], station["z"])
            )
        g.close()
    return stations


def read_velocity_model():
    kb = 1000
    dr = "O:\\O&G\\SalesSupport\\20190305_Paramount_2-28\\MC\\"
    f = open(dr + "DSA.mdl")
    lines = f.readlines()
    f.close()
    layer_colors = [
        "darkkhaki",
        "goldenrod",
        "indigo",
        "slategrey",
        "cadetblue",
        "sienna",
        "darksteelblue",
        "darkorange",
        "saddlebrown",
        "darkgreen",
        "olivedrab",
    ]
    velocity_model = []
    ii = -1
    for line in lines:
        ii += 1
        lspl = line.split()
        vp = 1000 * float(lspl[0])
        vs = 1000 * float(lspl[1])
        rho = 310 * vp ** (0.25)  # Gardner's relation
        top = 1000 * float(lspl[2]) - kb
        if ii == 0:
            velocity_model.append(
                {"rho": rho, "vp": vp, "vs": vs, "color": layer_colors[ii]}
            )
        else:
            velocity_model.append(
                {"top": top, "rho": rho, "vp": vp, "vs": vs, "color": layer_colors[ii]}
            )

    return velocity_model
