import matplotlib

matplotlib.use("Qt5agg")

import numpy as np

import matplotlib.pyplot as plt
from obspy import UTCDateTime
from obspy.imaging.beachball import beachball, beach
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.cm as cm
from matplotlib.colors import Normalize
import matplotlib as mpl
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import MultipleLocator

import mplstereonet

from read_inputs import read_events, read_wells, rotate

# INPUTS #

ddFile = None
outputFigDir = "figures"
rotation_degrees = 130
refElevation = 0
r2Thresh = 0.6
condThresh = 25.0
minStas = 6.0
mtSize = 80
sizeByMw = False
sizeRange = [0.01, 0.05]
# Station info format [Sta,lat,lon,elevation,... etc]
UseStaInfo = False
staFile = r"O:\O&G\Projects\SO50558 (Repsol Ferrier PFI Pilot)\PFIProject\Deliverables\PFI Client Deliverables\Station Metadata\stationData_virtualChannels.csv"
staColor = "grey"
staMarker = "^"
staAlpha = 0.5
LatLon = True
xlimit = "auto"  # Set to 'auto' or specify two values e.g. ..= [10, 50]
ylimit = "auto"
zlimit = "auto"
plot = "2d"  # use '2d' (plan view) or '3d'

minorLocator1 = MultipleLocator(500)
majorLocator1 = MultipleLocator(500)

# INPUT END #

events = read_events()
wells = read_wells()
offset = {
    "easting": np.average(np.array([v["easting"] for k, v in events.items()])),
    "northing": np.average(np.array([v["northing"] for k, v in events.items()])),
}
for well in wells:
    wells[well] = rotate(wells[well], rotation_degrees, offset=offset)
for event in events:
    events[event] = rotate(events[event], rotation_degrees, offset=offset)


if not os.path.isdir(outputFigDir):
    os.makedirs(outputFigDir)

print("Starting Event Count:", len(events))

gs = GridSpec(1, 3)
fig = plt.figure(figsize=(20, 16))
ax = fig.add_axes([0.1, 0.2, 0.8, 0.7])
ax_cbar = fig.add_axes([0.3, 0.1, 0.4, 0.04])


mags = np.array([v["mw"] for k, v in events.items()])
times = np.array([UTCDateTime(v["t0"]).timestamp for k, v in events.items()])
minMag = np.min(mags)
maxMag = np.max(mags)
cmap = cm.gnuplot
norm = Normalize(vmin=min(times), vmax=max(times))
norm_relative = Normalize(vmin=0, vmax=(np.ceil((max(times) - min(times)) / 86400)))
xys = []


np.ceil((max(times) - min(times)) / 86400)

sel_events = {
    k: v
    for k, v in events.items()
    if (
        v["rsq"] >= r2Thresh
        and v["cond_num"] <= condThresh
        and v["num_picks"] >= minStas
    )
}


for id in sel_events:
    mt = np.array(
        [
            events[id]["strike"] + rotation_degrees,
            events[id]["dip"],
            events[id]["rake"],
        ]
    )
    if sizeByMw:
        normScale = (events[id]["mw"] + (0 - minMag)) / (maxMag + (0 - minMag))
        size = (normScale * (sizeRange[1] - sizeRange[0])) + sizeRange[0]
        bb = beach(
            mt,
            xy=(events[id]["h1"], events[id]["h2"]),
            width=size,
            facecolor=cmap(norm(UTCDateTime(events[id]["t0"]).timestamp))[0:3],
            alpha=0.8,
            linewidth=0.25,
            zorder=3,
        )
    else:
        bb = beach(
            mt,
            xy=(events[id]["h1"], events[id]["h2"]),
            width=mtSize,
            facecolor=cmap(norm(UTCDateTime(events[id]["t0"]).timestamp))[0:3],
            alpha=0.8,
            linewidth=0.25,
            zorder=3,
        )
    ax.add_collection(bb)
    xys.append([events[id]["h1"], events[id]["h2"]])

print("Selected Event Count:", len(sel_events))


for well in wells:
    ax.plot(wells[well]["h1"], wells[well]["h2"], c="firebrick", zorder=-4, lw=5)
    ax.plot(wells[well]["h1"], wells[well]["h2"], c="0.8", zorder=-3, lw=3)

if ddFile is not None:
    dd = np.genfromtxt(ddFile, delimiter=",", dtype=str)
    for d in dd:
        d[1], d[2] = Proj(d[1], d[2])
    plt.scatter(
        dd[:, 1].astype(float),
        dd[:, 2].astype(float),
        s=1,
        c="red",
        alpha=0.25,
    )

ax.set_aspect("equal")
xys = np.array(xys)
if xlimit == "auto":
    ax.set_xlim(
        np.min(1.05 * xys[:, 0].astype(float)),
        1.05 * np.max(xys[:, 0].astype(float)),
    )
else:
    ax.set_xlim(xlimit[0], xlimit[1])
if ylimit == "auto":
    ax.set_ylim(
        np.min(1.05 * xys[:, 1].astype(float)),
        1.05 * np.max(xys[:, 1].astype(float)),
    )
else:
    ax.set_ylim(ylimit[0], ylimit[1])

ax.set_facecolor("0.95")
ax.grid(True)
ax.xaxis.set_major_locator(MultipleLocator(500))
ax.yaxis.set_major_locator(MultipleLocator(500))
ax.set_yticklabels([])
ax.set_xticklabels([])
ax.tick_params(axis="both", which="both", color="w")
cb = mpl.colorbar.ColorbarBase(
    ax_cbar, cmap=cm.gnuplot, norm=norm_relative, orientation="horizontal"
)
cb.set_label("days since start of completion")
plt.show()
