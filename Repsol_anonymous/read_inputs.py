import numpy as np
import os
import pyproj
import logging

Proj = pyproj.Proj(init="epsg:32611")


def read_events():

    eventFile = (
        "O:\\O&G\\Projects\\SO50558 (Repsol Ferrier PFI Pilot)\\"
        + "PFIProject\\Deliverables\\PFI Client Deliverables\\"
        + "Focal Mechanisms\\Repsol_Ferrier_PFI_MTI_solutions.csv"
    )

    f = open(eventFile)
    head = f.readline()
    lines = f.readlines()
    f.close()

    events = {}
    for line in lines:
        lspl = line.split(",")
        id = lspl[0]
        events[id] = {}
        lon, lat, events[id]["depth_km"] = [float(s) for s in lspl[1:4]]
        events[id]["easting"], events[id]["northing"] = Proj(lon, lat)
        events[id]["t0"] = lspl[4]
        events[id]["strike"], events[id]["dip"], events[id]["rake"] = [
            float(s) for s in lspl[5:8]
        ]
        events[id]["mw"], events[id]["rsq"], events[id]["cond_num"] = [
            float(s) for s in lspl[17:20]
        ]
        events[id]["num_picks"] = int(lspl[20])
    return events


def read_wells():
    well_dir = (
        "O:\\O&G\\Projects\\SO50558 (Repsol Ferrier PFI Pilot)\\"
        + "PFIProject\\FromClient\\Well_Deviations\\"
    )
    well_files = [
        os.path.join(well_dir, f)
        for f in os.listdir(well_dir)
        if f.split(".")[-1] == "csv"
    ]
    wells = {}
    for well_file in well_files:
        well = well_file.split("\\")[-1].split(".csv")[0]
        wells[well] = {}
        f = open(well_file)
        head = f.readline()
        lines = f.readlines()
        f.close()
        nl = len(lines)
        wells[well]["easting"], wells[well]["northing"], wells[well]["TVD"] = (
            np.zeros(nl),
            np.zeros(nl),
            np.zeros(nl),
        )
        for i_line, line in enumerate(lines):
            lspl = line.split(",")
            (
                wells[well]["easting"][i_line],
                wells[well]["northing"][i_line],
                wells[well]["TVD"][i_line],
            ) = [float(s) for s in lspl]

    return wells


def sqa(x):
    return np.squeeze(np.array(x))


def rotate(dict, angle, offset={"easting": 0, "northing": 0}):
    D2R = np.pi / 180.0
    angle_radians = D2R * angle
    rotation_matrix = np.matrix(
        [
            [np.cos(angle_radians), np.sin(angle_radians)],
            [-np.sin(angle_radians), np.cos(angle_radians)],
        ]
    )

    if "easting" in dict and "northing" in dict:
        if type(dict["easting"]) == float:
            h1, h2 = [
                sqa(x)
                for x in rotation_matrix
                * np.matrix(
                    [
                        dict["easting"] - offset["easting"],
                        dict["northing"] - offset["northing"],
                    ]
                ).T
            ]
        else:
            h1, h2 = [
                sqa(x)
                for x in rotation_matrix
                * np.matrix(
                    [
                        dict["easting"] - offset["easting"],
                        dict["northing"] - offset["northing"],
                    ]
                )
            ]
        dict["h1"] = h1
        dict["h2"] = h2
        return dict
    else:
        logging.error("northing and easting keys not in dictionary")
        return None
