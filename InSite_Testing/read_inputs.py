from datetime import datetime, timedelta

import pyproj as pr
from obspy import read_inventory

from NocMeta.Meta import NOC_META


LATLON_PROJ = pr.Proj(init="epsg:4326")
OUT_PROJ = pr.Proj(init=f"epsg:{NOC_META['FBK']['epsg']}")


def read_insite_catalog(filename="catalogs\\events_insite.csv"):
    with open(filename) as f:
        _head = f.readline()
        lines = f.readlines()
    events = {}
    for line in lines:
        lspl = line.split(",")
        datetime_object = datetime.strptime(
            lspl[2] + lspl[3] + lspl[4].split(".")[1][:6], "%d/%m/%Y%H:%M:%S%f"
        )
        event_id = datetime.strftime(datetime_object, "%Y%m%d%H%M%S%f")[:-3]
        events[event_id] = {
            "datetime": datetime_object + timedelta(0, float(lspl[21])),
            "northing": float(lspl[9]),
            "easting": float(lspl[10]),
            "depth": float(lspl[11]),
            "elevation": -float(lspl[11]),
            "located": int(lspl[8]) == 1,
            "location_error": float(lspl[13]),
            "residual": float(lspl[14]),
            "angular_residual": float(lspl[15]),
            "location_magnitude": float(lspl[22]),
            "moment_magnitude": float(lspl[23]),
            "instrument_magnitude": float(lspl[24]),
            "energy": float(lspl[25]),
            "es:ep": float(lspl[26]),
            "p_corner_frequency": float(lspl[27]),
            "s_corner_frequency": float(lspl[28]),
            "apparent_stress": float(lspl[29]),
            "static_stress_drop": float(lspl[30]),
            "source_radius": float(lspl[31]),
        }
    return events


def read_athena_catalog(filename="catalogs\\events_athena.csv"):
    with open(filename) as f:
        _head = f.readline()
        lines = f.readlines()
    events = {}
    for line in lines:
        lspl = line.split(",")
        datetime_object = datetime.strptime(lspl[3], "%Y-%m-%dT%H:%M:%SZ")
        event_id = datetime.strftime(datetime_object, "%Y%m%d%H%M%S")
        events[event_id] = {
            "datetime": datetime_object,
            "latitude": float(lspl[6]),
            "longitude": float(lspl[7]),
            "local magnitude": float(lspl[10]),
            "depth km": float(lspl[12]),
            "elevation": -float(lspl[12]) * 1000,
        }
    events = add_easting_and_northing(events)
    return events


def add_easting_and_northing(dictionary):
    longitudes = [v["longitude"] for v in dictionary.values()]
    latitudes = [v["latitude"] for v in dictionary.values()]
    eastings, northings = pr.transform(LATLON_PROJ, OUT_PROJ, longitudes, latitudes)
    for easting, northing, key in zip(eastings, northings, dictionary):
        dictionary[key]["easting"] = easting
        dictionary[key]["northing"] = northing
    return dictionary


def get_stations():
    INV = read_inventory(r"C:\Users\adambaig\Project\Braskem\Station_xml\FBK_Full.xml")
    stations = {}
    for net in INV:
        for station in net:
            for channel in station:
                if "DPZ" in channel.code or "HHZ" in channel.code:
                    stations[f"{net.code}.{station.code}."] = {
                        "elevation": station.elevation - channel.depth,
                        "longitude": station.longitude,
                        "latitude": station.latitude,
                    }
    stations = add_easting_and_northing(stations)
    return stations
