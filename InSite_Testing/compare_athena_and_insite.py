from datetime import datetime

import matplotlib.pyplot as plt

from read_inputs import read_insite_catalog, read_athena_catalog, get_stations
from generalPlots import gray_background_with_grid

insite_catalog = read_insite_catalog()
athena_catalog = read_athena_catalog()
stations = get_stations()
insite_events = {k: v for k, v in insite_catalog.items() if v["located"]}

x1, x2 = 197986.58688787775, 198854.75025504341
y1, y2 = 8932712.230823414, 8934324.638987586
pairs = [
    {"x": "easting", "y": "northing"},
    {"x": "easting", "y": "elevation"},
    {"x": "northing", "y": "elevation"},
]
for pair in pairs:
    fig, ax = plt.subplots(figsize=[12, 12])
    ax.set_aspect("equal")
    ax.plot(
        [v[pair["x"]] for v in insite_catalog.values()],
        [v[pair["y"]] for v in insite_catalog.values()],
        "o",
        color="firebrick",
        alpha=0.5,
    )
    ax.plot(
        [v[pair["x"]] for v in athena_catalog.values()],
        [v[pair["y"]] for v in athena_catalog.values()],
        "o",
        color="royalblue",
        alpha=0.5,
    )
    ax.plot(
        [v[pair["x"]] for v in stations.values()],
        [v[pair["y"]] for v in stations.values()],
        "v",
        color="0.1",
        alpha=0.8,
    )
    gray_background_with_grid(ax, grid_spacing=50)
